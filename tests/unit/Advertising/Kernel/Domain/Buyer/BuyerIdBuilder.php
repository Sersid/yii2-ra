<?php
declare(strict_types=1);

namespace Tests\Advertising\Kernel\Domain\Buyer;

use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

class BuyerIdBuilder
{
    private int $id;

    public function __construct()
    {
        $this->id = 1;
    }

    public function withId(int $id)
    {
        $this->id = $id;
    }

    public function build(): BuyerId
    {
        return new BuyerId($this->id);
    }
}
