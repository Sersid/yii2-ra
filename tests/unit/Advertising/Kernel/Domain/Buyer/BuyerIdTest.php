<?php
declare(strict_types=1);

namespace Tests\Advertising\Kernel\Domain\Buyer;

use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use PHPUnit\Framework\TestCase;

class BuyerIdTest extends TestCase
{
    public function testCreate()
    {
        $value = 1;
        $buyerId = new BuyerId($value);
        $this->assertEquals($buyerId->getValue(), $value);
    }
}
