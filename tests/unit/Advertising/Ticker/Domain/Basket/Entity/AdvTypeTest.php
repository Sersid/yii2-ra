<?php
declare(strict_types=1);

namespace Tests\Advertising\Ticker\Domain\Basket\Entity;

use NikoM\Advertising\Ticker\Domain\Basket\Entity\AdvType;
use PHPUnit\Framework\TestCase;

class AdvTypeTest extends TestCase
{
    public function testCreateDefault()
    {
        $advType = AdvType::default();
        $this->assertInstanceOf(AdvType::class, $advType);
        $this->assertTrue($advType->isInfo());
        $this->assertFalse($advType->isAdv());
    }

    public function testCreateInvalid()
    {
        $this->expectException(\InvalidArgumentException::class);
        new AdvType(3);
    }
}
