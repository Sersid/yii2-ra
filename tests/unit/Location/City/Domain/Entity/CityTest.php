<?php
declare(strict_types=1);

namespace Tests\Location\City\Domain\Entity;

use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\City\Domain\Entity\CityName;
use NikoM\Location\City\Domain\Entity\CityRegion;
use NikoM\Location\City\Domain\Entity\CitySeo;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;
use NikoM\Location\City\Domain\Entity\Seo\CityWhere;
use NikoM\Location\City\Domain\Event\CityCreatedEvent;
use PHPUnit\Framework\TestCase;

class CityTest extends TestCase
{
    public function additionProvider(): array
    {
        return [
            [
                'Сургут',
                'surgut',
                null,
                'в Сургуте',
                'Реклама в Сургуте',
                // 'Наружная реклама в Сургуте',
                // 'Бегущая строка на ТВ в Сургуте',
                // 'Реклама на радио в Сургуте',
                // 'Реклама на ТВ в Сургуте',
            ]
        ];
    }

    /**
     * @dataProvider additionProvider
     */
    public function testCreate($name, $slug, $region, $where, $pageTitle)
    {
        $this->assertSame(1, 1);
        // $name = new CityName($name);
        // $slug = new CitySlug($slug);
        // $region = new CityRegion($region);
        // $where = new CityWhere($where);
        // $seo = new CitySeo($pageTitle);
        // $city = City::create($name, $slug, $region, $where, $seo);
        //
        // $this->assertSame($city->getName(), $name);
        // $this->assertSame($city->getSlug(), $slug);
        // $this->assertSame($city->getRegion(), $region);
        // $this->assertSame($city->getWhere(), $where);
        // $this->assertSame($city->getSeo(), $seo);
        //
        // $events = $city->releaseEvents();
        // $this->assertCount(1, $events);
        // $this->assertInstanceOf(CityCreatedEvent::class, $events[0]);
    }
}
