<?php
declare(strict_types=1);

namespace Tests\Kernel\Domain\ValueObject;

use PHPUnit\Framework\TestCase;

class IntValueObjectTest extends TestCase
{
     public function testCreate()
     {
         $value = 123;
         $int = new IntValueObject($value);
         $this->assertEquals($int->getValue(), $value);
     }
}
