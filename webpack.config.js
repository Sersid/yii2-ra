const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

let config = {
  entry: './resources/app.js',
  output: {
    path: path.resolve(__dirname, 'frontend/web/js'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /.js$/,
        use: 'babel-loader'
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  }
};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.output.filename = 'bundle.dev.js';
  }

  return config;
};
