$(function() {
    $('[data-toggle="tooltip"]').tooltip();

    // Для очередности ajax
    var ajaxManager = (function() {
        var requests = [];

        return {
            addReq: function(opt) {
                requests.push(opt);
            },
            removeReq: function(opt) {
                if ($.inArray(opt, requests) > -1) {
                    requests.splice($.inArray(opt, requests), 1);
                }
            },
            run: function() {
                var self = this,
                    oriSuc;

                if (requests.length) {
                    oriSuc = requests[0].complete;

                    requests[0].complete = function() {
                        if (typeof (oriSuc) === 'function') {
                            oriSuc();
                        }
                        requests.shift();
                        self.run.apply(self, []);
                    };

                    $.ajax(requests[0]);
                } else {
                    self.tid = setTimeout(function() {
                        self.run.apply(self, []);
                    }, 1000);
                }
            },
            stop: function() {
                requests = [];
                clearTimeout(this.tid);
            }
        };
    }());
    ajaxManager.run();
    // ~ Для очередности ajax

    let box = $('.js-multiUploadPhotos');
    let modal = $('.js-bigModal');
    let modalTitle = $('.js-modalTitle');
    let modalBody = $('.js-modalBody');
    let smallModal = $('.js-smallModal');
    let modalSmallTitle = $('.js-modalSmallTitle');
    let modalSmallBody = $('.js-modalSmallBody');
    let showDirLinks = $('.js-showDir');

    /**
     * Добавление, редактирование и удаление папки
     * @param button
     * @param url
     */
    function getSmallModal(button, url) {
        button.click(function(e) {
            e.preventDefault();
            let self = $(this);
            smallModal.modal('show');
            modalSmallTitle.html(self.attr('title'));
            modalSmallBody.html('<p class="text-center">Загрузка...</p>');
            $.ajax({
                url: url,
                data: {id: self.data('id')},
                success: function(html) {
                    modalSmallBody.html(html);
                }
            });
        });
    }
    // Нажатие "Создать папку"
    getSmallModal($('.js-createFolder'), box.data('url-create-folder'));
    getSmallModal($('.js-updateFolder'), box.data('url-update-folder'));
    getSmallModal($('.js-deleteFolder'), box.data('url-delete-folder'));

    // Отправка формы внутри мелкого модального окна
    modalSmallBody.on('submit', 'form', function(e) {
        e.preventDefault();
        let data = $(this).serialize();
        $(this).find('button[type="submit"]').attr('disabled', 'disabled').html('Пожалуйста, подождите');
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: data,
            success: function(html) {
                modalSmallBody.html(html);
            }
        });
    });

    // Нажатие "Загрузить из всех папок"
    box.on('click', '.js-runUploadAll', function(e) {
        e.preventDefault();
        let self = $(this);
        let buttonsBox = self.closest('.js-btnMainGroup');
        let urlFolder = box.data('url-folder');
        let urlFiles = box.data('url-files');

        // блокируем кнопки
        showDirLinks.removeClass('pseudo');
        buttonsBox.find('button').attr('disabled', 'disabled');
        self.find('i').removeClass('fa-download').addClass('fa-spinner fa-spin');

        $('.js-row').addClass('text-muted js-nowLoading').each(function() {
            let row = $(this);
            let statusBox = row.find('.js-status');
            let statistic = row.find('.js-statistic');
            let folderButtons = row.find('.js-folderBtnGroup').find('button');
            let skippedBox = row.find('.js-skipped');
            let errorsBox = row.find('.js-errors');
            let successBox = row.find('.js-success');
            let total = row.find('.js-total');

            function setStatistic(data) {
                if ('statistic' in data) {
                    if ('skipped' in data.statistic) {
                        skippedBox.html(data.statistic.skipped);
                    }
                    if ('errors' in data.statistic) {
                        errorsBox.html(data.statistic.errors);
                    }
                    if ('success' in data.statistic) {
                        successBox.html(data.statistic.success);
                    }
                    if ('total' in data.statistic) {
                        total.html(data.statistic.total);
                    }
                    statistic.removeClass('hidden');
                }
            }

            function setStatus(data) {
                row.removeClass('text-muted js-nowLoading');
                if ('statusLabel' in data) {
                    statusBox.html(data.statusLabel).find('[data-toggle="tooltip"]').tooltip();
                }
                if ($('.js-nowLoading').length === 0) {
                    buttonsBox.find('button').removeAttr('disabled');
                    self.find('i').removeClass('fa-spinner fa-spin').addClass('fa-download');
                    showDirLinks.addClass('pseudo');
                    folderButtons.removeAttr('disabled');
                }
            }

            statusBox.html('<i class="fas fa-spinner fa-spin"></i>');
            folderButtons.attr('disabled', 'disabled');
            ajaxManager.addReq({
                url: urlFolder,
                data: {id: row.data('id')},
                dataType: 'json',
                complete: function() {
                },
                success: function(data) {
                    if (typeof data == 'object') {
                        if ('error' in data && data.error === true) {
                            setStatus(data);
                        } else {
                            setStatistic(data);
                            if ('files' in data) {
                                let cnt = data.files.length;
                                if (data.files.length > 0) {
                                    $.each(data.files, function(key, ids) {
                                        key = key + 1;
                                        ajaxManager.addReq({
                                            url: urlFiles,
                                            data: {ids: ids},
                                            dataType: 'json',
                                            success: function(folderData) {
                                                if ('statistic' in data) {
                                                    if ('errors' in data.statistic && 'errors' in folderData) {
                                                        data.statistic.errors += folderData.errors;
                                                    }
                                                    if ('success' in data.statistic && 'success' in folderData) {
                                                        data.statistic.success += folderData.success;
                                                    }
                                                }
                                                setStatistic(data);
                                                if (cnt === key) {
                                                    setStatus(data);
                                                }
                                            }
                                        });
                                    });
                                } else {
                                    setStatus(data);
                                }
                            }
                        }
                    }
                },
                error: function() {
                    alert('При загрузке произошла ошибка');
                }
            });
        });
    });
    // ~ Нажатие "Загрузить во всех папках"

    // Нажатие ссылку с названием папки
    showDirLinks.on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('pseudo')) {
            modalTitle.html('');
            let id = $(this).closest('.js-row').data('id');
            modalBody.html('<p class="text-center">Загрузка...</p>');
            modal.modal('show');
            $.ajax({
                url: box.data('url-view-folder'),
                data: {id: id},
                dataType: 'json',
                success: function(data) {
                    modalTitle.html(data.title);
                    modalBody.html(data.body).find('[data-toggle="tooltip"]').tooltip();
                },
                error: function() {
                    alert('При загрузке произошла ошибка');
                }
            });
        }
    });
    // ~ Нажатие ссылку с названием папки

    // Нажатие на кнопку "Обновить список файлов"
    $(document).on('click', '.js-reload', function(e) {
        e.preventDefault();
        let buttonIcon = 'fa-redo-alt';
        let self = $(this);
        let icon = self.find('i');
        let buttonsBox = self.closest('.js-buttonGroup');
        buttonsBox.find('button').attr('disabled', 'disabled');
        icon.removeClass(buttonIcon).addClass('fa-spinner fa-spin');
        ajaxManager.addReq({
            url: box.data('url-reload-folder'),
            data: {id: self.data('id')},
            complete: function() {
                buttonsBox.find('button').removeAttr('disabled');
                icon.removeClass('fa-spinner fa-spin').addClass(buttonIcon).removeAttr('disabled');
            },
            dataType: 'json',
            success: function(data) {
                modalBody.html(data.body).find('[data-toggle="tooltip"]').tooltip();
            },
            error: function() {
                alert('При загрузке произошла ошибка');
            }
        });
    });
    // ~ Нажатие на кнопку "Синхронизировать список файлов"

    // Нажатие на кнопку "Загрузить файлы"
    modalBody.on('click', '.js-downloadFiles', function(e) {
        e.preventDefault();
        let self = $(this);
        self.closest('.js-buttonGroup').find('button').attr('disabled', 'disabled');
        self.find('i').removeClass('fa-download').addClass('fa-spinner fa-spin');
        if (modalBody.find('.js-downloadFile').length > 0) {
            modalBody.find('.js-downloadFile').trigger('click');
        } else {
            self.closest('.js-buttonGroup').find('button').removeAttr('disabled');
            self.find('i').removeClass('fa-spinner fa-spin').addClass('fa-download');
        }
    });
    // ~ Нажатие на кнопку "Загрузить файлы"

    // Нажатие на кнопку "Скачать файл"
    $(document).on('click', '.js-downloadFile', function(e) {
        e.preventDefault();
        let self = $(this);
        let buttonsBox = self.closest('.js-buttonGroup');
        let fileRow = self.closest('.js-fileRow');
        buttonsBox.find('button').attr('disabled', 'disabled');
        self.attr('disabled', 'disabled').find('i').removeClass('fa-download').addClass('fa-spinner fa-spin');
        ajaxManager.addReq({
            url: self.data('url'),
            complete: function() {
                buttonsBox.find('button').removeAttr('disabled');
                self.find('i').removeClass('fa-spinner fa-spin').addClass('fa-download');
                self.removeAttr('disabled');
            },
            success: function(html) {
                fileRow.replaceWith(html).find('[data-toggle="tooltip"]').tooltip();
            },
            error: function() {
                alert('При загрузке произошла ошибка');
            }
        });
    });
    // ~ Нажатие на кнопку "Скачать файл"

    // Постраничная навигация
    $(document).on('click', '.js-pager a', function(e) {
        e.preventDefault();
        let self = $(this);
        $.ajax({
            url: self.attr('href'),
            dataType: 'json',
            success: function(data) {
                modalBody.html(data.body).find('[data-toggle="tooltip"]').tooltip();
            },
            error: function() {
                alert('При загрузке произошла ошибка');
            }
        });
    });
});
