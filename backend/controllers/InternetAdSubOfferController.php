<?php
declare(strict_types=1);

namespace backend\controllers;

use backend\models\InternetSubOfferSearch;
use common\models\InternetSubOffer;

class InternetAdSubOfferController extends BaseController
{
    public $model = InternetSubOffer::class;
    public $modelSearch = InternetSubOfferSearch::class;
}
