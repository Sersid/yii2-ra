<?php

namespace backend\controllers;

/**
 * Class EmployeeController
 * @package backend\controllers
 */
class EmployeeController extends BaseController
{
    public $model = 'common\models\Employee';
    public $modelSearch = 'backend\models\EmployeeSearch';
}
