<?php

namespace backend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class BaseController
 * @package backend\controllers
 */
class BaseController extends BaseAdminController
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @var Model
     */
    public $modelSearch;

    /**
     * Общий экшн для отображения списков
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex()
    {
        $searchModel = new $this->modelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    /**
     * Общий экшн для создания записи
     * @return string|Response
     * @throws InvalidArgumentException
     */
    public function actionCreate()
    {
        /* @var $model Model */
        $model = new $this->model();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = Yii::$app->request->get('back_url');
            $url = !empty($url) ? urldecode($url) : ['index'];
            return $this->redirect($url);
        } else {
            if (!Yii::$app->request->post()) {
                if ($model->hasProperty('status')) {
                    $model->status = true;
                }
                if ($model->hasProperty('sort')) {
                    $model->sort = 500;
                }
            }
            return $this->render('create', ['model' => $model]);
        }
    }

    /**
     * Общий экшн для обновления записи
     *
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = Yii::$app->request->get('back_url');
            $url = !empty($url) ? urldecode($url) : ['index'];
            return $this->redirect($url);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
     * Find model
     *
     * @param $id
     *
     * @return Model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $modelName = $this->model;
        if (($model = $modelName::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Общий экшн для удаления записи
     *
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $url = Yii::$app->request->get('back_url');
        $url = !empty($url) ? urldecode($url) : ['index'];
        return $this->redirect($url);
    }
}
