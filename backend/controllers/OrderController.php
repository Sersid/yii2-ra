<?php

namespace backend\controllers;

use common\models\Order;
use yii\web\NotFoundHttpException;

/**
 * Class OrderController
 * @package backend\controllers
 */
class OrderController extends BaseAdminController
{
    public function actionIndex($id)
    {
        return $this->render('view', ['order' => $this->findModel($id)]);
    }

    /**
     * Finds the OutdoorAd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Order::find()->where(['id' => $id])->with(
            [
                'outdoors',
                'radios',
                'tickers',
                'tvs',
            ]
        )->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
