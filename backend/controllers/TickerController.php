<?php

namespace backend\controllers;

use Yii;
use common\models\Ticker;

/**
 * Class TickerController
 * @package backend\controllers
 */
class TickerController extends BaseController
{
    public $model = 'common\models\Ticker';
    public $modelSearch = 'backend\models\TickerSearch';

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCreate()
    {
        $model = new Ticker();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            if (!Yii::$app->request->post()) {
                $model->status = true;
                $model->sort = 500;
                $model->type = Ticker::TYPE_1;
            }
            return $this->render('create', ['model' => $model]);
        }
    }
}
