<?php

namespace backend\controllers;

/**
 * Class FormSorryController
 * @package backend\controllers
 */
class FormSorryController extends BaseController
{
    public $model = 'common\models\FormSorry';
    public $modelSearch = 'backend\models\FormSorrySearch';
}
