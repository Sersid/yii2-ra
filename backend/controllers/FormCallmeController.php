<?php

namespace backend\controllers;

/**
 * Class FormCallmeController
 * @package backend\controllers
 */
class FormCallmeController extends BaseController
{
    public $model = 'common\models\FormCallMe';
    public $modelSearch = 'backend\models\FormCallMeSearch';
}
