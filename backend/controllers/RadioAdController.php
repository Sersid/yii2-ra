<?php

namespace backend\controllers;

use backend\models\GlobalDiscount;
use Yii;

/**
 * Class RadioAdController
 * @package backend\controllers
 */
class RadioAdController extends BaseController
{
    public $model = 'common\models\RadioAd';
    public $modelSearch = 'backend\models\RadioAdSearch';

    public function actionDiscount()
    {
        $model = new GlobalDiscount();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                Yii::$app->config->set('radio_discount', $model->discount);
                Yii::$app->session->setFlash('settings',true);
            }
        } else {
            $model->discount = Yii::$app->config->get('radio_discount');
        }
        return $this->render('discount', ['model' => $model]);
    }
}
