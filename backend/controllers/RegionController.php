<?php

namespace backend\controllers;

use Yii;

/**
 * Class RegionController
 * @package backend\controllers
 */
class RegionController extends BaseController
{
    public $model = 'common\models\Region';
    public $modelSearch = 'backend\models\RegionSearch';
}
