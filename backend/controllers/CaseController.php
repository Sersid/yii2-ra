<?php

namespace backend\controllers;

/**
 * Class CaseController
 * @package backend\controllers
 */
class CaseController extends BaseController
{
    public $model = 'common\models\AdvCase';
    public $modelSearch = 'backend\models\AdvCaseSearch';
}
