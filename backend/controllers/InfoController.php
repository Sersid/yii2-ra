<?php

namespace backend\controllers;

/**
 * Class InfoController
 * @package backend\controllers
 */
class InfoController extends BaseController
{
    public $model = 'common\models\Info';
    public $modelSearch = 'backend\models\InfoSearch';
}
