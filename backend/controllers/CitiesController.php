<?php

namespace backend\controllers;

use common\models\City;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class CitiesController
 * @package backend\controllers
 */
class CitiesController extends BaseController
{
    public $model = 'common\models\City';
    public $modelSearch = 'backend\models\CitySearch';

    /**
     * Создания города
     * @return string|Response
     * @throws InvalidArgumentException
     */
    public function actionCreate()
    {
        /* @var $model City */
        $model = new $this->model();
        if ($model->load(Yii::$app->request->post())) {
            $model->requisitesFile = UploadedFile::getInstance($model, 'requisitesFile');
            if ($model->save()) {
                if (!empty($model->requisitesFile)) {
                    $model->requisitesFile->saveAs($model->getRequisitesFilePath());
                }
                $url = Yii::$app->request->get('back_url');
                $url = !empty($url) ? urldecode($url) : ['index'];
                return $this->redirect($url);
            }
        } else {
            $model->status = true;
            $model->sort = 500;
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Обновление города
     *
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     */
    public function actionUpdate($id)
    {
        /* @var $model City */
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->requisitesFile = UploadedFile::getInstance($model, 'requisitesFile');
            if ($model->save()) {
                if (!empty($model->requisitesFile)) {
                    $model->requisitesFile->saveAs($model->getRequisitesFilePath());
                } else if (!empty($model->isNeedDeleteRequisitesFile)) {
                    FileHelper::unlink($model->getRequisitesFilePath());
                }
                $url = Yii::$app->request->get('back_url');
                $url = !empty($url) ? urldecode($url) : ['index'];
                return $this->redirect($url);
            }
        }
        return $this->render('update', ['model' => $model]);
    }
}
