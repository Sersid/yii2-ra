<?php

namespace backend\controllers;

/**
 * Class ReviewController
 * @package backend\controllers
 */
class ReviewController extends BaseController
{
    public $model = 'common\models\Review';
    public $modelSearch = 'backend\models\ReviewSearch';
}
