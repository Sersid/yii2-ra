<?php

namespace backend\controllers;

use kartik\tree\controllers\NodeController;
use backend\models\Book;

/**
 * Class BookController
 * @package backend\controllers
 */
class BookController extends NodeController
{
    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        $model = Book::find()->addOrderBy('root, lft');

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
