<?php

namespace backend\controllers;

/**
 * Class PageController
 * @package backend\controllers
 */
class PageController extends BaseController
{
    public $model = 'common\models\Page';
    public $modelSearch = 'backend\models\PageSearch';
}
