<?php

namespace backend\controllers;

use backend\models\settings\Requisites;
use Yii;
use yii\web\UploadedFile;

/**
 * Class RequisitesController
 * @package backend\controllers
 */
class RequisitesController extends BaseAdminController
{
    public function actionIndex()
    {
        $model = new Requisites();
        $success = false;
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $model->file->saveAs(
                    Yii::getAlias(Yii::$app->params['requisites_path'] . Yii::$app->params['default_requisites'])
                );
                $success = true;
            }
        }
        return $this->render('index', ['model' => $model, 'success' => $success]);
    }
}
