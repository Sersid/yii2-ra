<?php

namespace backend\controllers;

/**
 * Class CustomersController
 * @package backend\controllers
 */
class CustomersController extends BaseController
{
    public $model = 'common\models\Customer';
    public $modelSearch = 'backend\models\CustomerSearch';
}
