<?php

namespace backend\controllers;

/**
 * Class InfoCategoriesController
 * @package backend\controllers
 */
class InfoCategoriesController extends BaseController
{
    public $model = 'common\models\InfoCategory';
    public $modelSearch = 'backend\models\InfoCategorySearch';
}
