<?php

namespace backend\controllers;

use common\models\Street;
use Yii;
use common\models\OutdoorAd;
use yii\web\NotFoundHttpException;

/**
 * Class OutdoorAdController
 * @package backend\controllers
 */
class OutdoorAdController extends BaseController
{
    public $model = 'common\models\OutdoorAd';
    public $modelSearch = 'backend\models\OutdoorAdSearch';

    /**
     * Creates a new OutdoorAd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCreate()
    {
        $model = new OutdoorAd();

        if (Yii::$app->request->post()) {
            $model->attributes = Yii::$app->request->post('OutdoorAd');
            if (!empty($model->street_name)) {
                $model->street_id = Street::getIdByName($model->city_id, $model->street_name);
            }

            if ($model->save()) {
                $url = Yii::$app->request->get('back_url');
                $url = !empty($url) ? urldecode($url) : ['index'];
                return $this->redirect($url);
            }
        } else {
            $model->rotate = 0;
            $model->status = true;
            $model->sort = 500;
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing OutdoorAd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionUpdate($id)
    {
        /** @var OutdoorAd $model */
        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $model->attributes = Yii::$app->request->post('OutdoorAd');
            if (!empty($model->street_name)) {
                $model->street_id = Street::getIdByName($model->city_id, $model->street_name);
            }
            if ($model->save()) {
                $sides = Yii::$app->request->post('OutdoorAdSide');
                $busy = Yii::$app->request->post('Busy');
                foreach ($model->outdoorAdSides as $side) {
                    if (isset($sides[$side->id])) {
                        $side->attributes = $sides[$side->id];
                        $time = mktime(0, 0, 0, date('n') + 1, 1, date('Y'));
                        if (isset($busy[$side->id][date('Y', $time)][date('n', $time)])) {
                            $side->discount = $busy[$side->id][date('Y', $time)][date('n', $time)]['discount'];
                        }
                        $side->save();
                    }
                    foreach ($side->busy as $item) {
                        if (isset($busy[$side->id][$item->year][$item->month])) {
                            $item->attributes = $busy[$side->id][$item->year][$item->month];
                            $item->save();
                        }
                    }
                }
                $url = Yii::$app->request->get('back_url');
                $url = !empty($url) ? urldecode($url) : ['index'];
                return $this->redirect($url);
            }
        } else {
            $model->street_name = $model->street->name;
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Multi delete
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionMultiDelete()
    {
        $selection = (array)Yii::$app->request->post('selection');
        foreach ($selection as $id) {
            OutdoorAd::findOne(['id' => $id])->delete();
        }
        $url = Yii::$app->request->get('back_url');
        $url = !empty($url) ? urldecode($url) : ['index'];
        return $this->redirect($url);
    }
}
