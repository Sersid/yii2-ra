<?php
declare(strict_types=1);

namespace backend\controllers;

use backend\models\InternetMoreOfferSearch;
use common\models\InternetMoreOffer;

class InternetAdMoreOfferController extends BaseController
{
    public $model = InternetMoreOffer::class;
    public $modelSearch = InternetMoreOfferSearch::class;
}
