<?php

namespace backend\controllers;

use backend\models\OutdoorAdImport;
use common\models\City;
use common\models\OutdoorAd;
use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideBusy;
use common\models\Street;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\Exception;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class ImportOutdoorAdController
 * @package backend\controllers
 */
class ImportOutdoorAdController extends BaseAdminController
{
    /** @var int лимит импорта поверхностей за один запрос */
    const IMPORT_LIMIT = 120;

    /** @var string разделитель csv */
    const CSV_DELIMITER = ';';

    /** @var int кол-во строк, которые занимают заголовки */
    const HEADERS_ROWS = 6;

    /** @var string ключ кеша заголовков */
    protected $cacheKey = 'import-outdoor-ad-headers';

    /** @var string Путь к csv */
    protected $csvPath = '@runtime/import.csv';

    /** @var array "Карта" столбцов файла */
    protected $arMap;

    /** @var array содержимое текущей строки */
    protected $arItem;

    /** @var array занятость поверхностей по месяцам */
    protected $arBusy;

    /**
     * Форма загрузки
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex()
    {
        return $this->render('index', ['model' => new OutdoorAdImport()]);
    }

    /**
     * Загрузка
     * @return array
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function actionUpload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new OutdoorAdImport();
        if (Yii::$app->request->isPost) {
            Yii::$app->cache->delete($this->cacheKey);
            $model->file = UploadedFile::getInstance($model, 'file');
            if (empty($model->file)) {
                return ['error' => 'Файл не был загружен'];
            }
            if ($model->file->extension != 'csv') {
                return ['error' => 'Файл должен быть в формате csv'];
            }
            if ($model->validate()) {
                $model->file->saveAs(Yii::getAlias($this->csvPath));
                return $this->actionProcess();
            } else {
                return ['error' => implode(", ", $model->getFirstErrors())];
            }
        }
        return ['error' => 'File not upload'];
    }

    /**
     * Процесс импорта
     *
     * @param int $offset
     *
     * @return array
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function actionProcess($offset = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        // Проверка файла
        $this->setHeadersMap();
        if (empty($this->arMap)) {
            return ['error' => 'Ошибка чтения файла. Вы уверены, что структура не изменилась?'];
        }
        if (empty($this->arMap['fields'])) {
            return ['error' => 'Ошибка чтения файла: не определились заголовки'];
        }
        if (empty($this->arMap['dates'])) {
            return ['error' => 'Ошибка чтения файла: не определились месяцы'];
        }
        $offset = (int)$offset;
        // первый запрос (деактивируем все поверхности)
        if (empty($offset)) {
            OutdoorAdSide::updateAll(['status' => OutdoorAdSide::STATUS_DISABLED]);
            return [
                'stop' => false,
                'result' => [],
                'offset' => $offset + 1,
                'total' => count($this->getCsvData()) - static::HEADERS_ROWS,
            ];
        }
        $arSides = array_flip(OutdoorAdSide::getSides());
        $time = mktime(0, 0, 0, date('n') + 1, 1, date('Y'));
        $m = date('n', $time);
        $y = date('Y', $time);
        $limit = $offset + self::IMPORT_LIMIT;
        $arResult = [];
        while ($offset < $limit) {
            // Парсинг текущей строки
            $this->setRow($offset);
            // Строки закончились, обновляем ков-во сторон, останавливаем js
            if ($this->arItem === null) {
                $arOutdoorAds = OutdoorAd::find()
                    ->joinWith('outdoorAdSides')
                    ->all();
                /** @var OutdoorAd $arOutdoorAd */
                foreach ($arOutdoorAds as $arOutdoorAd) {
                    $arOutdoorAd->sides = count($arOutdoorAd->outdoorAdSides);
                    $arOutdoorAd->save(false);
                }
                return [
                    'stop' => true,
                    'offset' => $offset,
                    'result' => $arResult,
                ];
            }
            $offset++;
            // Строка пуста
            if (empty($this->arItem['full_number'])) {
                continue;
            }
            // Сохраняем данные
            $outdoorAd = OutdoorAd::find()
                ->where(['full_number' => $this->arItem['full_number']])
                ->one();
            if (empty($outdoorAd)) {
                $outdoorAd = new OutdoorAd();
            }
            $outdoorAd->full_number = $this->arItem['full_number'];
            $outdoorAd->sides = 1;
            $outdoorAd->number = empty($this->arItem['number']) ? null : $this->arItem['number'];
            $outdoorAd->status = OutdoorAd::STATUS_ENABLE;
            $outdoorAd->sort = $offset;
            $outdoorAd->size = empty($this->arItem['size']) ? null : $this->arItem['size'];
            $outdoorAd->format = $this->getFormat();
            $outdoorAd->type_display = $this->getTypeDisplay();
            $outdoorAd->city_id = empty($this->arItem['city']) ? null : City::getIdByName($this->arItem['city']);
            $outdoorAd->street_id = null;
            $address = null;
            if (!empty($this->arItem['address'])) {
                $arAddress = explode(',', $this->arItem['address']);
                if (!empty($arAddress)) {
                    if (!empty($outdoorAd->city_id)) {
                        $outdoorAd->street_id = Street::getIdByName($outdoorAd->city_id, $arAddress[0]);
                    }
                    unset($arAddress[0]);
                    $address = empty($arAddress) ? null : trim(implode(',', $arAddress));
                }
            }
            if (!$outdoorAd->save()) {
                $arResult[] = [
                    'error' => true,
                    'message' => implode(", ", $outdoorAd->getFirstErrors()),
                    'offset' => $offset,
                    'number' => $this->arItem['full_number'],
                    'side' => $this->arItem['side'],
                ];
                continue;
            }
            $side = isset($arSides[$this->arItem['side']]) ? $arSides[$this->arItem['side']] : $this->arItem['side'];
            $outdoorAdSide = OutdoorAdSide::find()
                ->where(['parent_id' => $outdoorAd->id, 'side' => $side])
                ->one();
            if (empty($outdoorAdSide)) {
                $outdoorAdSide = new OutdoorAdSide;
            }
            $outdoorAdSide->parent_id = $outdoorAd->id;
            $outdoorAdSide->side = $side;
            $outdoorAdSide->price = $this->getClearPrice($this->arItem['price']);
            $outdoorAdSide->price_installation = $this->getClearPrice($this->arItem['price_installation']);
            $outdoorAdSide->price_print = $this->getClearPrice($this->arItem['price_print']);
            $outdoorAdSide->rating = $this->getRating();
            $outdoorAdSide->status = OutdoorAdSide::STATUS_ENABLE;
            $outdoorAdSide->busy_status = OutdoorAdSide::BUSY_STATUS_FREE;
            $outdoorAdSide->discount_monthly =
                empty($this->arItem['discount_monthly']) ? null : $this->arItem['discount_monthly'];
            $outdoorAdSide->discount = null;
            $outdoorAdSide->address = $address;
            foreach ($this->arBusy as $arBusy) {
                if (empty($arBusy['month']) || empty($arBusy['year']) || $arBusy['month'] != $m
                    || $arBusy['year'] != $y) {
                    continue;
                }
                if ($arBusy['status'] == 'З' || $arBusy['status'] == 'з') {
                    $outdoorAdSide->busy_status = OutdoorAdSide::BUSY_STATUS_BUSY;
                }
                $outdoorAdSide->discount = OutdoorAdSideBusy::getDiscount($arBusy['status']);
            }
            if (!$outdoorAdSide->save()) {
                $arResult[] = [
                    'error' => true,
                    'message' => implode(", ", $outdoorAdSide->getFirstErrors()),
                    'offset' => $offset,
                    'number' => $this->arItem['full_number'],
                    'side' => $this->arItem['side'],
                ];
                continue;
            }
            // Save busy
            OutdoorAdSideBusy::deleteAll(['side_id' => $outdoorAdSide->id]);
            $insert = [];
            foreach ($this->arBusy as $arBusy) {
                if (empty($arBusy['month']) || empty($arBusy['year'])) {
                    continue;
                }
                $insert[] = [
                    $outdoorAdSide->id,
                    $arBusy['month'],
                    $arBusy['year'],
                    OutdoorAdSideBusy::getStatusByLetter($arBusy['status']),
                    OutdoorAdSideBusy::getDiscount($arBusy['status']),
                ];
            }
            if (!empty($insert)) {
                Yii::$app->db->createCommand()
                    ->batchInsert(
                        OutdoorAdSideBusy::tableName(),
                        ['side_id', 'month', 'year', 'status', 'discount'],
                        $insert
                    )
                    ->execute();
            }
            $arResult[] = [
                'number' => $this->arItem['full_number'],
                'side' => $this->arItem['side'],
                'error' => false,
                'message' => Yii::t('app', 'Success'),
            ];
        }
        return [
            'stop' => false,
            'offset' => $offset,
            'result' => $arResult,
        ];
    }

    /**
     * Получает карту файла по названям заголовков и датам
     * @return array
     * @throws InvalidArgumentException
     */
    protected function setHeadersMap()
    {
        Yii::$app->cache->delete($this->cacheKey);
        $this->arMap = Yii::$app->cache->get($this->cacheKey);
        if ($this->arMap === false) {
            $this->arMap = [];
            $arData = $this->getCsvData();
            if (empty($arData)) {
                return $this->arMap;
            }
            $arHeaders = [
                'Город' => 'city',
                'Адрес' => 'address',
                'Размер' => 'size',
                'Тип' => 'type',
                '№' => 'number',
                'Сторона' => 'side',
                'Оценка места' => 'rating',
                'Цена' => 'price',
                'монтаж (первичный)' => 'price_installation',
                'Печать' => 'price_print',
                'Полный' => 'full_number',
                'Скидка' => 'discount_monthly',
                'Тип отображения' => 'type_display',
            ];
            $arMonths = array_flip(OutdoorAdSideBusy::getMonths());
            for ($row = 0; $row <= self::HEADERS_ROWS; $row++) {
                $arCsv = str_getcsv($arData[$row], static::CSV_DELIMITER);
                foreach ($arCsv as $col => $name) {
                    $name = trim($name);
                    if (empty($name)) {
                        continue;
                    }
                    if (isset($arHeaders[$name])) {
                        $this->arMap['fields'][$col] = $arHeaders[$name];
                    } else {
                        $arDate = explode(' ', $name);
                        if (count($arDate) == 2) {
                            if (isset($arMonths[$arDate[0]])) {
                                $this->arMap['dates'][$col] = [
                                    'month' => $arMonths[$arDate[0]],
                                    'year' => (int)$arDate[1],
                                ];
                            }
                        }
                    }
                }
            }
            Yii::$app->cache->set($this->cacheKey, $this->arMap);
        }
        return $this->arMap;
    }

    /**
     * Данные из csv
     * @return array
     * @throws InvalidArgumentException
     */
    protected function getCsvData()
    {
        static $arData;
        if (is_null($arData)) {
            $content = file_get_contents(Yii::getAlias($this->csvPath));
            $content = iconv('Windows-1251', 'UTF-8', $content);
            $arData = explode("\n", trim($content));
        }
        return $arData;
    }

    /**
     * @param int $offset
     *
     * @throws InvalidArgumentException
     */
    protected function setRow($offset = 0)
    {
        $offset = (int)$offset + static::HEADERS_ROWS;
        $arData = $this->getCsvData();
        if (!array_key_exists($offset, $arData)) {
            $this->arItem = null;
        } else {
            $this->arItem = [];
            $this->arBusy = [];
            $arRow = str_getcsv($arData[$offset], static::CSV_DELIMITER);
            foreach ($arRow as $col => $value) {
                $value = trim($value);
                if (isset($this->arMap['fields'][$col])) {
                    $this->arItem[$this->arMap['fields'][$col]] = $value;
                } else if (isset($this->arMap['dates'][$col])) {
                    $this->arBusy[] = array_merge($this->arMap['dates'][$col], ['status' => $value]);
                }
            }
            foreach ($this->arMap['fields'] as $field) {
                if (!array_key_exists($field, $this->arItem)) {
                    $this->arItem[$field] = null;
                }
            }
        }
    }

    /**
     * Тип поверхности
     * @return int
     */
    protected function getFormat()
    {
        if (empty($this->arItem['type'])) {
            return null;
        }
        $this->arItem['type'] = trim($this->arItem['type']);
        $format = $this->arItem['type'];
        switch ($this->arItem['type']) {
            case 'Billboard':
            case 'Рекламный щит':
                $format = OutdoorAd::FORMAT_BILLBOARDS;
                break;
            case 'Brandmauer':
            case 'Брандмауэр':
                $format = OutdoorAd::FORMAT_FIREWALLS;
                break;
            case 'Рекламное ограждение':
                $format = OutdoorAd::FORMAT_ADV_FENCES;
                break;
            case 'Сити-формат':
                $format = OutdoorAd::FORMAT_CITY_FORMAT;
                break;
            case 'Пилон':
                $format = OutdoorAd::FORMAT_PYLON;
                break;
            case 'Светодиодный экран':
                $format = OutdoorAd::FORMAT_LED_DISPLAYS;
                break;
            case 'Проекционная реклама':
                $format = OutdoorAd::FORMAT_PROJECTION_AD;
                break;
            case 'Ситиборд':
                $format = OutdoorAd::FORMAT_CITY_BOARD;
                break;
            case 'Остановочный павильон':
                $format = OutdoorAd::FORMAT_STOPPING_PAVILION;
                break;
            case 'Пиллар':
                $format = OutdoorAd::FORMAT_PILLAR;
                break;
            case 'Суперсайт':
                $format = OutdoorAd::FORMAT_SUPER_SITE;
              break;
            case 'Панель-кронштейн':
                $format = OutdoorAd::FORMAT_BRACKET_PANEL;
                break;
        }
        return $format;
    }

    /**
     * Тип отображения
     * @return int
     */
    protected function getTypeDisplay()
    {
        if (empty($this->arItem['type_display']) || $this->arItem['type_display'] == '-') {
            return null;
        }
        $type = $this->arItem['type_display'];
        switch ($this->arItem['type_display']) {
            case 'статика':
                $type = OutdoorAd::TYPE_DISPLAY_STATICS;
                break;
            case 'призма':
                $type = OutdoorAd::TYPE_DISPLAY_PRISM;
                break;
            case 'скроллер':
                $type = OutdoorAd::TYPE_DISPLAY_SCROLLER;
                break;
        }
        return $type;
    }

    /**
     * Цена без пробелов и прочих символов
     *
     * @param $price
     *
     * @return string
     */
    protected function getClearPrice($price)
    {
        return floatval(preg_replace('/[^0-9]/', '', $price));
    }

    /**
     * Рейтинг
     * @return int|mixed|null
     */
    protected function getRating()
    {
        if (empty($this->arItem['rating'])) {
            return null;
        }
        switch ($this->arItem['rating']) {
            case '-':
                return 0;
                break;
            case '+':
                return 1;
                break;
            case '++':
                return 2;
                break;
            case '+++':
                return 3;
                break;
            default:
                return $this->arItem['rating'];
                break;
        }
    }
}
