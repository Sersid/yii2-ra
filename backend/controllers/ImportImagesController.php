<?php

namespace backend\controllers;

use backend\models\OutdoorAdImages;
use backend\widgets\ListView;
use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideImage;
use common\models\YandexDiskFolder;
use Exception;
use sersid\config\components\Config;
use Throwable;
use Yandex\Disk\DiskClient;
use Yandex\OAuth\OAuthClient;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\image\drivers\Image;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ImportImagesController
 * @package backend\controllerss
 */
class ImportImagesController extends BaseAdminController
{
    /** @var string путь к картинкам */
    const PATH_WEB_PHOTO = '@frontend/web/photos/';
    /** @var string путь к водяному знаку */
    const PATH_WATERMARK = '@frontend/web/img/watermark.png';
    /** @var int количество загружаемых файлов */
    const MULTI_UPLOAD_SIZE = 15;
    /** @var array Размеры загружаемых изображений */
    const SIZES = [
        'sm' => '555x270',
        'lg' => '1000',
    ];
    /** @var string Ключ токена в конфигах */
    const CONFIG_TOKEN_KEY = 'yandex.token';

    /**
     * @return string
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $this->getToken();
        // Существующие папки
        $folders = $this->getFolders();
        return $this->render('index', ['folders' => $folders]);
    }

    /**
     * Token яндекс.диска
     * @return string
     * @throws InvalidConfigException
     */
    private function getToken()
    {
        static $token;
        if (YII_ENV == 'dev') {
            $token = '';
        }
        if (!is_null($token)) {
            return $token;
        }
        $token = '';
        $oauth = new OAuthClient;
        $oauth->setClientId($this->getConfigComponent()->get('yandex.clientId'));
        $oauth->setClientSecret($this->getConfigComponent()->get('yandex.clientSecret'));
        $code = Yii::$app->request->get('code');
        if (empty($code)) {
            $token = $this->getConfigComponent()->get(self::CONFIG_TOKEN_KEY);
            if (empty($token)) {
                $this->redirect($oauth->getAuthUrl());
            }
        } else {
            try {
                $oauth->requestAccessToken($code);
                $token = $oauth->getAccessToken();
                $this->getConfigComponent()->set(self::CONFIG_TOKEN_KEY, $token);
            } catch (Throwable $e) {
                $this->getConfigComponent()->delete(self::CONFIG_TOKEN_KEY);
            }
            $this->redirect('index');
        }
        return $token;
    }

    /**
     * @return object|Config
     * @throws InvalidConfigException
     */
    private function getConfigComponent()
    {
        return Yii::$app->get('config');
    }

    /**
     * Существующие папки
     * @return array
     */
    private function getFolders()
    {
        $arFolders = [];
        /** @var YandexDiskFolder $item */
        foreach (YandexDiskFolder::find()->orderBy(['season' => SORT_ASC, 'updated_at' => SORT_DESC])->all() as $item) {
            $arFolders[$item->season][$item->name] = $item;
        }
        return $arFolders;
    }

    /**
     * Создание папки
     * @return string
     * @throws Throwable
     */
    public function actionCreateFolder()
    {
        $model = new YandexDiskFolder();
        if (Yii::$app->request->isPost) {
            $model->status = YandexDiskFolder::STATUS_NEW;
            $isSuccess = $model->load(Yii::$app->request->post()) && $model->save();
            $this->syncDir($model->id);
        } else {
            $isSuccess = false;
            $model->season = YandexDiskFolder::SEASON_SUMMER;
        }
        return $this->renderAjax('form-folder', ['model' => $model, 'isSuccess' => $isSuccess]);
    }

    /**
     * Синхронизация папки
     *
     * @param integer $id
     *
     * @return bool
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    private function syncDir($id)
    {
        if (YII_ENV == 'dev') {
            return false;
        }
        $folderModel = $this->getFolderModel($id);

        // Если прежде папка была недоступна - даем еще один шанс
        if ($folderModel->status != YandexDiskFolder::STATUS_OK) {
            $folderModel->status = YandexDiskFolder::STATUS_OK;
            $folderModel->save(false);
        }

        // Получение содержимого папки
        try {
            $arContents = $this->getDiskClient()->directoryContents($folderModel->href);
        } catch (Throwable $e) {
            // если произошла ошибка - обновляем статус
            $arContents = [];
            $folderModel->status = YandexDiskFolder::STATUS_ERROR;
            $folderModel->save(false);
        }

        // Список ранее известных файлов
        $arFiles = [];
        foreach ($folderModel->files as $fileModel) {
            $arFiles[$fileModel->file_name] = $fileModel;
        }

        // Проход по всему содержимому папки
        foreach ($arContents as $arContent) {
            // Директории не нужны
            if (empty($arContent['contentType'])) {
                continue;
            }

            // Было ли ранее известно ли о файле?
            if (isset($arFiles[$arContent['displayName']])) {
                $fileModel = $arFiles[$arContent['displayName']];
            } else {
                $fileModel = new OutdoorAdSideImage();
                $fileModel->status = OutdoorAdSideImage::STATUS_NEW;
                $fileModel->folder_id = $folderModel->id;
                $fileModel->file_name = $arContent['displayName'];
            }

            $fileModel->content_type = $arContent['contentType'];
            $fileModel->href = $arContent['href'];
            $fileModel->last_modified = strtotime($arContent['lastModified']);

            try {
                // Только изображения
                if (!in_array($fileModel->content_type, ['image/jpeg', 'image/png', 'image/webp'])) {
                    throw new Exception('Не фотография', OutdoorAdSideImage::STATUS_SKIP_NOT_IMAGE);
                }

                // Поиск номера в названии
                preg_match(OutdoorAdSideImage::PATTERN_NAME, $fileModel->file_name, $arMatches);
                if (empty($arMatches)) {
                    throw new Exception('Не корректное название', OutdoorAdSideImage::STATUS_ERROR_INCORRECT_NAME);
                }

                // Поиск стороны, по умолчанию А
                $letter = "А";
                $arName = explode('_', $fileModel->file_name);
                if (!empty($arName[1])) {
                    $letter = mb_substr(trim($arName[1]), 0, 1);
                }
                $arSides = array_merge(
                    array_flip(OutdoorAdSide::getSides()),
                    array_flip(OutdoorAdSide::getLatSides())
                );
                $side = isset($arSides[$letter]) ? $arSides[$letter] : OutdoorAdSide::SIDE_A;

                // Поиск id стороны
                $sideModel = OutdoorAdSide::find()->select('outdoor_ad_side.id')->joinWith('parent')->where(
                    [
                        'outdoor_ad.full_number' => $arMatches[1],
                        'side' => $side,
                    ]
                )->one();
                if (empty($sideModel)) {
                    throw new Exception('Сторона не найдена', OutdoorAdSideImage::STATUS_WARNING_SIDE_NOT_FOUND);
                }
                $fileModel->side_id = $sideModel->id;

                // Если изображение обновили или "воскресили"
                if ((!empty($fileModel->last_upload) && $fileModel->last_upload != $fileModel->last_modified) || $fileModel->isSkipped()) {
                    $fileModel->status = OutdoorAdSideImage::STATUS_UPDATED;
                    $fileModel->last_upload = null;
                }
            } catch (Exception $e) {
                $fileModel->status = $e->getCode();
                $fileModel->side_id = null;
            }

            $fileModel->save();
            unset($arFiles[$fileModel->file_name]);
        }

        // Проход по оставшимся файлам
        foreach ($arFiles as $fileModel) {
            if ($fileModel->status == OutdoorAdSideImage::STATUS_DELETED) {
                // Удаляем файлы, которые уже были забыты
                $fileModel->delete();
            } else {
                // "Забываем" файлы, которые числятся в бд
                $fileModel->status = OutdoorAdSideImage::STATUS_DELETED;
                $fileModel->save();
            }
        }
        return true;
    }

    /**
     * Поиск директории по id
     *
     * @param integer $id id директории
     * @param bool    $withFiles
     *
     * @return YandexDiskFolder
     * @throws NotFoundHttpException
     */
    private function getFolderModel($id, $withFiles = true)
    {
        /** @var YandexDiskFolder $model */
        $query = YandexDiskFolder::find();
        $query->where(['id' => $id]);
        if ($withFiles === true) {
            $query->with(
                [
                    'files' => function (ActiveQuery $query) {
                        $query->orderBy(['status' => SORT_ASC, 'file_name' => SORT_ASC]);
                    },
                ]
            );
        }
        $model = $query->one();
        if (empty($model)) {
            throw new NotFoundHttpException('Директория не найдена');
        }
        return $model;
    }

    /**
     * @return DiskClient
     * @throws InvalidConfigException
     */
    private function getDiskClient()
    {
        $diskClient = new DiskClient($this->getToken());
        $diskClient->setServiceScheme(DiskClient::HTTPS_SCHEME);
        return $diskClient;
    }

    /**
     * Обновление папки
     *
     * @param integer $id id папки
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionUpdateFolder($id)
    {
        $model = $this->getFolderModel($id, false);
        if (Yii::$app->request->isPost) {
            $model->status = YandexDiskFolder::STATUS_UPDATED;
            $isSuccess = $model->load(Yii::$app->request->post()) && $model->save();
            $this->syncDir($model->id);
        } else {
            $isSuccess = false;
        }
        return $this->renderAjax('form-folder', ['model' => $model, 'isSuccess' => $isSuccess]);
    }

    /**
     * Синхронизация списка файлов с яндекс.диском
     *
     * @param $id
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionReloadFolder($id)
    {
        $this->syncDir($id);
        return $this->actionViewFolder($id);
    }

    /**
     * @param $id
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionViewFolder($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->getFolderModel($id, false);
        $searchModel = new OutdoorAdImages();
        $dataProvider = $searchModel->search($model->id, Yii::$app->request->queryParams);
        return [
            'title' => $model->getNameHtml(),
            'body' => ListView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'layout' => $this->renderAjax('view-folder', ['model' => $model]),
                    'itemView' => '/import-images/file',
                    'pager' => ['options' => ['class' => 'pagination pagination-sm js-pager']],
                    'options' => ['class' => 'js-filesBox'],
                    'id' => 'files-list',
                    'emptyText' => 'Файлы не найдены. Попробуйте <a href="#" data-id="' . $model->id
                                   . '" class="alert-link js-reload">обновить список файлов</a>',
                    'emptyTextOptions' => ['class' => 'alert alert-warning'],
                ]
            ),
        ];
    }

    /**
     * Синхронизация папки
     *
     * @param integer $id
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionFolderInfo($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->syncDir($id);
        $folderModel = $this->getFolderModel($id);
        $arResult = [
            'id' => $folderModel->id,
            'error' => $folderModel->status == $folderModel::STATUS_ERROR,
            'statusLabel' => $folderModel->getStatusLabel(),
            'statistic' => [
                'skipped' => 0,
                'errors' => 0,
                'success' => 0,
                'total' => 0,
            ],
            'files' => [],
        ];

        $arFilesId = [];
        foreach ($folderModel->files as $file) {
            if ($file->isCanDownload()) {
                $arFilesId[] = $file->id;
            }
            if ($file->isSkipped()) {
                $arResult['statistic']['skipped']++;
            }
            $arResult['statistic']['total']++;
        }

        $arResult['files'] = array_chunk($arFilesId, self::MULTI_UPLOAD_SIZE);
        return $arResult;
    }

    /**
     * Множественная загрузка файлов
     * @return array
     * @throws InvalidConfigException
     */
    public function actionDownloadFiles()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ids = Yii::$app->request->get('ids');
        $models = $this->getFilesModels($ids);
        $arResult = [
            'errors' => 0,
            'success' => 0,
        ];
        foreach ($models as $fileModel) {
            $fileModel = $this->downloadFile($fileModel);
            if ($fileModel->isSuccess()) {
                $arResult['success']++;
            } else if ($fileModel->isError()) {
                $arResult['errors']++;
            }
        }
        return $arResult;
    }

    /**
     * @param $ids
     *
     * @return array|ActiveRecord[]
     */
    private function getFilesModels($ids)
    {
        static $models;
        if (is_null($models)) {
            $models = $this->getFilesQuery($ids)->limit(self::MULTI_UPLOAD_SIZE)->all();
        }
        return $models;
    }

    /**
     * @param integer|array $id
     *
     * @return ActiveQuery
     */
    private function getFilesQuery($id)
    {
        $query = OutdoorAdSideImage::find();
        $query->innerJoinWith(['folder', 'side', 'side.parent']);
        $query->where(
            [
                'outdoor_ad_side_image.id' => $id,
                'yandex_disk_folder.status' => YandexDiskFolder::STATUS_OK,
                'outdoor_ad_side_image.status' => OutdoorAdSideImage::getCanDownloadStatuses(),
            ]
        );
        return $query;
    }

    /**
     * Загружает файл
     *
     * @param OutdoorAdSideImage $fileModel
     *
     * @return OutdoorAdSideImage|string
     * @throws InvalidConfigException
     */
    private function downloadFile(OutdoorAdSideImage $fileModel)
    {
        if (!$fileModel->isCanDownload()) {
            return $fileModel;
        }
        if ($fileModel->last_modified == $fileModel->last_upload) {
            return $fileModel;
        }
        $watermark = $this->getImageComponent()->load($this->getPathWatermark());
        $hasError = false;
        foreach (self::SIZES as $sizeType => $size) {
            try {
                $path = $this->getWebPath() . $fileModel->getFileName($sizeType);
                $file = $this->getDiskClient()->getImagePreview($fileModel->href, $size);
                $fp = fopen($path, "w");
                fwrite($fp, $file['body']);
                fclose($fp);
                // Водяные знаки для больших фоток
                if ($sizeType == 'lg') {
                    /** @var Image $image */
                    $image = $this->getImageComponent()->load($path);
                    $image->watermark($watermark);
                    $image->save();
                }
            } catch (Throwable $e) {
                $hasError = true;
                break;
            }
        }
        if ($hasError) {
            $fileModel->status = OutdoorAdSideImage::STATUS_ERROR_DOWNLOAD;
        } else {
            $fileModel->status = OutdoorAdSideImage::STATUS_LOADED;
            $fileModel->last_upload = $fileModel->last_modified;
        }
        $fileModel->save();
        return $fileModel;
    }

    /**
     * @return object|Image
     * @throws InvalidConfigException
     */
    private function getImageComponent()
    {
        return Yii::$app->get('image');
    }

    /**
     * @return bool|string
     */
    private function getPathWatermark()
    {
        return Yii::getAlias(self::PATH_WATERMARK);
    }

    /**
     * @return bool|string
     * @throws InvalidArgumentException
     */
    private function getWebPath()
    {
        return Yii::getAlias(self::PATH_WEB_PHOTO);
    }

    /**
     * Загрузка картинки
     *
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionDownloadFile($id)
    {
        $fileModel = $this->downloadFile($this->getFileModel($id));
        return $this->renderAjax('file', ['model' => $fileModel]);
    }

    /**
     * Поиск файла по id
     *
     * @param integer $id id файла
     *
     * @return OutdoorAdSideImage
     * @throws NotFoundHttpException
     */
    private function getFileModel($id)
    {
        /** @var OutdoorAdSideImage $model */
        static $model;
        if (is_null($model)) {
            $model = $this->getFilesQuery($id)->one();
            if (empty($model)) {
                throw new NotFoundHttpException('Файл не найден');
            }
        }
        return $model;
    }

    /**
     * Удаляет токен диска
     * @throws InvalidConfigException
     */
    public function actionDeleteToken()
    {
        $this->getConfigComponent()->delete(self::CONFIG_TOKEN_KEY);
        $this->redirect(['index']);
    }

    /**
     * Удаление папки
     *
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteFolder($id)
    {
        $model = $this->getFolderModel($id, false);
        if (Yii::$app->request->isPost && !empty(Yii::$app->request->post('del'))) {
            $model->delete();
            $isSuccess = true;
        } else {
            $isSuccess = false;
        }
        return $this->renderAjax('delete-folder', ['model' => $model, 'isSuccess' => $isSuccess]);
    }
}
