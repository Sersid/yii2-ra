<?php

namespace backend\controllers;

/**
 * Class FaqController
 * @package backend\controllers
 */
class FaqController extends BaseController
{
    public $model = 'common\models\Faq';
    public $modelSearch = 'backend\models\FaqSearch';
}

