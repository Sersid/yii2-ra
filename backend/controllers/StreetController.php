<?php

namespace backend\controllers;

/**
 * Class StreetController
 * @package backend\controllers
 */
class StreetController extends BaseController
{
    public $model = 'common\models\Street';
    public $modelSearch = 'backend\models\StreetSearch';
}
