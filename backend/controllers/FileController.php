<?php

namespace backend\controllers;

/**
 * Class FileController
 * @package backend\controllers
 */
class FileController extends \common\controllers\FileController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'upload-image' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/images/',
                'path' => '@frontend/web/images',
                'validatorOptions' => [
                    'maxWidth' => 1000,
                    'maxHeight' => 1000,
                ],
            ],
        ];
    }

    function actionImages($img, $ext)
    {
        $path = \Yii::getAlias('@frontend/web/images/' . $img . '.' . $ext);
        header("Content-Type: " . \yii\helpers\FileHelper::getMimeType($path));
        readfile($path);
    }
}
