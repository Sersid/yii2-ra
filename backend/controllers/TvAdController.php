<?php
declare(strict_types=1);

namespace backend\controllers;

use backend\models\GlobalDiscount;
use common\models\TvAd;
use common\models\TvAdPotentialAudience;
use Yii;

/**
 * Class TvAdController
 * @package backend\controllers
 */
class TvAdController extends BaseController
{
    public $model = 'common\models\TvAd';
    public $modelSearch = 'backend\models\TvAdSearch';

    public function actionDiscount()
    {
        $model = new GlobalDiscount();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                Yii::$app->config->set('tv_discount', $model->discount);
                Yii::$app->session->setFlash('settings',true);
            }
        } else {
            $model->discount = Yii::$app->config->get('tv_discount');
        }
        return $this->render('discount', ['model' => $model]);
    }

    public function actionCreate()
    {
        $model = new TvAd();
        $potentialAudience = new TvAdPotentialAudience();
        if ($model->load(Yii::$app->request->post()) && $potentialAudience->load(Yii::$app->request->post())) {
            $model->status = true;
            $model->sort = 500;
            if ($model->save()) {
                $potentialAudience->tv_ad_id = $model->id;
                if ($potentialAudience->save()) {
                    $url = Yii::$app->request->get('back_url');
                    $url = !empty($url) ? urldecode($url) : ['index'];

                    return $this->redirect($url);
                }
            }
        }

        return $this->render('create', ['model' => $model, 'potentialAudience' => $potentialAudience]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $potentialAudience = TvAdPotentialAudience::findOne(['tv_ad_id' => $id]);
        if (empty($potentialAudience)) {
            $potentialAudience = new TvAdPotentialAudience();
            $potentialAudience->tv_ad_id = $id;
        }

        if ($model->load(Yii::$app->request->post())
            && $potentialAudience->load(Yii::$app->request->post())
            && $model->save()
            && $potentialAudience->save()
        ) {
            $url = Yii::$app->request->get('back_url');
            $url = !empty($url) ? urldecode($url) : ['index'];
            return $this->redirect($url);
        }
        return $this->render('update', ['model' => $model, 'potentialAudience' => $potentialAudience]);
    }
}
