<?php

namespace backend\controllers;

/**
 * Class RadioController
 * @package backend\controllers
 */
class RadioController extends BaseController
{
    public $model = 'common\models\Radio';
    public $modelSearch = 'backend\models\RadioSearch';
}
