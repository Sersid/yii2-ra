<?php

namespace backend\controllers;

/**
 * Class FormFeedbackController
 * @package backend\controllers
 */
class FormFeedbackController extends BaseController
{
    public $model = 'common\models\FormFeedback';
    public $modelSearch = 'backend\models\FormFeedbackSearch';
}
