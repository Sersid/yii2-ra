<?php
declare(strict_types=1);

namespace backend\controllers;

use backend\models\InternetOfferSearch;
use common\models\InternetOffer;
use common\models\InternetOfferFaq;
use common\models\InternetOfferMoreOffer;
use common\models\InternetOfferSubOffer;
use Yii;

class InternetAdController extends BaseController
{
    public $model = InternetOffer::class;
    public $modelSearch = InternetOfferSearch::class;

    public function actionCreate()
    {
        $model = new InternetOffer();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveRelations($model);
            $url = Yii::$app->request->get('back_url');
            $url = !empty($url) ? urldecode($url) : ['index'];
            return $this->redirect($url);
        } else {
            if (!Yii::$app->request->post()) {
                if ($model->hasProperty('status')) {
                    $model->status = true;
                }
                if ($model->hasProperty('sort')) {
                    $model->sort = 500;
                }
            }
            return $this->render('create', ['model' => $model]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveRelations($model);
            $url = Yii::$app->request->get('back_url');
            $url = !empty($url) ? urldecode($url) : ['index'];
            return $this->redirect($url);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    private function saveRelations(InternetOffer $model): void
    {
        $post = $this->request->post('InternetOffer');

        InternetOfferSubOffer::deleteAll(['offer_id' => $model->id]);
        foreach ($post['subOffers'] ?? [] as $moreOffer) {
            $subOfferLink = new InternetOfferSubOffer();
            $subOfferLink->offer_id = $model->id;
            $subOfferLink->sub_offer_id = $moreOffer;
            $subOfferLink->save();
        }

        InternetOfferMoreOffer::deleteAll(['offer_id' => $model->id]);
        foreach ($post['moreOffers'] ?? [] as $moreOffer) {
            $moreOfferLink = new InternetOfferMoreOffer();
            $moreOfferLink->offer_id = $model->id;
            $moreOfferLink->more_offer_id = $moreOffer;
            $moreOfferLink->save();
        }

        InternetOfferFaq::deleteAll(['offer_id' => $model->id]);
        foreach ($post['faqList'] ?? [] as $faqId) {
            $moreOfferLink = new InternetOfferFaq();
            $moreOfferLink->offer_id = $model->id;
            $moreOfferLink->faq_id = $faqId;
            $moreOfferLink->save();
        }
    }
}
