<?php

namespace backend\controllers;

use Yii;
use common\models\MetaTag;
use yii\web\NotFoundHttpException;

/**
 * Class MetaTagController
 * @package backend\controllers
 */
class MetaTagController extends BaseController
{
    public $model = 'common\models\MetaTag';
    public $modelSearch = 'backend\models\MetaTagSearch';

    /**
     * Displays a single MetaTag model.
     *
     * @param integer $id
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {
        return $this->render('view', ['model' => $this->findModel($id)]);
    }

    /**
     * Creates a new MetaTag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCreate()
    {
        $model = new MetaTag();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = Yii::$app->request->get('back_url');
            $url = !empty($url) ? urldecode($url) : ['index'];
            return $this->redirect($url);
        } else {
            $model->status = $model::STATUS_ACTIVE;
            return $this->render('create', ['model' => $model]);
        }
    }
}
