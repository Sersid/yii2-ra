<?php
declare(strict_types=1);

namespace backend\controllers;

/**
 * Class TvController
 * @package backend\controllers
 */
class TvController extends BaseController
{
    public $model = 'common\models\Tv';
    public $modelSearch = 'backend\models\TvSearch';
}
