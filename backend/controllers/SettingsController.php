<?php

namespace backend\controllers;

use backend\models\settings\CallTracking;
use backend\models\settings\YandexDisk;
use Yii;

/**
 * Class SettingsController
 * @package backend\controllers
 */
class SettingsController extends BaseAdminController
{
    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCallTracking()
    {
        $model = new CallTracking();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                Yii::$app->config->set('enableCallTracking', (bool)$model->enableCallTracking);
                Yii::$app->config->set('phoneCallTracking', $model->phoneCallTracking);
                Yii::$app->session->setFlash('settings',true);
            }
        } else {
            $model->enableCallTracking = Yii::$app->config->get('enableCallTracking');
            $model->phoneCallTracking = Yii::$app->config->get('phoneCallTracking');
        }
        return $this->render('call-tracking', ['model' => $model]);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionYandexDisk()
    {
        $model = new YandexDisk();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                Yii::$app->config->set('yandex.clientId', $model->clientId);
                Yii::$app->config->set('yandex.clientSecret', $model->clientSecret);
                Yii::$app->config->set('yandex.dirSummer', $model->dirSummer);
                Yii::$app->config->set('yandex.dirWinter', $model->dirWinter);
                Yii::$app->session->setFlash('settings',true);
            }
        } else {
            $model->clientId = Yii::$app->config->get('yandex.clientId', Yii::$app->params['client_id']);
            $model->clientSecret = Yii::$app->config->get('yandex.clientSecret', Yii::$app->params['client_secret']);
            $model->dirSummer = Yii::$app->config->get('yandex.dirSummer');
            $model->dirWinter = Yii::$app->config->get('yandex.dirWinter');
        }
        return $this->render('yandex-disk', ['model' => $model]);
    }
}
