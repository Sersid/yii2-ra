<?php
declare(strict_types=1);

namespace backend\controllers;

use backend\models\internet\FaqForm;
use NikoM\Advertising\Internet\FAQ\Application\Create\CreateFaqCommand;
use NikoM\Advertising\Internet\FAQ\Application\Create\CreateFaqCommandHandler;
use NikoM\Advertising\Internet\FAQ\Application\Delete\DeleteFaqCommand;
use NikoM\Advertising\Internet\FAQ\Application\Delete\DeleteFaqCommandHandler;
use NikoM\Advertising\Internet\FAQ\Application\FindCollection\FindFaqCollectionQuery;
use NikoM\Advertising\Internet\FAQ\Application\FindCollection\FindFaqCollectionQueryHandler;
use NikoM\Advertising\Internet\FAQ\Application\FindOne\FindOneFaqQuery;
use NikoM\Advertising\Internet\FAQ\Application\FindOne\FindOneFaqQueryHandler;
use NikoM\Advertising\Internet\FAQ\Application\Update\UpdateFaqCommand;
use NikoM\Advertising\Internet\FAQ\Application\Update\UpdateFaqCommandHandler;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqFilter;
use Yii;
use yii\web\NotFoundHttpException;

class InternetAdFaqController extends BaseAdminController
{
    public function actionIndex(): string
    {
        $query = new FindFaqCollectionQuery();
        $query->filter = new FaqFilter();
        $handler = Yii::$container->get(FindFaqCollectionQueryHandler::class);
        
        return $this->render('/internet-ad/faq/index', ['faqCollection' => $handler->handle($query)]);
    }

    public function actionCreate(): string
    {
        $model = new FaqForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $command = new CreateFaqCommand();
            $command->question = $model->question;
            $command->answer = $model->answer;
            $command->isActive = (bool)$model->isActive;
            $command->isGlobal = (bool)$model->isGlobal;
            $command->sort = empty($model->sort) ? null : (int)$model->sort;

            $handler = Yii::$container->get(CreateFaqCommandHandler::class);
            $handler->handle($command);

            $this->redirect('index');
        }

        return $this->render('/internet-ad/faq/create', ['model' => $model]);
    }

    public function actionUpdate(int $id): string
    {
        if (Yii::$app->request->isPost) {
            $model = new FaqForm();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $command = new UpdateFaqCommand();
                $command->id = $id;
                $command->question = $model->question;
                $command->answer = $model->answer;
                $command->isActive = (bool)$model->isActive;
                $command->isGlobal = (bool)$model->isGlobal;
                $command->sort = empty($model->sort) ? null : (int)$model->sort;

                $handler = Yii::$container->get(UpdateFaqCommandHandler::class);
                $handler->handle($command);

                $this->redirect('index');
            }
        } else {
            $query = new FindOneFaqQuery();
            $query->id = $id;
            $handler = Yii::$container->get(FindOneFaqQueryHandler::class);
            $dto = $handler->handle($query);
            if (is_null($dto)) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            $model = FaqForm::fromDTO($dto);
        }

        return $this->render('/internet-ad/faq/update', ['model' => $model]);
    }

    public function actionDelete(int $id)
    {
        $command = new DeleteFaqCommand();
        $command->id = $id;
        
        $handler = Yii::$container->get(DeleteFaqCommandHandler::class);
        $handler->handle($command);
        
        $this->redirect('index');
    }
}
