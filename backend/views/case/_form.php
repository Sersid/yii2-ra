<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\City;
use yii\helpers\ArrayHelper;

/* @var yii\web\View $this */
/* @var common\models\AdvCase $model */
/* @var yii\widgets\ActiveForm $form */

// TODO запросы и изменение логики не должны находиться в шаблоне
$cities = ArrayHelper::map(City::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
$categories = ['' => '- без категории -'] + $model->getCategories();
if ($model->isNewRecord) {
    $model->type = common\models\AdvCase::TYPE_DEFAULT;
}
$js = <<<JS
var input = $('#advcase-type');
input.change(function() {
  if ($(this).val() === '20') {
      $('.js-type-html').show();
      $('.js-type-default').hide();
  } else {
      $('.js-type-html').hide();
      $('.js-type-default').show();
  }
}).trigger('change');
JS;
$this->registerJs($js);
?>

<div class="adv-case-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php if (!$model->isNewRecord): ?>
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>
    <?= $form->field($model, 'image_id')->widget('backend\widgets\PhotoUploadWidget') ?>
    <?= $form->field($model, 'category')->dropDownList($categories) ?>
    <?= $form->field($model, 'type')->dropDownList($model->getTypes()) ?>
    <?= $form->field($model, 'short_desc')->textarea(['rows' => 4]) ?>
    <div class="js-type-html">
        <?= $form->field($model, 'html')->textarea(['rows' => 4]) ?>
    </div>
    <div class="js-type-default">
        <?= $form->field($model, 'product')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'customer')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'city_id')->dropDownList($cities) ?>
        <?= $form->field($model, 'purpose')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'full_desc')->widget(
            'vova07\imperavi\Widget',
            [
                'settings' => [
                    'minHeight' => 200,
                    'plugins' => [
                        'table',
                        'imagemanager',
                        'fullscreen',
                        'textexpander',
                    ],
                    'imageUpload' => \yii\helpers\Url::to(['/file/upload-image']),
                ],
            ]
        ); ?>
    </div>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
