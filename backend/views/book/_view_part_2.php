<?php
/**
 * Created by PhpStorm.
 * User: Yulai
 * Date: 26.02.2017
 * Time: 11:36
 */

use frontend\widgets\Html;

if (Yii::$app->request->isPost and $post = Yii::$app->request->post()) {
    $parentKey = is_numeric($post['parentKey']) ? $post['parentKey'] : $node->parent_id;

    if ($model = \backend\models\Book::findOne($parentKey)) {
        $parent_lvl = $model->lvl;
    } else {
        $parent_lvl = -1;
    }
} else {
    $parentKey = ($node->parent_id > 0) ? $node->parent_id : 0;
    $parent_lvl = $node->lvl ? $node->lvl - 1 : -1;
}

switch ($parent_lvl + 1) {
    case 0:
        $listData = [];
        break;
    case 1:
        $listData = [
            'book' => '<span class="fa fa-book"></span> Книга',
        ];
        break;
    default:
        $listData = [
            'tags' => '<span class="fa fa-tags"></span> Раздел',
            'tag' => '<span class="fa fa-tag"></span> Глава',
            'list-alt' => '<span class="fa fa-list-alt"></span> Страница',
        ];
        break;
}

if (!isset($node->lvl)) {
    switch ($parent_lvl + 1) {
        case 1:
            $selected = 'book';
            break;
        case 2:
            $selected = 'tags';
            break;
        case 3:
            $selected = 'tag';
            break;
        case 4:
            $selected = 'list-alt';
            break;
        default:
            $selected = 'list-alt';
            break;
    }
}

$node->icon = isset($node->lvl) ? $node->icon : $selected;

$this->registerJs(
    "
    jQuery(document).ready(function (){
        var id = $('input[type=radio]:checked').attr('value');
        if(id == 'list-alt')
        {
            $('.field-book-full_desc').css({'display':'block'});
        }
        else
        {
            $('.field-book-full_desc').css({'display':'none'});
        }

        $('input[type=radio]').off().change(function(){
            var id = $(this).attr('value');

            if(id == 'list-alt')
            {
                $('.field-book-full_desc').css({'display':'block'});
            }
            else
            {
                $('.field-book-full_desc').css({'display':'none'});
            }
        });
    });
",
    yii\web\View::POS_READY
);

?>

<?= $form->field($node, 'slug')->textInput(); ?>
<?= $form->field($node, 'icon')->radioList(
    $listData,
    [
        'class' => 'form-control input-multiselect',
        'style' => 'min-height: 60px !important; height: auto;',
    ]
); ?>
<?= $form->field($node, 'parent_id')->hiddenInput(['value' => $parentKey])->label(false); ?>
<?= ($parent_lvl == 0) ? $form->field($node, 'image_id')->widget('backend\widgets\PhotoUploadWidget') : ''; ?>
<?= ($parent_lvl == 0) ? $form->field($node, 'short_desc')->textArea(['rows' => 3]) : ''; ?>
<?= $form->field($node, 'full_desc')->widget(
    'vova07\imperavi\Widget',
    [
        'settings' => [
            'minHeight' => 200,
            'plugins' => [
                'table',
                'textdirection',
                'textexpander',
                'imagemanager',
            ],
            'imageManagerJson' => \yii\helpers\Url::to(['/default/images-get']),
        ],
    ]
);
