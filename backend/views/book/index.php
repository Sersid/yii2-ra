<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Book;
use kartik\tree\TreeView;
use kartik\tree\Module;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    echo TreeView::widget(
        [
            'query' => $model,
            'headingOptions' => ['label' => 'Книги'],
            'rootOptions' => ['label' => '<span class="text-primary">Список</span>'],
            'fontAwesome' => true,
            'isAdmin' => true,
            'showIDAttribute' => false,
            'displayValue' => 0,
            'iconEditSettings' => [
                'show' => 'none',
            ],
            'softDelete' => true,
            'cacheSettings' => ['enableCache' => true],
            'toolbar' => [
                TreeView::BTN_CREATE => [
                    'icon' => 'plus',
                    'options' => ['title' => Yii::t('app', 'Add new page'), 'disabled' => true],
                ],
                TreeView::BTN_CREATE_ROOT => false,
                TreeView::BTN_REMOVE => [
                    'icon' => 'trash',
                    'options' => ['title' => Yii::t('app', 'Delete book'), 'disabled' => true],
                ],
            ],
            'defaultChildNodeIcon' => '<i class="glyphicon glyphicon-list-alt"></i>',
            'nodeAddlViews' => [
                Module::VIEW_PART_2 => '@backend/views/book/_view_part_2',
            ],
        ]
    );
    ?>
</div>
