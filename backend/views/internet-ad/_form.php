<?php
declare(strict_types=1);

use common\models\Advertising\Internet\Faq;
use common\models\InternetMoreOffer;
use common\models\InternetOffer;
use common\models\InternetSubOffer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var View $this */
/* @var InternetOffer $model */
/* @var ActiveForm $form */

$icons = [];
foreach (glob(Yii::getAlias('@frontend/web/img/internet-reklama') . '/*.svg') as $icon)
{
    $icon = basename($icon);
    $icons[$icon] = Html::img('/img/internet-reklama/' . $icon);
}

$subOffers = ArrayHelper::map(InternetSubOffer::find()->all(), 'id', 'name');
$moreOffers = ArrayHelper::map(InternetMoreOffer::find()->all(), 'id', 'name');
$faqList = ArrayHelper::map(Faq::find()->all(), 'id', 'question');
?>
<style type="text/css">
  .icons {
      height: 250px;
      overflow-x: auto;
  }
  .radio {
      margin-bottom: 20px;
  }
  .radio img {
      max-height: 65px;
  }
</style>
<div class="info-form">
  <?php $form = ActiveForm::begin(); ?>
  <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'icon')->radioList($icons, ['encode' => false, 'class' => 'icons']) ?>
  <?= $form->field($model, 'intro_text')->textarea(['rows' => 3]) ?>
  <?= $form->field($model, 'subOffers')->checkboxList($subOffers) ?>
  <?= $form->field($model, 'moreOffers')->checkboxList($moreOffers) ?>
  <?= $form->field($model, 'faqList')->checkboxList($faqList) ?>
  <hr>
  <h3>SEO</h3>
  <?= $form->field($model, 'page_title')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'page_title_with_city')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'page_description')->textarea(['rows' => 3]) ?>
  <?= $form->field($model, 'page_description_with_city')->textarea(['rows' => 3]) ?>
  <?= $form->field($model, 'page_heading')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'page_heading_with_city')->textInput(['maxlength' => true]) ?>
  <p>Для города можно использовать автозамену:</p>
  <ul class="list-unstyled">
  <li>#CITY_NAME# - Сургут</li>
  <li>#CITY_WHERE# - в Сургуте</li>
  </ul>
  <hr>
  <h3>Тарифы</h3>
  <?= $form->field($model, 'tariff_title')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'tariff_description')->textarea(['rows' => 3]) ?>
  <hr>
  <?= $form->field($model, 'tariff_low_item_title')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'tariff_low_item_description')->textarea(['rows' => 3]) ?>
  <?= $form->field($model, 'tariff_low_item_price')->textInput(['maxlength' => true]) ?>
  <hr>
  <?= $form->field($model, 'tariff_mig_item_title')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'tariff_mig_item_description')->textarea(['rows' => 3]) ?>
  <?= $form->field($model, 'tariff_mig_item_price')->textInput(['maxlength' => true]) ?>
  <hr>
  <?= $form->field($model, 'tariff_high_item_title')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'tariff_high_item_description')->textarea(['rows' => 3]) ?>
  <?= $form->field($model, 'tariff_high_item_price')->textInput(['maxlength' => true]) ?>
  <hr>
  <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
  <?= $form->field($model, 'status')->checkbox() ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
