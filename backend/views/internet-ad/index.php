<?php
declare(strict_types=1);

use backend\models\InternetOfferSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use backend\widgets\GridView;
use yii\web\View;
use yii\widgets\Pjax;

/* @var View $this */
/* @var InternetOfferSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */

$this->title = 'Предложения интернет-рекламы';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<p>
    <?= Html::a('Добавить предложение', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Ответы на часто задаваемые вопросы', ['/internet-ad-faq'], ['class' => 'btn']) ?>
    <?= Html::a('Что мы предлагаем', ['/internet-ad-sub-offer'], ['class' => 'btn']) ?>
    <?= Html::a('Дополнительно предлагаем', ['/internet-ad-more-offer'], ['class' => 'btn']) ?>
</p>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'name',
        'status',
        'sort',
        'created_at',
        'updated_at',
        ['class' => 'backend\widgets\ActionColumn'],
    ],
]); ?>
<?php Pjax::end(); ?>
