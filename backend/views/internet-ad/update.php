<?php
declare(strict_types=1);

use common\models\InternetOffer;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var InternetOffer $model */

$this->title = 'Обновить предложение интернет-рекламы';
$this->params['breadcrumbs'][] = ['label' => 'Предложения интернет-рекламы', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= $this->render('_form', ['model' => $model]) ?>
