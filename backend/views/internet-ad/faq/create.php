<?php
declare(strict_types=1);

use backend\models\internet\FaqForm;
use yii\helpers\Html;
use yii\web\View;

/* @var FaqForm $model */
/* @var View $this */

$this->title = 'Добавить ответ на часто задаваемый вопрос';

echo Html::tag('h2', $this->title);
echo Html::tag('hr');
echo $this->render('_form', ['model' => $model]);
