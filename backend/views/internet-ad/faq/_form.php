<?php
declare(strict_types=1);

use backend\models\internet\FaqForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var FaqForm $model */

$this->params['breadcrumbs'][] = ['label' => 'Интернет-реклама', 'url' => ['/internet-ad']];
$this->params['breadcrumbs'][] = ['label' => 'Ответы на часто задаваемые вопросы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin();
echo $form->field($model, 'isActive')->checkbox();
echo $form->field($model, 'question')->textInput(['maxlength' => true]);
echo $form->field($model, 'answer')->textarea(['rows' => 6]);
echo $form->field($model, 'sort')->textInput(['type' => 'number']);
echo $form->field($model, 'isGlobal')->checkbox();
echo Html::beginTag('div', ['class' => 'form-group']);
echo Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success']);
echo Html::endTag('div');
ActiveForm::end();
