<?php
declare(strict_types=1);

use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqViewCollection;
use yii\helpers\Html;

$this->title = 'Ответы на часто задаваемые вопросы';
$this->params['breadcrumbs'][] = ['label' => 'Интернет-реклама', 'url' => ['/internet-ad']];
$this->params['breadcrumbs'][] = $this->title;
echo Html::tag('h2', $this->title);
echo Html::tag('hr');
echo Html::a('Добавить', ['create'], ['class' => 'btn btn-success']);

/** @var FaqViewCollection $faqCollection */
if ($faqCollection->isEmpty()) {
    echo Html::tag('p', 'Ответы на вопросы не найдены', ['class' => 'text-center']);
} else {
    echo Html::beginTag('table', ['class' => 'table table-striped']);

    echo Html::beginTag('thead');
    echo Html::beginTag('tr');
    echo Html::tag('th', 'ID', ['style' => '50px']);
    echo Html::tag('th', 'Вопрос');
    echo Html::tag('th', '', ['style' => '80px']);
    echo Html::endTag('tr');
    echo Html::endTag('thead');

    echo Html::beginTag('tbody');
    foreach ($faqCollection->all() as $faq) {
        echo Html::beginTag('tr');
        echo Html::tag('td', $faq->id());
        echo Html::tag('td', $faq->question());

        echo Html::beginTag('td');
        echo Html::beginTag('div', ['class' => 'btn-group pull-right']);
        echo Html::a(
            Html::tag('span', '', ['class' => 'glyphicon glyphicon-edit']),
            ['update', 'id' => $faq->id()],
            ['class' => 'btn btn-warning btn-xs']
        );
        echo Html::a(
            Html::tag('span', '', ['class' => 'glyphicon glyphicon-remove']),
            ['delete', 'id' => $faq->id()],
            [
                'class' => 'btn btn-danger btn-xs',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                    'method' => 'post',
                ],
            ]
        );
        echo Html::endTag('div');
        echo Html::endTag('td');

        echo Html::endTag('tr');
    }
    echo Html::endTag('tbody');

    echo Html::endTag('table');
}
