<?php
declare(strict_types=1);

use common\models\InternetOffer;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var InternetOffer $model */

$this->title = 'Добавить предложение';
$this->params['breadcrumbs'][] = ['label' => 'Список предложений интернет-рекламы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', ['model' => $model]) ?>
</div>
