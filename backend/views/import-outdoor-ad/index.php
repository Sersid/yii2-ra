<?php

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $review common\models\Review */

\backend\assets\jQueryFileUploadAsset::register($this);

$this->title = Yii::t('app', 'Import');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Outdoor Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
var error = $('#errors'),
    tableResult = $('#table-result'),
    form = $('#upload-photo'),
    process = $('#process'),
    reportBox = $('.js-reportBox'),
    total = 0;
$('#upload-file-input').fileupload({
    dataType: 'json',
    done: function (e, data) {
        console.log(data.result);
        if(data.result.error) {
            error.html(data.result.error);
            error.show();
            process.hide();
            reportBox.hide();
        } else {
            total = data.result.total;
            error.hide(); 
            form.hide();
            process.show();
            reportBox.show();
            importCsv(data.result.offset);
        }
    },
    progressall: function (e, data) {
        var progress_val = parseInt(data.loaded / data.total * 100, 10);
        process.show();
        process.find('.progress-bar').css(
            'width',
            progress_val + '%'
        ).html('Загрузка файла: ' + progress_val +'%');
    },
    error: function(jqXHR,textStatus,errorThrown) {
        alert(errorThrown);
    }
});
var importCsv = function(offset) {
  $.ajax({
      url: tableResult.data('url'),
      data: {offset:offset},
      dataType: 'json',
      success: function(data) {
          $.each(data.result, function (index, result) {
              tableResult.append('<tr class="'+(result.error ? 'danger' : 'success')+'"><td>'+result.number+' '+result.side+'</td><td>'+result.message+'</td></tr>');
          });
          var percent = 100 / total * offset;
          if (data.stop) {
              percent = 100;
          } else {
              importCsv(data.offset);
          }
          process.find('.progress-bar').css('width', percent +'%').html(percent.toFixed(2) +'%');
      },
      error: function(jqXHR,textStatus,errorThrown) {
          alert(errorThrown);
      }
  })
};
JS;
$this->registerJs($js);

?>
<?php
$form = ActiveForm::begin(
    [
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
        'enableClientValidation' => false,
        'id' => 'upload-photo',
        'action' => ['upload'],
    ]
);
?>
<?= $form->field($model, 'file')->fileInput(
    [
        'id' => 'upload-file-input',
    ]
) ?>
<?php ActiveForm::end(); ?>
<div class="progress" id="process" style="display: none">
    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<div id="errors" class="alert alert-danger" style="display: none;"></div>
<div class="js-reportBox" style="height:300px; overflow-y: scroll; display:none; ">
    <table class="table">
        <tbody id="table-result" data-url="<?= \yii\helpers\Url::to(['process']) ?>"></tbody>
    </table>
</div>
