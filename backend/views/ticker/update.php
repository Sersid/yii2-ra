<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ticker */

$this->title = Yii::t('app', 'Update Ticker');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tickers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ticker-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
