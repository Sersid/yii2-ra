<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\City;
use common\models\Tv;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TickerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tickers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticker-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Ticker'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'city_id',
                'value' => 'city.name',
                'filter' => ArrayHelper::map(City::find()->select(['id', 'name'])->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'tv_id',
                'value' => 'tv.name',
                'filter' => ArrayHelper::map(Tv::find()->select(['id', 'name'])->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all(), 'id', 'name'),
            ],
            'price_text_block',
            'price_comm',
            'price_no_comm',
            'type',
            'cnt_shows',
            'status',
            'sort',
//            'created_at',
//            'updated_at',

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
