<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ticker */

$this->title = Yii::t('app', 'Create Ticker');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tickers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticker-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
