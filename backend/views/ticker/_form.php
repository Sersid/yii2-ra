<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\City;
use yii\helpers\ArrayHelper;
use common\models\Tv;

/* @var $this yii\web\View */
/* @var $model common\models\Ticker */
/* @var $form yii\widgets\ActiveForm */

$cities = ArrayHelper::map(City::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
$tv = ArrayHelper::map(Tv::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
?>

<div class="ticker-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'city_id')->dropDownList($cities) ?>
    <?= $form->field($model, 'tv_id')->dropDownList($tv) ?>
    <?= $form->field($model, 'price_text_block')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'price_comm')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'price_no_comm')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->radioList($model->getTypes())?>
    <?= $form->field($model, 'cnt_shows')->textInput(['type' => 'integer']) ?>
    <?= $form->field($model, 'x2')->checkbox() ?>
    <?= $form->field($model, 'weekend')->checkbox() ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
