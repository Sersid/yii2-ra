<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\models\settings\YandexDisk */

$this->title = Yii::t('app', 'Yandex Disk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(); ?>
<?php if(Yii::$app->session->hasFlash('settings')): ?>
    <div class="alert alert-success" role="alert">
        Сохранено
    </div>
<?php endif; ?>
<?= $form->field($model, 'clientId')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'clientSecret')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'dirSummer')->textarea() ?>
<?= $form->field($model, 'dirWinter')->textarea() ?>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' =>  'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
