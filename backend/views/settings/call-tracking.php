<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\models\Settings */

$this->title = Yii::t('app', 'Call Tracking');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(); ?>
<?php if(Yii::$app->session->hasFlash('settings')): ?>
    <div class="alert alert-success" role="alert">
        Сохранено
    </div>
<?php endif; ?>
<?= $form->field($model, 'enableCallTracking')->checkbox() ?>
<?= $form->field($model, 'phoneCallTracking')->textInput(['maxlength' => true]) ?>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' =>  'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
