<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\models\Settings */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= \yii\bootstrap\Nav::widget(
    [
        'items' => [
            [
                'label' => Yii::t('app', 'Call Tracking'),
                'url' => ['call-tracking'],
            ],
            [
                'label' => Yii::t('app', 'Yandex Disk'),
                'url' => ['yandex-disk'],
            ],
        ],
    ]
); ?>
