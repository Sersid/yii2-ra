<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $order \common\models\Order */

$this->title = 'Информация о заказе #'.$order->getNumber();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <h3>Информация о клиенте</h3>
    <div class="row">
        <div class="col-md-6">
            <table class="table">
                <tr>
                    <td><?=$order->getAttributeLabel('name')?>:</td>
                    <td><?= Html::encode($order->name) ?></td>
                </tr>
                <tr>
                    <td><?=$order->getAttributeLabel('phone')?>:</td>
                    <td><?= Html::encode($order->phone) ?></td>
                </tr>
                <tr>
                    <td><?=$order->getAttributeLabel('email')?>:</td>
                    <td><?= Html::a(Html::encode($order->email), 'mailto:'.Html::encode($order->email)) ?></td>
                </tr>
                <tr>
                    <td>Откуда</td>
                    <td><?=Html::encode($order->model_type)?></td>
                </tr>
            </table>
        </div>
    </div>
    <?php if(!empty($order->outdoors)):?>
        <h3>Наружная реклама</h3>
        <?php $total = 0?>
        <table class="table">
            <tr>
                <th>Номер</th>
                <th>Месяцы</th>
                <th>Установка</th>
                <th>Печать</th>
                <th>Дизайн</th>
                <th>Стоимость</th>
            </tr>
            <?php foreach ($order->outdoors as $outdoor):?>
                <?php
            $total += $outdoor->getPrice();
            ?>
            <tr>
                <td><?=$outdoor->side->number?></td>
                <td><?=implode(', ', $outdoor->getDates())?></td>
                <td><?=$outdoor->install ? 'да' : 'нет'?></td>
                <td><?=$outdoor->print ? 'да' : 'нет'?></td>
                <td><?=$outdoor->design ? 'да' : 'нет'?></td>
                <td>
                    <?php if ($outdoor->getOldPrice() > $outdoor->getPrice()):?>
                    <span class="old-price"><?=number_format($outdoor->getOldPrice(), 0, ' ', ' ')?> руб.</span><br>
                    <?php endif; ?>
                    <?=number_format($outdoor->getPrice(), 0, ' ', ' ')?> руб.
                </td>
            </tr>
            <?php endforeach?>
        </table>
        <h4><strong>Итого:</strong> <?=number_format($total, 0, ' ', ' ')?> руб.</h4>
        <br><br>
    <?php endif;?>
    <?php if(!empty($order->tickers)):?>
        <h3>Бегущая строка</h3>
        <?php $total = 0?>
        <?php foreach ($order->tickers as $ticker):?>
            <?php
            $items = $ticker->getItems();
            $total += $items['total'];
            ?>
            <div class="row">
                <div class="col-md-6">
                    <table class="table">
                        <tr>
                            <td><?=$ticker->getAttributeLabel('type')?>:</td>
                            <td><?= $ticker->getTypeName() ?></td>
                        </tr>
                        <tr>
                            <td><?=$ticker->getAttributeLabel('message')?>:</td>
                            <td><?= Html::encode($ticker->message) ?></td>
                        </tr>
                        <tr>
                            <td><?=$ticker->getAttributeLabel('phone')?>:</td>
                            <td>
                                <?php if(!empty($ticker->phones)):?>
                                    <?php foreach ($ticker->phones as $phone):?>
                                        <?=$phone->phone?><br>
                                    <?php endforeach?>
                                <?php else:?>
                                    - не указаны -
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <td>Итого:</td>
                            <td><?=number_format($items['total'], 0, ' ', ' ')?> руб.</td>
                        </tr>
                    </table>
                </div>
            </div>
            <table class="table">
                <tr>
                    <th>Канал</th>
                    <th>Кол-во слов</th>
                    <th>Период проката</th>
                    <th>Итого</th>
                </tr>
                <?php foreach ($items['items'] as $item):?>
                    <tr>
                        <td><?=$item['name']?> (<?=$item['price_type'] == 'text_block' ? 'текстовый блок' : 'бегущая строка'?>)</td>
                        <td><?=$item['words']?></td>
                        <td>
                            <?=date('d.m.Y', strtotime($item['date_from']))?> -
                            <?=date('d.m.Y', strtotime($item['date_to']))?>
                            (<?=$item['cnt_days']?>)
                        </td>
                        <td>
                            <?=number_format($item['total'], 0, ' ', ' ')?> руб.
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
            <hr>
        <?php endforeach?>
        <h4><strong>Итого:</strong> <?=number_format($total, 0, ' ', ' ')?> руб.</h4>
        <br><br>
    <?php endif?>
    <?php if(!empty($order->radios)):?>
        <h3>Реклама на радио</h3>
        <?php $total = 0?>
        <?php foreach ($order->radios as $radio):?>
            <div class="row">
                <div class="col-md-6">
                    <table class="table">
                        <tr>
                            <td><?=$radio->getAttributeLabel('type')?>:</td>
                            <td><?= $radio->getTypeName() ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <table class="table">
                <tr>
                    <th>Радио</th>
                    <th>Город</th>
                    <th>Длительность ролика</th>
                    <th>Кол-во прокатов ролика в день</th>
                    <th>Кол-во прокатов ролика в день</th>
                    <th>Сумма</th>
                </tr>
                <?php foreach ($radio->userRadioAdRadios as $item):?>
                    <?php
                $price = $radio->getPrice($item) * $item->days;
                $total += $price;
                ?>
                <tr>
                    <td><?=$item->radioAd->radio->name?></td>
                    <td><?=$item->radioAd->city->name?></td>
                    <td><?=empty($item->duration) ? '-' : $item->duration?></td>
                    <td><?=empty($item->days) ? '-' : $item->days?></td>
                    <td><?=empty($item->views) ? '-' : $item->views?></td>
                    <td><?=empty($price) ? '-' : number_format($price, 0, ' ', ' ').' руб.' ?></td>
                </tr>
                <?php endforeach;?>
            </table>
            <hr>
        <?php endforeach;?>
        <?php if(!empty($total)):?>
        <h4><strong>Итого:</strong> <?=number_format($total, 0, ' ', ' ')?> руб.</h4>
        <?php endif?>
        <br><br>
    <?php endif;?>
    <?php if(!empty($order->tvs)):?>
        <h3>Реклама на тв</h3>
        <?php $total = 0?>
        <?php foreach ($order->tvs as $tv):?>
            <div class="row">
                <div class="col-md-6">
                    <table class="table">
                        <tr>
                            <td><?=$tv->getAttributeLabel('type')?>:</td>
                            <td><?= $tv->getTypeName() ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <table class="table">
                <tr>
                    <th>Радио</th>
                    <th>Город</th>
                    <th>Длительность ролика</th>
                    <th>Кол-во прокатов ролика в день</th>
                    <th>Кол-во прокатов ролика в день</th>
                    <th>Сумма</th>
                </tr>
                <?php foreach ($tv->userTvAdTvs as $item):?>
                    <?php
                    $price = $tv->getPrice($item);
                    $total += $price;
                    ?>
                    <tr>
                        <td><?=$item->tvAd->tv->name?></td>
                        <td><?=$item->tvAd->city->name?></td>
                        <td><?=empty($item->duration) ? '-' : $item->duration?></td>
                        <td><?=empty($item->days) ? '-' : $item->days?></td>
                        <td><?=empty($item->views) ? '-' : $item->views?></td>
                        <td><?=empty($price) ? '-' : number_format($price, 0, ' ', ' ').' руб.' ?></td>
                    </tr>
                <?php endforeach;?>
            </table>
            <hr>
        <?php endforeach;?>
        <?php if(!empty($total)):?>
            <h4><strong>Итого:</strong> <?=number_format($total, 0, ' ', ' ')?> руб.</h4>
        <?php endif?>
        <br><br>
    <?php endif?>
</div>
