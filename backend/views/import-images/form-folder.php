<?php

use common\models\YandexDiskFolder;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var YandexDiskFolder $model */
/* @var bool $isSuccess */
if ($isSuccess) {
    echo Html::tag(
        'div',
        $model->isNewRecord ? 'Папка успешно добавлена' : 'Папка успешно обновлена',
        ['class' => 'alert alert-success']
    );
    echo Html::script('setInterval(function() {location.reload();}, 1000);');
} else {
    $form = ActiveForm::begin(['enableClientValidation' => false]);
    echo $form->field($model, 'name')->textInput(['maxlength' => true]);
    echo $form->field($model, 'season')->radioList($model::getSeasons());
    ?>
    <div class="alert alert-info text-sm-left" role="alert">

        <p>
            <small><strong>Зачем указывать сезон?</strong></small>
        </p>
        <p>
            <small>
                У каждой рекламной поверхности может быть несколько фото (зимние и летние). Первыми сайт выведет те,
                которые соотвествуют текущему сезону. Но по названию папок не всегда понятно, к какому времени года они
                относятся, поэтому для корректной работы сайта, пожалуйста, укажите явно. Спасибо :)
            </small>
        </p>

    </div>
    <?php
    echo Html::tag('hr');
    echo Html::beginTag('div', ['class' => 'form-group']);
    echo Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update'), ['class' => 'btn btn-success']);
    echo Html::endTag('div');
    ActiveForm::end();
}

