<?php

use backend\models\OutdoorAdImages;
use common\models\YandexDiskFolder;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var YandexDiskFolder $model */
/* @var OutdoorAdImages $searchModel */
/* @var ActiveDataProvider $dataProvider */
?>
<div class="js-dir">
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4 text-right">
            <div class="btn-group js-buttonGroup">
                <?php
                echo Html::button(
                    Html::tag('i', '', ['class' => 'fas fa-redo-alt']) . ' Обновить',
                    [
                        'class' => 'btn btn-warning btn-sm js-reload',
                        'data' => ['id' => $model->id],
                    ]
                );
                echo Html::button(
                    Html::tag('i', '', ['class' => 'fas fa-download']) . ' Загрузить',
                    ['class' => 'btn btn-success btn-sm js-downloadFiles']
                );
                ?>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4"><p>{summary}</p></div>
        <div class="col-md-8 text-right">{pager}</div>
    </div>
    <table class="table">
        {items}
    </table>
    <div class="text-right">{pager}</div>
</div>
