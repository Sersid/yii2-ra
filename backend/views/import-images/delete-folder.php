<?php

use common\models\YandexDiskFolder;
use yii\helpers\Html;

/* @var YandexDiskFolder $model */
/* @var bool $isSuccess */

if ($isSuccess) {
    echo Html::tag('div', 'Папка успешно удалена', ['class' => 'alert alert-success']);
    echo Html::script('setInterval(function() {location.reload();}, 1000);');
} else {
    echo Html::beginForm();
    echo Html::tag(
        'p',
        "Вы уверены, что хотите удалить папку <strong>&laquo;{$model->getName()}&raquo;</strong> и <strong>все файлы</strong> на сайте внутри нее?"
    );
    echo Html::tag('hr');
    echo Html::hiddenInput('del', 1);
    echo Html::submitButton('Да, удалить папку', ['class' => 'btn btn-danger']);
    echo Html::endForm();
}
