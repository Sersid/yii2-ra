<?php

use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideImage;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var View $this */
/* @var OutdoorAdSideImage $model */
?>
<tr class="js-fileRow <?= $model->getRowCssClass() ?>">
    <td style="width: 70px;"><?= $model->getStatusLabel() ?></td>
    <td><?= $model->file_name ?></td>
    <td class="text-right">
        <div class="btn-group js-buttonGroup">
            <?php
            if ($model->status == OutdoorAdSideImage::STATUS_LOADED) {
                if ($model->side->status == OutdoorAdSide::STATUS_ENABLE) {
                    echo Html::a(
                        Html::tag('i', '', ['class' => 'fas fa-ticket-alt']),
                        $model->side->getDetailPageUrl(),
                        [
                            'title' => 'Посмотреть поверхность',
                            'target' => '_blank',
                            'class' => 'btn btn-default btn-xs',
                        ]
                    );
                } else {
                    echo Html::a(
                        Html::tag('i', '', ['class' => 'fas fa-lock']),
                        ['/outdoor-ad/update', 'id' => $model->side->parent_id],
                        [
                            'title' => 'Поверхность неактивна',
                            'target' => '_blank',
                            'class' => 'btn btn-default btn-xs',
                        ]
                    );
                }
                echo Html::a(
                    Html::tag('i', '', ['class' => 'fas fa-image']),
                    $model->getSrc(),
                    [
                        'title' => 'Посмотреть фото',
                        'target' => '_blank',
                        'class' => 'btn btn-default btn-xs',
                    ]
                );
            }
            if (in_array($model->status, OutdoorAdSideImage::getCanDownloadStatuses())) {
                echo Html::button(
                    Html::tag('i', '', ['class' => 'fas fa-download']),
                    [
                        'class' => 'btn btn-default btn-xs js-downloadFile',
                        'data' => [
                            'url' => Url::to(['download-file', 'id' => $model->id]),
                        ],
                        'title' => 'Скачать файл',
                    ]
                );
            }
            ?>
        </div>
    </td>
</tr>
