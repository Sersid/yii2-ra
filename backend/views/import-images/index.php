<?php

use common\models\YandexDiskFolder;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var View $this */
/* @var array $folders */

$this->title = 'Загрузка фотографий из Яндекс.Диска';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Outdoor Ads'), 'url' => ['/outdoor-ad']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<'CSS'
.pseudo {
    color: #337ab7;
    cursor: pointer;
    border-bottom: 1px dashed;
}
.js-pager {
    margin: 0;
}
CSS;
$this->registerCss($css);

// Модальное окно для управления папки
echo Modal::widget(
    [
        'options' => ['class' => 'js-smallModal'],
        'header' => Html::tag('h3', '', ['class' => 'js-modalSmallTitle']),
        'toggleButton' => false,
        'bodyOptions' => ['class' => 'modal-body js-modalSmallBody'],
    ]
);

// Модальное окно для просмотра содержимого папки
echo Modal::widget(
    [
        'options' => ['class' => 'js-bigModal'],
        'header' => Html::tag('h3', '', ['class' => 'js-modalTitle']),
        'toggleButton' => false,
        'size' => Modal::SIZE_LARGE,
        'bodyOptions' => ['class' => 'modal-body js-modalBody'],
    ]
);

// Основной блок
echo Html::beginTag(
    'div',
    [
        'class' => 'js-multiUploadPhotos',
        'data' => [
            'url-folder' => Url::to(['folder-info']),
            'url-files' => Url::to(['download-files']),
            'url-view-folder' => Url::to(['view-folder']),
            'url-create-folder' => Url::to(['create-folder']),
            'url-update-folder' => Url::to(['update-folder']),
            'url-delete-folder' => Url::to(['delete-folder']),
            'url-reload-folder' => Url::to(['reload-folder']),
        ],
    ]
);
?>
    <div class="js-multiUploadPhotos">
        <div class="row">
            <div class="col-md-8"><h3 style="margin-top: 5px">Загрузка фотографий из Яндекс.Диска</h3></div>
            <div class="col-md-4 text-right">
                <?php
                // Группа кнопок
                echo Html::beginTag('div', ['class' => 'btn-group js-btnMainGroup']);
                echo Html::button(
                    Html::tag('i', '', ['class' => 'fas fa-folder-plus']) . ' Добавить папку',
                    [
                        'class' => 'btn btn-default js-createFolder',
                        'title' => 'Добавить папку',
                    ]
                );
                echo Html::button(
                    Html::tag('i', '', ['class' => 'fas fa-download']) . ' Загрузить все файлы',
                    ['class' => 'btn btn-success js-runUploadAll']
                );
                echo Html::endTag('div');
                // ~ Группа кнопок
                ?>
            </div>
        </div>
        <hr>
        <?php foreach ($folders as $code => $arDirs) { ?>
            <h4><?= YandexDiskFolder::getSeasons()[$code] ?></h4>
            <table class="table table-striped">
                <?php
                /** @var YandexDiskFolder $dir */
                foreach ($arDirs as $dir) { ?>
                    <tr class="js-row" data-id="<?= $dir->id ?>">
                        <td style="width:50px;" class="js-status"><?= $dir->getStatusLabel() ?></td>
                        <td>
                            <?php if ($dir->isSuccess()) { ?>
                                <span class="js-showDir pseudo"><?= $dir->getName() ?></span>
                            <?php } else { ?>
                                <?= $dir->getName() ?>
                            <?php } ?>
                        </td>
                        <td class="text-right">
                            <div class="js-statistic hidden">
                                <span title="Успешно загружено" class="js-success text-success"></span> /
                                <span title="Ошибок" class="js-errors text-danger"></span> /
                                <span title="Пропущено" class="js-skipped text-muted"></span> /
                                <span title="Всего" class="js-total"></span>
                            </div>
                        </td>
                        <td class="text-right" width="70px;">
                            <div class="js-folderBtnGroup btn-group">
                                <?= Html::button(
                                    Html::tag(
                                        'i',
                                        '',
                                        ['class' => 'glyphicon glyphicon-edit']
                                    ),
                                    [
                                        'class' => 'btn btn-warning btn-xs js-updateFolder',
                                        'title' => 'Редактировать папку',
                                        'data-id' => $dir->id,
                                    ]
                                ) ?>
                                <?= Html::button(
                                    Html::tag(
                                        'i',
                                        '',
                                        ['class' => 'glyphicon glyphicon-remove']
                                    ),
                                    [
                                        'class' => 'btn btn-danger btn-xs js-deleteFolder',
                                        'title' => 'Удалить папку',
                                        'data-id' => $dir->id,
                                    ]
                                ) ?>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        <?php } ?>
    </div>
<?php
echo Html::endTag('div');
