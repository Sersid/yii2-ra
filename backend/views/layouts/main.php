<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin(
        [
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]
    );
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems = [
            [
                'label' => Yii::t('app', 'Home page'),
                'url' => ['/site/index'],
                'active' => $this->context->id == 'site',
            ],
            [
                'label' => 'Реклама',
                'active' => in_array($this->context->id,
                    [
                        'tv-ad',
                        'ticker',
                        'outdoor-ad',
                        'radio-ad',
                        'internet-ad',
                        'internet-ad-faq',
                    ]),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Tv Ads'),
                        'url' => ['/tv-ad'],
                        'active' => $this->context->id == 'tv-ad',
                    ],
                    [
                        'label' => Yii::t('app', 'Tickers'),
                        'url' => ['/ticker'],
                        'active' => $this->context->id == 'ticker',
                    ],
                    [
                        'label' => Yii::t('app', 'Outdoor Ads'),
                        'active' => in_array($this->context->id, ['outdoor-ad']),
                        'url' => ['/outdoor-ad/'],
                    ],
                    [
                        'label' => Yii::t('app', 'Radio Ads'),
                        'url' => ['/radio-ad'],
                        'active' => $this->context->id == 'radio-ad',
                    ],
                    [
                        'label' => 'Интернет-реклама',
                        'url' => ['/internet-ad'],
                        'active' => $this->context->id == 'internet-ad',
                    ],
                ],
            ],
            [
                'label' => Yii::t('app', 'Forms'),
                'active' => in_array($this->context->id, ['form-feedback', 'form-callme', 'form-sorry']),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Feedback'),
                        'url' => ['/form-feedback'],
                        'active' => $this->context->id == 'form-feedback',
                    ],
                    [
                        'label' => Yii::t('app', 'Call Me'),
                        'url' => ['/form-callme'],
                        'active' => $this->context->id == 'form-callme',
                    ],
                    [
                        'label' => Yii::t('app', 'Sorry'),
                        'url' => ['/form-sorry'],
                        'active' => $this->context->id == 'form-sorry',
                    ],
                ],
            ],
            [
                'label' => Yii::t('app', 'Info'),
                'active' => in_array(
                    $this->context->id,
                    ['review', 'customers', 'faq', 'info', 'info-categories', 'book', 'case', 'page']
                ),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Reviews'),
                        'url' => ['/review'],
                        'active' => $this->context->id == 'review',
                    ],
                    [
                        'label' => Yii::t('app', 'Customers'),
                        'url' => ['/customers'],
                        'active' => $this->context->id == 'customers',
                    ],
                    [
                        'label' => Yii::t('app', 'Faq'),
                        'url' => ['/faq'],
                        'active' => $this->context->id == 'faq',
                    ],
                    '<li class="divider"></li>',
                    [
                        'label' => Yii::t('app', 'Info list'),
                        'url' => ['/info'],
                        'active' => $this->context->id == 'info',
                    ],
                    [
                        'label' => Yii::t('app', 'Info Categories'),
                        'url' => ['/info-categories'],
                        'active' => $this->context->id == 'info-categories',
                    ],
                    '<li class="divider"></li>',
                    [
                        'label' => Yii::t('app', 'Books'),
                        'url' => ['/book'],
                        'active' => $this->context->id == 'book',
                    ],
                    '<li class="divider"></li>',
                    [
                        'label' => Yii::t('app', 'Cases'),
                        'url' => ['/case'],
                        'active' => $this->context->id == 'case',
                    ],
                    [
                        'label' => Yii::t('app', 'Case Categories'),
                        'url' => ['/case-category'],
                        'active' => $this->context->id == 'case-category',
                    ],
                    '<li class="divider"></li>',
                    [
                        'label' => Yii::t('app', 'Pages'),
                        'url' => ['/page'],
                        'active' => $this->context->id == 'page',
                    ],
                    '<li class="divider"></li>',
                    [
                        'label' => Yii::t('app', 'Cities'),
                        'url' => ['/cities'],
                        'active' => $this->context->id == 'cities',
                    ],
                    [
                        'label' => Yii::t('app', 'Regions'),
                        'url' => ['/region'],
                        'active' => $this->context->id == 'region',
                    ],
                    [
                        'label' => Yii::t('app', 'Streets'),
                        'url' => ['/street'],
                        'active' => $this->context->id == 'street',
                    ],
                    [
                        'label' => Yii::t('app', 'TV'),
                        'url' => ['/tv'],
                        'active' => $this->context->id == 'tv',
                    ],
                    [
                        'label' => Yii::t('app', 'Radio'),
                        'url' => ['/radio'],
                        'active' => $this->context->id == 'radio',
                    ],
                    ['label' => Yii::t('app', 'Meta Tags'), 'url' => ['/meta-tag']],
                    ['label' => 'Сотрудники', 'url' => ['/employee']],
                    ['label' => 'Загрузить реквизиты', 'url' => ['/requisites']],
                ],
            ],
            [
                'label' => Yii::t('app', 'Settings'),
                'active' => in_array(
                    $this->context->id,
                    ['call-tracking', 'yandex-disk']
                ),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Call Tracking'),
                        'url' => ['/settings/call-tracking'],
                        'active' => $this->context->id == 'call-tracking',
                    ],
                    [
                        'label' => Yii::t('app', 'Yandex Disk'),
                        'url' => ['/settings/yandex-disk'],
                        'active' => $this->context->id == 'yandex-disk',
                    ],
                ],
            ],
        ];
        $menuItems[] = '<li>' . Html::beginForm(['/site/logout'], 'post') . Html::submitButton(
                'Выйти (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            ) . Html::endForm() . '</li>';
    }
    echo Nav::widget(
        [
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]
    );
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Niko media <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
