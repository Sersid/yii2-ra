<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\City;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\AdvCase */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="adv-case-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?php endif;?>
    <?= $form->field($model, 'content')->textarea(['rows' =>  20]); ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
