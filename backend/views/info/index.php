<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\InfoCategory;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\InfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Material list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Material'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'category_id',
                'value' => 'category.name',
                'filter' => ArrayHelper::map(InfoCategory::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name'),
            ],
            'status',
            'sort',
            'created_at',
            'updated_at',
            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
