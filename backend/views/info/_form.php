<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\InfoCategory;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Info */
/* @var $form yii\widgets\ActiveForm */
$categories = ArrayHelper::map(InfoCategory::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
?>

<div class="info-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'category_id')->dropDownList($categories) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?php endif;?>
    <?= $form->field($model, 'image_id')->widget('backend\widgets\PhotoUploadWidget') ?>
    <?= $form->field($model, 'short_desc')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'full_desc')->widget('vova07\imperavi\Widget', [
        'settings' => [
            'minHeight' => 200,
            'plugins' => [
                'table',
                'textdirection',
                'textexpander',
                'imagemanager',
            ],
            'imageManagerJson' => \yii\helpers\Url::to(['/default/images-get']),
        ]
    ]); ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
