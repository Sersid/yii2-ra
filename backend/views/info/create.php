<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Info */

$this->title = Yii::t('app', 'Create Material');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Material list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
