<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tv */

$this->title = Yii::t('app', 'Update Radio');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Radio'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="radio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
