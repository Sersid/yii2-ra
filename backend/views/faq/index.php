<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Faq');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Faq'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                [
                    'attribute' => 'question',
                    'content' => function ($data) {
                        return Html::a($data->question, ['update', 'id' => $data->id]);
                    },
                ],
                'status',
                'sort',
                'created_at',
                'updated_at',
                ['class' => 'backend\widgets\ActionColumn'],
            ],
        ]
    ); ?>
    <?php Pjax::end(); ?></div>
