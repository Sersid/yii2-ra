<?php
declare(strict_types=1);

use common\models\InternetSubOffer;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var InternetSubOffer $model */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Интернет-реклама', 'url' => ['/internet-ad']];
$this->params['breadcrumbs'][] = ['label' => 'Дополнительно предлагаем', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= $this->render('_form', ['model' => $model]) ?>
