<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a(Yii::t('app', 'Create City'), ['create'], ['class' => 'btn btn-success']) ?></p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'name',
                [
                    'attribute' => 'salary',
                    'headerOptions' => ['style' => 'width:40px;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'content' => function ($data) {
                        return number_format($data->salary, 0, '.', ' ') . ' руб.';
                    },
                ],
                [
                    'attribute' => 'population',
                    'headerOptions' => ['style' => 'width:40px;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'content' => function ($data) {
                        return number_format($data->population, 2, '.', ' ');
                    },
                ],
                [
                    'attribute' => 'is_large_city',
                    'format' => 'boolean',
                    'filter' => [
                        Yii::t('app', 'No'),
                        Yii::t('app', 'Yes'),
                    ],
                    'headerOptions' => ['style' => 'width:40px;'],
                    'contentOptions' => ['class' => 'text-center'],
                ],
                'status',
                'sort',
                'created_at',
                'updated_at',
                ['class' => 'backend\widgets\ActionColumn'],
            ],
        ]
    ); ?>
    <?php Pjax::end(); ?>
</div>
