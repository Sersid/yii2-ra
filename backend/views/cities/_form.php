<?php

use common\models\Region;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\bootstrap\ActiveForm */

$regions = ArrayHelper::map(
    Region::find()->select(['id', 'name'])->orderBy('name')->asArray()->all(),
    'id',
    'name'
);
?>
<div class="city-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'where')->textInput(['maxlength' => true, 'placeholder' => 'в Сургуте']) ?>
    <?= $form->field($model, 'region_id')->dropDownList($regions) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone_digit')->checkbox(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title_outdoor_ad')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title_tv')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title_ticker')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title_radio')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'image_id')->widget('backend\widgets\PhotoUploadWidget') ?>
    <?= $form->field(
        $model,
        'salary',
        [
            'template' => "{label}\n{beginWrapper}\n<div class=\"input-group\">\n{input}<div class=\"input-group-addon\">руб.</div>\n</div>\n{hint}\n{error}\n{endWrapper}",
        ]
    )->textInput() ?>
    <?= $form->field(
        $model,
        'population',
        [
            'template' => "{label}\n{beginWrapper}\n<div class=\"input-group\">\n{input}<div class=\"input-group-addon\">тыс. чел.</div>\n</div>\n{hint}\n{error}\n{endWrapper}",
        ]
    )->textInput() ?>
    <?= $form->field(
        $model,
        'density',
        [
            'template' => "{label}\n{beginWrapper}\n<div class=\"input-group\">\n{input}<div class=\"input-group-addon\">чел./ км<sup>2</sup></div>\n</div>\n{hint}\n{error}\n{endWrapper}",
        ]
    )->textInput() ?>
    <?= $form->field($model, 'activities')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'is_large_city')->checkbox() ?>
    <?= $form->field($model, 'content')->widget(
        'vova07\imperavi\Widget',
        [
            'settings' => [
                'minHeight' => 200,
                'plugins' => [
                    'table',
                    'imagemanager',
                    'fullscreen',
                    'textexpander',
                ],
                'imageUpload' => Url::to(['/file/upload-image']),
            ],
        ]
    ); ?>
    <?= $form->field($model, 'content_outdoor_ad')->widget(
        'vova07\imperavi\Widget',
        [
            'settings' => [
                'minHeight' => 200,
                'plugins' => [
                    'table',
                    'imagemanager',
                    'fullscreen',
                    'textexpander',
                ],
                'imageUpload' => Url::to(['/file/upload-image']),
            ],
        ]
    ); ?>
    <?= $form->field($model, 'content_tv')->widget(
        'vova07\imperavi\Widget',
        [
            'settings' => [
                'minHeight' => 200,
                'plugins' => [
                    'table',
                    'imagemanager',
                    'fullscreen',
                    'textexpander',
                ],
                'imageUpload' => Url::to(['/file/upload-image']),
            ],
        ]
    ); ?>
    <?= $form->field($model, 'content_ticker')->widget(
        'vova07\imperavi\Widget',
        [
            'settings' => [
                'minHeight' => 200,
                'plugins' => [
                    'table',
                    'imagemanager',
                    'fullscreen',
                    'textexpander',
                ],
                'imageUpload' => Url::to(['/file/upload-image']),
            ],
        ]
    ); ?>
    <?= $form->field($model, 'content_radio')->widget(
        'vova07\imperavi\Widget',
        [
            'settings' => [
                'minHeight' => 200,
                'plugins' => [
                    'table',
                    'imagemanager',
                    'fullscreen',
                    'textexpander',
                ],
                'imageUpload' => Url::to(['/file/upload-image']),
            ],
        ]
    ); ?>
    <?= $form->field($model, 'contact_text')->widget(
        'vova07\imperavi\Widget',
        [
            'settings' => [
                'minHeight' => 200,
                'plugins' => [
                    'table',
                    'imagemanager',
                    'fullscreen',
                    'textexpander',
                ],
                'imageUpload' => Url::to(['/file/upload-image']),
            ],
        ]
    ); ?>
    <?php
    if (!empty($model->requisitesFile) && !$model->isNewRecord) {
        echo $form->field($model, 'isNeedDeleteRequisitesFile')->checkbox();
    } else {
        echo  $form->field($model, 'requisitesFile')->fileInput();
    }
    ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="navbar navbar-default navbar-fixed-bottom" style="padding-top: 15px">
        <div class="container">
            <div class="form-group">
                <?= Html::submitButton("Сохранить изменения", ['class' => 'btn btn-primary btn-block']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
