<?php

use yii\widgets\Pjax;
use frontend\widgets\Html;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Panel';
?>
<div class="review-index">

    <h1>Последние заказы</h1>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= \backend\widgets\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'content' => function($data){
                    return Html::a($data->getNumber(), ['/order', 'id' => $data->id]);
                }
            ],
            [
                'attribute' => 'name',
                'content' => function($data){
                    return $data->name;
                }
            ],
            'phone',
            'email',
            'created_at',
            ['class' => 'backend\widgets\ActionColumn', 'template' => '{delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>