<?php
declare(strict_types=1);

use common\models\TvAd;
use common\models\TvAdPotentialAudience;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var TvAd $model */
/* @var TvAdPotentialAudience $potentialAudience */

$this->title = Yii::t('app', 'Create Tv Ad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tv Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tv-ad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'potentialAudience' => $potentialAudience,
    ]) ?>

</div>
