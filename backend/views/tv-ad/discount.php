<?php
declare(strict_types=1);

use backend\models\GlobalDiscount;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var yii\web\View $this */
/* @var GlobalDiscount $model */

$this->title = 'Глобальная скидка';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tv Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(); ?>
<?php if(Yii::$app->session->hasFlash('settings')): ?>
    <div class="alert alert-success" role="alert">
        Сохранено
    </div>
<?php endif; ?>
<?= $form->field($model, 'discount')->textInput(['maxlength' => true, 'type' => 'number']) ?>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' =>  'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
