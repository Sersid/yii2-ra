<?php
declare(strict_types=1);

use backend\widgets\PhotoUploadWidget;
use common\models\TvAd;
use common\models\TvAdPotentialAudience;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\City;
use yii\helpers\ArrayHelper;
use common\models\Tv;
use yii\web\View;

/* @var View $this */
/* @var TvAd $model */
/* @var TvAdPotentialAudience $potentialAudience */
/* @var ActiveForm $form */

$cities = ArrayHelper::map(City::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
$tv = ArrayHelper::map(Tv::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
?>

<div class="tv-ad-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'city_id')->dropDownList($cities) ?>
    <?= $form->field($model, 'tv_id')->dropDownList($tv) ?>
    <?= $form->field($model, 'price_ad_sec')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'city_map')->widget(PhotoUploadWidget::class) ?>
    <?= $form->field($model, 'price_table')->textarea([
        'rows' => 10,
        'placeholder' => "5 сек | От 300 руб. до 60 000 руб. | От 150 руб. до 25 000 руб.\n10 сек | От 650 руб. до 120 000 руб. | От 350 руб. до 50 000 руб.",
    ]) ?>


    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>

    <h3>Потенциальная аудитория</h3>
    <?= $form->field($potentialAudience, 'men')->textInput(['type' => 'number']) ?>
    <?= $form->field($potentialAudience, 'women')->textInput(['type' => 'number']) ?>
    <?= $form->field($potentialAudience, 'youth')->textInput(['type' => 'number']) ?>
    <?= $form->field($potentialAudience, 'total')->textInput(['type' => 'number']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
