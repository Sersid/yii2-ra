<?php

/* @var $this yii\web\View */

$this->title = 'Реквизиты';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

use backend\models\settings\Requisites;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Requisites */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $success bool */
?>
<?php if ($success) { ?>
    <div class="alert alert-success" role="alert">
        Файл успешно загружен
    </div>
<?php } else { ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'file')->fileInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Загрузить', ['class' =>  'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?php }
