<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OutdoorAd */

$this->title = Yii::t('app', 'Update Outdoor Ad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Outdoor Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="outdoor-ad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
