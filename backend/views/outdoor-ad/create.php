<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OutdoorAd */

$this->title = Yii::t('app', 'Create Outdoor Ad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Outdoor Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outdoor-ad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
