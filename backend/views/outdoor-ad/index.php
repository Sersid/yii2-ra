<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;
use common\models\City;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OutdoorAdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Outdoor Ads');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
var button = $('#multi-delete');
$('.deleted, .select-on-check-all').change(function() {
  var keys = $('#w0').yiiGridView('getSelectedRows');
  if(keys.length > 0) {
      button.removeAttr('disabled');
  } else {
      button.attr('disabled', 'disabled');
  }
});
button.click(function() {
  $(this).addClass('disabled').html('Подождите...');
})
JS;
?>
<div class="outdoor-ad-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Import'), ['/import-outdoor-ad'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Загрузка изображений', ['/import-images'], ['class' => 'btn btn-warning']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?php $this->registerJs($js); ?>
    <?= Html::beginForm(['multi-delete', 'back_url' => urlencode(Yii::$app->request->url)], 'post'); ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'cssClass' => 'deleted',
                ],
                'id',
                [
                    'attribute' => 'number',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['style' => 'width:100px;'],
                ],
                [
                    'attribute' => 'sides',
                    'filter' => $searchModel->getSidesArr(),
                    'value' => function ($data) {
                        $sides = [];
                        /** @var \common\models\OutdoorAdSide $side */
                        foreach ($data->outdoorAdSides as $side) {
                            $sides[] = Html::a($side->getSide(), $side->getDetailPageUrl(), ['target' => '_blank']);
                        }
                        return implode(', ', $sides);
                    },
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['style' => 'width:90px;'],
                ],
                [
                    'attribute' => 'city_id',
                    'filter' => ArrayHelper::map(
                        City::find()->select(['id', 'name'])->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                            ->asArray()->all(),
                        'id',
                        'name'
                    ),
                    'value' => function ($data) {

                        return !empty($data->city) ? $data->city->name : null;
                    },
                    'headerOptions' => ['style' => 'width:120px;'],
                ],
                [
                    'attribute' => 'street_id',
                    'value' => function ($data) {
                        return $data->street->name;
                    },
                ],
                [
                    'attribute' => 'format',
                    'value' => function ($data) {
                        return $data->getFormat();
                    },
                    'filter' => $searchModel->getFormats(),
                ],
                [
                    'attribute' => 'size',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['style' => 'width:80px;'],
                ],
                [
                    'attribute' => 'lat',
                    'headerOptions' => ['style' => 'width:10px;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'value' => function ($data) {
                        return !empty($data->lat) && !empty($data->lon) ? Html::tag(
                            'i',
                            '',
                            ['class' => 'glyphicon glyphicon-map-marker']
                        ) : '';
                    },
                    'format' => 'raw',
                    'label' => 'Карта',
                ],
                'updated_at',
                ['class' => 'backend\widgets\ActionColumn'],
            ],
        ]
    ); ?>
    <?= Html::submitButton(
        "Удалить выбранные",
        ['class' => 'btn btn-danger', 'disabled' => 'disabled', 'id' => 'multi-delete']
    ) ?>
    <?= Html::endForm(); ?>
    <?php Pjax::end(); ?>
</div>
