<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\City;

/* @var $this yii\web\View */
/* @var $model common\models\OutdoorAd */
/* @var $form yii\widgets\ActiveForm */

$cities = ArrayHelper::map(City::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
$js = <<<JS
ymaps.ready(function() {
    var map = $('#map'),
        city = $('#outdoorad-city_id'),
        street = $('#outdoorad-street_name'),
        address = $('.js-address'),
        sides = $('#outdoorad-sides'),
        rotate = $('#outdoorad-rotate'),
        lat = $('#outdoorad-lat'),
        lon = $('#outdoorad-lon'),
        myMap = new ymaps.Map("map", {
            center: [55.76, 37.64],
            controls: ['zoomControl', 'typeSelector', 'fullscreenControl'],
            zoom: 7
        }),
        point = new ymaps.GeoObject({
            geometry: {
                type: "Point",
                coordinates: [55.76, 37.64]
            }
        }, {
            iconLayout: 'default#image',
            iconImageSize: [30, 30],
            iconOffset: [-3,25],
            balloonOffset:[8,-25],
            draggable: true
        }),
        search = function(text) {
          ymaps.geocode(text, {
            results: 1
            }).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0),
                    coords = firstGeoObject.geometry.getCoordinates(),
                    bounds = firstGeoObject.properties.get('boundedBy');
                
                point.geometry.setCoordinates(coords);
                lat.val(coords[0]);
                lon.val(coords[1]);
                myMap.setBounds(bounds);
            });
        };
    myMap.geoObjects.add(point);
    point.events.add('dragend', function (e) {
        var coords = point.geometry.getCoordinates();
        lat.val(coords[0]);
        lon.val(coords[1]);
    });
    map.on('update-map', function() {
        text = [];
        text.push(city.find('option[value='+city.val()+']').html());
        text.push(street.val());
        text.push(address.val());
        search(text.join(', '));
    });
    map.on('update-icon', function() {
        point.options.set('iconImageHref','/map/'+sides.val()+'/'+parseInt(rotate.val())+'.png');
    });
    city.change(function(){map.trigger('update-map');});
    street.change(function(){map.trigger('update-map');});
    address.change(function(){map.trigger('update-map');});
    sides.change(function(){
        rotate.attr('max', 360/parseInt($(this).val())-5);
        map.trigger('update-icon');
    }).trigger('change');
    rotate.change(function(){map.trigger('update-icon');});
    if(lat.val().length > 0 && lon.val().length > 0) {
        point.geometry.setCoordinates([lat.val(), lon.val()]);
        myMap.setBounds(myMap.geoObjects.getBounds());
        myMap.setZoom(13);
        // myMap.setBounds(point.properties.get('boundedBy'));
    } else {
        map.trigger('update-map');
    }
});
JS;
$this->registerJs($js);
?>
<div class="outdoor-ad-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'number')->textInput() ?>
    <?= $form->field($model, 'city_id')->dropDownList($cities) ?>
    <?= $form->field($model, 'street_name')->textInput() ?>
    <?= $form->field($model, 'sides')->dropDownList($model->getSidesArr()) ?>
    <?php
    foreach ($model->outdoorAdSides as $side) {
        echo '<h4>Сторона ' . $side->getSide() . '</h4>';
        echo $form->field($side, 'address')->textInput(
            [
                'maxlength' => true,
                'name' => 'OutdoorAdSide[' . $side->id . '][address]',
                'id' => $side->id . '_address',
                'class' => 'form-control js-address',
            ]
        );
    }
    ?>
    <div id="map" style="height: 400px;"></div>
    <?= $form->field($model, 'rotate')->textInput(['type' => 'range', 'min' => 0, 'max' => 355, 'step' => 5]) ?>
    <?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lon')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'format')->dropDownList($model->getFormats()) ?>
    <?= $form->field($model, 'type_display')->dropDownList($model->getTypeDisplays()) ?>
    <?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <hr>
    <?php
    foreach ($model->outdoorAdSides as $side) {
        echo '<h3>Сторона ' . $side->getSide() . '</h3>';
        foreach (['price', 'price_installation', 'price_print', 'price_design'] as $field) {
            echo $form->field($side, $field)->textInput(
                [
                    'maxlength' => true,
                    'name' => 'OutdoorAdSide[' . $side->id . '][' . $field . ']',
                    'id' => $side->id . '_' . $field,
                ]
            );
        }
        echo $form->field($side, 'description')->widget(
            vova07\imperavi\Widget::className(),
            [
                'settings' => [
                    'minHeight' => 200,
                    'plugins' => [
                        'table',
                        'textdirection',
                        'textexpander',
                        'imagemanager',
                    ],
                    'imageManagerJson' => \yii\helpers\Url::to(['/default/images-get']),
                ],
                'options' => [
                    'id' => $side->id . '_description',
                    'name' => 'OutdoorAdSide[' . $side->id . '][description]',
                ],
            ]
        );
        echo $form->field($side, 'rating')->dropDownList(
            $side->getRatings(),
            [
                'name' => 'OutdoorAdSide[' . $side->id . '][rating]',
                'id' => $side->id . '_rating',
            ]
        );
        echo $form->field($side, 'discount_monthly')->textInput(
            [
                'maxlength' => true,
                'name' => 'OutdoorAdSide[' . $side->id . '][discount_monthly]',
                'id' => $side->id . '_discount_monthly',
            ]
        );
        $i = 0;
        echo '<div class="row form-busy">';
        foreach ($side->busy as $busy) {
            // скрываем прошлые месяцы
            if ($busy->year < date('Y') || ($busy->year == date('Y') && $busy->month < date('n'))) {
                continue;
            }
            if ($i > 0 && $i % 4 == 0) {
                echo '</div><div class="row form-busy">';
            }
            $id = 'busy_' . $side->id . '-' . $busy->year . '_' . $busy->month;
            echo '<div class="col-sm-3"><div class="thumbnail">';
            echo '<p><strong>' . $busy->getMonthName() . ' ' . $busy->year . '</strong></p>';
            echo $form->field($busy, 'status')->dropDownList(
                $busy->getStatusesNames(),
                [
                    'name' => 'Busy[' . $side->id . '][' . $busy->year . '][' . $busy->month . '][status]',
                    'id' => $id . '_status',
                ]
            );
            echo $form->field($busy, 'discount')->textInput(
                [
                    'maxlength' => 6,
                    'name' => 'Busy[' . $side->id . '][' . $busy->year . '][' . $busy->month . '][discount]',
                    'id' => $id . '_discount',
                ]
            );
            echo '</div></div>';
            $i++;
        }
        echo '</div>';
    }
    ?>
    <hr>
    <div class="navbar navbar-default navbar-fixed-bottom" style="padding-top: 15px">
        <div class="container">
            <div class="form-group">
                <?= Html::submitButton("Сохранить изменения", ['class' => 'btn btn-primary btn-block']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
