<?php
declare(strict_types=1);

use backend\widgets\PhotoUploadWidget;
use common\models\Tv;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var View $this */
/* @var Tv $model */
/* @var ActiveForm $form */
?>

<div class="tv-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'image_id')->widget(PhotoUploadWidget::class) ?>
    <?= $form->field($model, 'big_logo')->widget(PhotoUploadWidget::class) ?>
    <?= $form->field($model, 'big_banner')->widget(PhotoUploadWidget::class) ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <h3>Стилизация рекламы на ТВ</h3>
    <?= $form->field($model, 'color_heading')->dropDownList([
        null => '',
        'red' => 'red',
    ]) ?>
    <?= $form->field($model, 'color_line')->dropDownList([
        null => '',
        'red' => 'red',
        'blue' => 'blue',
        'yellow' => 'yellow',
        'orange' => 'orange',
    ]) ?>
    <?= $form->field($model, 'color_potential_audience')->dropDownList([
        null => '',
        'gold' => 'gold',
        'blue' => 'blue',
        'yellow' => 'yellow',
        'red' => 'red',
        'orange' => 'orange',
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
