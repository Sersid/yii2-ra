<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tv */

$this->title = Yii::t('app', 'Create Tv');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TV'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tv-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
