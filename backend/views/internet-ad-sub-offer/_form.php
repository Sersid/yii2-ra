<?php
declare(strict_types=1);

use common\models\InternetMoreOffer;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var View $this */
/* @var InternetMoreOffer $model */
/* @var ActiveForm $form */

$icons = [];
foreach (glob(Yii::getAlias('@frontend/web/img/internet-reklama') . '/*.svg') as $icon)
{
    $icon = basename($icon);
    $icons[$icon] = Html::img('/img/internet-reklama/' . $icon);
}

?>
<style type="text/css">
    .icons {
        height: 250px;
        overflow-x: auto;
    }
    .radio {
        margin-bottom: 20px;
    }
    .radio img {
        max-height: 65px;
    }
</style>
<div class="info-category-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'icon')->radioList($icons, ['encode' => false, 'class' => 'icons']) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
