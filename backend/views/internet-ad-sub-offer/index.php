<?php
declare(strict_types=1);

use backend\models\InternetSubOfferSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use backend\widgets\GridView;
use yii\web\View;
use yii\widgets\Pjax;

/* @var View $this */
/* @var InternetSubOfferSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */

$this->title = 'Что мы предлагаем';
$this->params['breadcrumbs'][] = ['label' => 'Интернет-реклама', 'url' => ['/internet-ad']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<p>
    <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'name',
        'status',
        'sort',
        'created_at',
        'updated_at',
        ['class' => 'backend\widgets\ActionColumn'],
    ],
]); ?>
<?php Pjax::end(); ?>
