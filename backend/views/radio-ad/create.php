<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TvAd */

$this->title = Yii::t('app', 'Create Radio Ad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Radio Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radio-ad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
