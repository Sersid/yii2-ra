<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\City;
use yii\helpers\ArrayHelper;
use common\models\Radio;

/* @var $this yii\web\View */
/* @var $model common\models\TvAd */
/* @var $form yii\widgets\ActiveForm */

$cities = ArrayHelper::map(City::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
$tv = ArrayHelper::map(Radio::find()->select(['id', 'name'])->orderBy('sort')->asArray()->all(), 'id', 'name');
?>

<div class="tv-ad-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'city_id')->dropDownList($cities) ?>
    <?= $form->field($model, 'radio_id')->dropDownList($tv) ?>
    <?= $form->field($model, 'price_ad_sec')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'price_ad_show')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'discount')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
