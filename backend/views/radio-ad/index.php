<?php

use yii\helpers\Html;
use backend\widgets\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\City;
use common\models\Radio;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\RadioAdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Radio Ads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radio-ad-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Radio Ad'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a("Глобальная скидка", ['discount'], ['class' => 'btn btn-primary']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'city_id',
                'value' => 'city.name',
                'filter' => ArrayHelper::map(City::find()->select(['id', 'name'])->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all(), 'id', 'name'),
            ],
            [
                'attribute' => 'radio_id',
                'value' => 'radio.name',
                'filter' => ArrayHelper::map(Radio::find()->select(['id', 'name'])->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all(), 'id', 'name'),
            ],
            'discount',
            'status',
            'sort',
            'created_at',
            'updated_at',
            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
