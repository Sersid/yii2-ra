<?php

namespace backend\models;

use common\models\Ticker;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TickerSearch represents the model behind the search form about `common\models\Ticker`.
 */
class TickerSearch extends Ticker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'tv_id', 'status', 'sort', 'cnt_shows'], 'integer'],
            [['price_comm', 'price_no_comm', 'price_text_block'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ticker::find()->joinWith('city')->joinWith('tv');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id' => $this->id,
                'city_id' => $this->city_id,
                'tv_id' => $this->tv_id,
                'price_comm' => $this->price_comm,
                'price_no_comm' => $this->price_no_comm,
                'price_text_block' => $this->price_text_block,
                'cnt_shows' => $this->cnt_shows,
                'status' => $this->status,
                'sort' => $this->sort,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ]
        );

        return $dataProvider;
    }
}
