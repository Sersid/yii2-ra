<?php
/**
 * Created by PhpStorm.
 * User: Yulai
 * Date: 21.02.2017
 * Time: 17:21
 */

namespace backend\models;

use common\models\File;
use kartik\tree\models\Tree;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%book}}".
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $lvl
 * @property string  $name
 * @property string  $icon
 * @property integer $icon_type
 * @property boolean $active
 * @property boolean $selected
 * @property boolean $disabled
 * @property boolean $readonly
 * @property boolean $visible
 * @property boolean $collapsed
 * @property boolean $movable_u
 * @property boolean $movable_d
 * @property boolean $movable_l
 * @property boolean $movable_r
 * @property boolean $removable
 * @property boolean $removable_all
 */
class Book extends Tree
{
    //public $sort;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%book}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root', 'lft', 'rgt', 'lvl', 'icon_type', 'parent_id', 'image_id'], 'integer'],
            [['name'], 'required'],
            [
                [
                    'active',
                    'selected',
                    'disabled',
                    'readonly',
                    'visible',
                    'collapsed',
                    'movable_u',
                    'movable_d',
                    'movable_l',
                    'movable_r',
                    'removable',
                    'removable_all',
                ],
                'boolean',
            ],
            [['icon'], 'string', 'max' => 255],
            [['name', 'short_desc', 'full_desc', 'slug'], 'string'],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::className(),
                'targetAttribute' => ['image_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'root' => Yii::t('app', 'Root'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'lvl' => Yii::t('app', 'Lvl'),
            'name' => Yii::t('app', 'Name'),
            'icon' => Yii::t('app', 'Icon'),
            'icon_type' => Yii::t('app', 'Icon Type'),
            'active' => Yii::t('app', 'Active'),
            'selected' => Yii::t('app', 'Selected'),
            'disabled' => Yii::t('app', 'Disabled'),
            'readonly' => Yii::t('app', 'Readonly'),
            'visible' => Yii::t('app', 'Visible'),
            'collapsed' => Yii::t('app', 'Collapsed'),
            'movable_u' => Yii::t('app', 'Movable U'),
            'movable_d' => Yii::t('app', 'Movable D'),
            'movable_l' => Yii::t('app', 'Movable L'),
            'movable_r' => Yii::t('app', 'Movable R'),
            'removable' => Yii::t('app', 'Removable'),
            'removable_all' => Yii::t('app', 'Removable All'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'slug' => Yii::t('app', 'Slug'),
            'short_desc' => Yii::t('app', 'Short description'),
            'full_desc' => Yii::t('app', 'Full description'),
            'image_id' => Yii::t('app', 'Image'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (!isset($this->lvl)) {
                $this->lvl = 0;
            }

            if (!isset($this->lft)) {
                $this->lft = 0;
            }

            if (!isset($this->rgt)) {
                $this->rgt = 0;
            }

            if (!$this->parent_id) {
                $this->parent_id = 0;
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->image)) {
                $this->image->delete();
            }
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $slugBehavior = [
            'class' => 'Zelenin\yii\behaviors\Slug',
            'slugAttribute' => 'slug',
            'attribute' => 'name',
            // optional params
            'ensureUnique' => true,
            'replacement' => '-',
            'lowercase' => true,
            'immutable' => true,
            // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
            'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
        ];

        array_push($behaviors, $slugBehavior);

        return $behaviors;
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }
}
