<?php

namespace backend\models;

use common\models\OutdoorAd;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OutdoorAdSearch represents the model behind the search form about `common\models\OutdoorAd`.
 */
class OutdoorAdSearch extends OutdoorAd
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'city_id',
                    'street_id',
                    'format',
                    'sides',
                    'rotate',
                    'status',
                    'sort',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            [['size', 'number'], 'safe'],
            [['lat', 'lon'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws InvalidArgumentException
     */
    public function search($params)
    {
        $query = OutdoorAd::find();

        $query->with(
            [
                'city',
                'outdoorAdSides',
            ]
        );

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'outdoor_ad.id' => $this->id,
                'outdoor_ad.city_id' => $this->city_id,
                'street_id' => $this->street_id,
                'format' => $this->format,
                'sides' => $this->sides,
                'lat' => $this->lat,
                'lon' => $this->lon,
                'number' => $this->number,
                'rotate' => $this->rotate,
                'status' => $this->status,
                'sort' => $this->sort,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ]
        );

        $query->joinWith('street');

        return $dataProvider;
    }
}
