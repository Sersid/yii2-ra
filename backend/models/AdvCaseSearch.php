<?php

namespace backend\models;

use common\models\AdvCase;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AdvCaseSearch represents the model behind the search form about `common\models\AdvCase`.
 */
class AdvCaseSearch extends AdvCase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'image_id', 'city_id', 'status', 'sort'], 'integer'],
            [['name', 'slug', 'short_desc', 'full_desc', 'product', 'customer', 'purpose'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws InvalidArgumentException
     */
    public function search($params)
    {
        $query = AdvCase::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC]],
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id' => $this->id,
                'image_id' => $this->image_id,
                'city_id' => $this->city_id,
                'status' => $this->status,
                'sort' => $this->sort,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name])->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])->andFilterWhere(
                ['like', 'full_desc', $this->full_desc]
            )->andFilterWhere(['like', 'product', $this->product])->andFilterWhere(
                ['like', 'customer', $this->customer]
            )->andFilterWhere(['like', 'purpose', $this->purpose]);

        return $dataProvider;
    }
}
