<?php

namespace backend\models\settings;

use yii\base\Model;

/**
 * Class YandexDisk
 * @package backend\models\settings
 */
class YandexDisk extends Model
{
    public $clientId;
    public $clientSecret;
    public $dirSummer;
    public $dirWinter;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['clientId', 'clientSecret', 'dirSummer', 'dirWinter'], 'filter', 'filter' => 'trim'],
            [['clientId', 'clientSecret', 'dirSummer', 'dirWinter'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'clientId' => 'client_id',
            'clientSecret' => 'client_secret',
            'dirSummer' => 'Летние фото',
            'dirWinter' => 'Зимние фото',
        ];
    }
}
