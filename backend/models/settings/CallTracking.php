<?php

namespace backend\models\settings;

use yii\base\Model;

/**
 * Class CallTracking
 * @package backend\models\settings
 */
class CallTracking extends Model
{
    public $enableCallTracking;
    public $phoneCallTracking;

    public function rules()
    {
        return [
            [['enableCallTracking'], 'boolean'],
            [
                'phoneCallTracking',
                'required',
                'when' => function ($model) {
                    return $model->enableCallTracking;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#settings-enablecalltracking').is(':checked');
                }",
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'enableCallTracking' => 'Включить коллтрекинг',
            'phoneCallTracking' => 'Телефон для коллтрекинга',
        ];
    }
}
