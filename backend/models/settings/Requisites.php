<?php

namespace backend\models\settings;

use yii\base\Model;

/**
 * Class Requisites
 * @package backend\models\settings
 */
class Requisites extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file', 'extensions' => ['doc', 'docx', 'txt']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл реквизитов (doc)',
        ];
    }
}
