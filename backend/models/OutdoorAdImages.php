<?php

namespace backend\models;

use common\models\OutdoorAdSideImage;
use yii\data\ActiveDataProvider;

class OutdoorAdImages extends OutdoorAdSideImage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['file_name', 'save'],
        ];
    }

    /**
     * Поиск файлов
     *
     * @param $folder_id
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($folder_id, $params)
    {
        $query = self::find();
        $query->where(['folder_id' => $folder_id]);
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => ['defaultOrder' => ['status' => SORT_ASC, 'file_name' => SORT_ASC]],
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]
        );
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere(['status' => $this->status]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name]);

        return $dataProvider;
    }
}
