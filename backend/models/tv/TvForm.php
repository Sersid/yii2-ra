<?php
declare(strict_types=1);

namespace backend\models\tv;

use yii\base\Model;
use Yii;

class TvForm extends Model
{
    public $name;
    public $slug;
    public $logo;
    public $bigLogo;
    public $bigBanner;
    public $status = true;
    public $sort;

    public function rules(): array
    {
        return [
            [['name', 'slug'], 'filter', 'filter' => 'trim'],
            [['name', 'logo', 'status'], 'required'],
            [['logo', 'bigLogo', 'bigBanner', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'boolean'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'logo' => Yii::t('app', 'Logo'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'slug' => Yii::t('app', 'Slug'),
        ];
    }
}
