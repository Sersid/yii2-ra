<?php
declare(strict_types=1);

namespace backend\models;

use yii\base\Model;

class GlobalDiscount  extends Model
{
    public $discount;

    public function rules()
    {
        return [
            [['discount'], 'double', 'max' => 100, 'min' => 0],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'discount' => 'Скидка',
        ];
    }
}
