<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class PhotoUploadFromUrl extends Model
{
    /**
     * @var string
     */
    public $url;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['url', 'filter', 'filter' => 'trim'],
            ['url', 'required'],
            ['url', 'url'],
            ['url', 'validateImageURL'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'url' => Yii::t('app', 'URL'),
        ];
    }

    /**
     * @param $attribute
     *
     * @return bool
     */
    public function validateImageURL($attribute)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->$attribute);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_exec($ch);
        $info = curl_getinfo($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if (!empty($error)) {
            $this->addError($attribute, 'cUrl error: ' . $error);
        } else if ($info['http_code'] != 200) {
            $this->addError(
                $attribute,
                Yii::t('app', 'Connection failed. Code: {code}', ['code' => $info['http_code']])
            );
        } else if (!in_array(
            $info['content_type'],
            [
                'image/png',
                'image/jpeg',
                'image/gif',
            ]
        )) {
            $this->addError($attribute, Yii::t('app', 'Not an image'));
        }
    }
}
