<?php
declare(strict_types=1);

namespace backend\models\internet;

use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDto;
use yii\base\Model;

class FaqForm extends Model
{
    public $question;
    public $answer;
    public $isGlobal = false;
    public $isActive = true;
    public $sort;

    public static function fromDTO(FaqDto $dto): self
    {
        $self = new self();
        $self->question = $dto->question;
        $self->answer = $dto->answer;
        $self->isActive = $dto->isActive;
        $self->isGlobal = $dto->isGlobal;
        $self->sort = $dto->sort;

        return $self;
    }

    public function rules(): array
    {
        return [
            [['question', 'answer'], 'filter', 'filter' => 'trim'],
            [['question', 'answer', 'isActive', 'isGlobal'], 'required'],
            [['sort'], 'integer'],
            [['question'], 'string', 'max' => 255],
            [['isActive', 'isGlobal'], 'boolean'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'isActive' => 'Активность',
            'isGlobal' => 'Показывать на главной раздела "Интернет-реклама"',
            'sort' => 'Сортировка',
        ];
    }
}
