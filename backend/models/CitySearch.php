<?php

namespace backend\models;

use common\models\City;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CitySearch represents the model behind the search form about `common\models\City`.
 */
class CitySearch extends City
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'image_id', 'salary', 'population', 'density', 'is_large_city', 'status', 'sort'], 'integer'],
            [['name', 'activities'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC]],
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id' => $this->id,
                'image_id' => $this->image_id,
                'salary' => $this->salary,
                'population' => $this->population,
                'density' => $this->density,
                'is_large_city' => $this->is_large_city,
                'status' => $this->status,
                'sort' => $this->sort,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name])->andFilterWhere(
            ['like', 'activities', $this->activities]
        );

        return $dataProvider;
    }
}
