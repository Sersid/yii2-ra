<?php

namespace backend\models;

use common\models\MetaTag;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MetaTagSearch represents the model behind the search form about `common\models\MetaTag`.
 */
class MetaTagSearch extends MetaTag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description', 'url'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetaTag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(['query' => $query]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
                'id' => $this->id,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'status' => $this->status,
            ]
        );

        $query->andFilterWhere(['like', 'title', $this->title])->andFilterWhere(
            ['like', 'description', $this->description]
        )->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
