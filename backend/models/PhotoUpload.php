<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class PhotoUpload extends Model
{
    /**
     * @var string
     */
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            ['file', 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => Yii::t('app', 'File'),
        ];
    }
}