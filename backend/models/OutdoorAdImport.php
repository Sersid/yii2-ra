<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class OutdoorAdImport extends Model
{
    /**
     * @var string
     */
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            ['file', 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ['file' => Yii::t('app', 'File')];
    }
}
