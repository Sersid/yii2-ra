<?php
namespace backend\widgets;

use yii\web\AssetBundle;

class SearchAddressAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@backend/widgets/assets';


    /**
     * @inheritdoc
     */
    public $depends = [
        'backend\assets\TypeheadAsset',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'jquery.searchaddress.js',
    ];
}
