<?php
namespace backend\widgets;

use yii\base\Exception;
use yii\helpers\Html;
use yii\widgets\InputWidget;

class FileUploadWidget extends InputWidget
{
    /**
     * Url load file
     * @var string
     */
    public $urlLoadFile;

    /**
     * Url upload file
     * @var string
     */
    public $urlUploadFile;

    /**
     * Url delete file
     * @var string
     */
    public $urlDeletePhoto;

    /**
     * Url upload file from url
     * @var string
     */
    public $urlUploadFileFromUrl;

    /**
     * Url rotate photo
     * @var string
     */
    public $urlRotatePhoto;

    /**
     * Plugin options
     * @var array
     */
    public $pluginOptions = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        if($this->urlLoadFile === null && empty($this->pluginOptions['urlLoadFile'])) {
            throw new Exception('urlLoadFile is required');
        }
        if($this->urlUploadFile === null && empty($this->pluginOptions['urlUploadFile'])) {
            throw new Exception('urlUploadFile is required');
        }
        if($this->urlDeletePhoto === null && empty($this->pluginOptions['urlDeletePhoto'])) {
            throw new Exception('urlDeletePhoto is required');
        }

        $view = $this->getView();
        jQueryFileUploadAsset::register($view);
        FileUploadAsset::register($view);

        // get formatted date value
        if ($this->hasModel()) {
            $value = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            $value = $this->value;
        }

        $options = $this->options;
        $options['value'] = $value;

        if ($this->hasModel()) {
            echo Html::activeHiddenInput($this->model, $this->attribute, $options);
        } else {
            echo Html::hiddenInput($this->name, $value, $options);
        }

        if(!empty($this->urlLoadFile) && empty($this->pluginOptions['urlLoadFile'])) {
            $this->pluginOptions['urlLoadFile'] = $this->urlLoadFile;
        }
        if(!empty($this->urlUploadFile) && empty($this->pluginOptions['urlUploadFile'])) {
            $this->pluginOptions['urlUploadFile'] = $this->urlUploadFile;
        }
        if(!empty($this->urlDeletePhoto) && empty($this->pluginOptions['urlDeletePhoto'])) {
            $this->pluginOptions['urlDeletePhoto'] = $this->urlDeletePhoto;
        }
        if(!empty($this->urlUploadFileFromUrl) && empty($this->pluginOptions['urlUploadFileFromUrl'])) {
            $this->pluginOptions['urlUploadFileFromUrl'] = $this->urlUploadFileFromUrl;
        }
        if(!empty($this->urlRotatePhoto) && empty($this->pluginOptions['urlRotatePhoto'])) {
            $this->pluginOptions['urlRotatePhoto'] = $this->urlRotatePhoto;
        }

        $pluginOptions = [];
        foreach($this->pluginOptions as $k => $v) {
            $pluginOptions[] = $k.':"'.$v.'"';
        }
        $view->registerJs('jQuery("#'.$options['id'].'").SmartFileUploader({'.implode(",",$pluginOptions).'});');
    }

}