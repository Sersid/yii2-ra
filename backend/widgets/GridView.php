<?php
namespace backend\widgets;

use Yii;
use yii\helpers\Html;


class GridView extends \yii\grid\GridView
{
    public function init()
    {
        $this->tableOptions['class'] = 'table table-striped';
        foreach ($this->columns as $i => $column) {
            switch ($column) {
                case 'id':
                    $this->columns[$i] = [
                        'attribute' => 'id',
                        'headerOptions' => ['style' => 'width:50px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ];
                    break;
                case 'name':
                    $this->columns[$i] = [
                        'attribute' => 'name',
                        'content' => function($data){
                            return Html::a($data->name, ['update', 'id' => $data->id, 'back_url' => urlencode(Yii::$app->request->url)]);
                        }
                    ];
                    break;
                case 'sort':
                    $this->columns[$i] = [
                        'attribute' => 'sort',
                        'headerOptions' => ['style' => 'width:40px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ];
                    break;
                case 'status':
                    $this->columns[$i] = [
                        'attribute' => 'status',
                        'format'=>'boolean',
                        'filter' => [
                            Yii::t('app', 'No'),
                            Yii::t('app', 'Yes'),
                        ],
                        'headerOptions' => ['style' => 'width:40px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ];
                    break;
                case 'created_at':
                case 'updated_at':
                    $this->columns[$i] = [
                        'attribute' => $column,
                        'format' => ['date', 'php:d.m.Y H:i'],
                        'headerOptions' => ['style' => 'width:100px;'],
                        'contentOptions' => ['style' => 'white-space:nowrap;'],
                    ];
                    break;
            }
        }
        parent::init();
    }

}
