<?php
namespace backend\widgets;

use Yii;
use yii\helpers\Url;

class PhotoUploadWidget extends FileUploadWidget
{
    public function init()
    {
        $this->urlLoadFile = Url::to(['/file/load-file']);
        $this->urlUploadFile = Url::to(['/file/upload']);
        $this->urlUploadFileFromUrl = Url::to(['/file/upload-from-url']);
        $this->urlRotatePhoto = Url::to(['/file/rotate']);
        $this->urlDeletePhoto = Url::to(['/file/delete']);
        $this->pluginOptions = [
            'cssClassButtons' => 'btn-default btn-sm',
            'cssClassIconUploadButton' => 'fa-camera',
            'textUploadButton' => 'Загрузить изображение',
            'textMessageBoxTitle' => 'Загрузить по ссылке',
            'textMessageBoxContent' => 'Загрузить изображение по ссылке',
            'textMessageBoxButtonCancel' => 'Отмена',
            'textMessageBoxButtonUpload' => 'Загрузка',
            'textMessageBoxPlaceholder' => 'Введите адрес',
            'textLoaderButton' => 'Загрузка...',
            'textUploadButtonFromPc' => 'Загрузить с компьютера',
            'textUploadButtonFromUrl' => 'Загрузить по ссылке',
            'textErrorTitle' => 'Ошибка',
        ];

        parent::init();
    }

}