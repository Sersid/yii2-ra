<?php

namespace backend\widgets;

class ListView extends \yii\widgets\ListView
{
    public function renderItem($model, $key, $index)
    {
        if ($this->itemView === null) {
            $content = $key;
        } else if (is_string($this->itemView)) {
            $content = $this->getView()->render(
                $this->itemView,
                array_merge(
                    [
                        'model' => $model,
                        'key' => $key,
                        'index' => $index,
                        'widget' => $this,
                    ],
                    $this->viewParams
                )
            );
        } else {
            $content = call_user_func($this->itemView, $model, $key, $index, $this);
        }
        return $content;
    }
}
