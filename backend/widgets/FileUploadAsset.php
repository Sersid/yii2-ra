<?php
namespace backend\widgets;

use yii\web\AssetBundle;

class FileUploadAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@backend/widgets/assets';


    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'smartFileUploader.js',
    ];

    public $css = [
        'smartFileUploader.css',
    ];
}
