<?php
namespace backend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $template = '{update} {delete}';

    public function init()
    {
        $this->template = '<div class="btn-group pull-right">'.$this->template.'</div>';
        $this->headerOptions  = ['style' => 'width: 80px;'];
        parent::init();
    }

    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'View'),
                    'aria-label' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-default btn-xs',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Update'),
                    'aria-label' => Yii::t('yii', 'Update'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-warning btn-xs',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['status'])) {
            $this->buttons['status'] = function ($url, $model, $key) {
                $class = $model->className();
                $enable = $model->status == $class::STATUS_ENABLE;
                $icon = $enable ? 'glyphicon-remove' : 'glyphicon-ok';
                $css_class = $enable ? 'btn-danger' : 'btn-success';
                $options = array_merge([
                    'title' => Yii::t('yii', 'Change status'),
                    'aria-label' => Yii::t('yii', 'Change status'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to change status this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn '.$css_class.' btn-xs',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon '.$icon.'"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-danger btn-xs',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, $options);
            };
        }
    }

    /**
     * @inheritdoc
     */
    public function createUrl($action, $model, $key, $index)
    {
        if (is_callable($this->urlCreator)) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index, $this);
        } else {
            $params = is_array($key) ? $key : ['id' => (string) $key];
            $params[0] = $this->controller ? $this->controller . '/' . $action : $action;
            $params['back_url'] = urlencode(Yii::$app->request->url);
            return Url::toRoute($params);
        }
    }
}
