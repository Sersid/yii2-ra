(function($) {
    $.fn.searchAddress = function(options) {
        var options = $.extend({
            cities: {},
            citySelector: '#city',
            results: 10,
            url: 'http://geocode-maps.yandex.ru/1.x/',
            kind: 'house',
            ajaxLoaderClass:'ajax-loader'
        }, options);

        return this.each(function() {
            //console.log(options.cities);
            var activeCity = {},
                cache = {};
            $(options.citySelector).change(function() {
                var val = $.trim($(this).val());
                if (val.length < 1) {
                    activeCity = {};
                } else {
                    val = 'city' + val;
                    activeCity = (val in options.cities) ? options.cities[val] : {};
                }
            }).trigger('change');

            $(this).typeahead({
                source: function(query, process) {
                    query = (typeof (activeCity) !== 'undefined' && typeof (activeCity.name) === 'string') ? activeCity.name + ', ' + query : query;
                    //console.log(query);
                    if (query in cache) {
                        return cache[query];
                    }
                    //console.log('ajax');
                    var arrayUnique = function(a) {
                            return a.reduce(function(p, c) {
                                if (p.indexOf(c) < 0)
                                    p.push(c);
                                return p;
                            }, []);
                        },
                        data = {};
                    if (typeof (activeCity) !== 'undefined' && typeof (activeCity.ll) === 'object') {
                        data.ll = activeCity.ll[1] + ',' + activeCity.ll[0];
                        //console.log(data.ll);
                    }
                    if (typeof (activeCity) !== 'undefined' && typeof (activeCity.spn) === 'object') {
                        var str1, str2;
                        str1 = parseFloat(activeCity.spn[1][1]) - parseFloat(activeCity.spn[0][1]);
                        str2 = parseFloat(activeCity.spn[1][0]) - parseFloat(activeCity.spn[0][0]);

                        data.spn = str1 + ',' + str2;
                        //console.log(data.spn);
                    }
                    if (typeof (activeCity) !== 'undefined' && (typeof (activeCity.rspn) === 'string' || typeof (activeCity.rspn) === 'number')) {
                        data.rspn = $.trim(activeCity.rspn);
                    }
                    data = $.extend({
                        geocode: query,
                        format: 'json',
                        results: options.results,
                        kind: options.kind
                    }, data);
                    //console.log(data);
                    var el = this.$element;
                    el.addClass(options.ajaxLoaderClass);
                    $.ajax({
                        url: options.url,
                        dataType: 'jsonp',
                        data: data,
                        success: function(data) {
                            var source = [];
                            $.each(data.response.GeoObjectCollection.featureMember, function(i, address) {
                                //console.log(address.GeoObject);
                                source.push(address.GeoObject.name);
                            });
                            source = arrayUnique(source);
                            cache[query] = source;
                            //console.log(source);
                            el.removeClass(options.ajaxLoaderClass);
                            return process(source);
                        }
                    });
                },
                items: options.results
            });
        });
    };
})(jQuery);