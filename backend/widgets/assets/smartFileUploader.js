+function ($) {
    'use strict';

    // PUBLIC CLASS DEFINITION
    // ==============================

    var SmartFileUploader = function (element, options) {
        this.$element      = $(element);
        this.options       = $.extend({}, SmartFileUploader.DEFAULTS, options);
        this.$box          =
        this.$bntGroup     =
        this.$bntLoader    =
        this.$label        =
        this.$labelFromPc  =
        this.$labelFromURL =
        this.$fileInput    =
        this.$rotateLeft   =
        this.$rotateRight  =
        this.$delete       =
        this.$photoBox     = null;
        this.file          = {};
    };

    SmartFileUploader.DEFAULTS = {
        cssClassMainBox: '',
        cssClassPhotoBox: '',
        cssClassButtons: 'btn-default',
        cssClassIconUploadButton: 'fa-file',
        textMessageBoxTitle: 'Загрузка по ссылке',
        textMessageBoxContent: 'Загрузка файла по ссылке в интернете',
        textMessageBoxButtonCancel: 'Отмена',
        textMessageBoxButtonUpload: 'Загрузить',
        textMessageBoxInputValue: '',
        textMessageBoxPlaceholder: 'Введите адрес ссылки',
        textLoaderButton: 'Загрузка...',
        textUploadButton: 'Загрузить файл',
        textUploadButtonFromPc: 'Загрузить с компьютера',
        textUploadButtonFromUrl: 'Загрузить по ссылке',
        textErrorTitle: 'Ошибка',
        urlUploadFile: '',
        urlLoadFile: '',
        urlDeletePhoto: '',
        urlUploadFileFromUrl: '',
        urlRotatePhoto: ''
    };

    SmartFileUploader.prototype.init = function () {
        if(this.options.urlUploadFile.length == 0){
            console.log("options.urlUploadFile id required");
            return false;
        }
        if(this.options.urlLoadFile.length == 0){
            console.log("options.urlLoadFile id required");
            return false;
        }
        if(this.options.urlDeletePhoto.length == 0){
            console.log("options.urlDeletePhoto id required");
            return false;
        }
        this.$element.after(
            '<div class="file-uploader '+this.options.cssClassMainBox+'">' +
              '<div class="photo-box '+this.options.cssClassPhotoBox+'"></div>' +
              '<button class="btn '+this.options.cssClassButtons+' btn-loader" disabled="disabled">' +
                '<i class="fa fa-spinner fa-spin fa-fw"></i> ' +
                this.options.textLoaderButton +
              '</button>' +
              '<div class="btn-group">' +
                '<label class="btn '+this.options.cssClassButtons+'">' +
                  '<i class="fa fa-fw '+this.options.cssClassIconUploadButton+'"></i> ' +
                  this.options.textUploadButton +
                  '<input type="file" name="file" class="file-input"/>' +
                '</label>' +
                '<button class="btn dropdown-toggle '+this.options.cssClassButtons+'" data-toggle="dropdown">' +
                  '<span class="caret"></span>' +
                '</button>' +
                '<ul class="dropdown-menu">' +
                  '<li>' +
                    '<a class="from-pc" href="#">' +
                      '<i class="fa fa-fw fa-upload"></i> ' +
                      this.options.textUploadButtonFromPc +
                    '</a>' +
                  '</li>' +
                  '<li>' +
                    '<a class="from-url" href="#">' +
                      '<i class="fa fa-fw fa-link"></i> ' +
                      this.options.textUploadButtonFromUrl +
                    '</a>' +
                  '</li>' +
                '</ul>' +
              '</div>' +
            '</div>'
        );

        this.$box          = this.$element.next('div');
        this.$bntGroup     = this.$box.find('.btn-group');
        this.$bntLoader    = this.$box.find('.btn-loader');
        this.$label        = this.$box.find('label');
        this.$labelFromPc  = this.$box.find('.from-pc');
        this.$labelFromURL = this.$box.find('.from-url');
        this.$fileInput    = this.$box.find('.file-input');
        this.$photoBox     = this.$box.find('.photo-box');

        if(this.options.urlUploadFileFromUrl.length == 0){
            this.$box.find('.dropdown-toggle').hide();
        }

        this.loadFile();

        var $this = this;

        this.$labelFromPc.click(function(e){
            e.preventDefault();
            $this.$label.trigger('click');
        });

        this.$labelFromURL.click(function(e){
            e.preventDefault();
            $this.uploadFileFromURL();
        });

        this.uploadFile();
    };

    SmartFileUploader.prototype.showLoader = function() {
        this.$bntGroup.hide();
        this.$bntLoader.show();
    };

    SmartFileUploader.prototype.hideLoader = function() {
        this.$bntLoader.hide();
        this.$bntGroup.show();
    };

    SmartFileUploader.prototype.showError = function(error) {
        $.smallBox({
            title : this.options.textErrorTitle,
            content : error,
            color : "#C46A69",
            iconSmall : "fa fa-times fa-2x fadeInRight animated",
            timeout : 4000
        });
        this.hideLoader();
    };

    SmartFileUploader.prototype.loadFile = function() {
        var val = this.$element.val();
        if(val.length > 0){
            this.showLoader();
            var $this = this;
            $.ajax({
                url: this.options.urlLoadFile,
                data: {id:val},
                dataType: "json",
                success:function(file){
                    $this.pasteFile(file);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    $this.showError(errorThrown);
                }
            });
        }
    };

    SmartFileUploader.prototype.uploadFileFromURL = function() {
        var $this = this;
        $.SmartMessageBox({
            title : this.options.textMessageBoxTitle,
            content: this.options.textMessageBoxContent,
            buttons : "["+this.options.textMessageBoxButtonCancel+"]["+this.options.textMessageBoxButtonUpload+"]",
            input : "text",
            inputValue: this.options.textMessageBoxInputValue,
            placeholder : this.options.textMessageBoxPlaceholder
        }, function(button, value) {
            if(button == $this.options.textMessageBoxButtonUpload && value != "") {
                if($this.$delete) {
                    $this.$delete.trigger('click');
                }
                $this.showLoader();
                $.ajax({
                    url: $this.options.urlUploadFileFromUrl,
                    data: {url:value},
                    dataType: "json",
                    success:function(file){
                        $this.pasteFile(file);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        $this.showError(errorThrown);
                    }
                });
            }
        });
    };

    SmartFileUploader.prototype.uploadFile = function(){
        var $this = this;
        $this.$fileInput.fileupload({
            url: $this.options.urlUploadFile,
            dataType: 'json',
            change: function() {
                $this.showLoader();
                $this.$bntLoader.append('<span></span>');
                if($this.$delete) {
                    $this.$delete.trigger('click');
                }
            },
            done: function (e, data) {
                $this.$bntLoader.find('span').remove();
                $this.pasteFile(data.result);
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $this.$bntLoader.find('span').html(progress + '%');
            }
        });
    };

    SmartFileUploader.prototype.pasteFile = function(file) {
        if(file.error !== false) {
            this.showError(file.error);
            this.$element.val('');
        } else {
            this.file = file.file;
            this.$element.val(this.file.id);
            var html =
            '<span class="photo-box-wrapper">' +
                '<span class="photo-controller" data-id="'+this.file.id+'" data-code="'+this.file.code+'">';
            if(this.options.urlRotatePhoto.length > 0){
                html += '<i data-action="left" class="fa fa-rotate-left"></i>' +
                        '<i data-action="right" class="fa fa-rotate-right"></i>';
            }
            html += '<i data-action="delete" class="fa fa-times"></i>' +
                '</span>' +
                '<a href="'+this.file.src+'">' +
                    '<img src="'+this.file.src_small+'" alt="'+this.file.file_name+'">' +
                '</a>' +
            '</span>';
            this.$photoBox.html(html).show();
            this.initPhotoControllers();
        }
        this.hideLoader();
    };

    SmartFileUploader.prototype.initPhotoControllers = function() {
        var $this       = this,
            wrapper     = this.$photoBox.find('.photo-box-wrapper'),
            controllers = wrapper.find(".photo-controller"),
            link        = wrapper.find('a'),
            img         = wrapper.find('img'),
            i           = controllers.find('i');

        wrapper.hover(function(){
            controllers.slideDown();
        }, function(){
            controllers.slideUp();
        });

        link.fancybox({
            padding: 0
        });

        i.each(function(){
            switch ($(this).data('action')) {
                case 'left':
                    $this.$rotateLeft = $(this);
                    break;
                case 'right':
                    $this.$rotateRight = $(this);
                    break;
                case 'delete':
                    $this.$delete = $(this);
                    break;
            }
        });

        i.click(function(){
            link.prepend('<div class="photo-loader"></div>');
            var loader = link.find('.photo-loader');
            controllers.hide();
            switch ($(this).data('action')) {
                case 'left':
                case 'right':
                    $.ajax({
                        url: $this.options.urlRotatePhoto,
                        data: {route: $(this).data('action'), id: $this.file.id, code: $this.file.code},
                        success:function() {
                            var date = new Date().toString();
                            img.attr('src', img.attr('src') + '?' + date);
                            link.attr('href', link.attr('href') + '?' + date);
                            loader.remove();
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            $this.showError(errorThrown);
                        }
                    });
                    break;
                case 'delete':
                    $.ajax({
                        url: $this.options.urlDeletePhoto,
                        data: {id: $this.file.id, code:$this.file.code},
                        success:function() {
                            $this.file = {};
                            $this.$element.val('');
                            wrapper.remove();
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            $this.showError(errorThrown);
                        }
                    });
                    break;
            }
        });
    };


    // PLUGIN DEFINITION
    // ========================

    function Plugin(option) {
        return this.each(function () {
            var data  = new SmartFileUploader(this, option);
            data.init();
        });
    }

    var old = $.fn.SmartFileUploader;

    $.fn.SmartFileUploader             = Plugin;
    $.fn.SmartFileUploader.Constructor = SmartFileUploader;


    // NO CONFLICT
    // ==================

    $.fn.SmartFileUploader.noConflict = function () {
        $.fn.SmartFileUploader = old;
        return this
    }

}(jQuery);