import Vue from 'vue'
import Axios from 'axios';
import Tv from './NewTv';
import Radio from './Radio';

Vue.prototype.$http = Axios;
Vue.component('tv', Tv);
Vue.component('radio', Radio);
new Vue({
    el: '#app'
});
