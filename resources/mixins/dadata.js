export const daData = {
  methods: {
    /**
     * Поиск в daData
     * @param query
     * @param count
     * @param callback
     */
    search: function(query, count, callback) {
      this.$http.post(
        "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
        JSON.stringify({query: query, count: count}),
        {
          headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Token 8ad353e0bb0c3b61eee81efbd1d9abcbc98f23eb"
          }
        })
        .then(function(response) {
          callback(response);
        })
        .catch(function(response) {

        });
    }
  }
};
