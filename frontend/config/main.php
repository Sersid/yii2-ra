<?php
declare(strict_types=1);

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'НИКО-медиа',
    'sourceLanguage' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'container' => [
        'definitions' => [
            \Psr\EventDispatcher\EventDispatcherInterface::class => \NikoM\Kernel\Infrastructure\Aggregate\EventDispatcher::class,
            \Psr\EventDispatcher\ListenerProviderInterface::class => \NikoM\Kernel\Infrastructure\Aggregate\ListenerProvider::class,
            \NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderRepository::class => \NikoM\Advertising\Kernel\Infrastructure\Persistence\Order\OrderRepository::class,
            \NikoM\Advertising\Ticker\Domain\City\CityFetcher::class => \NikoM\Advertising\Ticker\Infrastructure\Persistence\City\CityFetcher::class,
            \NikoM\Advertising\Ticker\Domain\Basket\Entity\BasketRepository::class => \NikoM\Advertising\Ticker\Infrastructure\Persistence\Basket\BasketRepository::class,
            \NikoM\Location\City\Domain\Entity\ICityFetcher::class => \NikoM\Location\City\Infrastructure\CityFetcher::class,
            \NikoM\Advertising\Outdoor\Domain\Side\Entity\ISideFetcher::class => \NikoM\Advertising\Outdoor\Infrastructure\SideFetcher::class,
            \NikoM\Advertising\Outdoor\Domain\Side\Entity\ISideRepository::class => \NikoM\Advertising\Outdoor\Infrastructure\SideRepository::class,
            \NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferFetcher::class => \NikoM\Advertising\Internet\Offer\Infrastructure\OfferFetcher::class,
            \NikoM\Advertising\Internet\OurWork\Domain\ReadModel\IOurWorkFetcher::class => \NikoM\Advertising\Internet\OurWork\Infrastructure\OurWorkFetcher::class,
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_niko-media', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'niko-media',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'rules' => [
                '<controller:book>/<slug:[\w\-]+>/<node_id:\d+>' => '<controller>/view',
                '<controller:book>/<slug:[\w\-]+>' => '<controller>/view',
                '<controller:(cases|info)>/<category:\w+>' => '<controller>/index',
                '<controller:cases>/<slug>' => '<controller>/view',
                'p/<slug>' => 'page/index',
                '<controller:info>/<category:\w+>/<item>' => '<controller>/view',
                'image/<id:\d+>/<code:\w{32}+>.<ext:\w{2,10}+>' => 'file/render',
                'image/<id:\d+>/<params:\w+>/<code:\w{32}+>.<ext:\w{2,10}+>' => 'file/render',
                'photos/<img:.+>.<ext:\w{2,10}+>' => 'file/photos',
                'images/<img:.+>.<ext:\w{2,10}+>' => 'file/images',
                'outdoor-ad' => 'outdoor-ad/index',
                '<controller:(outdoor-ad|ticker)>/<action:(index|view|add-to-basket|delete|form|basket|save|calc|update)>' => '<controller>/<action>',
                '<controller:outdoor-ad>/<action:(map|map-ajax|map-content|streets|table-ajax|radius|radius-search|get-cities)>' => '<controller>/<action>',
                '<controller:outdoor-ad>/<format-slug:(reklamniy-shit-3x6|svetodiodniy-ekran|brandmauer|reklamnoe-ograjdenie)+>-<city-slug:[\w\-]+>' => '<controller>/index',
                '<controller:outdoor-ad>/<city-slug:[\w\-]+>' => '<controller>/index',
                '<controller:(tv|radio)>/<action:(index|basket|offers|add-to-basket|update-basket|update-basket-item|delete-from-basket|checkout)>' => '<controller>/<action>',
                '<controller:(tv)>/<citySlug:[\w\-]+>/<channelSlug:[\w\-\+]+>' => '<controller>/city-and-channel',
                '<controller:(tv|radio)>/<citySlug:[\w\-]+>/<offerSlug:[\w\-\+]+>' => '<controller>/index',
                '<controller:(tv|radio)>/<citySlug:[\w\-]+>' => '<controller>/index',
                '<controller:(outdoor-ad|ticker)>/<city:[\w\-]+>' => '<controller>/index',
                '<controller:(ticker)>/<city:[\w\-]+>/<tv:[\w\-\+]+>' => '<controller>/index',
                '<controller:(cities|customers)>/<slug:[\w\-]+>' => '<controller>/view',
                '<controller:(outdoor-ad)>/view/<number:[\w\-\d\.]+><side:[A-C]+>' => '<controller>/view',
                'sitemap-<sitemap:[\w\-\d]+>.xml' => 'sitemap/xml',
                'sitemap.xml' => 'sitemap/xml',
                'internet-ad/<offer:[\w\-]+>/<city:[\w\-]+>' => 'internet-advertising/offer',
                'internet-ad/city-<city:[\w\-]+>' => 'internet-advertising/index',
                'internet-ad/<offer:[\w\-]+>' => 'internet-advertising/offer',
                'internet-ad' => 'internet-advertising/index',
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
    ],
    'modules' => [
        'treemanager' => [
            'class' => '\kartik\tree\Module',
            'treeStructure' => [
                'treeAttribute' => 'root',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'lvl',
            ],
        ],
    ],
    'params' => $params,
];
