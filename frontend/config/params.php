<?php
declare(strict_types=1);

use NikoM\Advertising\AbstractMedia\Order\Domain\Event\OrderCreatedEvent;
use NikoM\Advertising\AbstractMedia\Order\Infrastructure\Listener\Bitrix24AddLeadOnCreatedOrderListener;
use NikoM\Advertising\AbstractMedia\Order\Infrastructure\Listener\SendMailOnCreatedOrderListener;

return [
    'listeners' => [
        OrderCreatedEvent::class => [
            SendMailOnCreatedOrderListener::class,
            Bitrix24AddLeadOnCreatedOrderListener::class,
        ]
    ],
];
