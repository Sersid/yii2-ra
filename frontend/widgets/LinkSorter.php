<?php

namespace frontend\widgets;


class LinkSorter extends \yii\widgets\LinkSorter
{
    protected function renderSortLinks()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        foreach ($attributes as $name) {
            $title = isset($this->sort->attributes[$name]['title']) ? $this->sort->attributes[$name]['title'] : null;
            echo '<th>';
            echo '<span class="filter-box hidden-sm hidden-xs'.(empty($title) ? '' : ' hint-item').'">';
            echo $this->sort->link($name, $this->linkOptions);
            if(!empty($title)) {
                echo '<div class="hint-text hidden-sm hidden-xs">'.$title.'</div>';
            }
            echo '</span></th>';
        }
    }
}