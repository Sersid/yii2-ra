<?php

namespace frontend\widgets;

use frontend\repositories\ReviewRepository;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class Reviews
 * @package frontend\widgets
 */
class Reviews extends Widget
{
    /** @var int */
    public $pageSize = 6;

    /** @var bool Показывать ли текст */
    public $showHeaderText = true;

    /** @var ReviewRepository */
    protected $repository;

    /**
     * @return string
     */
    public function run()
    {
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $this->repository->getListQuery(),
                'sort' => ['defaultOrder' => ['sort' => SORT_ASC, 'id' => SORT_DESC]],
                'pagination' => ['pageSize' => $this->pageSize],
            ]
        );
        return $this->view->render(
            '/widgets/reviews/reviews',
            ['dataProvider' => $dataProvider, 'showHeaderText' => $this->showHeaderText]
        );
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->repository = new ReviewRepository();
        parent::init();
    }
}
