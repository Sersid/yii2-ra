<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class FeedbackForm
 * @package frontend\widgets
 */
class FeedbackForm extends Widget
{
    /** @var string */
    public $titleCode = 'about-form-title';
    /** @var string */
    public $textCode = 'about-form-feedback-text';

    /**
     * @return string
     */
    public function run()
    {
        return $this->view->render(
            '/widgets/feedback-form/feedback-form',
            ['titleCode' => $this->titleCode, 'textCode' => $this->textCode]
        );
    }
}
