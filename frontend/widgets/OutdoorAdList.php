<?php

namespace frontend\widgets;

use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

/**
 * Class OutdoorAdList
 * @package frontend\widgets
 */
class OutdoorAdList extends ListView
{
    /**
     * Runs the widget.
     */
    public function run()
    {
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
            echo '<div class="naryw-table-block">
                        <div class="panel-group" id="accordion">
                            <table>
                                <thead>
                                <tr class="mesto-item">
                                    <th><span>№</span></th>
                                    ' . $this->renderSorter() . '
                                </tr>
                                </thead>
                                <tbody>
                                ' . $this->renderItems() . $this->renderPager() . '
                                </tbody>
                            </table>
                        </div>
                    </div>';
        } else {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'div');
            echo Html::tag($tag, $this->renderEmpty(), $options);
        }
    }

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int   $index
     *
     * @return mixed|string
     * @throws \yii\base\InvalidCallException
     * @throws \yii\base\ViewNotFoundException
     */
    public function renderItem($model, $key, $index)
    {
        if ($this->itemView === null) {
            $content = $key;
        } elseif (is_string($this->itemView)) {
            $content = $this->getView()->render(
                $this->itemView,
                array_merge(
                    [
                        'model' => $model,
                        'key' => $key,
                        'index' => $index,
                        'widget' => $this,
                    ],
                    $this->viewParams
                )
            );
        } else {
            $content = call_user_func($this->itemView, $model, $key, $index, $this);
        }

        return $content;
    }

    /**
     * @return string
     */
    public function renderPager()
    {
        return '<tr><td colspan="8" class="js-pager">' . parent::renderPager() . '</td></td></tr>';
    }
}
