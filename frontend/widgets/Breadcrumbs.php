<?php

namespace frontend\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use Yii;

class Breadcrumbs extends Widget
{
    /** @var array */
    public $links;

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function run()
    {
        $links = $this->getLinks();
        if (empty($links)) {
            return '';
        }
        $result[] = Html::beginTag('div',
            [
                'class' => 'breadcrumbs',
                'itemscope' => '',
                'itemtype' => 'http://schema.org/BreadcrumbList',
            ]);
        $result[] = Html::beginTag('ul', ['class' => 'breadcrumb']);
        foreach ($links as $i => $link) {
            $result[] = Html::beginTag('li',
                $link['active']
                    ? [
                    'itemprop' => 'itemListElement',
                    'itemscope' => '',
                    'itemtype' => 'http://schema.org/ListItem',
                    'class' => 'active',
                ]
                    : []);
            $label = Html::tag('span', $link['label'], ['itemprop' => 'name']);
            if ($link['active']) {
                $label .= '<meta itemprop="position" content="' . count($links) . '" />';
            }
            $options = $link;
            unset($options['template'], $options['label'], $options['url']);
            if ($options['active']) {
                $result[] = Html::tag('span', $label);
            } else {
                unset($options['active']);
                $result[] = Html::a($label, $link['url'], $options);
                $result[] = Html::endTag('li');
            }
        }
        $result[] = Html::endTag('ul');
        $result[] = Html::endTag('div');
        return implode('', $result);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    private function getLinks(): array
    {
        if (empty($this->links)) {
            return [];
        }
        $result[] = [
            'label' => Yii::t('yii', 'Home'),
            'url' => Yii::$app->homeUrl,
            'active' => false,
        ];
        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }
            if (!array_key_exists('label', $link)) {
                throw new InvalidConfigException('The "label" element is required for each link.');
            }
            $link['active'] = false;
            if (!isset($link['url'])) {
                $link['url'] = $_SERVER['REQUEST_URI'];
                $link['active'] = true;
            }
            $result[] = $link;
        }
        return $result;
    }
}
