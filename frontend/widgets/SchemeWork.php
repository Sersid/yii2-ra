<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class SchemeWork
 * @package frontend\widgets
 */
class SchemeWork extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->view->render('/widgets/scheme-work/scheme-work');
    }
}
