<?php
namespace frontend\widgets;

use common\models\Text as Model;
use yii\base\Component;
use yii\helpers\Url;

class Text extends Component
{
    public static function get($key, $default = null, $mode = 'text')
    {
        $editBox = '';
        $cache = \Yii::$app->cache;
        $text = $cache->get('text-'.$key);
        if ($text === false) {
            $text = Model::findOne(['key' => $key]);
            if($text !== null) {
                $text = $text->value;
                $cache->set('text-'.$key, $text);
            }
        }
        if(empty($text)) {
            $text = $default;
        } else {
            $default = '';
        }

        if(\Yii::$app->session->get('edit-mode')) {
            $editBox = '<span class="edit-box-widget" data-url="'.Url::toRoute([
                    '/edit-mode/text',
                    'key' => $key,
                    'mode' => $mode,
                    'default' => $default,
                ]).'"><button data-toggle="modal" data-target="#edit-text" type="button" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></span>';
        }
        return $text.$editBox;
    }
}