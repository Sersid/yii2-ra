<?php

namespace frontend\widgets;

class Html extends \yii\helpers\Html
{
    /**
     * Форматирование цены
     *
     * @param $price
     *
     * @return string
     */
    public static function priceFormatted($price)
    {
        $price = floatval($price);
        $decimals = count(explode('.', $price)) == 2 ? 2 : 0;

        return number_format($price, $decimals, '.', ' ');
    }

    public static function fistSpaceToBr(string $string): string
    {
        $parts = explode(' ', $string);
        if (count($parts) > 1) {
            $parts[0] .= '<br />';
        }

        return implode(' ', $parts);
    }

    public static function autoParagraph(string $text): string
    {
        $text = str_replace("\r\n", "\n", $text);
        $paragraphs = preg_split("/[\n]{2,}/", $text);
        foreach ($paragraphs as $key => $p) {
            $paragraphs[$key] = '<p>' . str_replace("\n", '<br />', $paragraphs[$key]) . '</p>';
        }

        return implode('', $paragraphs);
    }
}
