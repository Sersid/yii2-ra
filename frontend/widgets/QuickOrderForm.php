<?php

namespace frontend\widgets;

use common\models\QuickOutdoorOrder;
use yii\base\Widget;

/**
 * Class FeedbackForm
 * @package frontend\widgets
 */
class QuickOrderForm extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $model = new QuickOutdoorOrder();
        $model->model_type = '';
        return $this->view->render('/widgets/quick-order-form/quick-order-form', ['model' => $model]);
    }
}
