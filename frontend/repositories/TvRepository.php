<?php

namespace frontend\repositories;

use common\models\Tv as Model;

/**
 * Class TvRepository
 * @package frontend\repositories
 */
final class TvRepository extends \common\repositories\TvRepository
{
    /**
     * @param string $slug
     *
     * @return array|Model|null
     */
    public function getBySlug(string $slug)
    {
        if (empty($slug)) {
            return null;
        }
        return $this->query()->where(['slug' => $slug])->one();
    }
}
