<?php

namespace frontend\repositories;

use common\models\Customer as Model;

/**
 * Class CustomerRepository
 * @package frontend\repositories
 */
final class CustomerRepository extends \common\repositories\CustomerRepository
{
    /**
     * @return array|Model[]
     */
    public function getAll(): array
    {
        return $this->query()
            ->where(['customer.status' => Model::STATUS_ACTIVE])
            ->joinWith('image')
            ->orderBy('sort')
            ->all();
    }
}
