<?php

namespace frontend\repositories;

use common\models\InfoCategory as Model;

/**
 * Class InfoCategoryRepository
 * @package frontend\repositories
 */
final class InfoCategoryRepository extends \common\repositories\InfoCategoryRepository
{
    /**
     * @return array|Model[]
     */
    public function getForMenu(): array
    {
        return $this->query()
            ->select(['name', 'slug'])
            ->orderBy('sort')
            ->where(['status' => Model::STATUS_ACTIVE])
            ->all();
    }
}
