<?php

namespace frontend\repositories;

use Exception;
use frontend\models\City as Model;
use yii\db\ActiveRecord;

/**
 * Class CityRepository
 * @package frontend\repositories
 */
final class CityRepository extends \common\repositories\CityRepository
{
    /**
     * Возвращает крупные города
     * @return array
     */
    public function getLargeList(): array
    {
        return $this->query()
            ->select(['id', 'slug', 'name', 'image_id', 'salary', 'population', 'density'])
            ->orderBy(['sort' => SORT_ASC, 'salary' => SORT_DESC])
            ->with('image')
            ->where(['is_large_city' => Model::CITY_IS_LARGE, 'status' => Model::STATUS_ACTIVE])
            ->all();
    }

    /**
     * Возвращает мелкие города
     * @return array
     */
    public function getSmallList(): array
    {
        return $this->query()
            ->orderBy(['region.sort' => SORT_ASC, 'city.sort' => SORT_ASC, 'salary' => SORT_DESC])
            ->joinWith('region')
            ->where(['is_large_city' => Model::CITY_IS_SMALL, 'city.status' => Model::STATUS_ACTIVE])
            ->andWhere(['IS NOT', 'region_id', null])
            ->all();
    }

    /**
     * @param string|null $slug
     *
     * @return ActiveRecord|Model|null
     * @throws Exception
     */
    public function getBySlug(?string $slug)
    {
        if (empty($slug)) {
            return null;
        }
        $city = $this->query()
            ->where(['slug' => $slug, 'status' => Model::STATUS_ACTIVE])
            ->one();
        if (empty($city)) {
            throw new Exception('City not found', 404);
        }
        return $city;
    }

    /**
     * @param int $id
     *
     * @return ActiveRecord|Model|null
     */
    public function getById(int $id)
    {
        if (empty($id)) {
            return null;
        }
        return $this->query()
            ->where(['id' => $id, 'status' => Model::STATUS_ACTIVE])
            ->one();
    }
}
