<?php
declare(strict_types=1);

namespace frontend\repositories;

use common\models\Tv as Model;

final class RadioRepository extends \common\repositories\RadioRepository
{
    /**
     * @param string $slug
     *
     * @return array|Model|null
     */
    public function getBySlug(string $slug)
    {
        if (empty($slug)) {
            return null;
        }
        return $this->query()->where(['slug' => $slug])->one();
    }
}
