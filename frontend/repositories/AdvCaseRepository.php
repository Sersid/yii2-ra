<?php

namespace frontend\repositories;

use frontend\models\AdvCase as Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class AdvCaseRepository
 * @package frontend\repositories
 */
final class AdvCaseRepository extends \common\repositories\AdvCaseRepository
{
    /**
     * @return ActiveQuery
     */
    public function getMainListQuery()
    {
        return $this->query()
            ->select(['slug', 'name', 'short_desc', 'image_id'])
            ->with('image')
            ->where(['status' => Model::STATUS_ACTIVE]);
    }

    /**
     * @param string $slug
     *
     * @return array|ActiveRecord[]
     */
    public function getMoreCases(string $slug): array
    {
        if (empty($slug)) {
            return [];
        }
        return $this->query()
            ->select(['name', 'slug'])
            ->where(['status' => Model::STATUS_ACTIVE])
            ->andWhere(['<>', 'slug', $slug])
            ->orderBy(['id' => SORT_DESC])
            ->limit(5)
            ->all();
    }

    /**
     * @param string $slug
     *
     * @return array|ActiveRecord|null
     */
    public function getBySlug(string $slug)
    {
        if (empty($slug)) {
            return null;
        }
        return $this->query()
            ->where(['slug' => $slug, 'status' => Model::STATUS_ACTIVE])
            ->one();
    }

    /**
     * @param array $categories
     *
     * @return array|ActiveRecord[]
     */
    public  function getByCategories(array $categories): array
    {
        return $this->query()
            ->select(['slug', 'name', 'short_desc', 'image_id'])
            ->with('image')
            ->where(['category' => $categories, 'status' => Model::STATUS_ACTIVE])
            ->orderBy(['id' => SORT_DESC])
            ->limit(4)
            ->all();
    }
}
