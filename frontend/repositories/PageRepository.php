<?php

namespace frontend\repositories;

use common\models\Page as Model;
use yii\db\ActiveRecord;

/**
 * Class PageRepository
 * @package frontend\repositories
 */
final class PageRepository extends \common\repositories\PageRepository
{
    /**
     * @param string|null $slug
     *
     * @return array|Model|ActiveRecord|null
     */
    public function getBySlug(?string $slug)
    {
        if (empty($slug)) {
            return null;
        }
        return $this->query()
            ->where(['slug' => $slug, 'status' => Model::STATUS_ACTIVE])
            ->one();
    }
}
