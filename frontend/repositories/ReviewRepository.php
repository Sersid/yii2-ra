<?php

namespace frontend\repositories;

use common\models\Review as Model;
use yii\db\ActiveQuery;

/**
 * Class ReviewRepository
 * @package frontend\repositories
 */
final class ReviewRepository extends \common\repositories\ReviewRepository
{
    /**
     * @return ActiveQuery
     */
    public function getListQuery()
    {
        return $this->query()
            ->where([$this->tableName() . '.status' => Model::STATUS_ACTIVE])
            ->andWhere(['IS NOT', 'image_id', null])
            ->joinWith('image');
    }
}
