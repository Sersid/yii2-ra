<?php

namespace frontend\repositories;

use common\models\Info as Model;
use yii\db\ActiveQuery;

/**
 * Class InfoRepository
 * @package frontend\repositories
 */
final class InfoRepository extends \common\repositories\InfoRepository
{
    /**
     * @param string|null $category
     *
     * @return ActiveQuery
     */
    public function getListQuery(?string $category)
    {
        $query = $this->query()
            ->joinWith('category')
            ->joinWith('image')
            ->where(['info.status' => Model::STATUS_ACTIVE]);
        if (!empty($category)) {
            $query->where(['info_category.slug' => $category]);
        }
        return $query;
    }

    /**
     * @param string|null $slug
     *
     * @return array|Model|null
     */
    public function getBySlug(?string $slug)
    {
        if (empty($slug)) {
            return null;
        }
        return $this->query()
            ->where(['slug' => $slug, 'status' => Model::STATUS_ACTIVE])
            ->one();
    }
}
