<?php

namespace frontend\repositories;

use common\models\RadioAd as Model;

/**
 * Class RadioAdRepository
 * @package frontend\repositories
 */
final class RadioAdRepository extends \common\repositories\RadioAdRepository
{
    /**
     * Дата последнего обновления
     * @return string|null
     */
    public function getLastUpdated(): ?string
    {
        $last = $this->query()
            ->select('updated_at')
            ->where(['status' => Model::STATUS_ACTIVE])
            ->orderBy(['updated_at' => SORT_DESC])
            ->one();
        return empty($last) ? null : $last->updated_at;
    }

    /**
     * Возвращает кол-во в городах
     * @return array
     */
    public function getCountsInCities(): array
    {
        $counts = $this->query()
            ->select(['cnt' => 'count(city_id)', 'city_id'])
            ->where(['status' => Model::STATUS_ACTIVE])
            ->groupBy('city_id')
            ->asArray()
            ->all();
        $result = [];
        foreach ($counts as $count) {
            $result[$count['city_id']] = $count['cnt'];
        }
        return $result;
    }

    /**
     * Возвращает кол-во в городе
     *
     * @param int|null $city_id
     *
     * @return int
     */
    public function getCountInCity(?int $city_id): int
    {
        if (empty($city_id)) {
            return 0;
        }
        return $this->query()->where(['city_id' => $city_id, 'status' => Model::STATUS_ACTIVE])->count();
    }
}
