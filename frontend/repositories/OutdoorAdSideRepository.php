<?php

namespace frontend\repositories;

use common\models\OutdoorAd;
use common\models\OutdoorAdSide as Model;

/**
 * Class OutdoorAdSideRepository
 * @package frontend\repositories
 */
final class OutdoorAdSideRepository extends \common\repositories\OutdoorAdSideRepository
{
    /**
     * Возвращает кол-во в городах
     * @return array
     */
    public function getCountsInCities(): array
    {
        $items = $this->query()
            ->select(['parent_id', 'cnt' => 'count(city_id)', 'city_id'])
            ->joinWith('parent')
            ->where(['outdoor_ad_side.status' => Model::STATUS_ENABLE])
            ->groupBy('city_id')
            ->asArray()
            ->all();
        $result = [];
        foreach ($items as $item) {
            $result[$item['parent']['city_id']] = $item['cnt'];
        }
        return $result;
    }

    /**
     * Возвращает кол-во в городе
     *
     * @param int|null $city_id
     *
     * @return int
     */
    public function getCountInCity(?int $city_id): int
    {
        if (empty($city_id)) {
            return 0;
        }
        return $this->query()
            ->where(['city_id' => $city_id, Model::tableName() . '.status' => Model::STATUS_ENABLE])
            ->joinWith('parent')
            ->count();
    }

    /**
     * Возвращает кол-во светодиодных экранов в городе
     *
     * @param int|null $city_id
     *
     * @return int
     */
    public function getCountLedDisplaysInCity(?int $city_id): int
    {
        if (empty($city_id)) {
            return 0;
        }
        return $this->query()
            ->where([
                'city_id' => $city_id,
                Model::tableName() . '.status' => Model::STATUS_ENABLE,
                'outdoor_ad.format' => OutdoorAd::FORMAT_LED_DISPLAYS,
            ])
            ->joinWith('parent')
            ->count();
    }

    /**
     * Возвращает кол-во светодиодных экранов в городе
     *
     * @param int|null $city_id
     *
     * @return int
     */
    public function getCountFirewallsInCity(?int $city_id): int
    {
        if (empty($city_id)) {
            return 0;
        }
        return $this->query()
            ->where([
                'city_id' => $city_id,
                Model::tableName() . '.status' => Model::STATUS_ENABLE,
                'outdoor_ad.format' => OutdoorAd::FORMAT_FIREWALLS,
            ])
            ->joinWith('parent')
            ->count();
    }
}
