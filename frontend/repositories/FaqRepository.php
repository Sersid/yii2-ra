<?php

namespace frontend\repositories;

use common\models\Faq as Model;

/**
 * Class FaqRepository
 * @package frontend\repositories
 */
final class FaqRepository extends \common\repositories\FaqRepository
{
    /**
     * @return array|Model[]
     */
    public function getAll(): array
    {
        return $this->query()
            ->where(['status' => Model::STATUS_ACTIVE])
            ->orderBy('sort')
            ->all();
    }
}
