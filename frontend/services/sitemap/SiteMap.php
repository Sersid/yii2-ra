<?php

namespace frontend\services\sitemap;

use ErrorException;
use frontend\services\sitemap\interfaces\SiteMapInterface;
use frontend\services\sitemap\sitemaps;

/**
 * Class SiteMap
 * @package frontend\services\sitemap
 */
class SiteMap
{
    /** @var array */
    protected $siteMaps = [
        'outdoor-ad' => sitemaps\OutdoorAdSiteMap::class,
        'cities' => sitemaps\CitiesSiteMap::class,
        'cities-outdoor-ad' => sitemaps\CitiesOutdoorAdSiteMap::class,
        'cities-tv' => sitemaps\CitiesTvSiteMap::class,
        'cities-ticker' => sitemaps\CitiesTickerSiteMap::class,
        'cities-radio' => sitemaps\CitiesRadioSiteMap::class,
        'cases' => sitemaps\CasesSiteMap::class,
        'pages' => sitemaps\PagesSiteMap::class,
        'menu' => sitemaps\MenuSiteMap::class,
    ];

    /** @var SiteMapInterface */
    protected $generator;

    /**
     * @param string $sitemap
     *
     * @throws ErrorException
     */
    public function __construct(string $sitemap)
    {
        if (!empty($sitemap)) {
            if (!isset($this->siteMaps[$sitemap])) {
                throw new ErrorException('SiteMap ' . $sitemap . ' not found');
            }
            $this->generator = new SiteMapGenerator(new $this->siteMaps[$sitemap]);
        } else {
            $this->generator = new SiteMapIndexGenerator($this->siteMaps);
        }
    }

    /**
     * @inheritDoc
     */
    public function build(): string
    {
        return $this->generator->build();
    }
}
