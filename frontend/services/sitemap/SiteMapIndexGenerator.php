<?php

namespace frontend\services\sitemap;

use frontend\services\sitemap\interfaces\GeneratorInterface;
use frontend\services\sitemap\interfaces\SiteMapInterface;
use yii\helpers\Url;

/**
 * Class SiteMapIndexGenerator
 * @package Services\SiteMap\Generators
 */
class SiteMapIndexGenerator implements GeneratorInterface
{
    /** @var array */
    protected $siteMaps;

    /**
     * @param array $siteMaps
     */
    public function __construct(array $siteMaps)
    {
        $this->siteMaps = $siteMaps;
    }

    /**
     * @return string
     */
    public function build(): string
    {
        $result = '<?xml version="1.0" encoding="UTF-8"?>';
        $result .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($this->siteMaps as $siteMap) {
            $result .= $this->getSiteMap(new $siteMap);
        }
        $result .= '</sitemapindex>';
        return $result;
    }

    /**
     * @param SiteMapInterface $siteMap
     *
     * @return string
     */
    private function getSiteMap(SiteMapInterface $siteMap): string
    {
        $lastMod = $siteMap->getLastMod();
        $result = '<sitemap>';
        $result .= '<loc>' . Url::to($siteMap->getLoc(), true) . '</loc>';
        $result .= empty($lastMod) ? '' : '<lastmod>' . $lastMod . '</lastmod>';
        $result .= '</sitemap>';
        return $result;
    }
}
