<?php

namespace frontend\services\sitemap\repositories;

use frontend\repositories\TvAdRepository;
use frontend\services\sitemap\urls\CityTv;

/**
 * Class CityTvRepository
 * @package frontend\services\sitemap\repositories
 */
class CityTvRepository extends CityRepository
{
    /**
     * @inheritDoc
     */
    public function getLastUpdated(): ?string
    {
        $repository = new TvAdRepository();
        return $repository->getLastUpdated();
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return CityTv::class;
    }
}
