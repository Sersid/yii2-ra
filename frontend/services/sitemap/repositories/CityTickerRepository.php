<?php

namespace frontend\services\sitemap\repositories;

use frontend\repositories\TickerRepository;
use frontend\services\sitemap\urls\CityTicker;

/**
 * Class CityTickerRepository
 * @package frontend\services\sitemap\repositories
 */
class CityTickerRepository extends CityRepository
{
    /**
     * @inheritDoc
     */
    public function getLastUpdated(): ?string
    {
        $repository = new TickerRepository();
        return $repository->getLastUpdated();
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return CityTicker::class;
    }
}
