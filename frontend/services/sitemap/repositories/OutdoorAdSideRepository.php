<?php

namespace frontend\services\sitemap\repositories;

use common\repositories\BaseRepository;
use frontend\services\sitemap\interfaces\RepositoryInterface;
use frontend\services\sitemap\urls\OutdoorAdSide as Model;
use yii\db\ActiveQuery;

/**
 * Class OutdoorAdSideRepository
 * @package frontend\services\sitemap\repositories
 */
class OutdoorAdSideRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @return array
     */
    public function getUrls(): array
    {
        return $this->query()
            ->select(['id', 'updated_at', 'side', 'parent_id'])
            ->with([
                'parent' => function (ActiveQuery $query) {
                    $query->select(['id', 'full_number']);
                },
            ])
            ->where(['status' => Model::STATUS_ENABLE])
            ->all();
    }

    /**
     * @inheritDoc
     */
    public function getLastUpdated(): ?string
    {
        static $last;
        if (is_null($last)) {
            $last = $this->query()
                ->select('updated_at')
                ->where(['status' => Model::STATUS_ENABLE])
                ->orderBy(['updated_at' => SORT_DESC])
                ->one();
        }
        return empty($last) ? null : $last->updated_at;
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
