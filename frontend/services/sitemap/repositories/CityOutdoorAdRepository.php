<?php

namespace frontend\services\sitemap\repositories;

use frontend\services\sitemap\urls\CityOutdoorAd;

/**
 * Class CitiesOutdoorAdRepository
 * @package frontend\services\sitemap\repositories
 */
class CityOutdoorAdRepository extends CityRepository
{
    /**
     * @inheritDoc
     */
    public function getLastUpdated(): ?string
    {
        $outdoorRepository = new OutdoorAdSideRepository();
        return $outdoorRepository->getLastUpdated();
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return CityOutdoorAd::class;
    }
}
