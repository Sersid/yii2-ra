<?php

namespace frontend\services\sitemap\repositories;

use frontend\repositories\RadioAdRepository;
use frontend\services\sitemap\urls\CityRadio;

/**
 * Class CityRadioRepository
 * @package frontend\services\sitemap\repositories
 */
class CityRadioRepository extends CityRepository
{
    /**
     * @inheritDoc
     */
    public function getLastUpdated(): ?string
    {
        $repository = new RadioAdRepository();
        return $repository->getLastUpdated();
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return CityRadio::class;
    }
}
