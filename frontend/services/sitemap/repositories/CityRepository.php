<?php

namespace frontend\services\sitemap\repositories;

use common\repositories\BaseRepository;
use frontend\services\sitemap\interfaces\RepositoryInterface;
use frontend\services\sitemap\urls\City as Model;

/**
 * Class CityRepository
 * @package frontend\services\sitemap\repositories
 */
class CityRepository extends BaseRepository implements RepositoryInterface
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @inheritDoc
     */
    public function getUrls(): array
    {
        return $this->query()
            ->select(['updated_at', 'slug'])
            ->where(['status' => Model::STATUS_ACTIVE])
            ->all();
    }

    /**
     * @inheritDoc
     */
    public function getLastUpdated(): ?string
    {
        $last = $this->query()
            ->select('updated_at')
            ->where(['status' => Model::STATUS_ACTIVE])
            ->orderBy(['updated_at' => SORT_DESC])
            ->one();
        return empty($last) ? null : $last->updated_at;
    }
}
