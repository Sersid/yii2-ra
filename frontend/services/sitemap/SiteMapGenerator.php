<?php

namespace frontend\services\sitemap;


use frontend\services\sitemap\interfaces\GeneratorInterface;
use frontend\services\sitemap\interfaces\SiteMapInterface;
use frontend\services\sitemap\interfaces\UrlInterface;
use yii\helpers\Url;

/**
 * Class SiteMapGenerator
 * @package Services\SiteMap\Generators
 */
class SiteMapGenerator implements GeneratorInterface
{
    const CHANGE_FREQ_ALWAYS = 'always';
    const CHANGE_FREQ_HOURLY = 'hourly';
    const CHANGE_FREQ_DAILY = 'daily';
    const CHANGE_FREQ_WEEKLY = 'weekly';
    const CHANGE_FREQ_MONTHLY = 'monthly';
    const CHANGE_FREQ_YEARLY = 'yearly';
    const CHANGE_FREQ_NEVER = 'never';

    /** @var SiteMapInterface */
    protected $siteMap;

    /**
     * @param SiteMapInterface $siteMap
     */
    public function __construct(SiteMapInterface $siteMap)
    {
        $this->siteMap = $siteMap;
    }

    /**
     * @inheritDoc
     */
    public function build(): string
    {
        $result = '<?xml version="1.0" encoding="UTF-8"?>';
        $result .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($this->siteMap->getUrls() as $url) {
            $result .= $this->getUrl($url);
        }
        $result .= '</urlset>';
        return $result;
    }

    /**
     * @param UrlInterface $url
     *
     * @return string
     */
    private function getUrl(UrlInterface $url): string
    {
        $loc = $this->getLoc($url);
        if (empty($loc)) {
            return '';
        }
        $result = '<url>';
        $result .= $loc;
        $result .= $this->getLastMod($url);
        $result .= $this->getChangeFreq($url);
        $result .= $this->getPriority($url);
        $result .= '</url>';
        return $result;
    }

    /**
     * @param UrlInterface $url
     *
     * @return string
     */
    private function getLoc(UrlInterface $url): string
    {
        $loc = $url->getLoc();
        return empty($loc) ? '' : '<loc>' . Url::to($loc, true) . '</loc>';
    }

    /**
     * @param UrlInterface $url
     *
     * @return string
     */
    private function getLastMod(UrlInterface $url): string
    {
        $lastMod = $url->getLastMod();
        return empty($lastMod) ? '' : '<lastmod>' . $lastMod . '</lastmod>';
    }

    /**
     * @param UrlInterface $url
     *
     * @return string
     */
    private function getChangeFreq(UrlInterface $url): string
    {
        $changeFreq = $url->getChangeFreq();
        if (in_array(
            $changeFreq,
            [
                self::CHANGE_FREQ_ALWAYS,
                self::CHANGE_FREQ_HOURLY,
                self::CHANGE_FREQ_DAILY,
                self::CHANGE_FREQ_WEEKLY,
                self::CHANGE_FREQ_MONTHLY,
                self::CHANGE_FREQ_YEARLY,
                self::CHANGE_FREQ_NEVER,
            ]
        )) {
            return '<changefreq>' . $changeFreq . '</changefreq>';
        }
        return '';
    }

    /**
     * @param UrlInterface $url
     *
     * @return string
     */
    private function getPriority(UrlInterface $url): string
    {
        $priority = $url->getPriority();
        return ($priority >= 0 && $priority <= 1) ? '<priority>' . $priority . '</priority>' : '';
    }
}
