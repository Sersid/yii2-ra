<?php

namespace frontend\services\sitemap\traits;

trait DateTrait
{
    /**
     * @param int $timestamp
     *
     * @return string
     */
    public function getDate(int $timestamp)
    {
        return empty($timestamp) ? '' : date('c', $timestamp);
    }
}
