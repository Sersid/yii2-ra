<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\CityOutdoorAdRepository;

/**
 * Class CitiesOutdoorAdSiteMap
 * @package frontend\services\sitemap\sitemaps
 */
class CitiesOutdoorAdSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-cities-outdoor-ad.xml';
    }

    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return CityOutdoorAdRepository::class;
    }
}
