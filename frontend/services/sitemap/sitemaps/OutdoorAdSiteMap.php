<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\OutdoorAdSideRepository;

/**
 * Class OutdoorAdSiteMap
 * @property OutdoorAdSideRepository $repository
 * @package frontend\services\sitemap\sitemaps
 */
class OutdoorAdSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-outdoor-ad.xml';
    }

    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return OutdoorAdSideRepository::class;
    }
}
