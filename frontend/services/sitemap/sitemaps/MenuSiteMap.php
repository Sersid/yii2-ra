<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\interfaces\SiteMapInterface;
use frontend\services\sitemap\urls\Url;
use frontend\services\sitemap\SiteMapGenerator;

/**
 * Class MenuSiteMap
 * @package frontend\services\sitemap\sitemaps
 */
class MenuSiteMap implements SiteMapInterface
{
    /**
     * @inheritDoc
     */
    public function getUrls(): array
    {
        return [
            new Url('/outdoor-ad', '', SiteMapGenerator::CHANGE_FREQ_DAILY, 1),
            new Url('/tv', '', SiteMapGenerator::CHANGE_FREQ_DAILY, 1),
            new Url('/ticker', '', SiteMapGenerator::CHANGE_FREQ_DAILY, 1),
            new Url('/radio', '', SiteMapGenerator::CHANGE_FREQ_DAILY, 1),
            new Url('/contacts', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/about', '', SiteMapGenerator::CHANGE_FREQ_YEARLY, 0.5),
            new Url('/cases', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/reviews', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/example', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/faq', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/cities', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/info', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/book', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
            new Url('/customers', '', SiteMapGenerator::CHANGE_FREQ_WEEKLY, 0.5),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getLastMod(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-menu.xml';
    }
}
