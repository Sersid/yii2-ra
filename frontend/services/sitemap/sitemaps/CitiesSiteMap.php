<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\CityRepository;

/**
 * Class CitiesSiteMap
 * @package frontend\services\sitemap\sitemaps
 */
class CitiesSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return CityRepository::class;
    }

    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-cities.xml';
    }
}
