<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\CityTickerRepository;

/**
 * Class CitiesTickerSiteMap
 * @package frontend\services\sitemap\sitemaps
 */
class CitiesTickerSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-cities-ticker.xml';
    }

    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return CityTickerRepository::class;
    }
}
