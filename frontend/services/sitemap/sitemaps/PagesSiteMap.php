<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\PageRepository;

/**
 * Class PagesSiteMap
 * @property PageRepository $repository
 * @package frontend\services\sitemap\sitemaps
 */
class PagesSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-pages.xml';
    }

    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return PageRepository::class;
    }
}
