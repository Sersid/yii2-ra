<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\CityTvRepository;

/**
 * Class CitiesTvSiteMap
 * @package frontend\services\sitemap\sitemaps
 */
class CitiesTvSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-cities-tv.xml';
    }

    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return CityTvRepository::class;
    }
}
