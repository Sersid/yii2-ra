<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\CityRadioRepository;

/**
 * Class CitiesRadioSiteMap
 * @package frontend\services\sitemap\sitemaps
 */
class CitiesRadioSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-cities-radio.xml';
    }

    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return CityRadioRepository::class;
    }
}
