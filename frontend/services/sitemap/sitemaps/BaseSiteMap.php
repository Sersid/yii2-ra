<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\interfaces\RepositoryInterface;
use frontend\services\sitemap\interfaces\SiteMapInterface;
use frontend\services\sitemap\traits\DateTrait;

/**
 * Class BaseSiteMap
 * @package frontend\services\sitemap\sitemaps
 */
abstract class BaseSiteMap implements SiteMapInterface
{
    use DateTrait;

    /** @var RepositoryInterface */
    protected $repository;

    /**
     * BaseSiteMap constructor.
     */
    public function __construct()
    {
        $repositoryClass = $this->getRepositoryClass();
        $this->repository = new $repositoryClass();
    }

    /**
     * @return string
     */
    abstract protected function getRepositoryClass(): string;

    /**
     * @inheritDoc
     */
    public function getLastMod(): string
    {
        $lastUpdatedAt = $this->repository->getLastUpdated();
        return empty($lastUpdatedAt) ? '' : $this->getDate($lastUpdatedAt);
    }

    /**
     * @return array
     */
    public function getUrls(): array
    {
        return $this->repository->getUrls();
    }
}
