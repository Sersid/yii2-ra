<?php

namespace frontend\services\sitemap\sitemaps;

use frontend\services\sitemap\repositories\AdvCaseRepository;

/**
 * Class CasesSiteMap
 * @property AdvCaseRepository $repository
 * @package frontend\services\sitemap\sitemaps
 */
class CasesSiteMap extends BaseSiteMap
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/sitemap-cases.xml';
    }

    /**
     * @inheritDoc
     */
    protected function getRepositoryClass(): string
    {
        return AdvCaseRepository::class;
    }
}
