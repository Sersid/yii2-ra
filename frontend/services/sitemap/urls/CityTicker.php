<?php

namespace frontend\services\sitemap\urls;

use frontend\services\sitemap\SiteMapGenerator;

/**
 * Class CityTicker
 * @package frontend\services\sitemap\urls
 */
class CityTicker extends City
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return $this->getTickerPageUrl();
    }

    /**
     * @inheritDoc
     */
    public function getPriority(): float
    {
        return 1;
    }

    /**
     * @inheritDoc
     */
    public function getChangeFreq(): string
    {
        return SiteMapGenerator::CHANGE_FREQ_DAILY;
    }

    /**
     * @inheritDoc
     */
    public function getLastMod(): string
    {
        return '';
    }
}
