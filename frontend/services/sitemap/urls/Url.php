<?php

namespace frontend\services\sitemap\urls;

use frontend\services\sitemap\interfaces\UrlInterface;

/**
 * Class Menu
 * @package frontend\services\sitemap\models
 */
class Url implements UrlInterface
{
    /** @var string */
    protected $loc;
    /** @var string */
    protected $lastMod;
    /** @var string */
    protected $changeFreq;
    /** @var float */
    protected $priority;

    /**
     * Menu constructor.
     *
     * @param string $loc
     * @param string $lastMod
     * @param string $changeFreq
     * @param float  $priority
     */
    public function __construct(string $loc, string $lastMod, string $changeFreq, float $priority)
    {
        $this->loc = $loc;
        $this->lastMod = $lastMod;
        $this->changeFreq = $changeFreq;
        $this->priority = $priority;
    }

    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return $this->loc;
    }

    /**
     * @inheritDoc
     */
    public function getLastMod(): string
    {
        return $this->lastMod;
    }

    /**
     * @inheritDoc
     */
    public function getChangeFreq(): string
    {
        return $this->changeFreq;
    }

    /**
     * @inheritDoc
     */
    public function getPriority(): float
    {
        return $this->priority;
    }
}
