<?php

namespace frontend\services\sitemap\urls;

use frontend\services\sitemap\interfaces\UrlInterface;
use frontend\services\sitemap\SiteMapGenerator;
use frontend\services\sitemap\traits\DateTrait;

class Page extends \common\models\Page implements UrlInterface
{
    use DateTrait;
    
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return '/p/' . $this->slug;
    }

    /**
     * @inheritDoc
     */
    public function getLastMod(): string
    {
        return $this->getDate($this->updated_at);
    }

    /**
     * @inheritDoc
     */
    public function getChangeFreq(): string
    {
        return SiteMapGenerator::CHANGE_FREQ_MONTHLY;
    }

    /**
     * @inheritDoc
     */
    public function getPriority(): float
    {
        return 0.5;
    }
}
