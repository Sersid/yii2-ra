<?php

namespace frontend\services\sitemap\urls;

use frontend\services\sitemap\interfaces\UrlInterface;
use frontend\services\sitemap\SiteMapGenerator;
use frontend\services\sitemap\traits\DateTrait;

class OutdoorAdSide extends \common\models\OutdoorAdSide implements UrlInterface
{
    use DateTrait;

    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return $this->getDetailPageUrl();
    }

    /**
     * @inheritDoc
     */
    public function getLastMod(): string
    {
        return $this->getDate($this->updated_at);
    }

    /**
     * @inheritDoc
     */
    public function getChangeFreq(): string
    {
        return SiteMapGenerator::CHANGE_FREQ_DAILY;
    }

    /**
     * @inheritDoc
     */
    public function getPriority(): float
    {
        return 1;
    }
}
