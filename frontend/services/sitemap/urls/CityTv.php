<?php

namespace frontend\services\sitemap\urls;

use frontend\services\sitemap\SiteMapGenerator;

/**
 * Class CityTv
 * @package frontend\services\sitemap\urls
 */
class CityTv extends City
{
    /**
     * @inheritDoc
     */
    public function getLoc(): string
    {
        return $this->getTvPageUrl();
    }

    /**
     * @inheritDoc
     */
    public function getPriority(): float
    {
        return 1;
    }

    /**
     * @inheritDoc
     */
    public function getChangeFreq(): string
    {
        return SiteMapGenerator::CHANGE_FREQ_DAILY;
    }

    /**
     * @inheritDoc
     */
    public function getLastMod(): string
    {
        return '';
    }
}
