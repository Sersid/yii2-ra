<?php

namespace frontend\services\sitemap\interfaces;

/**
 * Interface RepositoryInterface
 * @package frontend\services\sitemap\interfaces
 */
interface RepositoryInterface
{
    /**
     * @return array
     */
    public function getUrls(): array;

    /**
     * @return string|null
     */
    public function getLastUpdated(): ?string;
}
