<?php

namespace frontend\services\sitemap\interfaces;
/**
 * Interface CollectionInterface
 */
interface SiteMapInterface
{
    /**
     * @return UrlInterface[]
     */
    public function getUrls(): array;

    /**
     * @return string
     */
    public function getLastMod(): string;

    /**
     * @return string
     */
    public function getLoc(): string;
}
