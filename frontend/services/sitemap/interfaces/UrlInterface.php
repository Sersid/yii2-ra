<?php

namespace frontend\services\sitemap\interfaces;

/**
 * Interface UrlInterface
 */
interface UrlInterface
{
    /**
     * @return string
     */
    public function getLoc(): string;

    /**
     * @return string
     */
    public function getLastMod(): string;

    /**
     * @return string
     */
    public function getChangeFreq(): string;

    /**
     * @return float
     */
    public function getPriority(): float;
}
