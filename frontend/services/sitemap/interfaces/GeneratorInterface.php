<?php

namespace frontend\services\sitemap\interfaces;

/**
 * Interface GeneratorInterface
 * @package Services\SiteMap\Interfaces
 */
interface GeneratorInterface
{
    /**
     * Generator builder
     * @return string
     */
    public function build(): string;
}
