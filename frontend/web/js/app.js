$(function() {
    // стилизованные селекты
    const сhangeText = (el, text) => {
        el.textContent = text;
    };

    const removeAllTooltips = (dropdownList) => {
        const existedTooltips = dropdownList.querySelectorAll(`.select-tooltip`);
        [...existedTooltips].forEach((it) => {
            it.remove();
        });
    };

    const closeMultiSelect = (select) => {
        select._el.classList.remove(`multi-select--open`);
        removeAllTooltips(select._el);
    };

    const onEscPress = (evt, select) => {
        if (evt.key === "Escape" || evt.key === "Esc") {
            closeMultiSelect(select);
        }
    };

    const onDocumentClick = (evt, select) => {
        if (!evt.target.closest(`.multi-select`)) {
            closeMultiSelect(select);
        }
    };

    const showTooltip = (container, quantity) => {
        removeAllTooltips(container.parentNode);
        const template = document.querySelector(`#tooltip`).content;
        const tooltip = template.cloneNode(true);
        const tooltipText = tooltip.querySelector(`.select-tooltip__text`);
        tooltipText.innerHTML = `Объектов найдено ${quantity}`;
        container.appendChild(tooltip);
    }

    checkDisabled = (multiSelect) => {
        [...multiSelect._state.checked].forEach((it) => {
            if(it.disabled) {
                multiSelect._state.checked.splice(multiSelect._state.checked.indexOf(it), 1);
                it.checked = false;
            }
        })
    };

    class MultiSelect {
        constructor(select) {
            this._el = select;
            this._toggler = null;
            this._dropdown = null;
            this._state = {
                open: false,
                checked: []
            };

            this._onTogglerClick = this._onTogglerClick.bind(this);
            this._onDropdownChange = this._onDropdownChange.bind(this);
            this._onChange = null;
            this._disableCheckboxes = null;
        }

        _onDropdownChange(evt) {
            if (typeof this._onChange === `function`) {
                this._onChange(evt);
            }
        }

        set onChange(fn) {
            this._onChange = fn;
        }



        set disableCheckboxes(fn) {
            this._disableCheckboxes = fn;
        }

        _onTogglerClick() {
            if (this._el.classList.contains(`multi-select--open`)) {
                closeMultiSelect(this);
                this.unbind();
            } else {
                [...multiSelect].forEach((it) => {
                    it.classList.remove(`multi-select--open`);
                });
                this._el.classList.add(`multi-select--open`);
                this.bind();
            }
        };

        unbind() {
            this._dropdown.removeEventListener(`change`, this._onDropdownChange);
            document.removeEventListener(`click`, onDocumentClick);
            document.removeEventListener(`keydown`, onEscPress);
        };

        bind() {
            this._dropdown.addEventListener(`change`, this._onDropdownChange);
            document.addEventListener(`click`, (evt) => {
                onDocumentClick(evt, this);
            });
            document.addEventListener(`keyup`, (evt) => {
                onEscPress(evt, this);
            });
        };

        init() {
            this._dropdown = this._el.querySelector(`.multi-select__dropdown`);
            this._toggler = this._el.querySelector(`.select__toggler--multi-select`);
            this._toggler.addEventListener(`click`, this._onTogglerClick);
            const checkedInputs = this._el.querySelectorAll(`input:checked`);
            [...checkedInputs].forEach((input) => {
                this._state.checked.push(input);
            });
            сhangeText(this._toggler, this._el.dataset.intro);
        };
    }

    const multiSelect = document.querySelectorAll(`.multi-select`);
    const multiSelectsArr = [];

    [...multiSelect].forEach((it) => {
        const multiSelect = new MultiSelect(it);
        multiSelectsArr.push(multiSelect);
        multiSelect.init();
        multiSelect.onChange = (evt) => {
            if (evt.target.checked) {
                multiSelect._state.checked.push(evt.target);
                const tooltipContainer = evt.target.parentNode;
                $('.js-searchForm').trigger('submit', tooltipContainer);
            } else {
                multiSelect._state.checked.splice(multiSelect._state.checked.indexOf(evt.target), 1);
                if ( multiSelect._state.checked.length > 0) {
                    const tooltipContainer = multiSelect._state.checked[multiSelect._state.checked.length - 1].parentNode;
                    removeAllTooltips(multiSelect._dropdown);
                    $('.js-searchForm').trigger('submit', tooltipContainer);
                } else {
                    removeAllTooltips(multiSelect._dropdown);
                }
            }
            const checkedItemsText =
                `${(multiSelect._state.checked.length > 0) ? 'Выбрано ' + multiSelect._state.checked.length
                    : multiSelect._el.dataset.intro}`;
            сhangeText(multiSelect._toggler, checkedItemsText);
        }
    });
    // ~ стилизованные селекты


    // скрытие / показ формы с фильтрами
    const filterToggler = document.querySelector(`.filter-toggler`);

    const toggleForm = (form) => {
        form.classList.toggle(`js-searchForm--open`);
    };

    const filterTogglerHandler = (evt) => {
        const self = evt.target;
        const form = self.previousElementSibling;
        if (form.classList.contains(`js-searchForm--open`)) {
            self.textContent = `Показать фильтры`;
            toggleForm(form);
        } else {
            self.textContent = `Скрыть фильтры`;
            toggleForm(form);
        }
    };

    if (filterToggler) {
        filterToggler.addEventListener(`click`, filterTogglerHandler);
    }
    // - скрытие / показ формы с фильтрами

    // автоматическая отправка фильтра
    if ($('.js-searchForm').length > 0) {

        $('.js-searchForm').on('change', 'input, select', function(evt) {
            console.log('changed and load smth');
            setUrl('?' + $('.js-searchForm').find('input[type="checkbox"], input[type="number"], select')
                .filter(function() {
                    return !!this.value;
                }).serialize());
            if (evt.target.matches('.multi-select__input')) {
                return;
            } else if (evt.target.matches('select')) {
                $('.js-searchForm').trigger('submit', evt.target);
            } else {
                $('.js-searchForm').trigger('submit');
            }

        });
    }
    // ~ автоматическая отправка фильтра

    // управление адресной строкой
    function setUrl(url) {
        try {
            history.pushState(null, null, url);
            return;
        } catch (e) {
            location.hash = '#' + url;
        }
    }

    // ~ управление адресной строкой

    // форматирование цены
    function numberFormat(number, decimals, dec_point, thousands_sep) {
        var i, j, kw, kd, km;
        if (isNaN(decimals = Math.abs(decimals))) {
            decimals = 2;
        }
        if (typeof dec_point == 'undefined') {
            dec_point = ",";
        }
        if (typeof thousands_sep == 'undefined') {
            thousands_sep = ".";
        }
        i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
        if ((j = i.length) > 3) {
            j = j % 3;
        } else {
            j = 0;
        }
        km = (j ? i.substr(0, j) + thousands_sep : "");
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
        return km + kw + kd;
    }

    // форматирование цены

    // наружная реклама вид картой
    if ($('#arenda-maps').length > 0 && $('.js-searchForm').length > 0) {
        ymaps.ready(function() {
            var map = new ymaps.Map("arenda-maps", {
                center: [61.254052, 73.396204],
                zoom: 7,
                controls: ['zoomControl', 'typeSelector', 'fullscreenControl']
            });
            var trafficControl = new ymaps.control.TrafficControl({state: {trafficShown: true}});
            map.controls.add(trafficControl, {top: 10, left: 10});

            var searchForm = $('.js-searchForm');
            var timerId;
            var xhr;
            var loadPoints = function(element) {
                console.log(element);
                clearTimeout(timerId);
                preloaderShow('.js-ajaxLoader');
                timerId = setTimeout(function() {
                    if (xhr && xhr.readyState != 4) {
                        xhr.abort();
                    }
                    xhr = $.ajax({
                        url: $('#arenda-maps').data('url'),
                        data: location.search.slice(1),
                        dataType: 'json',
                        success: function(data) {
                            $('.js-tableTabLink').attr('href', data.items.url.table);
                            if (data.items.count.table > 0) {
                                $('.js-tableTabLink').show();
                            } else {
                                $('.js-tableTabLink').hide();
                            }
                            $('.js-minPrice').attr('placeholder', data.items.min_price);
                            $('.js-maxPrice').attr('placeholder', data.items.max_price);
                            if(element && element.matches('option')) {
                                disableCheckboxes('.js-formats', data.items.formats);
                                disableCheckboxes('.js-periods', data.items.periods);
                                disableCheckboxes('.js-ratings', data.items.ratings);
                                disabledSelect('.js-streets', data.items.streets);
                                multiSelectsArr.forEach((it) => {
                                    it._state.checked = [];
                                    const checkedInputs = it._el.querySelectorAll(`input:checked`);
                                    [...checkedInputs].forEach((input) => {
                                        it._state.checked.push(input);
                                    })
                                    сhangeText(it._toggler, it._el.dataset.intro);
                                });
                            }
                            if(element && element.matches('li')) {
                                showTooltip(element, data.items.count.table)
                            }
                            $('.js-outdoorAdTable').html(data.html);
                            map.geoObjects.removeAll();
                            $.each(data.map, function(index, item) {
                                var placemark = new ymaps.GeoObject({
                                        geometry: {
                                            type: "Point",
                                            coordinates: [item.lat, item.lon]
                                        }
                                    }, {
                                        iconLayout: 'default#image',
                                        iconImageHref: '/map/sprite.png',
                                        iconImageSize: [30, 30],
                                        iconOffset: [-3, 25],
                                        iconImageClipRect: [
                                            [item.rotate * 30 / 5, (item.sides - 1) * 30],
                                            [item.rotate * 30 / 5 + 30, (item.sides - 1) * 30 + 30]
                                        ]
                                    }
                                );
                                placemark.events.add('click', function() {
                                    placemark.properties.set('balloonContent', '<div class="placemark-ajax-loader">'
                                        + '<div class="Preloader__spinner"></div>'
                                        + '</div>');
                                    $.ajax({
                                        url: $('#arenda-maps').data('content-url'),
                                        data: {id: item.id},
                                        success: function(item) {
                                            placemark.properties.set('balloonContent', item);
                                        }
                                    });
                                });
                                map.geoObjects.add(placemark);
                            });
                            preloaderHide('.js-ajaxLoader');
                            map.setBounds(map.geoObjects.getBounds());
                            if (map.getZoom() > 16) {
                                map.setZoom(16);
                            } else {
                                map.setZoom(map.getZoom() - 1);
                            }
                        }
                    });
                }, 500);
            };
            searchForm.on('submit', function(e, element) {
                e.preventDefault();
                e.stopImmediatePropagation();
                console.log('submit and loadPoints')
                loadPoints(element);
                return false;
            });
            loadPoints();
            window.onpopstate = function() {
                loadPoints();
            };
        });
    }
    // ~ наружная реклама вид картой

    // деактивация/активация опции выпадающиего списка
    function disabledSelect(cssClass, array) {
        $(cssClass).find('option').each(function() {
            var val = $(this).val();
            if (val === '' || array.indexOf(parseFloat(val)) >= 0) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', 'disabled');
            }
        });
    }
    // ~ деактивация опции выпадающиего списка
    // деактивация/активация чекбоксов
    function disableCheckboxes(cssClass, array) {
        $(cssClass).each(function() {
            var val = $(this).val();
            if (val === '' || array.indexOf(parseFloat(val)) >= 0) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', 'disabled');
                $(this).prop('checked', false);
            }
        });
    }
    // ~ деактивация опции чекбоксов

    // наружная реклама вид таблицей
    if ($('.js-outdoorAdTable').length > 0 && $('.js-searchForm').length > 0) {
        var searchForm = $('.js-searchForm');
        var timerId;
        var xhr;
        // отправка формы
        searchForm.on('submit', function(e, element) {
            e.preventDefault();
            e.stopImmediatePropagation();
            console.log('submit and load table')
            loadTable(element);
            return false;
        });
        // "назад" в браузере
        window.onpopstate = function() {
            loadTable();
        };
        // сортировка
        $('.js-outdoorAdTable').on('click', '.js-sorter', function(e) {
            e.preventDefault();
            setUrl($(this).attr('href'));
            loadTable();
        });
        // пагинация
        $('.js-outdoorAdTable').on('click', '.js-pager a', function(e) {
            e.preventDefault();
            setUrl($(this).attr('href'));
            loadTable();
            $('html,body').stop().animate({scrollTop: $('.js-outdoorAdTable').offset().top}, 1000);
        });

        // функция загрузки
        function loadTable(element = null) {
            preloaderShow('.js-ajaxLoader');
            clearTimeout(timerId);
            timerId = setTimeout(function() {
                if (xhr && xhr.readyState != 4) {
                    xhr.abort();
                }
                xhr = $.ajax({
                    url: $('.js-outdoorAdTable').data('url'),
                    data: location.search.slice(1),
                    dataType: 'json',
                    success: function(data) {
                        $('.js-minPrice').attr('placeholder', data.items.min_price);
                        $('.js-maxPrice').attr('placeholder', data.items.max_price);
                        if(element && element.matches('option')) {
                            disableCheckboxes('.js-formats', data.items.formats);
                            disableCheckboxes('.js-periods', data.items.periods);
                            disableCheckboxes('.js-ratings', data.items.ratings);
                            disabledSelect('.js-streets', data.items.streets);
                            multiSelectsArr.forEach((it) => {
                                it._state.checked = [];
                                const checkedInputs = it._el.querySelectorAll(`input:checked`);
                                [...checkedInputs].forEach((input) => {
                                    it._state.checked.push(input);
                                })
                                сhangeText(it._toggler, it._el.dataset.intro);
                            });
                        }
                        if(element && element.matches('li')) {
                            showTooltip(element, data.items.count.table)
                        }
                        if (data.items.count.map > 0) {
                            $('.js-mapCount').show();
                        } else {
                            $('.js-mapCount').hide();
                        }
                        $('.js-outdoorAdTable').html(data.html);
                        $('.js-mapTabLink').attr('href', data.items.url.map);
                        $('.js-mapCount').html(data.items.count.map);
                        preloaderHide('.js-ajaxLoader');
                    }
                });
            }, 500);

        }
    }
    // наружная реклама вид таблицей

    // установка формата по клику на иконку
    if ($('.js-setFormat').length > 0) {
        $('.js-setFormat a').click(function() {
            $('#quickoutdoororder-model_type').val($(this).data('format'));
        });
    }
    // ~ установка формата по клику на иконку

    // Загрузка улиц
    if ($('.js-city').length > 0 && $('.js-streets').length > 0) {
        $('.js-city').on('change', function() {
            $('.js-streets').attr('disabled', 'disabled').html('<option value="">Загрузка...</option>');
            $.ajax({
                url: '/outdoor-ad/streets',
                data: {city_id: $('.js-city').val(), is_map: $('#arenda-maps').length > 0},
                success: function(data) {
                    $('.js-streets').removeAttr('disabled').html(data);
                }
            });
        });
    }
    // ~ Загрузка улиц

    // Загрузка поверхности
    $(document).on('click', '.js-items', function(e) {
        e.preventDefault();
        var self = $(this),
            messageBox = $('#message'),
            showMessage = $('#show-message'),
            ajaxBox = self.next().find('.js-result');
        if (self.hasClass('active')) {
            self.removeClass('active');
            ajaxBox.slideUp('slow');
        } else {
            $('html,body').stop().animate({
                scrollTop: self.offset().top
            }, 1000);
            self.addClass('active');
            ajaxBox.slideDown('slow');
            if (!self.hasClass('loaded')) {
                ajaxBox.addClass('load Preloader__center').html('<div class="Preloader__spinner"></div>');
                $.ajax({
                    url: self.data('url'),
                    success: function(data) {
                        ajaxBox.removeClass('load').html(data);
                        self.addClass('loaded');
                        var form = ajaxBox.find('.js-outdoor-form');
                        ajaxBox.find(".js-carousel").owlCarousel({
                            items: 1,
                            itemsDesktop: 1,
                            itemsDesktopSmall: 1,
                            itemsTablet: 1,
                            itemsMobile: 1
                        });
                        ajaxBox.find('.js-fancybox').fancybox();
                        var map = ajaxBox.find('.js-map');
                        if (map.length > 0) {
                            ymaps.ready(function() {
                                var ymap = new ymaps.Map(map.attr('id'), {
                                    center: [map.data('lat'), map.data('lon')],
                                    zoom: 15,
                                    controls: ['zoomControl', 'typeSelector', 'fullscreenControl']
                                });
                                var trafficControl = new ymaps.control.TrafficControl({state: {trafficShown: true}});
                                ymap.controls.add(trafficControl, {top: 10, left: 10});
                                ymap.geoObjects.add(new ymaps.GeoObject({
                                        geometry: {
                                            type: "Point",
                                            coordinates: [map.data('lat'), map.data('lon')]
                                        }
                                    }, {
                                        iconLayout: 'default#image',
                                        iconImageHref: '/map/sprite.png',
                                        iconImageSize: [30, 30],
                                        iconOffset: [-3, 25],
                                        iconImageClipRect: [
                                            [map.data('rotate') * 30 / 5, (map.data('sides') - 1) * 30],
                                            [map.data('rotate') * 30 / 5 + 30, (map.data('sides') - 1) * 30 + 30]
                                        ]
                                    }
                                ));
                            });
                        }
                        var total = ajaxBox.find('.js-total'),
                            span = total.find('.js-total-price'),
                            oldPrice = ajaxBox.find('.js-total-old-price'),
                            priceBox = ajaxBox.find('.js-total-price-box'),
                            ecoBox = ajaxBox.find('.js-total-eco'),
                            servicesBox = total.find('.js-total-services');

                        total.hide();
                        ajaxBox.find('input').change(function() {
                            var price = 0;
                            var priceWithDiscount = 0;
                            ajaxBox.find('.month-checkbox input').each(function() {
                                if ($(this).is(':checked')) {
                                    var discount = parseFloat($(this).data('discount'));
                                    var currentPrice = parseFloat($(this).data('price'));
                                    price += currentPrice;
                                    if (discount > 0) {
                                        priceWithDiscount += currentPrice - (currentPrice / 100 * discount);
                                    } else {
                                        priceWithDiscount += currentPrice;
                                    }
                                }
                            });
                            if (price > 0) {
                                price += ajaxBox.find('input[name=install]').data('price');
                                priceWithDiscount += ajaxBox.find('input[name=install]').data('price');
                                var name = 'Базовая цена + монтаж';
                                ajaxBox.find('input[name=print]').each(function() {
                                    if ($(this).is(':checked')) {
                                        name += ' + печать';
                                        price += parseFloat($(this).data('price'));
                                        priceWithDiscount += parseFloat($(this).data('price'));
                                    }
                                });
                                ajaxBox.find('input[name=design]').each(function() {
                                    if ($(this).is(':checked')) {
                                        name += ' + дизайн';
                                        price += parseFloat($(this).data('price'));
                                        priceWithDiscount += parseFloat($(this).data('price'));
                                    }
                                });
                                servicesBox.html(name);
                                total.show();
                                ajaxBox.find('.add-to-basket').removeClass('btn disabled');
                                ajaxBox.find('.buy-now').removeClass('btn disabled');
                            } else {
                                total.hide();
                                ajaxBox.find('.add-to-basket').addClass('btn disabled');
                                ajaxBox.find('.buy-now').addClass('btn disabled');
                            }
                            oldPrice.html(numberFormat(price, 0, ' ', ' ') + ' руб.');
                            span.html(numberFormat(priceWithDiscount, 0, ' ', ' ') + ' руб.');
                            if (price > priceWithDiscount) {
                                oldPrice.show();
                                priceBox.addClass('total__grid--discount');
                                var perc = 100 - priceWithDiscount * 100 / price;
                                ecoBox.html('Вы экономите ' + Math.round(perc) + '%').show();
                            } else {
                                oldPrice.hide();
                                priceBox.removeClass('total__grid--discount');
                                ecoBox.hide();
                            }
                        }).trigger('change');
                        ajaxBox.find('.add-to-basket').click(function() {
                            $.ajax({
                                url: '/outdoor-ad/add-to-basket',
                                data: form.serialize(),
                                method: 'post',
                                success: function(data) {
                                    messageBox.html(data);
                                    showMessage.trigger('click');
                                    $('.basket-box').trigger('reload.basket');
                                    return false;
                                }
                            });
                            return false;
                        });
                        ajaxBox.find('.buy-now').click(function() {
                            var butt = $(this);
                            butt.html('Подождите...');
                            $.ajax({
                                url: '/outdoor-ad/add-to-basket',
                                data: form.serialize(),
                                method: 'post',
                                success: function(data) {
                                    $('.basket-box').addClass('autoclick').trigger('reload.basket');
                                    butt.html('Оформить');
                                    return false;
                                }
                            });
                            return false;
                        });
                    },
                    error: function() {
                        ajaxBox.removeClass('load').html('Ошибка при загрузке');
                    }
                });
            }
        }
    });
    // ~ Загрузка поверхности

    // Прелоадер
    function preloaderShow(obContainer) {

        var elements = '.Preloader__overlay, .Preloader__spinnerContainer';
        obContainer = $(obContainer);
        obContainer.removeClass('Preloader--top Preloader--bottom')
            .addClass('Preloader--active')
            .find(elements)
            .stop()
            .fadeIn(300);

        if (obContainer.height() > 200) {
            obContainer.addClass('Preloader--floating');
            preloaderPosition(obContainer);
            preloaderIndent(obContainer);
        } else {
            obContainer.removeClass('Preloader--floating')
                .find('.Preloader__spinnerContainer')
                .css({
                    left: '',
                    width: ''
                });
        }
    }

    function preloaderHide(obContainer) {

        var elements = '.Preloader__overlay, .Preloader__spinnerContainer';
        $(obContainer).removeClass('Preloader--active')
            .find(elements)
            .stop()
            .fadeOut(200);
    }

    function preloaderPosition(obContainer) {

        obContainer = $(obContainer);

        // Границы контейнера
        var containerTop = obContainer.offset().top;
        var containerBottom = obContainer.offset().top + obContainer.height();

        // Центр экрана
        var screenCenter = $(document).scrollTop() + $(window).height() / 2;

        // Проверки
        if (containerBottom - 100 > screenCenter) {
            obContainer.removeClass('Preloader--bottom');
        } else {
            obContainer.addClass('Preloader--bottom');
        }

        if (containerTop + 100 < screenCenter) {
            obContainer.removeClass('Preloader--top');
        } else {
            obContainer.addClass('Preloader--top');
        }
    }

    function preloaderIndent(obContainer) {

        $(obContainer).find('.Preloader__spinnerContainer').css({
            left: obContainer.offset().left + 'px',
            width: obContainer.width() + 'px'
        });
    }

    function preloaderHandleScrolling() {

        var preloaders = $('.Preloader--active.Preloader--floating');
        if (!preloaders.length) {
            return;
        }
        $.each(preloaders, function(i, item) {
            preloaderPosition(item);
        });
    }

    function preloaderHandleResize() {

        var preloaders = $('.Preloader--active.Preloader--floating');
        if (!preloaders.length) {
            return;
        }
        $.each(preloaders, function(index, obItem) {
            preloaderIndent(obItem);
        });
    }

    $(window).on('scroll', preloaderHandleScrolling);
    $(window).on('resize', preloaderHandleResize);
    // ~ Прелоадер

    var messageBox = $('#message'),
        showMessage = $('#show-message');

    var self = $('#add-to-basket'),
        form = self;

    self.find('.add-to-basket').click(function() {
        $.ajax({
            url: '/outdoor-ad/add-to-basket',
            data: form.serialize(),
            method: 'post',
            success: function(data) {
                messageBox.html(data);

                $('.basket-box').trigger('reload.basket');
            }
        });
        return false;
    });
    $('.big-foto').owlCarousel({
        items: 1,
        itemsDesktop: 1,
        itemsDesktopSmall: 1,
        itemsTablet: 1,
        itemsMobile: 1,
        pagination: false
    });
    $('.preview-foto').owlCarousel({
        items: 4,
        itemsDesktop: 4,
        itemsDesktopSmall: 4,
        itemsTablet: 4,
        itemsMobile: 4,
        pagination: false
    }).find('a').each(function() {
        $(this).click(function() {
            $('.big-foto').trigger('owl.goTo', $(this).data('i'));
            return false;
        })
    });
    $('.rekl-container').each(function() {
        var box = $(this),
            total = box.find('.js-total'),
            span = total.find('.span-total'),
            spanWithDiscount = total.find('.span-total-with-discount'),
            nameBox = total.find('.serv-name-box'),
            hintText = box.find('.hidden-hint-text');

        total.hide();
        box.find('input').change(function() {
            var price = 0;
            var priceWithDiscount = 0;
            box.find('.month-checkbox input').each(function() {
                if ($(this).is(':checked')) {
                    var discount = parseFloat($(this).data('discount'));
                    var currentPrice = parseFloat($(this).data('price'));
                    price += currentPrice;
                    if (discount > 0) {
                        priceWithDiscount += currentPrice - (currentPrice / 100 * discount);
                    } else {
                        priceWithDiscount += currentPrice;
                    }
                }
            });

            if (price > 0) {
                price += box.find('input[name=install]').data('price');
                priceWithDiscount += box.find('input[name=install]').data('price');
                var name = 'Базовая цена + монтаж';
                box.find('input[name=print]').each(function() {
                    if ($(this).is(':checked')) {
                        name += ' + печать';
                        price += parseFloat($(this).data('price'));
                        priceWithDiscount += parseFloat($(this).data('price'));
                    }
                });
                box.find('input[name=design]').each(function() {
                    if ($(this).is(':checked')) {
                        name += ' + дизайн';
                        price += parseFloat($(this).data('price'));
                        priceWithDiscount += parseFloat($(this).data('price'));
                    }
                });
                nameBox.html(name);
                total.show();
                hintText.hide();
                box.find('.add-to-basket').removeClass('btn disabled');
                box.find('.buy-now').removeClass('btn disabled');
            } else {
                hintText.show();
                total.hide();
                box.find('.add-to-basket').addClass('btn disabled');
                box.find('.buy-now').addClass('btn disabled');
            }
            span.html(numberFormat(price, 0, ' ', ' ') + ' руб.');
            if (price > priceWithDiscount) {
                span.addClass('old-price');
                spanWithDiscount.html('<b>' + numberFormat(priceWithDiscount, 0, ' ', ' ')
                    + ' руб.</b> Ваша выгода составит ' + numberFormat((price
                        - priceWithDiscount), 0, ' ', ' ') + ' руб!').show();
            } else {
                span.removeClass('old-price');
                spanWithDiscount.html('').hide();
            }
        }).trigger('change');
    });

    const menuToggler = $('.navbar-toggle');
    const menu = $('#main-menu');
    menuToggler.on('click', () => {
        $(this).toggleClass('collapsed');
        menu.slideToggle();
    });
});
