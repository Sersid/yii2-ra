document.addEventListener("DOMContentLoaded", function() {
    gsap.registerPlugin(ScrollTrigger);
    const graph = document.querySelector('.new-service__graph--desktop');
    if (graph) {
        const movingLayout = graph.querySelector(`.new-service__graph-moving-layout`);
        const graphLogo = graph.querySelector(`.graph-logo`);
        const divider = graph.querySelector(`.divider`);
        const tl = gsap.timeline({
            scrollTrigger: {
                trigger: ".new-service__graph",
                start: 'bottom bottom',
            }
        })
        tl.to(movingLayout, {x: '100%', duration: 1, ease: 'power4.slowMo'})
        tl.from(graphLogo, {opacity: 0, duration: .25, ease: 'power4.slowMo'})
        tl.from(divider, {scaleY: 0, duration: .25, ease: 'power4.slowMo', transformOrigin: "center bottom"})
    }
    
      gsap.registerPlugin(ScrollTrigger);
        const cols = document.querySelectorAll('.new-service-diagram__column');
        if (cols.length) {
            [...cols].forEach((col) => {
                const bar = col.querySelector(`div`);
                const text = col.querySelector(`p`);
                const tl = gsap.timeline({
                    scrollTrigger: {
                        trigger: ".new-service-diagrams",
                        start: 'top center',
                    }
                })
                tl.from(bar, {scaleY: 0, duration: 1.25, ease: 'power4.slowMo', transformOrigin: "center bottom"})
                tl.from(text, {opacity: 0, duration: 0.3, ease: 'power4.slowMo'})
    
            })
        }
});
