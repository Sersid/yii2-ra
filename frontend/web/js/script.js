window.human = false;

$(function() {
    Revealator.effects_padding = '-350';
    $('.about-box:eq(0)').addClass('revealator-zoomin revealator-delay0');
    $('.about-box:eq(1)').addClass('revealator-zoomin revealator-delay2');
    $('.about-box:eq(2)').addClass('revealator-zoomin revealator-delay4');
    $('.about-box:eq(3)').addClass('revealator-zoomin revealator-delay6');
    $('.etapu-item:eq(0)').addClass('revealator-slideright revealator-delay1');
    $('.etapu-item:eq(1)').addClass('revealator-slideright revealator-delay3');
    $('.etapu-item:eq(2)').addClass('revealator-slideright revealator-delay5');
    $('.etapu-item:eq(3)').addClass('revealator-slideright revealator-delay7');
    $('.etapu-item:eq(4)').addClass('revealator-slideright revealator-delay9');
});
$('#feedback-form').on('show.bs.modal', function() {
    var box = $(this).find('.form-body');
    var ajax = function(data) {
        box.html(data);
        var form = box.find('form');
        form.submit(function() {
            var serialize = form.serialize();
            var action = form.attr('action');
            box.html('<p style="color: white; text-align: center">Загрузка...</p>');
            $.ajax({
                url: action,
                method: 'post',
                data: serialize,
                success: function(data) {
                    ajax(data);
                }
            });
            return false;
        });
    };
    box.html('<p style="color: white; text-align: center">Загрузка...</p>');
    $.ajax({
        url: '/forms/feedback-modal',
        success: function(data) {
            ajax(data);
        }
    });
});
jQuery('.item-last:eq(0)').viewportChecker({
    classToAdd: 'visible-item animated fadeInLeft',
    offset: 100
});
jQuery('.item-last:eq(1)').viewportChecker({
    classToAdd: 'visible-item animated fadeInLeft',
    offset: 100
});
jQuery('.item-last:eq(2)').viewportChecker({
    classToAdd: 'visible-item animated fadeInRight',
    offset: 100
});
jQuery('.item-last:eq(3)').viewportChecker({
    classToAdd: 'visible-item animated fadeInRight',
    offset: 100
});

jQuery('.left-box.red-title').addClass("hidden-n").viewportChecker({
    classToAdd: 'visible-n animated bounceInUp',
    offset: 200
});
jQuery('.prichinu-item .red-title').addClass("hidden-item").viewportChecker({
    classToAdd: 'visible-item animated bounceInUp',
    offset: 200
});
jQuery('.form-bg').addClass("hidden-item").viewportChecker({
    classToAdd: 'visible-item animated flipInX',
    offset: 100
});
// стилизованные селекты
var customSelect = function(box) {
    box.find(".custom-select").each(function() {
        var classes = $(this).attr("class"),
            id = $(this).attr("id"),
            name = $(this).attr("name");
        var template = '<div class="' + classes + '">';
        template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
        $(this).find("option").each(function() {
            template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value")
                + '">' + $(this).html() + '</span>';
        });
        template += '</div></div>';

        $(this).wrap('<div class="custom-select-wrapper"></div>');
        $(this).hide();
        $(this).after(template);
    });
    box.find(".custom-option:first-of-type").hover(function() {
        $(this).parents(".custom-options").addClass("option-hover");
    }, function() {
        $(this).parents(".custom-options").removeClass("option-hover");
    });
    box.find(".custom-select-trigger").on("click", function() {
        $('html').one('click', function() {
            $(".custom-select").removeClass("opened");
        });
        $(this).parents(".custom-select").toggleClass("opened");
        event.stopPropagation();
    });
    box.find(".custom-option").on("click", function() {
        $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value")).trigger('change');
        $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
        $(this).addClass("selection");
        $(this).parents(".custom-select").removeClass("opened");
        $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
    });
};
customSelect($(document));

$(".fancybox").fancybox();

$('.basket-box').each(function() {
    var self = $(this),
        loadBasket = function() {
            $.ajax({
                url: self.data("url"),
                success: function(responce) {
                    self.html(responce);
                    if (document.body.clientWidth > 0) {
                        $('.basket-button-block').click(function() {
                            $(".link-basket").toggleClass('in');
                            return false;
                        });
                    }
                    if (self.hasClass('autoclick')) {
                        self.removeClass('autoclick');
                        self.find('.yellow-btn').trigger('click');
                    }
                    self.find('.basket-block-body-dell-item a').click(function() {
                        console.log($(this));
                        $.ajax({
                            url: $(this).attr('href'),
                            success: function() {
                                $('.ajax-block').trigger('reload.box');
                                $('.basket-box').trigger('reload.basket');
                            }
                        });
                        return false;
                    });
                }
            });
        };
    loadBasket();
    self.on('reload.basket', function() {
        loadBasket();
    })
});

function move_to_cart(data, href) {
    var block = ($("div.basket-button-icon").css('display') != 'none') ? $("div.basket-button-icon")
                                                                       : $("a.basket-button-icon");

    $('.summ-tovar')
        .clone().removeClass('hidden').addClass('red-circle').html('')
        .css({
            'position': 'absolute',
            'z-index': '11100',
            top: $(data).offset().top,
            left: $(data).offset().left
        })
        .appendTo("body")
        .animate({
            opacity: 0.05,
            left: block.offset()['left'],
            top: block.offset()['top'],
            width: 20
        }, 1000, function() {
            $(this).remove();

            if (href != undefined && href !== false) {
                var self = $(data),
                    form = self;

                $.ajax({
                    url: href,
                    data: form.serialize(),
                    method: 'post',
                    success: function(data) {
                        $('.basket-box').trigger('reload.basket');
                    }
                });
                return false;
            }
        });
}

if (document.querySelector(".sidebar-affix")) {
    var block = $('.sidebar-affix');

    // Получаем высоту всего окна с учетом scroll
    var scrollHeight = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );

    block.affix({
        offset: {
            top: block.offset().top,
            bottom: scrollHeight - $('.fixator-affix').offset().top
        }
    });
}

var fromBox = $('#edit-text-form');
$('.edit-box-widget').each(function() {
    var widget = $(this);
    widget.parent().addClass('edit-box-widget-parent');
    var ajax = function(data) {
        fromBox.html(data);
        var form = fromBox.find('form');
        form.submit(function() {
            $.ajax({
                url: form.attr('action'),
                method: 'post',
                data: form.serialize(),
                success: function(data) {
                    ajax(data);
                }
            });
            return false;
        });
    };
    widget.find('button').click(function() {
        fromBox.html('<div class="modal-body">Загрузка...</div>');
        $.ajax({
            url: widget.data('url'),
            success: function(data) {
                ajax(data);
            }
        });
    });
});

$('#set-city').on('show.bs.modal', function(event) {
    var modal = $(this),
        timerId,
        listBox = modal.find('.modal-body'),
        ajax = function() {
            listBox.html('Загрузка...');
            $.ajax({
                url: '/site/city-list',
                data: {back_url: modal.data('back_url'), name: $('#set-city-search').val()},
                success: function(data) {
                    listBox.html(data);
                }
            });
        };

    ajax();
    $('#set-city-search').keydown(function() {
        clearTimeout(timerId);
        timerId = setTimeout(function() {
            ajax();
        }, 500);
    });
});
$('.confirm-city').each(function() {
    var box = $(this);
    box.find('.confirm-city__btn').click(function() {
        if ($(this).hasClass('confirm-city__btn_yes') || $(this).hasClass('basket-block-body-dell-item')) {
            $.ajax({
                url: '/site/set-city',
                data: {slug: $(this).data('slug')},
                success: function(data) {
                    box.hide();
                }
            });
        } else {
            box.hide();
        }
    });
    box.find('.basket-block-body-dell-item').click(function() {
        $.ajax({
            url: '/site/set-city',
            data: {slug: $(this).data('slug')},
            success: function(data) {
                box.hide();
            }
        });
    });
});
const togglerText = {
    shown: 'Свернуть подробное описание',
    hidden: 'Подробное описание'
}
const onradioBlockTogglerClick = (evt) => {
    evt.preventDefault();
    const self = evt.target;
    const parent = self.closest(`.radio-item`);
    if (!parent.classList.contains(`radio-item--shown`)) {
        parent.classList.add(`radio-item--shown`);
        self.textContent = togglerText.shown;
    } else {
        parent.classList.remove(`radio-item--shown`);
        self.textContent = togglerText.hidden;
    }
}
document.addEventListener(`click`, (evt) => {
    if(evt.target === evt.target.closest(`.kanal__link`)) {
        onradioBlockTogglerClick(evt);
    }
})
//$(document).on('click', '.kanal__link', function(e) {
  //  e.preventDefault();
   // $(this).hide();
  // $(this).
//    $(this).next('.js-detailBox').show();
//});
