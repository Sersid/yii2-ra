document.addEventListener("DOMContentLoaded", function() {
    const internetTabs = document.querySelector(`.internet-reklama-tabs`);
    const tabsControls = internetTabs.querySelector(`.internet-reklama-tabs__controls`);
    const tabsContent = internetTabs.querySelectorAll(`.internet-reklama-tabs__item`);

    const showTab = (pointer) => {
        [...tabsContent].forEach((tab) => {
            tab.classList.remove(`internet-reklama-tabs__item--active`);
            if(tab.dataset.pointer === pointer) {
                tab.classList.add(`internet-reklama-tabs__item--active`);
            }
        });
    };

    const onTabsControlsChange = (evt) => {
        const pointer = evt.target.value;
        showTab(pointer);
    };

    tabsControls.addEventListener(`change`, onTabsControlsChange);

    showTab(tabsControls.querySelector(`input:checked`).value);
});
