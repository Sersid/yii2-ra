<?php

namespace frontend\controllers;

use common\models\City;
use frontend\repositories\CityRepository;
use frontend\repositories\OutdoorAdSideRepository;
use frontend\repositories\RadioAdRepository;
use frontend\repositories\TickerRepository;
use frontend\repositories\TvAdRepository;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Contacts controller
 * @package frontend\controllers
 */
class ContactsController extends Controller
{
    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $city_id = Yii::$app->city->id;
        if (!empty($city_id)) {
            $model = (new CityRepository())->getById($city_id);
            if (empty($model)) {
                throw new NotFoundHttpException('Город не найден.');
            }
        } else {
            $model = new City();
        }
        return $this->render('index',
            [
                'model' => $model,
                'cnt_tv' => (new TvAdRepository())->getCountInCity($model->id),
                'cnt_radio' => (new RadioAdRepository())->getCountInCity($model->id),
                'cnt_ticker' => (new TickerRepository())->getCountInCity($model->id),
                'cnt_outdoor_ad' => (new OutdoorAdSideRepository())->getCountInCity($model->id),
                'hideCntBlocks' => empty($city_id),
            ]);
    }
}
