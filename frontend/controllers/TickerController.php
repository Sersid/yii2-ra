<?php

namespace frontend\controllers;

use common\models\City;
use common\models\Ticker;
use common\models\Tv;
use common\models\UserTicker;
use common\models\UserTickerPhone;
use common\models\UserTickerTv;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidArgumentException;
use yii\db\ActiveRecord;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Contacts controller
 */
class TickerController extends Controller
{
    /**
     * @return string
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     */
    public function actionIndex()
    {
        $city = Yii::$app->request->get('city');
        $tickers = 0;
        if (!empty($city)) {
            $city = City::findOne(['slug' => $city]);
            if (empty($city)) {
                throw new NotFoundHttpException('Город не найден.');
            }
            $tickers = Ticker::find()
                ->where(['city_id' => $city->id, 'status' => Ticker::STATUS_ACTIVE])
                ->groupBy('tv_id')
                ->with(['tv'])
                ->orderBy(['ticker.sort' => SORT_ASC])
                ->all();
        }

        $tv = Yii::$app->request->get('tv');
        if (!empty($tv)) {
            $tv = Tv::findOne(['slug' => $tv]);
            if (empty($tv)) {
                throw new NotFoundHttpException('Канал не найден.');
            }
        }

        $model = $this->getModel();
        return $this->render('index', ['city' => $city, 'tv' => $tv, 'tickers' => $tickers, 'model_id' => $model->id]);
    }

    /**
     * @param bool $with
     *
     * @return $this|array|UserTicker|null|ActiveRecord
     * @throws HttpException
     */
    private function getModel($with = true)
    {
        $model = UserTicker::getModel();
        if ($with) {
            $model->with(
                [
                    'settings',
                    'settings.system',
                    'settings.system.tv',
                    'settings.system.city',
                ]
            );
        }

        $model = $model->one();

        if ($model === null) {
            $model = new UserTicker();
            $model->guest_id = Yii::$app->guest->id;
            $city = Yii::$app->request->get('city');
            if (!empty($city)) {
                $city = City::findOne(['slug' => $city]);
                if (empty($city)) {
                    throw new NotFoundHttpException('Город не найден.');
                }
                $model->city_id = $city->id;
            } else {
                $model->city_id = Yii::$app->city->id;
            }
            if (!$model->save()) {
                throw new HttpException('Ошибка добавления');
            }
        }

        return $model;
    }

    /**
     * Form
     * @return string
     * @throws HttpException
     */
    public function actionForm()
    {
        // Selected tv
        $tv = Yii::$app->request->get('tv');

        $model = $this->getModel(false);
        $selectedCity = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save(false);
            $model->channels = is_array($model->channels) ? $model->channels : [];

            // Clear tvs
            $channels = array_merge($model->channels, $model->hidden_channels);
            $model->hidden_channels = $model->channels = $channels;
            $condition = empty($channels)
                ? ['user_ticker_id' => $model->id]
                : [
                    'AND',
                    'user_ticker_id = ' . $model->id,
                    ['NOT IN', 'ticker_id', $channels],
                ];
            UserTickerTv::deleteAll($condition);

            // Add new channels
            $newChannels = $model->channels;
            foreach ($model->settings as $channel) {
                $keys = array_keys($newChannels, $channel->ticker_id);
                if (!empty($keys)) {
                    unset($newChannels[$keys[0]]);
                }
            }
            if (!empty($newChannels)) {
                foreach ($newChannels as $channel) {
                    $newChannel = new UserTickerTv();
                    $newChannel->user_ticker_id = $model->id;
                    $newChannel->ticker_id = (int)$channel;
                    $newChannel->save();
                }
            }

            $phones = Yii::$app->request->post('phones');
            if (!empty($phones)) {
                foreach (UserTickerPhone::findAll(['user_ticker_id' => $model->id]) as $phone) {
                    if (!isset($phones[$phone->id]) || empty($phones[$phone->id]['phone'])) {
                        $phone->delete();
                        continue;
                    }
                    if ($phones[$phone->id]['phone'] == $phone->phone && $phones[$phone->id]['type'] == $phone->type) {
                        continue;
                    }
                    $phone->phone = $phones[$phone->id]['phone'];
                    $phone->type = $phones[$phone->id]['type'];
                    $phone->save();
                }
            }

            if (!empty($model->new_phone) && !empty($model->new_phone_type)) {
                $newPhone = new UserTickerPhone();
                $newPhone->user_ticker_id = $model->id;
                $newPhone->phone = $model->new_phone;
                $newPhone->type = $model->new_phone_type;
                $newPhone->save();
                $model->new_phone = '';
                $model->new_phone_type = UserTickerPhone::TYPE_CITY;
            }
        } else {
            foreach ($model->settings as $channel) {
                $model->channels[] = $model->hidden_channels[] = $channel->ticker_id;
            }
            $getCity = Yii::$app->request->get('city');
            if (!empty($getCity)) {
                $model->city_id = $getCity;
            }
        }

        // Условие для фильтрации городов и каналов в зависимости от выбранного типа
        if ($model->type == UserTicker::TYPE_COMM) {
            $condition = ['OR', 'price_text_block IS NOT NULL', 'price_comm IS NOT NULL'];
        } else {
            $condition = ['OR', 'price_text_block IS NOT NULL', 'price_no_comm IS NOT NULL'];
        }

        // Список городов
        $items['cities'] = ['' => 'Все города'];
        $cities = City::find()
            ->where(['city.status' => City::STATUS_ACTIVE])
            ->joinWith('region')
            ->orderBy(['region.sort' => SORT_ASC, 'city.sort' => SORT_ASC, 'city.name' => SORT_ASC])
            ->all();
        foreach ($cities as $city) {
            $items['cities'][empty($city->region) ? '-' : $city->region->name][$city->id] = $city->name;
            if ($model->city_id == $city->id) {
                $selectedCity = $city;
            }
        }

        // Список каналов
        $channels = Ticker::find()
            ->where(['ticker.status' => Ticker::STATUS_ACTIVE])
            ->andWhere($condition)
            ->with(['tv', 'tv.image', 'city'])
            ->orderBy(['ticker.sort' => SORT_ASC, 'ticker.id' => SORT_ASC]);
        if (!empty($model->city_id)) {
            $channels->andFilterWhere(['city_id' => $model->city_id]);
        }
        if (!empty($tv)) {
            $channels->andFilterWhere(['tv_id' => $tv]);
        }
        $channelsCnt = $channels->count();
        if (!$model->show_all_cities) {
            $channels->limit(8);
        }
        foreach ($channels->all() as $ticker) {
            $items['channels']['items'][$ticker->id] = $ticker->tv->name . ' - ' . $ticker->city->name;
            $items['channels']['logos'][$ticker->id] =
                !empty($ticker->tv->image) ? $ticker->tv->image->getSrc(160, 80) : '';
            $items['channels']['price_types'][$ticker->id] = $ticker->getPriceType();

            $keys = array_keys($model->hidden_channels, $ticker->id);
            if (!empty($keys)) {
                unset($model->hidden_channels[$keys[0]]);
            }
        }

        return $this->renderAjax(
            'form',
            [
                'model' => $model,
                'items' => $items,
                'channelsCnt' => $channelsCnt,
                'city' => $selectedCity,
            ]
        );
    }

    /**
     * Basket (right block)
     * @return string
     * @throws HttpException
     */
    public function actionBasket()
    {
        $model = $this->getModel();
        $city = null;
        if (!empty($model->city_id)) {
            $city = City::findOne(['id' => $model->city_id]);
        }
        return $this->renderAjax('basket', $model->getItems() + ['city' => $city]);
    }

    /**
     * Delete position
     *
     * @param      $ticker_id
     * @param null $id
     *
     * @throws HttpException
     */
    public function actionDelete($ticker_id, $id = null) {
        if ($id !== null) {
            $model = UserTicker::find()
                ->where(['guest_id' => Yii::$app->guest->id, 'id' => $id])
                ->one();
        } else {
            $model = $this->getModel(false);
        }
        if ($model === null) {
            throw new HttpException('Запись не найдена');
        }
        UserTickerTv::deleteAll(['ticker_id' => $ticker_id, 'user_ticker_id' => $model->id]);
    }

    /**
     * Save to basket
     * @throws HttpException
     */
    public function actionSave()
    {
        $model = $this->getModel(false);
        $model->status = UserTicker::STATUS_BASKET;
        if (!$model->save()) {
            throw new HttpException('Ошибка добавления в корзину');
        }
        //$this->redirect(['/basket']);
    }

    /**
     * @return string
     * @throws HttpException
     */
    public function actionCalc()
    {
        $model = $this->getModel();
        return $this->renderAjax('calc', $model->getItems());
    }

    /**
     * @throws HttpException
     * @throws ExitException
     */
    public function actionUpdate()
    {
        $model = $this->getModel(false);
        $id = Yii::$app->request->post('id');
        /** @var UserTickerTv $ticker */
        $ticker = UserTickerTv::find()
            ->where(['user_ticker_id' => $model->id, 'ticker_id' => $id])
            ->one();
        if ($ticker === null) {
            throw new HttpException('Error update ticker');
        }

        $ticker->view_type = Yii::$app->request->post('view_type');
        $ticker->date_from = Yii::$app->request->post('date_from');
        $ticker->date_to = Yii::$app->request->post('date_to');
        if ($ticker->save()) {
            Yii::$app->end();
        } else {
            throw new HttpException('Ошибка обновления позиции');
        }
    }
}
