<?php
declare(strict_types=1);

namespace frontend\controllers;

use NikoM\Advertising\Ticker\Application\Basket\ShowBasket\ShowBasketQuery;
use NikoM\Advertising\Ticker\Application\Basket\ShowBasket\ShowBasketQueryHandler;
use NikoM\Advertising\Ticker\Application\City\ShowCities\ShowCitiesQuery;
use NikoM\Advertising\Ticker\Application\City\ShowCities\ShowCitiesQueryHandler;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\Basket;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\web\Controller;
use yii\web\Response;

class NewTickerController extends Controller
{
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->actionBasket();
    }

    public function actionUpdateBasket()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function getRegions()
    {
        $query = new ShowCitiesQuery();
        $handler = Yii::$container->get(ShowCitiesQueryHandler::class);

        return $handler->handle($query);
    }

    /**
     * @return Basket
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    private function actionBasket()
    {
        $query = new ShowBasketQuery();
        $query->buyerId = 251487; //Yii::$app->guest->id;

        $handler = Yii::$container->get(ShowBasketQueryHandler::class);

        return $handler->handle($query);
    }
}
