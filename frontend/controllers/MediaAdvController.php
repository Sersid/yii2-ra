<?php
declare(strict_types=1);

namespace frontend\controllers;

use common\models\OrderForm;
use common\repositories\BaseRepository;
use frontend\repositories\CityRepository;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\AddToBasket\AddToBasketCommand;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\AddToBasket\AddToBasketCommandHandler;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\DeleteFromBasket\DeleteFromBasketCommand;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\DeleteFromBasket\DeleteFromBasketCommandHandler;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasket\UpdateBasketCommand;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasket\UpdateBasketCommandHandler;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasketItem\UpdateBasketItemCommand;
use NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasketItem\UpdateBasketItemCommandHandler;
use NikoM\Advertising\AbstractMedia\Basket\Application\Query\ShowBasket\ShowBasketQuery;
use NikoM\Advertising\AbstractMedia\Basket\Application\Query\ShowBasket\ShowBasketQueryHandler;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketIsEmptyException;
use NikoM\Advertising\AbstractMedia\City\Application\Query\ShowCities\ShowCitiesQueryHandler;
use NikoM\Advertising\AbstractMedia\Offer\Application\Query\FindOffers\FindOffersQuery;
use NikoM\Advertising\AbstractMedia\Offer\Application\Query\FindOffers\FindOffersQueryHandler;
use NikoM\Advertising\AbstractMedia\Order\Application\Command\Checkout\CheckoutCommand;
use NikoM\Advertising\AbstractMedia\Order\Application\Command\Checkout\CheckoutCommandHandler;
use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use Yii;
use yii\web\Controller;
use yii\web\Response;

abstract class MediaAdvController extends Controller
{
    public function init()
    {
        parent::init();
        Yii::$container->setDefinitions($this->getDefinitions());
    }

    abstract protected function getDefinitions(): array;

    abstract protected function getOfferRepository(): BaseRepository;

    public function actionIndex(): string
    {
        $citySlug = trim((string)Yii::$app->request->get('citySlug'));
        if (!empty($citySlug)) {
            $city = (new CityRepository())->getBySlug($citySlug);
            $cityId = (int)$city->id;
        } else {
            $city = null;
            $cityId = null;
        }

        $offerSlug = (string)Yii::$app->request->get('offerSlug');
        if (!empty($offerSlug)) {
            $offer = $this->getOfferRepository()->getBySlug($offerSlug);
        } else {
            $offer = null;
        }

        $command = new FindOffersQuery();
        $command->cityId = $cityId;
        $command->offerSlug = empty($offerSlug) ? null : $offerSlug;
        $handler = Yii::$container->get(FindOffersQueryHandler::class);
        $offers = $handler->handle($command);

        return $this->render(
            'index',
            [
                'city' => $city,
                'offer' => $offer,
                'cityId' => $cityId,
                'regions' => is_null($cityId) ? $this->getRegions() : null,
                'offers' => $offers,
                'basket' => $this->getBasket(),
            ]
        );
    }

    private function getRegions(): RegionViewCollection
    {
        $handler = Yii::$container->get(ShowCitiesQueryHandler::class);

        return $handler->handle();
    }

    protected function getBasket(): IBasket
    {
        $command = new ShowBasketQuery();
        $command->buyerId = Yii::$app->guest->id;

        $handler = Yii::$container->get(ShowBasketQueryHandler::class);

        return $handler->handle($command);
    }

    public function actionBasket()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->getBasket();
    }

    public function actionOffers()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cityId =  (int)Yii::$app->request->get('cityId');
        $tvSlug = trim((string)Yii::$app->request->get('tv'));

        $command = new FindOffersQuery();
        $command->cityId = empty($cityId) ? null : $cityId;
        $command->offerSlug = empty($tvSlug) ? null : $tvSlug;
        $command->offset = (int)Yii::$app->request->get('offset');

        $handler = Yii::$container->get(FindOffersQueryHandler::class);

        return $handler->handle($command);
    }

    public function actionAddToBasket()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $command = new AddToBasketCommand();
        $command->buyerId = Yii::$app->guest->id;
        $command->offerId = (int)Yii::$app->request->get('offer');

        $handler = Yii::$container->get(AddToBasketCommandHandler::class);

        return $handler->handle($command);
    }

    public function actionUpdateBasket()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->enableCsrfValidation = false;

        $command = new UpdateBasketCommand();
        $command->buyerId = Yii::$app->guest->id;
        $command->advType = (int)Yii::$app->request->get('type');

        $handler = Yii::$container->get(UpdateBasketCommandHandler::class);

        return $handler->handle($command);
    }

    public function actionUpdateBasketItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $days = (int)Yii::$app->request->get('days');
        $duration = (int)Yii::$app->request->get('duration');
        $viewsPerDay = (int)Yii::$app->request->get('views-per-day');

        $command = new UpdateBasketItemCommand();
        $command->buyerId = Yii::$app->guest->id;
        $command->offerId = (int)Yii::$app->request->get('offer');
        $command->days = $days > 0 ? $days : null;
        $command->duration = $duration > 0 ? $duration : null;
        $command->viewsPerDay = $viewsPerDay > 0 ? $viewsPerDay : null;

        $handler = Yii::$container->get(UpdateBasketItemCommandHandler::class);

        return $handler->handle($command);
    }

    public function actionDeleteFromBasket()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $command = new DeleteFromBasketCommand();
        $command->buyerId = Yii::$app->guest->id;
        $command->offerId = (int)Yii::$app->request->get('offer');

        $handler = Yii::$container->get(DeleteFromBasketCommandHandler::class);

        return $handler->handle($command);
    }

    public function actionCheckout()
    {
        $model = new OrderForm();
        $success = false;
        $error = '';
        $dataLayer = [];
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $command = new CheckoutCommand();
            $command->buyerId = Yii::$app->guest->id;
            $command->name = $model->name;
            $command->phone = $model->phone;
            $command->email = $model->email;
            /** @var CheckoutCommandHandler $handler */
            $handler = Yii::$container->get(CheckoutCommandHandler::class);
            try {
                $handler->handle($command);
                $success = true;
            } catch (BasketIsEmptyException $e) {
                $error = 'Вы не выбрали каналы';
            }
        }

        return $this->render('/tv/checkout', [
            'model' => $model,
            'success' => $success,
            'error' => $error,
            'dataLayer' => $dataLayer,
        ]);
    }
}
