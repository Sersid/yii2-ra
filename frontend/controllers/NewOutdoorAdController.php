<?php
declare(strict_types=1);

namespace frontend\controllers;

use NikoM\Advertising\Outdoor\Application\Side\FindSide\FindSideQuery;
use NikoM\Advertising\Outdoor\Application\Side\FindSide\FindSideQueryHandler;
use NikoM\Advertising\Outdoor\Application\Side\FindSides\FindSidesQuery;
use NikoM\Advertising\Outdoor\Application\Side\FindSides\FindSidesQueryHandler;
use Yii;
use yii\web\Controller;

class NewOutdoorAdController extends Controller
{
    public function actionIndex(): string
    {
        $query = new FindSidesQuery();
        $query->cityId = $this->request->get('city');
        $query->streetId = $this->request->get('street');
        $query->formats = $this->request->get('formats');
        $query->periods = $this->request->get('periods');
        $query->priceFrom = $this->request->get('price-from');
        $query->priceTo = $this->request->get('price-to');
        $query->ratings = $this->request->get('ratings');
        $query->sides = $this->request->get('sides');
        $query->onlyWithPromotion = $this->request->get('only-promotion');
        $query->page = (int)$this->request->get('page');
        $handler = Yii::$container->get(FindSidesQueryHandler::class);

        return $this->render('index', ['sideCollection' => $handler->handle($query)]);
    }

    public function actionShow(): string
    {
        $query = new FindSideQuery();
        $query->fullNumber = (string)$this->request->get('full-number');
        $query->letter = (string)$this->request->get('letter');

        $handler = Yii::$container->get(FindSideQueryHandler::class);
        $side = $handler->handle($query);

        return $this->render('show', ['side' => $side]);
    }
}
