<?php

namespace frontend\controllers;

use common\components\Bitrix24;
use common\models\FormFeedbackModal;
use Yii;
use common\models\FormCallMe;
use common\models\FormFeedback;
use common\models\FormSorry;
use yii\web\Controller;

/**
 * FormsController implements the CRUD actions for FormCallMe model.
 */
class FormsController extends Controller
{
    /**
     * Creates a new FormCallMe model.
     * If creation is successful, the browser will be echo message.
     * @return mixed
     */
    public function actionCallMe()
    {
        $model = new FormCallMe();
        $success = $model->load(Yii::$app->request->post()) && $model->save();
        if ($success) {
            $model->sendEmail();
            /** @var Bitrix24 $bitrix24 */
            $bitrix24 = Yii::$app->bitrix24;
            $bitrix24->title = 'Форма заказа обратного звонка с сайта niko-m.ru';
            $bitrix24->name = $model->name;
            $bitrix24->email = $model->email;
            $bitrix24->phone = $model->phone;
            $bitrix24->addLead();
        }
        return $this->renderAjax(
            'call-me',
            [
                'model' => $model,
                'success' => $success,
            ]
        );
    }

    public function actionNewFeedback()
    {
        $model = new FormFeedback();
        $model->scenario = 'new';
        $model->ip = Yii::$app->request->userIP;
        $success = $model->load(Yii::$app->request->post()) && $model->validate();
        if ($success) {
            $model->name = $model->surname . ' ' . $model->name;
            $model->save(false);
            $model->sendEmail();
            /** @var  $bitrix24 Bitrix24 */
            $bitrix24 = Yii::$app->bitrix24;
            $bitrix24->title = 'Форма обратной связи с сайта niko-m.ru';
            $bitrix24->name = $model->name;
            $bitrix24->email = $model->email;
            $bitrix24->phone = $model->phone;
            $bitrix24->comments = $model->message;
            $bitrix24->addLead();
        }

        return $this->render('new-feedback', ['model' => $model, 'success' => $success]);
    }

    /**
     * @return string
     */
    public function actionFeedback()
    {
        $model = new FormFeedback();
        // $model->scenario = 'full';
        $model->ip = Yii::$app->request->userIP;
        $success = $model->load(Yii::$app->request->post()) && $model->save();
        if ($success) {
            $model->sendEmail();
            /** @var  $bitrix24 Bitrix24 */
            $bitrix24 = Yii::$app->bitrix24;
            $bitrix24->title = 'Форма обратной связи с сайта niko-m.ru';
            $bitrix24->name = $model->name;
            $bitrix24->email = $model->email;
            $bitrix24->phone = $model->phone;
            $bitrix24->comments = $model->message;
            $bitrix24->addLead();
        }
        return $this->renderAjax(
            'feedback',
            [
                'model' => $model,
                'success' => $success,
            ]
        );
    }

    /**
     * @return string
     */
    public function actionFeedbackModal()
    {
        $model = new FormFeedbackModal();
        $model->ip = Yii::$app->request->userIP;
        // $model->scenario = 'modal';
        $success = $model->load(Yii::$app->request->post()) && $model->save();
        if ($success) {
            $model->sendEmail();
            /** @var  $bitrix24 Bitrix24 */
            $bitrix24 = Yii::$app->bitrix24;
            $bitrix24->title = 'Форма обратной связи с сайта niko-m.ru';
            $bitrix24->name = $model->name;
            $bitrix24->email = $model->email;
            $bitrix24->phone = $model->phone;
            $bitrix24->comments = $model->message;
            $bitrix24->addLead();
        }
        return $this->renderAjax(
            'feedback-modal',
            [
                'model' => $model,
                'success' => $success,
            ]
        );
    }

    /**
     * @return string
     */
    public function actionSorry()
    {
        $model = new FormSorry();
        $success = $model->load(Yii::$app->request->post()) && $model->save();
        if ($success) {
            $model->sendEmail();
            /** @var  $bitrix24 Bitrix24 */
            $bitrix24 = Yii::$app->bitrix24;
            $bitrix24->title = 'Форма обратной связи с сайта niko-m.ru';
            $bitrix24->phone = $model->phone;
            $bitrix24->addLead();
        }
        return $this->renderAjax(
            (Yii::$app->request->post('view') == 'ajax' ? 'sorry-ajax' : 'sorry'),
            [
                'model' => $model,
                'success' => $success,
            ]
        );
    }
}
