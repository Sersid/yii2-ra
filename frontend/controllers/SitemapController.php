<?php

namespace frontend\controllers;

use common\models\City;
use common\models\OutdoorAdSide;
use common\models\TvAd;
use ErrorException;
use frontend\services\sitemap\SiteMap;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Sitemap controller
 */
class SitemapController extends Controller
{
    /**
     * @param string $sitemap
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionXml(string $sitemap = '')
    {
        try {
            $generator = new SiteMap($sitemap);
        } catch (ErrorException $e) {
            throw new NotFoundHttpException('Страница не найдена');
        }
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');
        return $generator->build();
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex()
    {
        // Outdoor ad
        $outdoorCities = [];
        foreach (
            OutdoorAdSide::find()
                ->where(['outdoor_ad_side.status' => OutdoorAdSide::STATUS_ENABLE])
                ->select(
                    ['city.id', 'city.name', 'parent_id']
                )
                ->joinWith(['parent', 'parent.city', 'parent.city.region'])
                ->groupBy('city.id')
                ->orderBy(
                    ['region.sort' => SORT_ASC, 'city.sort' => SORT_ASC, 'city.name' => SORT_ASC]
                )
                ->all() as $item
        ) {
            $row = [
                'label' => 'Наружная реклама ' . $item->parent->city->where,
                'url' => ['/outdoor-ad/' . $item->parent->city->slug],
            ];
            $row['items'] = [
                [
                    'label' => 'Рекламные щиты ' . $item->parent->city->where,
                    'url' => ['/outdoor-ad/reklamniy-shit-3x6-' . $item->parent->city->slug],
                ],
                [
                    'label' => 'Брандмауэеры ' . $item->parent->city->where,
                    'url' => ['/outdoor-ad/brandmauer-' . $item->parent->city->slug],
                ],
                [
                    'label' => 'Cветоидодные экраны ' . $item->parent->city->where,
                    'url' => ['/outdoor-ad/svetodiodniy-ekran-' . $item->parent->city->slug],
                ],
                [
                    'label' => 'Рекламные ограждения ' . $item->parent->city->where,
                    'url' => ['/outdoor-ad/reklamnoe-ograjdenie-' . $item->parent->city->slug],
                ],
            ];
            $outdoorCities[] = $row;
        }

        $cities = City::find()
            ->orderBy(['sort' => SORT_ASC, 'salary' => SORT_DESC])
            ->active()
            ->all();

        // Cities
        $citiesItems = [];
        foreach ($cities as $item) {
            $citiesItems[] = [
                'label' => $item->name,
                'url' => ['/cities/' . $item->slug],
            ];
        }

        $tvItems = [];
        foreach ($cities as $item) {
            $row = [
                'label' => 'Реклама на телевидении ' . $item->where,
                'url' => ['/tv/' . $item->slug],
            ];
            $tvs = TvAd::find()
                ->where(
                    [
                        'city_id' => $item->id,
                        'status' => TvAd::STATUS_ACTIVE,
                    ]
                )
                ->groupBy('tv_id')
                ->with(['tv'])
                ->orderBy(['tv_ad.sort' => SORT_ASC])
                ->all();

            foreach ($tvs as $tv) {
                $row['items'][] = [
                    'label' => 'Реклама на ' . $tv->tv->name . ' ' . $item->where,
                    'url' => ['/tv/' . $item->slug . '/' . $tv->tv->slug],
                ];
            }

            $tvItems[] = $row;
        }

        $tickerItems = [];
        foreach ($cities as $item) {
            $tickerItems[] = [
                'label' => 'Бегущая строка ' . $item->where,
                'url' => ['/ticker/' . $item->slug],
            ];
        }

        $radioItems = [];
        foreach ($cities as $item) {
            $radioItems[] = [
                'label' => 'Реклама на радио ' . $item->where,
                'url' => ['/radio/' . $item->slug],
            ];
        }

        $items = [
            [
                'label' => 'Наружная реклама',
                'url' => ['/outdoor-ad'],
                'items' => $outdoorCities,
            ],
            [
                'label' => 'Реклама на ТВ',
                'url' => ['/tv'],
                'items' => $tvItems,
            ],
            [
                'label' => 'Бегущая строка на ТВ',
                'url' => ['/ticker'],
                'items' => $tickerItems,
            ],
            [
                'label' => 'Реклама на радио',
                'url' => ['/radio'],
                'items' => $radioItems,
            ],
            ['label' => 'Комплексная реклама', 'url' => ['/p/complex']],
            [
                'label' => 'Реклама в городах',
                'url' => ['/cities'],
                'items' => $citiesItems,
            ],
        ];

        return $this->render('index', ['items' => $items]);
    }
}
