<?php

namespace frontend\controllers;

use common\models\OutdoorAd;
use common\models\OutdoorAdSide;
use common\models\UserOutdoorAd;
use frontend\models\OutdoorSearch;
use frontend\models\OutdoorSearchFacets;
use frontend\models\OutdoorSearchMap;
use frontend\models\OutdoorSearchRadius;
use frontend\models\OutdoorSearchTable;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * OutdoorAd controller
 */
class OutdoorAdController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'corsFilter' => [
                'class' => Cors::class,
            ],
        ];
        return array_merge(parent::behaviors(), $behaviors);
    }
    /**
     * Таблица
     * @return array|string
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionIndex()
    {
        $searchModel = new OutdoorSearchTable();
        $dataProvider = $searchModel->getActiveDataProvider();
        if (Yii::$app->request->isAjax && Yii::$app->request->get('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'html' => $this->renderAjax('table-ajax', ['dataProvider' => $dataProvider]),
                'items' => OutdoorSearchFacets::getItems(),
            ];
        } else {
            return $this->render(
                'index',
                [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'items' => OutdoorSearchFacets::getItems(),
                ]
            );
        }
    }

    /**
     * Карта
     * @return string
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionMap()
    {
        $searchModel = new OutdoorSearchMap();
        $searchModel->loadParams();
        return $this->render(
            'map',
            [
                'searchModel' => $searchModel,
                'items' => OutdoorSearchFacets::getItems(),
            ]
        );
    }

    /**
     * По радиусу
     * @return string
     * @throws Throwable
     */
    public function actionRadius()
    {
        $model = new OutdoorSearchRadius(['scenario' => OutdoorSearchRadius::SCENARIO_HTML]);
        return $this->render(
            'radius',
            [
                'searchModel' => $model,
                'items' => $model->getItems(),
            ]
        );
    }

    /**
     * Список городов
     * @return array
     */
    public function actionGetCities()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return (new OutdoorSearchRadius)->getCities();
    }

    /**
     * Поиск поверхностей в радиусе
     * @param array $query
     *
     * @return array
     */
    public function actionRadiusSearch(array $query)
    {
        $arResult = [];
        foreach ($query as $q) {
            $arQuery = json_decode($q);
            $arQuery = [
                'lat' => (float)ArrayHelper::getValue($arQuery, 'lat'),
                'lon' => (float)ArrayHelper::getValue($arQuery, 'lon'),
                'radius' => (float)ArrayHelper::getValue($arQuery, 'radius'),
            ];
            if (empty($arQuery['lat']) || empty($arQuery['lon']) || $arQuery['radius'] <= 50) {
                continue;
            }
            $model = new OutdoorSearchRadius(['scenario' => OutdoorSearchRadius::SCENARIO_API]);
            $model->setAttributes($arQuery);
            foreach ($model->getSearchQuery()->asArray()->all() as $arItem) {
                if (isset($arResult[$arItem['id']])) {
                    continue;
                }
                $arResult[$arItem['id']] = $arItem;
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return array_values($arResult);
    }

    /**
     * Аякс карта
     * @return array
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function actionMapAjax()
    {
        $searchModel = new OutdoorSearchMap();
        $searchModel->loadParams();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'map' => $searchModel->getSearchQuery()
                ->asArray()
                ->all(),
            'items' => OutdoorSearchFacets::getItems(),
        ];
    }

    /**
     * Загрузка содержимого карты
     *
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMapContent($id)
    {
        $model = OutdoorAd::find()
            ->where(['outdoor_ad.id' => $id])
            ->joinWith(
                [
                    'outdoorAdSides' => function ($query) {
                        $query->andWhere(['outdoor_ad_side.status' => OutdoorAdSide::STATUS_ENABLE]);
                    },
                ]
            )
            ->one();
        if (empty($model)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->renderAjax('map-content', ['model' => $model]);
    }

    /**
     * Детальный просмотр стороны
     * @return string
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     */
    public function actionView()
    {
        $side = array_search(Yii::$app->request->get('side'), OutdoorAdSide::getLatSides());
        if ($side === false) {
            throw new NotFoundHttpException('Услуга не найдена.');
        }
        $number = Yii::$app->request->get('number');
        if (empty($number)) {
            throw new NotFoundHttpException('Поверхность не найдена.');
        }
        $model = OutdoorAdSide::find()
            ->where(
                [
                    'outdoor_ad.full_number' => $number,
                    'outdoor_ad_side.side' => $side,
                    'outdoor_ad_side.status' => OutdoorAdSide::STATUS_ENABLE,
                ]
            )
            ->joinWith(['parent'])
            ->with('images')
            ->one();
        if (!$model) {
            throw new NotFoundHttpException('Поверхность не найдена.');
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view-ajax', ['model' => $model]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Add to basket
     */
    public function actionAddToBasket()
    {
        $outdoorAdId = Yii::$app->request->post('id');
        if (empty($outdoorAdId)) {
            return;
        }
        $isUpdate = true;
        $model = UserOutdoorAd::find()
            ->where(
                [
                    'outdoor_ad_id' => $outdoorAdId,
                    'guest_id' => Yii::$app->guest->id,
                    'user_outdoor_ad.status' => UserOutdoorAd::STATUS_BASKET,
                ]
            )
            ->one();
        if ($model === null) {
            $isUpdate = false;
            $model = new UserOutdoorAd();
        }
        $mounts = Yii::$app->request->post('mounts');
        $model->attributes = [
            'guest_id' => Yii::$app->guest->id,
            'outdoor_ad_id' => $outdoorAdId,
            'install' => Yii::$app->request->post('install'),
            'print' => Yii::$app->request->post('print'),
            'design' => Yii::$app->request->post('design'),
            'dates' => is_array($mounts) ? implode(',', $mounts) : '',
        ];
        if ($model->save()) {
            if ($isUpdate) {
                echo '<p>Значение обновлено</p>';
            } else {
                echo '<p>Добавлено в корзину</p>';
            }
            echo '<p>Сумма: ' . number_format($model->getPrice(), 0, ' ', ' ') . ' руб.</p>';
        } else {
            echo '<p style="color: red">' . implode('<br>', $model->getFirstErrors()) . '</p>';
        }
    }

    /**
     * @param $city_id
     * @param $is_map
     *
     * @return string
     */
    public function actionStreets($city_id, $is_map)
    {
        $model = new OutdoorSearch();
        $model->city = (int)$city_id;
        return $this->renderAjax('streets', ['model' => $model]);
    }

    /**
     * Delete from basket
     *
     * @param integer $id
     *
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = UserOutdoorAd::find()
            ->where(
                [
                    'guest_id' => Yii::$app->guest->id,
                    'status' => UserOutdoorAd::STATUS_BASKET,
                    'id' => $id,
                ]
            )
            ->one();
        if ($model === null) {
            throw new HttpException('Запись не найдена');
        }
        UserOutdoorAd::deleteAll(
            [
                'guest_id' => Yii::$app->guest->id,
                'status' => UserOutdoorAd::STATUS_BASKET,
                'id' => $id,
            ]
        );
    }
}
