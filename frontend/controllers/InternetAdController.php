<?php

namespace frontend\controllers;

use common\models\AdvCase;
use frontend\repositories\AdvCaseRepository;
use frontend\repositories\CityRepository;
use yii\web\Controller;

/**
 * Class InternetAdController
 * @package frontend\controllers
 */
class InternetAdController extends Controller
{
    /** @var CityRepository */
    protected $cityRepository;
    /** @var AdvCaseRepository */
    protected $caseRepository;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        $this->cityRepository = new CityRepository();
        $this->caseRepository = new AdvCaseRepository();
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function actionIndex($slug = '')
    {
        return $this->render('index', [
            'city' => $this->cityRepository->getBySlug($slug),
            'cases' => $this->caseRepository->getByCategories([AdvCase::CATEGORY_SEO, AdvCase::CATEGORY_MARKETING]),
        ]);
    }
}
