<?php

namespace frontend\controllers;

use frontend\repositories\FaqRepository;
use yii\web\Controller;

/**
 * Faq controller
 * @package frontend\controllers
 */
class FaqController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['faq' => (new FaqRepository)->getAll()]);
    }
}
