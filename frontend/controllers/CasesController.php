<?php

namespace frontend\controllers;

use frontend\repositories\AdvCaseRepository;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class CasesController
 * @package frontend\controllers
 */
class CasesController extends Controller
{
    /** @var AdvCaseRepository */
    protected $repository;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        $this->repository = new AdvCaseRepository();
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->repository->getMainListQuery(),
            'sort' => ['defaultOrder' => ['sort' => SORT_ASC, 'id' => SORT_DESC]],
            'pagination' => ['pageSize' => 6],
        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param string $slug
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView(string $slug)
    {
        $model = $this->repository->getBySlug($slug);
        if (empty($model)) {
            throw new NotFoundHttpException('Кейс не найден.');
        }
        $moreCases = $this->repository->getMoreCases($slug);
        return $this->render('view', ['model' => $model, 'cases' => $moreCases]);
    }
}
