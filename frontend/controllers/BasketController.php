<?php

namespace frontend\controllers;

use common\models\Order;
use common\models\QuickOrder;
use common\models\QuickOutdoorOrder;
use common\models\UserOutdoorAd;
use common\models\UserRadioAd;
use common\models\UserTicker;
use common\models\UserTvAd;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\Controller;

/**
 * Contacts controller
 */
class BasketController extends Controller
{
    /**
     * @var array Tickers
     */
    private $_tickers;

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Tickers
     * @return string
     */
    public function actionTickers()
    {
        return $this->renderAjax('tickers', $this->_getTickerItems());
    }

    /**
     * Tickers
     * @return array
     */
    private function _getTickerItems()
    {
        if ($this->_tickers === null) {
            $models = UserTicker::find()->where(
                [
                    'user_ticker.status' => UserTicker::STATUS_BASKET,
                    'guest_id' => Yii::$app->guest->id,
                ]
            )->with(
                [
                    'settings',
                    'settings.system',
                    'settings.system.tv',
                    'settings.system.city',
                ]
            )->all();
            $return = [
                'items' => [],
                'count' => 0,
                'total' => 0,
            ];
            foreach ($models as $model) {
                $items = $model->getItems();
                $return['items'] = array_merge($return['items'], $items['items']);
                $return['count'] += count($items['items']);
                $return['total'] += $items['total'];
            }
            $this->_tickers = $return;
        }

        return $this->_tickers;
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionOutdoorAd()
    {
        return $this->renderAjax('outdoor-ad', ['items' => $this->getOutdoorAdItems()]);
    }

    /**
     * Наружная реклама
     * @return array
     * @throws InvalidArgumentException
     */
    protected function getOutdoorAdItems()
    {
        $models = UserOutdoorAd::find()->where(
            [
                'user_outdoor_ad.status' => UserOutdoorAd::STATUS_BASKET,
                'guest_id' => Yii::$app->guest->id,
            ]
        )->joinWith(
            [
                'side',
                'side.parent',
                'side.parent.city',
                'side.parent.street',
                'side.image',
            ]
        )->all();

        $ruDates = [
            1 => 'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь',
        ];
        $dates = [];
        for ($i = 1; $i <= 9; $i++) {
            $time = strtotime('+' . $i . ' month');
            $m = date('n', $time);
            $y = date('Y', $time);

            if ($i > 5) {
                $key = 2;
                $dates[$key]['disabled'] = true;
            } else if ($y > date('Y')) {
                $key = 1;
                $dates[$key]['disabled'] = false;
            } else {
                $key = 0;
                $dates[$key]['disabled'] = false;
            }
            $dates[$key]['year'] = $y;
            $dates[$key]['months'][] = [
                'name' => $ruDates[$m],
                'num' => $m,
                'disabled' => $i > 5,
            ];
        }

        $items = [];
        /** @var UserOutdoorAd $model */
        foreach ($models as $model) {
            $row = [
                'id' => $model->id,
                'outdoor_ad_id' => $model->side->id,
                'number' => $model->side->getNumber(),
                'format' => $model->side->parent->getSurface(),
                'months' => $model->getCountMonth(),
                'total_price' => $model->getPrice(),
                'total_old_price' => $model->getOldPrice(),
                'price' => $model->side->getPrice(),
                'install' => $model->install,
                'print' => $model->print,
                'design' => $model->design,
                'side' => $model->side->getSide(),
                'rating' => $model->side->rating,
                'city' => $model->side->parent->city->name,
                'address' => $model->side->getFullAddress(),
                'price_print' => $model->side->getPricePrint(),
                'price_install' => $model->side->getPriceInstallation(),
                'price_design' => $model->side->getPriceDesign(),
                'image_small' => !empty($model->side->image) ? $model->side->image->getSrc(555, 270) : '',
                'image_big' => !empty($model->side->image) ? $model->side->image->getSrc() : '',
                'images' => $model->side->getImages(),
                'lat' => $model->side->parent->lat,
                'lon' => $model->side->parent->lon,
                'url' => $model->side->getDetailPageUrl(),
            ];

            foreach ($dates as $d => $date) {
                foreach ($date['months'] as $m => $month) {
                    $date['months'][$m]['is_busy'] = $model->side->getIsBusy($month['num'], $date['year']);
                    $date['months'][$m]['checked'] = in_array($month['num'] . '.' . $date['year'], $model->getDates());
                }
                $row['dates'][$d] = $date;
            }

            $items[] = $row;
        }

        return $items;
    }

    /**
     * Order
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionOrder()
    {
        $model = new Order();
        $model->guest_id = Yii::$app->guest->id;
        $success = false;
        $dataLayer = [];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;
            UserTicker::updateAll(
                [
                    'order_id' => $model->id,
                    'status' => UserTicker::STATUS_ORDER,
                ],
                [
                    'guest_id' => $model->guest_id,
                    'status' => UserTicker::STATUS_BASKET,
                ]
            );

            UserTvAd::updateAll(
                [
                    'order_id' => $model->id,
                    'status' => UserTvAd::STATUS_ORDER,
                ],
                [
                    'guest_id' => $model->guest_id,
                    'status' => UserTvAd::STATUS_BASKET,
                ]
            );

            UserRadioAd::updateAll(
                [
                    'order_id' => $model->id,
                    'status' => UserRadioAd::STATUS_ORDER,
                ],
                [
                    'guest_id' => $model->guest_id,
                    'status' => UserRadioAd::STATUS_BASKET,
                ]
            );

            UserOutdoorAd::updateAll(
                [
                    'order_id' => $model->id,
                    'status' => UserOutdoorAd::STATUS_ORDER,
                ],
                [
                    'guest_id' => $model->guest_id,
                    'status' => UserOutdoorAd::STATUS_BASKET,
                ]
            );
            $model->sendEmailToAdmin();
            $model->sendEmailToUser();

            /** @var  $bitrix24 \common\components\Bitrix24 */
            $bitrix24 = Yii::$app->bitrix24;
            $bitrix24->title = 'Заказ на сайте niko-m.ru';
            $bitrix24->name = $model->name;
            $bitrix24->email = $model->email;
            $bitrix24->phone = $model->phone;
            $bitrix24->addLead();

            $products = [];

            if (!empty($model->outdoors)) {
                foreach ($model->outdoors as $outdoor) {
                    $products[] = [
                        'id' => 'OUTDOORAD-' . $outdoor->side->id,
                        'name' => 'Наружная реклама: ' . $outdoor->side->number,
                        'price' => $outdoor->getPrice(),
                        'old_price' => $outdoor->getOldPrice(),
                        'category' => 'Наружная реклама',
                    ];
                }
            }

            if (!empty($model->tickers)) {
                foreach ($model->tickers as $ticker) {
                    $items = $ticker->getItems();
                    if (!empty($items)) {
                        foreach ($items['items'] as $item) {
                            $products[] = [
                                'id' => 'TICKER-' . $item['ticker_id'],
                                'name' => 'Бегущая строка: ' . $item['name'],
                                'price' => $item['total'],
                                'category' => 'Бегущая строка',
                            ];
                        }
                    }
                }
            }

            if (!empty($model->radios)) {
                foreach ($model->radios as $radio) {
                    foreach ($radio->userRadioAdRadios as $item) {
                        $products[] = [
                            'id' => 'RADIO-' . $item->radio_ad_id,
                            'name' => 'Реклама на радио: ' . $item->radioAd->radio->name . ' - '
                                      . $item->radioAd->city->name,
                            'price' => $radio->getPrice($item),
                            'category' => 'Реклама на радио',
                        ];
                    }
                }
            }

            if (!empty($model->tvs)) {
                foreach ($model->tvs as $tv) {
                    foreach ($tv->userTvAdTvs as $item) {
                        $products[] = [
                            'id' => 'TV-' . $item->tv_ad_id,
                            'name' => 'Реклама на тв: ' . $item->tvAd->tv->name . ' - ' . $item->tvAd->city->name,
                            'price' => $tv->getPrice($item),
                            'category' => 'Реклама на тв',
                        ];
                    }
                }
            }

            $dataLayer = [
                'ecommerce' => [
                    'purchase' => [
                        'actionField' => ['id' => $model->getNumber()],
                        'products' => $products,
                    ],
                ],
            ];
        }
        return $this->render('order', ['model' => $model, 'success' => $success, 'dataLayer' => $dataLayer]);
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionQuickOrder()
    {
        $model = new QuickOrder();
        $model->guest_id = Yii::$app->guest->id;
        $success = false;
        $dataLayer = [];
        if ($model->load(Yii::$app->request->post())) {
            if (!empty($model->model_id) && $model->model_type == Yii::$app->session->get('last_model_type')
                && $model->model_id == Yii::$app->session->get('last_model_id')) {
                $success = true;
            } else {
                if ($model->save()) {
                    Yii::$app->session->set('last_model_id', $model->model_id);
                    Yii::$app->session->set('last_model_type', $model->model_type);
                    $success = true;
                    switch ($model->model_type) {
                        case 'tv':
                            UserTvAd::updateAll(
                                [
                                    'order_id' => $model->id,
                                    'status' => UserTvAd::STATUS_ORDER,
                                ],
                                [
                                    'id' => $model->model_id,
                                    'guest_id' => $model->guest_id,
                                ]
                            );
                            break;
                        case 'ticker':
                            UserTicker::updateAll(
                                [
                                    'order_id' => $model->id,
                                    'status' => UserTicker::STATUS_ORDER,
                                ],
                                [
                                    'id' => $model->model_id,
                                    'guest_id' => $model->guest_id,
                                ]
                            );
                            break;
                        case 'radio':
                            UserRadioAd::updateAll(
                                [
                                    'order_id' => $model->id,
                                    'status' => UserRadioAd::STATUS_ORDER,
                                ],
                                [
                                    'id' => $model->model_id,
                                    'guest_id' => $model->guest_id,
                                ]
                            );
                            break;
                        case 'outdoor-ad':
                            UserOutdoorAd::updateAll(
                                [
                                    'order_id' => $model->id,
                                    'status' => UserOutdoorAd::STATUS_ORDER,
                                ],
                                [
                                    'status' => UserOutdoorAd::STATUS_BASKET,
                                    'guest_id' => $model->guest_id,
                                ]
                            );
                            break;
                    }

                    $model->sendEmailToAdmin();
                    $model->sendEmailToUser();

                    /** @var  $bitrix24 \common\components\Bitrix24 */
                    $bitrix24 = Yii::$app->bitrix24;
                    $bitrix24->title = 'Заказ на сайте niko-m.ru';
                    $bitrix24->name = $model->name;
                    $bitrix24->email = $model->email;
                    $bitrix24->phone = $model->phone;
                    $bitrix24->addLead();

                    $products = [];

                    if (!empty($model->outdoors)) {
                        foreach ($model->outdoors as $outdoor) {
                            $products[] = [
                                'id' => 'OUTDOORAD-' . $outdoor->side->id,
                                'name' => 'Наружная реклама: ' . $outdoor->side->number,
                                'price' => $outdoor->getPrice(),
                                'category' => 'Наружная реклама',
                            ];
                        }
                    }

                    if (!empty($model->tickers)) {
                        foreach ($model->tickers as $ticker) {
                            $items = $ticker->getItems();
                            if (!empty($items)) {
                                foreach ($items['items'] as $item) {
                                    $products[] = [
                                        'id' => 'TICKER-' . $item['ticker_id'],
                                        'name' => 'Бегущая строка: ' . $item['name'],
                                        'price' => $item['total'],
                                        'category' => 'Бегущая строка',
                                    ];
                                }
                            }
                        }
                    }

                    if (!empty($model->radios)) {
                        foreach ($model->radios as $radio) {
                            foreach ($radio->userRadioAdRadios as $item) {
                                $products[] = [
                                    'id' => 'RADIO-' . $item->radio_ad_id,
                                    'name' => 'Реклама на радио: ' . $item->radioAd->radio->name . ' - '
                                              . $item->radioAd->city->name,
                                    'price' => $radio->getPrice($item),
                                    'category' => 'Реклама на радио',
                                ];
                            }
                        }
                    }

                    if (!empty($model->tvs)) {
                        foreach ($model->tvs as $tv) {
                            foreach ($tv->userTvAdTvs as $item) {
                                $products[] = [
                                    'id' => 'TV-' . $item->tv_ad_id,
                                    'name' => 'Реклама на тв: ' . $item->tvAd->tv->name . ' - '
                                              . $item->tvAd->city->name,
                                    'price' => $tv->getPrice($item),
                                    'category' => 'Реклама на тв',
                                ];
                            }
                        }
                    }

                    $dataLayer = [
                        'ecommerce' => [
                            'purchase' => [
                                'actionField' => ['id' => $model->getNumber()],
                                'products' => $products,
                            ],
                        ],
                    ];
                }
            }
        }
        return $this->render('quick-order', ['model' => $model, 'success' => $success, 'dataLayer' => $dataLayer]);
    }

    public function actionQuickOutdoorOrder()
    {
        $model = new QuickOutdoorOrder();
        $model->guest_id = Yii::$app->guest->id;
        $success = false;
        $dataLayer = [];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;

            $model->sendEmailToAdmin();
            $model->sendEmailToUser();

            /** @var  $bitrix24 \common\components\Bitrix24 */
            $bitrix24 = Yii::$app->bitrix24;
            $bitrix24->title = 'Заказ на сайте niko-m.ru';
            $bitrix24->name = $model->name;
            $bitrix24->email = $model->email;
            $bitrix24->phone = $model->phone;
            $bitrix24->addLead();

            $products = [];
            $dataLayer = [
                'ecommerce' => [
                    'purchase' => [
                        'actionField' => ['id' => $model->getNumber()],
                        'products' => $products,
                    ],
                ],
            ];
        }
        return $this->render('quick-order', ['model' => $model, 'success' => $success, 'dataLayer' => $dataLayer]);
    }

    /**
     * Tv
     * @return string
     */
    public function actionTv()
    {
        return $this->renderAjax('tv', ['items' => $this->_getTvItems()]);
    }

    /**
     * @return array
     */
    private function _getTvItems()
    {
        $models = UserTvAd::find()->where(
            [
                'user_tv_ad.status' => UserTvAd::STATUS_BASKET,
                'guest_id' => Yii::$app->guest->id,
            ]
        )->joinWith(
            [
                'userTvAdTvs',
                'userTvAdTvs.tvAd',
                'userTvAdTvs.tvAd.tv',
                'userTvAdTvs.tvAd.city',
            ]
        )->all();
        $items = [];
        foreach ($models as $model) {
            foreach ($model->userTvAdTvs as $item) {
                $row = [
                    'id' => $model->id,
                    'tv_ad_id' => $item->tv_ad_id,
                    'tv' => $item->tvAd->tv->name,
                    'city' => $item->tvAd->city->name,
                    'type' => $model->getTypeName(),
                    'type_id' => $model->type,
                    'duration' => $item->duration,
                    'days' => $item->days,
                    'price' => $model->getPrice($item),
                ];

                $row['total'] = $row['price'] * $row['days'];
                $items[] = $row;
            }
        }

        return $items;
    }

    /**
     * Radio
     * @return string
     */
    public function actionRadio()
    {
        return $this->renderAjax('radio', ['items' => $this->_getRadioItems()]);
    }

    /**
     * Radio items
     * @return array
     */
    private function _getRadioItems()
    {
        $models = UserRadioAd::find()->where(
            [
                'user_radio_ad.status' => UserRadioAd::STATUS_BASKET,
                'guest_id' => Yii::$app->guest->id,
            ]
        )->joinWith(
            [
                'userRadioAdRadios',
                'userRadioAdRadios.radioAd',
                'userRadioAdRadios.radioAd.radio',
                'userRadioAdRadios.radioAd.city',
            ]
        )->all();
        $items = [];
        foreach ($models as $model) {
            foreach ($model->userRadioAdRadios as $item) {
                $row = [
                    'id' => $model->id,
                    'radio_ad_id' => $item->radio_ad_id,
                    'radio' => $item->radioAd->radio->name,
                    'city' => $item->radioAd->city->name,
                    'type' => $model->getTypeName(),
                    'type_id' => $model->type,
                    'duration' => $item->duration,
                    'days' => $item->days,
                    'price' => $model->getPrice($item),
                ];

                $row['total'] = $row['price'] * $row['days'];
                $items[] = $row;
            }
        }

        return $items;
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionWidget()
    {
        return $this->renderAjax(
            'widget',
            [
                'total' => $this->_getTotalPrice(),
                'ticker' => $this->_getTickerItems(),
                'radioItems' => $this->_getRadioItems(),
                'outdoorAdItems' => $this->getOutdoorAdItems(),
                'tvItems' => $this->_getTvItems(),
            ]
        );
    }

    /**
     * @return array
     */
    private function _getTotalPrice()
    {
        $total = 0;
        $cnt = 0;

        $tickers = $this->_getTickerItems();
        $total += $tickers['total'];
        $cnt += $tickers['count'];

        $models = UserTvAd::find()->where(
            [
                'user_tv_ad.status' => UserTvAd::STATUS_BASKET,
                'guest_id' => Yii::$app->guest->id,
            ]
        )->joinWith(['userTvAdTvs'])->all();

        /** @var UserTvAd $model */
        foreach ($models as $model) {
            foreach ($model->userTvAdTvs as $item) {
                $price = $model->getPrice($item);
                $total += $price * $item->days;
                $cnt++;
            }
        }

        $models = UserRadioAd::find()->where(
            [
                'user_radio_ad.status' => UserRadioAd::STATUS_BASKET,
                'guest_id' => Yii::$app->guest->id,
            ]
        )->joinWith(['userRadioAdRadios'])->all();

        /** @var UserRadioAd $model */
        foreach ($models as $model) {
            foreach ($model->userRadioAdRadios as $item) {
                $price = $model->getPrice($item);
                $total += $price * $item->days;
                $cnt++;
            }
        }

        $models = UserOutdoorAd::find()->where(
            [
                'user_outdoor_ad.status' => UserOutdoorAd::STATUS_BASKET,
                'guest_id' => Yii::$app->guest->id,
            ]
        )->joinWith('side')->all();
        /** @var UserOutdoorAd $model */
        foreach ($models as $model) {
            $total += $model->getPrice();
            $cnt++;
        }

        return [
            'cnt' => $cnt,
            'total' => $total,
        ];
    }

    public function actionTotal()
    {
        return $this->renderAjax('total', ['total' => $this->_getTotalPrice()]);
    }
}
