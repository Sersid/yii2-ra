<?php
declare(strict_types=1);

namespace frontend\controllers;

use NikoM\Advertising\Internet\FAQ\Application\FindCollection\FindFaqCollectionQuery;
use NikoM\Advertising\Internet\FAQ\Application\FindCollection\FindFaqCollectionQueryHandler;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqFilter;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqViewCollection;
use NikoM\Advertising\Internet\Offer\Application\ShowOffer\ShowOfferQuery;
use NikoM\Advertising\Internet\Offer\Application\ShowOffer\ShowOfferQueryHandler;
use NikoM\Advertising\Internet\Offer\Application\ShowOfferCollection\ShowOfferCollectionQuery;
use NikoM\Advertising\Internet\Offer\Application\ShowOfferCollection\ShowOfferCollectionQueryHandler;
use NikoM\Advertising\Internet\Offer\Domain\Exception\OfferNotFoundException;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferViewCollection;
use NikoM\Advertising\Internet\OurWork\Application\Query\FindOutWorkCollection\FindOutWorkCollectionQuery;
use NikoM\Advertising\Internet\OurWork\Application\Query\FindOutWorkCollection\FindOutWorkCollectionQueryHandler;
use NikoM\Advertising\Internet\OurWork\Domain\ReadModel\OurWorkViewCollection;
use NikoM\Location\City\Domain\Exception\CityNotFoundException;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class InternetAdvertisingController extends Controller
{
    public function actionIndex(): string
    {
        return $this->render(
            'index',
            [
                'offerCollection' => $this->findOfferCollection(),
                'faqCollection' => $this->findFaqCollection(),
                'ourWorkCollection' => $this->findOurWorkCollection(),
            ]
        );
    }

    public function actionOffer(): string
    {
        try {
            $query = new ShowOfferQuery();
            $query->offerSlug = $this->request->get('offer');
            $query->citySlug = $this->request->get('city');
            $handler = Yii::$container->get(ShowOfferQueryHandler::class);
            $offerView = $handler->handle($query);

            return $this->render(
                'offer',
                [
                    'offerView' => $offerView,
                    'ourWorkCollection' => $this->findOurWorkCollection(),
                ]
            );
        } catch (OfferNotFoundException | CityNotFoundException $e) {
            throw new NotFoundHttpException('Page not found');
        }
    }

    private function findOurWorkCollection(): OurWorkViewCollection
    {
        $query = new FindOutWorkCollectionQuery();
        $query->categories = [10, 20];
        $handler = Yii::$container->get(FindOutWorkCollectionQueryHandler::class);

        return $handler->handle($query);
    }

    private function findOfferCollection(): OfferViewCollection
    {
        $query = new ShowOfferCollectionQuery();
        $query->citySlug = $this->request->get('city');
        $query->filter->activeOnly = true;
        $handler = Yii::$container->get(ShowOfferCollectionQueryHandler::class);

        return $handler->handle($query);
    }

    private function findFaqCollection(): FaqViewCollection
    {
        $filter = new FaqFilter();
        $filter->activeOnly = true;
        $filter->globalOnly = true;
        $query = new FindFaqCollectionQuery();
        $query->filter = $filter;
        $handler = Yii::$container->get(FindFaqCollectionQueryHandler::class);

        return $handler->handle($query);
    }
}
