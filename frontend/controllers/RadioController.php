<?php
declare(strict_types=1);

namespace frontend\controllers;

use common\repositories\BaseRepository;
use frontend\repositories\RadioRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketGlobalDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Service\IBasketServiceProvider;
use NikoM\Advertising\AbstractMedia\City\Domain\ICityFetcher;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferRepository;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderType;
use NikoM\Advertising\Radio\Basket\Domain\Service\BasketServiceProvider;
use NikoM\Advertising\Radio\Basket\Infrastructure\Persistence\BasketGlobalDiscount;
use NikoM\Advertising\Radio\Basket\Infrastructure\Persistence\BasketRepository;
use NikoM\Advertising\Radio\City\Infrastructure\Persistence\CityFetcher;
use NikoM\Advertising\Radio\Offer\Infrastructure\Persistence\OfferFetcher;
use NikoM\Advertising\Radio\Offer\Infrastructure\Persistence\OfferRepository;
use NikoM\Advertising\Radio\Order\Domain\Entity\OrderType;

class RadioController extends MediaAdvController
{
    protected function getDefinitions(): array
    {
        return [
            ICityFetcher::class => CityFetcher::class,
            IOfferFetcher::class => OfferFetcher::class,
            IBasketServiceProvider::class => BasketServiceProvider::class,
            IBasketRepository::class => BasketRepository::class,
            IOfferRepository::class => OfferRepository::class,
            IBasketGlobalDiscount::class => BasketGlobalDiscount::class,
            IOrderType::class => OrderType::class,
        ];
    }

    protected function getOfferRepository(): BaseRepository
    {
        return new RadioRepository();
    }
}
