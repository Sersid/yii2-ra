<?php

namespace frontend\controllers;

use frontend\repositories\PageRepository;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Faq controller
 */
class PageController extends Controller
{
    /** @var PageRepository */
    protected $repository;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->repository = new PageRepository();
        parent::init();
    }

    /**
     * @param string $slug
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex(string $slug)
    {
        $model = $this->repository->getBySlug($slug);
        if (empty($model)) {
            throw new NotFoundHttpException('Страница не найдена.');
        }
        return $this->render('view', ['model' => $model]);
    }
}
