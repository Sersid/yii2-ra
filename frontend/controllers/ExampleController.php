<?php

namespace frontend\controllers;

use yii\web\Controller;

/**
 * Contacts controller
 */
class ExampleController extends Controller
{
    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
