<?php
declare(strict_types=1);

namespace frontend\controllers;

use common\repositories\BaseRepository;
use frontend\repositories\TvRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketGlobalDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Service\IBasketServiceProvider;
use NikoM\Advertising\AbstractMedia\City\Domain\ICityFetcher;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferFetcher as IAbstractOfferFetcherAlias;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferRepository;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderType;
use NikoM\Advertising\Tv\Basket\Domain\Service\BasketServiceProvider;
use NikoM\Advertising\Tv\Basket\Infrastructure\Persistence\BasketGlobalDiscount;
use NikoM\Advertising\Tv\Basket\Infrastructure\Persistence\BasketRepository;
use NikoM\Advertising\Tv\City\Infrastructure\Persistence\CityFetcher;
use NikoM\Advertising\Tv\Offer\Application\ShowAdvanced\ShowAdvancedQuery;
use NikoM\Advertising\Tv\Offer\Application\ShowAdvanced\ShowAdvancedQueryHandler;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\Tv\Offer\Infrastructure\Persistence\OfferFetcher;
use NikoM\Advertising\Tv\Offer\Infrastructure\Persistence\OfferRepository;
use NikoM\Advertising\Tv\Order\Domain\Entity\OrderType;
use Yii;

class TvController extends MediaAdvController
{
    protected function getDefinitions(): array
    {
        return [
            ICityFetcher::class => CityFetcher::class,
            IAbstractOfferFetcherAlias::class => OfferFetcher::class,
            IOfferFetcher::class => OfferFetcher::class,
            IBasketServiceProvider::class => BasketServiceProvider::class,
            IBasketRepository::class => BasketRepository::class,
            IOfferRepository::class => OfferRepository::class,
            IBasketGlobalDiscount::class => BasketGlobalDiscount::class,
            IOrderType::class => OrderType::class,
        ];
    }

    protected function getOfferRepository(): BaseRepository
    {
        return new TvRepository();
    }

    public function actionCityAndChannel(): string
    {
        $query = new ShowAdvancedQuery();
        $query->citySlug = (string)Yii::$app->request->get('citySlug');
        $query->channelSlug = (string)Yii::$app->request->get('channelSlug');

        $handler = Yii::$container->get(ShowAdvancedQueryHandler::class);
        $offer = $handler->handle($query);

        return $this->render('city-and-channel', ['offer' => $offer, 'basket' => $this->getBasket()]);
    }
}
