<?php
/**
 * Created by PhpStorm.
 * User: Yulai
 * Date: 21.02.2017
 * Time: 17:57
 */

namespace frontend\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\models\Book;

class BookController extends Controller
{
    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        $query = Book::find()->where(['lvl' => 1, 'active' => true]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['lft' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param      $slug
     * @param null $node_id
     *
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($slug, $node_id = null)
    {
        $model = Book::findOne(['slug' => $slug]);

        return $this->render('view', [
            'data' => $model,
        ]);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Book::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Материал не найден.');
        }
    }
}
