<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\Text as Model;

/**
 * Edit mode controller
 */
class EditModeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex($type = 'disabled')
    {
        $type = $type == 'enable' ? 'enable' : 'disabled';
        if ($type == 'enable') {
            Yii::$app->session->set('edit-mode', true);
        } else {
            Yii::$app->session->set('edit-mode', false);
        }
        $url = Yii::$app->request->get('back_url');
        $url = !empty($url) ? urldecode($url) : ['/'];
        return $this->redirect($url);
    }

    public function actionText()
    {
        $key = Yii::$app->request->get('key');
        $mode = Yii::$app->request->get('mode');
        $default = Yii::$app->request->get('default');

        $model = Model::findOne(['key' => $key]);
        if ($model === null) {
            $model = new Model();
            $model->key = $key;
            $model->value = $default;
        }

        $success = false;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->cache->set('text-' . $model->key, $model->value);
            $success = true;
        }

        return $this->renderPartial('text', ['model' => $model, 'mode' => $mode, 'success' => $success]);
    }
}
