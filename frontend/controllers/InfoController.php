<?php

namespace frontend\controllers;

use frontend\repositories\InfoCategoryRepository;
use frontend\repositories\InfoRepository;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Info controller
 * @package frontend\controllers
 */
class InfoController extends Controller
{
    /** @var InfoRepository */
    protected $infoRepository;

    public function init()
    {
        parent::init();
        $this->infoRepository = new InfoRepository();
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $category = Yii::$app->request->get('category');
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $this->infoRepository->getListQuery($category),
                'sort' => ['defaultOrder' => ['sort' => SORT_ASC, 'id' => SORT_DESC]],
                'pagination' => [
                    'pageSize' => 6,
                ],
            ]
        );
        return $this->render(
            'index',
            [
                'activeCategory' => $category,
                'categories' => (new InfoCategoryRepository())->getForMenu(),
                'category' => $category,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        $slug = Yii::$app->request->get('item');
        $category = Yii::$app->request->get('category');
        $model = $this->infoRepository->getBySlug($slug);
        if (empty($model)) {
            throw new NotFoundHttpException('Материал не найден.');
        }
        return $this->render('view', ['activeCategory' => $category, 'model' => $model]);
    }
}
