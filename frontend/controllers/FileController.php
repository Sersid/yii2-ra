<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;

/**
 * Class PhotoController
 * @package frontend\controllers
 */
class FileController extends \common\controllers\FileController
{
    function actionPhotos($img, $ext)
    {
        $path = \Yii::getAlias('@frontend/web/photos/' . $img . '.' . $ext);
        if (!is_file($path)) {
            throw new NotFoundHttpException('File ont found.');
        }
        header("Content-Type: " . \yii\helpers\FileHelper::getMimeType($path));
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: Wed, 07 Jun 2017 11:55:43 GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        readfile($path);
    }

    function actionImages($img, $ext)
    {
        $path = \Yii::getAlias('@frontend/web/images/' . $img . '.' . $ext);
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: Wed, 07 Jun 2017 11:55:43 GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        header("Content-Type: " . \yii\helpers\FileHelper::getMimeType($path));
        readfile($path);
    }
}
