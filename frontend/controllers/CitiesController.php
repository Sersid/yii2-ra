<?php

namespace frontend\controllers;

use frontend\repositories\CityRepository;
use frontend\repositories\OutdoorAdSideRepository;
use frontend\repositories\RadioAdRepository;
use frontend\repositories\TickerRepository;
use frontend\repositories\TvAdRepository;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Cities controller
 * @package frontend\controllers
 */
class CitiesController extends Controller
{
    /** @var CityRepository */
    protected $cityRepository;
    /** @var TvAdRepository */
    protected $tvAdRepository;
    /** @var RadioAdRepository */
    protected $radioAdRepository;
    /** @var TickerRepository */
    protected $tickerRepository;
    /** @var OutdoorAdSideRepository */
    protected $outdoorAdSideRepository;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        $this->cityRepository = new CityRepository();
        $this->tvAdRepository = new TvAdRepository();
        $this->radioAdRepository = new RadioAdRepository();
        $this->tickerRepository = new TickerRepository();
        $this->outdoorAdSideRepository = new OutdoorAdSideRepository();
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index',
            [
                'cities' => [
                    'large' => $this->cityRepository->getLargeList(),
                    'small' => $this->cityRepository->getSmallList(),
                ],
                'cnt' => [
                    'tvs' => $this->tvAdRepository->getCountsInCities(),
                    'radio' => $this->radioAdRepository->getCountsInCities(),
                    'tickers' => $this->tickerRepository->getCountsInCities(),
                    'outdoor_ads' => $this->outdoorAdSideRepository->getCountsInCities(),
                ],
            ]
        );
    }

    /**
     * @param string $slug
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView(string $slug)
    {
        $model = $this->cityRepository->getBySlug($slug);
        if (empty($model)) {
            throw new NotFoundHttpException('Город не найден.');
        }
        return $this->render('view',
            [
                'model' => $model,
                'cnt' => [
                    'tv' => $this->tvAdRepository->getCountInCity($model->id),
                    'radio' => $this->radioAdRepository->getCountInCity($model->id),
                    'ticker' => $this->tickerRepository->getCountInCity($model->id),
                    'outdoor_ad' => $this->outdoorAdSideRepository->getCountInCity($model->id),
                    'outdoor_ad_led_displays' => $this->outdoorAdSideRepository->getCountLedDisplaysInCity($model->id),
                    'outdoor_ad_firewalls' => $this->outdoorAdSideRepository->getCountFirewallsInCity($model->id),
                ],
            ]
        );
    }
}
