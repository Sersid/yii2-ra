<?php

namespace frontend\controllers;

use frontend\repositories\CustomerRepository;
use yii\web\Controller;

/**
 * Class CustomersController
 * @package frontend\controllers
 */
class CustomersController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['customers' => (new CustomerRepository())->getAll()]);
    }
}
