<?php
declare(strict_types=1);

namespace frontend\controllers;

use common\models\City;
use common\models\Customer;
use common\models\LoginForm;
use common\models\UserRadioAd;
use common\models\UserTicker;
use common\models\UserTvAd;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use NikoM\Location\City\Application\Query\SearchCity\SearchCityQuery;
use NikoM\Location\City\Application\Query\SearchCity\SearchCityQueryHandler;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'common\components\MathCaptchaAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     * @return string
     * @throws InvalidArgumentException
     */
    public function actionIndex()
    {
        $customers = Customer::find()
            ->where(['customer.status' => true])
            ->joinWith('image')
            ->orderBy('sort');
        return $this->render(
            'index',
            [
                'cnt_customers' => $customers->count(),
                'customers' => $customers->limit(8)
                    ->all(),
            ]
        );
    }

    /**
     * Выбор города
     *
     * @param $slug
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionSetCity($slug)
    {
        if (empty($slug)) {
            Yii::$app->session->set('close_city_box', true);
            return 'OK';
        }
        $city = City::findOne(['slug' => $slug, 'status' => City::STATUS_ACTIVE]);
        if (empty($city)) {
            throw new NotFoundHttpException('Город не найден.');
        }
        Yii::$app->session->set('city_id', $city->id);

        // Update ads
        $tv = UserTvAd::getModel();
        if ($tv !== null) {
            $tv->city_id = $city->id;
            $tv->save();
        }
        $ticker = UserTicker::getModel()
            ->one();
        if ($ticker !== null) {
            $ticker->city_id = $city->id;
            $ticker->save();
        }
        $radio = UserRadioAd::getModel();
        if ($radio !== null) {
            $radio->city_id = $city->id;
            $radio->save();
        }

        if (Yii::$app->request->isAjax) {
            return 'OK';
        }
        $url = Yii::$app->request->get('back_url');
        $url = !empty($url) ? urldecode($url) : ['/cities/' . $city->slug];
        return $this->redirect($url);
    }

    public function actionCityList($name = null)
    {
        $query = new SearchCityQuery();
        $query->query = empty($name) ? null : $name;
        $handler = Yii::$container->get(SearchCityQueryHandler::class);

        return $this->renderAjax('city-list', ['regions' => $handler->handle($query)]);
    }

    /**
     * Logs in a user.
     * @return string|Response
     * @throws InvalidArgumentException
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', ['model' => $model]);
        }
    }

    /**
     * Logs out the current user.
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     * @return string|Response
     * @throws InvalidArgumentException
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()
                    ->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', ['model' => $model]);
    }

    /**
     * Requests password reset.
     * @return string|Response
     * @throws InvalidArgumentException
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', ['model' => $model]);
    }

    /**
     * Resets password.
     *
     * @param $token
     *
     * @return string|Response
     * @throws BadRequestHttpException
     * @throws InvalidArgumentException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', ['model' => $model]);
    }

    public function actionTest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
    }
}
