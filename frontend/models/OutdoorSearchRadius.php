<?php

namespace frontend\models;

/**
 * Class OutdoorSearchRadius
 * @package frontend\models
 */
class OutdoorSearchRadius extends OutdoorSearchMap
{
    /** @var string страница */
    const SCENARIO_HTML = 'html';
    /** @var string аякс запрос */
    const SCENARIO_API = 'api';
    /** @var string Местоположение */
    public $address;
    /** @var integer Радиус */
    public $radius;
    /** @var float Радиус */
    public $lat;
    /** @var float Радиус */
    public $lon;

    /**
     * Сценарии
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_HTML => ['city', 'address', 'radius'],
            self::SCENARIO_API => ['lat', 'lon', 'radius'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city'], 'integer'],
            [['address'], 'string'],
            [['city', 'address', 'radius', 'lat', 'lon'], 'required'],
            [['lat', 'lon', 'radius'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'city' => 'Город',
            'address' => 'Местоположение',
            'radius' => 'Радиус',
            'lat' => 'Широта',
            'lon' => 'Долгота',
        ];
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return [
            'url' => $this->getUrl(),
            'count' => ['map' => 0],
        ];
    }

    /**
     *
     */
    public function setFilter()
    {
        $this->query->addSelect(
            [
                'distance' => '(6371 * acos(cos(radians(' . $this->lat
                              . ')) * cos(radians(lat)) * cos( radians( lon ) - radians(' . $this->lon
                              . ')) + sin(radians(' . $this->lat . ')) * sin(radians(lat))))',
            ]
        );
        $this->query->having(['<', 'distance', $this->radius / 1000]);
    }

    /**
     * @return array
     */
    public function getCities()
    {
        $arResult = [];
        foreach ($this->fetchCities() as $item) {
            $arResult[$item['region'] ?: '-'][] = [
                'id' => (int)$item['id'],
                'name' => $item['name'],
            ];
        }
        return $arResult;
    }
}
