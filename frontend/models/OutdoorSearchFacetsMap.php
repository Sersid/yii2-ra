<?php

namespace frontend\models;

use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideQuery;
use yii\db\Query;

/**
 * Class OutdoorSearchFacetsMap
 * @package frontend\models
 */
class OutdoorSearchFacetsMap extends OutdoorSearchFacets
{
    /**
     * @return OutdoorAdSideQuery|Query
     */
    public function getQuery()
    {
        $this->query = OutdoorAdSide::find()
            ->where(['outdoor_ad_side.status' => OutdoorAdSide::STATUS_ENABLE])
            ->andWhere(['is not', 'lat', null])
            ->joinWith('parent', false);
        return $this->query;
    }
}
