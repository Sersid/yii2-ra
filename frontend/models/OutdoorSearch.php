<?php

namespace frontend\models;

use common\models\City;
use common\models\OutdoorAd;
use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideBusy;
use common\models\OutdoorAdSideQuery;
use frontend\repositories\CityRepository;
use Throwable;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class OutdoorSearch
 * @package frontend\models
 */
class OutdoorSearch extends Model
{
    /** @var int время жизни кеша */
    const CACHE_TIME = 86400;
    /** @var integer */
    public $city;
    /** @var integer */
    public $street;
    /** @var array */
    public $format = [];
    /** @var integer */
    public $price_from;
    /** @var integer */
    public $price_to;
    /** @var bool */
    public $discount = false;
    /** @var array */
    public $rating = [];
    /** @var array */
    public $sides = [];
    /** @var array */
    public $period = [];
    /** @var ActiveQuery */
    public $query;
    /** @var array */
    public $selectedFormat = [];
    /** @var City */
    public $selectedCity;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'street'], 'integer'],
            [['period'], 'each', 'rule' => ['string']],
            [['rating'], 'each', 'rule' => ['string']],
            ['format', 'each', 'rule' => ['integer']],
            [['price_from', 'price_to'], 'number'],
            ['sides', 'each', 'rule' => ['integer']],
            ['discount', 'boolean'],
        ];
    }

    /**
     * Загрузка параметров фильтра
     * @throws NotFoundHttpException
     */
    public function loadParams()
    {
        $request = Yii::$app->request;
        $citySlug = $request->get('city-slug');
        $this->city = $request->get('city');
        if (!empty($citySlug)) {
            $this->selectedCity = (new CityRepository())->getBySlug($citySlug);
            if (empty($this->selectedCity)) {
                throw new NotFoundHttpException('Город не найден.');
            }
            if (empty($this->city)) {
                $this->city = $this->selectedCity->id;
            }
            $slugFormat = $request->get('format-slug');
            $formats = [
                'reklamniy-shit-3x6' => OutdoorAd::FORMAT_BILLBOARDS,
                'svetodiodniy-ekran' => OutdoorAd::FORMAT_LED_DISPLAYS,
                'brandmauer' => OutdoorAd::FORMAT_FIREWALLS,
                'reklamnoe-ograjdenie' => OutdoorAd::FORMAT_ADV_FENCES,
            ];
            $format = isset($formats[$slugFormat]) ? $formats[$slugFormat] : null;
            if (!empty($format)) {
                $this->selectedFormat = [
                    'slug' => $slugFormat,
                    'id' => $format,
                ];
                $this->format[] = $format;
            }
        } else {
            $this->format = $request->get('format');
        }

        $this->street = $request->get('street');
        $this->period = $request->get('period');
        $this->price_from = $request->get('price_from');
        $this->price_to = $request->get('price_to');
        $this->discount = $request->get('discount');
        $this->rating = $request->get('rating');
        $this->sides = $request->get('sides');
    }

    /**
     * @return array
     */
    public function getUrl()
    {
        $attributes = [];
        $labels = $this->attributeLabels();
        foreach ($this->getAttributes() as $name => $value) {
            if (empty($value) || !isset($labels[$name])) {
                continue;
            }
            $attributes[$name] = $value;
        }
        return [
            'table' => Url::to(['index'] + $attributes),
            'map' => Url::to(['map'] + $attributes),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city' => 'Город',
            'street' => 'Улица',
            'format' => 'Формат наружной рекламы',
            'period' => 'Период аренды',
            'price_from' => 'Цена (от)',
            'price_to' => 'Цена (до)',
            'rating' => 'Рейтинг места',
            'sides' => 'Сторона',
            'discount' => 'Только товары с акцией',
        ];
    }

    /**
     * Выборка городов
     * @return array
     */
    public function fetchCities()
    {
        try {
            $arItems = OutdoorAdSide::getDb()
                ->cache(
                    function () {
                        return $this->getQuery()
                            ->select(['city.id', 'city.name', 'region' => 'region.name'])
                            ->joinWith(['parent.city', 'parent.city.region'], false)
                            ->groupBy('city.id')
                            ->orderBy(['region.sort' => SORT_ASC, 'city.sort' => SORT_ASC, 'city.name' => SORT_ASC])
                            ->asArray()
                            ->all();
                    },
                    self::CACHE_TIME
                );
        } catch (Throwable $e) {
            $arItems = [];
        }
        return $arItems;
    }

    /**
     * Города для выпадающего списка
     * @return array
     */
    public function  getCities()
    {
        $arResult = ['' => 'Все города'];
        foreach ($this->fetchCities() as $item) {
            $arResult[$item['region'] ?: '-'][$item['id']] = $item['name'];
        }
        return $arResult;
    }

    /**
     * @return OutdoorAdSideQuery|Query
     */
    public function getQuery()
    {
        $this->query = OutdoorAdSide::find();
        $this->query->where(['outdoor_ad_side.status' => OutdoorAdSide::STATUS_ENABLE]);
        $this->query->joinWith('parent', false);
        return $this->query;
    }

    /**
     * Улицы для выпадающего списка
     * @return array
     * @throws Throwable
     */
    public function getStreets()
    {
        $arResult = [];
        if (!empty($this->city)) {
            $arResult[''] = 'Выберите улицу';
            $arItems = OutdoorAdSide::getDb()
                ->cache(
                    function () {
                        return $this->getQuery()
                            ->select(['street_id', 'street.name', 'parent_id'])
                            ->joinWith(['parent.street'])
                            ->where(['outdoor_ad.city_id' => $this->city])
                            ->groupBy('street_id')
                            ->orderBy(['street.sort' => SORT_ASC, 'street.name' => SORT_ASC])
                            ->asArray()
                            ->all();
                    },
                    self::CACHE_TIME
                );
            foreach ($arItems as $item) {
                $arResult[$item['street_id']] = $item['name'];
            }
        } else {
            $arResult[''] = 'Сначала выберите город';
        }
        return $arResult;
    }

    /**
     * Форматы
     * @return array
     */
    public function getFormats()
    {
        $arResult = [];
        static $arFormats;
        if (is_null($arFormats)) {
            $arFormats = OutdoorAd::getFormats();
        }
        try {
            $arItems = OutdoorAdSide::getDb()
                ->cache(
                    function () {
                        return $this->getQuery()
                            ->select(['format'])
                            ->groupBy('format')
                            ->orderBy('format')
                            ->asArray()
                            ->all();
                    },
                    self::CACHE_TIME
                );
        } catch (Throwable $e) {
            $arItems = [];
        }
        foreach ($arItems as $item) {
            $arResult[$item['format']] = $arFormats[$item['format']] ?? '';
        }
        return $arResult;
    }

    /**
     * Рейтинги
     * @return array
     */
    public function getRatings()
    {
        $arResult = [];
        foreach (OutdoorAdSide::getRatings() as $id => $label) {
            $arResult[$id] = $label;
        }
        return $arResult;
    }

    /**
     * Стороны
     * @return array
     */
    public function getSides()
    {
        $arResult = [];
        foreach (OutdoorAdSide::getSides() as $id => $label) {
            $arResult[$id] = $label;
        }
        return $arResult;
    }

    /**
     * Периоды
     * @return array
     */
    public function getPeriods()
    {
        $arResult = [];
        foreach (OutdoorAdSide::getDatesDropDownItems() as $id => $label) {
            $arResult[$id] = $label;
        }
        return $arResult;
    }

    /**
     * Кол-во найденых предложений
     * @return array
     */
    public function getCount()
    {
        try {
            $arResult = [
                'table' => OutdoorAdSide::getDb()
                    ->cache(
                        function () {
                            $query = $this->getQuery();
                            $this->setFilter();
                            return $query->select(['cnt' => new Expression('COUNT(1)')])
                                ->asArray()
                                ->one();
                        },
                        self::CACHE_TIME
                    )['cnt'],
                'map' => OutdoorAdSide::getDb()
                    ->cache(
                        function () {
                            $query = $this->getQuery();
                            $query->andWhere(['is not', 'lat', null]);
                            $this->setFilter();
                            return $query->select(['cnt' => new Expression('COUNT(1)')])
                                ->asArray()
                                ->one();
                        },
                        self::CACHE_TIME
                    )['cnt'],
            ];
        } catch (Throwable $e) {
            $arResult = [
                'table' => 0,
                'map' => 0,
            ];
        }
        return $arResult;
    }

    /**
     * Применение фильтра
     */
    public function setFilter()
    {
        $this->query->andFilterWhere(
            [
                'outdoor_ad.city_id' => $this->city,
                'outdoor_ad.street_id' => $this->street,
                'outdoor_ad.format' => $this->format,
                'outdoor_ad_side.rating' => $this->rating,
                'outdoor_ad_side.side' => $this->sides,
            ]
        );
        $this->query->andFilterWhere(['>=', 'price', $this->price_from]);
        $this->query->andFilterWhere(['<=', 'price', $this->price_to]);
        if (!empty($this->discount)) {
            if (OutdoorAdSide::isDiscountMonthlyValid()) {
                $this->query->andFilterWhere(
                    [
                        'OR',
                        [
                            'AND',
                            ['>', 'discount_monthly', 0],
                            ['!=', 'busy_status', OutdoorAdSide::BUSY_STATUS_BUSY],
                            ['>=', 'outdoor_ad_side.updated_at', mktime(0, 0, 0, date('n'), 1, date('Y'))],
                            ['<', 'outdoor_ad_side.updated_at', mktime(0, 0, 0, date('n') + 1, 1, date('Y'))],
                        ],
                        ['>', 'outdoor_ad_side.discount', 0],
                    ]
                );
            } else {
                $this->query->andFilterWhere(['>', 'outdoor_ad_side.discount', 0]);
            }
        }
        if (!empty($this->period)) {
            $arPeriods = ['OR'];
            foreach ($this->period as $period) {
                $period = explode('.', $period);
                $arPeriods[] = [
                    'AND',
                    ['=', 'outdoor_ad_side_busy.month', $period[0]],
                    ['=', 'outdoor_ad_side_busy.year', $period[1]],
                ];
            }
            if (count($arPeriods) > 1) {
                $this->query->joinWith('busy', false);
                $this->query->andFilterWhere(
                    [
                        'AND',
                        ['=', 'outdoor_ad_side_busy.status', OutdoorAdSideBusy::STATUS_FREE],
                        $arPeriods,
                    ]
                );
            }
        }
    }
}
