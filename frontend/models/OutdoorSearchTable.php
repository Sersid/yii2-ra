<?php

namespace frontend\models;

use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideQuery;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * Class OutdoorSearch
 * @package frontend\models
 */
class OutdoorSearchTable extends OutdoorSearch
{
    /**
     * @return ActiveDataProvider
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     */
    public function getActiveDataProvider()
    {
        return new ActiveDataProvider(
            [
                'query' => $this->getSearchQuery(),
                'pagination' => ['pageSize' => 20],
                'sort' => [
                    'defaultOrder' => ['status' => SORT_ASC],
                    'attributes' => [
                        'city' => [
                            'asc' => [
                                'city.sort' => SORT_ASC,
                                'city.name' => SORT_ASC,
                            ],
                            'desc' => [
                                'city.sort' => SORT_DESC,
                                'city.name' => SORT_DESC,
                            ],
                            'label' => 'Город',
                        ],
                        'address' => [
                            'asc' => [
                                'street.name' => SORT_ASC,
                                'address' => SORT_ASC,
                            ],
                            'desc' => [
                                'street.name' => SORT_DESC,
                                'address' => SORT_DESC,
                            ],
                            'label' => 'Адрес',
                        ],
                        'format' => [
                            'asc' => ['outdoor_ad.format' => SORT_ASC],
                            'desc' => ['outdoor_ad.format' => SORT_DESC],
                            'label' => 'Поверхность',
                        ],
                        'price' => [
                            'asc' => ['price' => SORT_ASC],
                            'desc' => ['price' => SORT_DESC],
                            'label' => 'Цена (руб/мес)',
                        ],
                        'side' => [
                            'asc' => ['side' => SORT_ASC],
                            'desc' => ['side' => SORT_DESC],
                            'label' => 'Сторона',
                        ],
                        'rating' => [
                            'asc' => ['rating' => SORT_ASC],
                            'desc' => ['rating' => SORT_DESC],
                            'label' => 'Рейтинг',
                        ],
                        'status' => [
                            'asc' => [
                                'busy_status' => SORT_ASC,
                                'city.sort' => SORT_ASC,
                                'city.name' => SORT_ASC,
                            ],
                            'desc' => [
                                'busy_status' => SORT_DESC,
                                'city.sort' => SORT_DESC,
                                'city.name' => SORT_DESC,
                            ],
                            'label' => 'Статус',
                            'title' => '<div class="hint-body notEvenPadding"><p class="notEvenP">'
                                       . 'Отображается свободность стороны на следующий календарный месяц размещения'
                                       . '</p></div>',
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * @return OutdoorAdSideQuery|Query
     * @throws NotFoundHttpException
     * @throws InvalidArgumentException
     */
    public function getSearchQuery()
    {
        $this->query = OutdoorAdSide::find()
            ->select(
                [
                    'outdoor_ad_side.id',
                    'parent_id',
                    'side',
                    'price',
                    'rating',
                    'busy_status',
                    'outdoor_ad_side.discount',
                    'discount_monthly',
                    'outdoor_ad_side.updated_at',
                    'address',
                ]
            )
            ->joinWith(
                [
                    'parent' => function (ActiveQuery $query) {
                        $query->select(
                            [
                                'id',
                                'city_id',
                                'street_id',
                                'number',
                                'format',
                                'size',
                                'sides',
                                'full_number',
                                'type_display',
                            ]
                        );
                    },
                    'parent.city' => function (ActiveQuery $query) {
                        $query->select(['id', 'name']);
                    },
                    'parent.street' => function (ActiveQuery $query) {
                        $query->select(['id', 'name']);
                    },
                ]
            );
        $this->query->with(['busy']);
        $this->query->where(['outdoor_ad_side.status' => OutdoorAdSide::STATUS_ENABLE]);
        $this->query->groupBy('id');
        $this->loadParams();
        if ($this->validate()) {
            $this->setFilter();
        }
        return $this->query;
    }
}
