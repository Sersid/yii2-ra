<?php

namespace frontend\models;

use yii\helpers\Url;

class AdvCase extends \common\models\AdvCase
{
    /**
     * @return string
     */
    public function getDetailPageUrl(): string
    {
        return Url::to(['cases/view', 'slug' => $this->slug]);
    }

    /**
     * @return bool
     */
    public function isHtml(): bool
    {
        return $this->type === self::TYPE_HTML;
    }
}
