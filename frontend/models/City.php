<?php

namespace frontend\models;

use yii\helpers\Url;

/**
 * Class City
 * @package frontend\models
 */
class City extends \common\models\City
{
    /**
     * @return string
     */
    public function getDetailPageUrl(): string
    {
        return Url::to(['cities/view', 'slug' => $this->slug]);
    }

    /**
     * @return string
     */
    public function getOutdoorAdPageUrl(): string
    {
        return '/outdoor-ad/' . $this->slug;
    }

    /**
     * @return string
     */
    public function getOutdoorAdFirewallsPageUrl(): string
    {
        return '/outdoor-ad/brandmauer-' . $this->slug;
    }

    /**
     * @return string
     */
    public function getOutdoorAdLedDisplaysPageUrl(): string
    {
        return '/outdoor-ad/svetodiodniy-ekran-' . $this->slug;
    }

    /**
     * @return string
     */
    public function getTvPageUrl(): string
    {
        return '/tv/' . $this->slug;
    }

    /**
     * @return string
     */
    public function getRadioPageUrl(): string
    {
        return '/radio/' . $this->slug;
    }

    /**
     * @return string
     */
    public function getTickerPageUrl(): string
    {
        return '/ticker/' . $this->slug;
    }

    /**
     * @return string
     */
    public function getInternetAdPageUrl(): string
    {
        return '/internet-ad/city-' . $this->slug;
    }

    public function getSeoPageUrl(): string
    {
        return '/internet-ad/prodvizhenie-saitov/' . $this->slug;
    }

    public function getContextAdPageUrl(): string
    {
        return '/internet-ad/kontekstnaja/' . $this->slug;
    }

    /**
     * @return string
     */
    public function getSalaryFormatted()
    {
        return number_format($this->salary, 0, '.', ' ');
    }
    /**
     * @return string
     */
    public function getPopulationFormatted()
    {
        return number_format($this->population, 0, '.', ' ');
    }

    /**
     * @return string
     */
    public function getDensityFormatted()
    {
        return number_format($this->density, 0, ',', ' ');
    }
}
