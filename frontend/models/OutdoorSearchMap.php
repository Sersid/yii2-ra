<?php

namespace frontend\models;

use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideQuery;
use yii\base\InvalidArgumentException;
use yii\db\Query;

/**
 * Class OutdoorSearch
 * @package frontend\models
 */
class OutdoorSearchMap extends OutdoorSearch
{
    /**
     * @return OutdoorAdSideQuery|Query
     * @throws InvalidArgumentException
     */
    public function getSearchQuery()
    {
        $this->query = OutdoorAdSide::find()
            ->select(['outdoor_ad.id', 'lat', 'lon', 'rotate', 'sides'])
            ->where(['is not', 'lat', null])
            ->andWhere(['outdoor_ad_side.status' => OutdoorAdSide::STATUS_ENABLE])
            ->joinWith('parent', false)
            ->groupBy('outdoor_ad.id');
        if ($this->validate()) {
            $this->setFilter();
        }
        return $this->query;
    }
}
