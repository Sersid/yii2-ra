<?php

namespace frontend\models;

use common\models\OutdoorAdSide;
use frontend\widgets\Html;
use Throwable;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * Class OutdoorSearchFacets
 * @package frontend\models
 */
class OutdoorSearchFacets extends OutdoorSearch
{
    /**
     * @return array
     * @throws Throwable
     * @throws NotFoundHttpException
     */
    public static function getItems()
    {
        $searchModel = new self();
        $searchModel->loadParams();
        $result = [];
        if ($searchModel->validate()) {
            $result = [
                'streets' => $searchModel->getStreets(),
                'formats' => $searchModel->getFormats(),
                'ratings' => $searchModel->getRatings(),
                'sides' => $searchModel->getSides(),
                'periods' => $searchModel->getPeriods(),
                'min_price' => $searchModel->getMinPrice(),
                'max_price' => $searchModel->getMaxPrice(),
                'count' => $searchModel->getCount(),
                'url' => $searchModel->getUrl(),
            ];
        }
        return $result;
    }

    /**
     * Активные, в зависимости от значений фильтра, города для выпадающего списка
     * @return array
     * @throws Throwable
     */
    public function getCities()
    {
        $arResult = [];
        $arItems = OutdoorAdSide::getDb()
            ->cache(
                function () {
                    $query = $this->getQuery();
                    $this->setFilter('city');
                    return $query->select(['city_id'])
                        ->groupBy('city_id')
                        ->asArray()
                        ->all();
                },
                self::CACHE_TIME
            );

        foreach ($arItems as $item) {
            if (empty($item['city_id'])) {
                continue;
            }
            $arResult[$item['city_id']] = null;
        }
        return array_keys($arResult);
    }

    /**
     * Применение фильтра
     *
     * @param null $without
     */
    public function setFilter($without = null)
    {
        $value = null;
        if (!empty($without) && property_exists($this, $without)) {
            $value = $this->$without;
            $this->$without = null;
        }
        parent::setFilter();
        if (!empty($without) && property_exists($this, $without)) {
            $this->$without = $value;
        }
    }

    /**
     * Активные, в зависимости от значений фильтра, улицы для выпадающего списка
     * @return array
     * @throws Throwable
     */
    public function getStreets()
    {
        $arResult = [];
        if (!empty($this->city)) {
            $arItems = OutdoorAdSide::getDb()
                ->cache(
                    function () {
                        $query = $this->getQuery();
                        $this->setFilter('street');
                        return $query->select(['street_id'])
                            ->groupBy('street_id')
                            ->asArray()
                            ->all();
                    },
                    self::CACHE_TIME
                );
            foreach ($arItems as $item) {
                if (empty($item['street_id'])) {
                    continue;
                }
                $arResult[$item['street_id']] = null;
            }
        }
        return array_keys($arResult);
    }

    /**
     * Активные форматы
     * @return array
     * @throws Throwable
     */
    public function getFormats()
    {
        $arResult = [];
        $arItems = OutdoorAdSide::getDb()
            ->cache(
                function () {
                    $query = $this->getQuery();
                    $this->setFilter('format');
                    return $query->select(['format'])
                        ->asArray()
                        ->all();
                },
                self::CACHE_TIME
            );
        foreach ($arItems as $item) {
            if (is_null($item['format'])) {
                continue;
            }
            $arResult[$item['format']] = null;
        }
        return array_keys($arResult);
    }

    /**
     * Активные рейтинги
     * @return array
     * @throws Throwable
     */
    public function getRatings()
    {
        $arResult = [];
        $arItems = OutdoorAdSide::getDb()
            ->cache(
                function () {
                    $query = $this->getQuery();
                    $this->setFilter('rating');
                    return $query->select(['rating'])
                        ->groupBy('rating')
                        ->asArray()
                        ->all();
                },
                self::CACHE_TIME
            );
        foreach ($arItems as $item) {
            if (is_null($item['rating'])) {
                continue;
            }
            $arResult[] = (int)$item['rating'];
        }
        return $arResult;
    }

    /**
     * Активные стороны
     * @return array
     * @throws Throwable
     */
    public function getSides()
    {
        $arResult = [];
        $arItems = OutdoorAdSide::getDb()
            ->cache(
                function () {
                    $query = $this->getQuery();
                    $this->setFilter('side');
                    return $query->select(['side'])
                        ->groupBy('side')
                        ->asArray()
                        ->all();
                },
                self::CACHE_TIME
            );
        foreach ($arItems as $item) {
            if (is_null($item['side'])) {
                continue;
            }
            $arResult[$item['side']] = null;
        }
        return array_keys($arResult);
    }

    /**
     * Активные периоды
     * @return array
     * @throws Throwable
     */
    public function getPeriods()
    {
        $arResult = [];
        $arItems = OutdoorAdSide::getDb()
            ->cache(
                function () {
                    $query = $this->getQuery();
                    $this->setFilter('period');
                    return $query->select(['month', 'year'])
                        ->innerJoinWith('busy', false)
                        ->groupBy(['year', 'month'])
                        ->asArray()
                        ->all();
                },
                self::CACHE_TIME
            );
        foreach ($arItems as $item) {
            $arResult[] = floatval($item['month'] . '.' . $item['year']);
        }
        return $arResult;
    }

    /**
     * Минимальная цена найденых предложений
     * @return string
     * @throws Throwable
     */
    public function getMinPrice()
    {
        $min = OutdoorAdSide::getDb()
            ->cache(
                function () {
                    $query = $this->getQuery();
                    $this->setFilter('price_from');
                    return $query->select(['min' => new Expression('MIN(price)')])
                        ->asArray()
                        ->one();
                },
                self::CACHE_TIME
            )['min'];
        return 'от ' . Html::priceFormatted($min);
    }

    /**
     * Максимальная цена найденых предложений
     * @return string
     * @throws Throwable
     */
    public function getMaxPrice()
    {
        $max = OutdoorAdSide::getDb()
            ->cache(
                function () {
                    $query = $this->getQuery();
                    $this->setFilter('price_to');
                    return $query->select(['max' => new Expression('MAX(price)')])
                        ->asArray()
                        ->one();
                },
                self::CACHE_TIME
            )['max'];
        return 'до ' . Html::priceFormatted($max);
    }
}
