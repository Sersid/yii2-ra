<?php

use common\models\UserTicker;
use frontend\widgets\Html;

/* @var yii\web\View $this */
/* @var common\models\Review $review */
/* @var yii\bootstrap\ActiveField $form */
/* @var UserTicker $model */
/** @var float $total */
?>
<div class="basket-block stroka-basket-block">
    <div class="basket-block-header">
        Вы выбрали:
    </div>
    <?php if(!empty($items)):?>
        <ul class="basket-block-body" style="">
            <?php foreach ($items as $item):?>
                <li>
                    <div class="basket-block-body-title"><?=$item['name']?></div>
                    <div class="basket-block-body-count">
                        <?php if(!empty($item['error'])):?>
                            <div class="basket-block-body-time" style="color:red;">
                                <?=$item['error']?>
                            </div>
                        <?php else:?>
                            <div class="basket-block-body-time">
                                <?=Yii::t('app', '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}', ['days' => $item['cnt_days']])?>, <?=Yii::t('app', '{words, plural, one{# слово} few{# слова} many{# слов} other{# слов}}', ['words' => $item['words']])?> <span>(<?=number_format($item['price'], 0, ' ', ' ')?> руб./слово)</span>
                            </div>
                            <div class="basket-block-body-price"><?=number_format($item['total'], 0, ' ', ' ')?> руб.</div>
                        <?php endif?>
                    </div>
                    <div class="basket-block-body-dell-item">
                        <?=Html::a('',['delete', 'ticker_id' => $item['ticker_id']], ['title' => 'Удалить позицию'])?>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
        <div class="basket-block-footer">
            <div class="basket-block-footer-price">
                <b>Итого:</b>
                <div><?=number_format($total, 0, ' ', ' ')?> <span>руб.</span></div>
            </div>
            <?=Html::a('Оформить', '#', ['class' => 'yellow-btn', 'data-toggle'=>"modal", 'data-target'=>"#order"])?>
        </div>
    <?php else:?>
        <div class="basket-block-text">
            <b>Выберите телеканалы</b>
        </div>
    <?php endif?>
</div>
<div class="callus-right">
    <div class="callus-box">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <img src="/img/icon-tel-yslygi.png" alt="">
            <div class="callus-box-info">
                <?php if(empty($city)):?>
                    <p>Нужен совет?</p>
                    <?php
                    $phone = str_replace([')', '('], '', Yii::$app->city->phone);
                    ?>
                    <b><span class="hidden-lg hidden-md hidden-sm hidden-xs"><?=substr($phone, 0, 2)?></span><?=substr($phone, 2)?></b>
                <?php else:?>
                    <p>Телефон <?=$city->where?></p>
                    <?php
                    $phone = str_replace([')', '('], '', $city->getPhone());
                    ?>
                    <b><span class="hidden-lg hidden-md hidden-sm hidden-xs"><?=substr($phone, 0, 2)?></span><?=substr($phone, 2)?></b>
                <?php endif?>
            </div>
            <p>Позвоните, и наш менеджер ответит на все ваши вопросы</p>
        </div>
    </div>
</div>
