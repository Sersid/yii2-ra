<?php

use common\models\City;
use common\models\UserTicker;
use common\models\UserTickerPhone;
use frontend\widgets\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

/* @var yii\web\View $this */
/* @var common\models\Review $review */
/* @var yii\bootstrap\ActiveField $form */
/* @var UserTicker $model */
/* @var integer $channelsCnt */
/* @var City $city */
?>

<?php $form = ActiveForm::begin(
    [
        'id' => 'settings-form',
        'errorCssClass' => false,
        'enableClientValidation' => false,
        'enableClientScript' => false,
        'fieldConfig' => [
            'template' => "{input}\n<div class='col-xs-12'>{error}</div>",
            'inputOptions' => ['class' => ''],
        ],
    ]
);
?>
<div class="kanal-block">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>1. Выберите тип рекламы</h2>
    </div>
    <?= $form->field($model, 'type')
        ->radioList(
            $model->getTypes(),
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $descr = [
                        1 => '<div class="kanal__tooltip-box">
                        <div class="kanal__tooltip kanal__tooltip_left">
                            <a class="kanal__link" href="javascript:void(0)">Подробное описание</a>
                            <div class="js-detailBox">
                                <p>Содержит информацию о товарах и услугах, акциях, распродажах, ярмарках, открытиях торговых точек и т.п.</p>
                            </div>
                        </div>
                    </div>',
                        // 1 => '<p>Содержит информацию о товарах и услугах, акциях, распродажах, ярмарках, открытиях торговых точек и т.п.</p>',
                        2 => '<div class="kanal__tooltip-box">
                        <div class="kanal__tooltip kanal__tooltip_left">
                            <a class="kanal__link" href="javascript:void(0)">Подробное описание</a>
                            <div class="js-detailBox">
                                <p>Объявление содержит информацию об открытых вакансиях или любую социальную информацию от частных лиц (поиск утерянных вещей, поздравления, уведомления и т.п.)</p>
                            </div>
                        </div>
                    </div>'
                        // 2 => '<p>Объявление содержит информацию об открытых вакансиях или любую социальную информацию от частных лиц (поиск утерянных вещей, поздравления, уведомления и т.п.)</p>',
                    ];
                    $return = Html::beginTag('div', ['class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-12 vubor-formata']);
                    $return .= Html::beginTag('div', ['class' => 'radio-item']);
                    $return .= Html::radio($name, $checked, ['value' => $value, 'id' => 'type-' . $value]);
                    $return .= Html::label(
                        Html::tag('span', $label) . Html::tag('div', $descr[$value]),
                        'type-' . $value
                    );
                    $return .= Html::endTag('div');
                    $return .= Html::endTag('div');
                    return $return;
                },
                'class' => 'vubor-formata-box',
            ]
        ) ?>
</div>
<div class="kanal-block">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>2. Выберите телеканал в городе</h2>
        <p>Каждый телеканал авторизовал нас на размещение рекламных роликов.</p>
        <div class="beg-block">
            <b>Типы размещений:</b>
            <script>
                $(".beg-link-1").click(function() {
                    $(".beg-descr-2").slideUp();
                    if ($(".beg-descr-1").is(':visible')) {
                        $(".beg-descr-1").slideUp();
                    } else {
                        $(".beg-descr-1").slideDown();
                    }
                });
                $(".beg-link-2").click(function() {
                    $(".beg-descr-1").slideUp();
                    if ($(".beg-descr-2").is(':visible')) {
                        $(".beg-descr-2").slideUp();
                    } else {
                        $(".beg-descr-2").slideDown();
                    }
                });
            </script>
            <div class="beg-link-cont">
                <div class="beg-link-1">*<span>Бегущая строка</span></div>
                <div class="beg-link-2">*<span>Текстовый блок</span></div>
            </div>
            <div class="beg-descr-1">"Бегущая строка" - это "бегущий" внизу экрана текст размещаемый во время
                телепередач.
            </div>
            <div class="beg-descr-2">"Текстовый блок" - это текст размещаемый в виде статичной заставки с объявлениями
                (обычно от 3 до 6)
            </div>
        </div>
        <?php if (empty(Yii::$app->request->get('city'))): ?>
            <?= $form->field($model, 'city_id', ['options' => ['class' => 'cust-select']])
                ->dropDownList(
                    $items['cities']
                ) ?>
        <?php else: ?>
            <?= $form->field($model, 'city_id')
                ->hiddenInput() ?>
        <?php endif; ?>
    </div>
    <?php
    if (!empty($model->hidden_channels)) {
        foreach ($model->hidden_channels as $value) {
            echo Html::hiddenInput(Html::getInputName($model, 'hidden_channels') . '[]', $value);
        }
    }
    ?>
    <?php if (!empty($items['channels']['items'])): ?>
        <?= $form->field($model, 'channels', ['options' => ['class' => 'kanal-all-item']])
            ->checkboxList(
                $items['channels']['items'],
                [
                    'item' => function ($index, $label, $name, $checked, $value) use ($items) {
                        $price_type = '';
                        switch ($items['channels']['price_types'][$value]) {
                            case 'ticker':
                                $price_type = '<p class="show-hint-label">бегущая строка</p>';
                                $price_type .= '<div class="hint-label"><p>"Бегущая строка" - это "бегущий" внизу экрана текст размещаемый во время телепередач.</p></div>';
                                break;
                            case 'text_block':
                                $price_type = '<p class="show-hint-label">текстовый блок</p>';
                                $price_type .= '<div class="hint-label"><p>"Текстовый блок" - это текст размещаемый в виде статичной заставки с объявлениями (обычно от 3 до 6)</p></div>';
                                break;
                        }
                        $return = Html::beginTag('div', ['class' => 'col-lg-3 col-md-3 col-sm-3 col-xs-6']);
                        $return .= Html::beginTag('div', ['class' => 'kanal-item checkbox-item']);
                        $return .= Html::beginTag('div', ['class' => 'checkbox-item-box text-center']);
                        $return .= Html::beginTag('div');
                        $return .= Html::checkbox($name, $checked, ['value' => $value, 'id' => 'channel-' . $value]);
                        $return .= Html::label(
                            Html::tag(
                                'div',
                                Html::img($items['channels']['logos'][$value]),
                                ['class' => 'checkbox-item-img']
                            ) . Html::tag('span', $label) . $price_type . '<p></p>',
                            'channel-' . $value
                        );
                        $return .= Html::endTag('div');
                        $return .= Html::endTag('div');
                        $return .= Html::endTag('div');
                        $return .= Html::endTag('div');
                        return $return;
                    },
                ]
            ) ?>
    <?php else: ?>
        <?= $this->render('/forms/sorry-new'); ?>
    <?php endif; ?>
    <?php if (!$model->show_all_cities && $channelsCnt > 8): ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center all-kanal-link">
            <?= Html::activeCheckbox($model, 'show_all_cities') ?>
        </div>
    <?php endif ?>
</div>
<div class="kanal-block">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>3. Текст объявления</h2>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-advert form-block">
            <?= $form->field($model, 'message', ['template' => "{label}\n{input}\n{error}"])
                ->textarea(
                    [
                        'placeholder' => 'Например: Торговая компания возьмет в аренду офисное помещение на длительный срок.',
                    ]
                ) ?>
            <div class="form-group">
                <label for="tel-o">Укажите номер телефона для объявления</label>
                <?php foreach ($model->phones as $i => $phone): ?>
                    <div class="form-advert-box">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-7 no-padding">
                            <div class="form-advert-input">
                                <div class="tel-radio-box">
                                    <?= $form->field($phone, 'type', ['template' => "{input}"])
                                        ->radioList(
                                            UserTickerPhone::getTypes(),
                                            [
                                                'item' => function ($index, $label, $name, $checked, $value) use ($phone
                                                ) {
                                                    $return = Html::radio(
                                                        'phones[' . $phone->id . '][type]',
                                                        $checked,
                                                        [
                                                            'value' => $value,
                                                            'id' => $name . '' . $index . '' . $phone->id,
                                                        ]
                                                    );
                                                    $return .= Html::label(
                                                        Html::tag('span', $label),
                                                        $name . '' . $index . '' . $phone->id
                                                    );
                                                    return $return;
                                                },
                                            ]
                                        ) ?>
                                </div>
                                <?= $form->field($phone, 'phone', ['template' => "{input}"])
                                     ->textInput([
                                         'placeholder' => UserTickerPhone::getPlaceholder($phone->type, $city),
                                         'class' => 'phone-mask',
                                         'id' => 'usertickerphone-phone-' . $phone->id,
                                         'name' => 'phones[' . $phone->id . '][phone]',
                                     ])
                                    /*->widget(
                                        MaskedInput::class,
                                        [
                                            'mask' => UserTickerPhone::getMask($phone->type, $city),
                                            'options' => [
                                                'placeholder' => UserTickerPhone::getPlaceholder($phone->type, $city),
                                                'class' => 'phone-mask',
                                                'id' => 'usertickerphone-phone-' . $phone->id,
                                                'name' => 'phones[' . $phone->id . '][phone]',
                                            ],
                                        ]
                                    )*/ ?>
                            </div>
                        </div>
                        <?php if (count($model->phones) == $i + 1): ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding add-more-number-box">
                                <div class="add-more-number">
                                    <a>Добавить еще один номер</a>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                <?php endforeach; ?>
                <div class="form-advert-box new-phone <?= empty($model->phones) ? 'active' : 'noactive' ?>">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-7 no-padding">
                        <div class="form-advert-input">
                            <div class="tel-radio-box">
                                <?= $form->field($model, 'new_phone_type', ['template' => "{input}"])
                                    ->radioList(
                                        UserTickerPhone::getTypes(),
                                        [
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                $return = Html::radio(
                                                    $name,
                                                    $checked,
                                                    [
                                                        'value' => $value,
                                                        'id' => $name . '' . $index,
                                                    ]
                                                );
                                                $return .= Html::label(Html::tag('span', $label), $name . '' . $index);
                                                return $return;
                                            },
                                        ]
                                    ) ?>
                            </div>
                            <?= $form->field($model, 'new_phone', ['template' => "{input}"])
                                ->textInput([
                                    'placeholder' => UserTickerPhone::getPlaceholder($model->new_phone_type, $city),
                                    'class' => 'phone-mask',
                                ])
                                /*->widget(
                                    MaskedInput::class,
                                    [
                                        'mask' => UserTickerPhone::getMask($model->new_phone_type, $city),
                                        'options' => [
                                            'placeholder' => UserTickerPhone::getPlaceholder($model->new_phone_type, $city),
                                            'class' => 'phone-mask',
                                        ],
                                    ]
                                )*/ ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <p class="form-advert-bottom-text">Количество слов влияет на стоимость объявления. Равно как количество
                    дней и выбранные телеканалы.</p>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
