<?php
use frontend\widgets\Html;

/* @var yii\web\View $this */
/** @var float $total */
?>
<?php if(!empty($items)):?>
    <div class="kanal-block">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>4. Калькулятор стоимости</h2>
            <p class="hidden-sm hidden-xs">Это окончательный расчет по прямому прайсу телеканалов, с учетом акций.</p>
            <p>Вы можете пропустить этот шаг, и не заполнять его. Мы свяжемся с вами и обсудим все детали по телефону.</p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 kalk-table-list">
            <div class="kalk-block kanal-period-table begyw-block">
                <table>
                    <thead>
                    <tr class="kalk-item">
                        <th class="header-kalk">
                            <div class="hint-box" title="Выбран Вами галочкой выше">Телеканал</div>
                        </th>
                        <th>
                            <div class="hint-box" title="Выберите в календаре даты, по которым хотите показывать Вашу рекламу/объявления">Период показа</div>
                        </th>
                        <th>
                            <div class="hint-box" title="Указано минимальное количество показов, которое мы гарантируем">Показов в сутки</div>
                        </th>
                        <th>
                            <div class="hint-box" title="Здесь отражается количество слов в Вашем объявлении и цена за одно слово">Цена</div>
                        </th>
                        <th class="footer-kalk">
                            <div class="hint-box" title="Конечная стоимость Вашей рекламы/объявления">Итого</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($items as $item):?>
                        <tr class="kalk-item">
                            <td class="header-kalk" data-label="Телеканал">
                                <input type="hidden" class="ticker-id" value="<?=$item['ticker_id']?>">
                                <div class="td"><?=$item['name']?></div>
                            </td>
                            <?php if(!empty($item['error'])):?>
                                <td colspan="5" class="error-td"><?=$item['error']?></td>
                            <?php else:?>
                                <td data-label="Период показа">
                                    <div class="td">
                                        <div>
                                            <input class="datepick" type="text" data-start-date="<?=date('d.m.Y',strtotime($item['date_from']))?>" data-end-date="<?=date('d.m.Y',strtotime($item['date_to']))?>">
                                            <input type="hidden" class="datepick-hidden-form" name="date_from" value="<?=$item['date_from']?>">
                                            <input type="hidden" class="datepick-hidden-to" name="date_to" value="<?=$item['date_to']?>">
                                        </div>
                                        <span class="dayCountdatepick">
                                            (<?=Yii::t('app', '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}', ['days' => $item['cnt_days']])?><?=$item['cnt_weekends'] > 0 ? ' без выходных' : ''?>)
                                        </span>
                                    </div>
                                </td>
                                <td data-label="Показов в сутки">
                                    <div class="td">
                                        <?= !is_array($item['view_types']) ? $item['view_types'] :  Html::dropDownList('views', $item['view_type'], $item['view_types'], [
                                            'class' => 'custom-select sources',
                                            'placeholder' => $item['view_types'][$item['view_type']],
                                        ])?>
                                    </div>
                                </td>
                                <td data-label="Цена">
                                    <div class="td">
                                        <?=Yii::t('app', '{words, plural, one{# слово} few{# слова} many{# слов} other{# слов}}', ['words' => $item['words']])?> <span><?=number_format($item['price'], 0, ' ', ' ')?> руб./слово</span>
                                    </div>
                                </td>
                                <td class="footer-kalk" data-label="Итого" style="position: relative;">
                                    <div class="td"><?=number_format($item['total'], 0, ' ', ' ')?> руб.</div>
                                    <?/*<div class="basket-block-body-dell-item">
                                        <a href="/ticker/delete?ticker_id=<?=$item['ticker_id']?>" title="Удалить позицию"></a>
                                    </div>*/?>
                                </td>
                            <?php endif?>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 plan-prokata">
            <div class="basket-link-box">
                <b>Расчет без наценок</b>
                <em>Мы не делаем наценок, телекомпании платят нам за прием объявлений</em>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bay-now">
            <a data-toggle="modal" data-target="#order" href="#" class="<?=(($total) ? 'yellow-btn' : 'yellow-btn btn disabled')?>">Оформить заявку</a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a data-toggle="modal" data-target="#feedback-form" href="#" class="yellow-btn">Задать вопрос</a>
        </div>
    </div>
<?php endif?>
