<?php

use frontend\assets\DateRangePickerAsset;
use frontend\widgets\Html;
use yii\helpers\Url;
use frontend\widgets\Text;
use yii\widgets\Pjax;

DateRangePickerAsset::register($this);

/* @var yii\web\View $this */
/* @var common\models\City $city */

if(!empty($city)) {
    $this->params['breadcrumbs'][] = ['label' => 'Реклама в городах', 'url' => ['/cities']];
    $this->params['breadcrumbs'][] = ['label' => $city->name, 'url' => ['/cities/'.$city->slug]];
    $this->params['subTitle'] = $city->content_ticker;
    $this->title = empty($city->title_ticker) ? "Бегущая строка на ТВ" : $city->title_ticker;
    if(empty($tv)) {
        $this->params['title'] = 'Реклама на ТВ '.$city->where.' - цены на размещение - РА «Нико-Медиа»';
    } else {
        $this->title = 'Бегущая строка на '.$tv->name.' '.$city->where;
        $this->params['title'] = 'Заказать бегущую строку на '.$tv->name.' '.$city->where.' - цены на размещение бегущей строки - РА «НИКО-медиа»';
        $this->params['description'] = 'Размещение рекламы по бегущей строке '.$tv->name.' '.$city->where.'. Рассчитайте стоимость рекламы прямо на сайте, введите ваш текст и узнайте цену.';
    }
    $this->params['cityBanner'] = $city;
} else {
    $this->title = "Бегущая строка на ТВ";
    $this->params['subTitle'] = Text::get('ticker-subtitle', 'Телевидение - наиболее универсальный рекламный канал, позволяет донести рекламное сообщение до жителей небольших отдаленных северных городков, где отсутствует наружная реклама и не ловится большинство радиостанций. Реклама на ТВ массовая и оперативная - ролик можно заменить в течение пары дней, однако эффективна она не для всех. ТВ-ролики незаменимы для основной массы ТНП и раскрутки бренда, а, например, для продажи недвижимости есть более подходящие рекламные каналы.', 'text');
}
$this->params['breadcrumbs'][] = $this->title;

$minDate = date('d.m.y', time() + 2*24*60*60);
// js
$js = <<<JS
var formBox = $('#ticker-form'),
    calcBox = $('#ticker-calc'),
    basketBox = $('#ticker-basket'),
    loader = $('.ajax-loader'),
    loadBlocks = function(self) {
        loader.addClass('active');
        $.ajax({
            url: self.data("url"),
            success: function(responce) {
                self.html(responce).trigger('init.block');
                loader.removeClass('active');
            }
        });
    },
    sorry = function() {
      formBox.find('.new-form-box-form').each(function() {
      var box = $(this),
          input = box.find('#formsorry-phone');
      input.inputmask({"mask": "+9 999 999–99–99"});
      box.find('.yellow-btn').click(function() {
        $.ajax({
            url: '/forms/sorry',
            method: 'post',
            data: {FormSorry: {phone: input.val()}, view: 'ajax'},
            success: function(responce) {
                box.html(responce);
                sorry();
            }
        });
        return false;
      });
      });
    };

$('.ajax-block').each(function(){
    loadBlocks($(this));
    $('html, body').animate({scrollTop: 0},500);
}).on('reload.block', function() {
  loadBlocks($(this));
});
formBox.on('init.block', function() {
  var self = formBox,
    form = self.find('form');
    form.find('.add-more-number').click(function() {
      // var div = form.find('.form-advert-input').clone();
      // form.find('.more-phones').append(div.html())
      return false;
    });
    form.find('.kanal-item label').each(function() {
      var link = $(this).find('.show-hint-label'),
            hint = $(this).find('.hint-label');
      link.hover(function() {
        hint.addClass('active');
      }, function() {
        hint.removeClass('active');
      });
    });
    form.find('.add-more-number-box').each(function() {
      var moreBox = $(this),
        link = moreBox.find('a');

      link.click(function() {
          $('.new-phone').show();
          $('#userticker-phone').focus();
          moreBox.hide();
        return false;
      })
    });
    sorry();
    function ajaxForm() {
        $.ajax({
            url: form.attr("action"),
            method: form.attr("method"),
            data: form.serialize(),
            success: function(responce) {
                self.html(responce).trigger('init.block');
                calcBox.trigger('reload.block');
                basketBox.trigger('reload.block');
            }
        });
    }
    self.find('select,input:not(#formsorry-phone),textarea').change(function() {
        ajaxForm();
        return false;
    });
    form.submit(function(){
       ajaxForm();
       return false; 
    });
});
calcBox.on('init.block', function() {
    var self = calcBox;
    customSelect(self);
    self.find('.kalk-item').each(function() {
       var view = $(this).find('.sources'),
        id = $(this).find('.ticker-id'),
        datePick = $(this).find('.datepick'),
        dateFromHidden = $(this).find('.datepick-hidden-form'),
        dateToHidden = $(this).find('.datepick-hidden-to'),
        dayCountdatepick = $(this).find('.dayCountdatepick'),
        price = $(this).find('.footer-kalk > .td'),
        saveSettings = function() {
          $.ajax({
              url: '/ticker/update',
              method: 'post',
              data: {
                  id: id.val(),
                  view_type: view.val(),
                  date_from: dateFromHidden.val(),
                  date_to: dateToHidden.val(),
              },
              success: function(html) {
                  price.html(html);
                  basketBox.trigger('reload.block');
                  calcBox.trigger('reload.block');
              }
          });
        };
        datePick.daterangepicker({
            locale: {
              format: 'DD.MM',
              applyLabel: 'Готово',
              cancelLabel: 'Отмена'
            },
            minDate: '$minDate'
        }, function(start, end) {
            dateFromHidden.val(start.format('YYYY-MM-DD'));
            dateToHidden.val(end.format('YYYY-MM-DD'));
            dayCountdatepick.html('(Loading...)');
            saveSettings();
        });
        view.change(function() {
          saveSettings();
        });
    });
});
basketBox.on('init.block', function() {
    var self = basketBox;
    self.find('.basket-block-body-dell-item a').click(function() {
      $.ajax({
          url: $(this).attr('href'),
          success: function() {
              basketBox.trigger('reload.block');
              formBox.trigger('reload.block');
              calcBox.trigger('reload.block');
          }
      });
      return false;
    });
});

JS;
$this->registerJs($js);
?>
<div class="vubor-kanala">
    <div class="container relative">
        <div class="ajax-loader"></div>
        <div class="row">
            <?php if(!empty($city) && empty($tickers)):?>
                <!-- НОВАЯ ФОРМА -->
                <div class="new-form-box">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                            <div class="form-block">
                                <div class="main-h3">
                                    Извините, сейчас мы не можем отобразить данные по этой услуге на сайте.
                                </div>
                                <p class="new-form-text">Мы обязательно сделаем для вас предложение. Заполните форму ниже, и мы свяжемся с вами, как можно скорее.</p>
                                <?php Pjax::begin(['enablePushState' => false]) ?>
                                <?= $this->render('/forms/sorry', ['model' => new common\models\FormSorry()])?>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 white-bg">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                                    <div class="manager-block no-bg">
                                        <img class="manager-img-new" src="<?=Yii::$app->girl->photo?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class="callus callus-new">
                                <img src="/img/icon-tel-yslygi.png" alt="" class="new-form-img-mrg">
                                <div class="callus-box">
                                    <p>Позвоните нам по телефону</p>
                                    <?php
                                    $phone = Yii::$app->city->phone;
                                    ?>
                                    <b><span class="hidden-lg hidden-md hidden-sm hidden-xs"><?=substr($phone, 0, 2)?></span><?=substr($phone, 2)?></b>
                                </div>
                                <div class="another-callus-box">
                                    <p class="new-form-font">
                                        <b><?=Yii::$app->girl->name?>,</b><br>
                                        <i>Менеджер по работе с клиентами</i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /НОВАЯ ФОРМА -->
            <?php else:?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <?=Html::tag('div', '', ['class' => 'ajax-block', 'id' => 'ticker-form', 'data-url' => Url::to(['form', 'city' => !empty($city) ? $city->id : null, 'tv' => !empty($tv) ? $tv->id: null])])?>
                    <?=Html::tag('div', '', ['class' => 'ajax-block', 'id' => 'ticker-calc', 'data-url' => Url::to(['calc'])])?>
                    <div class="modal fade" id="order" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="form-header text-center">
                                <a class="form-close" data-dismiss="modal" aria-hidden="true">
                                    <span class="close-one"></span>
                                    <span class="close-two"></span>
                                </a>
                                <h4>Оформить <br>заказ</h4>
                            </div>
                            <div class="form-body">
                                <?php
                                $model = new common\models\QuickOrder();
                                $model->model_id = $model_id;
                                $model->model_type = 'ticker';
                                ?>
                                <?= $this->render('/basket/quick-order-widget', ['model' => $model])?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="sidebar-affix">
                    <?=Html::tag('div','', ['class' => 'ajax-block', 'id' => 'ticker-basket', 'data-url' => Url::to(['basket'])])?>
                </div>
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="fixator-affix"></div>
    <div class="prichinu">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="text-center"><?=Text::get('ticker-causes-title', '4 причины заказать рекламу на ТВ у нас', 'string')?></h2>
                </div>
                <?php ob_start(); ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                    <div class="prichinu-item">
                        <div>
                            <div class="red-title">8</div>
                            <div class="prichinu-item-title">Лет опыта</div>
                        </div>
                        <p>
                            Работаем с 2006 года
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                    <div class="prichinu-item">
                        <div>
                            <div class="red-title">30</div>
                            <div class="prichinu-item-title">Городов</div>
                        </div>
                        <p>
                            Обширная филиальная сеть
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                    <div class="prichinu-item">
                        <div>
                            <div class="red-title">700</div>
                            <div class="prichinu-item-title">Проектов</div>
                        </div>
                        <p>
                            Ежедневно оттачиваем свое мастерство
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                    <div class="prichinu-item">
                        <div>
                            <div class="red-title">2 500</div>
                            <div class="prichinu-item-title">Клиентов</div>
                        </div>
                        <p>
                            Постоянные клиенты из года в год
                        </p>
                    </div>
                </div>
                <?php
                $text = ob_get_contents();
                ob_end_clean();
                ?>
                <?=Text::get('ticker-causes-text', $text, 'html')?>
            </div>
        </div>
    </div>
</div>
<div class="container seo-content" style="margin-top: 50px; margin-bottom: 50px;">
    <div class="row">
        <?php if(!empty($city)):?>
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                <?php if(empty($tv)):?>
                    <?=Text::get('after-ticker-form-'.$city->slug)?>
                    <?php if(!empty($tickers)):?>
                        <?php
                        $arr = [];
                        foreach ($tickers as $ticker) {
                            $arr[] = Html::a($ticker->tv->name, ['/ticker/'.$city->slug.'/'.$ticker->tv->slug]);
                        }
                        ?>
                        <p>Размещаем рекламу на телеканалах: <?=implode(', ', $arr)?>.</p>
                    <?php endif?>
                <?php else:?>
                    <?= Text::get('ticker-city-'.$city->name.'tv-'.$tv->name, '<p>Размещение рекламы по бегущей строке '.$tv->name.' '.$city->where.'. Рассчитайте стоимость рекламы прямо на сайте, введите ваш текст и узнайте цену. Появились вопросы? Позвоните или напишите нам, проконсультируем по всем интересующим вопросам</p>', 'html');?>
                <?php endif;?>
            </div>
        <?php else:?>
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                <?=Text::get('after-ticker-form'); ?>
            </div>
        <?php endif;?>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg form-shake">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?= Text::get('outdoor-form-title', 'Остались вопросы?', 'string') ?></div>
                        <p><?= Text::get(
                                'outdoor-form-feedback-text',
                                'Давайте обсудим Ваши задачи. И сделаем их',
                                'string'
                            ) ?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()]) ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl') ?>
            </div>
        </div>
    </div>
</div>
