<?php

/* @var yii\web\View $this */
/* @var common\models\Customer $customers */

$this->title = "Наши клиенты";
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;
?>
<div class="klientu-container">
    <div class="container">
        <div class="row">
            <div class="klientu-block">
                <?php foreach ($customers as $customer):?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <div class="item-img">
                            <?php /*<a href="<?=Url::to(['/customers/'.$customer->slug])?>"><img title="<?=$customer->name?>" src="<?=$customer->image->getSrc(300)?>" alt="<?=$customer->name?>"></a>*/?>
                            <img title="<?=$customer->name?>" src="<?=$customer->image->getSrc(300)?>" alt="<?=$customer->name?>">
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
