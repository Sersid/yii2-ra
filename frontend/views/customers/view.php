<?php

/* @var yii\web\View $this */
/* @var common\models\Customer $model */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => "Нали клиенты", 'url' => ['/customers']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;
?>
<div class="container"><?=$model->content?></div>
