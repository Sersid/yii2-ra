<?php

/* @var yii\web\View $this */
/* @var array $total */
/* @var array $outdoorAdItems */
/* @var array $ticker */
/* @var array $radioItems */
/* @var array $tvItems */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="basket-button-block">
    <div class="basket-button-box">
        <span class="summ-tovar <?php echo ($total['cnt'] > 0) ? '' : 'hidden'; ?>"><?= $total['cnt'] ?></span>
        <a class="basket-button-close" href="<?= Url::to(['/basket']) ?>">
            <span class="close-one"></span>
            <span class="close-two"></span>
        </a>
        <div href="/basket" class="basket-button-icon">
            <img src="/img/cart.svg" alt="">
        </div>
        <a href="/basket" class="basket-button-icon hidden-lg hidden-md hidden-sm hidden-xs">
            <img src="/img/cart.svg" alt="">
        </a>
    </div>
</div>

<div class="fade-basket-block">
    <div class="basket-block">
        <?php if ($total['cnt'] > 0): ?>
            <ul class="basket-block-body">
                <?php foreach ($outdoorAdItems as $item): ?>
                    <li>
                        <div class="basket-block-body-title"><a target="_blank" href="<?= $item['url'] ?>"><?= $item['format'] ?> №<?= $item['number'] ?></a></div>
                        <div class="basket-block-body-count">
                            <div class="basket-block-body-time">
                                <?= Yii::t(
                                    'app',
                                    '{months, plural, one{# месяц} few{# месяца} many{# месяцев} other{# месяцев}}',
                                    ['months' => $item['months']]
                                ) ?>
                                <?= $item['install'] || $item['print'] || $item['design'] ? '+' : '' ?>
                                <?php
                                $additional = [];
                                if ($item['install']) {
                                    $additional[] = 'монтаж';
                                }
                                if ($item['print']) {
                                    $additional[] = 'печать';
                                }
                                if ($item['design']) {
                                    $additional[] = 'дизайн';
                                }
                                ?>
                                <?= implode(', ', $additional) ?>
                            </div>
                            <div class="basket-block-body-price">
                                <?php if ($item['total_old_price'] > $item['total_price']) : ?>
                                    <span class="old-price"><?= number_format($item['total_old_price'], 0, ' ', ' ') ?> руб.</span>
                                <?php endif ?>
                                <?= number_format($item['total_price'], 0, ' ', ' ') ?> руб.
                            </div>
                        </div>
                        <div class="basket-block-body-dell-item">
                            <?= Html::a(
                                '&nbsp;',
                                ['/outdoor-ad/delete', 'id' => $item['id']],
                                ['title' => 'Удалить из корзины']
                            ) ?>
                        </div>
                    </li>
                <?php endforeach; ?>
                <?php foreach ($ticker['items'] as $item): ?>
                    <li>
                        <div class="basket-block-body-title"><?= $item['name'] ?></div>
                        <div class="basket-block-body-count">
                            <?php if (!empty($item['error'])): ?>
                                <div class="basket-block-body-time" style="color:red;">
                                    <?= $item['error'] ?>
                                </div>
                            <?php else: ?>
                                <div class="basket-block-body-time">
                                    <?= Yii::t(
                                        'app',
                                        '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}',
                                        ['days' => $item['cnt_days']]
                                    ) ?>, <?= Yii::t(
                                        'app',
                                        '{words, plural, one{# слово} few{# слова} many{# слов} other{# слов}}',
                                        ['words' => $item['words']]
                                    ) ?> <span>(<?= number_format($item['price'], 0, ' ', ' ') ?> руб./слово)</span>
                                </div>
                                <div class="basket-block-body-price"><?= number_format($item['total'], 0, ' ', ' ') ?>
                                    руб.
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="basket-block-body-dell-item">
                            <?= Html::a(
                                '&nbsp;',
                                ['/ticker/delete', 'id' => $item['user_ticker_id'], 'ticker_id' => $item['ticker_id']],
                                ['title' => 'Удалить из корзины']
                            ) ?>
                        </div>
                    </li>
                <?php endforeach; ?>
                <?php foreach ($radioItems as $item): ?>
                    <li>
                        <div class="basket-block-body-title"><?= $item['radio'] ?></div>
                        <div class="basket-block-body-count">
                            <div class="basket-block-body-time"><?= (int)$item['duration'] ?>сек., <?= $item['price'] ?>
                                р./день, <?= Yii::t(
                                    'app',
                                    '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}',
                                    ['days' => (int)$item['days']]
                                ) ?></div>
                            <div class="basket-block-body-price"><?= number_format($item['total'], 0, ' ', ' ') ?>руб.
                            </div>
                        </div>
                        <div class="basket-block-body-dell-item">
                            <?= Html::a(
                                '&nbsp;',
                                ['/radio/delete', 'id' => $item['id'], 'radio_ad_id' => $item['radio_ad_id']],
                                ['title' => 'Удалить из корзины']
                            ) ?>
                        </div>
                    </li>
                <?php endforeach; ?>
                <?php foreach ($tvItems as $item): ?>
                    <li>
                        <div class="basket-block-body-title"><?= $item['tv'] ?></div>
                        <div class="basket-block-body-count">
                            <?php if ($item['total'] > 0): ?>
                                <div class="basket-block-body-time"><?= (int)$item['duration'] ?>
                                    сек., <?= $item['price'] ?> р./день, <?= Yii::t(
                                        'app',
                                        '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}',
                                        ['days' => (int)$item['days']]
                                    ) ?></div>
                                <div class="basket-block-body-price"><?= number_format($item['total'], 0, ' ', ' ') ?>
                                    руб.
                                </div>
                            <?php else: ?>
                                <div class="basket-block-body-time">
                                    Стоимость будет известна после уточнения всех деталей
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="basket-block-body-dell-item">
                            <?= Html::a(
                                '&nbsp;',
                                ['/tv/delete', 'id' => $item['id'], 'tv_ad_id' => $item['tv_ad_id']],
                                ['title' => 'Удалить из корзины']
                            ) ?>
                        </div>
                    </li>
                <?php endforeach; ?>
                <li class="basket-no-padding">
                    <div class="basket-block-footer">
                        <div class="basket-block-footer-price">
                            <?php if ($total['total'] > 0): ?>
                                <b>Итого:</b>
                                <div><?= number_format($total['total'], 0, ' ', ' ') ?> <span>руб.</span></div>
                            <?php endif; ?>
                        </div>
                        <?= Html::a(
                            'Оформить',
                            '#',
                            ['class' => 'yellow-btn', 'data-toggle' => "modal", 'data-target' => "#order"]
                        ) ?>
                    </div>
                </li>
            </ul>
        <?php else: ?>
            <ul class="basket-block-body">
                <li><p>Ваша корзина пуста</p></li>
            </ul>
        <?php endif; ?>
    </div>
</div>
