<?php

/* @var yii\web\View $this */

?>
<?php if($total['cnt'] > 0):?>
<div class="basket-case-footer">
    <div class="col-lg-4 col-md-4 hidden-sm col-xs-12"></div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="basket-end-price">
            <b>Итого:</b>
            <p><?=Yii::t('app', '{cnt, plural, one{# услуга} few{# услуги} many{# услуг} other{# услуг}}', ['cnt' => $total['cnt']])?> на сумму <b><?=number_format($total['total'], 0, ' ', ' ')?> руб.</b></p>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-right">
        <a data-toggle="modal" data-target="#order" href="#" class="yellow-btn shadd">Отправить заявку</a>
    </div>
</div>
<?php else:?>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <br><br>
        <p>Ваша корзина пуста</p>
        <br><br>
    </div>
<?php endif;?>
