<?php
use frontend\widgets\Html;
use yii\helpers\Url;

/* @var yii\web\View $this */

$this->title = "Корзина";
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
var messageBox = $('#message'),
    showMessage = $('#show-message'),
    loadBlocks = function(self) {
        $.ajax({
            url: self.data("url"),
            success: function(responce) {
                self.html(responce);
                self.find('.basket-block-body-dell-item a').click(function() {
                    console.log($(this));
                  $.ajax({
                      url: $(this).attr('href'),
                      success: function() {
                          loadBlocks(self);
                          $('.basket-box').trigger('reload.basket');
                      }
                  });
                  return false;
                });
                self.find('form').each(function() {
                  var form = $(this),
                    button = form.find('.add-to-basket'),
                    sendData = function() {
                      $.ajax({
                          url: '/outdoor-ad/add-to-basket',
                          data: form.serialize(),
                          method: 'post',
                          success: function(data) {
                              messageBox.html(data);
                              showMessage.trigger('click');
                              $('.basket-box').trigger('reload.basket');
                              return false;
                          }
                      });
                    };
                  button.click(function() {
                    sendData();
                  });
                  form.find('input').change(function() {
                    sendData();
                  });
                  
                });
                self.find('.mesto-item-list').each(function () {
                    var box = $(this);
                    box.click(function () {
                        if(box.hasClass('active')) {
                            box.removeClass('active');
                        } else {
                            $('html,body').stop().animate({ scrollTop: $(box).offset().top }, 1000);
                            box.addClass('active');
                        }
                    });
                });
                self.find('.owl-carousel').owlCarousel({
                    items : 1, 
                    itemsDesktop : 1,
                    itemsDesktopSmall : 1,
                    itemsTablet: 1,
                    itemsMobile : 1
                });
            }
        });
    };

$('.ajax-block').each(function(){
    var self = $(this);
    loadBlocks(self);
    self.on('reload.box', function () {
        loadBlocks(self);
    });
});
JS;
$this->registerJs($js);

?>
<script src="https://api-maps.yandex.ru/2.1.47/?lang=ru_RU"></script>
<a href="#message" id="show-message" class="fancybox"></a>
<div id="message" style="display: none"></div>
<div class="basket-container">
    <div class="container">
        <div class="row">
            <div class="transport-block margin-bottom-40">
                <?=Html::tag('div', '', ['class' => 'ajax-block', 'data-url' => Url::to(['outdoor-ad'])])?>
                <?=Html::tag('div', '', ['class' => 'ajax-block', 'data-url' => Url::to(['tv'])])?>
                <?=Html::tag('div', '', ['class' => 'ajax-block', 'data-url' => Url::to(['radio'])])?>
                <?=Html::tag('div', '', ['class' => 'ajax-block', 'data-url' => Url::to(['tickers'])])?>
                <?=Html::tag('div', '', ['class' => 'basket-box', 'data-url' => Url::to(['total'])])?>
                <div class="modal fade" id="order" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="form-header text-center">
                            <a class="form-close" data-dismiss="modal" aria-hidden="true">
                                <span class="close-one"></span>
                                <span class="close-two"></span>
                            </a>
                            <h4>Оформить <br>заказ</h4>
                        </div>
                        <div class="form-body">
                            <?= $this->render('/basket/order-widget', ['model' => new common\models\Order()])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
