<?php

use frontend\widgets\Html;

/* @var yii\web\View $this */

?>
<?php if(!empty($items)):?>
<div class="basket-case horiz-case five-column-stroka">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>Бегущая строка</h2>
        <div class="naryw-table-block basket-rekl-table begyw-box">
            <div class="panel-group" id="accordion">
                <table class="shadow">
                    <thead>
                    <tr class="mesto-item">
                        <th>Канал</th>
                        <th>Кол-во слов</th>
                        <th>Цена за день</th>
                        <th>Период проката</th>
                        <th>Итого</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($items as $item):?>
                    <tr class="mesto-item-list basket-case-item other-serv">
                        <td data-label="Канал">
                            <div class="td"><b><?=$item['name']?></b></div>
                        </td>
                        <?php if(!empty($item['error'])):?>
                            <td colspan="4" class="error-td">
                                <div class="td"><?=$item['error']?></div>
                            </td>
                        <?php else:?>
                            <td data-label="Кол-во слов">
                                <div class="td"><?=Yii::t('app', '{words, plural, one{# слово} few{# слова} many{# слов} other{# слов}}', ['words' => $item['words']])?> (<?=number_format($item['price'], 0, ' ', ' ')?> руб./слово)</div>
                            </td>
                            <td data-label="Цена за день">
                                <div class="td"><?=number_format($item['total_day'], 0, ' ', ' ')?> руб.</div>
                            </td>
                            <td data-label="Период проката">
                                <div class="td"><?=Yii::t('app', '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}', ['days' => $item['cnt_days']])?></div>
                            </td>
                            <td data-label="Итого">
                                <div class="td"><?=number_format($item['total'], 0, ' ', ' ')?> руб.</div>
                            </td>
                        <?php endif?>
                        <td>
                            <div class="td">
                                <div class="basket-block-body-dell-item">
                                    <?=Html::a('', ['/ticker/delete', 'id' => $item['user_ticker_id'], 'ticker_id' => $item['ticker_id']], ['title' => 'Удалить из корзины'])?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php endif?>
