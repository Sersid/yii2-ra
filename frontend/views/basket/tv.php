<?php

use common\models\UserTvAd;
use frontend\widgets\Html;

/* @var yii\web\View $this */

?>
<?php if(!empty($items)):?>
    <div class="basket-case horiz-case five-column-rekl">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Реклама на ТВ</h2>
            <div class="naryw-table-block basket-rekl-table tv-box">
                <div class="panel-group" id="accordion">
                    <table class="shadow">
                        <thead>
                        <tr class="mesto-item">
                            <th>Канал</th>
                            <th>Город</th>
                            <th>Наименование</th>
                            <th>Кол-во выходов</th>
                            <th>Цена</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($items as $item):?>
                        <tr class="mesto-item-list basket-case-item other-serv">
                            <td data-label="Канал">
                                <div class="td"><b><?=$item['tv']?></b></div>
                            </td>
                            <td data-label="Город">
                                <div class="td"><?=$item['city']?></div></td>
                            <td data-label="Наименование">
                                <div class="td"><?=$item['type']?></div></td>
                            <?php if($item['type_id'] == UserTvAd::TYPE_AD):?>
                                <td data-label="Кол-во выходов">
                                    <div class="td"><?=(int)$item['duration']?> сек., <?=number_format($item['price'], 0, ' ', ' ')?> руб./день, <?=Yii::t('app', '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}', ['days' => (int)$item['days']])?></div></td>
                                <td data-label="Цена">
                                    <div class="td"><?=number_format($item['total'], 0, ' ', ' ')?> руб.</div>
                                </td>
                            <?php else:?>
                                <td data-label="Кол-во выходов" class="row-price-info">
                                    <div class="td">Стоимость будет известна после уточнения всех деталей</div>
                                </td>
                                <td></td>
                            <?php endif?>
                            <td>
                                <div class="td">
                                    <div class="basket-block-body-dell-item">
                                        <?=Html::a('&nbsp;', ['/tv/delete', 'id' => $item['id'], 'tv_ad_id' => $item['tv_ad_id']], ['title' => 'Удалить из корзины'])?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php endif?>
