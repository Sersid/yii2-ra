<?php

use himiklab\yii2\recaptcha\ReCaptcha3;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var common\models\Order $model */
/* @var yii\web\View $this */

$this->title = "Оформить заказ";
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
$('#quickorder-phone').inputmask({"mask": "+9 999 999–99–99"});
JS;
$this->registerJs($js);
?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
        <?php if(isset($success) && $success):?>
          <div class="alert alert-success">
            Спасибо за то, что оставили заявку. В ближайшее время наш менеджер свяжется с Вами для уточнения всех деталей, и при необходимости
            окажет консультацию по интересующим вас вопросам. <br>
            Вы также можете связаться с нами самостоятельно по номеру телефона +7 (3462) 550-877 или по электронной почте
            <a href="mailto:zakaz@niko-m.ru">zakaz@niko-m.ru</a>
          </div>
        <?php if(!empty($dataLayer)):?>
            <?php
            ob_start();?>
          window.dataLayer.push (<?=json_encode($dataLayer)?>);
            <?php foreach ($dataLayer['ecommerce']['purchase']['products'] as $product):?>
            ga('ecommerce:addItem', {
            'id': '<?=$product['id']?>',
            'name': '<?=$product['name']?>',
            'category': '<?=$product['category']?>',
            'price': '<?=$product['price']?>',
            'quantity': '1',
            'currency': 'RUR'
            });
            <?php endforeach?>
          ga('ecommerce:send');

            <?php
            $js = ob_get_contents();
            ob_end_clean();
            $this->registerJs($js);
            ?>
          <script type="text/javascript">
              <?php switch ($model->model_type) {
                  case 'tv':
                      $event = 'TV';
                      break;
                  case 'ticker':
                      $event = 'STROKA';
                      break;
                  case 'radio':
                      $event = 'RADIO';
                      break;
                  default:
                      $event = 'NARUZHKA';
                      break;
              }?>
              window.onload = function() {
                  yaCounter17709289.reachGoal('<?=$event?>', function(){console.log('Отправлено')});
                  gtag('event', 'forms_send', {
                      'event_category': 'forms',
                      'event_label': '<?=$event?>'
                  });
              };
          </script>
        <?php endif;?>
        <?php else:?>
        <?php $form = ActiveForm::begin([
            'action' => ['/basket/quick-order'],
        ]); ?>
        <?= $form->field($model, 'model_id')->hiddenInput() ?>
        <?= $form->field($model, 'model_type')->hiddenInput() ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Иван']) ?>
        <?= $form->field($model, 'phone')->textInput([
            'type'=> 'tel',
            'maxlength' => true,
            'placeholder' => '+7 3462 11-22-33'
        ]) ?>
        <?= $form->field($model, 'email')->textInput([
            'maxlength' => true,
            'placeholder' => 'ivanov@mail.ru',
            'type' => 'email',
        ]) ?>
        <?= $form->field($model, 'reCaptcha', ['template' => '{input}'])
            ->widget(ReCaptcha3::class, ['action' => 'quickorder']) ?>
        <?= Html::submitButton('Заказать', ['class' => 'yellow-btn']) ?>
        <?php ActiveForm::end(); ?>
          <div class="politics">
            <a href="/img/docs/Soglasie_na_obrabotku_pdn_niko.pdf" target="_blank">Согласие на обработку персональных данных</a>
          </div>

        <?php endif;?>
    </div>
  </div>
</div>
