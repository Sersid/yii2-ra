<?php

use frontend\widgets\Html;

/* @var yii\web\View $this */

?>
<?php if(!empty($items)):?>
<div class="basket-case">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>Наружная реклама</h2>
        <div class="naryw-table-block basket-rekl-table">
            <div class="panel-group" id="accordion">
                <table class="shadow">
                    <thead>
                    <tr class="mesto-item">
                        <th><span>№</span></th>
                        <th>
                            <span>Город
<!--                                <div class="filter-box hidden-sm hidden-xs">-->
<!--                                    <a class="filter-up active" href="#"></a>-->
<!--                                    <a class="filter-down" href="#"></a>-->
<!--                                </div>-->
                            </span>
                        </th>
                        <th>
                            <span>Адрес
<!--                                <div class="filter-box hidden-sm hidden-xs">-->
<!--                                    <a class="filter-up" href="#"></a>-->
<!--                                    <a class="filter-down" href="#"></a>-->
<!--                                </div>-->
                            </span>
                        </th>
                        <th>
                            <span>Поверхность
<!--                                <div class="filter-box hidden-sm hidden-xs">-->
<!--                                    <a class="filter-up" href="#"></a>-->
<!--                                    <a class="filter-down" href="#"></a>-->
<!--                                </div>-->
                            </span>
                        </th>
                        <th>
                            <span>Цена (руб/мес)
<!--                                <div class="filter-box hidden-sm hidden-xs">-->
<!--                                    <a class="filter-up" href="#"></a>-->
<!--                                    <a class="filter-down" href="#"></a>-->
<!--                                </div>-->
                            </span>
                        </th>
                        <th>
                            <span>Сторона
<!--                                <div class="filter-box hidden-sm hidden-xs">-->
<!--                                    <a class="filter-up" href="#"></a>-->
<!--                                    <a class="filter-down" href="#"></a>-->
<!--                                </div>-->
                            </span>
                        </th>
                        <th>
                            <span>Рейтинг
<!--                                <div class="filter-box hidden-sm hidden-xs">-->
<!--                                    <a class="filter-up" href="#"></a>-->
<!--                                    <a class="filter-down" href="#"></a>-->
<!--                                </div>-->
                            </span>
                        </th>
                        <th>
                            <span>Статус
<!--                                <div class="filter-box hidden-sm hidden-xs">-->
<!--                                    <a class="filter-up" href="#"></a>-->
<!--                                    <a class="filter-down" href="#"></a>-->
<!--                                </div>-->
                            </span>
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($items as $item):?>
                    <tr class="mesto-item-list basket-case-item" data-toggle="collapse" data-parent="#accordion" href="#mesto-<?=$item['id']?>" class="collapsed">
                        <td data-label="№">
                            <div class="td">
                                <a data-toggle="collapse" data-parent="#accordion" href="#mesto-<?=$item['id']?>" class="collapsed">
                                    <div class="mesto-title">
                                        <b class="hidden-lg hidden-md">Место №</b>
                                        <b><?=$item['number']?></b>
                                    </div>
                                </a>
                            </div>
                        </td>
                        <td data-label="Город">
                            <div class="td"><?=$item['city']?></div></td>
                        <td data-label="Адрес">
                            <div class="td"><?=$item['address']?></div></td>
                        <td data-label="Поверхность">
                            <div class="td"><?=$item['format']?></div></td>
                        <td data-label="Цена (руб/мес)">
                            <div class="td"><?=$item['price']?></div></td>
                        <td data-label="Сторона">
                            <div class="td"><?=$item['side']?></div></td>
                        <td data-label="Рейтинг">
                            <div class="td">
                                <div class="hint-item">
                                    <div class="rating">
                                        <?php for ($i = 1; $i <= 3; $i++):?>
                                            <label <?=$item['rating'] >= $i ? 'class="active"' : ''?>><i class="fa fa-star"></i></label>
                                        <?php endfor;?>
                                    </div>
                                    <div class="hint-text hidden-sm hidden-xs">
                                        <div class="hint-body">
                                            <p class="one-star">плохое место, размещение на нем мы не рекомендуем.</p>
                                            <p class="two-stars">нормальное место, можно уверенно размещаться.</p>
                                            <p class="three-stars">хорошее место, которое мы однозначно можем рекомендовать.</p>
                                        </div>
                                        <div class="hint-footer">
                                            <b>Не забывайте, в рекламе важно не только хорошее место, но и качественный рабочий дизайн рекламы</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="footer-kalk" data-label="Статус">
                            <div class="td green-title">Свободно</div>
                        </td>
                        <td>
                            <div class="td">
                                <div class="basket-block-body-dell-item">
                                    <?=Html::a('&nbsp;', ['/outdoor-ad/delete', 'id' => $item['id']], ['title' => 'Удалить из корзины'])?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="mesto-item-body">
                        <td colspan="8" class="panel panel-default">
                            <form>
                                <input type="hidden" name="id" value="<?=$item['outdoor_ad_id']?>">
                            <div id="mesto-<?=$item['id']?>" class="panel-collapse collapse">
                                <div class="mesto-descr no-padding-bottom">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rekl-box-case">
                                        <b>Дополнительные услуги:</b>
                                        <div class="rekl-box-serv-item">
                                            <div class="serv-item-checkbox">
                                                <input type="checkbox" id="i<?=$item['id']?>" disabled <?=$item['install'] ? 'checked' : ''?>>
                                                <label for="i<?=$item['id']?>">Монтаж в месяц размещения - <?=$item['price_install']?> руб./шт.</label>
                                                <input type="hidden" data-price="<?=$item['price_install']?>" name="install" value="1">
                                            </div>
                                            <div class="serv-item-checkbox">
                                                <input type="checkbox" name="print" id="p<?=$item['id']?>" value="1" <?=$item['print'] ? 'checked' : ''?>>
                                                <label for="p<?=$item['id']?>">Печать на баннере - <?=$item['price_print']?> руб./шт.</label>
                                            </div>
                                            <div class="serv-item-checkbox">
                                                <input type="checkbox" name="design" id="d<?=$item['id']?>" value="1" <?=$item['design'] ? 'checked' : ''?>>
                                                <label for="d<?=$item['id']?>">Дизайн - от <?=$item['price_design']?> руб.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php if(!empty($item['images'])):?>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-right-10">
                                                <div <?=count($item['images']) > 1 ? 'class="owl-carousel"' : ''?>>
                                                    <?php foreach ($item['images'] as $image):?>
                                                        <div class="item"><a rel="group-<?=$item['id']?>"  href="<?=$image['full']?>" class="fancybox mesto-img" style="background: url(<?=$image['sm']?>) no-repeat; background-size:cover; background-position: center center;"></a></div>
                                                    <?php endforeach;?>
                                                </div>
                                            </div>
                                        <?php endif?>
                                        <?php if(!empty($item['lat']) && !empty($item['lon'])):?>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-left-10">
                                                <script>
                                                    ymaps.ready(function(){
                                                        var map = new ymaps.Map("outdoor-ad-map-<?=$item['id']?>", {
                                                            center: [<?=$item['lat']?>, <?=$item['lon']?>],
                                                            zoom: 15,
                                                            controls: ['zoomControl', 'typeSelector', 'fullscreenControl']
                                                        });
                                                        var trafficControl = new ymaps.control.TrafficControl({state: {trafficShown: true}});
                                                        map.controls.add(trafficControl, {top: 10, left: 10});
                                                        map.geoObjects.add(new ymaps.Placemark([<?=$item['lat']?>, <?=$item['lon']?>]));
                                                    });
                                                </script>
                                                <div class="mesto-maps" id="outdoor-ad-map-<?=$item['id']?>"></div>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rekl-box-case">
                                        <?php foreach ($item['dates'] as $date):?>
                                            <div class="rekl-box-calendar <?=$date['disabled'] ? 'no-active-calendar' : 'active-calendar'?> text-center">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                    <div class="rekl-box-year"><?=$date['year']?></div>
                                                    <?php foreach ($date['months'] as $month):?>
                                                        <div class="col-lg-1-1 col-md-1-1 col-sm-1-1 col-xs-2-5 rekl-box-month">
                                                            <p><?=$month['name']?></p>
                                                            <?php if($month['disabled']):?>
                                                                <label class="disabled-month"></label>
                                                            <?php else:?>
                                                                <div class="month-checkbox">
                                                                    <input type="checkbox" name="mounts[]" value="<?=$month['num']?>.<?=$date['year']?>" id="m<?=$item['id']?>-<?=$date['year']?>-<?=$month['num']?>" <?=$month['is_busy'] ? 'disabled' : ''?> <?=$month['checked'] ? 'checked' : ''?>>
                                                                    <label for="m<?=$item['id']?>-<?=$date['year']?>-<?=$month['num']?>"></label>
                                                                </div>
                                                            <?php endif;?>
                                                        </div>
                                                    <?php endforeach;?>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-right-10">
                                        <div class="more-inform-btn">
                                            <?= \yii\helpers\Html::a('Узнать подробнее', ['/outdoor-ad/view', 'id' => $item['outdoor_ad_id']])?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-left-10">
                                        <div class="in-basket-btn">
                                            <a class="yellow-btn add-to-basket" href="#">Обновить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
