<?php

use himiklab\yii2\recaptcha\ReCaptcha3;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var common\models\Order $model */
/* @var yii\web\View $this */

$js = <<<JS
$('#quickorder-phone, #quickoutdoororder-phone').inputmask({"mask": "+9 999 999–99–99"});
JS;
$this->registerJs($js);
?>
<?php if(isset($success) && $success):?>
  <div class="alert alert-success">
    Спасибо за то, что оставили заявку. В ближайшее время наш менеджер свяжется с Вами для уточнения всех деталей, и при необходимости
    окажет консультацию по интересующим вас вопросам. <br>
    Вы также можете связаться с нами самостоятельно по номеру телефона +7 (3462) 550-877 или по электронной почте
    <a href="mailto:zakaz@niko-m.ru">zakaz@niko-m.ru</a>
  </div>
  <script type="text/javascript">
      yaCounter17709289.reachGoal('NARUZHKA', function(){console.log('Отправлено')});
      gtag('event', 'forms_send', {
          'event_category': 'forms',
          'event_label': 'NARUZHKA'
      });
  </script>
    <?php if(!empty($dataLayer)):?>
    <script type="text/javascript">
        window.dataLayer.push (<?=json_encode($dataLayer)?>);
    </script>
    <?php endif;?>
<?php else:?>
    <?php $form = ActiveForm::begin([
        'action' => [(isset($outdoor) ? '/basket/quick-outdoor-order' : '/basket/quick-order')],
        'id' => 'quick-outdoor-order-form',
    ]); ?>
    <?= Html::activeHiddenInput($model,'model_id') ?>
    <?= Html::activeHiddenInput($model,'model_type') ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Иван']) ?>
    <?= $form->field($model, 'phone')->textInput([
        'type'=> 'tel',
        'maxlength' => true,
        'placeholder' => '+7 3462 11-22-33'
    ]) ?>
    <?= $form->field($model, 'email')->textInput([
        'maxlength' => true,
        'placeholder' => 'ivanov@mail.ru',
        'type' => 'email',
    ]) ?>
    <?= Html::submitButton('Заказать', ['class' => 'yellow-btn']) ?>
    <?= $form->field($model, 'reCaptcha', ['template' => '{input}'])
        ->widget(ReCaptcha3::class, ['action' => 'quickorder']) ?>
    <?php ActiveForm::end(); ?>
  <div class="politics">
    <a href="/img/docs/Soglasie_na_obrabotku_pdn_niko.pdf" target="_blank">Согласие на обработку персональных данных</a>
  </div>
<?php endif;?>
