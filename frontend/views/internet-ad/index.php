<?php

use frontend\models\AdvCase;
use frontend\models\City;
use frontend\widgets\FeedbackForm;
use frontend\widgets\QuickOrderForm;
use frontend\widgets\Reviews;
use frontend\widgets\Html;
use frontend\widgets\SchemeWork;
use frontend\widgets\Text;

/** @var City $city */
/** @var AdvCase[] $cases */

$this->title = "Интернет реклама";
$this->params['breadcrumbs'][] = $this->title;
if (!empty($city)) {
    $this->title .= ' ' . $city->where;
}
$this->registerCssFile('/css/internet-reklama.css');
$this->registerJsFile('/js/internet-reklama.js');
if (!empty($city)) {
    $this->params['title'] = 'Реклама в интернете ' . $city->where . ': контекстная реклама, продвижение сайтов, SMM';
    $this->params['description'] =  'Интернет реклама ' . $city->where . '. Комплексный интернет-маркетинг от создания контекстной рекламы и продвижения сайтов до ведения Ваших соцсетей. Работаем на результат!';
}
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="text-wrap text-wrap--internet">
            <?php if (!empty($city)): ?>
                <p>Комплексная реклама в интернете <?= $city->where ?>. Подберём для вас оптимальный вариант
                    рекламы, проведём бесплатный аудит уже имеющихся рекламных кампаний, поможем с выбором стратегии.
                    Если у вас появились вопросы по размещению и ценам, позвоните нам, проконсультируем по всем
                    интересующим вас вопросам.</p>
            <?php else: ?>
                <p>Основная наша задача заключается в том, чтобы реклама приносила вам прибыль: если вы чувствуете
                    финансовую отдачу от проделанной нами работы, это значит что вы продолжите с нами сотрудничество.
                    Выгодно вам - вагодно нам. Для этого мы используем весь наш опыт и компетенции, накопленные за 12
                    лет работы в рекламном бизнесе.</p>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="internet-reklama-tabs">
    <div class="container">
        <img src="/img/internetTabs-girl.png" alt="#" class="internet-reklama-tabs__image-girl">
        <div class="internet-reklama-tabs__controls">
            <input type="radio" name="internet-reklama-tabs" id="internet-reklama-control1" value="internet-reklama-tab1" checked>
            <label for="internet-reklama-control1">Контекстная <br>реклама</label>
            <input type="radio" name="internet-reklama-tabs" id="internet-reklama-control2" value="internet-reklama-tab2">
            <label for="internet-reklama-control2">Реклама <br>в соцсетях</label>
            <input type="radio" name="internet-reklama-tabs" id="internet-reklama-control3" value="internet-reklama-tab3">
            <label for="internet-reklama-control3">Улучшение <br>сайтов</label>
            <input type="radio" name="internet-reklama-tabs" id="internet-reklama-control4" value="internet-reklama-tab4">
            <label for="internet-reklama-control4">Продвижение <br>сайтов</label>
        </div>
        <div class="internet-reklama-tabs__content">
            <div class="internet-reklama-tabs__item" data-pointer="internet-reklama-tab1">
                <?= Text::get('internet-reklama-tab1', 'Контекстная реклама', 'html') ?>
            </div>
            <div class="internet-reklama-tabs__item" data-pointer="internet-reklama-tab2">
                <?= Text::get('internet-reklama-tab2', 'Медийная реклама', 'html') ?>
            </div>
            <div class="internet-reklama-tabs__item" data-pointer="internet-reklama-tab3">
                <?= Text::get('internet-reklama-tab3', 'Реклама на youtube', 'html') ?>
            </div>
            <div class="internet-reklama-tabs__item" data-pointer="internet-reklama-tab4">
                <?= Text::get('internet-reklama-tab4', 'Продвижение сайтов', 'html') ?>
            </div>
        </div>
    </div>
</div>
<div class="internet-reklama">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="review-content-title">
                    <h2>Сделаем правильно</h2>
                    <div class="text-wrap">
                        <p>Рекламу увидят те, кому она нужна, и в тот момент, когда она нужна. При умелом подходе интернет реклама решит задачи маленькой компании и федеральной корпорации. Количество сценариев показа рекламы ограничивается лишь фантазией.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="internet-reklama__items js-setFormat">
            <div class="internet-reklama__items-layout">
                <div class="internet-reklama__item">
                    <div class="internet-reklama__item-header">
                        <h3>Тариф «Базовый»</h3>
                        <p>Подходит для компаний с небольшим спросом и конкуренцией.</p>
                    </div>
                    <div class="internet-reklama__item-price">
                        <p><strong>От 8 000  руб</strong></p>
                        <p>Первичная</p>
                        <p><strong>От 5 000  руб</strong></p>
                        <p>Ведение компании</p>
                    </div>
                    <ul class="internet-reklama__item-list">
                        <li>Яндекс Директ (поисковая рекламная кампания)</li>
                        <li>Google Ads (поисковая рекламная кампания)</li>
                    </ul>
                    <?= Html::a(
                        'Заказать',
                        '#',
                        [
                            'class' => 'yellow-btn add-to-basket',
                            'data-toggle' => "modal",
                            'data-target' => "#order-format",
                            'data-format' => 'Интернет реклама: Тариф «Базовый»',
                        ]
                    ) ?>
                </div>
            </div>
            <div class="internet-reklama__items-layout">
                <div class="internet-reklama__item">
                    <div class="internet-reklama__item-header">
                        <h3>Тариф «Оптимальный»</h3>
                        <p>Подходит для компаний со средним спросом и конкуренцией.</p>
                    </div>
                    <div class="internet-reklama__item-price">
                        <p><strong>От 15 000  руб</strong></p>
                        <p>Первичная</p>
                        <p><strong>От 8 000  руб</strong></p>
                        <p>Ведение компании</p>
                    </div>
                    <ul class="internet-reklama__item-list">
                        <li>Яндекс Директ (поисковая реклама + РСЯ)</li>
                        <li>Google Ads (поисковая реклама + КМС) </li>
                        <li>Ретаргетинг</li>
                        <li>Настройка аналитики (Яндекс Метрика)</li>
                    </ul>
                    <?= Html::a(
                        'Заказать',
                        '#',
                        [
                            'class' => 'yellow-btn add-to-basket',
                            'data-toggle' => "modal",
                            'data-target' => "#order-format",
                            'data-format' => 'Интернет реклама: Тариф «Оптимальный»',
                        ]
                    ) ?>
                </div>
            </div>
            <div class="internet-reklama__items-layout">
                <div class="internet-reklama__item">
                    <div class="internet-reklama__item-header">
                        <h3>Тариф «Расширенный»</h3>
                        <p>Для компаний, которые хотят охватить макс. кол-во площадок.</p>
                    </div>
                    <div class="internet-reklama__item-price">
                        <p><strong>От 25 000  руб</strong></p>
                        <p>Первичная</p>
                        <p><strong>От 20 000  руб</strong></p>
                        <p>Ведение компании</p>
                    </div>
                    <ul class="internet-reklama__item-list">
                        <li>Яндекс Директ (поисковая реклама + РСЯ)</li>
                        <li>Google Ads (поисковая реклама + КМС) </li>
                        <li>Ретаргетинг</li>
                        <li>Настройка аналитики (Яндекс Метрика)</li>
                        <li>Отслеживание звонков</li>
                        <li>Медийная реклама</li>
                    </ul>
                    <?= Html::a(
                        'Заказать',
                        '#',
                        [
                            'class' => 'yellow-btn add-to-basket',
                            'data-toggle' => "modal",
                            'data-target' => "#order-format",
                            'data-format' => 'Интернет реклама: Тариф «Расширенный»',
                        ]
                    ) ?>
                </div>
            </div>
        </div>
        <?php if (!empty($cases)) { ?>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="review-content-title">
                <h2>Посмотрите наши кейсы</h2>
                <div class="internet-reklama__portfolio">
                    <?php foreach ($cases as $case) {?>
                      <div class="internet-reklama__portfolio-item">
                        <p class="internet-reklama__portfolio-name"><a href="<?= $case->getDetailPageUrl() ?>"><?= Html::encode($case->name) ?></a></p>
                        <p class="internet-reklama__portfolio-description">
                            <?php if (!empty($case->image)): ?>
                        <div class="item-box-img" style="background: url(<?= $case->image->getSrc(325) ?>) no-repeat; background-position: center center; background-size: cover;"></div>
                          <?php endif ?>
                        </p>
                        <a href="<?= $case->getDetailPageUrl() ?>" class="internet-reklama__portfolio-link">Подробнее</a></div>
                    <?php }?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
    </div>
</div>
<?= Reviews::widget() ?>
<?= SchemeWork::widget() ?>
<div class="new-section">
    <div class="container">
        <?php
        if (!empty($city)) {
            echo Text::get('internet-ad-city-' . $city->slug, '', 'html');
        } else {
            echo Text::get('internet-ad', '', 'html');
        } ?>
    </div>
</div>
<?= FeedbackForm::widget() ?>
<?= QuickOrderForm::widget() ?>
