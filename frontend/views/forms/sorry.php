<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var yii\web\View $this */
/* @var common\models\FormSorry $model */
/* @var yii\widgets\ActiveForm $form */
new yii\widgets\ActiveField;
$js = <<<JS
$('#formsorry-phone').inputmask({"mask": "+9 999 999–99–99"});
JS;
$this->registerJs($js);
?>
<?php if(isset($success) && $success):?>
    <div class="alert alert-success">Ваш запрос успешно отправлен</div>
    <script type="text/javascript">
        window.onload = function() {
            yaCounter17709289.reachGoal('NO_SERVICE', function(){console.log('Отправлено')});
            gtag('event', 'forms_send', {
                'event_category': 'forms',
                'event_label': 'NO_SERVICE'
            });
        };
    </script>
<?php else:?>
    <?php $form = ActiveForm::begin([
        'action' => ['/forms/sorry'],
        'options' => ['data-pjax' => true],
        'fieldConfig' => [
            'template' => "{input}\n{error}",
            'inputOptions' => ['class' => ''],
        ],
    ]); ?>
    <div class="form-group row">
        <?= $form->field($model, 'phone', [
            'options' => ['class' => 'col-lg-7 col-md-12 col-sm-12 col-xs-12']
        ])->textInput([
            'type'=> 'tel',
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('phone'),
            'class' => 'new-form-input',
        ]) ?>
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 zindex">
            <?= Html::submitInput('Отправить', ['class' => 'yellow-btn new-btn']) ?>
        </div>
    </div>
    <div class="politics">
        <a href="/img/docs/Soglasie_na_obrabotku_pdn_niko.pdf" target="_blank" data-pjax="0">Согласие на обработку персональных данных</a>
    </div>
    <?php ActiveForm::end(); ?>
<?php endif;?>
