<?php

use common\models\FormSorry;
use yii\widgets\Pjax; ?>
<!-- НОВАЯ ФОРМА -->
<div class="new-form-box">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
            <div class="form-block">
                <div class="main-h3">
                    Извините, сейчас мы не можем отобразить данные по этой услуге на сайте.
                </div>
                <p class="new-form-text">Мы обязательно сделаем для вас предложение. Заполните форму ниже, и мы свяжемся с вами, как можно скорее.</p>
                <?php Pjax::begin(['enablePushState' => false]) ?>
                <?= $this->render('/forms/sorry', ['model' => new FormSorry()])?>
                <?php Pjax::end() ?>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 white-bg">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                    <div class="manager-block no-bg">
                        <img class="manager-img-new" src="<?=Yii::$app->girl->photo?>" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="callus callus-new">
                <img src="/img/icon-tel-yslygi.png" alt="" class="new-form-img-mrg">
                <div class="callus-box">
                    <p>Позвоните нам по телефону</p>
                    <?php
                    $phone = Yii::$app->city->phone;
                    ?>
                    <b><span class="hidden-lg hidden-md hidden-sm hidden-xs"><?=substr($phone, 0, 2)?></span><?=substr($phone, 2)?></b>
                </div>
                <div class="another-callus-box">
                    <p class="new-form-font">
                        <b><?=Yii::$app->girl->name?>,</b><br>
                        <i>Менеджер по работе с клиентами</i>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /НОВАЯ ФОРМА -->
