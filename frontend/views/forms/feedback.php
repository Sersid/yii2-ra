<?php

use himiklab\yii2\recaptcha\ReCaptcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var yii\web\View $this */
/* @var common\models\FormFeedback $model */
/* @var yii\widgets\ActiveForm $form */
new yii\widgets\ActiveField;
$js = <<<JS
$('#formfeedback-phone').inputmask({"mask": "+9 999 999–99–99"});
$('#feedback-form').submit(function() {
  console.log('Block');
});
JS;
$this->registerJs($js);
?>
<?php if(isset($success) && $success):?>
    <div class="alert alert-success">Ваш запрос успешно отправлен</div>
    <script type="text/javascript">
        yaCounter17709289.reachGoal('BOTTOM_FORM', function(){console.log('Отправлено')});
        gtag('event', 'forms_send', {
            'event_category': 'forms',
            'event_label': 'BOTTOM_FORM'
        });
    </script>
<?php else:?>
    <?php $form = ActiveForm::begin([
        'id' => 'feedback-form-bottom',
        'enableClientValidation'=> false,
        'action' => ['/forms/feedback'],
        'options' => ['data-pjax' => true],
        'fieldConfig' => [
            'template' => "{input}\n{error}",
            'inputOptions' => ['class' => ''],
        ],
    ]); ?>
    <div class="form-group row">
        <?= $form->field($model, 'name', [
            'options' => ['class' => 'col-lg-6 col-md-12 col-sm-12 col-xs-12']
        ])->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('name'),
        ]) ?>
        <?= $form->field($model, 'phone', [
            'options' => ['class' => 'col-lg-6 col-md-12 col-sm-12 col-xs-12']
        ])->textInput([
            'type'=> 'tel',
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel('phone'),
        ]) ?>
    </div>
    <?= $form->field($model, 'message')->textarea(['placeholder' => 'Задайте здесь свой вопрос']) ?>
    <?= $form->field($model, 'reCaptcha', ['template' => '{input}'])->widget(\himiklab\yii2\recaptcha\ReCaptcha3::class) ?>
    <div class="text-right">
        <?= Html::submitInput('Отправить', ['class' => 'yellow-btn']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?php endif;?>
