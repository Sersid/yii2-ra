<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var yii\web\View $this */
/* @var common\models\FormFeedbackModal $model */
/* @var yii\widgets\ActiveForm $form */
new yii\widgets\ActiveField;

// js
$js = <<<JS
$('#formfeedback-phone-2').inputmask({"mask": "+9 999 999–99–99"});
JS;
$this->registerJs($js);
?>
<?php if(isset($success) && $success):?>
    <div class="alert alert-success">Ваш запрос успешно отправлен</div>
    <script type="text/javascript">
        yaCounter17709289.reachGoal('WRITE_US', function(){console.log('Отправлено')});
        gtag('event', 'forms_send', {
            'event_category': 'forms',
            'event_label': 'WRITE_US'
        });
    </script>
<?php else:?>
    <?php $form = ActiveForm::begin([
        'id' => 'feedback-form-modal',
        'fieldConfig' => [
            'options' => ['class' => 'form-body-block'],
            'inputOptions' => ['class' => ''],
        ]
    ]); ?>
    <?= $form->field($model, 'name')->textInput([
        'maxlength' => true,
        'placeholder' => 'Иван',
    ]) ?>
    <?= $form->field($model, 'email')->textInput([
        'maxlength' => true,
        'placeholder' => 'ivanov@mail.ru',
        'type' => 'email',
    ]) ?>
    <?= $form->field($model, 'phone')->textInput([
        'type'=> 'tel',
        'maxlength' => true,
        'placeholder' => '+7 3462 11-22-33',
        'id' => 'formfeedback-phone-2',
    ]) ?>
    <?= $form->field($model, 'message')->textarea([
        'placeholder' => 'Задайте здесь свой вопрос',
    ]) ?>
    <?= $form->field($model, 'reCaptcha', ['template' => '{input}'])->widget(\himiklab\yii2\recaptcha\ReCaptcha3::class) ?>
    <?= Html::submitInput('Отправить', ['class' => 'yellow-btn']) ?>
    <?php ActiveForm::end(); ?>
<?php endif;?>
