<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var yii\web\View $this */
/* @var common\models\FormCallMe $model */
/* @var yii\widgets\ActiveForm $form */
$js = <<<JS
$('#formcallme-phone').inputmask({"mask": "+9 999 999–99–99"});
JS;
$this->registerJs($js);
?>
<?php if(isset($success) && $success):?>
    <div class="alert alert-success">Ваш запрос успешно отправлен</div>
    <script type="text/javascript">
        yaCounter17709289.reachGoal('CALLBACK', function() {
            console.log('CALLBACK send');
        });
        ga('send', 'event', 'Forms', 'SendForm', 'callback');
    </script>
<?php else:?>
    <?php $form = ActiveForm::begin([
        'action' => ['/forms/call-me'],
        'options' => ['data-pjax' => true],
        'fieldConfig' => [
            'options' => ['class' => 'form-body-block'],
            'inputOptions' => ['class' => ''],
        ]
    ]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Иван']) ?>
    <?= $form->field($model, 'phone')->textInput(['type'=> 'tel', 'maxlength' => true, 'placeholder' => '+7 3462 11-22-33']) ?>
    <?php /*= $form->field($model, 'email')->textInput(['maxlength' => true, 'type' => 'email']) */?>
    <?= $form->field($model, 'reCaptcha', ['template' => '{input}'])->widget(\himiklab\yii2\recaptcha\ReCaptcha3::class) ?>
    <?= Html::submitButton('Заказать звонок', ['class' => 'yellow-btn']) ?>
    <div class="politics">
        <a href="/img/docs/Soglasie_na_obrabotku_pdn_niko.pdf" target="_blank" data-pjax="0">Согласие на обработку персональных данных</a>
    </div>
    <?php ActiveForm::end(); ?>
<?php endif;?>
