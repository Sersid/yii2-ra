<?php
declare(strict_types=1);
?>
<div class="container">
    <div class="row">
        <?= $this->render('/forms/new-feedback-form', ['model' => $model, 'success' => $success]); ?>
    </div>
</div>
