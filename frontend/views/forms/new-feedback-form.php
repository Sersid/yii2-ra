<?php
declare(strict_types=1);
/** @var FormFeedback $model */

use common\models\FormFeedback;
use frontend\widgets\Html;

?>
<div class="new-form section">
    <h2>Напишите нам</h2>
    <p class="new-form__intro">
        Вы можете связаться с нами по всем интересующим вопросам, пришлите ваши контактные данные и мы вам перезвоним
    </p>
    <?php
    if (isset($success) && $success) {
        echo Html::tag('div', 'Ваш запрос успешно отправлен', ['class' => 'alert alert-success']);
    } else {
        echo Html::beginForm(['/forms/new-feedback']);
        echo Html::beginTag('div', ['class' => 'new-form__input-group']);
        echo Html::activeTextInput($model, 'name', ['placeholder' => 'Ваше имя', 'required' => 'required', 'id' => 'name']);
        echo Html::label('Ваше имя', 'name');
        echo Html::error($model, 'name', ['tag' => 'span', 'class' => 'new-form__error']);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => 'new-form__input-group']);
        echo Html::activeTextInput($model, 'surname', ['placeholder' => 'Ваша фамилия', 'required' => 'required', 'id' => 'surname']);
        echo Html::label('Ваша фамилия', 'surname');
        echo Html::error($model, 'surname', ['tag' => 'span', 'class' => 'new-form__error']);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => 'new-form__input-group']);
        echo Html::activeTextInput($model, 'email', ['placeholder' => 'Ваш E-mail', 'required' => 'required', 'id' => 'email', 'type' => 'email']);
        echo Html::label('Ваш E-mail', 'email');
        echo Html::error($model, 'email', ['tag' => 'span', 'class' => 'new-form__error']);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => 'new-form__input-group']);
        echo Html::activeTextInput($model, 'phone', ['placeholder' => 'Ваш телефон', 'required' => 'required', 'id' => 'phone']);
        echo Html::label('Ваш телефон', 'phone');
        echo Html::error($model, 'phone', ['tag' => 'span', 'class' => 'new-form__error']);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => 'new-form__input-group new-form__input-group--big']);
        echo Html::activeTextarea($model, 'message', ['placeholder' => 'Ваш вопрос', 'required' => 'required', 'id' => 'message']);
        echo Html::label('Ваш вопрос', 'message');
        echo Html::error($model, 'message', ['tag' => 'span', 'class' => 'new-form__error']);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => 'new-form__input-group new-form__input-group--big']);
        echo Html::submitButton('Отправить', ['class' => 'new-form__submit']);
        echo Html::beginTag('p', ['class' => 'new-form__policy']);
        echo Html::tag('small' , 'Нажимая на кнопку «Отправить» вы подтверждаете свое согласие на обработку персональных данных');
        echo Html::endTag('p');
        echo Html::endTag('div');

        echo Html::endForm();
    }
    ?>
</div>
