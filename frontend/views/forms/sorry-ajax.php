<?php

use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var common\models\FormSorry $model */
/* @var yii\widgets\ActiveForm $form */
new yii\widgets\ActiveField;
?>
<?php if(isset($success) && $success):?>
    <div class="alert alert-success">Ваш запрос успешно отправлен</div>
    <script type="text/javascript">
        yaCounter17709289.reachGoal('NO_SERVICE', function() {
            console.log('NO_SERVICE send');
        });
        ga('send', 'event', 'Forms', 'SendForm', 'no_service');
    </script>
<?php else:?>
    <div class="form-group row">
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 field-formsorry-phone required">
            <input type="tel" id="formsorry-phone" class="new-form-input" maxlength="255" placeholder="<?=$model->getAttributeLabel('phone')?>" aria-required="true">
            <?php
            $errors = $model->getErrors('phone');
            if(!empty($errors)):
            ?>
            <div class="help-block"><?=$errors[0]?></div>
            <?php endif;?>
        </div>
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 zindex">
            <?= Html::submitInput('Отправить', ['class' => 'yellow-btn new-btn']) ?>
        </div>
    </div>
<?php endif;?>
