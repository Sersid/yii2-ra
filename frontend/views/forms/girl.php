<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 white-bg">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
            <div class="manager-block">
                <p>
                    <b><?=Yii::$app->girl->name?>,</b>
                    <em>Менеджер по работе с клиентами</em>
                </p>
                <p>
                    Обратитесь в НИКО Медиа и вы получите полный комплекс рекламных услуг!
                </p>
                <img class="manager-img" src="<?=Yii::$app->girl->photo?>" alt="">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="callus">
                <img src="/img/icon-tel-yslygi.png" alt="">
                <div class="callus-box">
                    <p>Позвоните нам по телефону</p>
                    <?php
                    $phone = Yii::$app->city->phone;
                    ?>
                    <?php /*<b><span class="hidden-lg hidden-md hidden-sm hidden-xs"><?=substr($phone, 0, 2)?></span><?=substr($phone, 2)?></b>*/?>
                    <b><?=$phone?></b>
                </div>
            </div>
        </div>
    </div>
</div>
