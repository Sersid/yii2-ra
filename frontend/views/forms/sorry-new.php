<!-- НОВАЯ ФОРМА -->
<div class="new-form-box">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
            <div class="form-block">
                <div class="main-h3">
                    Извините, сейчас мы не можем отобразить данные по этой услуге на сайте.
                </div>
                <p class="new-form-text">Мы обязательно сделаем для вас предложение. Заполните форму ниже, и мы
                    свяжемся с вами, как можно скорее.</p>
                <div class="new-form-box-form">
                    <div class="form-group row">
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 field-formsorry-phone required">
                            <input type="tel" id="formsorry-phone" class="new-form-input" maxlength="255" placeholder="Ваш телефон (с кодом города)" aria-required="true">
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 zindex">
                            <input type="submit" class="yellow-btn new-btn" value="Отправить">
                        </div>
                        <div class="politics">
                            <a style="padding-left:15px" href="/img/docs/Soglasie_na_obrabotku_pdn_niko.pdf" target="_blank" data-pjax="0">Согласие
                                на обработку персональных данных</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 white-bg">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                    <div class="manager-block no-bg">
                        <img class="manager-img-new" src="<?= Yii::$app->girl->photo ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="callus callus-new">
                <img src="/img/icon-tel-yslygi.png" alt="" class="new-form-img-mrg">
                <div class="callus-box">
                    <p>Позвоните нам по телефону</p>
                    <b><?= Yii::$app->city->phone ?></b>
                </div>
                <div class="another-callus-box">
                    <p class="new-form-font">
                        <b><?= Yii::$app->girl->name ?>,</b><br>
                        <i>Менеджер по работе с клиентами</i>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /НОВАЯ ФОРМА -->

