<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\widgets\Text;

/* @var yii\web\View $this */
/* @var common\models\Page $model */

$this->title = Html::encode($model->name);
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/pages.css');
?>
<div class="text-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?=str_replace(['#CITY_PHONE#', '#CITY_WHERE#'],[Yii::$app->city->phone, Yii::$app->city->where], $model->content)?>
            </div>
        </div>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?=Text::get('about-form-title', 'Остались вопросы?', 'string')?></div>
                        <p><?=Text::get('about-form-feedback-text', 'Задайте вопрос через форму обратной связи.', 'string')?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()])?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl')?>
            </div>
        </div>
    </div>
</div>
