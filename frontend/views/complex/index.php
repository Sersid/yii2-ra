<?php

use yii\widgets\Pjax;
use frontend\widgets\Text;

/* @var yii\web\View $this */
/* @var common\models\Review $review */

$this->title = "Комплексная реклама";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?=Text::get('complex-page', '<p>Описание раздела</p>', 'html')?>
            </div>
        </div>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?=Text::get('about-form-title', 'Остались вопросы?', 'string')?></div>
                        <p><?=Text::get('about-form-feedback-text', 'Задайте вопрос через форму обратной связи.', 'string')?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()])?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 white-bg">
                    <div class="row">
                        <?php ob_start(); ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                            <div class="manager-block">
                                <p>
                                    <b>Петрова Мария Ивановна,</b>
                                    <em>Менеджер по работе с клиентами</em>
                                </p>
                                <p>
                                    Обратитесь в НИКО Медиа и вы получите полный комплекс рекламных услуг!
                                </p>
                                <img class="manager-img" src="/img/form-girl.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class="callus">
                                <img src="/img/icon-tel-yslygi.png" alt="">
                                <div class="callus-box">
                                    <p>Позвоните нам по телефону</p>
                                    <b><span class="hidden-lg hidden-md hidden-sm hidden-xs">+7</span>(3462) 550-877</b>
                                </div>
                            </div>
                        </div>
                        <?php
                        $text = ob_get_contents();
                        ob_end_clean();
                        ?>
                        <?=Text::get('form-description', $text, 'text')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
