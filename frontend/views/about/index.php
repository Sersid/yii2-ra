<?php

use frontend\widgets\Text;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var yii\web\View $this */
/* @var common\models\Review $review */

$this->title = "О компании";
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;
?>
<div class="text-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div>
                    <div class="dowld-box">
                        <p>
                            <img src="img/dwnld-btn.png" alt="">
                            <a href="<?= Yii::$app->city->requisitesFileUrl ?>">
                                Скачать реквизиты,<br>
                                DOC (<?= Yii::$app->city->requisitesFileSize ?>)
                            </a>
                        </p>
                    </div>
                    <p>
                        <?= Text::get(
                            'about-top-text',
                            'Агентство маркетинговых и рекламных коммуникаций "НИКО-медиа" специализируется на размещении рекламы, разработке и проведении комплексных рекламных кампаний в Тюменской области. Мы живем в Сургуте и хорошо знаем свой регион от Ишима до Нового Уренгоя. В 2002 году агентство было создано группой молодых специалистов для размещения рекламы иногородних рекламных агентств.',
                            'text'
                        ) ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg about-us-box">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 about-us-mob-left">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="direktor-block">
                                <img class="direktor-img color-img hidden-lg hidden-md hidden-xs" src="/img/direktor.png" alt="">
                                <img class="direktor-img hidden-sm hidden-xs" src="/img/direktor-cb.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 about-us-mob-right">
                    <div class="direktor-comm-right">
                        <p class="q-marks"><?= Text::get(
                                'about-q-marks',
                                'Работая с нами, наши клиенты могут заботиться только о построении собственного бизнеса, развития собственной технологии – все, что касается строительства и реконструкции – это уже наша забота и наша работа. Часть Вашего бизнес-плана, которая касается проектирования и строительства, полностью может взять на себя БИТЕКС.',
                                'text'
                            ) ?></p>
                        <p class="text-right">
                            <b><?= Text::get('about-director-name', 'Гравцев Александр Сергеевич', 'string') ?></b>
                            <span><?= Text::get('about-director-position', 'Генеральный директор', 'string') ?></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="text-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php ob_start(); ?>
                <p>Мы готовы к работе с требовательными и взыскательными клиентами, потому что хорошо знаем всех
                    собственников рекламы, а они знают нас. Мы умеем экономить время наших клиентов, оперативно
                    предоставляя индивидуальные проекты и четко работая с документами и отчетностью. Мы отвечаем за
                    качество своих размещений, проводя мониторинги и слаженно решая сложные ситуации, в которые зачастую
                    ставит нас северная природа. </p>
                <p>Мы гордимся, что свои рекламные бюджеты неоднократно нам доверяли такие компании как Сургутнефтегаз,
                    Сбербанк, Суперстрой, ЛенСпецСМУ, Надэль, СибБизнесБанк и Handy Банк, Единая Россия и автомобильные
                    бренды Suzuki, Toyota, Audi, а также многие федеральные и региональные рекламные и баинговые
                    агентства. </p>
                <?php
                $text = ob_get_contents();
                ob_end_clean();
                ?>
                <?= Text::get('about-second-text', $text, 'html') ?>
            </div>
        </div>
    </div>
</div>
<div class="about-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="main-h2 white-title"><?= Text::get(
                        'about-title-integers',
                        'О нас в цифрах',
                        'string'
                    ) ?></h2>
                <p><?= Text::get(
                        'about-title-integers-text',
                        'Агентство маркетинговых и рекламных коммуникаций "НИКО-медиа" специализируется на размещении рекламы, разработке и проведении комплексных рекламных кампаний в Тюменской области.',
                        'text'
                    ) ?></p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?= Text::get('counter-cases', '100', 'string') ?><span>+</span>
                        </div>
                        <div class="right-box">
                            <?= Text::get('title-cases', 'ПРОВЕДЕННЫХ РЕКЛАМНЫХ КаМПАНИЙ', 'string') ?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?= Url::to(['/cases']) ?>">Примеры успешных рекламных компаний в городах</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?= Text::get('counter-cities', '27', 'string') ?>
                        </div>
                        <div class="right-box">
                            <?= Text::get('title-cities', 'Городов и населенных пунктов', 'string') ?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?= Url::to(['/cities']) ?>">Сургут, Нижневартовск, Новый Уренгой,
                            Ханты-Мансийск...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?= Text::get('counter-outdoor-ads', '980', 'string') ?><span>+</span>
                        </div>
                        <div class="right-box">
                            <?= Text::get('title-outdoor-ads', 'Рекламных Поверхностей', 'string') ?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?= Url::to(['/outdoor-ad', 'view' => 'map']) ?>">Смотреть карту рекламных поверхностей
                            в городах</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?= Text::get('counter-years', '10', 'string') ?><span>+</span>
                        </div>
                        <div class="right-box">
                            <?= Text::get('title-years', 'Лет на рынке ХМАО и ЯНАО', 'string') ?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?= Url::to(['/about']) ?>">О компании</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <a class="yellow-btn" href="#" data-toggle="modal" data-target="#zakaz-zvonka">Связаться с нами</a>
            </div>
        </div>
    </div>
</div>
<div class="why-we-container">
    <div class="why-we-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <h2><?= Text::get('about-why-we', 'Почему с нами выгоднее', 'string') ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="why-we-item">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="red-title"><?= Text::get('about-why-1', 'Нет наценки!', 'string') ?></h3>
                    <p><?= Text::get(
                            'about-why-text-1',
                            'Многие московские рекламные компании работают с наценкой на стоимость заказа. Наш медиахолдинг имеет собственное производство, поэтому в стоимость рекламы не закладывается лишняя наценка за посредничество. Мы можем предложить низкие цены при самом высоком качестве работы!',
                            'text'
                        ) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="why-we-item">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="red-title"><?= Text::get(
                            'about-why-2',
                            'Высокотехнологичное оборудование',
                            'string'
                        ) ?></h3>
                    <p><?= Text::get(
                            'about-why-text-2',
                            'Рекламное агентство "Елена" обладает собственными рабочими цехами с самым современным оборудованием для полиграфии. В производственном процессе используются печатные машины RYOBI и Heidelberg). Вам не нужно тратить время на поездки - офисы наших подразделений и складские помещения находятся в шаговой доступности друг от друга.',
                            'text'
                        ) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="why-we-item">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="red-title"><?= Text::get('about-why-3', 'Надежные партнёры', 'string') ?></h3>
                    <p><?= Text::get(
                            'about-why-text-3',
                            'Мы сотрудничаем с крупными национальными компаниями, в их числе Сбербанк России, Газпром, Лукойл, Связной и другие. РА «Елена» аккредитована в холдингах "Деловой мир", "Пронто-Москва", "Экстра-М", "РДВ медиа" и других ведущих СМИ России. Наша компания успешно участвует в тендерах, проводимых московским правительством и является обладателем многих наград и номинаций.',
                            'text'
                        ) ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="why-we-item">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="red-title"><?= Text::get('about-why-4', 'Эффективные предложения', 'string') ?></h3>
                    <p><?= Text::get(
                            'about-why-text-4',
                            'Одним из структурных подразделений компании является «I-Promoter» - сертифицированный партнер Яндекс Директ и Google Adwords. Компания также является аккредитованным партнером систем управления сайтами -  1С-Битрикс, UMI CMS, HostCMS. Мы оказываем полный комплекс услуг - от разработки сайта до его продвижения в сети Интернет!',
                            'text'
                        ) ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg form-shake">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?= Text::get('about-form-title', 'Остались вопросы?', 'string') ?></div>
                        <p><?= Text::get(
                                'about-form-feedback-text',
                                'Задайте вопрос через форму обратной связи.',
                                'string'
                            ) ?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()]) ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl') ?>
            </div>
        </div>
    </div>
</div>
