<?php
declare(strict_types=1);

use frontend\widgets\Text;

?>
<section class="statistic color-white margin-b-70 padding-t-80 padding-b-80 bg-gradient-blue-image">
    <div class="container">
        <h2 class="statistic__heading">Выбирайте лучших</h2>
        <div class="statistic__layout align-center flex-justify-between">
            <ul class="statistic__left grid grid-sm-2">
                <li class="statistic__item">
                    <p class="statistic__giant bold"><?= Text::get('internet-ad-seo-years', '8 лет') ?></p>
                    <p>Опыта в создании<br> и продвижении сайтов</p>
                </li>
                <li class="statistic__item">
                    <p class="statistic__giant bold line-1_1"><?= Text::get('internet-ad-years', '10 лет') ?></p>
                    <p>Опыта в разработке<br> рекламы в интернете</p>
                </li>
                <li class="statistic__item">
                    <p class="statistic__giant bold line-1_1"><?= Text::get('internet-ad-cnt-projects', '100+') ?></p>
                    <p>Реализованных проектов<br> в интернете</p>
                </li>
                <li class="statistic__item">
                    <p class="statistic__giant bold line-1_1"><?= Text::get('internet-ad-cnt-clients', '30+') ?></p>
                    <p>Клиентов на абонентском<br> обслуживании</p>
                </li>
            </ul>
            <div class="statistic__right">
                <p>Мы не обещаем Вам золотые горы, и чудодейственные рецепты. Такими обещаниями завален весь интернет.</p>
                <p>Основываясь на своём опыте мы понимаем, что любой результат достигается только планомерной, систематической
                    работой, выстраиванием чёткой стратегии и плана, как бы это скучно не звучало. </p>
                <p>Именно поэтому мы предлагаем своим клиентам не “волшебную таблетку”, а конкретный перечень работ,
                    закреплённый в договоре, который поможет развитию вашего бизнеса. </p>
            </div>
        </div>
    </div>
</section>
