<?php
declare(strict_types=1);

/** @var FaqViewCollection $faqCollection */

use frontend\widgets\Html;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqViewCollection;

if ($faqCollection->isNotEmpty()) { ?>
  <section class="faq margin-b-80">
    <div class="container">
      <h2>Ответы на часто задаваемые вопросы</h2>
      <div class="faq__layout grid grid-sm-1 grid-row-gap-40">
          <?php foreach ($faqCollection->all() as $faqView) { ?>
            <div>
              <p class="faq__question bold relative"><?= $faqView->question() ?></p>
              <div class="faq__answer"><?= Html::autoParagraph($faqView->answer()) ?></div>
            </div>
          <?php } ?>
      </div>
    </div>
  </section>
<?php }
