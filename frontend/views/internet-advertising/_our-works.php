<?php
declare(strict_types=1);

use NikoM\Advertising\Internet\OurWork\Domain\ReadModel\OurWorkViewCollection;

/** @var OurWorkViewCollection $ourWorkCollection */

if ($ourWorkCollection->isNotEmpty()) { ?>
    <section class="results margin-b-80">
        <div class="container">
            <h2>Результаты наших работ</h2>
            <ul class="grid grid-xs-1 grid-sm-2 grid-md-3 grid-lg-4 grid-column-gap-30 grid-row-gap-30">
                <?php foreach ($ourWorkCollection->all() as $ourWorkView) { ?>
                    <li class="result__item border-light bg-white shadow-small relative">
                        <a class="result__link flex relative" href="<?= $ourWorkView->url() ?>">
                            <img class="absolute" src="<?= $ourWorkView->image()->src(265, 203) ?>" alt="<?= $ourWorkView->name() ?>">
                            <p class="bold"><?= $ourWorkView->name() ?></p>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </section>
<?php } ?>
