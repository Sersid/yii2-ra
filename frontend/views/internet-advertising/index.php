<?php
declare(strict_types=1);

use frontend\widgets\Text;
use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDtoCollection;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferViewCollection;
use frontend\widgets\Html;
use NikoM\Advertising\Internet\OurWork\Domain\ReadModel\OurWorkViewCollection;
use yii\web\View;

/** @var View $this */
/** @var OfferViewCollection $offerCollection */
/** @var FaqDtoCollection $faqCollection */
/** @var OurWorkViewCollection $ourWorkCollection */

$this->title = $offerCollection->pageHeading();
$this->params['title'] = $offerCollection->pageTitle();
$this->params['description'] = $offerCollection->pageDescription();
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('/styles/styles.css');
?>
<main class="temporary-styles">
  <div class="container">
    <div class="margin-b-70 page-intro-text">
      <p>
        Основная наша задача заключается в том, чтобы реклама в интернете приносила вам прибыль: если вы чувствуете
        финансовую отдачу от проделанной нами работы, это значит что вы продолжите с нами сотрудничество.<br> Выгодно
        вам — выгодно нам.
      </p>
      <p>Для этого мы используем весь наш опыт и компетенции, накопленные за 12 лет работы в рекламном бизнесе.</p>
    </div>
  </div>

  <?php if ($offerCollection->isNotEmpty()) { ?>
  <section class="offers margin-b-80">
    <div class="container">
      <h2>Мы предлагаем</h2>
      <ul class="grid grid-column-gap-30 grid-row-gap-30 grid-sm-2 grid-md-3 grid-lg-4">
        <?php foreach ($offerCollection->all() as $offerView) { ?>
        <li class="card card--square shadow-small border-light bg-white relative">
          <a class="card__link card__link--red flex flex-column flex-justify-content-between relative" href="<?= $offerView->url() ?>">
            <p class="card__name margin-b-0 bold line-1_2"><?= Html::fistSpaceToBr($offerView->name()) ?></p>
            <img class="card__image" src="<?= $offerView->icon()->src() ?>" alt="<?= $offerView->name() ?>"/>
          </a>
        </li>
        <?php } ?>
      </ul>
    </div>
  </section>
  <?php } ?>

  <?= $this->render('_statistic') ?>

  <?= $this->render('_our-works', ['ourWorkCollection' => $ourWorkCollection]) ?>

  <?= $this->render('_faq', ['faqCollection' => $faqCollection]) ?>

  <div class="container seo-content">
    <?= Text::get($offerCollection->seoTextCode(), '&nbsp;', 'html') ?>
  </div>
</main>
