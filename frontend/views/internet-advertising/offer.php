<?php
declare(strict_types=1);

use frontend\widgets\Html;
use frontend\widgets\Text;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferView;
use NikoM\Advertising\Internet\OurWork\Domain\ReadModel\OurWorkViewCollection;
use yii\web\View;

/** @var View $this */
/** @var OfferView $offerView */
/** @var OurWorkViewCollection $ourWorkCollection */

$this->title = $offerView->pageHeading();
$this->params['breadcrumbs'][] = ['label' => 'Интернет-реклама', 'url' => ['/internet-ad']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title'] = $offerView->pageTitle();
$this->params['description'] = $offerView->pageDescription();

$this->registerCssFile('/styles/styles.css');
?>
<main class="temporary-styles">
  <?php if ($offerView->hasIntroText()) { ?>
  <div class="container">
    <div class="margin-b-70 page-intro-text"><?= Html::autoParagraph($offerView->introText()) ?></div>
  </div>
  <?php } ?>

  <?php if ($offerView->subOfferCollection()->isNotEmpty()) { ?>
  <section class="offers padding-t-80 padding-b-80 bg-light-grey">
    <div class="container">
      <h2 class="offers__heading">Что мы предлагаем</h2>
      <ul class="grid grid-column-gap-30 grid-row-gap-30 grid-xs-1 grid-sm-2 grid-md-3 grid-lg-4 align-baseline">
        <?php foreach ($offerView->subOfferCollection()->all() as $subOffer) { ?>
        <li class="card relative">
          <img class="card__image card__image--top" src="<?= $subOffer->icon()->src() ?>" alt="<?= $subOffer->name() ?>">
          <p class="card__name card__name--small margin-b-10 bold"><?= Html::fistSpaceToBr($subOffer->name()) ?></p>
          <p class="card__text"><?= $subOffer->description() ?></p>
        </li>
        <?php } ?>
      </ul>
    </div>
  </section>
  <?php } ?>

  <section class="tariffs margin-b-80 padding-t-80 padding-b-80 color-white bg-gradient-blue">
    <div class="container">
      <h2><?= $offerView->tariff()->title() ?></h2>
      <?php if ($offerView->tariff()->hasDescription()) { ?>
      <div class="margin-b-40">
        <?= Html::autoParagraph($offerView->tariff()->description()) ?>
      </div>
      <?php } ?>
      <ul class="grid grid-xs-1 grid-sm-2 grid-md-3 grid-column-gap-30 grid-row-gap-30 line-1_4">
        <?php foreach ($offerView->tariff()->itemCollection()->all() as $tariffViewItem) { ?>
        <li class="card card--big-red-circle relative flex flex-column align-center text-center tariffs__item tariffs__item--<?= $tariffViewItem->position() ?> color-black bg-white">
          <p class="h2 margin-b-20 text-uppercase bold"><?= $tariffViewItem->name() ?></p>
          <div class="margin-b-50 card__tariff-descr">
            <p><?= Html::autoParagraph($tariffViewItem->description()) ?></p>
          </div>
          <p class="card__price">от <strong><?= Html::priceFormatted($tariffViewItem->price()) ?></strong> руб.</p>
          <button class="btn tariffs__btn" data-toggle="modal" data-target="#feedback-form">Заказать</button>
        </li>
        <?php } ?>
      </ul>
    </div>
  </section>

  <?php if ($offerView->moreOfferCollection()->isNotEmpty()) { ?>
  <section class="offers margin-b-80">
    <div class="container">
      <h2>дополнительно предлагаем</h2>
      <ul class="grid grid-column-gap-30 grid-row-gap-30 grid-sm-2 grid-md-3 grid-lg-4">
        <?php foreach ($offerView->moreOfferCollection()->all() as $moreOfferView) { ?>
        <li class="card card--rect shadow-small bg-light-grey relative">
          <div class="card__link card__link--blue flex flex-column flex-justify-content-between relative">
            <p class="card__name card__name--block flex align-center margin-b-0 bold line-1_2">
              <?= $moreOfferView->name() ?>
            </p>
            <img class="card__image" src="<?= $moreOfferView->icon()->src() ?>" alt="<?= $moreOfferView->name() ?>">
          </div>
        </li>
        <?php } ?>
      </ul>
    </div>
  </section>
  <?php } ?>

  <?= $this->render('_statistic') ?>

  <?= $this->render('_our-works', ['ourWorkCollection' => $ourWorkCollection]) ?>

  <?= $this->render('_faq', ['faqCollection' => $offerView->faqCollection()]) ?>

  <div class="container seo-content">
    <?= Text::get($offerView->seoTextCode(), '&nbsp;', 'html') ?>
  </div>
</main>
