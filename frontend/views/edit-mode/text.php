<?php
use yii\bootstrap\ActiveForm;
/* @var string $mode */
/* @var yii\web\View $this */
/* @var yii\bootstrap\ActiveForm $form */
?>
<?php $form = ActiveForm::begin(); ?>
    <div class="modal-body">
        <?php if($success):?>
            <div class="alert alert-success">
                <p>Текст успешно обновлен</p>
            </div>
        <?php endif?>
        <?php switch($mode) {
//            case 'html':
//                echo $form->field($model, 'value')->label('')->widget('vova07\imperavi\Widget', [
//                    'settings' => [
//                        'minHeight' => 150,
//                        'plugins' => [
//                            'table',
//                            'textdirection',
//                            'textexpander',
//                        ]
//                    ]
//                ]);
//                break;
            case 'string':
                echo $form->field($model, 'value')->label('')->textInput();
                break;
            default:
                echo $form->field($model, 'value')->label('')->textarea(['rows' => 15, 'class' => 'form-control']);
                break;
        }?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary">Изменить текст</button>
    </div>
<?php ActiveForm::end(); ?>
