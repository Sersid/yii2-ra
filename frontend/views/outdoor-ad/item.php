<?php
/* @var OutdoorAdSide $model */

use common\models\OutdoorAdSide;
use frontend\widgets\Html;
?>
<tr class="mesto-item-list js-items" data-url="<?= $model->getDetailPageUrl() ?>">
    <td data-label="№">
        <div class="td">
            <div class="mesto-title">
                <b class="hidden-lg hidden-md">Место №</b>
                <b><?= $model->parent->number ?> <?= $model->getSide() ?> </b>
            </div>
        </div>
    </td>
    <td data-label="Город">
        <div class="td"><?= $model->parent->city->name ?></div>
    </td>
    <td data-label="Адрес">
        <div class="td"><?= $model->getFullAddress() ?></div>
    </td>
    <td data-label="Поверхность">
        <div class="td"><?= $model->parent->getSurface(['type_display' => true]) ?></div>
    </td>
    <td data-label="Цена (руб/мес)">
        <div class="td">
            <?php if ($model->getDiscount() > 0): ?>
                <span class="old-price"><?= Html::priceFormatted($model->getPrice()) ?> руб</span><br>
                <span class="new-price"><?= Html::priceFormatted($model->getPriceWithDiscount()) ?> руб</span>
            <?php else : ?>
                <?= Html::priceFormatted($model->getPrice()) ?> руб
            <?php endif ?>
        </div>
    </td>
    <td data-label="Сторона">
        <div class="td"><?= $model->getSide(); ?></div>
    </td>
    <td data-label="Рейтинг">
        <div class="td">
            <div class="hint-item">
                <div class="rating">
                    <?php for ($i = 1; $i <= 3; $i++): ?>
                        <label <?= $model->rating >= $i ? 'class="active"' : '' ?>><i class="fa fa-star"></i></label>
                    <?php endfor; ?>
                </div>
                <div class="hint-text hidden-sm hidden-xs">
                    <div class="hint-body">
                        <p><span class="rating" style="position: absolute;left: 2px;top: 4px;">
                                            <label style="font-size: 10px;"><i class="fa fa-star"></i></label>
                                            <label style="font-size: 10px;"><i class="fa fa-star"></i></label>
                                            <label style="font-size: 10px;"><i class="fa fa-star"></i></label>
                                    </span>плохое место, размещение на нем мы не рекомендуем.</p>
                        <p class="one-star">посредственное место - бывают и лучше.</p>
                        <p class="two-stars">нормальное место, можно уверенно размещаться.</p>
                        <p class="three-stars">хорошее место, которое мы однозначно можем рекомендовать.</p>
                    </div>
                    <div class="hint-footer">
                        <b>Не забывайте, в рекламе важно не только хорошее место, но и качественный рабочий дизайн
                            рекламы</b>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td class="footer-kalk" data-label="Статус"><?= $model->getBusyStatusHTML() ?></td>
</tr>
<tr>
    <td colspan="8">
        <div class="mesto-item-body js-result"></div>
    </td>
</tr>
