<?php

/* @var yii\web\View $this */

use frontend\widgets\OutdoorAdList;
use yii\data\ActiveDataProvider;

/* @var ActiveDataProvider $dataProvider */

echo OutdoorAdList::widget(
    [
        'dataProvider' => $dataProvider,
        'layout' => "{sorter}\n{items}\n{pager}",
        'itemView' => 'item',
        'emptyTextOptions' => ['tag' => 'td', 'style' => 'padding: 20px 0'],
        'sorter' => [
            'class' => 'frontend\widgets\LinkSorter',
            'linkOptions' => ['class' => 'js-sorter'],
        ],
    ]
);
