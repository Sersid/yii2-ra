<?php

use frontend\assets;
use frontend\models\OutdoorSearch;
use yii\web\View;

/* @var View $this */
/* @var OutdoorSearch $searchModel */
/* @var array $items */

$this->title = "Наружная реклама";
$this->registerJsFile(
    Yii::$app->request->baseUrl . '/js/radius.js',
    [
        'depends' => [
            assets\YandexMapAsset::class,
        ],
    ]
);
echo $this->render('header', ['model' => $searchModel]);
?>
    <div class="container">
    <div class="row">
        <div class="transport-block margin-bottom-40">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="shadow naryw-table-margin">
                    <?= $this->render('nav-tabs', ['active' => 'radius', 'count' => 0, 'items' => $items]); ?>
                    <div class="tab-content arenda-container no-padding-bottom">
                        <style type="text/css">
                            .map-radius__added-body .selected {
                                /* Это нужно доработать и перенести в css */
                                font-weight: bold;
                                background: #93cdff;
                            }
                        </style>
                        <div id="app"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->render('footer', ['model' => $searchModel]);
