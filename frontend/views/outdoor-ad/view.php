<?php

use yii\helpers\Url;
use frontend\widgets\Html;

/* @var yii\web\View $this */
/* @var common\models\OutdoorAdSide $model */

$this->title = $model->getTitle();
$this->params['contentTitle'] = true;
$this->params['h3'] = $model->getH3();
$this->params['description'] = $model->getDescription();
$this->params['breadcrumbs'][] = ['label' => 'Наружная реклама', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['h3'];
?>
<a href="#message" id="show-message" class="fancybox"></a>
<div id="message" style="display: none"></div>
<form id="add-to-basket">
    <input type="hidden" name="id" value="<?= $model->id ?>">
    <div class="container">
        <div class="rekl-container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="shadow">
                    <div class="col-lg-7-5 col-md-7 col-sm-12 col-xs-12 rekl-item">
                        <?php
                        if (!empty($model->images)): ?>
                            <div id="rekl-shit-slider">
                                <div class="big-foto owl-carousel">
                                    <?php foreach ($model->images as $image): ?>
                                        <div class="item">
                                            <a rel="group" href="<?= $image->getSrc() ?>" class="fancybox"><img src="<?= $image->getSrc() ?>" alt=""></a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="preview-foto owl-carousel">
                                    <?php foreach ($model->images as $i => $image): ?>
                                        <div class="item">
                                            <a href="#" data-i="<?= $i ?>"><img src="<?= $image->getSrc('sm') ?>" alt=""></a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="col-lg-4-5 col-md-5 col-sm-12 col-xs-12 rekl-item">
                        <div class="rekl-box">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="rekl-box-price">
                                    <?php if ($model->getDiscount() > 0): ?>
                                        <span class="old-price"><?= Html::priceFormatted($model->getPrice()) ?> <span>руб./мес.</span></span>
                                        <div class="new-price"><?= Html::priceFormatted($model->getPriceWithDiscount()) ?>
                                            <span>руб./мес.</span></div>
                                        <p class="hint">Со скидкой <?= $model->getDiscountHTML() ?> ваша выгода
                                            составляет <?= Html::priceFormatted($model->getDiffDiscountPrice()) ?> руб./мес.</p>
                                    <?php else : ?>
                                        <?= Html::priceFormatted($model->getPrice()) ?> <span>руб./мес.</span>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12 rekl-box-left">
                                <div class="rekl-box-gray">
                                    <p><b>№<?= $model->number ?></b></p>
                                    <div class="rating">
                                        <b>Рейтинг: </b>
                                        <?php for ($i = 1; $i <= 3; $i++): ?>
                                            <label <?= $model->rating >= $i ? 'class="active"'
                                                : '' ?>><i class="fa fa-star"></i></label>
                                        <?php endfor; ?>
                                    </div>
                                    <p>
                                        <b>Адрес:</b>
                                        <?= $model->parent->city->name ?>, <?= $model->getFullAddress() ?>
                                    </p>
                                    <p>
                                        <b>Статус:</b>
                                        <?= $model->getBusyStatusHTML('span') ?>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                <?= $this->render('services', ['model' => $model]) ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center js-viewDetail">
                                <div class="row">
                                    <?= $this->render(
                                        'busy-statuses',
                                        [
                                            'model' => $model,
                                            'options' => [
                                                'boxClass' => 'col-lg-1-1 col-md-1-1 col-sm-1-1 col-xs-2-5 rekl-box-month',
                                                'showDisabledYear' => false,
                                            ],
                                        ]
                                    ) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7-5 col-md-7 col-sm-12 col-xs-12 rekl-item">
                        <?php if (!empty($model->description)): ?>
                            <div class="rekl-box rekl-descr">
                                <div class="h3 no-uppercase">Описание</div>
                                <?= $model->description ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="col-lg-4-5 col-md-5 col-sm-12 col-xs-12 rekl-item">
                        <div class="rekl-box in-basket-btn">
                            <a class="yellow-btn add-to-basket btn disabled" href="#" onclick="move_to_cart(this, false); return false;">Добавить
                                в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
if (!empty($model->parent->lat) && !empty($model->parent->lon)):
    $this->registerJsFile('https://api-maps.yandex.ru/2.1.47/?lang=ru_RU');
    ob_start();
    ?>
    ymaps.ready(function(){
        var map = new ymaps.Map("outdoor-ad-map", {
            center: [<?=$model->parent->lat?>, <?=$model->parent->lon?>],
            zoom: 15,
            controls: ['zoomControl', 'typeSelector', 'fullscreenControl']
        });
        map.behaviors.disable('scrollZoom');
        var trafficControl = new ymaps.control.TrafficControl({state: {trafficShown: true}});
        map.controls.add(trafficControl, {top: 10, left: 10});
        map.geoObjects.add(new ymaps.GeoObject({
            geometry: {
                type: "Point",
                coordinates: [<?=$model->parent->lat?>, <?=$model->parent->lon?>]
            }
            }, {
                iconLayout: 'default#image',
                iconImageHref: '/map/sprite.png',
                iconImageSize: [30, 30],
                iconOffset: [-3, 25],
                iconImageClipRect: [
                    [<?=$model->parent->rotate?> * 30 / 5, (<?=$model->parent->sides?> - 1) * 30],
                    [<?=$model->parent->rotate?> * 30 / 5 + 30, (<?=$model->parent->sides?> - 1) * 30 + 30]
                ]
            }
        ));
    });
    <?php
    $js = ob_get_contents();
    ob_end_clean();
    $this->registerJs($js);
?>
<div class="pasport-shita-map" id="outdoor-ad-map"></div>
<?= Html::tag('div', '', ['class' => 'basket-box link-basket', 'data-url' => Url::to(['/basket/widget'])])?>
<div class="modal fade" id="order" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="form-header text-center">
            <a class="form-close" data-dismiss="modal" aria-hidden="true">
                <span class="close-one"></span>
                <span class="close-two"></span>
            </a>
            <h4>Оформить <br>заказ</h4>
        </div>
        <div class="form-body">
            <?php
            $model = new common\models\QuickOrder();
            $model->model_type = 'outdoor-ad';
            echo $this->render('/basket/quick-order-widget', ['model' => $model]);
            ?>
        </div>
    </div>
</div>
<?php endif;?>
