<?php
/* @var OutdoorSearch $searchModel */
/* @var string $active */

/* @var array $items */

use frontend\models\OutdoorSearch;
use yii\helpers\Html;

?>
<ul class="nav nav-tabs">
    <li class="<?= $active == 'table' ? 'active ' : '' ?>tab-1 tab-3">
        <?= Html::a('<span>Таблицей</span>', $items['url']['table'], ['class' => 'js-tableTabLink']); ?>
    </li>
    <li class="<?= $active == 'map' ? 'active ' : '' ?>tab-2 tab-4">
        <?php
        $badge = Html::tag(
            'strong',
            $items['count']['map'],
            [
                'class' => 'js-mapCount badge',
                'style' => $items['count']['map'] > 0 ? 'display: inline-block;' : 'display: none;',
            ]
        );
        echo Html::a('<span>На карте ' . $badge . '</span>', $items['url']['map'], ['class' => 'js-mapTabLink']);
        ?>
    </li>
    <li class="<?= $active == 'radius' ? 'active ' : '' ?>tab new-tab">
        <?= Html::a('<span>По радиусу</span>', ['radius']); ?>
    </li>
</ul>
