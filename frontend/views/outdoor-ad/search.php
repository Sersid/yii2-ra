<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var yii\web\View $this */
/* @var frontend\models\OutdoorSearch $model */
/* @var yii\widgets\ActiveForm $form */
/* @var array $items */

$dropDownListTemplate = "{label}\n<div class='select'>{input}</div>\n{error}";
?>
<template id="tooltip">
    <div class="select-tooltip">
        <div class="select-tooltip__inner">
            <a href="#" class="select-tooltip__text">
                Объектов найдено
            </a>
        </div>
    </div>
</template>
<?php
$form = ActiveForm::begin(
    [
        'method' => 'get',
        'id' => 'search-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'options' => ['class' => 'js-searchForm'],
        'fieldConfig' => [
            'inputOptions' => ['class' => ''],
        ],
    ]
); ?>
<div class="arenda-block">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 arenda-box">
        <?= $form->field(
            $model,
            'city',
            [
                'options' => ['class' =>  !empty($model->city) ? ' active' : null],
                'inputOptions' => ['name' => 'city', 'class' => 'select__toggler js-city'],
                'template' => $dropDownListTemplate,
            ]
        )
            ->dropDownList($model->getCities()) ?>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 arenda-box">
        <div class="field-outdoorsearchtable-street">
            <label class="control-label" for="outdoorsearchtable-street"><?= $model->getAttributeLabel('street') ?></label>
            <div class="select">
                <select <?= empty($model->city) ? 'disabled' : ''?> id="outdoorsearchtable-street" class="select__toggler js-streets" name="street" aria-invalid="false">
                    <?php foreach ($model->getStreets() as $value => $name) {
                        echo Html::tag('option', $name, [
                                'value' => $value,
                                'disabled' => !empty($value) && !in_array($value, $items['streets']),
                        ]);
                    } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 arenda-box">
        <div class="float-box-left">
            <label for="">
                <span class="filter-box hint-item">
                    Цена (руб/мес)
                    <div class="hint-text hidden-sm hidden-xs">
                        <div class="hint-body notEvenPadding">
                            <p class="notEvenP">Цена размещения рекламы за один календарный месяц</p>
                        </div>
                    </div>
                </span>
            </label>
            <div class="hint-text hidden-sm hidden-xs">
                <div class="hint-body notEvenPadding">
                    <p class="notEvenP">Цена размещения рекламы за один календарный месяц</p>
                </div>
            </div>
            <div class="arenda-box-price">
                <div class="min-price">
                    <?= Html::activeTextInput(
                        $model,
                        'price_from',
                        [
                            'placeholder' => $items['min_price'],
                            'type' => 'number',
                            'class' => 'js-minPrice' . (!empty($model->price_from) ? ' active' : null),
                            'name' => 'price_from',
                        ]
                    ) ?>
                </div>
                <div class="tire-rasst">-</div>
                <div class="max-price">
                    <?= Html::activeTextInput(
                        $model,
                        'price_to',
                        [
                            'placeholder' => $items['max_price'],
                            'type' => 'number',
                            'class' => 'js-maxPrice' . (!empty($model->price_to) ? ' active' : null),
                            'name' => 'price_to',
                        ]
                    ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 arenda-box">
        <label class="control-label"><?= $model->getAttributeLabel('format') ?></label>
        <div class="select multi-select" data-intro="Выберите <?= strtolower($model->getAttributeLabel('format')) ?>">
            <button class="select__toggler select__toggler--multi-select" type="button"></button>
            <ul class="multi-select__dropdown">
                <?php foreach ($model->getFormats() as $value => $name) { ?>
                    <li>
                        <?= Html::checkbox(
                            'format[]',
                            in_array($value, !is_array($model->format) ? [] : $model->format),
                            [
                                'class' => 'multi-select__input js-formats',
                                'autocomplete' => 'off',
                                'id' => 'format-' . $value,
                                'value' => $value,
                                'disabled' => !in_array($value, $items['formats']),
                            ]
                        ) ?>
                        <label class="multi-select__label" for="format-<?= $value ?>"><span class="multi-select__label-checkbox"></span> <?= $name ?>
                        </label>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 arenda-box">
        <label class="control-label"><?= $model->getAttributeLabel('period') ?></label>
        <div class="select multi-select" data-intro="Выберите <?= strtolower($model->getAttributeLabel('period')) ?>">
            <button class="select__toggler select__toggler--multi-select" type="button"></button>
            <ul class="multi-select__dropdown">
                <?php foreach ($model->getPeriods() as $value => $name) { ?>
                    <li>
                        <?= Html::checkbox(
                            'period[]',
                            in_array($value, !is_array($model->period) ? [] : $model->period),
                            [
                                'class' => 'multi-select__input js-periods',
                                'autocomplete' => 'off',
                                'id' => 'period-' . $value,
                                'value' => $value,
                                'disabled' => !in_array($value, $items['periods']),
                            ]
                        ) ?>
                        <label class="multi-select__label" for="period-<?= $value ?>"><span class="multi-select__label-checkbox"></span> <?= $name ?>
                        </label>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div><?= $form->field($model, 'discount')->checkbox(['name' => 'discount']) ?></div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 arenda-box">
        <div class="arenda-box-case">
            <label class="control-label">
                <span class="filter-box hint-item">
                    <?= $model->getAttributeLabel('rating') ?>
                    <div class="hint-text hidden-sm hidden-xs">
                        <div class="hint-body">
                            <p>
                                <span class="rating">
                                    <label><i class="fa fa-star"></i></label>
                                    <label><i class="fa fa-star"></i></label>
                                    <label><i class="fa fa-star"></i></label>
                                </span>плохое место, размещение на нем мы не рекомендуем.
                            </p>
                            <p class="one-star">посредственное место - бывают и лучше.</p>
                            <p class="two-stars">нормальное место, можно уверенно размещаться.</p>
                            <p class="three-stars">хорошее место, которое мы однозначно можем рекомендовать.</p>
                        </div>
                        <div class="hint-footer">
                            <b>Не забывайте, в рекламе важно не только хорошее место, но и качественный рабочий дизайн рекламы</b>
                        </div>
                    </div>
                </span>
            </label>
            <div class="select multi-select" data-intro="Выберите <?= strtolower($model->getAttributeLabel('rating')) ?>">
                <button class="select__toggler select__toggler--multi-select" type="button"></button>
                <ul class="multi-select__dropdown">
                    <?php foreach ($model->getRatings() as $value => $name) { ?>
                        <li>
                            <?= Html::checkbox(
                                'rating[]',
                                in_array($value, !is_array($model->rating) ? [] : $model->rating),
                                [
                                    'class' => 'multi-select__input js-ratings',
                                    'autocomplete' => 'off',
                                    'id' => 'rating-' . $value,
                                    'value' => $value,
                                    'disabled' => !in_array($value, $items['ratings']),
                                ]
                            ) ?>
                            <label class="multi-select__label" for="rating-<?= $value ?>">
                                <span class="multi-select__label-checkbox"></span> <?= $name ?>
                            </label>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?php
            echo $form->field($model, 'sides', ['options' => ['class' => 'arenda-box-right']])
                ->checkboxList(
                    $model->getSides(),
                    [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            $return = Html::beginTag('div', ['class' => 'storona-a-box']);
                            $return .= Html::checkbox(
                                $name,
                                $checked,
                                ['value' => $value, 'id' => 'checkbox-' . $name . '-' . $index]
                            );
                            $return .= Html::label(Html::tag('span', $label), 'checkbox-' . $name . '-' . $index);
                            $return .= Html::endTag('div');
                            return $return;
                        },
                        'name' => 'sides[]',
                    ]
                )
                ->label(
                    '<span class="filter-box hint-item">Сторона'
                    . '<div class="hint-text hidden-sm hidden-xs"><div class="hint-body notEvenPadding">'
                    . '<p class="notEvenP">А - сторона расположенная по ходу движения с правой стороны (если не стоит '
                    . 'посреди дороги)</p><p class="notEvenP">Б - сторона расположена с левой стороны по ходу движения</p>'
                    . '</div></div></span>'
                );
            ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!---кнопка фильтров --->
<button class="filter-toggler btn">Показать фильтры</button>
