<?php

use frontend\assets\YandexMapAsset;
use frontend\models\OutdoorSearch;
use yii\helpers\Url;

/* @var yii\web\View $this */
/* @var OutdoorSearch $searchModel */
/* @var array $items */

YandexMapAsset::register($this);
echo $this->render('header', ['model' => $searchModel]);
?>
<div class="container">
    <div class="row">
        <div class="transport-block margin-bottom-40">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="shadow naryw-table-margin">
                    <?= $this->render('nav-tabs', ['active' => 'map', 'count' => 0, 'items' => $items]); ?>
                    <!-- Tab panes -->
                    <div class="tab-content arenda-container no-padding-bottom">
                        <div class="tab-pane active" id="tab-table">
                            <?= $this->render('search', ['model' => $searchModel, 'items' => $items]); ?>
                            <div class="js-ajaxLoader maps-block">
                                <div class="Preloader Preloader__center">
                                    <div id="arenda-maps"
                                         data-url="<?= Url::to(['map-ajax']) ?>"
                                         data-content-url="<?= Url::to(['map-content']) ?>"></div>
                                    <div class="Preloader__overlay">
                                        <div class="Preloader__spinnerContainer">
                                            <div class="Preloader__spinner">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->render('footer', ['model' => $searchModel]);
