<?php

use frontend\assets\YandexMapAsset;
use frontend\models\OutdoorSearch;
use frontend\widgets\OutdoorAdList;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/* @var yii\web\View $this */
/* @var OutdoorSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */
/* @var integer $count */
/* @var array $items */
YandexMapAsset::register($this);
echo $this->render('header', ['model' => $searchModel]);
?>
<div class="container" id="content">
    <div class="row">
        <div class="transport-block margin-bottom-40">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="shadow naryw-table-margin">
                    <?= $this->render('nav-tabs', ['active' => 'table', 'items' => $items]); ?>
                    <!-- Tab panes -->
                    <div class="tab-content arenda-container no-padding-bottom">
                        <div class="tab-pane active" id="tab-table">
                            <?= $this->render('search', ['model' => $searchModel, 'items' => $items]); ?>
                            <div class="js-ajaxLoader">
                                <div class="Preloader">
                                    <div class="js-outdoorAdTable"
                                         data-url="<?= Url::to(['/outdoor-ad', 'ajax' => 1]) ?>">
                                        <?= OutdoorAdList::widget(
                                            [
                                                'dataProvider' => $dataProvider,
                                                'layout' => "{sorter}\n{items}\n{pager}",
                                                'itemView' => 'item',
                                                'emptyTextOptions' => ['tag' => 'td', 'style' => 'padding: 20px 0'],
                                                'sorter' => [
                                                    'class' => 'frontend\widgets\LinkSorter',
                                                    'linkOptions' => ['class' => 'js-sorter'],
                                                ],
                                                'emptyText' => $this->render('/forms/widget'),
                                            ]
                                        ); ?>
                                    </div>
                                    <div class="Preloader__overlay">
                                        <div class="Preloader__spinnerContainer Preloader__center">
                                            <div class="Preloader__spinner"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="message" style="display: none"></div>
<?php
echo $this->render('footer', ['model' => $searchModel]);
