<?php
declare(strict_types=1);

/* @var frontend\models\OutdoorSearch $model */

use common\models\OutdoorAd;
use common\models\QuickOutdoorOrder;
use frontend\widgets\Text;
use yii\helpers\Html;

if (!empty($model->selectedCity)) {
    $this->params['breadcrumbs'][] = ['label' => 'Реклама в городах', 'url' => ['/cities']];
    $this->params['breadcrumbs'][] = [
        'label' => $model->selectedCity->name,
        'url' => ['/cities/' . $model->selectedCity->slug],
    ];
    $this->params['cityBanner'] = $model->selectedCity;

    $this->params['title'] = 'Аренда придорожных щитов 3х6 ' . $model->selectedCity->where . ' - цены на размещение';
    $this->params['description'] = 'Каталог придорожных щитов ' . $model->selectedCity->where . ' на сайте с удобным '
                                   . 'подбором по улицам, рейтингу, формату и стоимости. Подберите  свободные площади '
                                   . 'или обратитесь за помощью к нашим менеджерам.';
    $this->title = "Аренда придорожных щитов " . $model->selectedCity->where;

    if (!empty($model->selectedFormat)) {
        switch ($model->selectedFormat['id']) {
            case OutdoorAd::FORMAT_BILLBOARDS:
                $this->title = 'Арендовать рекламный щит 3x6 ' . $model->selectedCity->where;
                $this->params['title'] = 'Арендовать рекламный щит 3x6 ' . $model->selectedCity->where
                                         . ' - цены на размещение - РА «НИКО-медиа»';
                $this->params['description'] =
                    'Размещение рекламы на придорожных рекламных щитах 3x6 ' . $model->selectedCity->where
                    . '. Каталог с ценами, расположением и фотографиями. Заходите и подберите для себя оптимальный '
                    . 'вариант размещения. Бесплатные консультации, помощь в выборе.';
                break;
            case OutdoorAd::FORMAT_LED_DISPLAYS:
                $this->title = 'Арендовать светодиодный экран ' . $model->selectedCity->where;
                $this->params['title'] = 'Арендовать светодиодный экран ' . $model->selectedCity->where
                                         . ' - цены на размещение - РА «НИКО-медиа»';
                $this->params['description'] =
                    'Размещение рекламы на светодиодных рекламных экранах ' . $model->selectedCity->where
                    . '. Каталог с ценами, расположением и фотографиями. Заходите и подберите для себя оптимальный '
                    . 'вариант размещения. Бесплатные консультации, помощь в выборе.';
                break;
            case OutdoorAd::FORMAT_FIREWALLS:
                $this->title = 'Размещение рекламы на фасаде здания ' . $model->selectedCity->where;
                $this->params['title'] = 'Размещение рекламы на фасаде здания ' . $model->selectedCity->where
                                         . ' - цены на размещение - РА «НИКО-медиа»';
                $this->params['description'] = 'Реклама на фасадах жилых домов ' . $model->selectedCity->where
                                               . '. Каталог с ценами, расположением и фотографиями. Заходите и '
                                               . 'подберите для себя оптимальный вариант размещения. Бесплатные '
                                               . 'консультации, помощь в выборе.';
                break;
            case OutdoorAd::FORMAT_ADV_FENCES:
                $this->title = 'Арендовать рекламные ограждения ' . $model->selectedCity->where;
                $this->params['title'] = 'Арендовать рекламные ограждения ' . $model->selectedCity->where
                                         . ' - цены на размещение - РА «НИКО-медиа»';
                $this->params['description'] =
                    'Размещение наружной рекламы на дорожных ограждениях ' . $model->selectedCity->where
                    . '. Каталог с ценами, расположением и фотографиями. Заходите и подберите для себя оптимальный '
                    . 'вариант размещения. Бесплатные консультации, помощь в выборе.';
                break;
        }
    }
} else {
    $this->params['title'] = 'Аренда придорожных щитов 3х6 - цены на размещение';
    $this->params['description'] = 'Каталог придорожных щитов на сайте с удобным подбором по улицам, рейтингу, '
                                   . 'формату и стоимости. Подберите  свободные площади или обратитесь за помощью к нашим менеджерам.';
    $this->title = "Аренда придорожных щитов";
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="someLights js-setFormat">
            <div class="col-lg-2-5 col-md-2-5 col-sm-6 col-xs-6">
                <img src="/img/shit2.png" alt="Рекламные щиты">
                <h3>Рекламные щиты</h3>
                <?= Html::a(
                    'Заказать',
                    '#',
                    [
                        'class' => 'yellow-btn add-to-basket',
                        'data-toggle' => "modal",
                        'data-target' => "#order-format",
                        'data-format' => 'Рекламные щиты',
                    ]
                ) ?>
            </div>
            <div class="col-lg-2-5 col-md-2-5 col-sm-6 col-xs-6">
                <img src="/img/shit3.png" alt="Брандмауэры">
                <h3>Брандмауэры</h3>
                <?= Html::a(
                    'Заказать',
                    '#',
                    [
                        'class' => 'yellow-btn add-to-basket',
                        'data-toggle' => "modal",
                        'data-target' => "#order-format",
                        'data-format' => 'Брандмауэры',
                    ]
                ) ?>
            </div>
            <div class="col-lg-2-5 col-md-2-5 col-sm-6 col-xs-6">
                <img src="/img/shit.png" alt="Светодиодные экраны">
                <h3>Светодиодные экраны</h3>
                <?= Html::a(
                    'Заказать',
                    '#',
                    [
                        'class' => 'yellow-btn add-to-basket',
                        'data-toggle' => "modal",
                        'data-target' => "#order-format",
                        'data-format' => 'Светодиодные экраны',
                    ]
                ) ?>
            </div>
            <div class="col-lg-2-5 col-md-2-5 col-sm-6 col-xs-6">
                <img src="/img/shit4.png" alt="Рекламные ограждения">
                <h3>Рекламные ограждения</h3>
                <?= Html::a(
                    'Заказать',
                    '#',
                    [
                        'class' => 'yellow-btn add-to-basket',
                        'data-toggle' => "modal",
                        'data-target' => "#order-format",
                        'data-format' => 'Рекламные ограждения',
                    ]
                ) ?>
            </div>
            <div class="col-lg-2-5 col-md-2-5 col-sm-6 col-xs-6">
                <img src="/img/shit5.png" alt="Сити-формат">
                <h3>Сити-формат</h3>
                <?= Html::a(
                    'Заказать',
                    '#',
                    [
                        'class' => 'yellow-btn add-to-basket',
                        'data-toggle' => "modal",
                        'data-target' => "#order-format",
                        'data-format' => 'Сити-формат',
                    ]
                ) ?>
            </div>
        </div>
        <div class="modal fade" id="order-format" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="form-header text-center">
                    <a class="form-close" data-dismiss="modal" aria-hidden="true">
                        <span class="close-one"></span>
                        <span class="close-two"></span>
                    </a>
                    <h4>Оформить <br>заказ</h4>
                </div>
                <div class="form-body">
                    <?php
                    $order = new QuickOutdoorOrder();
                    $order->model_type = '';
                    ?>
                    <?= $this->render('/basket/quick-order-widget', ['model' => $order, 'outdoor' => true]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="hidden-xs">
                <?php if (empty($model->selectedCity)): ?>
                    <p><?= Text::get(
                            'outdoor-ad-main-text',
                            'Наружная реклама – неотъемлемый и зачастую важнейший элемент любой полноценной '
                            . 'рекламной кампании. Основные виды носителей - щиты, сити-форматы, брандмауэры, '
                            . 'панель-кронштейны, призматроны, видеоэкраны и др. При размещении рекламы на щитах важно '
                            . 'подобрать действительно нужные и арендовать их на оптимальный срок, чтобы рационально '
                            . 'использовать бюджет и максимально привлечь внимание целевой аудитории.',
                            'text'
                        ) ?></p>
                <?php else: ?>
                    <?= $model->selectedCity->content_outdoor_ad ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
