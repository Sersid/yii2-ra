<?php

use common\models\OutdoorAd;
use yii\helpers\Html;
use frontend\widgets\Text;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var frontend\models\OutdoorSearch $model */
?>
<div class="container seo-content" style="margin-bottom: 50px">
    <div class="row">
        <?php if (!empty($model->selectedCity)): ?>
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                <?php if (empty($model->arSelectedFormat)): ?>
                    <?= Text::get('outdoor-ad-city-' . $model->selectedCity->slug) ?>
                    <p>
                        Рекламные поверхности <?= $model->selectedCity->where ?>:
                        <?= Html::a(
                            'рекламные щиты',
                            ['/outdoor-ad/reklamniy-shit-3x6-' . $model->selectedCity->slug]
                        ); ?>
                        <?= Html::a(
                            'брандмауэры',
                            ['/outdoor-ad/brandmauer-' . $model->selectedCity->slug]
                        ); ?>
                        <?= Html::a(
                            'светодиодные экраны',
                            ['/outdoor-ad/svetodiodniy-ekran-' . $model->selectedCity->slug]
                        ); ?>
                        <?= Html::a(
                            'рекламные ограждения',
                            ['/outdoor-ad/reklamnoe-ograjdenie-' . $model->selectedCity->slug]
                        ); ?>
                    </p>
                <?php else: ?>
                    <?php
                    $default = '';
                    if (empty($text)) {
                        switch ($model->arSelectedFormat['id']) {
                            case OutdoorAd::FORMAT_BILLBOARDS:
                                $default = 'Размещение рекламы на придорожных рекламных щитах 3x6 '
                                           . $model->selectedCity->where
                                           . '. Каталог с ценами, расположением и фотографиями. Подберите для себя оптимальный вариант размещения. Если у вас появились вопросы, позвоните или напишите нам, проконсультируем по всем интересующим вас вопросам и поможем с выбором.';
                                break;
                            case OutdoorAd::FORMAT_LED_DISPLAYS:
                                $default = 'Размещение рекламы на светодиодных рекламных экранах '
                                           . $model->selectedCity->where
                                           . '. Каталог с ценами, расположением и фотографиями. Подберите для себя оптимальный вариант размещения.Если у вас появились вопросы, позвоните или напишите нам, проконсультируем по всем интересующим вас вопросам и поможем с выбором.';
                                break;
                            case OutdoorAd::FORMAT_FIREWALLS:
                                $default = 'Реклама на фасадах жилых домов ' . $model->selectedCity->where
                                           . '. Каталог с ценами, расположением и фотографиями. Подберите для себя оптимальный вариант размещения. Если у вас появились вопросы, позвоните или напишите нам, проконсультируем по всем интересующим вас вопросам и поможем с выбором.';
                                break;
                            case OutdoorAd::FORMAT_ADV_FENCES:
                                $default =
                                    'Размещение наружной рекламы на дорожных ограждениях ' . $model->selectedCity->where
                                    . '. Каталог с ценами, расположением и фотографиями. Подберите для себя оптимальный вариант размещения. Если у вас появились вопросы, позвоните или напишите нам, проконсультируем по всем интересующим вас вопросам и поможем с выбором.';
                                break;
                        }
                    }
                    echo '<p>' . Text::get(
                            'outdoor-ad-city-' . $model->selectedCity->slug . '-format-'
                            . $model->arSelectedFormat['id'],
                            $default
                        ) . '</p>';
                    ?>
                <?php endif ?>
            </div>
        <?php else: ?>
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                <?= Text::get('outdoor-ad-after-table'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg form-shake">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?= Text::get('outdoor-form-title', 'Остались вопросы?', 'string') ?></div>
                        <p><?= Text::get(
                                'outdoor-form-feedback-text',
                                'Давайте обсудим Ваши задачи. И сделаем их',
                                'string'
                            ) ?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()]) ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl') ?>
            </div>
        </div>
    </div>
</div>
<?= Html::tag('div', '', ['class' => 'basket-box link-basket', 'data-url' => Url::to(['/basket/widget'])]) ?>
<div class="modal fade" id="order" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="form-header text-center">
            <a class="form-close" data-dismiss="modal" aria-hidden="true">
                <span class="close-one"></span>
                <span class="close-two"></span>
            </a>
            <h4>Оформить <br>заказ</h4>
        </div>
        <div class="form-body">
            <?php
            $order = new common\models\QuickOrder();
            $order->model_type = 'outdoor-ad';
            ?>
            <?= $this->render('/basket/quick-order-widget', ['model' => $order]) ?>
        </div>
    </div>
</div>
