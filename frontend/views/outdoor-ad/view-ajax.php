<?php

use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideBusy;
use frontend\widgets\Html;
use yii\web\View;

/* @var OutdoorAdSide $model */
/* @var View $this */

/* @var array $busyStatuses */
$arDiscounts = [];
$arBusyStatuses = $model->getBusyStatuses();
foreach ($arBusyStatuses as $num => $arDate) {
    if ($arDate['disabled']) {
        break;
    }
    foreach ($arDate['months'] as $arMonth) {
        if (!empty($arMonth['discount']) && $arMonth['status'] != OutdoorAdSideBusy::STATUS_BUSY) {
            $arDiscounts[] = $arMonth['discount'] . '% скидка на ' . mb_strtolower($arMonth['name']);
        }
    }
}

$busyStatuses = $model->getBusyStatuses();
?>
<form class="js-outdoor-form">
    <input type="hidden" name="id" value="<?= $model->id ?>">
    <div class="mesto-descr">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rekl-box-case">
            <?= $this->render(
                'busy-statuses',
                [
                    'model' => $model,
                    'options' => [
                        'boxClass' => 'col-lg-1-1 col-md-1-1 col-sm-1-1 col-xs-2-5 rekl-box-month',
                        'showDisabledYear' => true,
                    ],
                ]
            ) ?>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rekl-box-case">
            <div class="rekl-box-serv-item">
                <p class="rekl-box-serv-item__title">Дополнительные услуги:</p>
                <div class="serv-item-checkbox">
                    <input type="checkbox" checked disabled data-price="<?= $model->price_installation ?>" name="install" id="i<?= $model->id ?>" value="1">
                    <label for="i<?= $model->id ?>">
                        <span>Монтаж в месяц размещения </span><span><?= $model->getPriceInstallation() ?> руб./шт.</span>
                    </label>
                    <input type="hidden" data-price="<?= $model->price_installation ?>" name="install" value="1">
                </div>
                <div class="serv-item-checkbox">
                    <?php
                    echo Html::checkbox(
                        'print',
                        $model->basket->print,
                        [
                            'id' => 'p' . $model->id,
                            'value' => 1,
                            'data-price' => $model->price_print,
                        ]
                    );
                    echo Html::label('<span>Печать на баннере</span><span>' . $model->getPricePrint() . ' руб./шт.<span>', 'p' . $model->id);
                    ?>
                </div>
                <div class="serv-item-checkbox">
                    <?php
                    echo Html::checkbox(
                        'design',
                        $model->basket->design,
                        [
                            'id' => 'd' . $model->id,
                            'value' => 1,
                            'data-price' => $model->price_design,
                        ]
                    );
                    echo Html::label('<span>Дизайн</span><span>Дизайн - от ' . $model->getPriceDesign() . ' руб.</span', 'd' . $model->id);
                    ?>
                </div>
                <!---цена со скидкой --->
                <div class="serv-item-checkbox total js-total">
                    <div class="total__inner">
                        <div class="total__grid js-total-price-box">
                            <p class="total__old-price js-total-old-price"></p>
                            <span class="span-total js-total-price">0 руб.</span>
                            <span class="serv-name-box js-total-services"></span>
                            <p class="total__label js-total-eco"></p>
                        </div>
                        <div class="more-inform-btn">
                            <a class="buy-now btn disabled" href="#">Оформить</a>
                        </div>
                        <div class="in-basket-btn">
                            <a class="yellow-btn add-to-basket btn disabled" href="#" onclick="move_to_cart(this, false); return false;">Добавить в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php if (!empty($model->images)): ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-right-10">
                    <div <?= count($model->images) > 1 ? 'class="js-carousel"' : '' ?>>
                        <?php foreach ($model->images as $image): ?>
                            <div class="item"><a
                                        rel="group-<?= $model->id ?>"
                                        href="<?= $image->getSrc() ?>"
                                        class="js-fancybox mesto-img"
                                        style="background: url(<?= $image->getSrc(
                                            'sm'
                                        ) ?>) no-repeat; background-size:cover; background-position: center center;"></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ?>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-left-10">
                <?php
                if (!empty($model->parent->lat) && !empty($model->parent->lon)) {
                    echo Html::tag(
                        'div',
                        '',
                        [
                            'class' => 'mesto-maps js-map',
                            'id' => 'outdoor-ad-map-' . $model->id,
                            'data' => [
                                'lat' => $model->parent->lat,
                                'lon' => $model->parent->lon,
                                'sides' => $model->parent->sides,
                                'rotate' => $model->parent->rotate,
                            ],
                        ]
                    );
                }
                ?>
            </div>
        </div>
    </div>
</form>
