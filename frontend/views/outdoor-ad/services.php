<?php

use yii\helpers\Html;
use common\models\OutdoorAdSideBusy;

/* @var common\models\OutdoorAdSide $model */
/* @var yii\web\View $this */

/* @var array $busyStatuses */
$arDiscounts = [];
$arBusyStatuses = $model->getBusyStatuses();
foreach ($arBusyStatuses as $num => $arDate) {
    if ($arDate['disabled']) {
        break;
    }
    foreach ($arDate['months'] as $arMonth) {
        if (!empty($arMonth['discount']) && $arMonth['status'] != OutdoorAdSideBusy::STATUS_BUSY) {
            $arDiscounts[] = $arMonth['discount'] . '% скидка на ' . mb_strtolower($arMonth['name']);
        }
    }
}
?>
<div class="rekl-box-serv-item">
    <div class="serv-item-checkbox">
        <input type="checkbox" checked disabled data-price="<?= $model->price_installation ?>" name="install" id="i<?= $model->id ?>" value="1">
        <label for="i<?= $model->id ?>">
            Монтаж в месяц размещения - <b><?= $model->getPriceInstallation() ?> руб./шт.</b>
        </label>
        <input type="hidden" data-price="<?= $model->price_installation ?>" name="install" value="1">
    </div>
    <div class="serv-item-checkbox">
        <?php
        echo Html::checkbox(
            'print',
            $model->basket->print,
            [
                'id' => 'p' . $model->id,
                'value' => 1,
                'data-price' => $model->price_print,
            ]
        );
        echo Html::label('Печать на баннере - ' . $model->getPricePrint() . ' руб./шт.', 'p' . $model->id);
        ?>
    </div>
    <div class="serv-item-checkbox">
        <?php
        echo Html::checkbox(
            'design',
            $model->basket->design,
            [
                'id' => 'd' . $model->id,
                'value' => 1,
                'data-price' => $model->price_design,
            ]
        );
        echo Html::label('Дизайн - от ' . $model->getPriceDesign() . ' руб.', 'd' . $model->id);
        ?>
    </div>
    <div class="serv-item-checkbox total js-total">
        <span class="serv-name-box"></span>:
        <b><span class="span-total">0 руб.</span></b>
        <span class="span-total-with-discount"></span>
    </div>
    <div class="hidden-hint-text">Стоимость: выберите период размещения ниже
        <?php
        if (!empty($arDiscounts)) {
            echo '. <span class="discontMessage">Действует ' . implode(', ', $arDiscounts) . '</span>';
        }
        ?>
    </div>
</div>
