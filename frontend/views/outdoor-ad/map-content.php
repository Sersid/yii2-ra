<?php
/* @var yii\web\View $this */
/* @var OutdoorAd $model */

use common\models\OutdoorAd;
use \frontend\widgets\Html;
?>
<div class="maps-hint-block js-content-<?= $model->id ?>">
    <div class="h4"><?= $model->getSurface(['type_display' => true]) ?></div>
    <?php if (count($model->outdoorAdSides) > 1) { ?>
        <ul class="maps-hint-tabs">
            <?php foreach ($model->outdoorAdSides as $i => $side) { ?>
                <li <?= ($i == 0 ? 'class="active"' : '') ?>>
                    <a href="#side-<?= $side->id ?>" data-toggle="tab">Сторона <?= $side->getSide() ?></a>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>
    <div class="tab-content">
        <?php foreach ($model->outdoorAdSides as $i => $side) { ?>
            <div class="tab-pane<?= $i == 0 ? ' active' : '' ?>" id="side-<?= $side->id ?>">
                <div class="maps-hint-box">
                    <div class="col-md-5 col-lg-5 col-sm-5 no-padding">
                        <div class="maps-hint-left">
                            <div class="maps-hint-box-img">
                                <?php if (!empty($side->images)) { ?>
                                    <div style="background: url(<?= $side->images[0]->getSrc('sm')?>) no-repeat; background-size: cover; background-position: center;"></div>
                                <?php } ?>
                            </div>
                            <div class="maps-hint-box-price">
                                <b>Стоимость:</b>
                                <div>
                                    <?php if ($side->getDiscount() > 0) { ?>
                                        <span class="old-price"><?= Html::priceFormatted($side->getPrice()) ?> <span>руб/мес</span></span>
                                        <br/>
                                    <?php } ?>
                                    <?= Html::priceFormatted($side->getPriceWithDiscount()) ?> <span>руб/мес</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-7 col-sm-7 no-padding">
                        <div class="maps-hint-right">
                            <p><b>№<?= $side->number ?></b></p>
                            <div class="rating">
                                <b>Рейтинг: </b>
                                <?php for ($i = 1; $i <= 3; $i++) { ?>
                                    <label <?= $side->rating >= $i ? 'class="active"' : '' ?>'>
                                    <i class="fa fa-star"></i></label>
                                <?php } ?>
                            </div>
                            <p><b>Адрес:</b> <?= $model->city->name . ', ' . $side->getFullAddress() ?></p>
                            <p><b>Статус:</b> <?= $side->getBusyStatusHTML('span') ?></p>
                        </div>
                        <div class="maps-hint-footer-link">
                            <div class="col-lg-6 col-md-6 col-sm-6 no-padding">
                                <a target="_blank" class="more-link" href="<?= $side->getDetailPageUrl() ?>">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
