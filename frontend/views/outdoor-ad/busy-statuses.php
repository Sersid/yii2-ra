<?php
/* @var common\models\OutdoorAdSide $model */
/* @var yii\web\View $this */

/* @var array $busyStatuses */

/* @var array $options */

use common\models\OutdoorAdSideBusy;
use yii\helpers\Html;

$arBusyStatuses = $model->getBusyStatuses();
$prevYear = '';
$isFirstChecked = false;
foreach ($arBusyStatuses as $num => $arDate) {
    if (!$options['showDisabledYear'] && $arDate['disabled']) {
        continue;
    }
    $cssClass = 'rekl-box-calendar text-center ';
    $cssClass .= $arDate['disabled'] ? 'no-active-calendar' : 'active-calendar';
    echo Html::beginTag('div', ['class' => $cssClass]);
    echo Html::beginTag('div', ['class' => 'col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding']);
    echo Html::tag('div', $prevYear == $arDate['year'] ? '&nbsp;' : $arDate['year'], ['class' => 'rekl-box-year']);
    foreach ($arDate['months'] as $month) {
        echo Html::beginTag('div', ['class' => $options['boxClass']]);
        echo Html::tag('p', $month['name']);
        if ($month['disabled']) {
            echo Html::label('', '', ['class' => 'disabled-month']);
        } else {
            echo Html::beginTag('div', ['class' => 'month-checkbox']);
            $id = 'm' . $model->id . '-' . $arDate['year'] . '-' . $month['num'];
            if ($month['status'] != OutdoorAdSideBusy::STATUS_BUSY) {
                $checked = false;
                if (in_array($month['num'] . '.' . $arDate['year'], $model->basket->getDates()) ||
                    (empty($model->basket->dates) && !$isFirstChecked)) {
                    $checked = true;
                    $isFirstChecked = true;
                }
                echo Html::checkbox(
                    'mounts[]',
                    $checked,
                    [
                        'data' => [
                            'price' => $model->price,
                            'discount' => $month['discount'],
                        ],
                        'value' => $month['num'] . '.' . $arDate['year'],
                        'id' => $id,
                    ]
                );
            } else {
                echo Html::checkbox('', false, ['disabled' => 'disabled', 'id' => $id]);
            }
            switch ($month['status']) {
                case OutdoorAdSideBusy::STATUS_RESERVE:
                    echo Html::label(
                        '<div class="hint-text hidden-sm hidden-xs"><div class="hint-body notEvenPadding">'
                        . '<p class="notEvenP">Данная поверхность уже находится на рассмотрении у нашего Клиента, поэтому '
                        . 'не гарантируем свободность</p></div></div>',
                        $id,
                        ['class' => 'hint-item reserve']
                    );
                    break;
                case OutdoorAdSideBusy::STATUS_REQUEST:
                    echo Html::label(
                        '<div class="hint-text hidden-sm hidden-xs"><div class="hint-body notEvenPadding">'
                        . '<p class="notEvenP">Статус свободности этой стороны необходимо уточнить у менеджер</p></div></div>',
                        $id,
                        ['class' => 'hint-item request']
                    );
                    break;
                default:
                    echo Html::label('', $id);
                    break;
            }
            echo Html::endTag('div');
        }
        echo Html::endTag('div');
    }
    echo Html::endTag('div');
    echo Html::endTag('div');
    $prevYear = $arDate['year'];
}
