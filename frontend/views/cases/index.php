<?php
use yii\widgets\ListView;

/* @var yii\web\View $this */
/* @var common\models\Review $review */

$this->title = "Рекламные кейсы";
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;
?>
<div class="content-body gray-bg">
    <div class="container">
        <div class="row">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'layout' => "{items}\n<div class=\"col-lg-12 col-md-12 hidden-sm hidden-xs\">{pager}</div>",
                'emptyTextOptions' => ['class' => 'col-md-12'],
                'options' => ['class' => 'content-wrap'],
                'pager' => [
                    'options' => ['class' => 'pagination-block'],
                    'nextPageLabel' => false,
                    'prevPageLabel' => false,
                    'disabledPageCssClass' => 'no-active',
                    'maxButtonCount' => 5
                ],
            ]); ?>
        </div>
    </div>
</div>
