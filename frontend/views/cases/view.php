<?php

use frontend\models\AdvCase;
use yii\widgets\Pjax;
use frontend\widgets\Text;

/* @var yii\web\View $this */
/* @var AdvCase $model */
/* @var AdvCase[] $cases */

$this->title = "Рекламные кейсы";
$this->registerMetaTag(['name' => 'description', 'content' => $model->short_desc], 'description');
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;
?>
<?= $this->render($model->isHtml() ? '_type_html' : '_type_default', ['model' => $model, 'cases' => $cases]) ?>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?=Text::get('cases-form-title', 'Хотите так же?', 'string')?></div>
                        <p><?=Text::get('cases-form-description', 'Давайте обсудим Ваши задачи. И сделаем их', 'string')?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()])?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl')?>
            </div>
        </div>
    </div>
</div>
