<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\widgets\Text;

/* @var yii\web\View $this */
/* @var common\models\AdvCase $model */
$this->registerCssFile('/css/cases.css');

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/gsap/3.3.4/gsap.min.js');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/gsap/3.3.4/ScrollTrigger.min.js');
$this->registerJsFile('/js/cases.js');
?>
<section class="new-service">
    <div class="container">
        <?= $model->html; ?>
    </div>
</section>
