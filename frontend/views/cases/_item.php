<?php

use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var frontend\models\AdvCase $model */
?>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center content-box">
    <a href="<?= $model->getDetailPageUrl() ?>">
        <div class="content-item-box">
            <?php if (!empty($model->image)): ?>
                <div class="item-box-img" style="background: url(<?= $model->image->getSrc(325) ?>) no-repeat; background-position: center center; background-size: cover;"></div>
            <?php endif ?>
            <h2 class="no-uppercase"><?= Html::encode($model->name) ?></h2>
            <div>
                <p><?= $model->short_desc ?></p>
            </div>
            <div class="content-reed-more">
                <div class="yellow-btn" href="<?= $model->getDetailPageUrl() ?>">Подробнее</div>
            </div>
        </div>
    </a>
</div>
