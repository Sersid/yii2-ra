<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\widgets\Text;

/* @var yii\web\View $this */
/* @var common\models\AdvCase $model */
?>
<div class="content-body no-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="kanal-block">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 obyavlenie-block">
                            <div class="gray-bg obyavlenie-box">
                                <h2><?= Html::encode($model->name)?></h2>
                                <div>
                                    <?php if(!empty($model->product)):?>
                                    <p><b>Продукт:</b> <?=$model->product?></p>
                                    <?php endif;?>
                                    <?php if(!empty($model->customer)):?>
                                        <p><b>Портрет клиента:</b> <?=$model->customer?></p>
                                    <?php endif;?>
                                    <?php if(!empty($model->city)):?>
                                        <p><b>Город:</b> <?=$model->city->name?></p>
                                    <?php endif;?>
                                    <?php if(!empty($model->purpose)):?>
                                        <p><b>Цель:</b> <?=$model->purpose?></p>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 kalk-table-list case-view">
                            <div class="h3 no-uppercase">Описание</div>
                            <?=str_replace([
                                '½',
                                '¼',
                                '<table',
                                '</table>',
                            ], [
                                '<span style="font-family: Arial">½</span>',
                                '<span style="font-family: Arial">¼</span>',
                                '<div class="case-table"><table',
                                '</table></div>',
                            ], empty($model->full_desc) ? $model->short_desc : $model->full_desc)?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if(!empty($cases)):?>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                <div class="basket-block rightbar">
                    <div class="basket-block-header">
                        Другие кейсы:
                    </div>
                    <ul class="basket-block-body">
                        <?php foreach ($cases as $case):?>
                        <li><?= Html::a(Html::encode($case->name), ['view', 'item' => $case->slug])?></li>
                        <?php endforeach;?>
                    </ul>
                    <div class="basket-block-footer">
                        <?= Html::a('Все кейсы', ['/cases'], ['class' => 'yellow-btn'])?>
                    </div>
                </div>
            </div>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?=Text::get('cases-form-title', 'Хотите так же?', 'string')?></div>
                        <p><?=Text::get('cases-form-description', 'Давайте обсудим Ваши задачи. И сделаем их', 'string')?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()])?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl')?>
            </div>
        </div>
    </div>
</div>
