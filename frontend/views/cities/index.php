<?php

use frontend\widgets\Text;
use frontend\models\City;
use yii\helpers\Html;

/* @var array $cnt */
/** @var City $city */

$this->title = "Реклама в городах";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gray-bg town-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>Самые крупные города региона</h2>
            </div>
            <div>
            <?php foreach ($cities['large'] as $city): ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 town-box">
                    <div class="town-item">
                        <?php if (!empty($city->image)): ?>
                            <div class="town-item-left-box">
                                <div class="town-item-img">
                                    <img src="<?= $city->image->getSrc(null, 105, 3) ?>" alt="<?= $city->name ?>">
                                </div>
                            </div>
                        <?php endif ?>
                        <div class="town-item-right-box">
                            <h3><?= Html::a($city->name, $city->getDetailPageUrl()) ?></h3>
                            <div>
                                <?php if (!empty($city->salary)): ?>
                                    <p><b>Средняя зарплата:</b>
                                        <?= $city->getSalaryFormatted() ?> руб.</p>
                                <?php endif; ?>
                                <?php if (!empty($city->population)): ?>
                                    <p><b>Численность населения:</b>
                                        <?= $city->getPopulationFormatted() ?> тыс. чел.</p>
                                <?php endif; ?>
                                <?php if (!empty($city->density)): ?>
                                    <p><b>Плотность населения:</b>
                                        <?= $city->getDensityFormatted() ?>чел./км<sup>2</sup></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <!--новая верстка "реклама в городах"-->
                        <div class="statisticBox">
                            <ul>
                                <li><a href="<?= $city->getOutdoorAdPageUrl() ?>"><?php
                                        echo isset($cnt['outdoor_ads'][$city->id]) ? $cnt['outdoor_ads'][$city->id] : 0;
                                        ?></a>
                                    <p>поверхностей</p></li>
                                <li><a href="<?= $city->getTvPageUrl() ?>"><?php
                                        echo isset($cnt['tvs'][$city->id]) ? $cnt['tvs'][$city->id] : 0;
                                        ?></a>
                                    <p>телеканалов</p></li>
                                <li><a href="<?= $city->getRadioPageUrl() ?>"><?php
                                        echo isset($cnt['radio'][$city->id]) ? $cnt['radio'][$city->id] : 0;
                                        ?></a>
                                    <p>радиостанций</p></li>
                                <li><a href="<?= $city->getTickerPageUrl() ?>"><?php
                                        echo isset($cnt['tickers'][$city->id]) ? $cnt['tickers'][$city->id] : 0;
                                        ?></a>
                                    <p>телеканалов<br>(бегущая строка)</p></li>
                            </ul>
                        </div>
                        <!--новая верстка "реклама в городах"-->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zakaz-town text-center">
                <a class="yellow-btn" data-toggle="modal" data-target="#feedback-form" href="#">Заказать подбор городов
                    и рекламных мест</a>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 kalk-table-list ">
                <div class="kalk-block table-town">
                    <table>
                        <thead>
                        <tr class="kalk-item">
                            <th class="header-kalk">Город</th>
                            <th>Факты</th>
                            <th class="footer-kalk">Основные виды деятельности</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $lastRegion = null; ?>
                        <?php foreach ($cities['small'] as $city): ?>
                            <?php if ($city->region->name != $lastRegion): ?>
                                <!--новая верстка "реклама в городах" БЛОК ДЛЯ ДЕЛЕНИЯ ПО РЕГИОНАМ-->
                                <tr class="kalk-item">
                                    <td class="maxWidth">
                                        <div class="td"><?= $city->region->name ?></div>
                                    </td>
                                </tr>
                                <?php $lastRegion = $city->region->name; ?>
                                <!--новая верстка "реклама в городах"-->
                            <?php endif ?>
                            <tr class="kalk-item">
                                <td class="header-kalk" data-label="Город">
                                    <div class="td"><?= Html::a($city->name, [$city->slug]) ?></div>
                                </td>
                                <td data-label="Факты">
                                    <div class="td">
                                        <?php if (!empty($city->salary)): ?>
                                            <p><b>Средняя зарплата:</b>
                                                <?= $city->getSalaryFormatted() ?> руб.</p>
                                        <?php endif; ?>
                                        <?php if (!empty($city->population)): ?>
                                            <p><b>Численность населения:</b>
                                                <?= $city->getPopulationFormatted() ?> тыс. чел.</p>
                                        <?php endif; ?>
                                        <?php if (!empty($city->density)): ?>
                                            <p><b>Плотность населения:</b>
                                                <?= $city->getDensityFormatted() ?>чел./км<sup>2</sup></p>
                                        <?php endif; ?>
                                        <!--новая верстка "реклама в городах" БЛОК СТАТИСТИКИ-->
                                        <div class="statisticBox">
                                            <ul class="noMrgTable">
                                                <li><a href="<?= $city->getOutdoorAdPageUrl() ?>"><?php
                                                        echo isset($cnt['outdoor_ads'][$city->id])
                                                            ? $cnt['outdoor_ads'][$city->id]
                                                            : 0;
                                                        ?></a>
                                                    <p>поверхностей</p></li>
                                                <li><a href="<?= $city->getTvPageUrl() ?>"><?php
                                                        echo isset($cnt['tvs'][$city->id])
                                                            ? $cnt['tvs'][$city->id]
                                                            : 0;
                                                        ?></a>
                                                    <p>телеканалов</p></li>
                                                <li><a href="<?= $city->getRadioPageUrl() ?>"><?php
                                                        echo isset($cnt['radio'][$city->id])
                                                            ? $cnt['radio'][$city->id]
                                                            : 0;
                                                        ?></a>
                                                    <p>радиостанций</p></li>
                                                <li><a href="<?= $city->getTickerPageUrl() ?>"><?php
                                                        echo isset($cnt['tickers'][$city->id])
                                                            ? $cnt['tickers'][$city->id]
                                                            : 0;
                                                        ?></a>
                                                    <p>телеканалов<br>(бегущая строка)</p></li>
                                            </ul>
                                        </div>
                                        <!--новая верстка "реклама в городах"-->
                                    </div>
                                </td>
                                <td class="footer-kalk" data-label="Основные виды деятельности">
                                    <div class="td">
                                        <?= $city->activities ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container seo-content" style="margin-top: 50px; margin-bottom: 50px;">
    <div class="row">
        <div class="text-widget"><?= Text::get('after-cities') ?></div>
    </div>
</div>
