<?php
declare(strict_types=1);

use frontend\widgets\Text;

?>
<div class="primeru-rabot">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2><?= Text::get('examples', 'Примеры нашей рекламы', 'string') ?></h2>
      </div>
        <?php ob_start(); ?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-padding-video">
        <div class="primeru-video-item">
          <div class="primeru-video">
            <iframe width="560" height="400" src="https://www.youtube.com/embed/MTHrFeLVdiY" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="primeru-video-title">Рекламный ролик «SetlGroup»</div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-padding-video">
        <div class="primeru-video-item">
          <div class="primeru-video">
            <iframe width="560" height="400" src="https://www.youtube.com/embed/qqfRI9UPsyc" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="primeru-video-title">Рекламный ролик «Бюро»</div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-padding-video">
        <div class="primeru-video-item">
          <div class="primeru-video">
            <iframe width="560" height="400" src="https://www.youtube.com/embed/1f4ipsPhIs8" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="primeru-video-title">Рекламный ролик «Мотив»</div>
        </div>
      </div>
        <?php
        $text = ob_get_contents();
        ob_end_clean();
        ?>
        <?= Text::get('video-examples', $text, 'html') ?>
    </div>
  </div>
</div>
