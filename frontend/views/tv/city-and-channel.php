<?php
declare(strict_types=1);

use common\models\OrderForm;
use frontend\widgets\Text;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\OfferViewCollection;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\OfferView;
use yii\web\View;

/* @var View $this */
/* @var OfferView $offer */
/* @var IBasket $basket */

$this->registerCssFile('/css/new-channels.css');
$this->registerCssFile('/css/services.css');
if (YII_ENV_DEV) {
    $this->registerJsFile('/js/bundle.dev.js');
} else {
    $this->registerJsFile('/js/bundle.js');
}

$city = $offer->city();
$channel = $offer->channel();

$this->title = $offer->heading();
$this->params['breadcrumbs'][] = ['label' => 'Реклама в городах', 'url' => ['/cities']];
$this->params['breadcrumbs'][] = ['label' => $city->name(), 'url' => $city->url()];
$this->params['breadcrumbs'][] = ['label' => $channel->name()];
$this->params['title'] = $offer->pageTitle();
$this->params['description'] = $offer->pageDescription();
if (!$offer->isAdvanced()) {
    $this->params['subTitle'] = $city->subTitle();
    echo $this->render('_examples');
} else {
    $this->params['hideBreadcrumbs'] = true;
    $this->params['isMinifyMenu'] = true;
?>
<div class="banner">
    <?php if ($channel->hasBigLogo()) { ?>
    <img class="banner__top" src="<?= $channel->bigLogo()->originalSizeSrc() ?>" alt="" style="margin-bottom: 20px;">
    <?php } ?>
    <p class="banner__city<?= $channel->color()->hasHeading() ? ' banner__' . $channel->color()->heading() : '' ?>">
      <strong><?= mb_strtoupper($city->where()) ?></strong>
    </p>
    <?php if ($channel->hasBigBanner()) { ?>
    <div class="banner__image">
        <img src="<?= $channel->bigBanner()->originalSizeSrc() ?>" alt="">
    </div>
    <?php } ?>
    <div class="container">
        <h2 class="banner__heading<?= $channel->color()->hasHeading() ? ' banner__' . $channel->color()->heading() : '' ?>">
          Аудитория телеканала
        </h2>
        <?php if ($offer->hasPotentialAudience() || $offer->hasCityMapImage()) { ?>
        <div class="banner__columns">
            <?php if ($offer->hasPotentialAudience()) { ?>
            <div class="banner__info banner__info--<?= $channel->color()->line() ?>">
                <div class="banner__block">
                    <p class="banner__subheading">Потенциальная аудитория:</p>
                    <p class="banner__text">
                        <span class="banner__gradient banner__gradient--<?= $channel->color()->potentialAudience() ?>">
                            <?= $offer->potentialAudience()->total() ?>
                        </span>чел.
                    </p>
                </div>
                <div class="banner__block">
                    <p class="banner__subheading">Пол и возраст:</p>
                    <p class="banner__text">
                      Мужчины
                      <span class="banner__gradient banner__gradient--<?= $channel->color()->potentialAudience() ?>">
                          <?= $offer->potentialAudience()->men() ?></span> чел.
                    </p>
                    <p class="banner__text">Женщины
                      <span class="banner__gradient banner__gradient--<?= $channel->color()->potentialAudience() ?>">
                          <?= $offer->potentialAudience()->women() ?></span> чел.
                    </p>
                    <p class="banner__text">
                      От 18 до 45 лет
                      <span class="banner__gradient banner__gradient--<?= $channel->color()->potentialAudience() ?>">
                          <?= $offer->potentialAudience()->youth() ?></span> чел.
                    </p>
                </div>
            </div>
            <?php } ?>
            <?php if ($offer->hasCityMapImage()) { ?>
            <div class="banner__map">
                <img src="<?= $offer->cityMapImage()->src() ?>" alt="" width="461" height="465">
            </div>
            <?php } ?>
        </div>
        <?php } ?>
        <div class="banner__footer">
            <p>Рекламное агентство «НИКО-медиа» предлагает изготовление и размещение рекламы на всех популярных телевизионных каналах города <?= $city->name() ?>.</p>
            <p>В зависимости от особенностей целевой аудитории и информационных материалов, мы готовы предложить нашим клиентам ролики, заставки и участие на правах спонсора. </p>
            <p>Рассчитать стоимость рекламы на телевидении вы можете на нашем калькуляторе ниже, либо просто проконсультируйтесь с нами по телефону.</p>
        </div>
    </div>
</div>
<?php } ?>
<div class="container" id="app">
  <tv
    :regions='null'
    :types='<?= json_encode($basket->getAdvTypeCollection()) ?>'
    :default-offers='<?= json_encode(new OfferViewCollection([$offer], 1, null)) ?>'
    :default-basket='<?= json_encode($basket) ?>'
    :default-limit="9"
    default-city-id="<?= $offer->city()->id() ?>"
  ></tv>
  <div class="modal fade" id="order" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="form-header text-center">
        <a class="form-close" data-dismiss="modal" aria-hidden="true">
          <span class="close-one"></span>
          <span class="close-two"></span>
        </a>
        <h4>Оформить <br>заказ</h4>
      </div>
      <div class="form-body">
          <?= $this->render('form', ['model' =>  new OrderForm()])?>
      </div>
    </div>
  </div>
</div>
<?= $this->render('_how- to-place') ?>
<div class="container content-padding seo-content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <b>Реклама на телеканале <?=$offer->name()?> <?=$city->where()?></b>
        <?php ob_start(); ?>
        <p>
          Реклама на телеканалах города <?= $city->name() ?>.
          Размещаем рекламные ролики и заставки на телеканале <?= $channel->name() ?>.
          Подберём для вас оптимальный вариант размещения. Если у вас появились вопросы по размещению и
          ценам, позвоните нам, проконсультируем по всем интересующим вас вопросам.
        </p>
        <?php
        $text = ob_get_contents();
        ob_end_clean();
        echo Text::get('tv-city-' . $city->name() . 'tv-' . $channel->name(), $text, 'html');
        ?>
    </div>
  </div>
</div>
<?php if ($offer->hasPriceTableCollection()) { ?>
  <style>
      .table-wrapper {
          overflow-x: auto;
      }
      .table-wrapper--new-channel table {
          width: 100%;
          min-width: 600px;
          text-align: center;
      }
      .table-wrapper--new-channel table th {
          font-weight: normal;
          text-align: center;
          color: #025599;
      }
      .table-wrapper--new-channel table th,
      .table-wrapper--new-channel table td {
          padding: 25px;
      }
      .table-wrapper--new-channel tbody tr:nth-child(even) {
          background-color: rgba(0, 0, 0, 0.03);
      }
      @media screen and (min-width: 1200px) {
          .table-wrapper--new-channel table th {
              font-size: 20px;
          }
          .table-wrapper--new-channel table td {
              font-size: 18px;
          }
      }
  </style>
  <div class="container content-padding">
    <b>Стоимость рекламы</b>
    <div class="table-wrapper table-wrapper--new-channel">
      <table>
        <thead>
        <tr>
          <th rowspan="2">Длительность<br> ролика</th>
          <th colspan="2" style="border-bottom: 1px solid #ECECEC">Время выхода/стоимость</th>
        </tr>
        <tr>
          <th>Прайм-тайм</th>
          <th>Off-прайм</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($offer->priceTableCollection()->all() as $priceTableLine) { ?>
          <tr>
            <td><?= $priceTableLine->duration() ?></td>
            <td><?= $priceTableLine->primeTime() ?></td>
            <td><?= $priceTableLine->offPrime() ?></td>
          </tr>
        <?php }?>
        </tbody>
      </table>
    </div>
  </div>
<?php } ?>
<?= $this->render('/layouts/partial/block/causes', ['afterCausesKey' => 'after-tv-form-'.$city->slug()]) ?>
<?= $this->render('/layouts/partial/block/main-feedback-form') ?>
