<?php
declare(strict_types=1);

use frontend\widgets\Text;

?>
<div class="fixator-affix"></div>
<div class="kak-razmestit-reklamy">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="text-center white-title"><?=Text::get('kak-razm-title', 'Как разместить рекламу на ТВ', 'string')?></h2>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="etapu-item">
                    <div class="etapu-item-namber">
                        <span>1</span>
                    </div>
                    <div class="etapu-item-text">
                        <div class="h3">
                            <?= Text::get('kak-razm-subtitle-1', 'Формат рекламы', 'string')?>
                        </div>
                        <p>
                            <?=Text::get('kak-razm-text-1', 'Определяемся с форматом рекламы, которая будет наиболее эффективна в вашем случае.', 'text')?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="etapu-item">
                    <div class="etapu-item-namber">
                        <span>2</span>
                    </div>
                    <div class="etapu-item-text">
                        <div class="h3">
                            <?=Text::get('kak-razm-subtitle-2', 'Создание рекламы', 'string')?>
                        </div>
                        <p>
                            <?=Text::get('kak-razm-text-2', 'Необходимо создать ваше рекламное сообщение, от которого будет зависеть удачная будет реклама или нет.', 'text')?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-before">
                <div class="etapu-item">
                    <div class="etapu-item-namber">
                        <span>3</span>
                    </div>
                    <div class="etapu-item-text">
                        <div class="h3">
                            <?=Text::get('kak-razm-subtitle-3', 'Создание плана проката', 'string')?>
                        </div>
                        <p>
                            <?=Text::get('kak-razm-text-3', 'Одна из немаловажных работ, отвечающая на вопрос где и когда будет показываться ваша реклама.', 'text')?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="etapu-item">
                    <div class="etapu-item-namber">
                        <span>4</span>
                    </div>
                    <div class="etapu-item-text">
                        <div class="h3">
                            <?=Text::get('kak-razm-subtitle-4', 'Прокат рекламы', 'string')?>
                        </div>
                        <p>
                            <?=Text::get('kak-razm-text-4', 'Определяемся с форматом рекламы, которая будет наиболее эффективна в вашем случае.', 'text')?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-before">
                <div class="etapu-item">
                    <div class="etapu-item-namber">
                        <span>5</span>
                    </div>
                    <div class="etapu-item-text">
                        <div class="h3">
                            <?=Text::get('kak-razm-subtitle-5', 'Предоставление отчетности', 'string')?>
                        </div>
                        <p>
                            <?=Text::get('kak-razm-text-5', 'Необходимо создать ваше рекламное сообщение, от которого будет зависеть удачная будет реклама или нет.', 'text')?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
