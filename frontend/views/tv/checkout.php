<?php
declare(strict_types=1);

use common\models\OrderForm;

/** @var OrderForm $model */
/** @var boolean $success */
/** @var array $dataLayer */
/** @var string $error */

$this->title = "Оформить заказ";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php if (empty($error)) {
              echo $this->render('form', [
                  'model' => $model,
                  'success' => $success,
                  'dataLayer' => $dataLayer,
              ]);
            } else { ?>
              <div class="alert alert-danger">
                <?= $error ?>
              </div>
            <?php } ?>
        </div>
    </div>
</div>
