<?php
declare(strict_types=1);

use common\models\City;
use common\models\OrderForm;
use common\models\Tv;
use frontend\widgets\Html;
use frontend\widgets\Text;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\OfferViewCollection;
use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use yii\web\View;
use yii\widgets\Pjax;

/* @var View $this */
/* @var RegionViewCollection $regions */
/* @var OfferViewCollection $offers */
/* @var IBasket $basket */
/* @var int|null $cityId */
/* @var City|null $city */
/* @var Tv|null $offer */

if (!empty($city)) {
    $this->params['breadcrumbs'][] = ['label' => 'Реклама в городах', 'url' => ['/cities']];
    $this->params['breadcrumbs'][] = ['label' => $city->name, 'url' => ['/cities/' . $city->slug]];
    $this->params['subTitle'] = $city->content_tv;
    $this->title = empty($city->title_tv) ? "Реклама на ТВ" : $city->title_tv;
    if (empty($offer)) {
        $this->params['title'] = 'Реклама на ТВ ' . $city->where . ' - цены на размещение - РА «Нико-Медиа»';
    } else {
        $this->title = 'Реклама на ' . $offer->name . ' ' . $city->where;
        $this->params['title'] = 'Реклама на ' . $offer->name . ' ' . $city->where . ' - РА «Нико-Медиа»';
        $this->params['description'] =
            'Реклама на телеканалах города ' . $city->name . '. Размещаем рекламные ролики и заставки на телеканале '
            . $offer->name . '. Подберём для вас оптимальный вариант размещения.';
    }
    $this->params['cityBanner'] = $city;
} else {
    $this->title = "Реклама на ТВ";
    $this->params['subTitle'] = Text::get('tv-subtitle',
        'Телевидение - наиболее универсальный рекламный канал, позволяет донести рекламное сообщение до жителей небольших отдаленных северных городков, где отсутствует наружная реклама и не ловится большинство радиостанций. Реклама на ТВ массовая и оперативная - ролик можно заменить в течение пары дней, однако эффективна она не для всех. ТВ-ролики незаменимы для основной массы ТНП и раскрутки бренда, а, например, для продажи недвижимости есть более подходящие рекламные каналы.',
        'text');
}
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('/css/services.css');
if (YII_ENV_DEV) {
    $this->registerJsFile('/js/bundle.dev.js');
} else {
    $this->registerJsFile('/js/bundle.js');
}
echo $this->render('_examples');
?>
<div class="container" id="app">

  <tv
      :types='<?= json_encode($basket->getAdvTypeCollection()) ?>'
      :regions='<?= json_encode($regions) ?>'
      :default-offers='<?= json_encode($offers) ?>'
      :default-basket='<?= json_encode($basket) ?>'
      :default-limit="9"
      default-city-id="<?= $cityId ?>"
  ></tv>
  <div class="modal fade" id="order" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="form-header text-center">
        <a class="form-close" data-dismiss="modal" aria-hidden="true">
          <span class="close-one"></span>
          <span class="close-two"></span>
        </a>
        <h4>Оформить <br>заказ</h4>
      </div>
      <div class="form-body">
          <?= $this->render('form', ['model' =>  new OrderForm()])?>
      </div>
    </div>
  </div>
</div>
<?= $this->render('_how- to-place') ?>
<div class="container content-padding seo-content">
  <div class="row">
      <?php if(!empty($city)):?>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <?php if(empty($offer)):?>
              <b><?=Text::get('tv-seo-title-'.$city->slug, $city->title, 'string')?></b>
            <?php else:?>
              <b>Реклама на телеканале <?=$offer->name?> <?=$city->where?></b>
            <?php endif;?>
            <?php if(empty($offer)):?>
              <div class="text-widget">
                  <?=Text::get('tv-seo-text'.$city->slug, '', 'html')?>
              </div>
                <?php if($offers->isNotEmpty()):?>
                    <?php
                    $arr = [];
                    foreach ($offers->all() as $offerView) {
                        $arr[] = Html::a($offerView->name(), $offerView->url());
                    }
                    ?>
                <p>Заказать рекламу на телеканалах <?=implode(', ', $arr)?>.</p>
                <?php endif?>
            <?php else:?>
                <?= Text::get('tv-city-'.$city->name.'tv-' . $offer->name, '<p>Реклама на телеканалах города ' . $city->name . '. Размещаем рекламные ролики и заставки на телеканале ' . $offer->name . '.
                    Подберём для вас оптимальный вариант размещения. Если у вас появились вопросы по размещению и ценам, позвоните нам,
                    проконсультируем по всем интересующим вас вопросам.</p>', 'html');
                ?>
            <?php endif;?>
        </div>
      <?php else:?>
        <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
          <b><?=Text::get('tv-seo-title', 'Какая она - реклама на телевидении?', 'string')?></b>
          <div>
              <?php ob_start(); ?>
            <p>Наружная реклама – неотъемлемый и зачастую важнейший элемент любой полноценной рекламной кампании. Основные виды носителей - щиты, сити-форматы, брандмауэры, панель-кронштейны, призматроны, видеоэкраны и др. При размещении рекламы на щитах важно подобрать действительно нужные и арендовать их на оптимальный срок, чтобы рационально использовать бюджет и максимально привлечь внимание целевой аудитории.</p>
            <p>Телевидение - наиболее универсальный рекламный канал, позволяет донести рекламное сообщение до жителей небольших отдаленных северных городков, где отсутствует наружная реклама и не ловится большинство радиостанций. Реклама на ТВ массовая и оперативная - ролик можно заменить в течение пары дней, однако эффективна она не для всех. ТВ-ролики незаменимы для основной массы ТНП и раскрутки бренда, а, например, для продажи недвижимости есть более подходящие рекламные каналы. Если же вам нужно подать объявление о продаже авто, найме персонала или промо-акции в отдельном магазине - воспользуйтесь Бегущей Строкой онлайн.</p>
              <?php
              $text = ob_get_contents();
              ob_end_clean();
              ?>
              <?=Text::get('tv-seo-text', $text, 'html')?>
          </div>
        </div>
      <?php endif;?>
  </div>
</div>
<?= $this->render(
    '/layouts/partial/block/causes',
    ['afterCausesKey' => empty($city) ? 'after-tv-form' : 'after-tv-form-' . $city->slug]
) ?>
<?= $this->render('/layouts/partial/block/main-feedback-form') ?>
