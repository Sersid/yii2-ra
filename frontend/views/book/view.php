<?php
/**
 * Created by PhpStorm.
 * User: Yulai
 * Date: 21.02.2017
 * Time: 18:07
 */

use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var common\models\Review $review */
$this->title = Html::encode($data['name']);
$this->registerMetaTag(['name' => 'description', 'content' => trim(mb_substr(trim(strip_tags(empty($data['full_desc']) ? $data['short_desc'] : $data['full_desc'])), 0, 150,'utf-8'))], 'description');

$this->params['breadcrumbs'][] = ['label' => 'Книги', 'url' => ['/book']];

$this->params['breadcrumbs'][] = $this->title;

$this->params['contentTitle'] = true;
?>
<?php if($data['icon'] != 'list-alt') : ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 anchors">
                <?php echo $this->render('_view_part', ['value' => $data]); ?>
            </div>
        </div>
    </div>
<?php else : ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?=empty($model->image) ? '' : Html::tag('p', Html::img($model->image->getSrc(), ['alt' => $model->name, 'class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-12'])) ?>
            <?=empty($data['full_desc']) ? Html::tag('p', $data['short_desc']) : $data['full_desc'];?>
        </div>
    </div>
</div>
<?php endif; ?>
