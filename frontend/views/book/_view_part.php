<?php
/**
 * Created by PhpStorm.
 * User: Yulai
 * Date: 24.02.2017
 * Time: 15:49
 */

use frontend\widgets\Html;
/** @var array $value */
?>

<?php echo ($value['icon'] == 'list-alt') ? Html::a($value['name'], ['/book/view', 'slug' => $value['slug']]) : Html::tag('p', $value['name']); ?>

<?php if($value->children()->all()) : ?>
<ul>
    <?php foreach($value->children(1)->all() as $value) : ?>
        <li class="<?php echo $value['icon']; ?>">
            <?php if($value->children()->all()) : ?>
                <?php echo $this->render('_view_part', ['value' => $value]); ?>
            <?php else : ?>
                <?php if($value['active'] == true) : ?>
                    <?php echo ($value['icon'] == 'list-alt') ? Html::a($value['name'], ['/book/view', 'slug' => $value['slug']]) : Html::tag('p', $value['name']); ?>
                <?php endif; ?>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
