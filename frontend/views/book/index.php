<?php
/**
 * Created by PhpStorm.
 * User: Yulai
 * Date: 21.02.2017
 * Time: 18:01
 */

use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var yii\web\View $this */
/* @var common\models\Review $review */
/* @var common\models\Review $dataProvider */

$this->title = "Книги";
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;

?>

<div class="content-body gray-bg">
    <div class="container">
        <div class="row">
            <?php Pjax::begin([
                'scrollTo' => 300,
            ])?>

            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'layout' => "{items}\n<div class=\"col-lg-12 col-md-12 hidden-sm hidden-xs\">{pager}</div>",
                'emptyTextOptions' => ['class' => 'col-md-12'],
                'pager' => [
                    'options' => ['class' => 'pagination-block'],
                    'nextPageLabel' => false,
                    'prevPageLabel' => false,
                    'disabledPageCssClass' => 'no-active',
                    'maxButtonCount' => 5
                ],
            ]); ?>
            <?php Pjax::end()?>
        </div>
    </div>
</div>
