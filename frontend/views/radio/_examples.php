<?php
$showTitle = isset($showTitle) ? $showTitle : true;
?>
<div class="primeru-rabot">
    <div class="container">
        <div class="row">
          <?php if ($showTitle) { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?=\frontend\widgets\Text::get('examples', 'Примеры нашей рекламы', 'string')?></h2>
            </div>
          <?php } ?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/ingener.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/ingener.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Инженер»</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/parfum.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/parfum.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Парфюм»</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/riabinushka.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/riabinushka.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Рябинушка»</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/uyut.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/uyut.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Уют»</div>
                </div>
            </div>
        </div>
    </div>
</div>
