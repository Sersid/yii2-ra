<?php
declare(strict_types=1);

use common\models\City;
use common\models\OrderForm;
use common\models\Radio;
use frontend\widgets\Html;
use frontend\widgets\Text;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\OfferViewCollection;
use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use yii\web\View;
use yii\widgets\Pjax;

/* @var View $this */
/* @var RegionViewCollection $regions */
/* @var OfferViewCollection $offers */
/* @var IBasket $basket */
/* @var int|null $cityId */
/* @var City|null $city */
/* @var Radio|null $offer */

if (!empty($city)) {
    $this->params['breadcrumbs'][] = ['label' => 'Реклама в городах', 'url' => ['/cities']];
    $this->params['breadcrumbs'][] = ['label' => $city->name, 'url' => ['/cities/' . $city->slug]];
    $this->params['subTitle'] = $city->content_radio;
    $this->title = empty($city->title_radio) ? "Реклама на радио" : $city->title_radio;
    if (empty($offer)) {
        $this->params['title'] = 'Реклама на радио ' . $city->where . ' - цены на размещение - РА «НИКО-Медиа»';
    } else {
        $this->title = 'Реклама на радио "' . $offer->name . '" ' . $city->where;
        $this->params['title'] = 'Заказать рекламу на радио "'
                                 . $offer->name . '" ' . $city->where
                                 . ' - РА «НИКО-Медиа»';
        $this->params['description'] = 'Размещение рекламы на радио "'
                                       . $offer->name . '" ' . $city->where
                                       . '. Наши специалисты помогут выбрать вам оптимальный вариант размещения '
                                       . 'рекламы на радио для ваших услуг товаров.';
    }
    $this->params['cityBanner'] = $city;
} else {
    $this->title = "Реклама на радио";
    $this->params['subTitle'] = Text::get('radio-subtitle', 'Краткое описание раздела');
}
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('/css/services.css');
if (YII_ENV_DEV) {
    $this->registerJsFile('/js/bundle.dev.js');
} else {
    $this->registerJsFile('/js/bundle.js');
}
echo $this->render('_examples.php');
?>
<div class="container" id="app">
  <radio :types='<?= json_encode($basket->getAdvTypeCollection()) ?>'
    :regions='<?= json_encode($regions) ?>'
    :default-offers='<?= json_encode($offers) ?>'
    :default-basket='<?= json_encode($basket) ?>'
    :default-limit="9"
    default-city-id="<?= $cityId ?>"
  ></radio>
  <div class="modal fade" id="order" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="form-header text-center">
        <a class="form-close" data-dismiss="modal" aria-hidden="true">
          <span class="close-one"></span>
          <span class="close-two"></span>
        </a>
        <h4>Оформить <br>заказ</h4>
      </div>
      <div class="form-body">
          <?= $this->render('/tv/form', ['model' =>  new OrderForm()])?>
      </div>
    </div>
  </div>
</div>
<div class="prichinu">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="text-center"><?=Text::get('radio-causes-title', '4 причины заказать рекламу на радио у нас', 'string')?></h2>
            </div>
            <?php ob_start(); ?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">8</div>
                        <div class="prichinu-item-title">Лет опыта</div>
                    </div>
                    <p>
                        Работаем с 2006 года
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">30</div>
                        <div class="prichinu-item-title">Городов</div>
                    </div>
                    <p>
                        Обширная филиальная сеть
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">700</div>
                        <div class="prichinu-item-title">Проектов</div>
                    </div>
                    <p>
                        Ежедневно оттачиваем свое мастерство
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">2 500</div>
                        <div class="prichinu-item-title">Клиентов</div>
                    </div>
                    <p>
                        Постоянные клиенты из года в год
                    </p>
                </div>
            </div>
            <?php
            $text = ob_get_contents();
            ob_end_clean();
            ?>
            <?=Text::get('radio-causes-text', $text, 'html')?>
        </div>
    </div>
</div>
<div class="container content-padding seo-content">
    <div class="row">
        <?php if(!empty($city)):?>
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                <?php if(empty($offer)):?>
                    <?=Text::get('after-radio-form-'.$city->slug)?>
                <?php else:?>
                    <b>Размещение рекламы на радио "<?=$offer->name?>" <?=$city->where?> </b>
                <?php endif;?>
                <?php if(empty($offer)):?>
                    <div class="text-widget">
                        <?=Text::get('radio-seo-text'.$city->slug, '', 'html')?>
                    </div>
                    <?php if($offers->isNotEmpty()):?>
                        <?php
                        $arr = [];
                        foreach ($offers->all() as $offerView) {
                            $arr[] = Html::a($offerView->name(), $offerView->url());
                        }
                        ?>
                    <p>Заказать рекламу на радиостанциях <?=implode(', ', $arr)?>.</p>
                    <?php endif?>
                <?php else:?>
                    <?= Text::get('radio-city-'.$city->name.'radio-'.$offer->name, '<p>Размещение рекламы на радио "'.$offer->name.'" '.$city->where.'. Наши специалисты помогут выбрать вам оптимальный вариант размещения рекламы на радио для ваших услуг и товаров.</p>', 'html');
                    ?>
                <?php endif;?>
            </div>
        <?php else:?>
            <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                    <?=Text::get('after-radio-form'); ?>
                </div>
            </div>
        <?php endif;?>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg form-shake">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?= Text::get('outdoor-form-title', 'Остались вопросы?', 'string') ?></div>
                        <p><?= Text::get(
                                'outdoor-form-feedback-text',
                                'Давайте обсудим Ваши задачи. И сделаем их',
                                'string'
                            ) ?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()]) ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl') ?>
            </div>
        </div>
    </div>
</div>
