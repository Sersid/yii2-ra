<?php
/* @var View $this */
/* @var bool $hideCntBlocks */
/* @var int $cnt_outdoor_ad */
/* @var int $cnt_tv */
/* @var int $cnt_radio */
/* @var int $cnt_ticker */

/* @var City $model */

use frontend\models\City;
use frontend\widgets\Text;
use yii\bootstrap\Html;
use yii\web\View;

$this->title = "Контакты";
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <div class="container">
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-4">
                <?= Html::a(
                    'Вы выбрали: ' . Yii::$app->city->name,
                    '#',
                    ['class' => 'yellow-btn', 'data-toggle' => 'modal', 'data-target' => '#set-city']
                ); ?>
            </div>
        </div>

        <div class="row">
            <div class="adress-block">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adress-item">
                    <h2></h2>
                    <?= empty($model->contact_text) ? 'Размещаем рекламу в городе ' . $model->name
                                                      . '. Большой выбор рекламных площадок. Консультации и помощь в выборе.'
                        : $model->contact_text ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adress-item">
                    <h2>Заказать рекламу <?= $model->where ?></h2>
                    <p><?= Yii::$app->city->phone ?></p>
                    <p><a href="mailto:zakaz@niko-m.ru">zakaz@niko-m.ru</a></p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adress-item">
                    <h2>Часы работы офиса</h2>
                    <div class="time-work-left">
                        <div class="time-work-green-border">
                            <p>Пн</p>
                            <p>Вт</p>
                            <p>Ср</p>
                            <p>Чт</p>
                            <p>Пт</p>
                        </div>
                        <div class="text-center">
                            <b><?= Text::get('contact-mode-time', '9:00 - 18:00', 'string') ?></b>
                        </div>
                    </div>
                    <div class="time-work-right">
                        <div class="time-work-gray-border">
                            <p>Сб</p>
                            <p>Вс</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adress-item">
                    <h2>Реквизиты фирмы</h2>
                    <div class="dowld-box">
                        <img src="img/dwnld-btn.png" alt="">
                        <a href="<?= Yii::$app->city->requisitesFileUrl ?>">
                            Скачать реквизиты,<br>
                            DOC (<?= Yii::$app->city->requisitesFileSize ?>)
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!$hideCntBlocks): ?>
        <div class="container">
            <div class="row" style="margin-bottom: 40px;">
                <div class="adress-block" style="margin-bottom: 15px;">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adress-item">
                        <h2><?= $model->title ?></h2>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adress-item"></div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                            <div class="content-item hover-item">
                                <a href="<?= $model->getOutdoorAdPageUrl() ?>">
                                    <div class="item-img">
                                        <img src="/img/yslygi-1.png" alt="">
                                    </div>
                                    <div class="item-title">Наружная реклама <?= $model->where ?>
                                        <br/>(<?= $cnt_outdoor_ad ?> поверхностей)
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                            <div class="content-item hover-item">
                                <a href="<?= $model->getTvPageUrl() ?>">
                                    <div class="item-img">
                                        <img src="/img/yslygi-2.png" alt="">
                                    </div>
                                    <div class="item-title">Реклама на телевидении <?= $model->where ?>
                                        <br/>(<?= $cnt_tv ?> телеканалов)
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                            <div class="content-item hover-item">
                                <a href="<?= $model->getRadioPageUrl() ?>">
                                    <div class="item-img">
                                        <img src="/img/yslygi-3.png" alt="">
                                    </div>
                                    <div class="item-title">Реклама на радио <?= $model->where ?><br/>(<?= $cnt_radio ?>
                                        радиостанций)<br/></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                            <div class="content-item hover-item">
                                <a href="<?= $model->getTickerPageUrl() ?>">
                                    <div class="item-img">
                                        <img src="/img/yslygi-4.png" alt="">
                                    </div>
                                    <div class="item-title">Бегущая строка <?= $model->where ?><br/>(<?= $cnt_ticker ?>
                                        телеканалов)
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    <?php endif; ?>
    <div id="contact-maps">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad50d02127f45d001970dcc9f82e60562234de44f9e00d8fbebfdd806ffef2571&amp;width=100%25&amp;lang=ru_RU&amp;scroll=true"></script>
    </div>
</div>
