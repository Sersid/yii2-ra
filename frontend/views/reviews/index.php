<?php

use frontend\widgets\Reviews;
use frontend\widgets\Text;

$this->title = "Отзывы";
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;
?>
<div class="review-content-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="review-content-title">
                    <p>
                        <?=Text::get('reviews-text', 'Агентство маркетинговых и рекламных коммуникаций "НИКО-медиа" специализируется на размещении рекламы, разработке и проведении комплексных рекламных кампаний в Тюменской области. Мы живем в Сургуте и хорошо знаем свой регион от Ишима до Нового Уренгоя. В 2002 году агентство было создано группой молодых специалистов для размещения рекламы иногородних рекламных агентств.', 'text')?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?= Reviews::widget(['showHeaderText' => false]) ?>
