<?php

use frontend\widgets\Text;

/* @var yii\web\View $this */
/* @var common\models\Review $review */

$this->title = "Примеры наших работ";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="primeru-rabot" style="padding-top: 0px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3><?=Text::get('examples-tv-title', 'Примеры рекламы на ТВ', 'string')?></h3>
            </div>
            <?php ob_start(); ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-padding-video">
                <div class="primeru-video-item">
                    <div class="primeru-video">
                        <iframe width="560" height="400" src="https://www.youtube.com/embed/MTHrFeLVdiY" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="primeru-video-title">Рекламный ролик «SetlGroup»</div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-padding-video">
                <div class="primeru-video-item">
                    <div class="primeru-video">
                        <iframe width="560" height="400" src="https://www.youtube.com/embed/qqfRI9UPsyc" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="primeru-video-title">Рекламный ролик «Бюро»</div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-padding-video">
                <div class="primeru-video-item">
                    <div class="primeru-video">
                        <iframe width="560" height="400" src="https://www.youtube.com/embed/1f4ipsPhIs8" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="primeru-video-title">Рекламный ролик «Мотив»</div>
                </div>
            </div>
            <?php
            $text = ob_get_contents();
            ob_end_clean();
            ?>
            <?=Text::get('video-examples', $text, 'html')?>
        </div>
    </div>
</div>
<div class="primeru-rabot">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3><?=Text::get('examples-radio-title', 'Примеры рекламы на радио', 'string')?></h3>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/ingener.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/ingener.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Инженер»</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/parfum.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/parfum.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Парфюм»</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/riabinushka.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/riabinushka.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Рябинушка»</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="primeru-video-item">
                    <audio class="radio-audio" controls>
                        <source src="/img/audio/uyut.mp3" type="audio/mpeg">
                        <p>Аудио не поддерживается вашим браузером</p>
                        <a href="/img/audio/uyut.mp3">Вы можете скачать аудиоролик</a>
                    </audio>
                    <div class="primeru-video-title">Рекламный ролик «Уют»</div>
                </div>
            </div>
        </div>
    </div>
</div>
