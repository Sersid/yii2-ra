<?php

/* @var yii\web\View $this */
/* @var common\models\FormCallMe $model */
/* @var yii\widgets\ActiveForm $form */
new yii\widgets\ActiveField;
?>
<div class="modal fade" id="zakaz-zvonka" tabindex="-1" role="dialog" aria-labelledby="zakaz-zvonka-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="form-header text-center">
            <a class="form-close" data-dismiss="modal" aria-hidden="true">
                <span class="close-one"></span>
                <span class="close-two"></span>
            </a>
            <h4 id="zakaz-zvonka-title">Оформление <br>заявки</h4>
        </div>
        <div class="form-body">
            <?php /* Pjax::begin(['enablePushState' => false]) ?>
            <?if(isset($success) && $success):?>
                <div class="alert">123</div>
            <?else:?>
            <?php $form = ActiveForm::begin([
                    'action' => ['/call-me'],
                    'options' => ['data-pjax' => true],
                    'fieldConfig' => [
                        'options' => ['class' => 'form-body-block'],
                        'inputOptions' => ['class' => ''],
                    ]
                ]); ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => '+7 (___) ___-__-__']) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'type' => 'email']) ?>
                <?= Html::submitButton('Оформить заявку', ['class' => 'yellow-btn']) ?>
            <?php ActiveForm::end(); ?>
            <?endif;?>
            <?php Pjax::end(); */ ?>
        </div>
    </div>
</div>


