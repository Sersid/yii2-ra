<?php
declare(strict_types=1);

use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;

/** @var Side $side */
?>
<div class="container">
  <table class="table table-bordered">
    <tr>
      <td>Page title</td>
      <td><?= $side->getPageTitle() ?></td>
    </tr>
    <tr>
      <td>Page description</td>
      <td><?= $side->getPageDescription() ?></td>
    </tr>
    <tr>
      <td>Название</td>
      <td><?= $side->getName() ?></td>
    </tr>
    <tr>
      <td>Цена</td>
      <td><?= $side->getPrices()->getFormattedRent() ?></td>
    </tr>
    <tr>
      <td>№</td>
      <td><?= $side->getNumber() ?></td>
    </tr>
    <tr>
      <td>Рейтинг</td>
      <td><?= $side->getRating() ?></td>
    </tr>
    <tr>
      <td>Адрес</td>
      <td><?= $side->getFullAddress() ?></td>
    </tr>
    <tr>
      <td>Статус</td>
      <td class="bg-danger">!!!TODO!!!</td>
    </tr>
    <tr>
      <td>Монтаж в месяц размещения</td>
      <td><?= $side->getPrices()->getFormattedInstallation() ?></td>
    </tr>
    <tr>
      <td>Печать на баннере</td>
      <td><?= $side->getPrices()->getFormattedPrint() ?></td>
    </tr>
    <tr>
      <td>Дизайн</td>
      <td><?= $side->getPrices()->getFormattedDesign() ?></td>
    </tr>
    <tr>
      <td>Изображения</td>
      <td>
        <?php if ($side->getPhotoCollection()->isNotEmpty()) {
          foreach ($side->getPhotoCollection() as $photo) {
              echo $photo->getSmall()->getSrc();
              echo $photo->getLarge()->getSrc();
          }
        } ?>
      </td>
    </tr>
  </table>
</div>
