<?php
declare(strict_types=1);

use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\SideCollection;

/** @var SideCollection $sideCollection */
/** @var Side $side */
?>
<div class="container">
  <table class="table table-bordered">
      <?php foreach ($sideCollection as $side) { ?>
        <tr>
          <td><a href="<?= $side->getUrl() ?>"><?= $side->getNumber() ?></a></td>
          <td><?= $side->getCityName() ?></td>
          <td><?= $side->getShortAddress() ?></td>
          <td><?= $side->getSurface() ?></td>
          <td><?= $side->getPrices()->getRent() ?></td>
          <td><?= $side->getCyrillicLetter() ?></td>
          <td><?= $side->getRating() ?></td>
        </tr>
      <?php } ?>
  </table>
</div>
