<?php
declare(strict_types=1);


/** @var City $this->params['cityBanner'] */

use common\models\City;

if (isset($this->params['cityBanner'])) {
  switch ($this->params['cityBanner']->slug) {
      // case 'surgut':
      //     $url = '/outdoor-ad/surgut';
      //     $bannerXl = '/img/banners/surgut-desctop.jpg';
      //     $bannerSm = '/img/banners/surgut-mobile.jpg';
      //     break;
      // case 'sochi':
      //     $url = '/outdoor-ad/sochi';
      //     $bannerXl = '/img/banners/ostalnie-desctop.jpg';
      //     $bannerSm = '/img/banners/ostalnie-mob.jpg';
      //     break;
      // case 'simferopol':
      //     $url = '/outdoor-ad/simferopol';
      //     $bannerXl = '/img/banners/ostalnie-desctop.jpg';
      //     $bannerSm = '/img/banners/ostalnie-mob.jpg';
      //     break;
    default:
        $url = '';
        $bannerXl = '';
        $bannerSm = '';
      break;
  }
  if (!empty($url) && !empty($bannerXl) && !empty($bannerSm)) {
    ?>
    <div class="banner-50" style="margin-top: -15px;padding-bottom: 25px">
      <a href="<?= $url ?>">
        <picture>
          <source media="(min-width: 600px)" srcset="<?= $bannerXl ?>" />
          <source srcset="<?= $bannerSm ?>" />
          <img src="<?= $bannerXl ?>" alt="Цены снижены на 50% на январь 2021" style="display: block; width: 100%; height: auto;">
        </picture>
      </a>
    </div>
    <?php
  }
}
