<?php

use yii\widgets\Menu;

/* @var yii\web\View $this */
/* @var common\models\Review $review */

$this->title = "Карта сайта";
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
<?= Menu::widget([
    'items' => $items,
]);?>
</div>
