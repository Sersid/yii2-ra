<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var yii\web\View $this */
/* @var common\models\Review $review */

$this->title = "Полезная информация";
$this->params['breadcrumbs'][] = $this->title;
$this->params['showInnerMenu'] = true;
?>
<div class="content-body gray-bg">
    <div class="container">
        <div class="row">
            <?php Pjax::begin([
                'scrollTo' => 300,
            ])?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="filter-container">
                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 text-center filter-box polezn-inf">
                            <div class="filter-btn <?=empty($activeCategory) ? 'active' : ''?>">
                                <?=Html::a('Все материалы', ['/info'])?>
                            </div>
                        </div>
                        <?php foreach ($categories as $category):?>
                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 text-center filter-box polezn-inf">
                            <div class="filter-btn <?=$activeCategory == $category->slug ? 'active' : ''?>">
                                <?=Html::a($category->name, ['/info/index', 'category' => $category->slug])?>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'layout' => "{items}\n<div class=\"col-lg-12 col-md-12 hidden-sm hidden-xs\">{pager}</div>",
                'emptyTextOptions' => ['class' => 'col-md-12'],
                'pager' => [
                    'options' => ['class' => 'pagination-block'],
                    'nextPageLabel' => false,
                    'prevPageLabel' => false,
                    'disabledPageCssClass' => 'no-active',
                    'maxButtonCount' => 5
                ],
            ]); ?>
            <?php Pjax::end()?>
        </div>
    </div>
</div>
