<?php

use yii\helpers\Html;

/* @var yii\web\View $this */
/* @var common\models\Review $review */
$this->title = Html::encode($model->name);
$this->registerMetaTag(['name' => 'description', 'content' => $model->short_desc], 'description');

$this->params['breadcrumbs'][] = ['label' => 'Полезная информация', 'url' => ['/info','category' => $activeCategory]];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contentTitle'] = true;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?=empty($model->image) ? '' : Html::tag('p', Html::img($model->image->getSrc(), ['alt' => $model->name, 'class' => 'col-lg-6 col-md-6 col-sm-6 col-xs-12'])) ?>
            <?=empty($model->full_desc) ? Html::tag('p', $model->short_desc) : $model->full_desc;?>
        </div>
    </div>
</div>
