<?php
declare(strict_types=1);

use frontend\widgets\Html;

if (!Yii::$app->user->isGuest) { ?>
    <div id="top-panel">
        <div class="container text-right">
            Вы можете
            <?php
            if (Yii::$app->session->get('edit-mode')) {
                echo Html::a(
                    'выключить режим правки',
                    ['/edit-mode', 'type' => 'disable', 'back_url' => urlencode(Yii::$app->request->url)],
                    ['class' => 'btn btn-warning btn-sm active']
                );
            } else {
                echo Html::a(
                    'включить режим правки',
                    ['/edit-mode', 'type' => 'enable', 'back_url' => urlencode(Yii::$app->request->url)],
                    ['class' => 'btn btn-info btn-sm']
                );
            }
            ?>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="edit-text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Изменить текст</h4>
                </div>
                <div id="edit-text-form"></div>
            </div>
        </div>
    </div>
<?php }
