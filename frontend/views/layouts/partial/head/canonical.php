<?php
declare(strict_types=1);

$this->registerLinkTag(['rel' => 'canonical', 'href' => $_SERVER['REQUEST_URI']]);
