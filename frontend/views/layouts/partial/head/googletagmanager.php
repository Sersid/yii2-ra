<?php
declare(strict_types=1);

?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-100061811-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-100061811-1');
    setTimeout(function() {
        gtag('event', location.pathname, {
            'event_category': 'Новый посетитель'
        });
    }, 15000);
</script>
