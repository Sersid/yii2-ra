<?php
declare(strict_types=1);

use common\models\MetaTag;

$description = MetaTag::getDescription(Yii::$app->request->url) . ($this->params['description'] ?? '');
$this->registerMetaTag(['name' => 'description', 'content' => $description], 'description');
