<?php
declare(strict_types=1);

use common\models\MetaTag;

$title = MetaTag::getTitle(Yii::$app->request->url);
if (empty($title)) {
    if (isset($this->params['title'])) {
        $title = $this->params['title'];
    } else {
        $title = $this->title;
    }
}
echo $title;
