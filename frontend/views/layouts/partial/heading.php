<?php
declare(strict_types=1);

use frontend\widgets\Html;

if (!isset($this->params['show_title']) || $this->params['show_title']) {
    echo Html::tag(
        'h1',
        Html::encode((isset($this->params['h3']) ? $this->params['h3'] : $this->title)),
        ['class' => isset($this->params['contentTitle']) ? 'content-title hidden-xs' : null]
    );
}
