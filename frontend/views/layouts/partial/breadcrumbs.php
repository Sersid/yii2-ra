<?php
declare(strict_types=1);

use frontend\widgets\Breadcrumbs;

if (isset($this->params['breadcrumbs'])) {
    echo Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]);
}
