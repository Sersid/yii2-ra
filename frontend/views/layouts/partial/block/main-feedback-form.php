<?php
declare(strict_types=1);

use frontend\widgets\Text;
use yii\widgets\Pjax;

?>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg form-shake">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?= Text::get('outdoor-form-title', 'Остались вопросы?', 'string') ?></div>
                        <p><?= Text::get(
                                'outdoor-form-feedback-text',
                                'Давайте обсудим Ваши задачи. И сделаем их',
                                'string'
                            ) ?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()]) ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl') ?>
            </div>
        </div>
    </div>
</div>
