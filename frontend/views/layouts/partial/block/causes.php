<?php
declare(strict_types=1);

use frontend\widgets\Text;

$afterCausesKey = $afterCausesKey ?? 'after-causes';
?>
<div class="prichinu">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="text-center">
                    <?= Text::get('causes-title', '4 причины заказать рекламу на ТВ у нас', 'string') ?>
                </h2>
            </div>
            <?php ob_start(); ?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">14</div>
                        <div class="prichinu-item-title">Лет опыта</div>
                    </div>
                    <p>Работаем с 2003 года</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">30</div>
                        <div class="prichinu-item-title">Городов</div>
                    </div>
                    <p>Обширная филиальная сеть</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">700</div>
                        <div class="prichinu-item-title">Проектов</div>
                    </div>
                    <p>Ежедневно оттачиваем свое мастерство</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item-last">
                <div class="prichinu-item">
                    <div>
                        <div class="red-title">2 500</div>
                        <div class="prichinu-item-title">Клиентов</div>
                    </div>
                    <p>Постоянные клиенты из года в год</p>
                </div>
            </div>
            <?php
            $text = ob_get_contents();
            ob_end_clean();
            echo Text::get('causes-text', $text, 'html');
            ?>
        </div>
        <div class="text-widget"><?=Text::get($afterCausesKey)?></div>
    </div>
</div>
