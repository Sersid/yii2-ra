<?php

use frontend\assets\AppAsset;
use frontend\widgets\Text;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Menu;

$isHome = $this->context->id == 'site' && $this->context->module->requestedAction->id == 'index';
$showBreadcrumbs = !($isHome || (isset($this->params['hideBreadcrumbs']) && $this->params['hideBreadcrumbs'] === true));
$isMinifyMenu = isset($this->params['isMinifyMenu']) && $this->params['isMinifyMenu'] === true;

/* @var View $this */
/* @var string $content */
$asset = AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="robots" content="noyaca"/>
  <link href="<?= $asset->baseUrl ?>/img/favicon.ico" rel="icon" type="image/x-icon">
  <?php $this->render('partial/head/canonical') ?>
  <?php $this->render('partial/head/description') ?>
  <title><?= $this->render('partial/head/title') ?></title>
  <?= Html::csrfMetaTags() ?>
  <?= $this->render('partial/head/googletagmanager') ?>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;700&display=swap" rel="stylesheet">
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $this->render('partial/edit') ?>
<div id="wrapper">
  <header class="header">
    <div class="black-main-menu white-main-menu">
      <div class="container">
        <div class="row">
          <nav class="navbar-default">
            <div class="navbar-header">
              <div class="hidden-lg hidden-md col-sm-2 col-xs-2">
                <div class="mob-menu-button-block">
                  <button type="button" class="navbar-toggle collapsed">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
              </div>
              <div class="hidden-lg hidden-md col-sm-8 col-xs-8 text-center">
                <div class="mob-logo">
                  <a href="/">
                    <img src="<?= $asset->baseUrl ?>/img/logo.png" alt="Рекламное агенство НИКО-медиа">
                  </a>
                </div>
                <a class="choose-city-header" href="#" data-toggle="modal" data-target="#set-city">Выбрать город</a>
              </div>
              <div class="hidden-lg hidden-md col-sm-2 col-xs-2 text-right">
                <div class="mob-tel-portf-block">
                  <div class="mob-tel">
                    <a href="tel:+<?= preg_replace('/[^0-9]/', '', Yii::$app->city->phone); ?>">
                      <img src="<?= $asset->baseUrl ?>/img/icon-top-tel.png" alt="">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
    <div class="main-slogan-block">
      <div class="container">
        <div class="header-desktop">
          <div class="logo">
            <a href="/"><img src="<?= $asset->baseUrl ?>/img/logo.png" alt="" width="244" height="40"></a>
          </div>
          <div class="set-city-box">
            <a data-toggle="modal" data-target="#set-city"><?= Yii::$app->city->name ?></a>
              <?php if (!Yii::$app->city->is_selected): ?>
                <div class="confirm-city">
                  <a class="basket-block-body-dell-item" data-slug="<?= Yii::$app->city->slug ?>" title="Закрыть"></a>
                  <span class="confirm-city__title">Выберите город, в котором хотите заказать рекламу</span>
                  <div class="confirm-city__block">
                    <button class="confirm-city__btn" data-toggle="modal" data-target="#set-city">
                      Выбрать город
                    </button>
                  </div>
                </div>
              <?php endif; ?>
          </div>
          <div class="slogan">
            <div class="blue-title"><?= Text::get(
                    'site-name',
                    'Рекламное агентство НИКО-медиа',
                    'string'
                ) ?></div>
            <div class="slogan-box">
              <span><?= Text::get('slogan-1', 'Честно', 'string') ?></span>
              <span><?= Text::get('slogan-2', 'Эффективно', 'string') ?></span>
              <span><?= Text::get('slogan-2', 'Внимательно!', 'string') ?></span>
            </div>
          </div>
          <div class="phone">
            <div class="phone-namber">
              <p><span><?= Yii::$app->city->phone ?></span></p>
            </div>
            <a class="callus-link" href="#callbackwidget">Перезвоните мне</a>
            <p class="desktop-mail">почта для связи: <a href="mailto:zakaz@niko-m.ru">zakaz@niko-m.ru</a></p>
          </div>
          <!-- Modal -->
          <div class="modal fade" id="set-city" tabindex="-1" role="dialog"
               ria-labelledby="zakaz-zvonka-title"
               aria-hidden="true"
               data-back_url="<?= urlencode($this->context->id == 'contacts' ? '/contacts' : null) ?>">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <div class="row">
                    <div class="col-md-5 col-xs-12">
                      <h3 class="modal-title" id="myModalLabel">Выберите свой город</h3>
                    </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="city-search">
                        <input type="text" id="set-city-search" placeholder="Название города">
                        <input type="submit" name="submit">
                      </div>
                    </div>
                  </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="close-set-city" aria-hidden="true"></span></button>
                </div>
                <div class="modal-body"></div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="feedback-form" tabindex="-1" role="dialog"
               aria-labelledby="feedback-form-title"
               aria-hidden="true">
            <div class="modal-dialog">
              <div class="form-header text-center">
                <a class="form-close" data-dismiss="modal" aria-hidden="true">
                  <span class="close-one"></span>
                  <span class="close-two"></span>
                </a>
                <h4 id="zakaz-zvonka-title">Написать нам</h4>
              </div>
              <div class="form-body"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="red-main-menu" id="main-menu" <?= $isMinifyMenu ? 'style="display:none"' : '' ?>>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 no-padding">
            <ul class="main-nav nav navbar-nav">
              <li><a href="/outdoor-ad">Наружная реклама</a></li>
              <li><a class="nav__link" href="/tv">Реклама на ТВ</a></li>
              <li><a class="nav__link" href="/telestroka">Бегущая строка на ТВ</a></li>
              <li><a class="nav__link" href="/radio">Реклама на радио</a></li>
              <li><a class="nav__link" href="/internet-ad">Интернет-реклама</a></li>
              <li class="main-nav__dropdown">
                <a class="nav__link nav__link--icon" href="/p/complex">Другое</a>
                <ul class="dropdown-list">
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/contacts">Контакты</a></li>
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/p/complex">Комплексная
                      реклама</a></li>
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/about">О компании</a></li>
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/cases">Рекламные кейсы</a></li>
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/reviews">Отзывы</a></li>
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/example">Примеры наших работ</a>
                  </li>
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/faq">Вопросы и ответы</a></li>
                  <li class="dropdown-menu__item"><a class="dropdown-menu__link" href="/cities">Реклама в городах</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php if ($isMinifyMenu) { ?>
    <div class="container">
      <div class="new-breadcrumbs-menu-wrapper">
        <?= $this->render('partial/breadcrumbs') ?>
        <div class="new-menu-wrapper">
          <div class="new-menu new-menu--dropdown">
            <span>
              МЕНЮ
              <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path d="M1.65094 4.19818C0.707547 4.19818 0 3.49063 0 2.54723C0 1.60384 0.707547 0.849121 1.65094 0.849121H18.3019C19.2453 0.849121 20 1.60384 20 2.54723C20 3.49063 19.2453 4.19818 18.3019 4.19818H1.65094Z"/>
                <path d="M18.3019 8.302C19.2453 8.302 20 9.05672 20 10.0001C20 10.9435 19.2453 11.6511 18.3019 11.6511H1.65094C0.707547 11.6511 0 10.9435 0 10.0001C0 9.05672 0.707547 8.302 1.65094 8.302H18.3019Z"/>
                <path d="M18.3019 15.802C19.2453 15.802 20 16.5095 20 17.4529C20 18.3963 19.2453 19.1511 18.3019 19.1511H1.65094C0.707547 19.1511 0 18.3963 0 17.4529C0 16.5095 0.707547 15.802 1.65094 15.802H18.3019Z"/>
              </svg>
            </span>
            <ul class="new-dropdown new-dropdown--main">
              <li><a href="/outdoor-ad">Наружная реклама</a></li>
              <li><a href="/tv">Реклама на ТВ</a></li>
              <li><a href="/telestroka">Бегущая строка на ТВ</a></li>
              <li><a href="/radio">Реклама на радио</a></li>
              <li><a href="/internet-ad">Интернет-реклама</a></li>
              <li class="new-menu--dropdown new-menu--dropdown-sub">
                <a href="/p/complex">Другое</a>
                <ul class="new-dropdown">
                  <li><a href="/contacts">Контакты</a></li>
                  <li><a href="/p/complex">Комплексная реклама</a></li>
                  <li><a href="/about">О компании</a></li>
                  <li><a href="/cases">Рекламные кейсы</a></li>
                  <li ><a href="/reviews">Отзывы</a></li>
                  <li><a href="/example">Примеры наших работ</a></li>
                  <li><a href="/faq">Вопросы и ответы</a></li>
                  <li><a href="/cities">Реклама в городах</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <?= $this->render('partial/heading') ?>
    </div>
    <?php } ?>
  </header>
  <div class="content-container tablet-container">
      <?= $this->render('/skidki2020/banner'); ?>
      <?php if ($showBreadcrumbs): ?>
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= $this->render('partial/breadcrumbs') ?>
                <?= $this->render('partial/heading') ?>
                <?php if (isset($this->params['subTitle'])): ?>
                  <p><?= $this->params['subTitle'] ?></p>
                <?php endif; ?>
            </div>
          </div>
        </div>
      <?php endif ?>
      <?php if (isset($this->params['showInnerMenu'])): ?>
        <div class="content-header-menu">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?= Menu::widget(
                      [
                          'items' => [
                              [
                                  'label' => 'О компании',
                                  'url' => ['/about'],
                                  'active' => $this->context->id == 'about',
                              ],
                              [
                                  'label' => 'Рекламные кейсы',
                                  'url' => ['/cases'],
                                  'active' => $this->context->id == 'cases',
                              ],
                              [
                                  'label' => 'Отзывы',
                                  'url' => ['/reviews'],
                                  'active' => $this->context->id == 'reviews',
                              ],
                              [
                                  'label' => 'Полезное',
                                  'url' => ['/info'],
                                  'active' => $this->context->id == 'info',
                              ],
                              [
                                  'label' => 'Книги',
                                  'url' => ['/book'],
                                  'active' => $this->context->id == 'book',
                              ],
                              [
                                  'label' => 'Наши клиенты',
                                  'url' => ['/customers'],
                                  'active' => $this->context->id == 'customers',
                              ],
                          ],
                      ]
                  ); ?>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <?= $content ?>
  </div>
  <div class="footer-menu-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
          <div class="row">
              <?= Menu::widget(
                  [
                      'items' => [
                          [
                              'label' => 'Наружная реклама',
                              'url' => ['/outdoor-ad'],
                              'active' => $this->context->id == 'outdoor-ad',
                          ],
                          [
                              'label' => 'Реклама на ТВ',
                              'url' => ['/tv'],
                              'active' => $this->context->id == 'tv',
                          ],
                          [
                              'label' => 'Бегущая строка на ТВ',
                              'url' => ['/telestroka'],
                              'active' => $this->context->id == 'ticker',
                          ],
                          [
                              'label' => 'Реклама на радио',
                              'url' => ['/radio'],
                              'active' => $this->context->id == 'radio',
                          ],
                          [
                              'label' => 'Комплексная реклама',
                              'url' => ['/p/complex'],
                              'active' => $_SERVER['REQUEST_URI'] == '/p/complex',
                          ],
                      ],
                      'options' => ['class' => 'nav navbar-nav'],
                      'itemOptions' => ['class' => 'col-lg-2-5 col-md-2-5 col-sm-4 hidden-xs'],
                  ]
              ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="mob-logo">
            <a href="/"><img src="<?= $asset->baseUrl ?>/img/logo.png" alt=""></a>
          </div>
          <div class="footer-contact" style="margin-top: 7px">

          </div>
          <div class="footer-contact" style="margin-top: 7px">
            <p>
              <a href="<?= $asset->baseUrl ?>/img/docs/Politika_obrabotki_PDn_niko.pdf" target="_blank">
                Политика обработки персональных данных
              </a>
            </p>
          </div>
          <div class="footer-contact" style="margin-top: 7px">
            <p>Не является публичной офертой</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
          <div class="footer-contact">
            <b><?= Yii::$app->city->phone ?></b>
              <?= Text::get('footer-contact', '<p>Сургут, ул. Киртбая, 11, этаж 4, офис 4</p>', 'text') ?>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="developer">
            <span>Разработка и продвижение</span>
            <a href="http://a1-reklama.ru/" target="_blank">
              <img src="<?= $asset->baseUrl ?>/img/a1-logo.png" alt="А1 Интернет Эксперт">
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>
</div>
<?= $this->render('partial/counters'); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
