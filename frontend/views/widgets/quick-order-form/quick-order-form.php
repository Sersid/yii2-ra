<div class="modal fade" id="order-format" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="form-header text-center">
            <a class="form-close" data-dismiss="modal" aria-hidden="true">
                <span class="close-one"></span>
                <span class="close-two"></span>
            </a>
            <h4>Оформить <br>заказ</h4>
        </div>
        <div class="form-body">
            <?= $this->render('/basket/quick-order-widget', ['model' => $model, 'outdoor' => true]) ?>
        </div>
    </div>
</div>
