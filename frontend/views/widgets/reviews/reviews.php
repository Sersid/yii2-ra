<?php

use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var ActiveDataProvider $dataProvider */
/* @var bool $showHeaderText */
?>
<div class="review-content-padding">
    <div class="gray-bg">
        <div class="container">
            <?php if ($showHeaderText) { ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-content-title" style="text-align: center">
                        <h2>Мы помогаем нашим клиентам<br>
                            зарабатывать больше</h2>
                        <div class="text-wrap">
                            <p>Основная наша задача заключается в том, чтобы реклама приносила вам прибыль: если вы
                                чувствуете финансовуб отдачу от проделанной нами работы, это значит что вы продолжите с
                                нами сотрудничество. Выгодно вам - вагодно нам. Для этого мы используем весь наш опыт и
                                компетенции, накопленные за 12 лет работы в рекламном бизнесе.</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row" id="reviews-block">
                <?php Pjax::begin() ?>
                <?= ListView::widget(
                    [
                        'dataProvider' => $dataProvider,
                        'itemView' => '_item',
                        'layout' => "{items}\n<div class=\"col-lg-12 col-md-12 hidden-sm hidden-xs\">{pager}</div>",
                        'emptyTextOptions' => ['class' => 'col-md-12'],
                        'pager' => [
                            'options' => ['class' => 'pagination-block'],
                            'nextPageLabel' => false,
                            'prevPageLabel' => false,
                            'disabledPageCssClass' => 'no-active',
                            'maxButtonCount' => 5,
                        ],
                    ]
                ); ?>
                <?php Pjax::end() ?>
            </div>
            <div class="col-lg-12 col-md-12 hidden-sm hidden-xs"></div>
        </div>
    </div>
</div>
