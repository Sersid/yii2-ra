<?php

/* @var yii\web\View $this */
/* @var common\models\Review $model */
?>
<div class="review-item">
    <div class="review-item-frame">
        <a class="fancybox" rel="review-3" href="<?= $model->image->getSrc(1000, null, 5) ?>">
            <img src="/img/review-frame.png" alt="<?= $model->name ?>">
            <div class="review-item-img" style="background: url(<?= $model->image->getSrc(340,
                426,
                5) ?>) no-repeat; background-position: center center; background-size: cover;">
            </div>
        </a>
    </div>
</div>
