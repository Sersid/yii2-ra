<div class="new-section steps wrapper__section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 complex-page-step-blocks">
                <h2 class="title text-center new-section__title">Схема работы</h2>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="step-number">1</div>
                    <span>Вам необходимо провести массовую рекламную кампанию, охватить как можно большую аудиторию или просто решить специфическую рекламную задачу</span>
                    <span class="complex-next-arrow-down hidden-sm hidden-xs"></span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="step-number">2</div>
                    <span>Вы обращаетесь к нам, рассказываете свои цели, задачи, ожидаемый результат</span>
                    <span class="complex-next-arrow-up hidden-sm hidden-xs"></span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="step-number">3</div>
                    <span>На основании полученной информации наши специалисты с опытом работы в проведении рекламных кампаний разрабатывают для вас готовое предложение</span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="step-number">4</div>
                    <span>Совместно с вами мы обсуждаем подготовленный план, при необходимости вносим коррективы и дополняем его</span>
                    <span class="complex-next-arrow-down hidden-sm hidden-xs"></span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="step-number">5</div>
                    <span>После согласования всех деталей мы запускаем производственный процесс и рекламную кампанию</span>
                    <span class="complex-next-arrow-up hidden-sm hidden-xs"></span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="step-number">6</div>
                    <span>К концу отчетного периода мы подводим итоги рекламной кампании, ее оценку и эффективность</span>
                </div>
            </div>
        </div>
    </div>
</div>
