<?php

/* @var yii\web\View $this */
/* @var common\models\Review $review */
/* @var array $faq */

$this->title = "Ответы на часто задаваемые вопросы";
$this->params['breadcrumbs'][] = "Ответы на вопросы";
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="accordion" class="faq-container">
                <?php foreach ($faq as $i => $f):?>
                <div class="quest-box panel">
                    <h4>
                        <a data-toggle="collapse" data-parent="#accordion" href="#quest-<?=$f->id?>" <?=$i > 0 ? 'class="collapsed"' : ''?>>
                            <div class="quest-title">
                                <span><?=$f->question?></span>
                            </div>
                        </a>
                    </h4>
                    <div id="quest-<?=$f->id?>" class="panel-collapse collapse <?=$i == 0 ? 'in' : ''?>">
                        <div class="answ-text">
                            <?=$f->answer?>
                        </div>
                    </div>
                </div>
                <?php endforeach?>
            </div>
        </div>
    </div>
</div>
