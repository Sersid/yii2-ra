<?php

/* @var yii\web\View $this */
/* @var string $name */
/* @var string $message */
/* @var Exception $exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>

            <p>
                При обработки вашего запроса произошла ошибка.
            </p>
            <p>
                Пожалуйста, свяжитесь с нами, если вы думаете, что это ошибка сервера. Спасибо.
            </p>
        </div>
    </div>
</div>
