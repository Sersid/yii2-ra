<?php

/* @var yii\web\View $this */
/* @var yii\bootstrap\ActiveForm $form */
/* @var LoginForm $model */

use common\models\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <p>Для продолжения, необходимо авторизоваться:</p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

<!--                <div style="color:#999;margin:1em 0">-->
<!--                    If you forgot your password you can -->
            <?php //= Html::a('reset it', ['site/request-password-reset']) ?><!--.-->
<!--                </div>-->
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<br/><br/><br/><br/><br/>
