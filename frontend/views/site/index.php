<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use frontend\widgets\Text;

/* @var yii\web\View $this */

$this->title = "Рекламное агенство НИКО-медиа";
?>
<div class="content-container canvas-wrapper">
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="content-header">
                    <div class="main-h1 blue-title"><?=Text::get('index-title', 'Выберите рекламу, которую хотите заказать', 'string')?></div>
                    <div class="info-text"><?=Text::get('index-info-text', 'Работаем на территории всей Тюменской области <br>от Тюмени до Салехарда без посредников', 'text')?></div>
                </div>
                <div class="content-block row">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <div class="content-item hover-item">
                            <a href="<?=Url::to(['/outdoor-ad'])?>">
                                <div class="item-img">
                                    <img src="/img/yslygi-1.png" alt="">
                                </div>
                                <div class="item-title">Наружная реклама</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <div class="content-item hover-item">
                            <a href="<?=Url::to(['/tv'])?>">
                                <div class="item-img">
                                    <img src="/img/yslygi-2.png" alt="">
                                </div>
                                <div class="item-title">Реклама на ТВ</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <div class="content-item hover-item">
                            <a href="<?=Url::to(['/ticker'])?>">
                                <div class="item-img">
                                    <img src="/img/yslygi-4.png" alt="">
                                </div>
                                <div class="item-title">Бегущая строка на ТВ</div>
                            </a>
                        </div>
                    </div>
					<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <div class="content-item hover-item">
                            <a href="<?=Url::to(['/radio'])?>">
                                <div class="item-img">
                                    <img src="/img/yslygi-3.png" alt="">
                                </div>
                                <div class="item-title">Реклама на радио</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <div class="content-item hover-item">
                            <a href="<?=Url::to(['/p/complex'])?>">
                                <div class="item-img">
                                    <img src="/img/yslygi-5.png" alt="">
                                </div>
                                <div class="item-title">Комплексная реклама</div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <div class="content-item hover-item">
                            <a href="<?=Url::to(['/internet-ad'])?>">
                                <div class="item-img">
                                    <img src="/img/int-reklama.png" alt="">
                                </div>
                                <div class="item-title">Интернет-реклама</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="main-h2 white-title"><?=Text::get('index-white-title', 'Рекламное агентство Niko Media', 'string')?></h2>
                <p><?=Text::get('index-white-text', 'Агентство маркетинговых и рекламных коммуникаций "НИКО-медиа" специализируется на размещении рекламы, разработке и проведении комплексных рекламных кампаний в Тюменской области.', 'text')?></p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?=Text::get('counter-cases', '100', 'string')?><span>+</span>
                        </div>
                        <div class="right-box">
                            <?=Text::get('title-cases', 'ПРОВЕДЕННЫХ РЕКЛАМНЫХ КаМПАНИЙ', 'string')?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?=Url::to(['/cases'])?>">Примеры успешных рекламных компаний в городах</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?=Text::get('counter-cities', '27', 'string')?>
                        </div>
                        <div class="right-box">
                            <?=Text::get('title-cities', 'Городов и населенных пунктов', 'string')?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?=Url::to(['/cities'])?>">Сургут, Нижневартовск, Новый Уренгой, Ханты-Мансийск...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?=Text::get('counter-outdoor-ads', '980', 'string')?><span>+</span>
                        </div>
                        <div class="right-box">
                            <?=Text::get('title-outdoor-ads', 'Рекламных Поверхностей', 'string')?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?=Url::to(['/outdoor-ad', 'view' => 'map'])?>">Смотреть карту рекламных поверхностей в городах</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 about-box">
                <div class="about-block">
                    <div class="about-block-top">
                        <div class="left-box red-title">
                            <?=Text::get('counter-years', '10', 'string')?><span>+</span>
                        </div>
                        <div class="right-box">
                            <?=Text::get('title-years', 'Лет на рынке ХМАО и ЯНАО', 'string')?>
                        </div>
                    </div>
                    <div class="about-block-bottom">
                        <a href="<?=Url::to(['/about'])?>">О компании</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <a class="yellow-btn" href="#" data-toggle="modal" data-target="#feedback-form">Связаться с нами</a>
            </div>
        </div>
    </div>
</div>
<?php if(!empty($customers)):?>
    <div class="klientu-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="main-h1 blue-title"><?=Text::get('title-customers', 'Наши клиенты', 'string')?></h2>
                </div>
                <div class="klientu-block">
                    <?php foreach ($customers as $customer):?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="item-img">
                                <img title="<?=$customer->name?>" src="<?=$customer->image->getSrc(300)?>" alt="<?=$customer->name?>">
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <a class="link-all-item" href="<?=Url::to(['/customers'])?>">Все наши клиенты</a>
                </div>
            </div>

        </div>
    </div>
<?php endif;?>
<div class="container seo-content" style="margin-bottom: 50px">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
            <div class="text-widget"><?=Text::get('index-after-customers')?></div>
        </div>
    </div>
</div>
<div class="form-main-container">
    <div class="container">
        <div class="form-bg form-shake">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 white-bg">
                    <div class="form-block">
                        <div class="main-h2"><?=Text::get('title-form-main', 'Вам нужны новые клиенты?', 'string')?></div>
                        <p><?=Text::get('description-form-main', 'Давайте обсудим Ваши задачи. И сделаем их', 'string')?></p>
                        <?php Pjax::begin(['enablePushState' => false]) ?>
                        <?= $this->render('/forms/feedback', ['model' => new common\models\FormFeedback()])?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
                <?= $this->render('/forms/girl')?>
            </div>
        </div>
    </div>
</div>
