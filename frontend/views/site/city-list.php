<?php
declare(strict_types=1);

use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use yii\helpers\Html;

/** @var RegionViewCollection $regions */
?>
<?php if ($regions->isEmpty()) {
    ?><p>Город не найден</p><?php
} else {
    ?>
  <div class="row">
      <?php foreach ($regions as $region) { ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <strong><?= $region ?></strong>
          <ul class="list-unstyled">
              <?php foreach ($region->cities as $city) { ?>
                <li><?= Html::a(
                        $city->name,
                        [
                            '/site/set-city',
                            'slug' => $city->slug,
                            'back_url' => Yii::$app->request->get('back_url'),
                        ]
                    )
                    ?></li>
              <?php } ?>
          </ul>
        </div>
      <?php } ?>
  </div>
    <?php
}
