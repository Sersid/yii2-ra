<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class MomentAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/moment';

    public $js = [
        'moment.js',
        'locale/ru.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}