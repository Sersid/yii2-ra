<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class YandexMapAsset
 * @package frontend\assets
 */
class YandexMapAsset extends AssetBundle
{
    public $js = [
        'https://api-maps.yandex.ru/2.1.47/?lang=ru_RU',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
