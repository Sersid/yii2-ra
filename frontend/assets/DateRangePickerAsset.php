<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class DateRangePickerAsset
 * @package frontend\assets
 */
class DateRangePickerAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/bootstrap-daterangepicker';

    public $js = [
        'daterangepicker.js',
    ];

    public $css = [
        'daterangepicker.css',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\MomentAsset',
    ];
}
