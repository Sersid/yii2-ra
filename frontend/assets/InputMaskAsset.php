<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class InputMaskAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@vendor/robinherbots/jquery.inputmask/dist';

    public $js = [
        'jquery.inputmask.min.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
