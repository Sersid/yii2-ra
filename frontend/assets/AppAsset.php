<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $css = [
        'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        'css/mobile-first.css',
        'css/tablet.css',
        'css/desktop.css',
        'css/fonts.css',
        'css/fm.revealator.jquery.css',
        'css/owl.carousel.css',
        'css/owl.theme.default.css',
        'css/developer.css',
        'css/dev.css',
    ];
    public $js = [
        'js/jquery.viewportchecker.min.js',
        'js/fm.revealator.jquery.min.js',
        'js/multiselect.js',
        'js/owl.carousel.min.js',
        'js/script.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'newerton\fancybox\FancyBoxAsset',
        'frontend\assets\InputMaskAsset',
    ];
}
