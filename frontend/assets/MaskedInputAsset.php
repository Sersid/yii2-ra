<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class MaskedInputAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/jquery.maskedinput/dist';

    public $js = [
        'jquery.maskedinput.min.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}