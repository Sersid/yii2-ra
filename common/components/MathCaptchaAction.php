<?php

namespace common\components;

use yii\captcha\CaptchaAction;

/**
 * Class MathCaptchaAction
 * @package common\components
 */
class MathCaptchaAction extends CaptchaAction
{
    public $minLength = 0;
    public $maxLength = 50;

    /**
     * @inheritdoc
     */
    public function generateVerifyCode()
    {
        return mt_rand((int)$this->minLength, (int)$this->maxLength);
    }

    /**
     * @inheritdoc
     */
    public function renderImage($code)
    {
        return parent::renderImage($this->getText($code));
    }

    /**
     * @param $code
     *
     * @return string
     */
    protected function getText($code)
    {
        $code = (int)$code;
        $rand = mt_rand(min(1, $code - 1), max(1, $code - 1));
        $operation = mt_rand(0, 1);
        if ($operation === 1) {
            return $code - $rand . '+' . $rand;
        } else {
            return $code + $rand . '-' . $rand;
        }
    }
}
