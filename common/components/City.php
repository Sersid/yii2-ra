<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;

class City extends Component
{
    /**
     * City id
     * @var integer
     */
    public $id;

    /**
     * City name
     * @var string
     */
    public $name;

    /**
     * City slug
     * @var string
     */
    public $slug;

    /**
     * City slug
     * @var string
     */
    public $phone;

    /**
     * City where
     * @var string
     */
    public $where;

    /**
     * Is selected
     * @var bool
     */
    public $is_selected = false;

    /**
     * Requisites file url
     * @var string
     */
    public $requisitesFileUrl;

    /**
     * Requisites file size
     * @var integer
     */
    public $requisitesFileSize;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $requisitesFile = '';
        if ($this->id === null) {
            $city_id = Yii::$app->session->get('city_id');
            if ($city_id != null) {
                $this->id = $city_id;
                $model = \common\models\City::find()->where(['id' => $city_id])->one();
                $this->id = $model->id;
                $this->name = $model->name;
                $this->slug = $model->slug;
                $this->phone = $model->getPhone();
                $this->where = $model->where;
                $this->is_selected = true;
                $requisitesFile = $model->requisitesFile;
            } else {
                $this->phone = Yii::$app->params['default_phone'];
                $this->name = 'Город не выбран';
                $this->is_selected = Yii::$app->session->get('close_city_box', false);
            }
        }
        if (empty($requisitesFile)) {
            $requisitesFile = Yii::$app->params['default_requisites'];
        }

        $this->requisitesFileUrl = Url::to(Yii::$app->params['requisites_web_path'] . $requisitesFile);
        $filePath = Yii::getAlias(Yii::$app->params['requisites_path'] . $requisitesFile);
        if (is_file($filePath)) {
            $size = filesize(Yii::getAlias(Yii::$app->params['requisites_path'] . $requisitesFile));
            $units = ['б', 'кб', 'мб', 'гб'];
            $power = $size > 0 ? floor(log($size, 1024)) : 0;
            $this->requisitesFileSize = number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
        }

        $enableCallTracking = Yii::$app->config->get('enableCallTracking');
        $phoneCallTracking = Yii::$app->config->get('phoneCallTracking');
        if ($enableCallTracking && !empty($phoneCallTracking)) {
            $this->phone = $phoneCallTracking;
        }
    }
}
