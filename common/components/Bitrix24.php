<?php

namespace common\components;

use Yii;
use yii\base\Component;

class Bitrix24 extends Component
{
    /** @var string id пользователя */
    public $user_id;

    /** @var string вебхук */
    public $web_hook;

    /**
     * Domain
     * @var string
     */
    public $domain;

    /**
     * Title
     * @var string
     */
    public $title;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * Phone
     * @var string
     */
    public $phone;

    /**
     * Comments
     * @var string
     */
    public $comments;
    /**
     * utm_source
     * @var string
     */
    public $utm_source;

    /**
     * utm_medium
     * @var string
     */
    public $utm_medium;

    /**
     * utm_campaign
     * @var string
     */
    public $utm_campaign;

    /**
     * utm_content
     * @var string
     */
    public $utm_content;

    /**
     * utm_term
     * @var string
     */
    public $utm_term;

    /**
     * @return mixed
     */
    public function addLead()
    {
        // формируем URL в переменной $queryUrl
        $queryUrl = 'https://' . $this->domain . '/rest/' . $this->user_id . '/' . $this->web_hook . '/crm.lead.add.json';
        $query = http_build_query(
            [
                'fields' => [
                    'TITLE' => $this->title,
                    'EMAIL' => [
                        "n0" => [
                            "VALUE" => $this->email,
                            "VALUE_TYPE" => "WORK",
                        ],
                    ],
                    'PHONE' => [
                        "n0" => [
                            "VALUE" => $this->phone,
                            "VALUE_TYPE" => "MOBILE",
                        ],
                    ],
                    'NAME' => $this->name,
                    'COMMENTS' => $this->comments,
                    'SOURCE_ID' => "WEB",
                    'UTM_CAMPAIGN' => $this->utm_campaign,
                    'UTM_CONTENT' => $this->utm_content,
                    'UTM_MEDIUM' => $this->utm_medium,
                    'UTM_SOURCE' => $this->utm_source,
                    'UTM_TERM' => $this->utm_term,
                ],
                'params' => ["REGISTER_SONET_EVENT" => "Y"],
            ]
        );
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $query,
            ]
        );
        $server_output = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($server_output, true);

        return $data;
    }

    /**
     * Инициализация компонента
     */
    public function init()
    {
        if (!empty((string)Yii::$app->request->get('utm_source'))) {
            $arUtm = [
                'utm_source' => (string)Yii::$app->request->get('utm_source'),
                'utm_medium' => (string)Yii::$app->request->get('utm_medium'),
                'utm_campaign' => (string)Yii::$app->request->get('utm_campaign'),
                'utm_content' => (string)Yii::$app->request->get('utm_content'),
                'utm_term' => (string)Yii::$app->request->get('utm_term'),
            ];
            Yii::$app->session->set('utm', $arUtm);
        } else {
            $arUtm = Yii::$app->session->get('utm', []);
        }
        $this->utm_source = $arUtm['utm_source'] ?? null;
        $this->utm_medium = $arUtm['utm_medium'] ?? null;
        $this->utm_campaign = $arUtm['utm_campaign'] ?? null;
        $this->utm_content = $arUtm['utm_content'] ?? null;
        $this->utm_term = $arUtm['utm_term'] ?? null;
    }
}
