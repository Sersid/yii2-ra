<?php

namespace common\components;

use common\models\Employee;
use yii\db\Expression;
use Yii;

/**
 * Class Girl
 * @package common\components
 */
class Girl extends \yii\base\Component
{
    /**
     * Girl name
     * @var string
     */
    public $name;

    /**
     * Girl photo
     * @var string
     */
    public $photo;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $last = Yii::$app->session->get('last_employee');
        if ($last === null) {
            $last = 0;
        }
        /** @var Employee $employee */
        $employee = Employee::find()->joinWith('photo')->where(['!=', 'employee.id', $last])->orderBy(new Expression('rand()'))->one();
        if (!$employee) {
            $employee = Employee::find()->joinWith('photo')->one();
        }
        if ($employee) {
            $this->name = $employee->name;
            $this->photo = $employee->photo->getSrc();
            Yii::$app->session->set('last_employee', $employee->id);
        }
    }
}
