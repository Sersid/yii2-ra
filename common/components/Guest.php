<?php
/**
 * Created by PhpStorm.
 * User: Sers
 * Date: 28.11.2014
 * Time: 5:47
 */

namespace common\components;

use Yii;

class Guest extends \yii\base\Component
{
    /**
     * Guest id
     * @var integer
     */
    public $id;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->id === null) {
            $guest_id = Yii::$app->session->get('guest_id');
            if ($guest_id == null) {
                $model = $this->_new_guest();
                $this->id = $model->id;
            } else {
                $model = \common\models\Guest::findOne($guest_id);
                if (empty($model)) {
                    $model = $this->_new_guest();
                } else {
                    $model->updated_at = time();
                    $model->save(false);
                }
                $this->id = (int)$model->id;
            }
        }
    }

    /**
     * Add new guest
     * @return \common\models\Guest
     */
    private function _new_guest()
    {
        $model = new \common\models\Guest();
        $model->save(false);
        Yii::$app->session->set('guest_id', $model->id);
        return $model;
    }
}
