<?php

namespace common\repositories;

use common\models\Ticker as Model;

/**
 * Class TickerRepository
 * @package common\repositories
 */
class TickerRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
