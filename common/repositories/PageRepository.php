<?php

namespace common\repositories;

use common\models\Page as Model;

/**
 * Class PageRepository
 * @package common\repositories
 */
class PageRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
