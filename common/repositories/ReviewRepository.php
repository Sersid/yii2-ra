<?php

namespace common\repositories;

use common\models\Review as Model;
use yii\db\ActiveQuery;

/**
 * Class ReviewRepository
 * @package common\repositories
 */
class ReviewRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
