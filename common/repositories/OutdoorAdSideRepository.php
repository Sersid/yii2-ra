<?php

namespace common\repositories;

use common\models\OutdoorAdSide as Model;

/**
 * Class OutdoorAdSideRepository
 * @package common\repositories
 */
class OutdoorAdSideRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
