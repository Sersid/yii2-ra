<?php

namespace common\repositories;

use common\models\Info as Model;
use yii\db\ActiveQuery;

/**
 * Class InfoRepository
 * @package common\repositories
 */
class InfoRepository extends BaseRepository
{
        /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
