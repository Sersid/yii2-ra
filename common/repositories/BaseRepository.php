<?php

namespace common\repositories;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class BaseRepository
 * @package common\repositories
 */
abstract class BaseRepository
{
    /**
     * @return string
     */
    abstract protected function getModelClass(): string;

    /**
     * @return ActiveQuery
     */
    public function query()
    {
        $queryClass = $this->getQueryClass();
        return new $queryClass($this->getModelClass());
    }

    /**
     * @return string
     */
    public function getQueryClass(): string
    {
        return ActiveQuery::class;
    }

    /**
     * @return string
     */
    public function tableName(): string
    {
        /** @var ActiveRecord $modelClass */
        $modelClass = $this->getModelClass();
        return $modelClass::tableName();
    }
}
