<?php

namespace common\repositories;

use frontend\models\City as Model;

/**
 * Class CityRepository
 * @package common\repositories
 */
class CityRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
