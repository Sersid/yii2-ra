<?php

namespace common\repositories;

use common\models\Tv as Model;

/**
 * Class TvRepository
 * @package common\repositories
 */
class TvRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
