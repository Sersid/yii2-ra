<?php

namespace common\repositories;

use common\models\TvAd as Model;

/**
 * Class TvRepository
 * @package common\repositories
 */
class TvAdRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
