<?php

namespace common\repositories;

use common\models\RadioAd as Model;

/**
 * Class RadioAdRepository
 * @package common\repositories
 */
class RadioAdRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
