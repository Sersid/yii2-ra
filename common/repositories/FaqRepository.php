<?php

namespace common\repositories;

use common\models\Faq as Model;

/**
 * Class FaqRepository
 * @package common\repositories
 */
class FaqRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
