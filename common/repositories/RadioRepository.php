<?php
declare(strict_types=1);

namespace common\repositories;

use common\models\Radio as Model;

/**
 * Class RadioRepository
 * @package common\repositories
 */
class RadioRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
