<?php

namespace common\repositories;

use common\models\InfoCategory as Model;

/**
 * Class InfoCategoryRepository
 * @package common\repositories
 */
class InfoCategoryRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
