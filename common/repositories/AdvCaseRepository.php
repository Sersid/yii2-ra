<?php

namespace common\repositories;

use frontend\models\AdvCase as Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class AdvCaseRepository
 * @package common\repositories
 */
class AdvCaseRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
