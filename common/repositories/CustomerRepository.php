<?php

namespace common\repositories;

use common\models\Customer as Model;

/**
 * Class CustomerRepository
 * @package common\repositories
 */
class CustomerRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
