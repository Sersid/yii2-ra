<?php
declare(strict_types=1);

use NikoM\Advertising\Internet\FAQ\Domain\Entity\IFaqRepository;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\IFaqFetcher;
use NikoM\Advertising\Internet\FAQ\Infrastructure\FaqFetcher;
use NikoM\Advertising\Internet\FAQ\Infrastructure\FaqRepository;

return [
    'language' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'image/<id:\d+>/<code:\w{32}+>.<ext:\w{2,10}+>' => 'file/render',
                'image/<id:\d+>/<params:\w+>/<code:\w{32}+>.<ext:\w{2,10}+>' => 'file/render',
                'telestroka' => 'ticker/index',
                'telestroka/<city:\w+>' => 'ticker/index',
            ],
        ],
        'file' => [
            'class' => 'sersid\fileupload\File',
            'model' => 'common\models\File',
            'dir' => '@files',
        ],
        'image' => [
            'class' => 'yii\image\ImageDriver',
        ],
        'guest' => [
            'class' => 'common\components\Guest',
        ],
        'config' => [
            'class' => 'sersid\config\components\Config',
        ],
        'city' => [
            'class' => 'common\components\City',
        ],
        'girl' => [
            'class' => 'common\components\Girl',
        ],
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV3' => '6Ld3sPAZAAAAALnF7no7JJ9vgfLASOn0xzXJdHIm',
            'secretV3' => '6Ld3sPAZAAAAAL4VvHO4XXuFRKQwRn48FvQ_lreE',
        ],
        'bitrix24' => [
            'class' => 'common\components\Bitrix24',
            'domain' => 'nikomedia.bitrix24.ru',
            'user_id' => '1',
            'web_hook' => 'gthu9op7te7t8eo4',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
    ],
    'container' => [
        'definitions' => [
            IFaqRepository::class => FaqRepository::class,
            IFaqFetcher::class => FaqFetcher::class,
        ],
    ],
];
