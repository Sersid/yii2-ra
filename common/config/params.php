<?php
return [
    'adminEmail' => ['oleg-86@ya.ru', 'zakaz@niko-m.ru'], // sersid_bro@mail.ru imperil@yandex.ru
    'supportEmail' => 'zakaz@niko-m.ru',
    'supportName' => 'Zakaz',
    'user.passwordResetTokenExpire' => 3600,
    'client_id' => '57305fa64f1941cfb824cf8e5fd68952',
    'client_secret' => '488b4b9934134b71b636de5f0296391b',
    'default_phone' => '+7 (3462) 550-877',
    'girls' => [
        [
            'name' => 'Алина',
            'photo' => '/img/form-girl-2.png',
        ],
        [
            'name' => 'Александра',
            'photo' => '/img/form-girl-3.png',
        ],
    ],
    'requisites_path' => '@frontend/web/requisites/',
    'requisites_web_path' => '@web/requisites/',
    'default_requisites' => 'requisites.doc',
];
