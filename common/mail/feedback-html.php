<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FormFeedback */

?>
<p>Обратная связь:</p>
<br>
<?if(!empty($model->name)):?>
    <p><strong>Имя:</strong> <?=$model->name?></p>
<?endif;?>
<?if(!empty($model->phone)):?>
    <p><strong>Номер телефона:</strong> <?=$model->phone?></p>
<?endif;?>
<?if(!empty($model->email)):?>
    <p><strong>E-mail:</strong> <?=$model->email?></p>
<?endif;?>
<?if(!empty($model->message)):?>
    <p><strong>Сообщение:</strong> <?=$model->message?></p>
<?endif;?>

