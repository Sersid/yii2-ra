<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\FormCallMe */

?>
Поступил заказ:

Номер: <?=$model->getNumber()?>
<?if(!empty($model->name)):?>
    Имя: <?=$model->name?>
<?endif;?>
<?if(!empty($model->phone)):?>
    Номер телефона: <?=$model->phone?>
<?endif;?>
<?if(!empty($model->email)):?>
    E-mail: <?=$model->email?>
<?endif;?>

Для детального просмотра перейдите по ссылке:
<?=Url::to(['/admin/order', 'id' => $model->id], true)?>