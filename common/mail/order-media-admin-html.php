<?php
declare(strict_types=1);

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;
use yii\helpers\Url;

/** @var Order $order */
$basket = $order->getBasket();
$total = $basket->getTotal();
?>
<p>Поступил заказ "<?= $order->getType()->getName() ?>":</p>
<br>
<p>
    <strong>Номер заказа:</strong>
    <a href="<?= Url::to(['/admin/order', 'id' => $order->getId()->getValue()], true) ?>"><?= $order->getNumber() ?></a>
</p>
<?php if (!empty($order->getName())) { ?>
    <p>
        <strong>Имя:</strong>
        <?= $order->getName() ?>
    </p>
<?php } ?>
<?php if (!empty($order->getPhone())) { ?>
    <p>
        <strong>Номер телефона:</strong>
        <a href="tel:<?= $order->getPhone() ?>"><?= $order->getPhone() ?></a>
    </p>
<?php } ?>
<?php if (!empty($order->getEmail())) { ?>
    <p>
        <strong>E-mail:</strong>
        <a href="mailto:<?= $order->getEmail() ?>"><?= $order->getEmail() ?></a>
    </p>
<?php } ?>
<p>
    <strong>Тип рекламы:</strong>
    <?= $basket->getAdvType()->getName() ?>
</p>
<table>
    <thead>
    <tr>
        <th>Предложение</th>
        <th>Длительность <br />ролика в сек.</th>
        <th>Кол-во прокатов <br />ролика в день</th>
        <th>Кол-во дней <br />проката ролика</th>
        <th>Цена <br />без скидки</th>
        <th>Цена <br />со скидкой</th>
        <th>Выгода</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($basket->getItemCollection()->all() as $basketItem) { ?>
        <tr>
            <td><?= $basketItem->getOffer()->name() ?></td>
            <?php if (!$basket->getAdvType()->mustCalculate() || !$basketItem->getOffer()->hasPricePerSec()) { ?>
                <td colspan="6">Необходимо уточнить детали</td>
            <?php } else { ?>
                <td><?= $basketItem->getDuration()->isNotNull() ? $basketItem->getDuration() . ' сек.' : '-' ?></td>
                <td><?= $basketItem->getViewsPerDay()->isNotNull() ? $basketItem->getViewsPerDay() . ' раз' : '-' ?></td>
                <td><?= $basketItem->getDays()->isNotNull() ? $basketItem->getDays() . ' дней' : '-' ?></td>
                <td><?= $basketItem->getPriceWithoutDiscount()->isNotNull() ? $basketItem->getPriceWithoutDiscount()->getFormatted() . ' руб.' : '-' ?></td>
                <td><?= $basketItem->getPrice()->isNotNull() ? $basketItem->getPrice()->getFormatted() . ' руб.' : '-' ?></td>
                <td><?= $basketItem->getProfit()->isNotNull() ? $basketItem->getProfit()->getFormatted() . ' руб.' : '-' ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <td>Итог</td>
        <?php if (!$basket->getAdvType()->mustCalculate()) { ?>
            <td colspan="6">-</td>
        <?php } else { ?>
            <td><?= $total->getDuration()->isNotNull() ? $total->getDuration() . ' сек.' : '-'; ?></td>
            <td><?= $total->getViewsPerDay()->isNotNull() ? $total->getViewsPerDay() . ' раз' : '-'; ?></td>
            <td><?= $total->getDays()->isNotNull() ? $total->getDays() . ' дней' : '-'; ?></td>
            <td><?= $total->getPriceWithoutDiscount()->isNotNull() ? $total->getPriceWithoutDiscount()->getFormatted() . ' руб.' : '-'; ?></td>
            <td><?= $total->getPrice()->isNotNull() ? $total->getPrice()->getFormatted() . ' руб.' : '-'; ?></td>
            <td><?= $total->getProfit()->isNotNull() ? $total->getProfit()->getFormatted() . ' руб.' : '-'; ?></td>
        <?php } ?>
    </tr>
    </tfoot>
</table>
