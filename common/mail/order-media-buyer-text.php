<?php
declare(strict_types=1);

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;

/** @var Order $order */
$basket = $order->getBasket();
$total = $basket->getTotal();
if (!empty($order->getName())) {
    echo "Уважаемый " . $order->getName() . "!";
    echo "\n";
}
echo "Ваш заказ принят, и сейчас находится в обработке. Мы свяжемся с Вами в ближайшее время.";
echo "\n";
echo "\n";
echo "Перечень заказа №" . $order->getNumber() . ":";
echo "\n";
echo "\n";
echo $order->getType()->getOfferName();
echo " | ";
echo "Длительность ролика в сек.";
echo " | ";
echo "Кол-во прокатов ролика в день";
echo " | ";
echo "Кол-во дней проката ролика";
echo " | ";
echo "Итого";
echo "\n";
foreach ($basket->getItemCollection()->all() as $basketItem) {
    echo $basketItem->getOffer()->name();
    echo " | ";
    if (!$basket->getAdvType()->mustCalculate() || !$basketItem->getOffer()->hasPricePerSec()) {
        echo "Стоимость будет известна после уточнения всех деталей";
    } else {
        echo $basketItem->getDuration()->isNotNull() ? $basketItem->getDuration() . ' сек.' : '-';
        echo " | ";
        echo $basketItem->getViewsPerDay()->isNotNull() ? $basketItem->getViewsPerDay() . ' раз' : '-';
        echo " | ";
        echo $basketItem->getDays()->isNotNull() ? $basketItem->getDays() . ' дней' : '-';
        echo " | ";
        echo $basketItem->getPriceWithoutDiscount()->isNotNull() ? "Старая цена " . $basketItem->getPriceWithoutDiscount()->getFormatted() . ' руб. ' : '';
        echo $basketItem->getPrice()->isNotNull() ? "Цена " . $basketItem->getPrice()->getFormatted() . ' руб.' : '-';
        echo $basketItem->getProfit()->isNotNull() ? ' Ваша выгода ' . $basketItem->getProfit()->getFormatted() . ' руб.' : '';
        echo "\n";
    }
}
echo "\n";
echo "Итого";
echo " | ";
if (!$basket->getAdvType()->mustCalculate()) {
    echo "-";
} else {
    echo $total->getDuration()->isNotNull() ? $total->getDuration() . ' сек.' : '-';
    echo " | ";
    echo $total->getViewsPerDay()->isNotNull() ? $total->getViewsPerDay() . ' раз' : '-';
    echo " | ";
    echo $total->getDays()->isNotNull() ? $total->getDays() . ' дней' : '-';
    echo " | ";
    echo $total->getPriceWithoutDiscount()->isNotNull() ? "Старая цена " . $total->getPriceWithoutDiscount()->getFormatted() . ' руб. ' : '';
    echo $total->getPrice()->isNotNull() ? "Цена " . $total->getPrice()->getFormatted() . ' руб.' : '-';
    echo $total->getProfit()->isNotNull() ? ' Ваша выгода ' . $total->getProfit()->getFormatted() . ' руб.' : '';
} ?>


ВНИМАНИЕ! По всем вопросам, связанным с оказанием услуг, обращайтесь в службу поддержки Niko-Media по телефону +7 (3462) 550-877 или через электронную почту reklama@niko-m.ru
