<?php
declare(strict_types=1);

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;

/** @var Order $order */
$basket = $order->getBasket();
$total = $basket->getTotal();
?>
<?php if (!empty($order->getName())) { ?>
  <p>Уважаемый <?= $order->getName() ?>!</p>
<?php } ?>
<p>Ваш заказ принят, и сейчас находится в обработке. Мы свяжемся с Вами в ближайшее время.</p>
<p style="font-size:18px;"><strong>Перечень заказа №<?= $order->getNumber() ?>:</strong></p>

<table border="1" style="border-spacing:0; border-collapse: collapse;">
  <thead>
  <tr>
    <th style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $order->getType()->getOfferName() ?></th>
    <th style="padding:3px 10px; font-size:14px; border-collapse: collapse;">Длительность <br />ролика в сек.	</th>
    <th style="padding:3px 10px; font-size:14px; border-collapse: collapse;">Кол-во прокатов <br />ролика в день</th>
    <th style="padding:3px 10px; font-size:14px; border-collapse: collapse;">Кол-во дней <br />проката ролика</th>
    <th style="padding:3px 10px; font-size:14px; border-collapse: collapse;">Итого</th>
  </tr>
  </thead>
  <tbody>
  <tr>
      <?php foreach ($basket->getItemCollection()->all() as $basketItem) { ?>
  <tr>
    <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $basketItem->getOffer()->name() ?></td>
      <?php if (!$basket->getAdvType()->mustCalculate() || !$basketItem->getOffer()->hasPricePerSec()) { ?>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse; text-align: center" colspan="6">Стоимость будет известна после уточнения всех деталей</td>
      <?php } else { ?>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $basketItem->getDuration()->isNotNull() ? $basketItem->getDuration() . ' сек.' : '-' ?></td>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $basketItem->getViewsPerDay()->isNotNull() ? $basketItem->getViewsPerDay() . ' раз' : '-' ?></td>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $basketItem->getDays()->isNotNull() ? $basketItem->getDays() . ' дней' : '-' ?></td>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;">
            <?= $basketItem->getPriceWithoutDiscount()->isNotNull() ? '<strike>' . $basketItem->getPriceWithoutDiscount()->getFormatted() . ' руб.</strike><br />' : '' ?>
          <strong><?= $basketItem->getPrice()->isNotNull() ? $basketItem->getPrice()->getFormatted() . ' руб.' : '-' ?></strong>
            <?= $basketItem->getProfit()->isNotNull() ? '<br />Ваша выгода ' . $basketItem->getProfit()->getFormatted() . ' руб.' : '' ?>
        </td>
      <?php } ?>
  </tr>
  <?php } ?>
  </tr>
  </tbody>
  <tfoot>
  <tr>
    <td><strong>Итог</strong></td>
      <?php if (!$basket->getAdvType()->mustCalculate()) { ?>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;" colspan="6">-</td>
      <?php } else { ?>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $total->getDuration()->isNotNull() ? $total->getDuration() . ' сек.' : '-'; ?></td>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $total->getViewsPerDay()->isNotNull() ? $total->getViewsPerDay() . ' раз' : '-'; ?></td>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $total->getDays()->isNotNull() ? $total->getDays() . ' дней' : '-'; ?></td>
        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;">
            <?= $total->getPriceWithoutDiscount()->isNotNull() ? '<strike>' . $total->getPriceWithoutDiscount()->getFormatted() . ' руб.</strike><br />' : ''; ?>
          <strong><?= $total->getPrice()->isNotNull() ? $total->getPrice()->getFormatted() . ' руб.' : '-'; ?></strong>
            <?= $total->getProfit()->isNotNull() ? '<br />Ваша выгода ' . $total->getProfit()->getFormatted() . ' руб.' : '' ?>
        </td>
      <?php } ?>
  </tr>
  </tfoot>
</table>
<br>
<p><strong>ВНИМАНИЕ!</strong> По всем вопросам, связанным с оказанием услуг, обращайтесь в службу поддержки Niko-Media
  по телефону +7 (3462) 550-877 или через электронную почту reklama@niko-m.ru.</p>
