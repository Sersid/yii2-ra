<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\FormCallMe */

?>
<p>Поступил заказ:</p>
<br>
<p><strong>Номер:</strong> <?=$model->getNumber()?></p>
<?if(!empty($model->name)):?>
    <p><strong>Имя:</strong> <?=$model->name?></p>
<?endif;?>
<?if(!empty($model->phone)):?>
    <p><strong>Номер телефона:</strong> <?=$model->phone?></p>
<?endif;?>
<?if(!empty($model->email)):?>
    <p><strong>E-mail:</strong> <?=$model->email?></p>
<?endif;?>
<p>Для детального просмотра перейдите по ссылке:</p>
<p><a href="<?=Url::to(['/admin/order', 'id' => $model->id], true)?>"><?=Url::to(['/admin/order', 'id' => $model->id], true)?></a></p>
