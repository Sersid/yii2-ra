<?php
/* @var $this yii\web\View */
/* @var $model common\models\Order */
?>
<? if (!empty($model->name)): ?>
    <p>Уважаемый <?= $model->name ?>!</p>
<? endif ?>
<p>Ваш заказ принят, и сейчас находится в обработке. Мы свяжемся с Вами в ближайшее время.</p>
<p style="font-size:18px;"><strong>Перечень заказа № <?= $model->getNumber() ?>:</strong></p>
<? if (!empty($model->outdoors)): ?>
    <p><strong>Наружная реклама</strong></p>
    <table border="1" style="border-spacing:0; border-collapse: collapse;">
        <? foreach ($model->outdoors as $outdoor): ?>
            <tr>
                <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $outdoor->side->parent->getSurface(
                    ) ?> <?= $outdoor->side->getNumber() ?></td>
                <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;">
                    <?= Yii::t(
                        'app',
                        '{months, plural, one{# месяц} few{# месяца} many{# месяцев} other{# месяцев}}',
                        ['months' => $outdoor->getCountMonth()]
                    ) ?>
                    <?= $outdoor->install || $outdoor->print || $outdoor->design ? '+' : '' ?>
                    <?
                    $additional = [];
                    if ($outdoor->install) {
                        $additional[] = 'монтаж';
                    }
                    if ($outdoor->print) {
                        $additional[] = 'печать';
                    }
                    if ($outdoor->design) {
                        $additional[] = 'дизайн';
                    }
                    ?>
                    <?= implode(', ', $additional) ?>
                </td>
                <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;">
                    <?php
                    if ($outdoor->getOldPrice() > $outdoor->getPrice()) {
                        echo '<p style="color: red; text-decoration: line-through;">' . $outdoor->getOldPrice()
                             . '</p>';
                    }
                    echo '<p>' . number_format($outdoor->getPrice(), 0, ' ', ' ') . ' руб.</p>';
                    ?>
                </td>
            </tr>
        <? endforeach ?>
    </table>
<? endif ?>
<? if (!empty($model->tvs)): ?>
    <p><strong>Реклама на тв</strong></p>
    <table border="1" style="border-spacing:0; border-collapse: collapse;">
        <? foreach ($model->tvs as $tv): ?>
            <? if (!empty($tv->userTvAdTvs)): ?>
                <? foreach ($tv->userTvAdTvs as $item): ?>
                    <?
                    $row = [
                        'duration' => $item->duration,
                        'days' => $item->days,
                        'price' => $tv->getPrice($item),
                    ];
                    $row['total'] = $row['price'] * $row['days'];
                    ?>
                    <tr>
                        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $item->tvAd->tv->name
                                                                                                     . ' - '
                                                                                                     . $item->tvAd->city->name ?></td>
                        <? if ($row['total'] > 0): ?>
                            <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= (int)$row['duration'] ?>
                                сек., <?= $row['price'] ?> р./день, <?= Yii::t(
                                    'app',
                                    '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}',
                                    ['days' => (int)$row['days']]
                                ) ?></td>
                            <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= number_format(
                                    $row['total'],
                                    0,
                                    ' ',
                                    ' '
                                ) ?> руб.
                            </td>
                        <? else: ?>
                            <td rowspan="2">Стоимость будет известна после уточнения всех деталей</td>
                        <? endif ?>
                    </tr>
                <? endforeach ?>
            <? endif; ?>
        <? endforeach ?>
    </table>
<? endif; ?>
<? if (!empty($model->tickers)): ?>
    <p><strong>Бегущая строка</strong></p>
    <table border="1" style="border-spacing:0; border-collapse: collapse;">
        <? foreach ($model->tickers as $ticker):
            $items = $ticker->getItems();
            if (!empty($items)):
                foreach ($items['items'] as $item):?>
                    <tr>
                        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $item['name'] ?></td>
                        <?
                        if (!empty($item['error'])):?>
                            <td rowspan="2">
                                <?= $item['error'] ?>
                            </td>
                        <? else: ?>
                            <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;">
                                <?= Yii::t(
                                    'app',
                                    '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}',
                                    ['days' => $item['cnt_days']]
                                ) ?>, <?= Yii::t(
                                    'app',
                                    '{words, plural, one{# слово} few{# слова} many{# слов} other{# слов}}',
                                    ['words' => $item['words']]
                                ) ?> <span>(<?= number_format($item['price'], 0, ' ', ' ') ?> руб./слово)</span>
                            </td>
                            <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= number_format(
                                    $item['total'],
                                    0,
                                    ' ',
                                    ' '
                                ) ?> руб.
                            </td>
                        <? endif ?>
                    </tr>
                <?
                endforeach;
            endif;
        endforeach;
        ?>
    </table>
<? endif ?>
<? if (!empty($model->radios)): ?>
    <p><strong>Реклама на радио</strong></p>
    <table border="1" style="border-spacing:0; border-collapse: collapse;">
        <? foreach ($model->radios as $radio): ?>
            <? if (!empty($radio->userRadioAdRadios)): ?>
                <? foreach ($radio->userRadioAdRadios as $item): ?>
                    <?
                    $row = [
                        'duration' => $item->duration,
                        'days' => $item->days,
                        'price' => $radio->getPrice($item),
                    ];

                    $row['total'] = $row['price'] * $row['days'];
                    ?>
                    <tr>
                        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= $item->radioAd->radio->name
                                                                                                     . ' - '
                                                                                                     . $item->radioAd->city->name ?></td>
                        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= (int)$row['duration'] ?>
                            сек., <?= $row['price'] ?> р./день, <?= Yii::t(
                                'app',
                                '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}',
                                ['days' => (int)$row['days']]
                            ) ?></td>
                        <td style="padding:3px 10px; font-size:14px; border-collapse: collapse;"><?= number_format(
                                $row['total'],
                                0,
                                ' ',
                                ' '
                            ) ?> руб.
                        </td>
                    </tr>
                <? endforeach; ?>
            <? endif; ?>
        <? endforeach; ?>
    </table>
<? endif; ?>
<br>
<p><strong>ВНИМАНИЕ!</strong> По всем вопросам, связанным с оказанием услуг, обращайтесь в службу поддержки Niko-Media
    по телефону +7 (3462) 550-877 или через электронную почту reklama@niko-m.ru.</p>
