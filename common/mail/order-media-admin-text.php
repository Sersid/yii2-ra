<?php
declare(strict_types=1);

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;
use yii\helpers\Url;

/** @var Order $order */
$basket = $order->getBasket();
$total = $basket->getTotal();
echo 'Поступил заказ "' . $order->getType()->getName() . '":';
echo "\n";
echo "\n";
echo "Номер заказа: " . $order->getNumber() . "(" . Url::to(['/admin/order', 'id' => $order->getId()->getValue()], true) . ")";
echo "\n";
if (!empty($order->getName())) {
    echo "Имя: ";
    echo $order->getName();
    echo "\n";
}
if (!empty($order->getPhone())) {
    echo "Номер телефона: ";
    echo $order->getPhone();
    echo "\n";
}
if (!empty($order->getEmail())) {
    echo "E-mail: ";
    echo $order->getEmail();
    echo "\n";
}
echo "Тип рекламы: ";
echo $basket->getAdvType()->getName();
echo "\n";
echo "\n";
echo "Предложение";
echo " | ";
echo "Длительность ролика в сек.";
echo " | ";
echo "Кол-во прокатов ролика в день";
echo " | ";
echo "Кол-во дней проката ролика";
echo " | ";
echo "Цена без скидки";
echo " | ";
echo "Цена со скидкой";
echo " | ";
echo "Выгода";
echo "\n";
foreach ($basket->getItemCollection()->all() as $basketItem) {
    echo $basketItem->getOffer()->name();
    echo " | ";
    if (!$basket->getAdvType()->mustCalculate() || !$basketItem->getOffer()->hasPricePerSec()) {
        echo 'Необходимо уточнить детали';
    } else {
        echo $basketItem->getDuration()->isNotNull() ? $basketItem->getDuration() . ' сек.' : '-';
        echo " | ";
        echo $basketItem->getViewsPerDay()->isNotNull() ? $basketItem->getViewsPerDay() . ' раз' : '-';
        echo " | ";
        echo $basketItem->getDays()->isNotNull() ? $basketItem->getDays() . ' дней' : '-';
        echo " | ";
        echo $basketItem->getPriceWithoutDiscount()->isNotNull() ? $basketItem->getPriceWithoutDiscount() . ' руб.' : '-';
        echo " | ";
        echo $basketItem->getPrice()->isNotNull() ? $basketItem->getPrice() . ' руб.' : '-';
        echo " | ";
        echo $basketItem->getProfit()->isNotNull() ? $basketItem->getProfit() . ' руб.' : '-';
        echo "\n";
    }
}
echo "\n";
echo "Итог";
echo " | ";
if (!$basket->getAdvType()->mustCalculate()) {
    echo "-";
} else {
    echo $total->getDuration()->isNotNull() ? $total->getDuration() . ' сек.' : '-';
    echo " | ";
    echo $total->getViewsPerDay()->isNotNull() ? $total->getViewsPerDay() . ' раз' : '-';
    echo " | ";
    echo $total->getDays()->isNotNull() ? $total->getDays() . ' дней' : '-';
    echo " | ";
    echo $total->getPriceWithoutDiscount()->isNotNull() ? $total->getPriceWithoutDiscount() . ' р.' : '-';
    echo " | ";
    echo $total->getPrice()->isNotNull() ? $total->getPrice() . ' р.' : '-';
    echo " | ";
    echo $total->getProfit()->isNotNull() ? $total->getProfit() . ' р.' : '-';
}
