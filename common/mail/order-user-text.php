<?php

/* @var $this yii\web\View */
/* @var $model common\models\Order */

?>
Ваш заказ успешно сформирован. С Вами свяжутся в ближайшее времяю

Номер заказа: <?=$model->getNumber()?>

<?php
/* @var $this yii\web\View */
/* @var $model common\models\Order */
?>
<?if(!empty($model->name)):?>
Уважаемый <?=$model->name?>!

<?endif?>
Ваш заказ принят, и сейчас находится в обработке. Мы свяжемся с Вами в ближайшее время.

Перечень заказа № <?=$model->getNumber()?>:
<?
if(!empty($model->outdoors)) {
    echo "Наружная реклама:\n";
    foreach ($model->outdoors as $outdoor) {
        echo $outdoor->side->parent->getSurface().' '.$outdoor->side->getNumber()." | ";
        echo Yii::t('app', '{months, plural, one{# месяц} few{# месяца} many{# месяцев} other{# месяцев}}', ['months' => $outdoor->getCountMonth()]);
        echo $outdoor->install || $outdoor->print || $outdoor->design ? '+' : '';
        $additional = [];
        if($outdoor->install) {
            $additional[] = 'монтаж';
        }
        if($outdoor->print) {
            $additional[] = 'печать';
        }
        if($outdoor->design) {
            $additional[] = 'дизайн';
        }
        echo implode(', ', $additional).' | ';
        echo number_format($outdoor->getPrice(), 0, ' ', ' ')." руб.\n";
    }
    echo "\n";
}
if(!empty($model->tvs)) {
    echo "Реклама на тв:\n";
    foreach ($model->tvs as $tv) {
        if(!empty($tv->userTvAdTvs)) {
            foreach ($tv->userTvAdTvs as $item) {
                $row = [
                    'duration' => $item->duration,
                    'days' => $item->days,
                    'price' => $tv->getPrice($item),
                ];
                $row['total'] = $row['price'] * $row['days'];

                echo $item->tvAd->tv->name.' - '.$item->tvAd->city->name.' | ';
                if($row['total'] > 0) {
                    echo (int)$row['duration']." сек., ".$row['price']." р./день, ".Yii::t('app', '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}', ['days' => (int)$row['days']])." | ";
                    echo number_format($row['total'], 0, ' ', ' ')." руб.";
                } else {
                    echo "Стоимость будет известна после уточнения всех деталей";
                }
                echo "\n";
            }
        }
    }
    echo "\n";
}
if(!empty($model->tickers)) {
    echo "Бегущая строка:\n";
    foreach ($model->tickers as $ticker) {
        $items = $ticker->getItems();
        if(!empty($items)) {
            foreach ($items['items'] as $item) {
                echo $item['name']." | ".$item['total'];
                if(!empty($item['error'])) {
                    echo $item['error'];
                } else {
                    echo Yii::t('app', '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}', ['days' => $item['cnt_days']]).", ".Yii::t('app', '{words, plural, one{# слово} few{# слова} many{# слов} other{# слов}}', ['words' => $item['words']])." (".number_format($item['price'], 0, ' ', ' ')." руб./слово) | ";
                    echo number_format($item['total'], 0, ' ', ' ')." руб.";
                }
                echo "\n";
            }
        }
    }
    echo "\n";
}

if(!empty($model->radios)) {
    echo "Реклама на радио:\n";
    foreach ($model->radios as $radio) {
        if(!empty($radio->userRadioAdRadios)) {
            foreach ($radio->userRadioAdRadios as $item) {
                $row = [
                    'duration' => $item->duration,
                    'days' => $item->days,
                    'price' => $radio->getPrice($item),
                ];

                $row['total'] = $row['price'] * $row['days'];

                echo $item->radioAd->radio->name.' - '.$item->radioAd->city->name." | ";
                echo (int)$row['duration']." сек., ".$row['price']." р./день, ".Yii::t('app', '{days, plural, one{# день} few{# дня} many{# дней} other{# дней}}', ['days' => (int)$row['days']]). " | ";
                echo number_format($row['total'], 0, ' ', ' ')." руб.";
                echo "\n";
            }
        }
    }
}
?>

ВНИМАНИЕ! По всем вопросам, связанным с оказанием услуг, обращайтесь в службу поддержки Niko-Media по телефону +7 (3462) 550-877 или через электронную почту reklama@niko-m.ru.