<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class FancyboxAsset
 * @package common\assets
 */
class FancyboxAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/fancybox/source';

    /**
     * @inheritdoc
     */
    public $css = [
        'jquery.fancybox.css',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     * @inheritdoc
     */
    public function registerAssetFiles($view)
    {
        $this->js[] = 'jquery.fancybox' . (!YII_DEBUG ? '.pack' : '') . '.js';
        parent::registerAssetFiles($view);
    }
}
