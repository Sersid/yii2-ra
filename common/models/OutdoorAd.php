<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%outdoor_ad}}".
 * @property integer         $id
 * @property string          $number       Краткий номер
 * @property integer         $city_id      Привязка к '{{%city}}'
 * @property integer         $street_id    Привязка к '{{%street}}'
 * @property integer         $format       Формат
 * @property integer         $type_display Типа отображения
 * @property string          $size         Размер
 * @property integer         $sides        Кол-во сторон
 * @property string          $lat          Широта
 * @property string          $lon          Долгота
 * @property integer         $rotate       Поворот
 * @property integer         $status       Статус
 * @property integer         $sort         Сортировка
 * @property integer         $created_at
 * @property integer         $updated_at
 * @property string          $full_number  Полный номер
 * @property City            $city
 * @property Street          $street
 * @property OutdoorAdSide[] $outdoorAdSides
 */
class OutdoorAd extends ActiveRecord
{
    /** @var int Status Enable */
    const STATUS_ENABLE = 1;
    /** @var int Status Disabled */
    const STATUS_DISABLED = 2;
    /** @var int Side A */
    const SIDE_A = 1;
    /** @var int Side A and B */
    const SIDE_A_B = 2;
    /** @var int Side A and B and C */
    const SIDE_A_B_C = 3;
    /** @var int Side A and B and C and D */
    const SIDE_A_B_C_D = 4;
    /** @var int Format - billboards */
    const FORMAT_BILLBOARDS = 10;
    /** @var int Format - led displays */
    const FORMAT_LED_DISPLAYS = 20;
    /** @var int Format - firewalls */
    const FORMAT_FIREWALLS = 30;
    /** @var int Format - adv fences */
    const FORMAT_ADV_FENCES = 40;
    /** @var int Format - city format */
    const FORMAT_CITY_FORMAT = 50;
    /** @var int Format - pylon */
    const FORMAT_PYLON = 60;
    /** @var int Format - projection ad */
    const FORMAT_PROJECTION_AD = 70;
    /** @var int Format - City board */
    const FORMAT_CITY_BOARD = 80;
    /** @var int Format - Stopping Pavilion */
    const FORMAT_STOPPING_PAVILION = 90;
    /** @var int Format - Pillar */
    const FORMAT_PILLAR = 100;
    /** @var int Format - Суперсайт */
    const FORMAT_SUPER_SITE = 110;
    /** @var int Панель-кронштейн */
    const FORMAT_BRACKET_PANEL = 120;
    /** @var int Type display - statics */
    const TYPE_DISPLAY_STATICS = 10;
    /** @var int Type display - prism */
    const TYPE_DISPLAY_PRISM = 20;
    /** @var int Type display - prism */
    const TYPE_DISPLAY_SCROLLER = 30;
    /** @var string */
    public $street_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%outdoor_ad}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['street_name', 'number', 'full_number'], 'filter', 'filter' => 'trim'],
            [['street_name', 'number', 'full_number'], 'string'],
            [
                ['full_number'],
                function ($attribute, $params, $validator) {
                    if (preg_match("/[а-я]/usi", $this->$attribute)) {
                        $this->addError($attribute, 'Полный номер содержит кириллицу.');
                    }
                },
            ],
            [['full_number'], 'unique'],
            [['city_id', 'sides', 'status', 'number', 'sort'], 'required'],
            [['city_id', 'street_id', 'rotate', 'status', 'sort'], 'integer'],
            ['sides', 'in', 'range' => array_keys($this->getSidesArr())],
            ['format', 'in', 'range' => array_keys($this->getFormats())],
            ['type_display', 'in', 'range' => array_keys($this->getTypeDisplays())],
            [['lat', 'lon'], 'number'],
            [['size'], 'string', 'max' => 255],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
            [
                ['street_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Street::class,
                'targetAttribute' => ['street_id' => 'id'],
            ],
        ];
    }

    /**
     * Sides
     * @return array
     */
    public function getSidesArr()
    {
        return [
            static::SIDE_A => 'A',
            static::SIDE_A_B => 'A, Б',
            static::SIDE_A_B_C => 'A, Б, B',
            static::SIDE_A_B_C_D => 'A, Б, B, Г',
        ];
    }

    /**
     * Formats
     * @return array
     */
    public static function getFormats()
    {
        return [
            static::FORMAT_BILLBOARDS => 'Рекламный щит',
            static::FORMAT_LED_DISPLAYS => 'Светодиодный экран',
            static::FORMAT_FIREWALLS => 'Брандмауэр',
            static::FORMAT_ADV_FENCES => 'Рекламное ограждение',
            static::FORMAT_CITY_FORMAT => 'Сити-формат',
            static::FORMAT_PYLON => 'Пилон',
            static::FORMAT_PROJECTION_AD => 'Проекционная реклама',
            static::FORMAT_CITY_BOARD => 'Ситиборд',
            static::FORMAT_STOPPING_PAVILION => 'Остановочный павильон',
            static::FORMAT_PILLAR => 'Пиллар',
            static::FORMAT_SUPER_SITE => 'Суперсайт',
            static::FORMAT_BRACKET_PANEL => 'Панель-кронштейн',
        ];
    }

    /**
     * @return array
     */
    public function getTypeDisplays()
    {
        return [
            static::TYPE_DISPLAY_STATICS => 'Статика',
            static::TYPE_DISPLAY_PRISM => 'Призма',
            static::TYPE_DISPLAY_SCROLLER => 'Скроллер',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'street_id' => Yii::t('app', 'Street'),
            'format' => Yii::t('app', 'Format'),
            'type_display' => Yii::t('app', 'Type display'),
            'size' => Yii::t('app', 'Size'),
            'sides' => Yii::t('app', 'Sides'),
            'lat' => Yii::t('app', 'Lat'),
            'lon' => Yii::t('app', 'Lon'),
            'rotate' => Yii::t('app', 'Rotate'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'street_name' => Yii::t('app', 'Street Name'),
            'number' => Yii::t('app', 'Number'),
            'full_number' => Yii::t('app', 'Full number'),
            'discount' => Yii::t('app', 'Discount Only'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->rotate = (int)$this->rotate;
    }

    /**
     * Sides
     * @return string
     */
    public function getSides()
    {
        $sides = $this->getSidesArr();
        return isset($sides[$this->sides]) ? $sides[$this->sides] : $this->sides;
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStreet()
    {
        return $this->hasOne(Street::class, ['id' => 'street_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOutdoorAdSides()
    {
        return $this->hasMany(OutdoorAdSide::class, ['parent_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * Краткий номер
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Полный номер
     * @return string
     */
    public function getFullNumber()
    {
        return $this->full_number;
    }

    /**
     * @param array|null $arOptions
     *
     * @return string
     */
    public function getSurface(array $arOptions = [])
    {
        $arOptions = array_merge(['type_display' => false], $arOptions);
        $arr = [];
        $format = $this->getFormat();
        if (!empty($format)) {
            $arr[] = $format;
        }
        if (!empty($this->size)) {
            $arr[] = trim($this->size);
        }
        if ($arOptions['type_display']) {
            $typeDisplay = $this->getTypeDisplay();
            if (!empty($typeDisplay)) {
                $arr[] = '(' . mb_strtolower($typeDisplay) . ')';
            }
        }
        return trim(implode(' ', $arr));
    }

    /**
     * ФОрмат
     * @return string
     */
    public function getFormat()
    {
        $formats = $this->getFormats();
        return isset($formats[$this->format]) ? $formats[$this->format] : '';
    }

    /**
     * Тип отображения
     * @return string
     */
    public function getTypeDisplay()
    {
        $arTypes = $this->getTypeDisplays();
        return isset($arTypes[$this->type_display]) ? $arTypes[$this->type_display] : '';
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->outdoorAdSides)) {
                foreach ($this->outdoorAdSides as $side) {
                    $side->delete();
                }
            }
            return true;
        }
        return false;
    }
}
