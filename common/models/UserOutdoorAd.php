<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_outdoor_ad}}".
 * @property integer       $guest_id
 * @property integer       $outdoor_ad_id
 * @property bool          $install
 * @property bool          $print
 * @property bool          $design
 * @property string        $dates
 * @property integer       $status
 * @property integer       $created_at
 * @property integer       $updated_at
 * @property Guest         $guest
 * @property OutdoorAdSide $outdoorAdSide
 * @property OutdoorAdSide $side
 */
class UserOutdoorAd extends ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_BASKET = 2;
    const STATUS_ORDER = 3;
    /**
     * @var float
     */
    private $_oldPrice;
    /**
     * @var float
     */
    private $_price;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_outdoor_ad}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => static::STATUS_BASKET],
            ['status', 'in', 'range' => array_keys($this->getStatuses())],
            [['guest_id', 'install', 'outdoor_ad_id'], 'required'],
            [['dates'], 'required', 'message' => 'Необходимо выбрать период аренды'],
            [['install', 'print', 'design'], 'boolean'],
            [['guest_id', 'outdoor_ad_id'], 'integer'],
            [['dates'], 'string', 'max' => 255],
            [
                ['guest_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Guest::class,
                'targetAttribute' => ['guest_id' => 'id'],
            ],
            [
                ['outdoor_ad_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OutdoorAdSide::class,
                'targetAttribute' => ['outdoor_ad_id' => 'id'],
            ],
        ];
    }

    /**
     * Statuses
     * @return array
     */
    public function getStatuses()
    {
        return [
            static::STATUS_NEW => 'Новая запись',
            static::STATUS_BASKET => 'В корзине',
            static::STATUS_ORDER => 'Заказан',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'guest_id' => Yii::t('app', 'Guest'),
            'outdoor_ad_id' => Yii::t('app', 'Guest'),
            'install' => 'Монтаж',
            'print' => Yii::t('app', 'Print'),
            'design' => Yii::t('app', 'Design'),
            'dates' => Yii::t('app', 'Dates'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(Guest::class, ['id' => 'guest_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSide()
    {
        return $this->getOutdoorAdSide();
    }

    /**
     * @return ActiveQuery
     */
    public function getOutdoorAdSide()
    {
        return $this->hasOne(OutdoorAdSide::class, ['id' => 'outdoor_ad_id']);
    }

    /**
     * Цена с учетом скидки
     * @return float
     */
    public function getPrice()
    {
        if ($this->_price === null) {
            $price = 0;
            if ($this->install) {
                $price += (float)$this->side->price_installation;
            }
            if ($this->print) {
                $price += (float)$this->side->price_print;
            }
            if ($this->design) {
                $price += (float)$this->side->price_design;
            }
            if (!empty($this->side->busy)) {
                $arDates = $this->getDates();
                foreach ($this->side->busy as $busy) {
                    if (in_array($busy->month . '.' . $busy->year, $arDates)) {
                        $price += (float)$this->side->getPriceWithDiscount();
                    }
                }
            }

            $this->_price = $price;
        }
        return $this->_price;
    }

    /**
     * @return array
     */
    public function getDates()
    {
        static $arDates;
        if ($arDates !== null) {
            return $arDates;
        }
        $arDates = explode(',', $this->dates);
        return $arDates;
    }

    /**
     * Цена без учета скидки
     * @return float
     */
    public function getOldPrice()
    {
        if ($this->_oldPrice === null) {
            $price = 0;
            if ($this->install) {
                $price += (float)$this->side->price_installation;
            }
            if ($this->print) {
                $price += (float)$this->side->price_print;
            }
            if ($this->design) {
                $price += (float)$this->side->price_design;
            }
            $price += $this->getCountMonth() * $this->side->price;
            $this->_oldPrice = $price;
        }

        return $this->_oldPrice;
    }

    /**
     * @return int
     */
    public function getCountMonth()
    {
        return count($this->getDates());
    }
}
