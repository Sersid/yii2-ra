<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%outdoor_ad_side_busy}}".
 * @property integer       $side_id
 * @property integer       $month
 * @property integer       $year
 * @property OutdoorAdSide $side
 * @property integer       $status
 * @property float         $discount
 */
class OutdoorAdSideBusy extends ActiveRecord
{
    // Занят
    const STATUS_BUSY = 10;
    // Свободен
    const STATUS_FREE = 20;
    // Зарезервирован
    const STATUS_RESERVE = 30;
    // По запросу
    const STATUS_REQUEST = 40;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%outdoor_ad_side_busy}}';
    }

    /**
     * @return array
     */
    public static function getStatusesNames()
    {
        return [
            self::STATUS_BUSY => 'Занято',
            self::STATUS_FREE => 'Свободно',
            self::STATUS_RESERVE => 'Зарезервиновано',
            self::STATUS_REQUEST => 'По запросу',
        ];
    }

    /**
     * Передает статус по букве
     *
     * @param $letter
     *
     * @return int|mixed
     */
    public static function getStatusByLetter($letter)
    {
        if (static::getDiscount($letter) > 0) {
            return self::STATUS_FREE;
        }
        $letter = mb_strtoupper($letter);
        $statuses = self::getStatuses();
        if (isset($statuses[$letter])) {
            return $statuses[$letter];
        }
        $letter = mb_strtolower($letter);
        return isset($statuses[$letter]) ? $statuses[$letter] : self::STATUS_BUSY;
    }

    /**
     * Значение скидки
     *
     * @param $discount
     *
     * @return float|int
     */
    public static function getDiscount($discount)
    {
        $discount = floatval(trim($discount));
        return $discount > 0 && $discount < 100 ? $discount : 0;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['side_id', 'month', 'year'], 'required'],
            [['side_id', 'month', 'year'], 'integer'],
            [['status'], 'in', 'range' => $this->getStatuses()],
            [
                ['side_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OutdoorAdSide::class,
                'targetAttribute' => ['side_id' => 'id'],
            ],
            [['discount'], 'number', 'min' => 0, 'max' => 99.99],
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            'З' => self::STATUS_BUSY,
            'С' => self::STATUS_FREE,
            'Р' => self::STATUS_RESERVE,
            'пз' => self::STATUS_REQUEST,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'side_id' => Yii::t('app', 'Outdoor ad side'),
            'month' => Yii::t('app', 'Month'),
            'year' => Yii::t('app', 'Year'),
            'status' => Yii::t('app', 'Status'),
            'discount' => Yii::t('app', 'Discount'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->discount = (float)$this->discount;
    }

    /**
     * @return ActiveQuery
     */
    public function getSide()
    {
        return $this->hasOne(OutdoorAdSide::class, ['id' => 'side_id']);
    }

    /**
     * Имя месяца
     * @return string
     */
    public function getMonthName()
    {
        $arMonths = static::getMonths();
        return isset($arMonths[$this->month]) ? $arMonths[$this->month] : '-';
    }

    /**
     * Названия месяцев
     * @return array
     */
    public static function getMonths()
    {
        return [
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь',
        ];
    }
}
