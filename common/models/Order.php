<?php

namespace common\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator3;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 * @property integer         $id
 * @property integer         $guest_id
 * @property string          $name
 * @property string          $email
 * @property string          $phone
 * @property integer         $created_at
 * @property integer         $updated_at
 * @property Guest           $guest
 * @property UserOutdoorAd[] $outdoors
 * @property UserRadioAd[]   $radios
 * @property UserTicker[]    $tickers
 * @property UserTvAd[]      $tvs
 */
class Order extends ActiveRecord
{
    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'filter', 'filter' => 'trim'],
            [['guest_id', 'email', 'phone'], 'required'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            [
                ['guest_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Guest::class,
                'targetAttribute' => ['guest_id' => 'id'],
            ],
            [['reCaptcha'], ReCaptchaValidator3::class, 'action' => 'quickorder'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'guest_id' => Yii::t('app', 'Guest'),
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(Guest::class, ['id' => 'guest_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOutdoors()
    {
        return $this->hasMany(UserOutdoorAd::class, ['order_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRadios()
    {
        return $this->hasMany(UserRadioAd::class, ['order_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTickers()
    {
        return $this->hasMany(UserTicker::class, ['order_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTvs()
    {
        return $this->hasMany(UserTvAd::class, ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->outdoors)) {
                foreach ($this->outdoors as $outdoor) {
                    $outdoor->delete();
                }
            }
            if (!empty($this->radios)) {
                foreach ($this->radios as $radio) {
                    $radio->delete();
                }
            }
            if (!empty($this->tickers)) {
                foreach ($this->tickers as $ticker) {
                    $ticker->delete();
                }
            }
            if (!empty($this->tvs)) {
                foreach ($this->tvs as $tv) {
                    $tv->delete();
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return boolean whether the email was sent
     */
    public function sendEmailToAdmin()
    {
        return Yii::$app->mailer->compose(
            ['html' => 'order-admin-html', 'text' => 'order-admin-text'],
            ['model' => $this]
        )->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])->setTo(
            Yii::$app->params['adminEmail']
        )->setSubject('заказ')->send();
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return boolean whether the email was sent
     */
    public function sendEmailToUser()
    {
        if (empty($this->email)) {
            return true;
        }
        return Yii::$app->mailer->compose(
            ['html' => 'order-user-html', 'text' => 'order-user-text'],
            ['model' => $this]
        )->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])->setTo($this->email)
            ->setSubject('Заказ #' . $this->getNumber() . ' ' . Yii::$app->name)->send();
    }

    /**
     * Get 6 integer number
     * @return string
     */
    public function getNumber()
    {
        return str_repeat(0, 6 - strlen($this->id)) . $this->id;
    }
}
