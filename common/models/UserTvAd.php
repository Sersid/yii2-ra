<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_tv_ad}}".
 * @property integer      $id
 * @property integer      $guest_id
 * @property integer      $type
 * @property integer      $city_id
 * @property integer      $show_all_cities
 * @property integer      $status
 * @property integer      $created_at
 * @property integer      $updated_at
 * @property City         $city
 * @property Guest        $guest
 * @property UserTvAdTv[] $userTvAdTvs
 * @property TvAd[]       $tvAds
 */
class UserTvAd extends ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_BASKET = 2;
    const STATUS_ORDER = 3;
    const TYPE_AD = 1;
    const TYPE_SPONSORSHIP = 2;
    const TYPE_SCREEN = 3;
    public $channels = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_tv_ad}}';
    }

    /**
     * @return array|null|ActiveRecord
     */
    public static function getModel()
    {
        return self::find()->where(
            [
                'guest_id' => Yii::$app->guest->id,
                'user_tv_ad.status' => self::STATUS_NEW,
            ]
        )->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'default', 'value' => static::TYPE_AD],
            ['type', 'in', 'range' => array_keys($this->getTypes())],
            ['status', 'default', 'value' => static::STATUS_NEW],
            ['status', 'in', 'range' => array_keys($this->getStatuses())],
            [['guest_id', 'type', 'status'], 'required'],
            [['show_all_cities'], 'boolean'],
            ['channels', 'each', 'rule' => ['integer']],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
            [
                ['guest_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Guest::class,
                'targetAttribute' => ['guest_id' => 'id'],
            ],
        ];
    }

    /**
     * Types
     * @return array
     */
    public function getTypes()
    {
        return [
            static::TYPE_AD => 'Рекламный ролик',
            static::TYPE_SPONSORSHIP => 'Спонсорство',
            static::TYPE_SCREEN => 'Заставка',
        ];
    }

    /**
     * Statuses
     * @return array
     */
    public function getStatuses()
    {
        return [
            static::STATUS_NEW => 'Новая запись',
            static::STATUS_BASKET => 'В корзине',
        ];
    }

    public function getTypeName()
    {
        $types = $this->getTypes();
        return isset($types[$this->type]) ? $types[$this->type] : '-';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'guest_id' => Yii::t('app', 'Guest'),
            'type' => Yii::t('app', 'Формат объявления'),
            'city_id' => Yii::t('app', 'Город'),
            'show_all_cities' => 'Смотреть список всех телеканалов',
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(Guest::class, ['id' => 'guest_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTvAdTvs()
    {
        return $this->hasMany(UserTvAdTv::class, ['user_tv_ad_id' => 'id'])->orderBy('user_tv_ad_tv.created_at');
    }

    /**
     * @return ActiveQuery
     */
    public function getTvAds()
    {
        return $this->hasMany(TvAd::class, ['id' => 'tv_ad_id'])->viaTable(
            '{{%user_tv_ad_tv}}',
            ['user_tv_ad_id' => 'id']
        );
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->getOldAttribute('city_id') != $this->city_id) {
            $this->show_all_cities = false;
        }
        return parent::beforeSave($insert);
    }

    /**
     * Get price for 1 day
     *
     * @param $item
     *
     * @return null
     */
    public function getPrice($item)
    {
        $price = null;
        switch ($this->type) {
            case static::TYPE_AD:
                if ($item->tvAd->price_ad_sec !== null) {
                    $price = $item->duration * $item->tvAd->price_ad_sec * $item->views;
                }
                break;
            case static::TYPE_SPONSORSHIP:
                if ($item->tvAd->price_sponsorship_sec !== null) {
                    $price = $item->duration * $item->tvAd->price_sponsorship_sec * $item->views;
                }
                break;
            case static::TYPE_SCREEN:
                if ($item->tvAd->price_screen_sec !== null) {
                    $price = $item->duration * $item->tvAd->price_screen_sec * $item->views;
                }
                break;
        }

        return $price;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->userTvAdTvs)) {
                foreach ($this->userTvAdTvs as $userTvAdTv) {
                    $userTvAdTv->delete();
                }
            }
            return true;
        }
        return false;
    }
}
