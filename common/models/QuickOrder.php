<?php

namespace common\models;

/**
 * Class QuickOrder
 * @package common\models
 * @property $model_type
 */
class QuickOrder extends Order
{
    /** @var integer */
    public $model_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['model_type'], 'required'],
                [
                    'model_id',
                    'required',
                    'when' => function ($model) {
                        return in_array($model->model_type, ['tv', 'ticker', 'radio']);
                    },
                ],
            ]
        );
    }
}
