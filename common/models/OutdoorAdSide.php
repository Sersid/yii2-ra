<?php

namespace common\models;

use Throwable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%outdoor_ad_side}}".
 * @property integer              $id
 * @property integer              $parent_id          Связь с {{%outdoor_ad}}
 * @property integer              $side               Номер стороны
 * @property integer              $image_id           Связь с {{%file}}
 * @property string               $price              Стоимость
 * @property string               $price_installation Стоимость установки
 * @property string               $price_print        Стоимость печати
 * @property string               $price_design       Стоимость дизайна
 * @property string               $description        Описание
 * @property integer              $rating             Рейтинг
 * @property integer              $status             Статус
 * @property integer              $busy_status        Статус занятости
 * @property integer              $created_at
 * @property integer              $updated_at
 * @property float                $discount           Скидка на весь месяц
 * @property float                $discount_monthly   Ежемесячная скидка
 * @property string               $address            Адрес
 * @property File                 $image
 * @property OutdoorAd            $parent
 * @property OutdoorAdSideBusy[]  $busy
 * @property UserOutdoorAd[]      $baskets
 * @property UserOutdoorAd        $basket
 * @property OutdoorAdSideImage[] $images
 * @property OutdoorAdSideImage[] $allImages
 */
class OutdoorAdSide extends ActiveRecord
{
    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 2;

    const SIDE_A = 1;
    const SIDE_B = 2;
    const SIDE_C = 3;

    const BUSY_STATUS_FREE = 10;
    const BUSY_STATUS_BUSY = 20;
    /** @var array */
    private $_dates;
    /** @var array */
    private $_images;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%outdoor_ad_side}}';
    }

    /**
     * @inheritdoc
     * @return OutdoorAdSideQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OutdoorAdSideQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getDatesDropDownItems()
    {
        $dates = self::getDates();
        $items = [];
        foreach ($dates as $date) {
            if ($date['disabled']) {
                continue;
            }
            foreach ($date['months'] as $mount) {
                $items[$mount['num'] . '.' . $date['year']] = $mount['name'];
            }
        }
        return $items;
    }

    /**
     * Массив месяцев для генерации чекбоксов (5 доступных для покупки, 4 деактивированных)
     * @return array
     */
    public static function getDates()
    {
        static $arDates;
        if (is_null($arDates)) {
            $arDates = [];
            $ruDates = OutdoorAdSideBusy::getMonths();
            $key = 0;
            $lastYear = 0;
            for ($i = 1; $i <= 9; $i++) {
                $time = mktime(0, 0, 0, date('n') + $i, 1, date('Y'));
                $m = date('n', $time);
                $y = date('Y', $time);

                if ($lastYear > 0 && $lastYear != $y || $i == 6) {
                    $key++;
                }
                if ($i > 5) {
                    $arDates[$key]['disabled'] = true;
                } else {
                    $arDates[$key]['disabled'] = false;
                }
                $arDates[$key]['year'] = $y;
                $arDates[$key]['months'][] = [
                    'name' => $ruDates[$m],
                    'num' => $m,
                    'disabled' => $i > 5,
                ];
                $lastYear = $y;
            }
        }
        return $arDates;
    }

    /**
     * Рейтинг места
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['price_design', 'default', 'value' => 5000],
            ['busy_status', 'default', 'value' => self::BUSY_STATUS_FREE],
            [
                [
                    'parent_id',
                    'side',
                    'price',
                    'price_installation',
                    'price_print',
                    'price_design',
                    'status',
                    'busy_status',
                ],
                'required',
            ],
            [['parent_id', 'image_id', 'rating'], 'integer'],
            ['status', 'in', 'range' => array_keys($this->getStatuses())],
            [['description'], 'safe'],
            [['price', 'price_installation', 'price_print', 'price_design'], 'number'],
            [['discount', 'discount_monthly'], 'number', 'min' => 0, 'max' => 99.9],
            [
                'side',
                'in',
                'range' => array_keys(self::getSides()),
                'message' => 'Сторона должна быть А, Б или В (русские буквы)',
            ],
            ['rating', 'in', 'range' => array_keys($this->getRatings())],
            [['address'], 'string', 'max' => 255],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['image_id' => 'id'],
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OutdoorAd::class,
                'targetAttribute' => ['parent_id' => 'id'],
            ],
        ];
    }

    /**
     * Statuses
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ENABLE => 'Enable',
            self::STATUS_DISABLED => 'Disabled',
        ];
    }

    /**
     * Sides
     * @return array
     */
    public static function getSides()
    {
        return [
            self::SIDE_A => 'А',
            self::SIDE_B => 'Б',
            self::SIDE_C => 'В',
        ];
    }

    /**
     * @return array
     */
    public static function getRatings()
    {
        return [
            0 => '0 (Плохое место)',
            1 => '1 (Посредственное место)',
            2 => '2 (Хорошее место)',
            3 => '3 (Отличное место)',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Outdoor ad'),
            'number' => Yii::t('app', 'Number'),
            'side' => Yii::t('app', 'Side'),
            'image_id' => Yii::t('app', 'Image'),
            'price' => Yii::t('app', 'Price'),
            'price_installation' => Yii::t('app', 'Price Installation'),
            'price_print' => Yii::t('app', 'Price Print'),
            'price_design' => Yii::t('app', 'Price Design'),
            'rating' => Yii::t('app', 'Rating'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'description' => Yii::t('app', 'Description'),
            'discount' => Yii::t('app', 'Discount'),
            'discount_monthly' => Yii::t('app', 'Monthly discount'),
            'address' => Yii::t('app', 'Address'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getImages()
    {
        $query = $this->hasMany(OutdoorAdSideImage::class, ['side_id' => 'id']);
        $query->where(['outdoor_ad_side_image.status' => OutdoorAdSideImage::STATUS_LOADED]);
        $query->joinWith('folder');
        $order = SORT_ASC;
        $m = (int)date('n');
        if ($m < 5 || $m >= 11) {
            $order = SORT_DESC;
        }
        $query->orderBy(['season' => $order]);
        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(OutdoorAd::class, ['id' => 'parent_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getBusy()
    {
        return $this->hasMany(OutdoorAdSideBusy::class, ['side_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getBaskets()
    {
        return $this->hasMany(UserOutdoorAd::class, ['outdoor_ad_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAllImages()
    {
        return $this->hasMany(OutdoorAdSideImage::class, ['side_id' => 'id']);
    }

    /**
     * @return UserOutdoorAd
     */
    public function getBasket()
    {
        $basket = UserOutdoorAd::find()
            ->where(['outdoor_ad_id' => $this->id, 'guest_id' => Yii::$app->guest->id])
            ->one();
        if (empty($basket)) {
            $basket = new UserOutdoorAd();
            $basket->outdoor_ad_id = $this->id;
            $basket->guest_id = Yii::$app->guest->id;
        }
        return $basket;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->price = (float)$this->price;
        $this->price_installation = (float)$this->price_installation;
        $this->price_design = (float)$this->price_design;
        $this->price_print = (float)$this->price_print;
        $this->discount = (float)$this->discount;
        $this->discount_monthly = (float)$this->discount_monthly;
        parent::afterFind();
    }

    /**
     * @return string
     */
    public function getPriceWithDiscount()
    {
        return $this->price - $this->getDiffDiscountPrice();
    }

    /**
     * @return string
     */
    public function getDiffDiscountPrice()
    {
        return $this->price / 100 * $this->getDiscount();
    }

    /**
     * Процент скидки на следующий месяц
     * @return float|int
     */
    public function getDiscount()
    {
        $discount = $this->discount;
        $discountMonthly = $this->getDiscountMonthly();
        if ($discountMonthly > $discount) {
            $discount = $discountMonthly;
        }
        return $discount;
    }

    /**
     * Ежемесячная скидка
     * Данная скидка применяется при следующих условиях:
     * 1. Скидка заполнена
     * 2. Запись обновлена в этом месяце
     * 2. Действует до 18:00 последнего дня месяца
     * 3. В выходные не действует
     */
    public function getDiscountMonthly()
    {
        $discountMonthly = 0;
        if (!empty($this->discount_monthly)
            && $this->busy_status == self::BUSY_STATUS_FREE
            && date('n.Y', $this->updated_at) == date('n.Y')
            && self::isDiscountMonthlyValid()) {
            $discountMonthly = $this->discount_monthly;
        }
        return $discountMonthly;
    }

    /**
     * Действует ли скидка
     * @return bool
     */
    public static function isDiscountMonthlyValid()
    {
        static $isValid;
        if (is_null($isValid)) {
            $current = time();
            $from = mktime(0, 0, 0, date('n'), 15, date('Y'));
            $to = mktime(18, 0, 0, date('n') + 1, -4, date('Y'));
            $isValid = $current >= $from && $current < $to && !in_array(date('N'), [6, 7]);
        }
        return $isValid;
    }

    /**
     * @return string
     */
    public function getPriceInstallation()
    {
        return $this->price_installation;
    }

    /**
     * @return string
     */
    public function getPriceDesign()
    {
        return $this->price_design;
    }

    /**
     * @return string
     */
    public function getPricePrint()
    {
        return $this->price_print;
    }

    /**
     * @return string
     */
    public function getDiscountHTML()
    {
        return $this->getDiscount() . '%';
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->parent->number . ' ' . $this->getSide();
    }

    /**
     * @return int
     */
    public function getSide()
    {
        $sides = $this->getSides();
        return isset($sides[$this->side]) ? $sides[$this->side] : '-';
    }

    /**
     * @return string
     */
    public function getH3()
    {
        $arResult[] = $this->parent->getSurface();
        if ($side = $this->getSide()) {
            $arResult[] = '(сторона ' . $side . ')';
        }
        if ($typeDisplay = $this->parent->getTypeDisplay()) {
            $arResult[] = '- ' . strtolower($typeDisplay);
        }
        return implode(' ', $arResult);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->parent->getSurface() . ' (сторона ' . $this->getSide() . ') г. ' . $this->parent->city->name
               . ', ' . $this->getFullAddress() . ' - цена ' . $this->getPrice() . ' руб./мес.';
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        $arr = [];
        if (!empty($this->parent->street->name)) {
            $arr[] = $this->parent->street->name;
        }
        if (!empty($this->address)) {
            $arr[] = $this->address;
        }
        return implode(', ', $arr);
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Нужен ' . $this->parent->getSurface() . ' по адресу г. ' . $this->parent->city->name . ', '
               . $this->getFullAddress()
               . '? Заходите и подберите себе подходящее время размещения. Онлайн заказ. Бесплатная консультация по '
               . 'всем интересующим вас вопросам.';
    }

    /**
     * @param $month
     * @param $year
     *
     * @return bool
     */
    public function getIsBusy($month, $year)
    {
        static $arBusy;
        if (is_null($arBusy)) {
            foreach ($this->busy as $busy) {
                $arBusy[$busy->month . '.' . $busy->year] = $busy->status;
            }
        }
        if (is_null($arBusy)) {
            return false;
        }
        if (isset($arBusy[$month . '.' . $year])) {
            return $arBusy[$month . '.' . $year] == OutdoorAdSideBusy::STATUS_BUSY;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getBusyStatuses()
    {
        if (is_null($this->_dates)) {
            $busyItems = [];
            foreach ($this->busy as $busy) {
                $busyItems[$busy->month . '.' . $busy->year] = $busy;
            }
            $this->_dates = self::getDates();
            $current = date('n.Y', mktime(0, 0, 0, date('n') + 1, 1, date('Y')));
            foreach ($this->_dates as &$date) {
                foreach ($date['months'] as &$mount) {
                    if (isset($busyItems[$mount['num'] . '.' . $date['year']])) {
                        $mount['status'] = $busyItems[$mount['num'] . '.' . $date['year']]->status;
                        if ($mount['num'] . '.' . $date['year'] == $current) {
                            $mount['discount'] = $this->getDiscount();
                        } else {
                            $mount['discount'] = $busyItems[$mount['num'] . '.' . $date['year']]->discount;
                        }
                    } else {
                        $mount['status'] = OutdoorAdSideBusy::STATUS_FREE;
                        $mount['discount'] = 0;
                    }
                }
            }
        }
        return $this->_dates;
    }

    /**
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->busy)) {
                foreach ($this->busy as $busy) {
                    $busy->delete();
                }
            }

            if (!empty($this->baskets)) {
                foreach ($this->baskets as $basket) {
                    $basket->delete();
                }
            }

            if (!empty($this->image)) {
                $this->image->delete();
            }

            if (!empty($this->allImages)) {
                foreach ($this->allImages as $image) {
                    $image->delete();
                }
            }

            return true;
        }
        return false;
    }

    /**
     * @param string $tag
     *
     * @return string
     */
    public function getBusyStatusHTML($tag = 'div')
    {
        if ($this->busy_status == self::BUSY_STATUS_BUSY) {
            return Html::tag($tag, 'Занято', ['class' => 'td red-title']);
        } else {
            return Html::tag($tag, 'Свободно', ['class' => 'td green-title']);
        }
    }

    /**
     * URL детальной страницы
     * @return string
     */
    public function getDetailPageUrl()
    {
        $arSides = self::getLatSides();
        if (isset($arSides[$this->side])) {
            return '/outdoor-ad/view/' . $this->parent->full_number . $arSides[$this->side];
        } else {
            return '#';
        }
    }

    /**
     * Стороны в латинице
     * @return array
     */
    public static function getLatSides()
    {
        return [
            self::SIDE_A => 'A',
            self::SIDE_B => 'B',
            self::SIDE_C => 'C',
        ];
    }
}
