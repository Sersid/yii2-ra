<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%street}}".
 * @property integer     $id
 * @property string      $name
 * @property integer     $city_id
 * @property integer     $status
 * @property integer     $sort
 * @property integer     $created_at
 * @property integer     $updated_at
 * @property OutdoorAd[] $outdoorAds
 * @property City        $city
 */
class Street extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%street}}';
    }

    public static function getIdByName($city_id, $name)
    {
        $model = self::find()->where(['city_id' => $city_id, 'name' => $name])->one();
        if (empty($model)) {
            $model = new self();
            $model->name = $name;
            $model->city_id = $city_id;
            $model->status = self::STATUS_ACTIVE;
            $model->sort = 500;
            $model->save();
        }

        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'city_id', 'sort'], 'required'],
            [['city_id', 'status', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'city_id' => Yii::t('app', 'City'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOutdoorAds()
    {
        return $this->hasMany(OutdoorAd::class, ['street_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
