<?php
declare(strict_types=1);

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tv_ad}}".
 * @property int                   $id
 * @property int                   $city_id
 * @property int                   $tv_id
 * @property string                $price_ad_sec
 * @property string                $price_sponsorship_sec
 * @property string                $price_screen_sec
 * @property string                $price_ad_show
 * @property string                $price_sponsorship_show
 * @property string                $price_screen_show
 * @property int                   $discount
 * @property int                   $status
 * @property int                   $sort
 * @property int                   $created_at
 * @property int                   $updated_at
 * @property City                  $city
 * @property Tv                    $tv
 * @property TvAdPotentialAudience $potentialAudience
 * @property File                  $cityMap
 * @property UserTvAd[]            $userTvAds
 * @property ?string               $price_table
 */
class TvAd extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tv_ad}}';
    }

    /**
     * @inheritdoc
     * @return TvAdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvAdQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'tv_id', 'status', 'sort'], 'required'],
            [['sort', 'city_map'], 'integer'],
            [['status'], 'boolean'],
            [
                [
                    'price_ad_sec',
                    'price_sponsorship_sec',
                    'price_screen_sec',
                    'price_ad_show',
                    'price_sponsorship_show',
                    'price_screen_show',
                    'discount',
                ],
                'number',
            ],
            [
                ['city_id', 'tv_id'],
                'unique',
                'targetAttribute' => ['city_id', 'tv_id'],
                'message' => 'The combination of City and TV has already been taken.',
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
            [
                ['tv_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Tv::class,
                'targetAttribute' => ['tv_id' => 'id'],
            ],
            [
                ['city_map'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['city_map' => 'id'],
            ],
            [['price_table'], 'filter', 'filter' => 'trim'],
            [['price_table'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'tv_id' => Yii::t('app', 'TV'),
            'price_ad_sec' => Yii::t('app', 'Ads - Sec'),
            'price_sponsorship_sec' => Yii::t('app', 'Sponsorship - Sec'),
            'price_screen_sec' => Yii::t('app', 'Screen - Sec'),
            'price_ad_show' => Yii::t('app', 'Ads - Show'),
            'price_sponsorship_show' => Yii::t('app', 'Sponsorship - Show'),
            'price_screen_show' => Yii::t('app', 'Screen - Show'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'discount' => 'Скидка',
            'city_map' => 'Изображение карты города (цветная обводка)',
            'price_table' => 'Таблица цен',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTv()
    {
        return $this->hasOne(Tv::class, ['id' => 'tv_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPotentialAudience()
    {
        return $this->hasOne(TvAdPotentialAudience::class, ['tv_ad_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCityMap()
    {
        return $this->hasOne(File::class, ['id' => 'city_map']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->price_ad_sec = $this->price_ad_sec !== null ? (float)$this->price_ad_sec : null;
        $this->price_sponsorship_sec =
            $this->price_sponsorship_sec !== null ? (float)$this->price_sponsorship_sec : null;
        $this->price_screen_sec = $this->price_screen_sec !== null ? (float)$this->price_screen_sec : null;
        $this->price_ad_show = $this->price_ad_show !== null ? (float)$this->price_ad_show : null;
        $this->price_sponsorship_show =
            $this->price_sponsorship_show !== null ? (float)$this->price_sponsorship_show : null;
        $this->price_screen_show = $this->price_screen_show !== null ? (float)$this->price_screen_show : null;
        $this->discount = $this->discount !== null ? (float)$this->discount : null;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->userTvAds)) {
                foreach ($this->userTvAds as $userTvAds) {
                    $userTvAds->delete();
                }
            }
            if (!empty($this->potentialAudience)) {
                $this->potentialAudience->delete();
            }
            if (!empty($this->cityMap)) {
                $this->cityMap->delete();
            }
            return true;
        }
        return false;
    }
}
