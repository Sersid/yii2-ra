<?php

namespace common\models;

use Throwable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "{{%city}}".
 * @property integer  $id
 * @property string   $name
 * @property integer  $image_id
 * @property integer  $salary
 * @property integer  $population
 * @property integer  $density
 * @property string   $activities
 * @property integer  $is_large_city
 * @property integer  $status
 * @property integer  $sort
 * @property integer  $created_at
 * @property integer  $updated_at
 * @property string   $title
 * @property string   $content
 * @property string   $content_outdoor_ad
 * @property string   $content_tv
 * @property string   $content_ticker
 * @property string   $content_radio
 * @property string   $title_outdoor_ad
 * @property string   $title_tv
 * @property string   $title_ticker
 * @property string   $title_radio
 * @property string   $slug
 * @property string   $phone
 * @property string   $contact_text
 * @property integer  $region_id
 * @property string   $where
 * @property string   $requisitesFile
 * @property integer  $phone_digit
 * @property File     $image
 * @property Street[] $streets
 */
class City extends ActiveRecord
{
    const REQUISITES_PATH = '@frontend/web/requisites/';
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLE = 2;
    const SIX_DIGIT_PHONE = 0;
    const SEVEN_DIGIT_PHONE = 1;
    const CITY_IS_LARGE = 1;
    const CITY_IS_SMALL = 0;

    /** @var string файл реквизитов */
    public $requisitesFile;
    /** @var bool необходимо удалить файл реквизитов */
    public $isNeedDeleteRequisitesFile = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * Get id by name
     *
     * @param $name
     *
     * @return int
     */
    public static function getIdByName($name)
    {
        $name = trim($name);
        $model = self::find()
            ->where(['name' => $name])
            ->one();
        if (empty($model)) {
            $model = new self();
            $model->name = $name;
            $model->salary = 0;
            $model->population = 0;
            $model->is_large_city = false;
            $model->status = self::STATUS_DISABLE;
            $model->sort = 500;
            $model->save(false);
        }
        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'activities',
                    'title',
                    'content',
                    'content_outdoor_ad',
                    'content_tv',
                    'content_ticker',
                    'content_radio',
                    'slug',
                    'title_outdoor_ad',
                    'title_tv',
                    'title_ticker',
                    'title_radio',
                    'phone',
                    'contact_text',
                    'where',
                ],
                'filter',
                'filter' => 'trim',
            ],
            [['name', 'salary', 'population', 'is_large_city', 'status', 'sort', 'region_id'], 'required'],
            [['region_id', 'image_id', 'sort'], 'integer'],
            [['salary', 'population', 'density'], 'number'],
            [['status', 'is_large_city', 'phone_digit'], 'boolean'],
            [
                ['name', 'title', 'title_outdoor_ad', 'title_tv', 'title_ticker', 'title_radio', 'where'],
                'string',
                'max' => 255,
            ],
            [['slug'], 'unique'],
            [
                [
                    'activities',
                    'content',
                    'content_outdoor_ad',
                    'content_tv',
                    'content_ticker',
                    'content_radio',
                    'phone',
                    'contact_text',
                ],
                'string',
            ],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['image_id' => 'id'],
            ],
            ['isNeedDeleteRequisitesFile', 'boolean'],
            ['requisitesFile', 'file', 'extensions' => ['doc', 'docx']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'image_id' => Yii::t('app', 'Coat of arms'),
            'salary' => Yii::t('app', 'Average salary'),
            'population' => Yii::t('app', 'Population'),
            'density' => Yii::t('app', 'Population density'),
            'activities' => Yii::t('app', 'Main activities'),
            'is_large_city' => Yii::t('app', 'Is large city'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'title' => Yii::t('app', 'Title city'),
            'content' => Yii::t('app', 'Content'),
            'content_outdoor_ad' => Yii::t('app', 'Content Outdoor Ad'),
            'content_tv' => Yii::t('app', 'Content TV'),
            'content_ticker' => Yii::t('app', 'Content Ticker'),
            'content_radio' => Yii::t('app', 'Content Radio'),
            'slug' => Yii::t('app', 'Slug'),
            'title_outdoor_ad' => Yii::t('app', 'Title Outdoor Ad'),
            'title_tv' => Yii::t('app', 'Title TV'),
            'title_ticker' => Yii::t('app', 'Title Ticker'),
            'title_radio' => Yii::t('app', 'Title Radio'),
            'region_id' => Yii::t('app', 'Region'),
            'region' => Yii::t('app', 'Region'),
            'phone' => Yii::t('app', 'Phone'),
            'contact_text' => Yii::t('app', 'Text in Contacts page'),
            'where' => Yii::t('app', 'Where?'),
            'requisitesFile' => 'Реквизиты',
            'isNeedDeleteRequisitesFile' => 'Удалить файл реквизитов',
            'phone_digit' => 'Семизначные номера городских телефонов',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStreets()
    {
        return $this->hasMany(Street::class, ['city_id' => 'id']);
    }

    /**
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->image)) {
                $this->image->delete();
            }
            if (!empty($this->streets)) {
                foreach ($this->streets as $street) {
                    $street->delete();
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => true,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->salary = (float)$this->salary;

        $filePath = $this->getRequisitesFilePath();
        if (is_file($filePath)) {
            $this->requisitesFile = $this->getRequisitesFileName();
        }
    }

    /**
     * @return string
     */
    public function getRequisitesFilePath()
    {
        return Yii::getAlias(Yii::$app->params['requisites_path'] . $this->getRequisitesFileName());
    }

    /**
     * @return string
     */
    public function getRequisitesFileName()
    {
        return 'requisites_' . $this->slug . '.doc';
    }

    /**
     * Get phone
     * @return string
     */
    public function getPhone()
    {
        return empty($this->phone) ? Yii::$app->params['default_phone'] : $this->phone;
    }
}
