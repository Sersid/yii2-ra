<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%ticker}}".
 * @property integer    $id
 * @property integer    $city_id
 * @property integer    $tv_id
 * @property integer    $type
 * @property string     $price_comm
 * @property string     $price_no_comm
 * @property string     $price_text_block
 * @property integer    $status
 * @property integer    $sort
 * @property integer    $cnt_shows
 * @property boolean    $weekend
 * @property boolean    $x2
 * @property integer    $created_at
 * @property integer    $updated_at
 * @property City       $city
 * @property Tv         $tv
 * @property UserTicker $userTickers
 */
class Ticker extends ActiveRecord
{
    const TYPE_1 = 1;
    const TYPE_2 = 2;
    const TYPE_3 = 3;
    const TYPE_4 = 4;
    const TYPE_5 = 5;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ticker}}';
    }

    /**
     * @inheritdoc
     * @return TickerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TickerQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'tv_id', 'status', 'cnt_shows', 'sort'], 'required'],
            [['city_id', 'tv_id', 'cnt_shows', 'sort'], 'integer'],
            [['price_comm', 'price_no_comm', 'price_text_block'], 'number'],
            ['type', 'in', 'range' => array_keys($this->getTypes())],
            [['status', 'x2', 'weekend'], 'boolean'],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
            [
                ['tv_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Tv::class,
                'targetAttribute' => ['tv_id' => 'id'],
            ],
        ];
    }

    public function getTypes()
    {
        return [
            self::TYPE_1 => 1,
            self::TYPE_2 => 2,
            self::TYPE_3 => 3,
            self::TYPE_4 => 4,
            self::TYPE_5 => 5,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'tv_id' => Yii::t('app', 'TV'),
            'price_comm' => Yii::t('app', 'Commercial'),
            'price_no_comm' => Yii::t('app', 'Non-profit'),
            'price_text_block' => Yii::t('app', 'Text block'),
            'type' => Yii::t('app', 'Words calculation system'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'x2' => Yii::t('app', 'x2'),
            'weekend' => Yii::t('app', 'Weekend'),
            'cnt_shows' => Yii::t('app', 'Count Shows'),
        ];
    }

    /**
     * @return string
     */
    public function getPriceType()
    {
        if (!empty($this->price_comm) || !empty($this->price_no_comm)) {
            return 'ticker';
        }
        return 'text_block';
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id'])->orderBy(
            [
                'city.sort' => SORT_ASC,
                'city.id' => SORT_ASC,
            ]
        )->where(['city.status' => City::STATUS_ACTIVE]);
    }

    /**
     * @return ActiveQuery
     */
    public function getTv()
    {
        return $this->hasOne(Tv::class, ['id' => 'tv_id'])->where(['tv.status' => Tv::STATUS_ACTIVE]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTickers()
    {
        return $this->hasMany(UserTicker::class, ['id' => 'user_ticker_id'])->viaTable(
            'user_ticker_tv',
            ['ticker_id' => 'id']
        );
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->price_comm = $this->price_comm !== null ? (float)$this->price_comm : null;
        $this->price_no_comm = $this->price_no_comm !== null ? (float)$this->price_no_comm : null;
        $this->price_text_block = $this->price_text_block !== null ? (float)$this->price_text_block : null;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->userTickers)) {
                foreach ($this->userTickers as $userTicker) {
                    $userTicker->delete();
                }
            }
            return true;
        }
        return false;
    }
}
