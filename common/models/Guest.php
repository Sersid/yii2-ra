<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%guest}}".
 * @property Order[]         $orders
 * @property UserOutdoorAd[] $outdoors
 * @property UserRadioAd[]   $radios
 * @property UserTicker[]    $tickers
 * @property UserTvAd[]      $tv
 * @property integer         $id
 * @property string          $ip
 * @property integer         $created_at
 * @property integer         $updated_at
 */
class Guest extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%guest}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (!empty(Yii::$app->request->userIP)) {
            $this->ip = Yii::$app->request->userIP;
        }
    }

    /**
     * @return ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['guest_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOutdoors()
    {
        return $this->hasMany(UserOutdoorAd::class, ['guest_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRadios()
    {
        return $this->hasMany(UserRadioAd::class, ['guest_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTickers()
    {
        return $this->hasMany(UserTicker::class, ['guest_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTv()
    {
        return $this->hasMany(UserTvAd::class, ['guest_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (!empty($this->outdoors)) {
            foreach ($this->outdoors as $outdoor) {
                $outdoor->delete();
            }
        }

        if (!empty($this->tickers)) {
            foreach ($this->tickers as $ticker) {
                $ticker->delete();
            }
        }

        if (!empty($this->radios)) {
            foreach ($this->radios as $radio) {
                $radio->delete();
            }
        }

        if (!empty($this->tv)) {
            foreach ($this->tv as $tv) {
                $tv->delete();
            }
        }

        return parent::beforeDelete();
    }
}
