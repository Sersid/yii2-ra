<?php

namespace common\models;

use Throwable;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%outdoor_ad_side_image}}".
 * @property int              $id
 * @property int              $folder_id
 * @property string           $file_name
 * @property int              $side_id
 * @property string           $href
 * @property string           $content_type
 * @property int              $last_modified
 * @property int              $last_upload
 * @property int              $status
 * @property string           $created_at
 * @property string           $updated_at
 * @property YandexDiskFolder $folder
 * @property OutdoorAdSide    $side
 */
class OutdoorAdSideImage extends ActiveRecord
{
    /** @var string Шаблон имени файла.
     * Пример: SG.UM.860299.062 (Быстринская, 13 перек. Генерала Иванова).jpg => SG.UM.860299.062
     */
    const PATTERN_NAME = '/^([a-z]{2}\.[a-z]{2,}.[0-9]{1,}\.\d{1,}).*$/usi';

    /** @var int Ошибка при загрузке */
    const STATUS_ERROR_DOWNLOAD = 100;
    /** @var int Новая запись */
    const STATUS_NEW = 200;
    /** @var int Загружено */
    const STATUS_LOADED = 300;
    /** @var int Обновлен */
    const STATUS_UPDATED = 400;
    /** @var int Имя не по шаблону */
    const STATUS_ERROR_INCORRECT_NAME = 500;
    /** @var int Сторона не найдена */
    const STATUS_WARNING_SIDE_NOT_FOUND = 600;
    /** @var int Файл был удален */
    const STATUS_DELETED = 700;
    /** @var int Не является изображением */
    const STATUS_SKIP_NOT_IMAGE = 1000;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%outdoor_ad_side_image}}';
    }

    /**
     * Файл был пропущен
     * @return bool
     */
    public function isSkipped()
    {
        return in_array(
            $this->status,
            [
                self::STATUS_ERROR_INCORRECT_NAME,
                self::STATUS_WARNING_SIDE_NOT_FOUND,
                self::STATUS_DELETED,
                self::STATUS_SKIP_NOT_IMAGE,
            ]
        );
    }

    /**
     * Может быть загружен
     * @return bool
     */
    public function isCanDownload()
    {
        return in_array($this->status, self::getCanDownloadStatuses());
    }

    /**
     * Статусы изображений, которые можно загрузить (для фильтра)
     * @return array
     */
    public static function getCanDownloadStatuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_UPDATED,
            self::STATUS_LOADED,
            self::STATUS_ERROR_DOWNLOAD,
        ];
    }

    /**
     * Был загружен
     * @return bool
     */
    public function isSuccess()
    {
        return in_array($this->status, [self::STATUS_LOADED]);
    }

    /**
     * Была ошибка
     * @return bool
     */
    public function isError()
    {
        return in_array($this->status, [self::STATUS_ERROR_DOWNLOAD]);
    }

    /**
     * Файл будет пропущен
     * @return bool
     */
    public function isCanLoaded()
    {
        return in_array(
            $this->status,
            [
                self::STATUS_SKIP_NOT_IMAGE,
                self::STATUS_DELETED,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['folder_id', 'file_name', 'href', 'status'], 'required'],
            [['folder_id', 'side_id', 'status', 'last_modified'], 'integer'],
            [['href'], 'string'],
            [['file_name', 'content_type'], 'string', 'max' => 255],
            [['folder_id', 'file_name'], 'unique', 'targetAttribute' => ['folder_id', 'file_name']],
            [
                ['folder_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => YandexDiskFolder::class,
                'targetAttribute' => ['folder_id' => 'id'],
            ],
            [
                ['side_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OutdoorAdSide::class,
                'targetAttribute' => ['side_id' => 'id'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getRowCssClass()
    {
        switch ($this->status) {
            case static::STATUS_ERROR_INCORRECT_NAME:
            case static::STATUS_ERROR_DOWNLOAD:
                return 'list-group-item-danger';
                break;
            case static::STATUS_SKIP_NOT_IMAGE:
            case static::STATUS_DELETED:
                return 'text-muted';
                break;
            case static::STATUS_NEW:
                return 'list-group-item-info';
                break;
            case static::STATUS_LOADED:
                return 'list-group-item-success';
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        switch ($this->status) {
            case static::STATUS_NEW:
                return $this->getHtmlLabel('New', 'info', 'Новое изображение, требуется загрузить на сайт');
                break;
            case static::STATUS_WARNING_SIDE_NOT_FOUND:
                return $this->getHtmlLabel('Warning', 'warning', 'На сайте не найдена поверхность с данным номером');
                break;
            case static::STATUS_ERROR_INCORRECT_NAME:
                return $this->getHtmlLabel('Error', 'danger', 'Некорректное имя файла');
                break;
            case static::STATUS_ERROR_DOWNLOAD:
                return $this->getHtmlLabel('Error', 'danger', 'Ошибка при загрузке файла');
                break;
            case static::STATUS_SKIP_NOT_IMAGE:
                return $this->getHtmlLabel('Skip', 'default', 'Пропущено: не изображение');
                break;
            case static::STATUS_DELETED:
                return $this->getHtmlLabel('Deleted', 'default', 'Файл не был найден в Яндекс.Диске');
                break;
            case static::STATUS_LOADED:
                return $this->getHtmlLabel('Ok', 'success', 'Изображение загружено');
                break;
            case static::STATUS_UPDATED:
                return $this->getHtmlLabel('Updated', 'info', 'Изображение обновлено, требуется загрузить на сайт');
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * Возвращает html ярлык с подказкой
     *
     * @param string $label
     * @param string $class
     * @param string $title
     *
     * @return string
     */
    private function getHtmlLabel($label, $class, $title = '')
    {
        return Html::tag(
            'span',
            $label,
            [
                'class' => 'label label-' . $class,
                'data' => ['toggle' => 'tooltip', 'placement' => 'top'],
                'title' => $title,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'folder_id' => 'Папка',
            'file_name' => 'Название файла',
            'side_id' => 'Сторона',
            'href' => 'Путь к файлу',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getFolder()
    {
        return $this->hasOne(YandexDiskFolder::class, ['id' => 'folder_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSide()
    {
        return $this->hasOne(OutdoorAdSide::class, ['id' => 'side_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @param string $size
     *
     * @return string
     */
    public function getSrc($size = 'lg')
    {
        return '/photos/' . $this->getFileName($size) . '?' . $this->last_modified;
    }

    /**
     * Название локального файла
     *
     * @param string $size размер файла 'sm' или 'lg'
     *
     * @return string
     */
    public function getFileName($size)
    {
        try {
            $arFileName = [
                $this->side->parent->full_number,
                $this->side->side,
                $this->folder->season,
                $this->id,
                $size,
            ];
            return implode('_', $arFileName) . '.jpg';
        } catch (Throwable $e) {
            return '#';
        }
    }
}
