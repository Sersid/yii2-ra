<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_radio_ad_radio}}".
 * @property integer     $user_radio_ad_id
 * @property integer     $radio_ad_id
 * @property integer     $duration
 * @property integer     $views
 * @property integer     $days
 * @property integer     $created_at
 * @property integer     $updated_at
 * @property float       $price
 * @property float       $price_without_discount
 * @property RadioAd     $radioAd
 * @property UserRadioAd $userRadioAd
 */
class UserRadioAdRadio extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_radio_ad_radio}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_radio_ad_id', 'radio_ad_id'], 'required'],
            [['user_radio_ad_id', 'radio_ad_id', 'duration', 'views', 'days'], 'integer'],
            [
                ['radio_ad_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => RadioAd::class,
                'targetAttribute' => ['radio_ad_id' => 'id'],
            ],
            [
                ['user_radio_ad_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UserRadioAd::class,
                'targetAttribute' => ['user_radio_ad_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_radio_ad_id' => Yii::t('app', 'User Radio Ad ID'),
            'radio_ad_id' => Yii::t('app', 'Radio Ad ID'),
            'duration' => 'Длительность ролика в сек.',
            'views' => 'Кол-во прокатов ролика в день',
            'days' => 'Кол-во дней проката ролика',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getRadioAd()
    {
        return $this->hasOne(RadioAd::class, ['id' => 'radio_ad_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserRadioAd()
    {
        return $this->hasOne(UserRadioAd::class, ['id' => 'user_radio_ad_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
