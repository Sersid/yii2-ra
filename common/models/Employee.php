<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%employee}}".
 * @property int    $id
 * @property string $name
 * @property string $position
 * @property int    $photo_id
 * @property int    $status
 * @property int    $created_at
 * @property int    $updated_at
 * @property File   $photo
 */
class Employee extends ActiveRecord
{
    const STATUS_ENABLE = 100;
    const STATUS_DISABLE = 200;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%employee}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'photo_id', 'status'], 'required'],
            [['photo_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'position'], 'string', 'max' => 255],
            [
                ['photo_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['photo_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'position' => 'Должность',
            'photo_id' => 'Фотография',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(File::class, ['id' => 'photo_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
