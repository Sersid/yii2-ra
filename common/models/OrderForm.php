<?php
declare(strict_types=1);

namespace common\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator3;
use yii\base\Model;

class OrderForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $reCaptcha;

    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'filter', 'filter' => 'trim'],
            [['name', 'email', 'phone'], 'required'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            // [['reCaptcha'], ReCaptchaValidator3::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
        ];
    }
}
