<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%info}}".
 * @property integer      $id
 * @property string       $name
 * @property integer      $image_id
 * @property integer      $category_id
 * @property integer      $short_desc
 * @property integer      $full_desc
 * @property integer      $status
 * @property integer      $sort
 * @property integer      $created_at
 * @property integer      $updated_at
 * @property InfoCategory $category
 * @property File         $image
 */
class Info extends ActiveRecord
{
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%info}}';
    }

    /**
     * @inheritdoc
     * @return InfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InfoQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_desc', 'full_desc', 'slug'], 'filter', 'filter' => 'trim'],
            [['name', 'category_id', 'status', 'sort'], 'required'],
            [['image_id', 'category_id', 'sort'], 'integer'],
            [['short_desc', 'full_desc'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'boolean'],
            [
                ['category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InfoCategory::class,
                'targetAttribute' => ['category_id' => 'id'],
            ],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['image_id' => 'id'],
            ],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'image_id' => Yii::t('app', 'Image'),
            'category_id' => Yii::t('app', 'Category'),
            'short_desc' => Yii::t('app', 'Short description'),
            'full_desc' => Yii::t('app', 'Full description'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(InfoCategory::class, ['id' => 'category_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->image)) {
                $this->image->delete();
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => true,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }
}
