<?php

namespace common\models;

use Throwable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "user_ticker".
 * @property integer           $id
 * @property integer           $guest_id        Идентификатор гостя
 * @property integer           $type            Тип объявления (Коммерческое или Некомерческое)
 * @property integer           $city_id         Выбранный город в форме, для фильтрации телеканалов
 * @property integer           $show_all_cities Флаг, "Смотреть список всех телеканалов"
 * @property string            $message         Сообщение
 * @property integer           $status          Статус (Новое, В корзине, Заказан)
 * @property integer           $created_at      Дата создания
 * @property integer           $updated_at      Дата последнего обновления
 * @property City              $city            Привязка к городу
 * @property Guest             $guest           Привязка к гостю
 * @property UserTickerTv[]    $settings        Настройки для каждого телеканала
 * @property UserTickerPhone[] $phones          Номера телефонов
 * @property Ticker[]          $system          Системные настройки телеканала (сумма, город и т.д.)
 */
class UserTicker extends ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_BASKET = 2;
    const STATUS_ORDER = 3;
    const TYPE_COMM = 1;
    const TYPE_NOT_COMM = 2;
    public $channels = [];
    public $hidden_channels = [];
    public $new_phone;
    public $new_phone_type = UserTickerPhone::TYPE_MOBILE;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_ticker}}';
    }

    /**
     * @return ActiveQuery
     */
    public static function getModel()
    {
        return self::find()
            ->where(['guest_id' => Yii::$app->guest->id, 'user_ticker.status' => self::STATUS_NEW]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['show_all_cities'], 'boolean'],
            ['type', 'default', 'value' => static::TYPE_NOT_COMM],
            ['status', 'default', 'value' => static::STATUS_NEW],
            ['new_phone_type', 'default', 'value' => UserTickerPhone::TYPE_CITY],
            [['guest_id', 'type', 'status', 'new_phone_type'], 'required'],
            ['type', 'in', 'range' => array_keys($this->getTypes())],
            ['status', 'in', 'range' => array_keys($this->getStatuses())],
            ['new_phone_type', 'in', 'range' => array_keys(UserTickerPhone::getTypes())],
            [['message', 'new_phone'], 'filter', 'filter' => 'trim'],
            [['message', 'new_phone'], 'string'],
            [['channels', 'hidden_channels'], 'each', 'rule' => ['integer']],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
            [
                ['guest_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Guest::class,
                'targetAttribute' => ['guest_id' => 'id'],
            ],
        ];
    }

    /**
     * Types
     * @return array
     */
    public function getTypes()
    {
        return [
            static::TYPE_NOT_COMM => 'Информационное',
            static::TYPE_COMM => 'Рекламное',
        ];
    }

    /**
     * Statuses
     * @return array
     */
    public function getStatuses()
    {
        return [
            static::STATUS_NEW => 'Новая запись',
            static::STATUS_BASKET => 'В корзине',
            static::STATUS_ORDER => 'Заказан',
        ];
    }

    /**
     * Type name
     * @return string
     */
    public function getTypeName()
    {
        $types = $this->getTypes();
        return isset($types[$this->type]) ? $types[$this->type] : '-';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'guest_id' => Yii::t('app', 'Guest'),
            'type' => 'Тип',
            'city_id' => 'Город',
            'message' => 'Текст сообщения',
            'status' => Yii::t('app', 'Status'),
            'show_all_cities' => 'Смотреть список всех телеканалов',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGuest()
    {
        return $this->hasOne(Guest::class, ['id' => 'guest_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(UserTickerPhone::class, ['user_ticker_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(UserTickerTv::class, ['user_ticker_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->getOldAttribute('city_id') != $this->city_id) {
            $this->show_all_cities = false;
        }
        return parent::beforeSave($insert);
    }

    /**
     * Get items
     * @return array
     */
    public function getItems()
    {
        $items = [
            'items' => [],
            'total' => 0,
        ];
        foreach ($this->settings as $setting) {
            $setting->parent = $this;
            $row = [
                'ticker_id' => $setting->ticker_id,
                'user_ticker_id' => $this->id,
                'name' => $setting->getName(),
                'words' => $setting->getCntWords(),
                'view_type' => $setting->view_type,
                'view_types' => $setting->getViewTypes(),
                'date_from' => $setting->date_from,
                'date_to' => $setting->date_to,
                'cnt_days' => $setting->getCntDays(),
                'cnt_weekends' => $setting->getCntWeekends(),
                'price' => $setting->getPrice(),
                'price_type' => $setting->getPriceType(),
                'total_day' => $setting->getTotalDay(),
                'total' => $setting->getTotalPrice(),
                'error' => $setting->getError(),
            ];

            $items['items'][] = $row;
            $items['total'] += $row['total'];
        }

        return $items;
    }

    /**
     * @inheritdoc
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->phones)) {
                foreach ($this->phones as $phone) {
                    $phone->delete();
                }
            }
            if (!empty($this->settings)) {
                foreach ($this->settings as $setting) {
                    $setting->delete();
                }
            }
            return true;
        }
        return false;
    }
}
