<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%meta_tag}}".
 * @property integer $id
 * @property string  $title
 * @property string  $description
 * @property string  $url
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $status
 */
class MetaTag extends ActiveRecord
{
    const STATUS_ACTIVE = true;
    const STATUS_DISABLE = false;
    const STATUS_ENABLE = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%meta_tag}}';
    }

    /**
     * Get page title
     *
     * @param $url
     *
     * @return bool|mixed
     */
    public static function getTitle($url)
    {
        if ($model = self::getMetaTags($url)) {
            return $model->title;
        }

        return false;
    }

    /**
     * @param $url
     *
     * @return array|bool|ActiveRecord|null
     */
    public static function getMetaTags($url)
    {
        // отбрасываем лишние параметры
        if ($explode = explode('?', $url)) {
            $url = $explode[0];
        }

        if ($model = self::find()->where(['url' => $url, 'status' => self::STATUS_ACTIVE])->one()) {
            return $model;
        }

        return false;
    }

    /**
     * Get page description
     *
     * @param $url
     *
     * @return bool|mixed
     */
    public static function getDescription($url)
    {
        if ($model = self::getMetaTags($url)) {
            return $model->description;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'url'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['status'], 'boolean'],
            [['title'], 'string', 'max' => 128],
            [['description', 'url'], 'string', 'max' => 255],
            [['url'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'url' => Yii::t('app', 'Url'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            // если в адресе присутствует название домена, то обрезаем до относительного пути
            if ($parse_url = parse_url($this->url)) {
                $this->url = $parse_url['path'];
            }

            return true;
        }
        return false;
    }
}
