<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%form_sorry}}".
 * @property integer $id
 * @property string  $phone
 * @property integer $created_at
 * @property integer $updated_at
 */
class FormSorry extends ActiveRecord
{
    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%form_sorry}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'filter', 'filter' => 'trim'],
            [['phone'], 'required'],
            [['phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Ваш телефон (с кодом города)',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose(
            ['html' => 'sorry-html', 'text' => 'sorry-text'],
            ['model' => $this]
        )->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])->setTo(
            Yii::$app->params['adminEmail']
        )->setSubject('заказ')->send();
    }
}
