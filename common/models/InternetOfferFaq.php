<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%internet_offer_faq}}".
 *
 * @property int $offer_id
 * @property int $faq_id
 */
class InternetOfferFaq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%internet_offer_faq}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['offer_id', 'faq_id'], 'required'],
            [['offer_id', 'faq_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offer_id' => 'Offer ID',
            'faq_id' => 'Faq ID',
        ];
    }
}
