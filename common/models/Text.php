<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%texts}}".
 * @property string $key
 * @property string $value
 */
class Text extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%texts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['key'], 'string', 'max' => 255],
            [['key'], 'unique'],
            [['value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => 'Ключ',
            'value' => 'Значение',
        ];
    }
}
