<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_ticker_phone}}".
 * @property integer    $id
 * @property integer    $user_ticker_id
 * @property string     $phone
 * @property integer    $type
 * @property integer    $created_at
 * @property integer    $updated_at
 * @property UserTicker $userTicker
 */
class UserTickerPhone extends ActiveRecord
{
    const TYPE_CITY = 1;
    const TYPE_MOBILE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_ticker_phone}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'default', 'value' => static::TYPE_CITY],
            ['type', 'in', 'range' => array_keys($this->getTypes())],
            [['user_ticker_id', 'phone'], 'required'],
            [['user_ticker_id'], 'integer'],
            [['phone'], 'string', 'max' => 255],
            [
                ['user_ticker_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UserTicker::class,
                'targetAttribute' => ['user_ticker_id' => 'id'],
            ],
        ];
    }

    /**
     * Types
     * @return array
     */
    public static function getTypes()
    {
        return [
            static::TYPE_CITY => 'Городской',
            static::TYPE_MOBILE => 'Мобильный',
        ];
    }

    /**
     * Mask
     * @param integer $type
     * @param City $city
     *
     * @return string
     */
    public static function getMask($type, $city)
    {
        if ($type == self::TYPE_CITY) {
            if (is_null($city) || $city->phone_digit == City::SIX_DIGIT_PHONE) {
                return '99-99-99';
            } else {
                return '999-99-99';
            }
        } else {
            return '+7 (999) 999 99 99';
        }
    }

    /**
     * Placeholder
     * @param integer $type
     * @param City $city
     *
     * @return string
     */
    public static function getPlaceholder($type, $city)
    {
        if ($type == self::TYPE_CITY) {
            if (is_null($city) || $city->phone_digit == City::SIX_DIGIT_PHONE) {
                return '__-__-__';
            } else {
                return '___-__-__';
            }
        } else {
            return '+7 (___) ___ __ __';
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_ticker_id' => Yii::t('app', 'User Ticker ID'),
            'phone' => Yii::t('app', 'Phone'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTicker()
    {
        return $this->hasOne(UserTicker::class, ['id' => 'user_ticker_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
