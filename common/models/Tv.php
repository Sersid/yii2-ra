<?php
declare(strict_types=1);

namespace common\models;

use Throwable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "{{%tv}}".
 * @property int      $id
 * @property string   $name
 * @property int      $image_id
 * @property int      $status
 * @property int      $sort
 * @property int      $created_at
 * @property int      $updated_at
 * @property File     $image
 * @property Ticker[] $tickers
 * @property TvAd[]   $tvAds
 * @property File     $bigLogo
 * @property File     $bigBanner
 * @property string   $color_heading
 * @property string   $color_line
 * @property string   $color_potential_audience
 */
class Tv extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tv}}';
    }

    /**
     * @inheritdoc
     * @return TvQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TvQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'color_heading', 'color_line', 'color_potential_audience'], 'filter', 'filter' => 'trim'],
            [['name', 'image_id', 'status', 'sort'], 'required'],
            [['image_id', 'big_logo', 'big_banner', 'sort'], 'integer'],
            [['name', 'color_heading', 'color_line', 'color_potential_audience'], 'string', 'max' => 255],
            [['status'], 'boolean'],
            [['slug'], 'unique'],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['image_id' => 'id'],
            ],
            [
                ['big_logo'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['big_logo' => 'id'],
            ],
            [
                ['big_banner'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['big_banner' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'image_id' => Yii::t('app', 'Logo'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'slug' => Yii::t('app', 'Slug'),
            'big_logo' => 'Большой логотип',
            'big_banner' => 'Баннер',
            'color_heading' => 'Цвет заголовка',
            'color_line' => 'Цвет блока потенциальной аудитории',
            'color_potential_audience' => 'Цвет чисел потенциальной аудитории',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getBigLogo()
    {
        return $this->hasOne(File::class, ['id' => 'big_logo']);
    }

    /**
     * @return ActiveQuery
     */
    public function getBigBanner()
    {
        return $this->hasOne(File::class, ['id' => 'big_banner']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTickers()
    {
        return $this->hasMany(Ticker::class, ['tv_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTvAds()
    {
        return $this->hasMany(TvAd::class, ['tv_id' => 'id']);
    }

    /**
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->image)) {
                $this->image->delete();
            }
            if (!empty($this->bigLogo)) {
                $this->bigLogo->delete();
            }
            if (!empty($this->bigBanner)) {
                $this->bigBanner->delete();
            }
            if (!empty($this->tickers)) {
                foreach ($this->tickers as $ticker) {
                    $ticker->delete();
                }
            }
            if (!empty($this->tvAds)) {
                foreach ($this->tvAds as $tvAd) {
                    $tvAd->delete();
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => true,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }
}
