<?php

namespace common\models;

use Throwable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use Zelenin\yii\behaviors\Slug;

/**
 * This is the model class for table "{{%adv_case}}".
 * @property integer $id
 * @property string  $name
 * @property string  $slug
 * @property integer $image_id
 * @property string  $short_desc
 * @property string  $full_desc
 * @property string  $product
 * @property string  $customer
 * @property integer $city_id
 * @property string  $purpose
 * @property string  $html
 * @property string  $type
 * @property integer $status
 * @property integer $sort
 * @property integer $created_at
 * @property integer $updated_at
 * @property City    $city
 * @property File    $image
 */
class AdvCase extends ActiveRecord
{
    const STATUS_ACTIVE = 1;

    const TYPE_DEFAULT = 10;
    const TYPE_HTML = 20;

     const CATEGORY_SEO = 10;
     const CATEGORY_MARKETING = 20;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%adv_case}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_desc', 'full_desc', 'slug'], 'filter', 'filter' => 'trim'],
            [['name', 'image_id', 'short_desc', 'status', 'sort', 'type'], 'required'],
            [['image_id', 'city_id', 'sort', 'category', 'type'], 'integer'],
            [['short_desc', 'full_desc', 'html'], 'string'],
            [['name', 'slug', 'product', 'customer', 'purpose'], 'string', 'max' => 255],
            [['status'], 'boolean'],
            [['slug'], 'unique'],
            [['type'], 'in', 'range' => array_keys($this->getTypes())],
            [['category'], 'in', 'range' => array_keys($this->getCategories())],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['image_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'image_id' => Yii::t('app', 'Image'),
            'short_desc' => Yii::t('app', 'Short description'),
            'full_desc' => Yii::t('app', 'Full description'),
            'product' => Yii::t('app', 'Product'),
            'customer' => Yii::t('app', 'Customer Portrait'),
            'city_id' => Yii::t('app', 'City'),
            'purpose' => Yii::t('app', 'Purpose'),
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'html' => Yii::t('app', 'HTML'),
            'type' => Yii::t('app', 'Type'),
            'category' => Yii::t('app', 'Category'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }

    /**
     * @inheritdoc
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!empty($this->image)) {
                $this->image->delete();
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => Slug::class,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }

    /**
     * @return array|string[]
     */
    public function getTypes(): array
    {
        return [
            self::TYPE_DEFAULT => 'По-умолчанию',
            self::TYPE_HTML => 'Вставка html',
        ];
    }

    /**
     * @return array|string[]
     */
    public function getCategories(): array
    {
        return [
            self::CATEGORY_SEO => 'SEO',
            self::CATEGORY_MARKETING => 'Маркейтинг',
        ];
    }
}
