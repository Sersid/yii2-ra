<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_ticker_tv}}".
 * @property integer    $user_ticker_id
 * @property integer    $ticker_id
 * @property string     $date_from
 * @property string     $date_to
 * @property integer    $view_type
 * @property integer    $created_at
 * @property integer    $updated_at
 * @property Ticker     $system
 * @property UserTicker parent
 */
class UserTickerTv extends ActiveRecord
{
    const VIEW_TYPE_SMALL = 1;
    const VIEW_TYPE_LARGE = 2;

    public $parent;
    /**
     * @var integer Count Words
     */
    private $_cnt_words;
    /**
     * @var array Work Days and Weekends
     */
    private $_days;
    /**
     * @var float Price per day
     */
    private $_price_day;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_ticker_tv}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['view_type', 'default', 'value' => static::VIEW_TYPE_SMALL],
            ['date_from', 'default', 'value' => date('Y-m-d', time() + 2 * 24 * 60 * 60)],
            ['date_to', 'default', 'value' => date('Y-m-d', time() + 4 * 24 * 60 * 60)],
            [['user_ticker_id', 'ticker_id', 'date_from', 'date_to', 'view_type'], 'required'],
            [['view_type'], 'in', 'range' => [self::VIEW_TYPE_SMALL, self::VIEW_TYPE_LARGE]],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
            [
                ['ticker_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Ticker::class,
                'targetAttribute' => ['ticker_id' => 'id'],
            ],
            [
                ['user_ticker_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UserTicker::class,
                'targetAttribute' => ['user_ticker_id' => 'id'],
            ],
        ];
    }

    /**
     * Setting name
     * @return string
     */
    public function getName()
    {
        return $this->system->tv->name . ' - ' . $this->system->city->name;
    }

    /**
     * @return integer
     */
    public function getCntWeekends()
    {
        if ($this->_days === null) {
            $this->getCntDays();
        }

        return $this->_days['weekends'];
    }

    /**
     * @return integer
     */
    public function getCntDays()
    {
        if ($this->_days === null) {
            $days = $weekend = 0;
            if (!empty($this->date_from) && $this->date_to) {
                for ($i = strtotime($this->date_from); $i <= strtotime($this->date_to); $i = $i + 86400) {
                    if ($this->system->weekend) {
                        $w = date('w', $i);
                        if ($w == 0 || $w == 6) {
                            $weekend++;
                        } else {
                            $days++;
                        }
                    } else {
                        $days++;
                    }
                }
            }
            $this->_days = ['days' => $days, 'weekends' => $weekend];
        }

        return $this->_days['days'];
    }

    /**
     * Total price per one day
     * @return float
     */
    public function getTotalDay()
    {
        if ($this->system->type == Ticker::TYPE_5) {
            return $this->getPrice() * $this->view_type;
        } else {
            return $this->getPrice() * $this->getCntWords() * $this->view_type;
        }
    }

    /**
     * Get price per day
     * @return float
     */
    public function getPrice()
    {
        if ($this->_price_day === null) {
            if ($this->parent->type == UserTicker::TYPE_COMM) {
                $this->_price_day =
                    empty($this->system->price_comm) ? $this->system->price_text_block : $this->system->price_comm;
            } else {
                $this->_price_day = empty($this->system->price_no_comm) ? $this->system->price_text_block
                    : $this->system->price_no_comm;
            }
        }

        return $this->_price_day;
    }

    /**
     * Get Count Words
     * @return int
     */
    public function getCntWords()
    {
        if ($this->_cnt_words === null) {
            $this->_cnt_words = 0;
            switch ($this->system->type) {
                case Ticker::TYPE_1:
                    foreach (explode(' ', $this->parent->message) as $word) {
                        $word = trim($word);
                        if (!empty($word) && mb_strlen($word) > 2) {
                            $this->_cnt_words++;
                        }
                    }
                    $this->_cnt_words += count($this->parent->phones);
                    break;
                case Ticker::TYPE_2:
                    foreach (explode(' ', $this->parent->message) as $word) {
                        $word = trim($word);
                        if (!empty($word) && mb_strlen($word) > 1) {
                            $this->_cnt_words++;
                        }
                    }
                    $this->_cnt_words += count($this->parent->phones) * 2;
                    break;
                case Ticker::TYPE_3:
                    foreach (explode(' ', $this->parent->message) as $word) {
                        $word = trim($word);
                        if (!empty($word)) {
                            $this->_cnt_words++;
                        }
                    }
                    $this->_cnt_words += count($this->parent->phones) * 6;
                    break;
                case Ticker::TYPE_4:
                    $this->_cnt_words = mb_strlen($this->parent->message);
                    foreach ($this->parent->phones as $phone) {
                        $this->_cnt_words += mb_strlen($phone->phone) + 3;
                    }
                    break;
                case Ticker::TYPE_5:
                    foreach (explode(' ', $this->parent->message) as $word) {
                        $word = trim($word);
                        if (!empty($word)) {
                            $this->_cnt_words++;
                        }
                    }
                    $this->_cnt_words += count($this->parent->phones);
                    break;
            }
        }

        return $this->_cnt_words;
    }

    /**
     * Get price type
     * @return float
     */
    public function getPriceType()
    {
        return $this->system->getPriceType();
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        if ($this->system->type == Ticker::TYPE_5) {
            return $this->getCntWords() > 0 ? $this->getPrice() * $this->getCntDays() * $this->view_type : 0;
        } else {
            return $this->getPrice() * $this->getCntWords() * $this->getCntDays() * $this->view_type;
        }
    }

    /**
     * Error
     * @return string
     */
    public function getError()
    {
        if (empty($this->getPrice())) {
            return 'Размещение невозможно';
        }

        if ($this->system->type == 5 && $this->getCntWords() > 15) {
            return 'Объявление не должно превышать 15 слов';
        }
    }

    /**
     * View types
     * @return array|int
     */
    public function getViewTypes()
    {
        if ($this->system->x2) {
            return [
                self::VIEW_TYPE_SMALL => ($this->system->cnt_shows / 2) . ' - ' . $this->system->cnt_shows,
                self::VIEW_TYPE_LARGE => $this->system->cnt_shows . ' - ' . ($this->system->cnt_shows * 2),
            ];
        } else {
            return $this->system->cnt_shows;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_ticker_id' => Yii::t('app', 'User Ticker ID'),
            'ticker_id' => Yii::t('app', 'Ticker'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'view_type' => Yii::t('app', 'View Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSystem()
    {
        return $this->hasOne(Ticker::class, ['id' => 'ticker_id'])->orderBy(
            [
                'ticker.sort' => SORT_ASC,
                'id' => SORT_ASC,
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
