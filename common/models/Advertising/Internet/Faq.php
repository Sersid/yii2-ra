<?php
declare(strict_types=1);

namespace common\models\Advertising\Internet;

use yii\db\ActiveRecord;

/**
 * @property int    $id
 * @property string $question
 * @property string $answer
 * @property bool   $is_active
 * @property bool   $is_global
 * @property int    $sort
 * @property string $date_created
 * @property string $date_updated
 */
class Faq extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%internet_faq}}';
    }
}
