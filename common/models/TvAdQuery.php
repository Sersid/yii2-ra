<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[TvAd]].
 * @see TvAd
 */
class TvAdQuery extends ActiveQuery
{
    /**
     * @return TvAdQuery
     */
    public function active()
    {
        return $this->andWhere('[[tv_ad.status]]=1');
    }

    /**
     * @inheritdoc
     * @return TvAd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TvAd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
