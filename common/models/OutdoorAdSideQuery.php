<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[OutdoorAdSide]].
 * @see OutdoorAdSide
 */
class OutdoorAdSideQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return OutdoorAdSide[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OutdoorAdSide|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
