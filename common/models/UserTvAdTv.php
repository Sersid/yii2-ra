<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_tv_ad_tv}}".
 * @property integer  $user_tv_ad_id
 * @property integer  $tv_ad_id
 * @property integer  $duration
 * @property integer  $views
 * @property integer  $days
 * @property integer  $created_at
 * @property integer  $updated_at
 * @property float    $price
 * @property float    $price_without_discount
 * @property TvAd     $tvAd
 * @property UserTvAd $userTvAd
 */
class UserTvAdTv extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_tv_ad_tv}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_tv_ad_id', 'tv_ad_id'], 'required'],
            [['user_tv_ad_id', 'tv_ad_id', 'duration', 'views', 'days'], 'integer'],
            [
                ['tv_ad_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TvAd::class,
                'targetAttribute' => ['tv_ad_id' => 'id'],
            ],
            [
                ['user_tv_ad_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UserTvAd::class,
                'targetAttribute' => ['user_tv_ad_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_tv_ad_id' => Yii::t('app', 'User Tv Ad ID'),
            'tv_ad_id' => Yii::t('app', 'Tv Ad ID'),
            'duration' => 'Длительность ролика в сек.',
            'views' => 'Кол-во прокатов ролика в день',
            'days' => 'Кол-во дней проката ролика',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTvAd()
    {
        return $this->hasOne(TvAd::class, ['id' => 'tv_ad_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTvAd()
    {
        return $this->hasOne(UserTvAd::class, ['id' => 'user_tv_ad_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
