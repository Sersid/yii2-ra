<?php

namespace common\models;

use Throwable;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%yandex_disk_folder}}".
 * @property int                  $id
 * @property string               $name
 * @property string               $href
 * @property int                  $season
 * @property int                  $status
 * @property string               $created_at
 * @property string               $updated_at
 * @property OutdoorAdSideImage[] $files
 */
class YandexDiskFolder extends ActiveRecord
{
    /** @var int Лето */
    const SEASON_SUMMER = 10;
    /** @var int Зима */
    const SEASON_WINTER = 20;

    /** @var int Новая папка */
    const STATUS_NEW = 5;
    /** @var int Измененная папка */
    const STATUS_UPDATED = 8;
    /** @var int Папка найдена */
    const STATUS_OK = 10;
    /** @var int Папка не найдена или недоступна */
    const STATUS_ERROR = 20;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%yandex_disk_folder}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'season', 'status'], 'required'],
            [['name'], 'unique'],
            [['season', 'status'], 'integer'],
            ['season', 'in', 'range' => array_keys(static::getSeasons())],
            ['status', 'in', 'range' => array_keys(static::getStatuses())],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * Времена года
     * @return array
     */
    public static function getSeasons()
    {
        return [
            static::SEASON_SUMMER => 'Лето',
            static::SEASON_WINTER => 'Зима',
        ];
    }

    /**
     * Статусы
     * @return array
     */
    public static function getStatuses()
    {
        return [
            static::STATUS_NEW => [
                'label' => 'New',
                'css_class' => 'info',
                'name' => 'Новая папка',
            ],
            static::STATUS_UPDATED => [
                'label' => 'Edit',
                'css_class' => 'warning',
                'name' => 'Папка изменялась',
            ],
            static::STATUS_OK => [
                'label' => 'Оk',
                'css_class' => 'success',
                'name' => 'Папка найдена',
            ],
            static::STATUS_ERROR => [
                'label' => 'Error',
                'css_class' => 'danger',
                'name' => 'Ошибка. Папка не найдена',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->status != self::STATUS_ERROR;
    }

    /**
     * @return string
     */
    public function getNameHtml()
    {
        $return = Html::tag(
            'i',
            '',
            [
                'class' => 'fas fa-' . ($this->season == self::SEASON_SUMMER ? 'umbrella-beach' : 'snowflake'),
                'title' => $this->season == $this::SEASON_SUMMER ? 'Лето' : 'Зима',
            ]
        );
        $return .= ' ' . $this->getName();
        return $return;
    }

    /**
     * Имя папки
     * @return string
     */
    public function getName()
    {
        return '/' . $this->name . '/';
    }

    /**
     * Ярлык иконки
     * @return string
     */
    public function getStatusLabel()
    {
        $arStatus = array_fill_keys(['label', 'css_class', 'name'], null);
        $arStatus = self::getStatuses()[$this->status] ?: $arStatus;
        return $this->getHtmlLabel($arStatus['label'], $arStatus['css_class'], $arStatus['name']);
    }

    /**
     * Возвращает html ярлык с подказкой
     *
     * @param string $label
     * @param string $class
     * @param string $title
     *
     * @return string
     */
    private function getHtmlLabel($label, $class, $title = '')
    {
        return Html::tag(
            'span',
            $label,
            [
                'class' => 'label label-' . $class,
                'data' => ['toggle' => 'tooltip', 'placement' => 'top'],
                'title' => $title,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название папки',
            'href' => 'Путь к папке',
            'season' => 'Сезон',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(OutdoorAdSideImage::class, ['folder_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeDelete()
    {
        if (!empty($this->files)) {
            foreach ($this->files as $adSideImage) {
                $adSideImage->delete();
            }
        }
        return parent::beforeDelete();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // Имя
        $this->name = trim(trim($this->name), '/');
        // Генерируем путь к папке
        $this->href = implode(
            '/',
            array_map(
                function ($item) {
                    return str_replace('+', '%20', urlencode($item));
                },
                explode('/', trim(trim($this->name), '/'))
            )
        );
        return parent::beforeValidate();
    }

}
