<?php
declare(strict_types=1);

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Zelenin\yii\behaviors\Slug;

/**
 * @property int                 $id
 * @property string              $name
 * @property string              $slug
 * @property string              $icon
 * @property string|null         $intro_text
 * @property string|null         $page_title
 * @property string|null         $page_heading
 * @property string|null         $page_description
 * @property int                 $status Active
 * @property int                 $sort
 * @property int                 $created_at
 * @property int                 $updated_at
 * @property InternetSubOffer[]  $subOffers
 * @property InternetMoreOffer[] $moreOffers
 */
class InternetOffer extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%internet_offer}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'slug',
                    'icon',
                    'intro_text',
                    'page_title',
                    'page_heading',
                    'page_description',
                    'page_title_with_city',
                    'page_heading_with_city',
                    'page_description_with_city',
                    'tariff_title',
                    'tariff_description',
                    'tariff_low_item_title',
                    'tariff_low_item_description',
                    'tariff_mig_item_title',
                    'tariff_mig_item_description',
                    'tariff_high_item_title',
                    'tariff_high_item_description',
                ],
                'string',
            ],
            [['name', 'icon', 'status', 'sort'], 'required'],
            [['tariff_low_item_price', 'tariff_mig_item_price', 'tariff_high_item_price',], 'number'],
            [['status', 'sort'], 'integer'],
            [
                [
                    'name',
                    'slug',
                    'icon',
                    'page_title',
                    'page_heading',
                    'page_title_with_city',
                    'page_heading_with_city',
                ],
                'string',
                'max' => 255,
            ],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Символьный код',
            'icon' => 'Иконка',
            'intro_text' => 'Вступительный текст',
            'status' => 'Активность',
            'sort' => 'Сортировка',
            'page_title' => 'Title страницы',
            'page_description' => 'Description страницы',
            'page_heading' => 'H1 страницы',
            'page_title_with_city' => 'Title страницы с городом',
            'page_heading_with_city' => 'H1 страницы с городом',
            'page_description_with_city' => 'Description страницы с городом',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'tariff_title' => 'Заголовок в тарифах',
            'tariff_description' => 'Описание тарифов',
            'tariff_low_item_title' => 'Заголовок первого тарифа',
            'tariff_low_item_description' => 'Описание первого тарифа',
            'tariff_low_item_price' => 'Цена первого тарифа',
            'tariff_mig_item_title' => 'Заголовок второго тарифа',
            'tariff_mig_item_description' => 'Описание второго тарифа',
            'tariff_mig_item_price' => 'Цена второго тарифа',
            'tariff_high_item_title' => 'Заголовок третьего тарифа',
            'tariff_high_item_description' => 'Описание третьего тарифа',
            'tariff_high_item_price' => 'Цена третьего тарифа',
            'subOffers' => 'Что мы предлагаем',
            'moreOffers' => 'Дополнительно предлагаем',
            'faqList' => 'Ответы на часто задаваемые вопросы    ',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => Slug::class,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;',
            ],
        ];
    }

    public function getSubOffers()
    {
        return $this->hasMany(InternetSubOffer::class, ['id' => 'sub_offer_id'])
            ->viaTable('{{%internet_offer_sub_offer}}', ['offer_id' => 'id']);
    }

    public function getMoreOffers()
    {
        return $this->hasMany(InternetMoreOffer::class, ['id' => 'more_offer_id'])
            ->viaTable('{{%internet_offer_more_offer}}', ['offer_id' => 'id']);
    }

    public function getFaqList()
    {
        return $this->hasMany(\common\models\Advertising\Internet\Faq::class, ['id' => 'faq_id'])
            ->viaTable('{{%internet_offer_faq}}', ['offer_id' => 'id']);
    }
}
