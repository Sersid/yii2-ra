<?php

namespace common\models;

use sersid\fileupload\models\Model;

/**
 * Class File
 * @package common\models
 */
class File extends Model
{
    /**
     * Get photo src
     *
     * @param null $width
     * @param null $height
     * @param null $sharpen
     *
     * @return string
     */
    public function getSrc($width = null, $height = null, $sharpen = null)
    {
        $size = [];
        if ($width === 'original') {
            $size[] = $width;
        } else {
            if (!empty($width)) {
                $size[] = 'w' . $width;
            }
            if (!empty($height)) {
                $size[] = 'h' . $height;
            }
            if (!empty($sharpen)) {
                $size[] = 'sh' . $sharpen;
            }
        }

        $size = !empty($size) ? implode('_', $size) . '/' : null;
        return '/image/' . $this->id . '/' . $size . $this->code . '.' . $this->ext;
    }
}
