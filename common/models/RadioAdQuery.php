<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[RadioAd]].
 * @see RadioAd
 */
class RadioAdQuery extends ActiveQuery
{
    /**
     * @return RadioAdQuery
     */
    public function active()
    {
        return $this->andWhere('[[radio_ad.status]]=1');
    }

    /**
     * @inheritdoc
     * @return RadioAd[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RadioAd|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
