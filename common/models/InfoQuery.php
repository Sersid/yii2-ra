<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Info]].
 * @see Info
 */
class InfoQuery extends ActiveQuery
{
    /**
     * @return InfoQuery
     */
    public function active()
    {
        return $this->andWhere('[[info.status]]=1');
    }

    /**
     * @inheritdoc
     * @return Info[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Info|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
