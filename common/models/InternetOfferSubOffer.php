<?php
declare(strict_types=1);

namespace common\models;

use Yii;

/**
 * @property int $offer_id
 * @property int $sub_offer_id
 */
class InternetOfferSubOffer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%internet_offer_sub_offer}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['offer_id', 'sub_offer_id'], 'required'],
            [['offer_id', 'sub_offer_id'], 'integer'],
            [['offer_id', 'sub_offer_id'], 'unique', 'targetAttribute' => ['offer_id', 'sub_offer_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offer_id' => 'Offer ID',
            'sub_offer_id' => 'Sub Offer ID',
        ];
    }
}
