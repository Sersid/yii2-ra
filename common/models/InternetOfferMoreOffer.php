<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%internet_offer_more_offer}}".
 *
 * @property int $offer_id
 * @property int $more_offer_id
 */
class InternetOfferMoreOffer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%internet_offer_more_offer}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['offer_id', 'more_offer_id'], 'required'],
            [['offer_id', 'more_offer_id'], 'integer'],
            [['offer_id', 'more_offer_id'], 'unique', 'targetAttribute' => ['offer_id', 'more_offer_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'offer_id' => 'Offer ID',
            'more_offer_id' => 'More Offer ID',
        ];
    }
}
