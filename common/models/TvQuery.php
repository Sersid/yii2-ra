<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Tv]].
 * @see Tv
 */
class TvQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Tv[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tv|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
