<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "form_call_me".
 * @property integer $id
 * @property string  $name
 * @property string  $phone
 * @property string  $email
 */
class FormCallMe extends ActiveRecord
{
    /** @var string */
    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%form_call_me}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'filter', 'filter' => 'trim'],
            [['phone'], 'required'],
            ['email', 'email'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ваше имя',
            'phone' => 'Ваш телефон (с кодом города)',
            'email' => 'Ваш E-mail',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'reCaptcha' => 'Captcha',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose(
            ['html' => 'call-me-html', 'text' => 'call-me-text'],
            ['model' => $this]
        )->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])->setTo(
            Yii::$app->params['adminEmail']
        )->setSubject('заказ')->send();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
