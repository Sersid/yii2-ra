<?php
declare(strict_types=1);

namespace common\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator3;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%form_feedback}}".
 * @property integer $id
 * @property string  $phone
 * @property string  $name
 * @property string  $email
 * @property string  $message
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $ip
 */
class FormFeedback extends ActiveRecord
{
    public $reCaptcha;
    public $surname;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%form_feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'message', 'email'], 'filter', 'filter' => 'trim'],
            [['phone'], 'required'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            ['email', 'email'],
            [
                'email',
                'required',
                'when' => function ($model) {
                    return empty($model->phone);
                },
                'whenClient' => "function (attribute, value) {
                return $('#formfeedback-phone').val().length == 0;
            }",
            ],
            [['message', 'ip'], 'string'],
            [['reCaptcha'], ReCaptchaValidator3::class],
            [['surname'], 'filter', 'filter' => 'trim', 'on' => 'new'],
            [['surname'], 'string', 'max' => 255, 'on' => 'new'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Ваш телефон (с кодом города)',
            'name' => 'Ваше имя',
            'email' => 'Ваш E-mail',
            'message' => 'Комментарии или вопросы',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'reCaptcha' => 'Captcha',
            'surname' => 'Ваша фамилия',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose(
            ['html' => 'feedback-html', 'text' => 'feedback-text'],
            ['model' => $this]
        )->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])->setTo(
            Yii::$app->params['adminEmail']
        )->setSubject('заказ')->send();
    }
}
