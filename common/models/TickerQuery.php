<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Ticker]].
 * @see Ticker
 */
class TickerQuery extends ActiveQuery
{
    /**
     * @return TickerQuery
     */
    public function active()
    {
        return $this->andWhere('[[ticker.status]]=1');
    }

    /**
     * @inheritdoc
     * @return Ticker[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ticker|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
