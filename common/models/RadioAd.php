<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%radio_ad}}".
 * @property integer $id
 * @property integer $city_id
 * @property integer $radio_id
 * @property integer $tv_id
 * @property string  $price_ad_sec
 * @property string  $price_ad_show
 * @property float   $discount
 * @property integer $status
 * @property integer $sort
 * @property integer $created_at
 * @property integer $updated_at
 * @property City    $city
 * @property Radio   $radio
 */
class RadioAd extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%radio_ad}}';
    }

    /**
     * @inheritdoc
     * @return TvAdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RadioAdQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'radio_id', 'status', 'sort'], 'required'],
            [['sort'], 'integer'],
            [['status'], 'boolean'],
            [['price_ad_sec', 'price_ad_show', 'discount'], 'number'],
            [
                ['city_id', 'radio_id'],
                'unique',
                'targetAttribute' => ['city_id', 'radio_id'],
                'message' => 'The combination of City and Radio has already been taken.',
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => City::class,
                'targetAttribute' => ['city_id' => 'id'],
            ],
            [
                ['radio_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Radio::class,
                'targetAttribute' => ['radio_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'radio_id' => Yii::t('app', 'Radio'),
            'price_ad_sec' => Yii::t('app', 'Radio Ads - Sec'),
            'price_ad_show' => Yii::t('app', 'Radio Ads - Show'),
            'discount' => 'Скидка',
            'status' => Yii::t('app', 'Active'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRadio()
    {
        return $this->hasOne(Radio::class, ['id' => 'radio_id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->price_ad_sec = $this->price_ad_sec !== null ? (float)$this->price_ad_sec : null;
        $this->price_ad_show = $this->price_ad_show !== null ? (float)$this->price_ad_show : null;
        $this->discount = $this->discount !== null ? (float)$this->discount : null;
    }
}
