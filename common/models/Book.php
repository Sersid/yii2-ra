<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%book}}".
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $lvl
 * @property string  $name
 * @property string  $icon
 * @property integer $icon_type
 * @property boolean $active
 * @property boolean $selected
 * @property boolean $disabled
 * @property boolean $readonly
 * @property boolean $visible
 * @property boolean $collapsed
 * @property boolean $movable_u
 * @property boolean $movable_d
 * @property boolean $movable_l
 * @property boolean $movable_r
 * @property boolean $removable
 * @property boolean $removable_all
 */
class Book extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%book}}';
    }

    /**
     * @param      $slug
     * @param null $node_id
     *
     * @return bool|mixed
     */
    public static function getPage($slug, $node_id = null)
    {
        $query = Book::find()->where(['active' => true, 'slug' => $slug])->asArray()->one();

        $array[$query['id']] = $query;
        $array[$query['id']]['url'][] = '/book/view';
        $array[$query['id']]['url']['slug'] = $query['slug'];
        $array[$query['id']]['breadcrumbs'][] = ['label' => $query['name'], 'url' => $array[$query['id']]['url']];

        $data = self::recursion($array[$query['id']], $query['id']);

        if ($node_id) {
            $data[$query['id']] = $data;
            $data = self::find_page($data, $node_id);
        }

        return $data;
    }

    /**
     * @param $data
     * @param $parent_id
     *
     * @return mixed
     */
    public static function recursion(&$data, $parent_id)
    {
        if ($query =
            Book::find()->where(['active' => true, 'parent_id' => $parent_id])->orderBy('lft')->asArray()->all()) {
            foreach ($query as $value) {
                $data['children'][$value['id']] = $value;

                $url = $data['url'];
                $data['children'][$value['id']]['breadcrumbs'] = $data['breadcrumbs'];

                $url['node_id'] = $value['id'];
                $data['children'][$value['id']]['breadcrumbs'][] = ['label' => $value['name'], 'url' => $url];
                $data['children'][$value['id']]['url'] = $url;

                if (Book::find()->where(['active' => true, 'parent_id' => $value['id']])->exists()) {
                    $data['children'][$value['id']] = self::recursion($data['children'][$value['id']], $value['id']);
                }
            }

            return $data;
        }
    }

    public static function find_page(&$dataset, $id)
    {
        if (isset($dataset[$id])) {
            return $dataset[$id];
        }

        foreach ($dataset as $value) {
            if (isset($value['children'])) {
                $result = self::find_page($value['children'], $id);
                if ($result) {
                    return $result;
                } // вот он - выход!
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root', 'lft', 'rgt', 'lvl', 'icon_type', 'parent_id', 'image_id'], 'integer'],
            [['name'], 'required'],
            [
                [
                    'active',
                    'selected',
                    'disabled',
                    'readonly',
                    'visible',
                    'collapsed',
                    'movable_u',
                    'movable_d',
                    'movable_l',
                    'movable_r',
                    'removable',
                    'removable_all',
                ],
                'boolean',
            ],
            [['icon'], 'string', 'max' => 255],
            [['name', 'short_desc', 'full_desc'], 'string'],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => File::class,
                'targetAttribute' => ['image_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'root' => Yii::t('app', 'Root'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'lvl' => Yii::t('app', 'Lvl'),
            'name' => Yii::t('app', 'Name'),
            'icon' => Yii::t('app', 'Icon'),
            'icon_type' => Yii::t('app', 'Icon Type'),
            'active' => Yii::t('app', 'Active'),
            'selected' => Yii::t('app', 'Selected'),
            'disabled' => Yii::t('app', 'Disabled'),
            'readonly' => Yii::t('app', 'Readonly'),
            'visible' => Yii::t('app', 'Visible'),
            'collapsed' => Yii::t('app', 'Collapsed'),
            'movable_u' => Yii::t('app', 'Movable U'),
            'movable_d' => Yii::t('app', 'Movable D'),
            'movable_l' => Yii::t('app', 'Movable L'),
            'movable_r' => Yii::t('app', 'Movable R'),
            'removable' => Yii::t('app', 'Removable'),
            'removable_all' => Yii::t('app', 'Removable All'),
            'image_id' => Yii::t('app', 'Image'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'slug' => Yii::t('app', 'Slug'),
            'short_desc' => Yii::t('app', 'Short description'),
            'full_desc' => Yii::t('app', 'Full description'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'image_id']);
    }
}
