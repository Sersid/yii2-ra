<?php
declare(strict_types=1);

namespace common\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * @property int $tv_ad_id
 * @property float $men
 * @property float $women
 * @property float $youth
 * @property float $total
 */
class TvAdPotentialAudience extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tv_ad_potential_audience}}';
    }

    public function rules()
    {
        return [
            [['tv_ad_id'], 'required'],
            [['tv_ad_id'], 'integer'],
            [['men', 'women', 'youth', 'total'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tv_ad_id' => Yii::t('app', 'Tv Ad ID'),
            'men' => 'Мужчины',
            'women' => 'Женщины',
            'youth' => 'От 18 до 45 лет',
            'total' => 'Всего',
        ];
    }
}
