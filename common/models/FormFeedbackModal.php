<?php

namespace common\models;

/**
 * This is the model class for table "{{%form_feedback}}".
 * @property integer $id
 * @property string  $phone
 * @property string  $name
 * @property string  $email
 * @property string  $message
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $captcha
 * @property string  $ip
 */
class FormFeedbackModal extends FormFeedback
{
}
