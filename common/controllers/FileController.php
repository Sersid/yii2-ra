<?php

namespace common\controllers;

use Yii;
use yii\base\Exception;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class PhotoController
 * @package frontend\controllers
 */
class FileController extends Controller
{
    /**
     * @return \sersid\fileupload\File
     */
    private function _getFile()
    {
        return Yii::$app->file;
    }

    /**
     * Render photo
     *
     * @param      $code
     * @param      $id
     * @param      $ext
     * @param null $params
     *
     * @throws HttpException
     */
    public function actionRender($id, $code, $ext, $params = null)
    {
        if ($params !== null) {
            if ($params === 'original') {
                $params = [
                    'original' => true,
                ];
            } else {
                if (!preg_match('/^(w(\d{1,4}))?(_)?(h(\d{1,4}))?(_)?(sh(\d{1,2}))?+$/i', $params, $matches)) {
                    throw new HttpException(404, 'File not found');
                }

                $params = [
                    'width' => (int)$matches[2],
                    'height' => array_key_exists(5, $matches) ? (int)$matches[5] : 0,
                    'sharpen' => array_key_exists(8, $matches) ? (int)$matches[8] : 0,
                ];
            }
        }

        $this->_getFile()->renderImage($id, $code, $ext, $params);
    }

    /**
     * Delete photo
     *
     * @param $id   integer
     * @param $code string
     *
     * @return false|int
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id, $code)
    {
        $model = $this->_getFile()->model->findOne(['id' => $id]);
        if ($model === null || $model->code != $code) {
            throw new HttpException(404, 'File not found');
        }
        return $model->delete();
    }

    /**
     * Rotate photo
     *
     * @param $id
     * @param $code string
     * @param $route
     *
     * @return \yii\web\Response
     * @throws Exception
     * @throws HttpException
     */
    public function actionRotate($id, $code, $route)
    {
        if (!in_array($route, ['left', 'right'])) {
            throw new Exception('Wrong route value');
        }
        $route = ($route == 'left') ? -90 : 90;
        return $this->_getFile()->rotateImage($id, $code, $route);
    }

    /**
     * Upload photo from URL
     *
     * @param $url string
     *
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionUploadFromUrl($url)
    {
        $form = new \backend\models\PhotoUploadFromUrl();
        $form->url = $url;
        if ($form->validate()) {
            if (!$this->_getFile()->upload($form->url)) {
                return Json::encode(['error' => 'Error loading file']);
            }
            /* @var $photo \common\models\File */
            $photo = $this->_getFile()->model;
            return Json::encode(
                [
                    'error' => false,
                    'file' => [
                        'id' => $photo->id,
                        'size' => $photo->size,
                        'is_image' => $photo->is_image,
                        'code' => $photo->code,
                        'file_name' => $photo->file_name,
                        'width' => $photo->width,
                        'height' => $photo->height,
                        'src' => $photo->getSrc(),
                        'src_small' => $photo->getSrc(170, 170),
                    ],
                ]
            );
        }
        return Json::encode(['error' => $form->getFirstError('url')]);
    }

    /**
     * Upload photo
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionUpload()
    {
        $form = new \backend\models\PhotoUpload();

        $form->file = UploadedFile::getInstanceByName('file');
        if ($form->validate()) {
            if (!$this->_getFile()->upload($form->file)) {
                return Json::encode(
                    [
                        'error' => 'Error loading file',
                    ]
                );
            }
            /* @var $photo \common\models\File */
            $photo = $this->_getFile()->model;
            return Json::encode(
                [
                    'error' => false,
                    'file' => [
                        'id' => $photo->id,
                        'size' => $photo->size,
                        'is_image' => $photo->is_image,
                        'code' => $photo->code,
                        'file_name' => $photo->file_name,
                        'width' => $photo->width,
                        'height' => $photo->height,
                        'src' => $photo->getSrc(),
                        'src_small' => $photo->getSrc(170, 170),
                    ],
                ]
            );
        }
        return Json::encode(['error' => $form->getFirstError('file')]);
    }

    /**
     * Load file
     *
     * @param $id
     *
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionLoadFile($id)
    {
        /* @var $model \common\models\File */

        $model = $this->_getFile()->model;
        $photo = $model->findOne(['id' => $id]);
        if ($photo === null) {
            return Json::encode(['error' => 'Error loading file']);
        }

        return Json::encode(
            [
                'error' => false,
                'file' => [
                    'id' => $photo->id,
                    'size' => $photo->size,
                    'is_image' => $photo->is_image,
                    'code' => $photo->code,
                    'file_name' => $photo->file_name,
                    'width' => $photo->width,
                    'height' => $photo->height,
                    'src' => $photo->getSrc(),
                    'src_small' => $photo->getSrc(170, 170),
                ],
            ]
        );
    }
}
