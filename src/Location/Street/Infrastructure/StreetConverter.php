<?php
declare(strict_types=1);

namespace NikoM\Location\Street\Infrastructure;

use NikoM\Location\Street\Domain\DTO\StreetDTO;

class StreetConverter
{
    public function fromArrayToDTO(array $data): StreetDTO
    {
        $dto = new StreetDTO();
        if (isset($data['id'])) {
            $dto->id = (int)$data['id'];
        }
        if (isset($data['name'])) {
            $dto->name = $data['name'];
        }

        return $dto;
    }
}
