<?php
declare(strict_types=1);

namespace NikoM\Location\Street\Domain\Entity;

use NikoM\Location\Street\Domain\DTO\StreetDTO;

class Street
{
    private StreetId $id;
    private StreetName $name;

    private function __construct()
    {
    }

    public static function fromDTO(StreetDTO $dto): self
    {
        $self = new self();
        $self->id = new StreetId($dto->id);
        if (isset($dto->name)) {
            $self->name = new StreetName($dto->name);
        }

        return $self;
    }

    public function getId(): StreetId
    {
        return $this->id;
    }

    public function getName(): StreetName
    {
        return $this->name;
    }
}
