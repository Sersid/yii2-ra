<?php
declare(strict_types=1);

namespace NikoM\Location\Street\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;

class StreetName extends StringValueObject
{
}
