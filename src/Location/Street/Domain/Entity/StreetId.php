<?php
declare(strict_types=1);

namespace NikoM\Location\Street\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;

class StreetId extends IntValueObject
{
}
