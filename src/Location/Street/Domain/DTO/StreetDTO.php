<?php
declare(strict_types=1);

namespace NikoM\Location\Street\Domain\DTO;

class StreetDTO
{
    public int $id;
    public string $name;
}
