<?php
declare(strict_types=1);

namespace NikoM\Location\Region\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;

class RegionId extends IntValueObject
{
}
