<?php
declare(strict_types=1);

namespace NikoM\Location\Region\Domain\Entity;

use NikoM\Location\City\Domain\Entity\CityCollection;
use NikoM\Location\Region\Domain\DTO\RegionDTO;

class Region
{
    private RegionId $id;
    private RegionName $name;
    private CityCollection $cityCollection;

    private function __construct()
    {
    }

    public static function fromDTO(RegionDTO $dto): self
    {
        $self = new self();
        $self->id = new RegionId($dto->id);
        $self->name = new RegionName($dto->name);
        if (isset($dto->cities)) {
            $self->cityCollection = CityCollection::fromDTO(...$dto->cities);
        }

        return $self;
    }

    public function getId(): RegionId
    {
        return $this->id;
    }

    public function getName(): RegionName
    {
        return $this->name;
    }

    public function isEqualTo(self $region): bool
    {
        return $this->getId()->isEqual($region->getId());
    }
}
