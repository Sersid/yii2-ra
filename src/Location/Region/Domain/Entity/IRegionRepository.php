<?php
declare(strict_types=1);

namespace NikoM\Location\Region\Domain\Entity;

use NikoM\Location\Region\Domain\Exception\RegionNotFoundException;

interface IRegionRepository
{
    public function add(Region $region): void;

    /**
     * @param RegionId $id
     *
     * @return Region
     * @throws RegionNotFoundException
     */
    public function getById(RegionId $id): Region;
    public function save(Region $region): void;
    public function delete(RegionId $regionId): void;
}
