<?php
declare(strict_types=1);

namespace NikoM\Location\Region\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;

class RegionName extends StringValueObject
{
}
