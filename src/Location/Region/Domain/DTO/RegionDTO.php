<?php
declare(strict_types=1);

namespace NikoM\Location\Region\Domain\DTO;

use NikoM\Location\City\Domain\DTO\CityDto;

class RegionDTO
{
    public int $id;
    public string $name;
    /** @var CityDto[] */
    public array $cities;
}
