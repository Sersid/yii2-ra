<?php
declare(strict_types=1);

namespace NikoM\Location\Region\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\NotFoundException;

class RegionNotFoundException extends NotFoundException
{
    public function message(): string
    {
        return 'Region not found';
    }
}
