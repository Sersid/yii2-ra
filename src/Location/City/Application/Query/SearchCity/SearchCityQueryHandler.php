<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Query\SearchCity;

use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use NikoM\Location\City\Domain\Entity\ICityFetcher;

class SearchCityQueryHandler
{
    private ICityFetcher $fetcher;

    public function __construct(ICityFetcher $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    public function handle(SearchCityQuery $query): RegionViewCollection
    {
        return $this->fetcher->findAll($query->query);
    }
}
