<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Query\SearchCity;

class SearchCityQuery
{
    public ?string $query;
}
