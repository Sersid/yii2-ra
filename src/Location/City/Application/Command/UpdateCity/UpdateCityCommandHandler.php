<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\UpdateCity;

use InvalidArgumentException;
use NikoM\Location\City\Application\Command\CityCommandHandler;
use NikoM\Location\City\Domain\Entity\CityId;
use NikoM\Location\City\Domain\Entity\CityName;
use NikoM\Location\City\Domain\Entity\CityRegion;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;
use NikoM\Location\City\Domain\Exception\CityNotFoundException;
use NikoM\Location\City\Domain\Exception\NameAlreadyExistException;
use NikoM\Location\City\Domain\Exception\SlugAlreadyExistException;
use NikoM\Location\Region\Domain\Entity\RegionId;
use NikoM\Location\Region\Domain\Exception\RegionNotFoundException;

class UpdateCityCommandHandler extends CityCommandHandler
{
    /**
     * @param UpdateCityCommand $command
     *
     * @throws InvalidArgumentException
     * @throws CityNotFoundException
     * @throws NameAlreadyExistException
     * @throws SlugAlreadyExistException
     * @throws RegionNotFoundException
     */
    public function handle(UpdateCityCommand $command)
    {
        $id = new CityId($command->id);
        $this->city = $this->cityRepository->getById($id);

        if (isset($command->name)) {
            $this->rename($command->name);
        }
        if (isset($command->slug)) {
            $this->changeSlug($command->slug);
        }
        if (isset($command->regionId)) {
            $this->changeRegion($command->regionId);
        }

        $this->cityRepository->save($this->city);
    }

    private function rename(string $name): void
    {
        $name = new CityName($name);
        if ($this->city->getName()->isEqual($name)) {
            return;
        }
        $this->throwIfNotUniqueByName($name);
        $this->city->rename($name);
    }

    private function changeSlug(string $slug): void
    {
        $slug = new CitySlug($slug);
        if ($this->city->getSlug()->isEqual($slug)) {
            return;
        }
        $this->throwIfNotUniqueBySlug($slug);
        $this->city->changeSlug($slug);
    }

    private function changeRegion(?int $regionId): void
    {
        $region = is_null($regionId) ? null : $this->regionRepository->getById(new RegionId($regionId));
        $this->city->changeRegion(new CityRegion($region));
    }
}
