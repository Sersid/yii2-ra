<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\UpdateCity;

class UpdateCityCommand
{
    public int $id;
    public string $name;
    public string $slug;
    public ?int $regionId;
}
