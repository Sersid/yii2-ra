<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\DeleteCity;

class DeleteCityCommand
{
    public int $id;
}
