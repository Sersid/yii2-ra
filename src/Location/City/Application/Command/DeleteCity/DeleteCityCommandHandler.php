<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\DeleteCity;

use NikoM\Location\City\Application\Command\CityCommandHandler;
use NikoM\Location\City\Domain\Entity\CityId;
use NikoM\Location\City\Domain\Exception\CityNotFoundException;

class DeleteCityCommandHandler extends CityCommandHandler
{
    /**
     * @param DeleteCityCommand $command
     *
     * @throws CityNotFoundException
     */
    public function handler(DeleteCityCommand $command): void
    {
        $id = new CityId($command->id);
        if (!$this->cityFetcher->hasById($id)) {
            throw new CityNotFoundException();
        }
        $this->cityRepository->delete($id);
    }
}
