<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\AddCity;

class AddCityCommand
{
    public string $name;
    public ?string $slug;
    public ?int $regionId;
}
