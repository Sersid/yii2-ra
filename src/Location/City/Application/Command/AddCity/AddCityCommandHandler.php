<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\AddCity;

use InvalidArgumentException;
use NikoM\Location\City\Application\Command\CityCommandHandler;
use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\City\Domain\Entity\CityName;
use NikoM\Location\City\Domain\Entity\CityRegion;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;
use NikoM\Location\City\Domain\Exception\NameAlreadyExistException;
use NikoM\Location\City\Domain\Exception\SlugAlreadyExistException;
use NikoM\Location\Region\Domain\Entity\Region;
use NikoM\Location\Region\Domain\Entity\RegionId;
use NikoM\Location\Region\Domain\Exception\RegionNotFoundException;

class AddCityCommandHandler extends CityCommandHandler
{
    /**
     * @param AddCityCommand $command
     *
     * @throws InvalidArgumentException
     * @throws NameAlreadyExistException
     * @throws SlugAlreadyExistException
     * @throws RegionNotFoundException
     */
    public function handle(AddCityCommand $command): void
    {
        $name = new CityName($command->name);
        $slug = is_null($command->slug) ? CitySlug::fromString($command->name) : new CitySlug($command->slug);
        $region = new CityRegion($this->getRegionEntity($command->regionId));

        $this->throwIfNotUniqueByName($name);
        $this->throwIfNotUniqueBySlug($slug);

        $this->city = City::create($name, $slug, $region);
        $this->cityRepository->add($this->city);

        $this->eventDispatcher->dispatch($this->city->releaseEvents());
    }

    /**
     * @param int|null $regionId
     *
     * @return Region|null
     * @throws RegionNotFoundException
     */
    private function getRegionEntity(?int $regionId): ?Region
    {
        if (is_null($regionId)) {
            return null;
        } else {
            return $this->regionRepository->getById(new RegionId($regionId));
        }
    }
}
