<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\ClearRegion;

use NikoM\Location\City\Application\Command\CityCommandHandler;
use NikoM\Location\City\Domain\Exception\RegionAlreadyClearedException;
use NikoM\Location\Region\Domain\Entity\RegionId;

class ClearRegionCommandHandler extends CityCommandHandler
{
    public function handle(ClearRegionCommand $command): void
    {
        $regionId = new RegionId($command->regionId);
        $cityCollection = $this->cityRepository->getAllByRegionId($regionId);
        foreach ($cityCollection->all() as $city)
        {
            if ($city->getRegion()->isEmpty()) {
                throw new RegionAlreadyClearedException($city);
            }
            $city->clearRegion();
            $this->cityRepository->save($city);
            $this->eventDispatcher->dispatch($city->releaseEvents());
        }
    }
}
