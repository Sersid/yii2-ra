<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command\ClearRegion;

class ClearRegionCommand
{
    public int $regionId;
}
