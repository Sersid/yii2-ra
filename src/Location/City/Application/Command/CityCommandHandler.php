<?php
declare(strict_types=1);

namespace NikoM\Location\City\Application\Command;

use NikoM\Kernel\Domain\Aggregate\EventDispatcher;
use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\City\Domain\Entity\CityName;
use NikoM\Location\City\Domain\Entity\ICityFetcher;
use NikoM\Location\City\Domain\Entity\ICityRepository;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;
use NikoM\Location\City\Domain\Exception\NameAlreadyExistException;
use NikoM\Location\City\Domain\Exception\SlugAlreadyExistException;
use NikoM\Location\Region\Domain\Entity\IRegionRepository;

abstract class CityCommandHandler
{
    protected ICityRepository $cityRepository;
    protected ICityFetcher $cityFetcher;
    protected IRegionRepository $regionRepository;
    protected City $city;
    protected EventDispatcher $eventDispatcher;

    public function __construct(
        ICityRepository $cityRepository,
        ICityFetcher $cityFetcher,
        IRegionRepository $regionRepository,
        EventDispatcher $eventDispatcher
    )
    {
        $this->cityRepository = $cityRepository;
        $this->regionRepository = $regionRepository;
        $this->cityFetcher = $cityFetcher;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @param CityName $name
     * @throws NameAlreadyExistException
     */
    protected function throwIfNotUniqueByName(CityName $name)
    {
        if ($this->cityFetcher->hasByName($name)) {
            throw new NameAlreadyExistException($name);
        }
    }

    /**
     * @param CitySlug $slug
     * @throws SlugAlreadyExistException
     */
    protected function throwIfNotUniqueBySlug(CitySlug $slug)
    {
        if ($this->cityFetcher->hasBySlug($slug)) {
            throw new SlugAlreadyExistException($slug);
        }
    }
}
