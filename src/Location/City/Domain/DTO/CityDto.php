<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\DTO;

use NikoM\Kernel\Domain\Dto;
use NikoM\Location\Region\Domain\DTO\RegionDTO;

class CityDto extends Dto
{
    public int $id;
    public string $name;
    public string $slug;
    public string $where;
    public ?string $subTitle;
    public ?RegionDTO $region;
}
