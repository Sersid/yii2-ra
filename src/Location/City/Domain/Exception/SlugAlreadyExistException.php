<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\AlreadyExistException;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

class SlugAlreadyExistException extends AlreadyExistException
{
    public function __construct(CitySlug $slug)
    {
        parent::__construct('Slug "' . $slug . '" already exist');
    }
}
