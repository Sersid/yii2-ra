<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\AlreadyExistException;
use NikoM\Location\City\Domain\Entity\City;

class RegionAlreadyClearedException extends AlreadyExistException
{
    public function __construct(City $city)
    {
        parent::__construct('Region has already been cleared in the city "' . $city->getName() . '"');
    }
}
