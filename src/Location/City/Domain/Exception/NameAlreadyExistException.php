<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\AlreadyExistException;
use NikoM\Location\City\Domain\Entity\CityName;

class NameAlreadyExistException extends AlreadyExistException
{
    public function __construct(CityName $name)
    {
        parent::__construct('Name "' . $name . '" already exist');
    }
}
