<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\NotFoundException;

class CityNotFoundException extends NotFoundException
{
    public function message(): string
    {
        return 'City not found';
    }
}
