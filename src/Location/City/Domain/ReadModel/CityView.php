<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\ReadModel;

use NikoM\Location\City\Domain\DTO\CityDto;

class CityView
{
    private int $id;
    private string $name;
    private string $slug;
    private string $where;
    private ?string $subTitle;

    public function __construct(CityDto $dto)
    {
        if ($dto->hasProperty('id')) {
            $this->id = $dto->id;
        }
        if ($dto->hasProperty('name')) {
            $this->name = $dto->name;
        }
        if ($dto->hasProperty('slug')) {
            $this->slug = $dto->slug;
        }
        if ($dto->hasProperty('where')) {
            $this->where = $dto->where;
        }
        if ($dto->hasProperty('subTitle')) {
            $this->subTitle = $dto->subTitle;
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function slug(): string
    {
        return $this->slug;
    }

    public function where(): string
    {
        return $this->where;
    }

    public function url(): string
    {
        return '/cities/' . $this->slug;
    }

    public function subTitle(): ?string
    {
        return $this->subTitle;
    }
}
