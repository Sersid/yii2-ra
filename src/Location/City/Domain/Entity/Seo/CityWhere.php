<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Seo;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringNullableValueObject;

class CityWhere extends StringNullableValueObject
{
}
