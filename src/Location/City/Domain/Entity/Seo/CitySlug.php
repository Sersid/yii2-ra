<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Seo;

use NikoM\Kernel\Domain\Entity\ValueObject\Slug\SlugValueObject;

class CitySlug extends SlugValueObject
{
}
