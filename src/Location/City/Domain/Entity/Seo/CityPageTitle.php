<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Seo;

use NikoM\Kernel\Domain\Entity\ValueObject\PageTitle\PageTitleNullableValueObject;

class CityPageTitle extends PageTitleNullableValueObject
{
}
