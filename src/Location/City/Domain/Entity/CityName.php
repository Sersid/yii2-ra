<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;

class CityName extends StringValueObject
{
}
