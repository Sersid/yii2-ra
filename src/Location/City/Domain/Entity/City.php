<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Kernel\Domain\Aggregate\AggregateRoot;
use NikoM\Kernel\Domain\Aggregate\EventTrait;
use NikoM\Location\City\Domain\DTO\CityDto;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;
use NikoM\Location\City\Domain\Event\CityCreatedEvent;
use NikoM\Location\City\Domain\Event\CityRegionChangedEvent;
use NikoM\Location\City\Domain\Event\CityRegionClearedEvent;
use NikoM\Location\City\Domain\Event\CityRenamedEvent;
use NikoM\Location\City\Domain\Event\CitySlugChangedEvent;
use NikoM\Location\Region\Domain\Entity\Region;

class City implements AggregateRoot
{
    use EventTrait;

    private CityId $id;
    private CityName $name;
    private ICityRegion $region;
    private CitySlug $slug;
    private CityUrl $url;
    private CityStatus $status;
    private CitySort $sort;
    private CitySetting $setting;
    private CitySeo $seo;
    private CityStatistic $statistic;

    private function __construct() {}

    public static function create(
        CityName $name,
        ICityRegion $region,
        CitySlug $slug,
        CityStatus $status,
        CitySort $sort,
        CitySeo $seo,
        CitySetting $setting,
        CityStatistic $statistic
    ): self {
        $self = new self();
        $self->name = $name;
        $self->region = $region;
        $self->slug = $slug;
        $self->status = $status;
        $self->sort = $sort;
        $self->seo = $seo;
        $self->setting = $setting;
        $self->statistic = $statistic;
        $self->url = new CityUrl($slug);
        $self->recordEvent(new CityCreatedEvent($self));

        return $self;
    }

    public static function fromDTO(CityDto $dto): self
    {
        $self = new self();
        $self->id = new CityId($dto->id);
        if (isset($dto->name)) {
            $self->name = new CityName($dto->name);
        }
        if (isset($dto->region)) {
            $region = is_null($dto->region) ? null : Region::fromDTO($dto->region);
            $self->region = new CityRegion($region);
        }
        if (isset($dto->slug)) {
            $self->slug = new CitySlug($dto->slug);
            $self->url = new CityUrl($self->slug);
        }
        if (isset($dto->seo)) {
            $self->seo = CitySeo::fromDTO($dto->seo);
        }

        return $self;
    }

    public function getId(): CityId
    {
        return $this->id;
    }

    public function getSlug(): CitySlug
    {
        return $this->slug;
    }

    public function getUrl(): CityUrl
    {
        return $this->url;
    }

    public function changeSlug(CitySlug $slug): void
    {
        if (!$this->getSlug()->isEqual($slug)) {
            $this->recordEvent(new CitySlugChangedEvent($this, $this->getSlug(), $slug));
            $this->slug = $slug;
        }
    }

    public function getName(): CityName
    {
        return $this->name;
    }

    public function rename(CityName $name): void
    {
        if (!$this->name->isEqual($name)) {
            $this->recordEvent(new CityRenamedEvent($this, $this->name, $name));
            $this->name = $name;
        }
    }

    public function getRegion(): ICityRegion
    {
        return $this->region;
    }

    public function clearRegion(): void
    {
        if ($this->region->isNotEmpty()) {
            $this->changeRegion(new CityRegion(null));
        }
    }

    public function changeRegion(ICityRegion $region): void
    {
        if (!$this->region->isEqualTo($region)) {
            if ($region->isEmpty()) {
                $this->recordEvent(new CityRegionClearedEvent($this));
            }
            $this->recordEvent(new CityRegionChangedEvent($this, $this->region, $region));
            $this->region = $region;
        }
    }

    public function getStatus(): CityStatus
    {
        return $this->status;
    }

    public function changeStatus(CityStatus $status): void
    {
        if (!$this->status->isEqual($status)) {
            $this->status = $status;
        }
    }

    public function getSetting(): CitySetting
    {
        return $this->setting;
    }

    public function getSeo(): CitySeo
    {
        return $this->seo;
    }

    public function getStatistic(): CityStatistic
    {
        return $this->statistic;
    }

    public function getSort(): CitySort
    {
        return $this->sort;
    }

    public function changeSort(CitySort $sort): void
    {
        if (!$this->sort->isEqual($sort)) {
            $this->sort = $sort;
        }
    }
}
