<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Location\City\Domain\Entity\Statistic\CityAverageSalary;
use NikoM\Location\City\Domain\Entity\Statistic\CityIsLarge;
use NikoM\Location\City\Domain\Entity\Statistic\CityMainActivity;
use NikoM\Location\City\Domain\Entity\Statistic\CityPopulation;
use NikoM\Location\City\Domain\Entity\Statistic\CityPopulationDensity;

class CityStatistic
{
    private CityAverageSalary $averageSalary;
    private CityPopulation $population;
    private CityPopulationDensity $populationDensity;
    private CityMainActivity $mainActivity;
    private CityIsLarge $isLarge;

    public function __construct(
        CityAverageSalary $averageSalary,
        CityPopulation $population,
        CityPopulationDensity $populationDensity,
        CityMainActivity $mainActivity,
        CityIsLarge $isLarge
    )
    {
        $this->averageSalary = $averageSalary;
        $this->population = $population;
        $this->populationDensity = $populationDensity;
        $this->mainActivity = $mainActivity;
        $this->isLarge = $isLarge;
    }

    public function getAverageSalary(): CityAverageSalary
    {
        return $this->averageSalary;
    }

    public function getPopulation(): CityPopulation
    {
        return $this->population;
    }

    public function getPopulationDensity(): CityPopulationDensity
    {
        return $this->populationDensity;
    }

    public function getMainActivity(): CityMainActivity
    {
        return $this->mainActivity;
    }

    public function getIsLarge(): CityIsLarge
    {
        return $this->isLarge;
    }
}
