<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Setting;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringNullableValueObject;

class CityPhone extends StringNullableValueObject
{
}
