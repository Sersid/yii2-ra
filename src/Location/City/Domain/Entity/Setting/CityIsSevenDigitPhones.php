<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Setting;

use NikoM\Kernel\Domain\Entity\ValueObject\Bool\BoolValueObject;

class CityIsSevenDigitPhones extends BoolValueObject
{
}
