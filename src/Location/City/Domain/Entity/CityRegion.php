<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Location\Region\Domain\Entity\Region;

class CityRegion implements ICityRegion
{
    private ?Region $entity;

    public function __construct(?Region $region)
    {
        $this->entity = $region;
    }

    public function getId(): int
    {
        return $this->entity->getId()->getValue();
    }

    public function getName(): string
    {
        return $this->entity->getName()->getValue();
    }

    public function isEmpty(): bool
    {
        return is_null($this->entity);
    }

    public function isNotEmpty(): bool
    {
        return !$this->isEmpty();
    }

    public function isEqualTo(ICityRegion $other): bool
    {
        if ($this->isEmpty() && $other->isEmpty()) {
            return true;
        }
        if ($this->isNotEmpty() && $other->isNotEmpty() && $this->getId() === $other->getId()) {
            return true;
        }
        return false;
    }
}
