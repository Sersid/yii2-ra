<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

interface ICityRegion
{
    public function getId(): int;
    public function getName(): string;
    public function isEmpty(): bool;
    public function isNotEmpty(): bool;
    public function isEqualTo(self $other): bool;
}
