<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use NikoM\Location\City\Domain\DTO\CityDto;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

interface ICityFetcher
{
    public function findAll(?string $search): RegionViewCollection;
    public function hasById(CityId $id): bool;
    public function hasByName(CityName $name): bool;
    public function hasBySlug(CitySlug $slug): bool;
    public function findBySlug(CitySlug $slug): ?CityDto;
    public function getBySlug(CitySlug $slug): CityDto;
}
