<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Location\City\Domain\Exception\CityNotFoundException;
use NikoM\Location\Region\Domain\Entity\RegionId;

interface ICityRepository
{
    public function add(City $city): void;
    public function save(City $city): void;
    public function delete(CityId $id): void;
    /**
     * @param CityId $id
     *
     * @return City
     * @throws CityNotFoundException
     */
    public function getById(CityId $id): City;
    public function getAllByRegionId(RegionId $regionId): CityCollection;
}
