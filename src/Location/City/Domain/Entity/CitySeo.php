<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Location\City\Domain\DTO\CitySeoDTO;
use NikoM\Location\City\Domain\Entity\Seo\CityPageTitle;
use NikoM\Location\City\Domain\Entity\Seo\CityWhere;

class CitySeo
{
    private CityWhere $where;
    private CityPageTitle $pageTitle;

    private function __construct()
    {
    }

    public static function create(CityWhere $where, CityPageTitle $pageTitle): self
    {
        $self = new self();
        $self->where = $where;
        $self->pageTitle = $pageTitle;

        return $self;
    }

    public static function fromDTO(CitySeoDTO $dto): self
    {
        $self = new self();
        if (isset($dto->where)) {
            $self->where = new CityWhere($dto->where);
        }
        if (isset($dto->pageTitle)) {
            $self->pageTitle = new CityPageTitle($dto->pageTitle);
        }

        return $self;
    }

    public function getWhere(): CityWhere
    {
        return $this->where;
    }

    public function setWhere(CityWhere $where): void
    {
        $this->where = $where;
    }

    public function getPageTitle(): CityPageTitle
    {
        return $this->pageTitle;
    }

    public function setPageTitle(CityPageTitle $pageTitle): void
    {
        $this->pageTitle = $pageTitle;
    }
}
