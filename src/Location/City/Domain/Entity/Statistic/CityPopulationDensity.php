<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Statistic;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntNullableValueObject;

/**
 * Плотность населения
 */
class CityPopulationDensity extends IntNullableValueObject
{
}
