<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Statistic;
use NikoM\Kernel\Domain\Entity\ValueObject\String\StringNullableValueObject;

/**
 * Основные виды деятельности
 */
class CityMainActivity extends StringNullableValueObject
{
}
