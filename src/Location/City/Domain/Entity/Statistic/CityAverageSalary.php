<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity\Statistic;

use NikoM\Kernel\Domain\Entity\ValueObject\Money\MoneyNullableValueObject;

/**
 * Средняя заработная плата
 */
class CityAverageSalary extends MoneyNullableValueObject
{
}
