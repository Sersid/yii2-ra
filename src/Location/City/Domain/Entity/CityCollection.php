<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Kernel\Domain\Entity\Collection\Collection;
use NikoM\Location\City\Domain\DTO\CityDto;

/**
 * @method City[] all()
 */
class CityCollection extends Collection
{
    protected static function type(): string
    {
        return City::class;
    }

    public static function fromDTO(CityDto ...$cityDTOList): self
    {
        $cities = [];
        foreach ($cityDTOList as $cityDTO) {
            $cities[] = City::fromDTO($cityDTO);
        }

        return new self($cities);
    }
}
