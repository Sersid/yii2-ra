<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

class CityUrl extends StringValueObject
{
    public function __construct(CitySlug $slug)
    {
        $value = '/cities/' . $slug->getValue();
        parent::__construct($value);
    }
}
