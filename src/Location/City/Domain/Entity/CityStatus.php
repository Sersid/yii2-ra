<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Bool\BoolValueObject;

class CityStatus extends BoolValueObject
{
}
