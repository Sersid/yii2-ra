<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Entity;

use NikoM\Location\City\Domain\Entity\Setting\CityIsSevenDigitPhones;
use NikoM\Location\City\Domain\Entity\Setting\CityPhone;

class CitySetting
{
    private CityPhone $phone;
    private CityIsSevenDigitPhones $isSevenDigitPhones;

    public function getPhone(): CityPhone
    {
        return $this->phone;
    }

    public function getIsSevenDigitPhones(): CityIsSevenDigitPhones
    {
        return $this->isSevenDigitPhones;
    }
}
