<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Event;

use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\City\Domain\Entity\ICityRegion;

class CityRegionChangedEvent
{
    private City $city;
    private ICityRegion $oldRegion;
    private ICityRegion $newRegion;

    public function __construct(City $city, ICityRegion $oldRegion, ICityRegion $newRegion)
    {
        $this->city = $city;
        $this->oldRegion = $oldRegion;
        $this->newRegion = $newRegion;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function getOldRegion(): ICityRegion
    {
        return $this->oldRegion;
    }

    public function getNewRegion(): ICityRegion
    {
        return $this->newRegion;
    }
}
