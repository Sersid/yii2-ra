<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Event;

use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

class CitySlugChangedEvent
{
    private City $city;
    private CitySlug $oldSlug;
    private CitySlug $newSlug;

    public function __construct(City $city, CitySlug $oldSlug, CitySlug $newSlug)
    {
        $this->city = $city;
        $this->oldSlug = $oldSlug;
        $this->newSlug = $newSlug;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function getOldSlug(): CitySlug
    {
        return $this->oldSlug;
    }

    public function getNewSlug(): CitySlug
    {
        return $this->newSlug;
    }
}
