<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Event;

use NikoM\Location\City\Domain\Entity\City;

class CityRegionClearedEvent
{
    private City $city;

    public function __construct(City $city)
    {
        $this->city = $city;
    }

    public function getCity(): City
    {
        return $this->city;
    }
}
