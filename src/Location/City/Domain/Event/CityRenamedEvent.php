<?php
declare(strict_types=1);

namespace NikoM\Location\City\Domain\Event;

use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\City\Domain\Entity\CityName;

class CityRenamedEvent
{
    private City $city;
    private CityName $oldName;
    private CityName $newName;

    public function __construct(City $city, CityName $oldName, CityName $newName)
    {
        $this->city = $city;
        $this->oldName = $oldName;
        $this->newName = $newName;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function getOldName(): CityName
    {
        return $this->oldName;
    }

    public function getNewName(): CityName
    {
        return $this->newName;
    }
}
