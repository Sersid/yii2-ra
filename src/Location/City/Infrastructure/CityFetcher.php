<?php
declare(strict_types=1);

namespace NikoM\Location\City\Infrastructure;

use common\models\City;
use common\models\Region;
use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use NikoM\Location\City\Domain\DTO\CityDto;
use NikoM\Location\City\Domain\Entity\CityId;
use NikoM\Location\City\Domain\Entity\CityName;
use NikoM\Location\City\Domain\Entity\ICityFetcher;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;
use NikoM\Location\City\Domain\Exception\CityNotFoundException;

class CityFetcher implements ICityFetcher
{
    private CityConverter $cityConverter;

    public function __construct(CityConverter $cityConverter)
    {
        $this->cityConverter = $cityConverter;
    }

    public function findAll(?string $search): RegionViewCollection
    {
        $query = City::find()
            ->select([
                'id' => 'city.id',
                'name' => 'city.name',
                'slug' => 'city.slug',
                'region_id',
                'region_name' => 'region.name',
            ])
            ->where(['city.status' => City::STATUS_ACTIVE, 'region.status' => Region::STATUS_ACTIVE])
            ->joinWith('region')
            ->orderBy(['region.sort' => SORT_ASC, 'city.name' => SORT_ASC])
            ->asArray();
        if (!empty($search)) {
            $query->andFilterWhere(['like', 'city.name', $search]);
        }
        $result = [];
        foreach ($query->all() as $city) {
            $result[$city['region_id']]['name'] = (string)$city['region_name'];
            $result[$city['region_id']]['cities'][] = [
                'id' => (int)$city['id'],
                'name' => $city['name'],
                'slug' => $city['slug'],
            ];
        }

        return RegionViewCollection::createFromArray($result);
    }

    public function hasById(CityId $id): bool
    {
        // TODO: Implement hasById() method.
    }

    public function hasByName(CityName $name): bool
    {
        // TODO: Implement hasByName() method.
    }

    public function hasBySlug(CitySlug $slug): bool
    {
        // TODO: Implement hasBySlug() method.
    }

    public function findBySlug(CitySlug $slug): ?CityDto
    {
        $data = City::find()
            ->select([
                'id' => 'city.id',
                'name' => 'city.name',
                'slug' => 'city.slug',
                'where' => 'city.where',
            ])
            ->where(['city.status' => City::STATUS_ACTIVE, 'slug' => $slug->getValue()])
            ->asArray()
            ->one();

        if (empty($data)) {
            return null;
        } else {
            return $this->cityConverter->fromArrayToDTO($data);
        }
    }

    public function getBySlug(CitySlug $slug): CityDto
    {
        $cityDto = $this->findBySlug($slug);
        if ($cityDto === null) {
            throw new CityNotFoundException();
        }

        return $cityDto;
    }
}
