<?php
declare(strict_types=1);

namespace NikoM\Location\City\Infrastructure;

use NikoM\Location\City\Domain\DTO\CityDto;

class CityConverter
{
    public function fromArrayToDTO(array $data): CityDto
    {
        $dto = new CityDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('name', $data)) {
            $dto->name = $data['name'];
        }
        if (array_key_exists('slug', $data)) {
            $dto->slug = $data['slug'];
        }
        if (array_key_exists('where', $data)) {
            $dto->where = $data['where'];
        }
        if (array_key_exists('content_tv', $data)) {
            $dto->subTitle = $data['content_tv'];
        }

        return $dto;
    }
}
