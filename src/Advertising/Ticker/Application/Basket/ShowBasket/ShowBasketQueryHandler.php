<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Application\Basket\ShowBasket;

use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\Basket;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\BasketRepository;

class ShowBasketQueryHandler
{
    private BasketRepository $repository;

    public function __construct(BasketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(ShowBasketQuery $query): Basket
    {
        $buyerId = new BuyerId($query->buyerId);
        try {
            return $this->repository->getByBuyer($buyerId);
        } catch (BasketNotFoundException $exception) {
            return Basket::create($buyerId);
        }
    }
}
