<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Application\Basket\ShowBasket;

class ShowBasketQuery
{
    public int $buyerId;
}
