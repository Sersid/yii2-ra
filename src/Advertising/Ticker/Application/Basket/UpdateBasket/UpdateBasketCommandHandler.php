<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Application\Basket\UpdateBasket;

use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\Basket;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\AdvType;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\BasketRepository;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\Text;
use NikoM\Advertising\Ticker\Domain\Basket\Service\BasketService;

class UpdateBasketCommandHandler
{
    private BasketService $service;
    private BasketRepository $repository;

    public function __construct(BasketService $service, BasketRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function handle(UpdateBasketCommand $command): Basket
    {
        $basket = $this->service->getOrCreate(new BuyerId($command->buyerId));
        $advType = new AdvType($command->advType);
        if (!$basket->getAdvType()->isEqual($advType)) {
            $basket->changeAdvType($advType);
        }
        $text = new Text($command->text);
        if (!$basket->getText()->isEqual($text)) {
            $basket->changeText($text);
        }
        $this->repository->save($basket);

        return $basket;
    }
}
