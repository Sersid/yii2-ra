<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Application\Basket\UpdateBasket;

class UpdateBasketCommand
{
    public int $buyerId;
    public int $advType;
    public string $text;
}
