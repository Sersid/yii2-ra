<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Application\City\ShowCities;

use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use NikoM\Advertising\Ticker\Domain\City\CityFetcher;

class ShowCitiesQueryHandler
{
    private CityFetcher $fetcher;

    public function __construct(CityFetcher $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    public function handle(ShowCitiesQuery $query): RegionViewCollection
    {
        return $this->fetcher->all();
    }
}
