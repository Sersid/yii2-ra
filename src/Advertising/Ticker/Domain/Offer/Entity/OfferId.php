<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Offer\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;

class OfferId extends IntValueObject
{
}
