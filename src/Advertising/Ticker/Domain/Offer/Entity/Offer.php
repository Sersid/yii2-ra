<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Offer\Entity;

use JsonSerializable;

class Offer implements JsonSerializable
{
    private OfferId $id;
    private string $name;
    private OfferWordCalcSystem $wordCalcSystem;

    private function __construct()
    {
    }

    public static function fromArray(array $data): self
    {
        $self = new self();
        $self->id = new OfferId((int) $data['id']);
        $self->name = $data['tv']['name'] . ' - ' . $data['city']['name'];
        $self->wordCalcSystem = new OfferWordCalcSystem((int)$data['type']);

        return $self;
    }

    public function getWordCalcSystem(): OfferWordCalcSystem
    {
        return $this->wordCalcSystem;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id->getValue(),
            'name' => $this->name,
        ];
    }
}
