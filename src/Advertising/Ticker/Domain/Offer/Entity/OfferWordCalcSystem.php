<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Offer\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class OfferWordCalcSystem extends IntValueObject
{
    private const TYPE_1 = 1;
    private const TYPE_2 = 2;
    private const TYPE_3 = 3;
    private const TYPE_4 = 4;
    private const TYPE_5 = 5;

    public function __construct(int $value)
    {
        Assert::inArray($value, [self::TYPE_1, self::TYPE_2, self::TYPE_3, self::TYPE_4, self::TYPE_5]);
        parent::__construct($value);
    }

    public function isType1(): bool
    {
        return $this->value === self::TYPE_1;
    }

    public function isType2(): bool
    {
        return $this->value === self::TYPE_2;
    }

    public function isType3(): bool
    {
        return $this->value === self::TYPE_3;
    }

    public function isType4(): bool
    {
        return $this->value === self::TYPE_4;
    }

    public function isType5(): bool
    {
        return $this->value === self::TYPE_5;
    }
}
