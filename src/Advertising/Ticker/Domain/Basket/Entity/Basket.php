<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use JsonSerializable;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketAdvTypeAlreadyException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Ticker\Domain\Basket\Exception\BasketTextNotChangedException;

class Basket implements JsonSerializable
{
    private BuyerId $buyerId;
    private AdvType $advType;
    private Text $text;
    private Phones $phones;
    private Items $items;

    private function __construct()
    {
    }

    public function __clone()
    {
        $this->advType = clone $this->advType;
        $this->text = clone $this->text;
        $this->phones = clone $this->phones;
        $this->items = clone $this->items;
    }

    public static function create(BuyerId $buyerId): self
    {
        $self = new self();
        $self->buyerId = $buyerId;
        $self->advType = AdvType::default();
        $self->text = new Text("");
        $self->phones = new Phones();
        $self->items = new Items();

        return $self;
    }

    public static function fromArray(array $data): self
    {
        $self = new self();
        $self->buyerId = new BuyerId((int)$data['guest_id']);
        $self->advType = new AdvType((int)$data['type']);
        $self->text = new Text($data['message']);
        $phones = [];
        if (!empty($data['phones'])) {
            foreach ($data['phones'] as $phone) {
                $phones[] = new Phone((int)$phone['type'], $phone['phone']);
            }
        }
        $self->phones = new Phones(...$phones);
        $items = [];
        if (!empty($data['settings'])) {
            foreach ($data['settings'] as $item) {
                $items[] = Item::fromArray($item, $self->text, $self->phones);
            }
        }
        $self->items = new Items(...$items);

        return $self;
    }

    public function getAdvType(): AdvType
    {
        return $this->advType;
    }

    public function changeAdvType(AdvType $advType)
    {
        if ($this->advType->isEqual($advType)) {
            throw new BasketAdvTypeAlreadyException();
        }
        $this->advType = $advType;
    }

    public function getText(): Text
    {
        return $this->text;
    }

    public function changeText(Text $text): void
    {
        if ($this->text->isEqual($text)) {
            throw new BasketTextNotChangedException();
        }
        $this->text = $text;
    }

    public function jsonSerialize(): array
    {
        return [
            'advType' => $this->advType,
            'text' => $this->text,
            'phones' => $this->phones,
            'items' => $this->items,
        ];
    }
}
