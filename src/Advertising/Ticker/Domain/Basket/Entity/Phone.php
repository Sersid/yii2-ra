<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use JsonSerializable;
use Webmozart\Assert\Assert;

class Phone implements JsonSerializable
{
    private const CITY = 1;
    private const MOBILE = 2;
    private int $type;
    private string $value;

    public function __construct(int $type, string $value)
    {
        Assert::inArray($type, [self::CITY, self::MOBILE]);
        $this->type = $type;

        $this->value = $value;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function isEqualTo(self $other): bool
    {
        return $this->getValue() === $other->getValue() && $this->type === $other->getType();
    }

    public function jsonSerialize(): array
    {
        return [
            'type' => $this->type,
            'value' => $this->value,
        ];
    }
}
