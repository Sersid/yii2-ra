<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use DateTimeImmutable;
use JsonSerializable;
use NikoM\Advertising\Ticker\Domain\Offer\Entity\Offer;

class Item implements JsonSerializable
{
    private Offer $offer;
    private Period $period;
    private Text $text;
    private Phones $phones;
    private int $countWords;
    private ?float $price;

    private function __construct()
    {
    }

    public static function create(Offer $offer, Text $text, Phones $phones): Item
    {
        $self = new self();
        $self->offer = $offer;
        $self->text = $text;
        $self->period = Period::default();
        $self->phones = $phones;
        $self->calculateCountWords();
        $self->calculatePrice();

        return $self;
    }

    private function calculateCountWords()
    {
        $this->countWords = 0;
        $offerWordCalcSystem = $this->offer->getWordCalcSystem();
        switch (true) {
            case $offerWordCalcSystem->isType1():
                foreach ($this->text->getWords() as $word) {
                    if (mb_strlen($word) > 2) {
                        $this->countWords++;
                    }
                }
                $this->countWords += count($this->phones);
                break;
            case $offerWordCalcSystem->isType2():
                foreach ($this->text->getWords() as $word) {
                    if (mb_strlen($word) > 1) {
                        $this->countWords++;
                    }
                }
                $this->countWords += count($this->phones) * 2;
                break;
            case $offerWordCalcSystem->isType3():
                $this->countWords += count($this->text->getWords());
                $this->countWords += count($this->phones) * 6;
                break;
            case $offerWordCalcSystem->isType4():
                $this->countWords = (int)mb_strlen(implode(" ", $this->text->getWords()));
                foreach ($this->phones as $phone) {
                    $this->countWords += mb_strlen($phone->getValue()) + 3;
                }
                break;
            case $offerWordCalcSystem->isType5():
                $this->countWords += count($this->text->getWords());
                $this->countWords += count($this->phones);
                break;
        }
    }

    private function calculatePrice()
    {
        $this->price = 0;
    }

    public static function fromArray(array $data, Text $text, Phones $phones): self
    {
        $self = new self();
        $self->offer = Offer::fromArray($data['system']);
        $dateFrom = new DateTimeImmutable($data['date_from']);
        $dateTo = new DateTimeImmutable($data['date_to']);
        $self->period = new Period($dateFrom, $dateTo);
        $self->text = $text;
        $self->phones = $phones;

        $self->calculateCountWords();
        $self->calculatePrice();

        return $self;
    }

    public function jsonSerialize(): array
    {
        return [
            'offer' => $this->offer,
            'period' => $this->period,
            'countWords' => $this->countWords,
            'price' => $this->price,
        ];
    }
}
