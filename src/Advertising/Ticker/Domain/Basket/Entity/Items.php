<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

class Items
{
    private array $items;

    public function __construct(Item ...$items)
    {
        $this->items = $items;
    }
}
