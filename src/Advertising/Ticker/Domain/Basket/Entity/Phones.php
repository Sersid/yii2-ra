<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use Countable;
use JsonSerializable;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Ticker\Domain\Basket\Exception\BasketPhoneAlreadyExistsException;

class Phones implements Countable, JsonSerializable
{
    private array $phones;

    public function __construct(Phone ...$phones)
    {
        $this->phones = $phones;
    }

    public function add(Phone $phone)
    {
        foreach ($this->phones as $item) {
            if ($item->isEqualTo($phone)) {
                throw new BasketPhoneAlreadyExistsException();
            }
        }
    }

    public function remove(int $index): Phone
    {
        if (!isset($this->phones[$index])) {
            throw new BasketNotFoundException();
        }
        $phone = $this->phones[$index];
        unset($this->phones[$index]);

        return $phone;
    }

    public function get(): array
    {
        return $this->phones;
    }

    public function count(): int
    {
        return count($this->phones);
    }

    public function jsonSerialize(): array
    {
        return $this->phones;
    }
}
