<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;

class Text extends StringValueObject
{
    private array $words;

    public function __construct(string $value)
    {
        $value = trim($value);
        parent::__construct($value);

        $this->words = [];
        foreach (explode(' ', $value) as $word) {
            $word = trim($word);
            if (!empty($word)) {
                $this->words[] = $word;
            }
        }
    }

    /**
     * @return string[]
     */
    public function getWords(): array
    {
        return $this->words;
    }
}
