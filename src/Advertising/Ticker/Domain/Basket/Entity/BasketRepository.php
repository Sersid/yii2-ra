<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

interface BasketRepository
{
    /**
     * @param BuyerId $buyerId
     *
     * @return Basket
     * @throws BasketNotFoundException
     */
    public function getByBuyer(BuyerId $buyerId): Basket;

    public function add(Basket $basket): void;

    public function save(Basket $basket): void;
}
