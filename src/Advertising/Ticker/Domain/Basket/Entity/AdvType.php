<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class AdvType extends IntValueObject
{
    private const INFO = 1;
    private const ADV = 2;

    public function __construct(int $value)
    {
        Assert::inArray($value, [self::INFO, self::ADV]);
        parent::__construct($value);
    }

    public static function default(): self
    {
        return new self(self::INFO);
    }

    public function isInfo(): bool
    {
        return $this->value === self::INFO;
    }

    public function isAdv(): bool
    {
        return $this->value === self::ADV;
    }
}
