<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Entity;

use DateTimeImmutable;
use JsonSerializable;

class Period implements JsonSerializable
{
    private DateTimeImmutable $dateFrom;
    private DateTimeImmutable $dateTo;

    public function __construct(DateTimeImmutable $dateFrom, DateTimeImmutable $dateTo)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public static function default(): self
    {
        $dateFrom = new DateTimeImmutable('+2 days');
        $dateTo = new DateTimeImmutable('+4 days');

        return new self($dateFrom, $dateTo);
    }

    public function jsonSerialize(): array
    {
        return [
            'dateFrom' => $this->dateFrom->format('Y-m-d'),
            'dateTo' => $this->dateTo->format('Y-m-d'),
        ];
    }
}
