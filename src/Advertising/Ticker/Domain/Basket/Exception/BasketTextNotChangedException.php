<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class BasketTextNotChangedException extends DomainException
{
    public function message(): string
    {
        return 'Text not changed';
    }

    public function code(): int
    {
        return 400;
    }
}
