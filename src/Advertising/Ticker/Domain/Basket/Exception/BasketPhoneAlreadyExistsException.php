<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class BasketPhoneAlreadyExistsException extends DomainException
{
    public function message(): string
    {
        return 'Phone already exist';
    }

    public function code(): int
    {
        return 400;
    }
}
