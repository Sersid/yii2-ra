<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class BasketPhoneNotFoundException extends DomainException
{
    public function message(): string
    {
        return 'Phone not found';
    }

    public function code(): int
    {
        return 404;
    }
}
