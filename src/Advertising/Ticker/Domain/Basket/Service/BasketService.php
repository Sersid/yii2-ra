<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\Basket\Service;

use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\Basket;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\BasketRepository;

class BasketService
{
    private BasketRepository $repository;

    public function __construct(BasketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getOrCreate(BuyerId $buyerId): Basket
    {
        try {
            return $this->repository->getByBuyer($buyerId);
        } catch (BasketNotFoundException $exception) {
            $basket = Basket::create($buyerId);
            $this->repository->add($basket);

            return $basket;
        }
    }
}
