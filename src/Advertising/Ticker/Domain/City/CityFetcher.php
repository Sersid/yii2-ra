<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Domain\City;

use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;

interface CityFetcher
{
    public function all(): RegionViewCollection;
}
