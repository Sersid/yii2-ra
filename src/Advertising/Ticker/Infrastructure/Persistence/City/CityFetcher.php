<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Infrastructure\Persistence\City;

use common\models\Ticker;
use NikoM\Advertising\Kernel\Infrastructure\Persistence\AbstractCityFetcher;

class CityFetcher extends AbstractCityFetcher implements \NikoM\Advertising\Ticker\Domain\City\CityFetcher
{
    protected function getCitiesId(): array
    {
        $result = [];
        $cities = Ticker::find()
            ->select('city_id')
            ->groupBy('city_id')
            ->where(['status' => Ticker::STATUS_ACTIVE])
            ->asArray()
            ->all();
        foreach ($cities as $city) {
            $result[] = (int)$city['city_id'];
        }
        return $result;
    }
}
