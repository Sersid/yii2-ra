<?php
declare(strict_types=1);

namespace NikoM\Advertising\Ticker\Infrastructure\Persistence\Basket;

use common\models\UserTicker;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Ticker\Domain\Basket\Entity\Basket;

class BasketRepository implements \NikoM\Advertising\Ticker\Domain\Basket\Entity\BasketRepository
{
    private static Basket $basket;

    public function getByBuyer(BuyerId $buyerId): Basket
    {
        $data = UserTicker::find()
            ->select(['id', 'type', 'order_id', 'message', 'guest_id'])
            ->where(['guest_id' => $buyerId->getValue(), 'user_ticker.status' => UserTicker::STATUS_NEW])
            ->with('phones', 'settings', 'settings.system', 'settings.system.tv', 'settings.system.city')
            ->asArray()
            ->one();

        if (empty($data)) {
            throw new BasketNotFoundException();
        }

        $basket = Basket::fromArray($data);
        self::$basket = clone $basket;

        return $basket;
    }

    public function add(Basket $basket): void
    {
        // TODO: Implement add() method.
    }

    public function save(Basket $basket): void
    {
        // TODO: Implement save() method.
    }
}
