<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Infrastructure;

use NikoM\Advertising\Outdoor\Domain\Basket\Exception\ItemNotFoundException;
use NikoM\Advertising\Outdoor\Domain\Construction\Entity\FullNumber;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\ISideRepository;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Letter;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\SideId;
use yii\db\ActiveQuery;

class SideRepository extends SideQuery implements ISideRepository
{
    private SideHydrator $sideHydrator;

    public function __construct(SideHydrator $sideHydrator)
    {
        $this->sideHydrator = $sideHydrator;
    }

    public function getById(SideId $id): Side
    {
        // TODO: Implement getById() method.
    }

    public function getByFullNumberAndLetter(FullNumber $fullNumber, Letter $letter): Side
    {
        $query = $this->query();
        $query->select([
            'outdoor_ad_side.id',
            'outdoor_ad_side.parent_id',
            'outdoor_ad_side.side',
            'outdoor_ad_side.price',
            'outdoor_ad_side.price_installation',
            'outdoor_ad_side.price_print',
            'outdoor_ad_side.price_design',
            'outdoor_ad_side.rating',
            'outdoor_ad_side.address',
        ]);
        $query->joinWith('parent', false);
        $query->where([
            'outdoor_ad.full_number' => $fullNumber->getValue(),
            'outdoor_ad_side.side' => $letter->getValue(),
        ]);
        $query->with([
            'parent' => function ($query) {
                $query->select([
                    'outdoor_ad.id',
                    'outdoor_ad.city_id',
                    'outdoor_ad.street_id',
                    'outdoor_ad.format',
                    'outdoor_ad.size',
                    'outdoor_ad.type_display',
                    'outdoor_ad.number',
                ]);
            },
            'parent.city' => function (ActiveQuery $query) {
                $query->select(['city.id', 'city.name']);
            },
            'parent.street' => function (ActiveQuery $query) {
                $query->select(['street.id', 'street.name']);
            },
            // 'images' => function (ActiveQuery $query) {
            //     // $query->select(['street.id', 'street.name']);
            // },
        ]);
        $data = $query->one();

        if (empty($data)) {
            throw new ItemNotFoundException();
        }

        return $this->sideHydrator->hydrate($data);
    }
}
