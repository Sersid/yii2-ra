<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Infrastructure;

use NikoM\Advertising\Outdoor\Domain\Side\Entity\BusyStatus;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\ISideFetcher;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\SideCollection;
use NikoM\Advertising\Outdoor\Domain\Side\Query\Filter;

class SideFetcher extends SideQuery implements ISideFetcher
{
    private SideConverter $sideConverter;

    public function __construct(SideConverter $sideConverter)
    {
        $this->sideConverter = $sideConverter;
    }

    public function paginate(Filter $filter, int $page, int $perPage): SideCollection
    {
        $query = $this->query();
        $query->select([
            'outdoor_ad_side.id',
            'outdoor_ad_side.parent_id',
            'outdoor_ad_side.side',
            'outdoor_ad_side.address',
            'outdoor_ad_side.price',
            'outdoor_ad_side.rating',
        ]);
        $query->with([
            'parent' => function ($query) {
                $query->select([
                    'outdoor_ad.id',
                    'outdoor_ad.city_id',
                    'outdoor_ad.street_id',
                    'outdoor_ad.full_number',
                    'outdoor_ad.number',
                    'outdoor_ad.format',
                    'outdoor_ad.size',
                    'outdoor_ad.type_display',
                ]);
            },
            'parent.city' => function ($query) {
                $query->select(['city.id', 'city.name']);
            },
            'parent.street' => function ($query) {
                $query->select(['street.id', 'street.name']);
            },
        ]);
        $constructionFilter = [];
        if ($filter->hasCityId()) {
            $constructionFilter['outdoor_ad.city_id'] = $filter->getCityId();
        }
        if ($filter->hasStreetId()) {
            $constructionFilter['outdoor_ad.street_id'] = $filter->getStreetId();
        }
        if ($filter->hasFormats()) {
            $constructionFilter['outdoor_ad.format'] = $filter->getFormats();
        }
        if (!empty($constructionFilter)) {
            $query->joinWith('parent', false);
            $query->andWhere($constructionFilter);
        }
        if ($filter->hasPriceFrom()) {
            $query->andWhere(['>=', 'outdoor_ad_side.price', $filter->getPriceFrom()]);
        }
        if ($filter->hasPriceTo()) {
            $query->andWhere(['<=', 'outdoor_ad_side.price', $filter->getPriceTo()]);
        }
        if ($filter->hasRatings()) {
            $query->andWhere(['outdoor_ad_side.rating' => $filter->getRatings()]);
        }
        if ($filter->hasSides()) {
            $query->andWhere(['outdoor_ad_side.side' => $filter->getSides()]);
        }
        if ($filter->hasPeriods()) {
            $conditionPeriods = ['OR'];
            foreach ($filter->getPeriods() as $period) {
                $conditionPeriods[] = [
                    'AND',
                    ['=', 'outdoor_ad_side_busy.month', $period['month']],
                    ['=', 'outdoor_ad_side_busy.year', $period['year']],
                ];
            }
            $query->joinWith('busy', false);
            $query->where([
                'AND',
                ['=', 'outdoor_ad_side_busy.status', BusyStatus::free()->getValue()],
                $conditionPeriods,
            ]);
        }

        $query->offset($perPage * ($page < 1 ? 1 : $page) - $perPage);
        $query->limit($perPage);

        $sides = [];
        foreach ($query->all() as $side) {
            $sideDTO = $this->sideConverter->fromArrayToDTO($side);
            $sides[] = Side::fromDTO($sideDTO);
        }

        return new SideCollection($sides);
    }
}
