<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Infrastructure;

use NikoM\Advertising\Outdoor\Domain\Construction\DTO\AddressDTO;
use NikoM\Location\City\Infrastructure\CityConverter;
use NikoM\Location\Street\Infrastructure\StreetConverter;

class AddressConverter
{
    private CityConverter $cityConverter;
    private StreetConverter $streetConverter;

    public function __construct(CityConverter $cityConverter, StreetConverter $streetConverter)
    {
        $this->cityConverter = $cityConverter;
        $this->streetConverter = $streetConverter;
    }

    public function fromArrayToDTO(array $data): AddressDTO
    {
        $dto = new AddressDTO();
        if (isset($data['lat'])) {
            $dto->lat = empty($data['lat']) ? null : (float)$data['lat'];
        }
        if (isset($data['lon'])) {
            $dto->lon = empty($data['lon']) ? null : (float)$data['lon'];
        }
        if (isset($data['rotate'])) {
            $dto->rotate = empty($data['rotate']) ? null : (int)$data['rotate'];
        }
        if (isset($data['city'])) {
            $dto->city = $this->cityConverter->fromArrayToDTO($data['city']);
        }
        if (isset($data['street'])) {
            $dto->street = $this->streetConverter->fromArrayToDTO($data['street']);
        }

        return $dto;
    }
}
