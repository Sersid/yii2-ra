<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Infrastructure;

use common\models\OutdoorAdSide;
use common\models\OutdoorAdSideQuery;

abstract class SideQuery
{
    public function query(): OutdoorAdSideQuery
    {
        $query = OutdoorAdSide::find();
        $query->asArray();

        return $query;
    }
}
