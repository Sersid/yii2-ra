<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Infrastructure;

use NikoM\Advertising\Outdoor\Domain\Side\DTO\PricesDTO;
use NikoM\Advertising\Outdoor\Domain\Side\DTO\SideDTO;

class SideConverter
{
    private ConstructionConverter $constructionConverter;

    public function __construct(ConstructionConverter $constructionConverter)
    {
        $this->constructionConverter = $constructionConverter;
    }

    public function fromArrayToDTO(array $data): SideDTO
    {
        $dto = new SideDTO();
        if (isset($data['id'])) {
            $dto->id = (int)$data['id'];
        }
        if (isset($data['address'])) {
            $dto->exactAddress = $data['address'];
        }
        if (isset($data['side'])) {
            $dto->letter = (int)$data['side'];
        }
        if (isset($data['rating'])) {
            $dto->rating = (int)$data['rating'];
        }
        if (isset($data['price'])
            || isset($data['price_installation'])
            || isset($data['price_print'])
            || isset($data['price_design'])) {
            $dto->prices = new PricesDTO();
            if (isset($data['price'])) {
                $dto->prices->rent = (float)$data['price'];
            }
            if (isset($data['price_installation'])) {
                $dto->prices->installation = (float)$data['price_installation'];
            }
            if (isset($data['price_print'])) {
                $dto->prices->print = (float)$data['price_print'];
            }
            if (isset($data['price_design'])) {
                $dto->prices->design = (float)$data['price_design'];
            }
        }
        if (isset($data['status'])) {
            $dto->status = (int)$data['status'];
        }
        if (isset($data['parent'])) {
            $dto->construction = $this->constructionConverter->fromArrayToDTO($data['parent']);
        }

        return $dto;
    }
}
