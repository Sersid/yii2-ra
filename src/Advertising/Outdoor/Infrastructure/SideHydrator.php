<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Infrastructure;

use NikoM\Advertising\Outdoor\Domain\Construction\Entity\Address;
use NikoM\Advertising\Outdoor\Domain\Construction\Entity\Construction;
use NikoM\Advertising\Outdoor\Domain\Construction\Entity\Format;
use NikoM\Advertising\Outdoor\Domain\Construction\Entity\FullNumber;
use NikoM\Advertising\Outdoor\Domain\Construction\Entity\ShortNumber;
use NikoM\Advertising\Outdoor\Domain\Construction\Entity\TypeDisplay;
use NikoM\Advertising\Outdoor\Domain\Photo\Entity\PhotoCollection;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Letter;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Prices;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Rating;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\SideId;
use NikoM\Kernel\Domain\Entity\Hydrator;
use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\City\Domain\Entity\CityId;
use NikoM\Location\City\Domain\Entity\CityName;
use NikoM\Location\Street\Domain\Entity\Street;
use NikoM\Location\Street\Domain\Entity\StreetId;
use NikoM\Location\Street\Domain\Entity\StreetName;

class SideHydrator
{
    private Hydrator $hydrator;

    public function __construct(Hydrator $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    public function hydrate(array $data): Side
    {
        $props = [];
        if (isset($data['id'])) {
            $props['id'] = new SideId((int)$data['id']);
        }
        if (isset($data['address'])) {
            $props['exactAddress'] = $data['address'];
        }
        if (isset($data['side'])) {
            $props['letter'] = new Letter((int)$data['side']);
        }
        if (isset($data['rating'])) {
            $props['rating'] = new Rating((int)$data['rating']);
        }
        if (isset($data['parent'])) {
            $props['construction'] = $this->getConstruction($data['parent']);
        }
        if (isset($data['price'])
            || isset($data['price_installation'])
            || isset($data['price_print'])
            || isset($data['price_design'])) {
            $props['prices'] = $this->getPrices($data);
        }

        $props['photoCollection'] = new PhotoCollection([]);

        /** @var Side $result */
        $result = $this->hydrator->hydrate(Side::class, $props);

        return $result;
    }

    private function getConstruction(array $data): Construction
    {
        $props = [];
        if (isset($data['full_number'])) {
            $props['fullNumber'] = new FullNumber($data['full_number']);
        }
        if (isset($data['number'])) {
            $props['shortNumber'] = new ShortNumber($data['number']);
        }
        if (isset($data['format'])) {
            $props['format'] = new Format((int)$data['format']);
        }
        if (isset($data['size'])) {
            $props['size'] = $data['size'];
        }
        if (isset($data['type_display'])) {
            $props['typeDisplay'] = new TypeDisplay((int)$data['type_display']);
        }
        if (isset($data['city']) || isset($data['street'])) {
            $address = [];
            if (isset($data['city'])) {
                $address['city'] = $this->getCity($data['city']);
            }
            if (isset($data['street'])) {
                $address['street'] = $this->getStreet($data['street']);
            }
            $props['address'] = $this->getAddress($address);
        }

        /** @var Construction $result */
        $result = $this->hydrator->hydrate(Construction::class, $props);

        return $result;
    }

    private function getCity(array $data): City
    {
        $props = [];
        if (isset($data['id'])) {
            $props['id'] = new CityId((int)$data['id']);
        }
        if (isset($data['name'])) {
            $props['name'] = new CityName($data['name']);
        }

        /** @var City $city */
        $city = $this->hydrator->hydrate(City::class, $props);

        return $city;
    }

    private function getStreet(array $data): Street
    {
        $props = [];
        if (isset($data['id'])) {
            $props['id'] = new StreetId((int)$data['id']);
        }
        if (isset($data['name'])) {
            $props['name'] = new StreetName($data['name']);
        }

        /** @var Street $city */
        $city = $this->hydrator->hydrate(Street::class, $props);

        return $city;
    }

    private function getAddress(array $data): Address
    {
        /** @var Address $address */
        $address = $this->hydrator->hydrate(Address::class, $data);

        return $address;
    }

    private function getPrices(array $data): Prices
    {
        $props = [];
        if (isset($data['price'])) {
            $props['rent'] = (float)$data['price'];
        }
        if (isset($data['price_installation'])) {
            $props['installation'] = (float)$data['price_installation'];
        }
        if (isset($data['price_print'])) {
            $props['print'] = (float)$data['price_print'];
        }
        if (isset($data['price_design'])) {
            $props['design'] = (float)$data['price_design'];
        }

        /** @var Prices $result */
        $result = $this->hydrator->hydrate(Prices::class, $props);

        return $result;
    }
}
