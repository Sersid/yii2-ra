<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Infrastructure;

use NikoM\Advertising\Outdoor\Domain\Construction\DTO\ConstructionDTO;

class ConstructionConverter
{
    private AddressConverter $addressConverter;

    public function __construct(AddressConverter $addressConverter)
    {
        $this->addressConverter = $addressConverter;
    }

    public function fromArrayToDTO(array $data): ConstructionDTO
    {
        $dto = new ConstructionDTO();
        if (isset($data['id'])) {
            $dto->id = (int)$data['id'];
        }
        if (isset($data['full_number'])) {
            $dto->fullNumber = $data['full_number'];
        }
        if (isset($data['number'])) {
            $dto->shortNumber = $data['number'];
        }
        if (isset($data['format'])) {
            $dto->format = (int)$data['format'];
        }
        if (isset($data['size'])) {
            $dto->size = $data['size'];
        }
        if (isset($data['type_display'])) {
            $dto->typeDisplay = (int)$data['type_display'];
        }
        if (isset($data['lat'])
            || isset($data['lon'])
            || isset($data['rotate'])
            || isset($data['city'])
            || isset($data['street'])) {
            $dto->address = $this->addressConverter->fromArrayToDTO($data);
        }

        return $dto;
    }
}
