<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Photo\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdNullableValueObject;

class PhotoId extends IdNullableValueObject
{
}
