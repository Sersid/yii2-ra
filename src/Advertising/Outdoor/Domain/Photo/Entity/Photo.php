<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Photo\Entity;

use NikoM\Advertising\Kernel\Domain\Image\Image;

class Photo
{
    private PhotoId $id;
    private Image $small;
    private Image $large;

    public function getId(): PhotoId
    {
        return $this->id;
    }

    public function getSmall(): Image
    {
        return $this->small;
    }

    public function getLarge(): Image
    {
        return $this->large;
    }
}
