<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Photo\Entity;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

class PhotoCollection extends Collection
{
    protected static function type(): string
    {
        return Photo::class;
    }
}
