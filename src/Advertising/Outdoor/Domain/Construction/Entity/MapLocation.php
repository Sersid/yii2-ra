<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

class MapLocation
{
    private ?float $lat;
    private ?float $lon;
    private ?int $rotate;

    public function __construct(?float $lat, ?float $lon, ?int $rotate)
    {
        $this->lat = $lat;
        $this->lon = $lon;
        $this->rotate = $rotate;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function getLon(): ?float
    {
        return $this->lon;
    }

    public function getRotate(): ?int
    {
        return $this->rotate;
    }
}
