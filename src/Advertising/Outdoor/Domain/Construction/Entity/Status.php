<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class Status extends IntValueObject
{
    private const ENABLED = 1;
    private const DISABLED = 2;

    public function __construct(int $value)
    {
        Assert::inArray($value, [self::ENABLED, self::DISABLED]);
        parent::__construct($value);
    }

    public function isEnabled(): bool
    {
        return $this->value === self::ENABLED;
    }

    public function isDisabled(): bool
    {
        return $this->value === self::DISABLED;
    }
}
