<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class Format extends IntValueObject
{
    private const BILLBOARDS = 10;
    private const LED_DISPLAYS = 20;
    private const FIREWALLS = 30;
    private const ADV_FENCES = 40;
    private const CITY_FORMAT = 50;
    private const PYLON = 60;
    private const PROJECTION_AD = 70;
    private const CITY_BOARD = 80;
    private const STOPPING_PAVILION = 90;
    private const PILLAR = 100;
    private const SUPER_SITE = 110;

    public function __construct(int $value)
    {
        Assert::inArray(
            $value,
            [
                self::BILLBOARDS,
                self::LED_DISPLAYS,
                self::FIREWALLS,
                self::ADV_FENCES,
                self::CITY_FORMAT,
                self::PYLON,
                self::PROJECTION_AD,
                self::CITY_BOARD,
                self::STOPPING_PAVILION,
                self::PILLAR,
                self::SUPER_SITE,
            ]
        );
        parent::__construct($value);
    }

    public static function getAll(): array
    {
        return [
            self::BILLBOARDS => 'Рекламный щит',
            self::LED_DISPLAYS => 'Светодиодный экран',
            self::FIREWALLS => 'Брандмауэр',
            self::ADV_FENCES => 'Рекламное ограждение',
            self::CITY_FORMAT => 'Сити-формат',
            self::PYLON => 'Пилон',
            self::PROJECTION_AD => 'Проекционная реклама',
            self::CITY_BOARD => 'Ситиборд',
            self::STOPPING_PAVILION => 'Остановочный павильон',
            self::PILLAR => 'Пиллар',
            self::SUPER_SITE => 'Суперсайт',
        ];
    }

    public function isBillboards(): bool
    {
        return $this->value === self::BILLBOARDS;
    }

    public function isLedDisplays(): bool
    {
        return $this->value === self::LED_DISPLAYS;
    }

    public function isFirewalls(): bool
    {
        return $this->value === self::FIREWALLS;
    }

    public function isAdvFences(): bool
    {
        return $this->value === self::ADV_FENCES;
    }

    public function isCityFormat(): bool
    {
        return $this->value === self::CITY_FORMAT;
    }

    public function isPylon(): bool
    {
        return $this->value === self::PYLON;
    }

    public function isProjectionAd(): bool
    {
        return $this->value === self::PROJECTION_AD;
    }

    public function isCityBoard(): bool
    {
        return $this->value === self::CITY_BOARD;
    }

    public function isStoppingPavilion(): bool
    {
        return $this->value === self::STOPPING_PAVILION;
    }

    public function isPillar(): bool
    {
        return $this->value === self::PILLAR;
    }

    public function isSuperSite(): bool
    {
        return $this->value === self::SUPER_SITE;
    }

    public function getFormattedValue(): string
    {
        return self::getAll()[$this->value];
    }

    public function __toString(): string
    {
        return $this->getFormattedValue();
    }
}
