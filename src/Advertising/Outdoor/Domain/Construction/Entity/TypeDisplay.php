<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntNullableValueObject;
use Webmozart\Assert\Assert;

class TypeDisplay extends IntNullableValueObject
{
    private const STATICS = 10;
    private const PRISM = 20;
    private const SCROLLER = 30;

    public function __construct(?int $value)
    {
        if (!is_null($value)) {
            Assert::inArray($value, [self::STATICS, self::PRISM, self::SCROLLER]);
        }
        parent::__construct($value);
    }

    public function isStatics(): bool
    {
        return $this->value === self::STATICS;
    }

    public function isPrism(): bool
    {
        return $this->value === self::PRISM;
    }

    public function isScroller(): bool
    {
        return $this->value === self::SCROLLER;
    }

    public static function getAll(): array
    {
        return  [
            self::STATICS => 'Статика',
            self::PRISM => 'Призма',
            self::SCROLLER => 'Скроллер',
        ];
    }

    public function getFormattedValue(): string
    {
        return self::getAll()[$this->value];
    }

    public function notEmpty(): bool
    {
        return !is_null($this->value);
    }

    public function __toString(): string
    {
        return $this->getFormattedValue();
    }
}
