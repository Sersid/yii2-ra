<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;
use Webmozart\Assert\Assert;

class FullNumber extends StringValueObject
{
    public function __construct(string $value)
    {
        $value = trim($value);
        Assert::notEmpty($value);
        parent::__construct($value);
    }
}
