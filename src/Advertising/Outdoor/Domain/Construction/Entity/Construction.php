<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

use NikoM\Advertising\Outdoor\Domain\Construction\DTO\ConstructionDTO;

/**
 * Конструкция рекламного объекта
 */
class Construction
{
    private ConstructionId $id;
    private FullNumber $fullNumber;
    private ShortNumber $shortNumber;
    private Address $address;
    private Format $format;
    private string $size;
    private int $countSides;
    private TypeDisplay $typeDisplay;
    private Status $status;

    private function __construct()
    {
    }

    public static function fromDTO(ConstructionDTO $dto): self
    {
        $self = new self();
        if (isset($dto->fullNumber)) {
            $self->fullNumber = new FullNumber($dto->fullNumber);
        }
        if (isset($dto->shortNumber)) {
            $self->shortNumber = new ShortNumber($dto->shortNumber);
        }
        if (isset($dto->format)) {
            $self->format = new Format($dto->format);
        }
        if (isset($dto->size)) {
            $self->size = $dto->size;
        }
        if (isset($dto->typeDisplay)) {
            $self->typeDisplay = new TypeDisplay($dto->typeDisplay);
        }
        if (isset($dto->address)) {
            $self->address = Address::fromDTO($dto->address);
        }

        return $self;
    }

    public function getId(): ConstructionId
    {
        return $this->id;
    }

    public function getFullNumber(): FullNumber
    {
        return $this->fullNumber;
    }

    public function getShortNumber(): ShortNumber
    {
        return $this->shortNumber;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getFormat(): Format
    {
        return $this->format;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function getCountSides(): int
    {
        return $this->countSides;
    }

    public function getTypeDisplay(): TypeDisplay
    {
        return $this->typeDisplay;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getCityName(): string
    {
        return (string)$this->address->getCity()->getName();
    }

    public function getStreetName(): string
    {
        return (string)$this->address->getStreet()->getName();
    }

    public function getFormatAndSize(): string
    {
        return $this->format . ' ' . $this->size;
    }

    public function getSurface(): string
    {
        $result = $this->getFormatAndSize();
        if ($this->typeDisplay->notEmpty()) {
            $result .= ' (' . $this->typeDisplay . ')';
        }

        return $result;
    }
}
