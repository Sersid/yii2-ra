<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;

class ShortNumber extends StringValueObject
{
}
