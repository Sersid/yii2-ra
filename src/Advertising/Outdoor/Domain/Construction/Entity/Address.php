<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\Entity;

use NikoM\Advertising\Outdoor\Domain\Construction\DTO\AddressDTO;
use NikoM\Location\City\Domain\Entity\City;
use NikoM\Location\Street\Domain\Entity\Street;

class Address
{
    private City $city;
    private Street $street;
    private MapLocation $mapLocation;

    private function __construct()
    {
    }

    public static function fromDTO(AddressDTO $dto): self
    {
        $self = new self();
        if (isset($dto->city)) {
            $self->city = City::fromDTO($dto->city);
        }
        if (isset($dto->street)) {
            $self->street = Street::fromDTO($dto->street);
        }
        if (isset($dto->lat) || isset($dto->lon) || isset($dto->rotate)) {
            $self->mapLocation = new MapLocation($dto->lat, $dto->lon, $dto->rotate);
        }

        return $self;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function getStreet(): Street
    {
        return $this->street;
    }

    public function getMapLocation(): MapLocation
    {
        return $this->mapLocation;
    }
}
