<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\DTO;

use NikoM\Location\City\Domain\DTO\CityDto;
use NikoM\Location\Street\Domain\DTO\StreetDTO;

class AddressDTO
{
    public CityDto $city;
    public StreetDTO $street;
    public ?float $lat;
    public ?float $lon;
    public ?int $rotate;
}
