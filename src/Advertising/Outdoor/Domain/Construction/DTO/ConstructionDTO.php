<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Construction\DTO;

class ConstructionDTO
{
    public int $id;
    public string $fullNumber;
    public string $shortNumber;
    public int $format;
    public string $size;
    public int $countSides;
    public ?int $typeDisplay;
    public int $status;
    public AddressDTO $address;
}
