<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\DTO;

use NikoM\Advertising\Outdoor\Domain\Construction\DTO\ConstructionDTO;

class SideDTO
{
    public int $id;
    public ConstructionDTO $construction;
    public ?string $exactAddress;
    public int $letter;
    public PricesDTO $prices;
    public int $rating;
    public ?string $description;
    public int $status;
}
