<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\DTO;

class PricesDTO
{
    public float $rent;
    public float $installation;
    public float $print;
    public float $design;
}
