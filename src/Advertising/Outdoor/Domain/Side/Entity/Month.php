<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use Webmozart\Assert\Assert;

class Month
{
    private int $month;
    private int $year;
    private BusyStatus $busyStatus;
    private ?float $discount;

    public function __construct(int $month, int $year, BusyStatus $busyStatus, ?float $discount)
    {
        Assert::range($month, 1, 12);
        Assert::length($year, 4);
        $this->month = $month;
        $this->year = $year;
        $this->busyStatus = $busyStatus;
        $this->discount = $discount;
    }

    public static function createByMonthAndYear(int $month, int $year): self
    {
        return new self($month, $year, BusyStatus::undefined(), null);
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getBusyStatus(): BusyStatus
    {
        return $this->busyStatus;
    }

    public function setBusyStatus(BusyStatus $busyStatus): void
    {
        $this->busyStatus = $busyStatus;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): void
    {
        $this->discount = $discount;
    }
}
