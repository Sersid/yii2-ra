<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Advertising\Outdoor\Domain\Construction\Entity\FullNumber;
use NikoM\Advertising\Outdoor\Domain\Side\Exception\SideNotFoundException;

interface ISideRepository
{
    /**
     * @param SideId $id
     *
     * @return Side
     * @throws SideNotFoundException
     */
    public function getById(SideId $id): Side;

    /**
     * @param FullNumber $fullNumber
     * @param Letter     $letter
     *
     * @return Side
     * @throws SideNotFoundException
     */
    public function getByFullNumberAndLetter(FullNumber $fullNumber, Letter $letter): Side;
}
