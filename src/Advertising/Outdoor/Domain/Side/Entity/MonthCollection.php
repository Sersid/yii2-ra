<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use DateInterval;
use DateTime;
use NikoM\Advertising\Outdoor\Domain\Side\Exception\MonthNotFoundException;
use NikoM\Kernel\Domain\Entity\Collection\Collection;
use Webmozart\Assert\Assert;

class MonthCollection extends Collection
{
    /**
     * @param Month[] $items
     */
    public function __construct(array $items)
    {
        parent::__construct($items);
        foreach ($items as $key => $month) {
            Assert::eq((int)$key, self::getKeyByMonthAndYear($month->getMonth(), $month->getYear()));
        }
    }

    protected static function type(): string
    {
        return Month::class;
    }

    public static function generatePrototype(int $months = 9): self
    {
        $monthList = [];
        $date = new DateTime();
        for ($i = 1; $i <= $months; $i++) {
            $date->add(new DateInterval('P1M'));
            $month = (int)$date->format('n');
            $year = (int)$date->format('Y');
            // TODO генерировать ключ в конструкторе
            $key = self::getKeyByMonthAndYear($month, $year);
            $monthList[$key] = Month::createByMonthAndYear($month, $year);
        }

        return new self($monthList);
    }

    public function getByMonthAndYear(int $month, int $year): Month
    {
        $key = self::getKeyByMonthAndYear($month, $year);
        if (!isset($this->items[$key])) {
            throw new MonthNotFoundException();
        }

        return $this->items[$key];
    }
    
    private static function getKeyByMonthAndYear(int $month, int $year): int
    {
        if ($month < 10) {
            $month = '0' . $month;
        }
        return intval($year . $month);
    }
}
