<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class BusyStatus extends IntValueObject
{
    // Неизвестен
    private const UNDEFINED = 0;
    // Занят
    private const BUSY = 10;
    // Свободен
    private const FREE = 20;
    // Зарезервирован
    private const RESERVE = 30;
    // По запросу
    private const ON_REQUEST = 40;

    public function __construct(int $value)
    {
        Assert::inArray($value, [self::UNDEFINED, self::BUSY, self::FREE, self::RESERVE, self::ON_REQUEST]);
        parent::__construct($value);
    }

    public static function undefined(): self
    {
        return new self(self::UNDEFINED);
    }

    public static function free(): self
    {
        return new self(self::FREE);
    }

    public function isUndefined(): bool
    {
        return $this->value === self::UNDEFINED;
    }

    public function isFree(): bool
    {
        return $this->value === self::FREE;
    }

    public function isBusy(): bool
    {
        return $this->value === self::BUSY;
    }

    public function isReserve(): bool
    {
        return $this->value === self::RESERVE;
    }

    public function isOnRequest(): bool
    {
        return $this->value === self::ON_REQUEST;
    }
}
