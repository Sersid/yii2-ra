<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Advertising\Outdoor\Domain\Side\DTO\PricesDTO;
use NikoM\Kernel\Domain\FormatPriceTrait;

class Prices
{
    use FormatPriceTrait;

    private float $rent;
    private float $installation;
    private float $print;
    private float $design;

    private function __construct()
    {
    }

    public static function create(float $rent, float $installation, float $print, float $design): self
    {
        $self = new self();
        $self->rent = $rent;
        $self->installation = $installation;
        $self->print = $print;
        $self->design = $design;

        return $self;
    }

    public static function fromDTO(PricesDTO $dto): self
    {
        $self = new self();
        if (isset($dto->rent)) {
            $self->rent = $dto->rent;
        }
        if (isset($dto->installation)) {
            $self->installation = $dto->installation;
        }
        if (isset($dto->print)) {
            $self->print = $dto->print;
        }
        if (isset($dto->design)) {
            $self->design = $dto->design;
        }

        return $self;
    }

    public function getRent(): float
    {
        return $this->rent;
    }

    public function getInstallation(): float
    {
        return $this->installation;
    }

    public function getPrint(): float
    {
        return $this->print;
    }

    public function getDesign(): float
    {
        return $this->design;
    }

    public function getFormattedRent(): string
    {
        return $this->formatPrice($this->rent);
    }

    public function getFormattedInstallation(): string
    {
        return $this->formatPrice($this->installation);
    }

    public function getFormattedPrint(): string
    {
        return $this->formatPrice($this->print);
    }

    public function getFormattedDesign(): string
    {
        return $this->formatPrice($this->design);
    }
}
