<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Advertising\Outdoor\Domain\Side\Query\Filter;

interface ISideFetcher
{
    public function paginate(Filter $filter, int $page, int $perPage): SideCollection;
}
