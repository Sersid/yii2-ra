<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class Rating extends IntValueObject
{
    public function __construct(int $value)
    {
        Assert::range($value, 0, 3);
        parent::__construct($value);
    }
}
