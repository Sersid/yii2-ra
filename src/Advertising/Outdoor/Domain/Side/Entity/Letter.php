<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class Letter extends IntValueObject
{
    private const A = 1;
    private const B = 2;
    private const C = 3;

    public function __construct(int $value)
    {
        Assert::inArray($value, [self::A, self::B, self::C]);
        parent::__construct($value);
    }

    public static function fromLatin(string $letter): self
    {
        $letter = array_search($letter, self::getAllLatin());
        Assert::notFalse($letter, 'Letter incorrect');

        return new self($letter);
    }

    public static function fromCyrillic(string $letter): self
    {
        $letter = array_search($letter, self::getAllCyrillic());
        Assert::notFalse($letter, 'Letter incorrect');

        return new self($letter);
    }

    public function getCyrillic(): string
    {
        return self::getAllCyrillic()[$this->value];
    }

    public static function getAllCyrillic(): array
    {
        return [
            self::A => 'А',
            self::B => 'Б',
            self::C => 'В',
        ];
    }

    public function getLatin(): string
    {
        return self::getAllLatin()[$this->value];
    }

    public static function getAllLatin(): array
    {
        return [
            self::A => 'A',
            self::B => 'B',
            self::C => 'C',
        ];
    }
}
