<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;

class Status extends IntValueObject
{
}
