<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

class SideCollection extends Collection
{
    protected static function type(): string
    {
        return Side::class;
    }
}
