<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdNullableValueObject;

class SideId extends IdNullableValueObject
{
}
