<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Entity;

use NikoM\Advertising\Outdoor\Domain\Construction\Entity\Construction;
use NikoM\Advertising\Outdoor\Domain\Photo\Entity\Photo;
use NikoM\Advertising\Outdoor\Domain\Photo\Entity\PhotoCollection;
use NikoM\Advertising\Outdoor\Domain\Side\DTO\SideDTO;

/**
 * Арендная сторона рекламной конструкции
 */
class Side
{
    private SideId $id;
    private Construction $construction;
    private ?string $exactAddress;
    private Letter $letter;
    private PhotoCollection $photoCollection;
    private Prices $prices;
    private Rating $rating;
    private ?string $description;
    private Status $status;
    private MonthCollection $monthCollection;

    private function __construct()
    {
    }

    public static function fromDTO(SideDTO $dto): self
    {
        $self = new self();
        if (isset($dto->id)) {
            $self->id = new SideId($dto->id);
        }
        if (isset($dto->construction)) {
            $self->construction = Construction::fromDTO($dto->construction);
        }
        if (isset($dto->exactAddress)) {
            $self->exactAddress = $dto->exactAddress;
        }
        if (isset($dto->letter)) {
            $self->letter = new Letter($dto->letter);
        }
        if (isset($dto->prices)) {
            $self->prices = Prices::fromDTO($dto->prices);
        }
        if (isset($dto->rating)) {
            $self->rating = new Rating($dto->rating);
        }
        if (isset($dto->description)) {
            $self->description = $dto->description;
        }
        if (isset($dto->status)) {
            $self->status = new Status($dto->status);
        }

        return $self;
    }

    public function getId(): SideId
    {
        return $this->id;
    }

    public function getConstruction(): Construction
    {
        return $this->construction;
    }

    public function getLetter(): Letter
    {
        return $this->letter;
    }

    /**
     * @return PhotoCollection|Photo[]
     */
    public function getPhotoCollection(): PhotoCollection
    {
        return $this->photoCollection;
    }

    public function getPrices(): Prices
    {
        return $this->prices;
    }

    public function getRating(): Rating
    {
        return $this->rating;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return MonthCollection|Month[]
     */
    public function getMonthCollection(): MonthCollection
    {
        return $this->monthCollection;
    }

    public function isEqual(self $other): bool
    {
        return $this->id->getValue() === $other->getId()->getValue();
    }

    public function getNumber(): string
    {
        return $this->construction->getShortNumber() . ' ' . $this->letter->getCyrillic();
    }

    public function getUrl(): string
    {
        return '/outdoor-ad/view/' . $this->construction->getFullNumber() . $this->getLatinLetter();
    }

    public function getLatinLetter(): string
    {
        return $this->letter->getLatin();
    }

    public function getName(): string
    {
        $result = $this->construction->getFormatAndSize() . ' (сторона ' . $this->getCyrillicLetter() . ')';
        if ($this->construction->getTypeDisplay()->notEmpty()) {
            $result .= ' - ' . $this->construction->getTypeDisplay();
        }

        return $result;
    }

    public function getCyrillicLetter(): string
    {
        return $this->letter->getCyrillic();
    }

    public function getFullAddress(): string
    {
        return $this->getCityName() . ', ' . $this->getShortAddress();
    }

    public function getCityName(): string
    {
        return $this->construction->getCityName();
    }

    public function getShortAddress(): string
    {
        return $this->construction->getStreetName() . ', ' . $this->exactAddress;
    }

    public function getSurface(): string
    {
        return $this->construction->getSurface();
    }

    public function getPageTitle(): string
    {
        return $this->construction->getFormatAndSize() . ' (сторона ' . $this->getCyrillicLetter() . ') '
               . $this->getFullAddress() . ' - цена ' . $this->prices->getFormattedRent() . ' руб./мес.';
    }

    public function getPageDescription(): string
    {
        return 'Нужен ' . $this->getSurface() . ' по адресу ' . $this->getFullAddress() . '? '
               . 'Заходите и подберите себе подходящее время размещения. Онлайн заказ. Бесплатная консультация по '
               . 'всем интересующим вас вопросам.';
    }
}
