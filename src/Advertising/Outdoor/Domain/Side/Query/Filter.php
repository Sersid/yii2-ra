<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Query;

use Webmozart\Assert\Assert;

class Filter
{
    private int $cityId;
    private int $streetId;
    private array $formats;
    private array $periods;
    private float $priceFrom;
    private float $priceTo;
    private array $ratings;
    private array $sides;
    private bool $isPromoOnly;

    public function hasCityId(): bool
    {
        return isset($this->cityId);
    }

    public function getCityId(): int
    {
        return $this->cityId;
    }

    public function setCityId($cityId): void
    {
        $cityId = (int)$cityId;
        if ($cityId > 0) {
            $this->cityId = $cityId;
        }
    }

    public function hasStreetId(): bool
    {
        return isset($this->streetId);
    }

    public function getStreetId(): int
    {
        return $this->streetId;
    }

    public function setStreetId($streetId): void
    {
        $streetId = (int)$streetId;
        if ($streetId > 0) {
            $this->streetId = $streetId;
        }
    }

    public function hasFormats(): bool
    {
        return isset($this->formats);
    }

    public function getFormats(): array
    {
        return $this->formats;
    }

    public function setFormats($formats): void
    {
        $values = $this->getIntValues($formats);
        if (!empty($values)) {
            $this->formats = $values;
        }
    }

    private function getIntValues($values): array
    {
        $result = [];
        if (is_array($values)) {
            foreach ($values as $value) {
                $value = (int)$value;
                if ($value > 0) {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    public function hasPeriods(): bool
    {
        return isset($this->periods);
    }

    public function getPeriods(): array
    {
        return $this->periods;
    }

    public function setPeriods($periods): void
    {
        $result = [];
        if (is_array($periods)) {
            foreach ($periods as $value) {
                $values = explode('.', trim((string)$value));
                $month = (int)$values[0];
                $year = (int)$values[1];
                Assert::count($values, 2);
                Assert::range($month, 1, 12);
                Assert::natural($year);
                Assert::length($year, 4);
                $result[] = ['month' => $month, 'year' => $year];
            }
        }

        if (!empty($result)) {
            $this->periods = $result;
        }
    }

    public function hasPriceFrom(): bool
    {
        return isset($this->priceFrom);
    }

    public function getPriceFrom(): float
    {
        return $this->priceFrom;
    }

    public function setPriceFrom($priceFrom): void
    {
        $priceFrom = (float)$priceFrom;
        if ($priceFrom > 0) {
            $this->priceFrom = $priceFrom;
        }
    }

    public function hasPriceTo(): bool
    {
        return isset($this->priceTo);
    }

    public function getPriceTo(): float
    {
        return $this->priceTo;
    }

    public function setPriceTo($priceTo): void
    {
        $priceTo = (float)$priceTo;
        if ($priceTo > 0) {
            $this->priceTo = $priceTo;
        }
    }

    public function hasRatings(): bool
    {
        return isset($this->ratings);
    }

    public function getRatings(): array
    {
        return $this->ratings;
    }

    public function setRatings($ratings): void
    {
        $values = $this->getIntValues($ratings);
        if (!empty($values)) {
            $this->ratings = $values;
        }
    }

    public function hasSides(): bool
    {
        return isset($this->sides);
    }

    public function getSides(): array
    {
        return $this->sides;
    }

    public function setSides($sides): void
    {
        $values = $this->getIntValues($sides);
        if (!empty($values)) {
            $this->sides = $values;
        }
    }

    public function hasIsPromoOnly(): bool
    {
        return isset($this->isPromoOnly);
    }

    public function isPromoOnly(): bool
    {
        return $this->isPromoOnly;
    }

    public function setIsPromoOnly($isPromoOnly): void
    {
        $isPromoOnly = trim((string)$isPromoOnly);
        if (strlen($isPromoOnly) > 0) {
            $this->isPromoOnly = (bool)$isPromoOnly;
        }
    }
}
