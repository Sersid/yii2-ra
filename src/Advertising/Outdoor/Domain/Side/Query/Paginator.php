<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Query;

use NikoM\Kernel\Domain\Query\AbstractPaginator;

class Paginator extends AbstractPaginator
{
}
