<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class MonthNotFoundException extends DomainException
{
    public function message(): string
    {
        return 'Month not found';
    }

    public function code(): int
    {
        return 404;
    }
}
