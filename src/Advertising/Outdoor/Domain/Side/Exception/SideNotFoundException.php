<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Side\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class SideNotFoundException extends DomainException
{
    public function message(): string
    {
        return 'Construct side not found';
    }

    public function code(): int
    {
        return 404;
    }
}
