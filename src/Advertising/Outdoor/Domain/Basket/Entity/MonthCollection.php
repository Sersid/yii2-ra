<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Entity;

use NikoM\Advertising\Outdoor\Domain\Basket\Exception\MonthAlreadyExistException;
use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @property Month[] $items
 */
class MonthCollection extends Collection
{
    protected static function type(): string
    {
        return Month::class;
    }

    public function has(Month $month): bool
    {
        foreach ($this->items as $item) {
            if ($item->getMonth() === $month->getMonth() && $item->getYear() === $month->getYear()) {
                return true;
            }
        }

        return false;
    }

    public function add(Month $month): void
    {
        if ($this->has($month)) {
            throw new MonthAlreadyExistException();
        }
        array_push($this->items, $month);
    }
}
