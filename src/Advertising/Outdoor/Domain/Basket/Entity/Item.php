<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Entity;

use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;

class Item
{
    private Side $side;
    private bool $isNeedInstall = true;
    private bool $isNeedPrint;
    private bool $isNeedDesign;
    private MonthCollection $monthCollection;

    public function __construct(Side $side)
    {
        $this->side = $side;
        $this->monthCollection = new MonthCollection([]);
    }

    public function getSide(): Side
    {
        return $this->side;
    }

    public function isEqual(self $other): bool
    {
        return $this->side->isEqual($other->getSide());
    }

    public function addMonth(Month $month): void
    {
        $this->monthCollection->add($month);
    }

    public function isNeedInstall(): bool
    {
        return $this->isNeedInstall;
    }

    public function setIsNeedPrint(bool $isNeedPrint): void
    {
        $this->isNeedPrint = $isNeedPrint;
    }

    public function isNeedPrint(): bool
    {
        return $this->isNeedPrint;
    }

    public function isNeedDesign(): bool
    {
        return $this->isNeedDesign;
    }

    public function setIsNeedDesign(bool $isNeedDesign): void
    {
        $this->isNeedDesign = $isNeedDesign;
    }

    public function getMonthCollection(): MonthCollection
    {
        return $this->monthCollection;
    }
}
