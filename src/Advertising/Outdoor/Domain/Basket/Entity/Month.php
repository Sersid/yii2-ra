<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Entity;

use Webmozart\Assert\Assert;

class Month
{
    private int $month;
    private int $year;

    public function __construct(int $month, int $year)
    {
        Assert::range($month, 1, 12);
        Assert::natural($year);
        Assert::length($year, 4);
        $this->month = $month;
        $this->year = $year;
    }

    public static function fromString(string $str): self
    {
        $str = trim($str);
        Assert::isEmpty($str);
        $parts = explode('.', $str);
        Assert::count($parts, 2);

        return new self((int)$parts[0], (int)$parts[1]);
    }


    public function getMonth(): int
    {
        return $this->month;
    }

    public function getYear(): int
    {
        return $this->year;
    }
}
