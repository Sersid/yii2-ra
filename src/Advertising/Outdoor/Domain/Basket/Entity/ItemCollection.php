<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Entity;

use NikoM\Advertising\Outdoor\Domain\Basket\Exception\ItemNotFoundException;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;
use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @property Item[] $items
 */
class ItemCollection extends Collection
{
    protected static function type(): string
    {
        return Item::class;
    }

    public function add(Item $item): void
    {
        array_push($this->items, $item);
    }

    public function get(Side $side): Item
    {
        foreach ($this->items as $item) {
            if ($item->getSide()->isEqual($side)) {
                return $item;
            }
        }
        throw new ItemNotFoundException();
    }
}
