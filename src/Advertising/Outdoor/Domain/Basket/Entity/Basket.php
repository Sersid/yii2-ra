<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Entity;

use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;

class Basket
{
    private BuyerId $buyerId;
    private ItemCollection $itemCollection;

    public function __construct(BuyerId $buyerId)
    {
        $this->buyerId = $buyerId;
        $this->itemCollection = new ItemCollection([]);
    }

    public function getItem(Side $side): Item
    {
        return $this->itemCollection->get($side);
    }

    public function addItem(Item $item): void
    {
        $this->itemCollection->add($item);
    }
}
