<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Service;

use NikoM\Advertising\Outdoor\Domain\Basket\Entity\Basket;
use NikoM\Advertising\Outdoor\Domain\Basket\Entity\IBasketRepository;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Outdoor\Domain\Basket\Entity\ItemCollection;

class BasketService
{
    private IBasketRepository $repository;

    public function __construct(IBasketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getByBuyer(BuyerId $buyerId): Basket
    {
        try {
            return $this->repository->getByBuyerId($buyerId);
        } catch (BasketNotFoundException $exception) {
            return new Basket($buyerId);
        }
    }
}
