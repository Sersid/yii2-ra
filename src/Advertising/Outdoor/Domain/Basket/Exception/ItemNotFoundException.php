<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class ItemNotFoundException extends DomainException
{
    public function message(): string
    {
        return 'Item not found';
    }

    public function code(): int
    {
        return 404;
    }
}
