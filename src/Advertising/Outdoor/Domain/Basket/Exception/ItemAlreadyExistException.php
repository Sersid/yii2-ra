<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class ItemAlreadyExistException extends DomainException
{
    public function message(): string
    {
        return 'Item already exist';
    }

    public function code(): int
    {
        return 400;
    }
}
