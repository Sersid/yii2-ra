<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class MonthAlreadyExistException extends DomainException
{
    public function message(): string
    {
        return 'Month already exist';
    }

    public function code(): int
    {
        return 400;
    }
}
