<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Application\Side\FindSides;

use NikoM\Advertising\Outdoor\Domain\Side\Entity\ISideFetcher;
use NikoM\Advertising\Outdoor\Domain\Side\Query\Filter;

class FindSidesQueryHandler
{
    private ISideFetcher $fetcher;

    public function __construct(ISideFetcher $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    public function handle(FindSidesQuery $query)
    {
        $filter = new Filter();
        $filter->setCityId($query->cityId);
        $filter->setStreetId($query->streetId);
        $filter->setFormats($query->formats);
        $filter->setPeriods($query->periods);
        $filter->setPriceFrom($query->priceFrom);
        $filter->setPriceTo($query->priceTo);
        $filter->setRatings($query->ratings);
        $filter->setSides($query->sides);
        $filter->setIsPromoOnly($query->onlyWithPromotion);

        return $this->fetcher->paginate($filter, $query->page, $query->perPage);
    }
}
