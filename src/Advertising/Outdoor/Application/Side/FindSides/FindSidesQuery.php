<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Application\Side\FindSides;

class FindSidesQuery
{
    public $cityId;
    public $streetId;
    public $formats;
    public $periods;
    public $priceFrom;
    public $priceTo;
    public $ratings;
    public $sides;
    public $onlyWithPromotion;

    public int $page = 1;
    public int $perPage = 20;
}
