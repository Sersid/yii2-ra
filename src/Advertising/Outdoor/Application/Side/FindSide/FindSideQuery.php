<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Application\Side\FindSide;

class FindSideQuery
{
    public string $fullNumber;
    public string $letter;
}
