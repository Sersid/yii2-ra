<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Application\Side\FindSide;

use NikoM\Advertising\Outdoor\Domain\Construction\Entity\FullNumber;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\ISideRepository;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Letter;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\Side;

class FindSideQueryHandler
{
    private ISideRepository $repository;

    public function __construct(ISideRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(FindSideQuery $query): Side
    {
        $fullNumber = new FullNumber($query->fullNumber);
        $letter = Letter::fromLatin($query->letter);

        return $this->repository->getByFullNumberAndLetter($fullNumber, $letter);
    }
}
