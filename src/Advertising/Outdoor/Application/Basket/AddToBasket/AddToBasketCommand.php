<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Application\Basket\AddToBasket;

class AddToBasketCommand
{
    public int $buyerId;
    public int $sideId;
    public string $month;
}
