<?php
declare(strict_types=1);

namespace NikoM\Advertising\Outdoor\Application\Basket\AddToBasket;

use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Outdoor\Domain\Basket\Entity\Basket;
use NikoM\Advertising\Outdoor\Domain\Basket\Entity\IBasketRepository;
use NikoM\Advertising\Outdoor\Domain\Basket\Entity\Item;
use NikoM\Advertising\Outdoor\Domain\Basket\Entity\Month;
use NikoM\Advertising\Outdoor\Domain\Basket\Exception\ItemNotFoundException;
use NikoM\Advertising\Outdoor\Domain\Basket\Service\BasketService;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\ISideRepository;
use NikoM\Advertising\Outdoor\Domain\Side\Entity\SideId;
use NikoM\Advertising\Outdoor\Domain\Side\Exception\SideNotFoundException;

class AddToBasketCommandHandler
{
    private BasketService $basketService;
    private IBasketRepository $basketRepository;
    private ISideRepository $sideRepository;

    public function __construct(
        BasketService $basketService,
        IBasketRepository $basketRepository,
        ISideRepository $sideRepository)
    {
        $this->basketService = $basketService;
        $this->basketRepository = $basketRepository;
        $this->sideRepository = $sideRepository;
    }

    /**
     * @param AddToBasketCommand $command
     *
     * @return Basket
     * @throws BasketNotFoundException
     * @throws SideNotFoundException
     */
    public function handle(AddToBasketCommand $command): Basket
    {
        $basket = $this->basketService->getByBuyer(new BuyerId($command->buyerId));
        $side = $this->sideRepository->getById(new SideId($command->sideId));

        try {
            $item = $basket->getItem($side);
        } catch (ItemNotFoundException $e) {
            $item = new Item($side);
            $basket->addItem($item);
        }

        $month = Month::fromString($command->month);
        $item->addMonth($month);

        $this->basketRepository->save($basket);

        return $basket;
    }
}
