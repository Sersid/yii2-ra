<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\City;

use JsonSerializable;
use NikoM\Kernel\Domain\Entity\Collection\Collection;
/**
 * @deprecated
 */
class CityViewCollection extends Collection implements JsonSerializable
{
    protected static function type(): string
    {
        return CityView::class;
    }

    public static function createFromArray(array $cities): self
    {
        $result = [];
        foreach ($cities as $city) {
            $cityView = new CityView();
            if(isset($city['id'])) {
                $cityView->id = (int)$city['id'];
            }
            if(isset($city['name'])) {
                $cityView->name = $city['name'];
            }
            if(isset($city['slug'])) {
                $cityView->slug = $city['slug'];
            }
            $result[] = $cityView;
        }
        return new self($result);
    }

    public function jsonSerialize(): array
    {
        return $this->all();
    }
}
