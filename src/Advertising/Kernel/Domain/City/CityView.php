<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\City;

/**
 * @deprecated
 */
class CityView
{
    public int $id;
    public string $name;
    public string $slug;
}
