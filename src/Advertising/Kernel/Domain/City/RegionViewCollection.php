<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\City;

use JsonSerializable;
use NikoM\Kernel\Domain\Entity\Collection\Collection;
/**
 * @deprecated
 */
class RegionViewCollection extends Collection implements JsonSerializable
{
    protected static function type(): string
    {
        return RegionView::class;
    }

    public static function createFromArray(array $regions): self
    {
        $result = [];
        foreach ($regions as $region) {
            $regionView = new RegionView();
            if (isset($region['id'])) {
                $regionView->id = (int)$region['id'];
            }
            if (isset($region['name'])) {
                $regionView->name = $region['name'];
            }
            if (isset($region['cities'])) {
                $regionView->cities = CityViewCollection::createFromArray($region['cities']);
            }
            $result[] = $regionView;
        }
        return new self($result);
    }

    public function jsonSerialize(): array
    {
        return $this->all();
    }
}
