<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\City;
/**
 * @deprecated
 */
class RegionView
{
    public int $id;
    public string $name;
    public CityViewCollection $cities;

    public function __toString(): string
    {
        return $this->name;
    }
}
