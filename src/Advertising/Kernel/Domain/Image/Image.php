<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\Image;

class Image
{
    private string $src;
    private string $alt;

    public function getSrc(): string
    {
        return $this->src;
    }

    public function getAlt(): string
    {
        return $this->alt;
    }
}
