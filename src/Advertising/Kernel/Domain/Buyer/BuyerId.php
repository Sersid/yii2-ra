<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\Buyer;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdNullableValueObject;

class BuyerId extends IdNullableValueObject
{
}
