<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class BasketAdvTypeAlreadyException extends DomainException
{
    public function message(): string
    {
        return 'The ad type is already set.';
    }

    public function code(): int
    {
        return 400;
    }
}
