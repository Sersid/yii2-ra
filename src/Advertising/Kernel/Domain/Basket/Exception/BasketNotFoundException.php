<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Domain\Basket\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class BasketNotFoundException extends DomainException
{
    public function message(): string
    {
        return 'Basket not found.';
    }

    public function code(): int
    {
        return 404;
    }
}
