<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Infrastructure\Persistence;

use common\models\City as Model;
use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;
use yii\db\ActiveQuery;

abstract class AbstractCityFetcher
{
    public function all(): RegionViewCollection
    {
        return RegionViewCollection::createFromArray($this->getRegions());
    }

    protected function getRegions(): array
    {
        $citiesId = $this->getCitiesId();
        if (empty($citiesId)) {
            return [];
        }
        $cities = Model::find()
            ->select([
                'id' => 'city.id',
                'name' => 'city.name',
                'region_id',
                'region_name' => 'region.name',
            ])
            ->where(['city.status' => Model::STATUS_ACTIVE, 'city.id' => $citiesId])
            ->joinWith('region')
            ->orderBy(['region.sort' => SORT_ASC, 'city.sort' => SORT_ASC, 'city.name' => SORT_ASC])
            ->asArray()
            ->all();
        $result = [];
        foreach ($cities as $city) {
            $result[$city['region_id']]['name'] = (string)$city['region_name'];
            $result[$city['region_id']]['cities'][] = [
                'id' => $city['id'],
                'name' => $city['name'],
            ];
        }

        return $result;
    }

    private function getCitiesId(): array
    {
        $result = [];
        $cities = $this->getQuery()
            ->asArray()
            ->all();
        foreach ($cities as $city) {
            $result[] = (int)$city['city_id'];
        }

        return $result;
    }

    abstract protected function getQuery(): ActiveQuery;
}
