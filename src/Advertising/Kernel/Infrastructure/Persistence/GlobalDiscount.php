<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Infrastructure\Persistence;

use Yii;

abstract class GlobalDiscount
{
    protected ?float $discount;

    abstract protected function getConfigKey(): string;

    public function __construct()
    {
        $discount = (float)Yii::$app->config->get($this->getConfigKey());
        $this->discount = $discount > 0 && $discount < 100 ? $discount : null;
    }

    public function getValue(): ?float
    {
        return $this->discount;
    }

    public function has(): bool
    {
        return !is_null($this->discount);
    }
}
