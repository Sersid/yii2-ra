<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\Infrastructure\Persistence\Order;

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderRepository;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\OrderId;

class OrderRepository implements IOrderRepository
{
    public function add(Order $order): void
    {
        $model = new \common\models\Order();
        $model->guest_id = $order->getBasket()->getBuyerId()->getValue();
        $model->name = $order->getName();
        $model->phone = $order->getPhone();
        $model->email = $order->getEmail();
        $model->save(false);

        $order->setId(new OrderId($model->id));
    }
}
