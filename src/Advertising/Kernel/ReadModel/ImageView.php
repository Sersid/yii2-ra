<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\ReadModel;

use NikoM\Advertising\Kernel\DTO\ImageDto;

class ImageView
{
    private ImageDto $dto;

    public function __construct(ImageDto $dto)
    {
        $this->dto = $dto;
    }

    public function src(?int $width = null, ?int $height = null, ?int $sharpen = null): string
    {
        $size = [];
        if (!is_null($width)) {
            $size[] = 'w' . $width;
        }
        if (!is_null($height)) {
            $size[] = 'h' . $height;
        }
        if (!is_null($sharpen)) {
            $size[] = 'sh' . $sharpen;
        }

        $image = ['image', $this->dto->id];
        if (!empty($size)) {
            $image[] = implode('_', $size);
        }
        $image[] = $this->dto->code . '.' . $this->dto->ext;

        return '/' . implode('/', $image);
    }

    public function originalSizeSrc(): string
    {
        return '/image/' . $this->dto->id . '/original/' . $this->dto->code . '.' . $this->dto->ext;
    }

    public function alt(): ?string
    {
        return $this->dto->alt;
    }
    
    public function title(): ?string
    {
        return $this->dto->title;
    }

    public function width(): ?int
    {
        return $this->dto->width;
    }
    
    public function height(): ?int
    {
        return $this->dto->height;
    }
}
