<?php
declare(strict_types=1);

namespace NikoM\Advertising\Kernel\DTO;

use NikoM\Kernel\Domain\Dto;

class ImageDto extends Dto
{
    public int $id;
    public string $code;
    public string $ext;
    public int $width;
    public int $height;
    public ?string $alt;
    public ?string $title;
}
