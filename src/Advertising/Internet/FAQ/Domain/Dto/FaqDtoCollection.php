<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Dto;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method FaqDto[] all()
 */
class FaqDtoCollection extends Collection
{
    protected static function type(): string
    {
        return FaqDto::class;
    }
}
