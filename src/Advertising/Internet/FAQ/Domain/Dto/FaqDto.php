<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Dto;

class FaqDto
{
    public int $id;
    public string $question;
    public string $answer;
    public bool $isActive;
    public bool $isGlobal;
    public int $sort;
    public string $dateCreated;
    public ?string $dateUpdated;
}
