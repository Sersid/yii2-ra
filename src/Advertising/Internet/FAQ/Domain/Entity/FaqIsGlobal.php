<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Bool\BoolValueObject;

class FaqIsGlobal extends BoolValueObject
{
}
