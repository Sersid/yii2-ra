<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;
use Webmozart\Assert\Assert;

class FaqQuestion extends StringValueObject
{
    public function __construct(string $value)
    {
        $value = trim($value);
        Assert::notEmpty($value);
        parent::__construct($value);
    }
}
