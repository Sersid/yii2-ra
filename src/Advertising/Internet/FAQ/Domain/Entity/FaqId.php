<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdValueObject;

class FaqId extends IdValueObject
{
}
