<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Entity;

use NikoM\Advertising\Internet\FAQ\Domain\Exception\FaqNotFoundException;

interface IFaqRepository
{
    public function add(Faq $faq): void;
    /**
     * @param FaqId $id
     *
     * @return Faq
     * @throws FaqNotFoundException
     */
    public function getById(FaqId $id): Faq;
    public function save(Faq $faq): void;
    public function delete(FaqId $id): void;
}
