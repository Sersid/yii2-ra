<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Entity;

use DateTimeImmutable;
use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDto;

class Faq
{
    private FaqId $id;
    private FaqQuestion $question;
    private FaqAnswer $answer;
    private FaqIsActive $isActive;
    private FaqIsGlobal $isGlobal;
    private FaqSort $sort;
    private DateTimeImmutable $dateCreated;
    private ?DateTimeImmutable $dateUpdated;

    private function __construct()
    {
    }

    public static function create(FaqQuestion $question,
        FaqAnswer $answer,
        FaqIsActive $isActive,
        FaqSort $sort,
        FaqIsGlobal $isGlobal): self
    {
        $self = new self();
        $self->question = $question;
        $self->answer = $answer;
        $self->isActive = $isActive;
        $self->isGlobal = $isGlobal;
        $self->sort = $sort;
        $self->dateCreated = new DateTimeImmutable();

        return $self;
    }

    public static function fromDTO(FaqDto $dto): self
    {
        $self = new self();
        $self->id = new FaqId($dto->id);
        $self->question = new FaqQuestion($dto->question);
        $self->answer = new FaqAnswer($dto->answer);
        $self->isActive = new FaqIsActive($dto->isActive);
        $self->isGlobal = new FaqIsGlobal($dto->isGlobal);
        $self->sort = new FaqSort($dto->sort);
        $self->dateCreated = new DateTimeImmutable($dto->dateCreated);
        $self->dateUpdated = is_null($dto->dateUpdated) ? null : new DateTimeImmutable($dto->dateUpdated);

        return $self;
    }

    public function getId(): FaqId
    {
        return $this->id;
    }

    public function getQuestion(): FaqQuestion
    {
        return $this->question;
    }

    public function changeQuestion(FaqQuestion $question): void
    {
        if (!$this->question->isEqual($question)) {
            $this->question = $question;
            $this->onChanged();
        }
    }

    public function getAnswer(): FaqAnswer
    {
        return $this->answer;
    }

    public function changeAnswer(FaqAnswer $answer): void
    {
        if (!$this->answer->isEqual($answer)) {
            $this->answer = $answer;
            $this->onChanged();
        }
    }

    public function getIsActive(): FaqIsActive
    {
        return $this->isActive;
    }

    public function changeIsActive(FaqIsActive $status): void
    {
        if (!$this->isActive->isEqual($status)) {
            $this->isActive = $status;
            $this->onChanged();
        }
    }

    public function getIsGlobal(): FaqIsGlobal
    {
        return $this->isGlobal;
    }

    public function changeIsGlobal(FaqIsGlobal $isGlobal): void
    {
        if (!$this->isGlobal->isEqual($isGlobal)) {
            $this->isGlobal = $isGlobal;
            $this->onChanged();
        }
    }

    public function getSort(): FaqSort
    {
        return $this->sort;
    }

    public function changeSort(FaqSort $sort): void
    {
        if (!$this->sort->isEqual($sort)) {
            $this->sort = $sort;
            $this->onChanged();
        }
    }

    private function onChanged(): void
    {
        $this->dateUpdated = new DateTimeImmutable();
    }

    public function getDateCreated(): DateTimeImmutable
    {
        return $this->dateCreated;
    }

    public function getDateUpdated(): ?DateTimeImmutable
    {
        return $this->dateUpdated;
    }
}
