<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\NotFoundException;

class FaqNotFoundException extends NotFoundException
{
    public function message(): string
    {
        return 'Faq not found';
    }
}
