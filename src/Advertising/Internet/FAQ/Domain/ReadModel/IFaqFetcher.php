<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\ReadModel;

use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDto;
use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDtoCollection;

interface IFaqFetcher
{
    public function findById(int $id): ?FaqDto;

    public function findCollection(FaqFilter $filter): FaqDtoCollection;
}
