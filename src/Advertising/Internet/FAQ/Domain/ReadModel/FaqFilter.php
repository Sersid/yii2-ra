<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\ReadModel;

class FaqFilter
{
    public bool $activeOnly;
    public bool $globalOnly;
}
