<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\ReadModel;

use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDto;

class FaqView
{
    private FaqDto $dto;

    public function __construct(FaqDto $dto)
    {
        $this->dto = $dto;
    }

    public function id(): int
    {
        return $this->dto->id;
    }

    public function question(): string
    {
        return $this->dto->question;
    }

    public function answer(): string
    {
        return $this->dto->answer;
    }
}
