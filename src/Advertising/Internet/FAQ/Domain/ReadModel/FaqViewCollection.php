<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Domain\ReadModel;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method FaqView[] all()
 */
class FaqViewCollection extends Collection
{
    protected static function type(): string
    {
        return FaqView::class;
    }
}
