<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Infrastructure;

use common\models\Advertising\Internet\Faq;
use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDto;
use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDtoCollection;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqFilter;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\IFaqFetcher;

class FaqFetcher implements IFaqFetcher
{
    private FaqConverter $faqConverter;

    public function __construct(FaqConverter $faqConverter)
    {
        $this->faqConverter = $faqConverter;
    }

    public function findById(int $id): ?FaqDto
    {
        $data = Faq::find()
            ->where(['id' => $id])
            ->asArray()
            ->one();

        if (empty($data)) {
            return null;
        } else {
            return $this->faqConverter->fromArrayToDTO($data);
        }
    }

    public function findCollection(FaqFilter $filter): FaqDtoCollection
    {
        $query = Faq::find()
            ->select(['id', 'question', 'answer'])
            ->orderBy(['sort' => SORT_ASC])
            ->asArray();

        if (isset($filter->activeOnly)) {
            $query->andWhere(['is_active' => 1]);
        }
        if (isset($filter->globalOnly)) {
            $query->andWhere(['is_global' => 1]);
        }

        $items = [];
        foreach ($query->all() as $data) {
            $items[] = $this->faqConverter->fromArrayToDTO($data);
        }

        return new FaqDtoCollection($items);
    }
}
