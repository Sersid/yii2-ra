<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Infrastructure;

use common\models\Advertising\Internet\Faq as FaqModel;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\Faq;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqId;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\IFaqRepository;
use NikoM\Advertising\Internet\FAQ\Domain\Exception\FaqNotFoundException;

class FaqRepository implements IFaqRepository
{
    private FaqFetcher $faqFetcher;

    public function __construct(FaqFetcher $faqFetcher)
    {
        $this->faqFetcher = $faqFetcher;
    }

    public function add(Faq $faq): void
    {
        $model = new FaqModel();
        $model->question = $faq->getQuestion()->getValue();
        $model->answer = $faq->getAnswer()->getValue();
        $model->is_active = $faq->getIsActive()->getValue();
        $model->is_global = $faq->getIsGlobal()->getValue();
        $model->sort = $faq->getSort()->getValue();
        $model->date_created = $faq->getDateCreated()->format('Y-m-d H:i:s');
        $model->save(false);
    }

    public function getById(FaqId $id): Faq
    {
        $faqDTO = $this->faqFetcher->findById($id->getValue());
        if (empty($faqDTO)) {
            throw new FaqNotFoundException();
        }

        return Faq::fromDTO($faqDTO);
    }

    public function save(Faq $faq): void
    {
        FaqModel::updateAll(
            [
                'question' => $faq->getQuestion()->getValue(),
                'answer' => $faq->getAnswer()->getValue(),
                'is_active' => $faq->getIsActive()->getValue(),
                'is_global' => $faq->getIsGlobal() ->getValue(),
                'sort' => $faq->getSort()->getValue(),
                'date_updated' => $faq->getDateUpdated()->format('Y-m-d H:i:s'),
            ],
            ['id' => $faq->getId()->getValue()]
        );
    }

    public function delete(FaqId $id): void
    {
        FaqModel::deleteAll(['id' => $id]);
    }
}
