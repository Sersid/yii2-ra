<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Infrastructure;

use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDto;
use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDtoCollection;

class FaqConverter
{
    public function fromArrayToDTO(array $data): FaqDto
    {
        $dto = new FaqDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('question', $data)) {
            $dto->question = $data['question'];
        }
        if (array_key_exists('answer', $data)) {
            $dto->answer = $data['answer'];
        }
        if (array_key_exists('is_active', $data)) {
            $dto->isActive = (bool)$data['is_active'];
        }
        if (array_key_exists('is_global', $data)) {
            $dto->isGlobal = (bool)$data['is_global'];
        }
        if (array_key_exists('sort', $data)) {
            $dto->sort = (int)$data['sort'];
        }
        if (array_key_exists('date_created', $data)) {
            $dto->dateCreated = $data['date_created'];
        }
        if (array_key_exists('date_updated', $data)) {
            $dto->dateUpdated = $data['date_updated'];
        }

        return $dto;
    }

    public function fromArrayToDTOCollection(array $items): FaqDtoCollection
    {
        $result = [];
        foreach ($items as $item) {
            $result[] = $this->fromArrayToDTO($item);
        }

        return new FaqDtoCollection($result);
    }
}
