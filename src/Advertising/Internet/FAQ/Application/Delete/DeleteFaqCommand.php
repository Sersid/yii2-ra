<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\Delete;

class DeleteFaqCommand
{
    public int $id;
}
