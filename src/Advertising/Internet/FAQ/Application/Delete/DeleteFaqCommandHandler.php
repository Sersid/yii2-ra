<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\Delete;

use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqId;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\IFaqRepository;

class DeleteFaqCommandHandler
{
    private IFaqRepository $faqRepository;

    public function __construct(IFaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    public function handle(DeleteFaqCommand $command): void
    {
        $id = new FaqId($command->id);
        $this->faqRepository->delete($id);
    }
}
