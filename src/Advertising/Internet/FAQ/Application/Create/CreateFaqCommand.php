<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\Create;

class CreateFaqCommand
{
    public string $question;
    public string $answer;
    public bool $isActive;
    public bool $isGlobal;
    public ?int $sort;
}
