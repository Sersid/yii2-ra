<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\Create;

use NikoM\Advertising\Internet\FAQ\Domain\Entity\Faq;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqAnswer;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqIsGlobal;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqQuestion;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqSort;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqIsActive;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\IFaqRepository;

class CreateFaqCommandHandler
{
    private IFaqRepository $faqRepository;

    public function __construct(IFaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    public function handle(CreateFaqCommand $command): void
    {
        $question = new FaqQuestion($command->question);
        $answer = new FaqAnswer($command->answer);
        $isActive = new FaqIsActive($command->isActive);
        $isGlobal = new FaqIsGlobal($command->isGlobal);
        $sort = new FaqSort($command->sort);
        $faq = Faq::create($question, $answer, $isActive, $sort, $isGlobal);

        $this->faqRepository->add($faq);
    }
}
