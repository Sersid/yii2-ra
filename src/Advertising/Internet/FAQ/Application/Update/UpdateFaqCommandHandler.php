<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\Update;

use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqAnswer;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqId;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqIsGlobal;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqQuestion;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqSort;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\FaqIsActive;
use NikoM\Advertising\Internet\FAQ\Domain\Entity\IFaqRepository;

class UpdateFaqCommandHandler
{
    private IFaqRepository $faqRepository;

    public function __construct(IFaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    public function handle(UpdateFaqCommand $command): void
    {
        $id = new FaqId($command->id);
        $question = new FaqQuestion($command->question);
        $answer = new FaqAnswer($command->answer);
        $isActive = new FaqIsActive($command->isActive);
        $isGlobal = new FaqIsGlobal($command->isGlobal);
        $sort = new FaqSort($command->sort);

        $faq = $this->faqRepository->getById($id);
        $faq->changeQuestion($question);
        $faq->changeAnswer($answer);
        $faq->changeIsActive($isActive);
        $faq->changeIsGlobal($isGlobal);
        $faq->changeSort($sort);

        $this->faqRepository->save($faq);
    }
}
