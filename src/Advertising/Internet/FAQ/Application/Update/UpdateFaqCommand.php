<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\Update;

use NikoM\Advertising\Internet\FAQ\Application\Create\CreateFaqCommand;

class UpdateFaqCommand extends CreateFaqCommand
{
    public int $id;
}
