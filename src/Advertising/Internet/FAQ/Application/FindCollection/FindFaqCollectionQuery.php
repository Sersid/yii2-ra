<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\FindCollection;

use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqFilter;

class FindFaqCollectionQuery
{
    public FaqFilter $filter;
}
