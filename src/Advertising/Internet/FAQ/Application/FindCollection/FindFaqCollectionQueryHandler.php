<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\FindCollection;

use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqViewCollection;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\IFaqFetcher;

class FindFaqCollectionQueryHandler
{
    private IFaqFetcher $faqFetcher;

    public function __construct(IFaqFetcher $faqFetcher)
    {
        $this->faqFetcher = $faqFetcher;
    }

    public function handle(FindFaqCollectionQuery $query): FaqViewCollection
    {
        $dtoCollection = $this->faqFetcher->findCollection($query->filter);

        return FaqViewCollection::fromDtoCollection($dtoCollection);
    }
}
