<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\FindOne;

class FindOneFaqQuery
{
    public int $id;
}
