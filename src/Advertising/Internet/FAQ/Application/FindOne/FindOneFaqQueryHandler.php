<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\FAQ\Application\FindOne;

use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDto;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\IFaqFetcher;

class FindOneFaqQueryHandler
{
    private IFaqFetcher $faqFetcher;

    public function __construct(IFaqFetcher $faqFetcher)
    {
        $this->faqFetcher = $faqFetcher;
    }

    public function handle(FindOneFaqQuery $query): ?FaqDto
    {
        return $this->faqFetcher->findById($query->id);
    }
}
