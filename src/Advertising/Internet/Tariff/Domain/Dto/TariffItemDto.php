<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Tariff\Domain\Dto;

class TariffItemDto
{
    public string $name;
    public string $description;
    public float $price;
    public string $position;
}
