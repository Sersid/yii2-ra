<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Tariff\Domain\Dto;

use NikoM\Kernel\Domain\Dto;

class TariffDto extends Dto
{
    public string $title;
    public ?string $description;
    public TariffItemDtoCollection $itemCollection;
}
