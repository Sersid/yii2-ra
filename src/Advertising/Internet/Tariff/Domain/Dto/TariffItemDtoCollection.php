<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Tariff\Domain\Dto;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method TariffItemDto[] all()
 */
class TariffItemDtoCollection extends Collection
{
    protected static function type(): string
    {
        return TariffItemDto::class;
    }
}
