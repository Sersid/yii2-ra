<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Tariff\Domain\ReadModel;

use NikoM\Advertising\Internet\Tariff\Domain\Dto\TariffItemDto;

class TariffViewItem
{
    private TariffItemDto $dto;

    public function __construct(TariffItemDto $dto)
    {
        $this->dto = $dto;
    }

    public function name(): string
    {
        return $this->dto->name;
    }

    public function description(): string
    {
        return $this->dto->description;
    }

    public function price(): float
    {
        return $this->dto->price;
    }

    public function position(): string
    {
        return $this->dto->position;
    }
}
