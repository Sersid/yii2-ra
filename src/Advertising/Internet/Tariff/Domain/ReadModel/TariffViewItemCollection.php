<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Tariff\Domain\ReadModel;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method TariffViewItem[] all()
 */
class TariffViewItemCollection extends Collection
{
    protected static function type(): string
    {
        return TariffViewItem::class;
    }
}
