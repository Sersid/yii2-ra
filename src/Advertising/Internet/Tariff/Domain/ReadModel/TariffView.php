<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Tariff\Domain\ReadModel;

use NikoM\Advertising\Internet\Tariff\Domain\Dto\TariffDto;

class TariffView
{
    private TariffDto $dto;
    private TariffViewItemCollection $itemCollection;

    public function __construct(TariffDto $dto)
    {
        $this->dto = $dto;
        $this->itemCollection = TariffViewItemCollection::fromDtoCollection($dto->itemCollection);
    }

    public function title(): string
    {
        return $this->dto->title;
    }

    public function hasDescription(): bool
    {
        return !is_null($this->dto->description);
    }

    public function description(): ?string
    {
        return $this->dto->description;
    }

    public function itemCollection(): TariffViewItemCollection
    {
        return $this->itemCollection;
    }
}
