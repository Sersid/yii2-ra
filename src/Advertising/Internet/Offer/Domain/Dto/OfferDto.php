<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\Dto;

use NikoM\Advertising\Internet\FAQ\Domain\Dto\FaqDtoCollection;
use NikoM\Advertising\Internet\MoreOffer\Domain\Dto\MoreOfferDtoCollection;
use NikoM\Advertising\Internet\SubOffer\Domain\Dto\SubOfferDTOCollection;
use NikoM\Advertising\Internet\Tariff\Domain\Dto\TariffDto;
use NikoM\Kernel\Domain\Dto;

class OfferDto extends Dto
{
    public int $id;
    public string $name;
    public string $slug;
    public string $icon;
    public ?string $introText;
    public ?string $pageTitle;
    public ?string $pageHeading;
    public ?string $pageDescription;
    public ?string $pageTitleWithCity;
    public ?string $pageHeadingWithCity;
    public ?string $pageDescriptionWithCity;
    public TariffDto $tariff;
    public SubOfferDTOCollection $subOfferDTOCollection;
    public MoreOfferDtoCollection $moreOfferDtoCollection;
    public FaqDtoCollection $faqDtoCollection;
}
