<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\Dto;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method OfferDto[] all()
 */
class OfferDtoCollection extends Collection
{
    protected static function type(): string
    {
        return OfferDto::class;
    }
}
