<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\NotFoundException;

class OfferNotFoundException extends NotFoundException
{
    public function message(): string
    {
        return 'Offer not found';
    }
}
