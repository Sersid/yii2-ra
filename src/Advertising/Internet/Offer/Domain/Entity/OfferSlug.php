<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Slug\SlugValueObject;

class OfferSlug extends SlugValueObject
{
}
