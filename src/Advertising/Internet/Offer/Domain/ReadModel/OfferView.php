<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel;

use NikoM\Advertising\Internet\Kernel\Domain\ReadModel\IconView;
use NikoM\Advertising\Internet\MoreOffer\Domain\ReadModel\MoreOfferViewCollection;
use NikoM\Advertising\Internet\FAQ\Domain\ReadModel\FaqViewCollection;
use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDto;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy\WithCityOfferStrategy;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy\WithoutCityOfferStrategy;
use NikoM\Advertising\Internet\SubOffer\Domain\ReadModel\SubOfferViewCollection;
use NikoM\Advertising\Internet\Tariff\Domain\ReadModel\TariffView;
use NikoM\Location\City\Domain\DTO\CityDto;

class OfferView implements IOfferStrategy
{
    private OfferDto $dto;
    private IconView $icon;
    private SubOfferViewCollection $subOfferCollection;
    private TariffView $tariff;
    private MoreOfferViewCollection $moreOfferViewCollection;
    private FaqViewCollection $faqCollection;
    private IOfferStrategy $strategy;

    public function __construct(OfferDto $dto)
    {
        $this->dto = $dto;
        if (isset($dto->icon)) {
            $this->icon = new IconView($dto->icon);
        }
        if (isset($dto->subOfferDTOCollection)) {
            $this->subOfferCollection = SubOfferViewCollection::fromDtoCollection($dto->subOfferDTOCollection);
        }
        if (isset($dto->moreOfferDtoCollection)) {
            $this->moreOfferViewCollection = MoreOfferViewCollection::fromDtoCollection($dto->moreOfferDtoCollection);
        }
        if (isset($dto->faqDtoCollection)) {
            $this->faqCollection = FaqViewCollection::fromDtoCollection($dto->faqDtoCollection);
        }
        if (isset($dto->tariff)) {
            $this->tariff = new TariffView($dto->tariff);
        }
        $this->strategy = new WithoutCityOfferStrategy($dto);
    }

    public function setCity(CityDto $cityDto) {
        $this->strategy = new WithCityOfferStrategy($this->dto, $cityDto);
    }

    public function id(): int
    {
        return $this->dto->id;
    }

    public function name(): string
    {
        return $this->dto->name;
    }

    public function slug(): string
    {
        return $this->dto->slug;
    }

    public function url(): string
    {
        return $this->strategy->url();
    }

    public function icon(): IconView
    {
        return $this->icon;
    }

    public function hasIntroText(): bool
    {
        return !is_null($this->introText());
    }

    public function introText(): ?string
    {
        return $this->dto->introText;
    }

    public function pageTitle(): string
    {
        return $this->strategy->pageTitle();
    }

    public function pageHeading(): string
    {
        return $this->strategy->pageHeading();
    }

    public function pageDescription(): string
    {
        return $this->strategy->pageDescription();
    }

    public function subOfferCollection(): SubOfferViewCollection
    {
        return $this->subOfferCollection;
    }

    public function tariff(): TariffView
    {
        return $this->tariff;
    }

    public function moreOfferCollection(): MoreOfferViewCollection
    {
        return $this->moreOfferViewCollection;
    }

    public function faqCollection(): FaqViewCollection
    {
        return $this->faqCollection;
    }

    public function seoTextCode(): string
    {
      return $this->strategy->seoTextCode();
    }
}
