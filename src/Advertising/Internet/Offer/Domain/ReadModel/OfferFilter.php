<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel;

use NikoM\Advertising\Internet\Offer\Domain\Entity\OfferSlug;

class OfferFilter
{
    public bool $activeOnly;
    public OfferSlug $slug;
}
