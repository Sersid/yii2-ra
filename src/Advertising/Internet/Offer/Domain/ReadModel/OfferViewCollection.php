<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel;

use NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy\WithCityOfferCollectionStrategy;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy\WithoutCityOfferCollectionStrategy;
use NikoM\Kernel\Domain\Entity\Collection\Collection;
use NikoM\Location\City\Domain\DTO\CityDto;

/**
 * @method OfferView[] all()
 */
class OfferViewCollection extends Collection implements IOfferCollectionStrategy
{
    private IOfferCollectionStrategy $strategy;

    public function __construct(array $items)
    {
        parent::__construct($items);
        $this->strategy = new WithoutCityOfferCollectionStrategy();
    }

    protected static function type(): string
    {
        return OfferView::class;
    }

    public function setCity(CityDto $cityDto): void
    {
        $this->strategy = new WithCityOfferCollectionStrategy($cityDto);
        foreach ($this->all() as $offerView) {
            $offerView->setCity($cityDto);
        }
    }

    public function pageTitle(): string
    {
        return $this->strategy->pageTitle();
    }

    public function pageHeading(): string
    {
        return $this->strategy->pageHeading();
    }

    public function pageDescription(): string
    {
        return $this->strategy->pageDescription();
    }

    public function seoTextCode(): string
    {
        return $this->strategy->seoTextCode();
    }
}
