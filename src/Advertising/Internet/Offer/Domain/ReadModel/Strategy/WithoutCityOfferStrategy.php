<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy;

use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDto;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferStrategy;

class WithoutCityOfferStrategy implements IOfferStrategy
{
    private OfferDto $dto;

    public function __construct(OfferDto $dto)
    {
        $this->dto = $dto;
    }

    public function pageTitle(): string
    {
        return $this->dto->pageTitle ?? $this->dto->name;
    }

    public function pageHeading(): string
    {
        return $this->dto->pageHeading ?? $this->dto->name;
    }

    public function pageDescription(): string
    {
        return $this->dto->pageDescription ?? $this->dto->name;
    }

    public function url(): string
    {
        return '/internet-ad/' . $this->dto->slug;
    }

    public function seoTextCode(): string
    {
        return 'internet-advertising-offer-' . $this->dto->slug;
    }
}
