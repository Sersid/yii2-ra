<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy;

use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDto;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferStrategy;
use NikoM\Location\City\Domain\DTO\CityDto;

class WithCityOfferStrategy implements IOfferStrategy
{
    private OfferDto $offerDto;
    private CityDto $cityDto;

    public function __construct(OfferDto $offerDto, CityDto $cityDto)
    {
        $this->offerDto = $offerDto;
        $this->cityDto = $cityDto;
    }

    public function pageTitle(): string
    {
        if ($this->offerDto->pageTitleWithCity !== null) {
            return $this->replace($this->offerDto->pageTitleWithCity);
        } elseif ($this->offerDto->pageTitle !== null) {
            return $this->offerDto->pageTitle;
        } else {
            return $this->offerDto->name;
        }
    }

    public function pageHeading(): string
    {
        if ($this->offerDto->pageHeadingWithCity !== null) {
            return $this->replace($this->offerDto->pageHeadingWithCity);
        } elseif ($this->offerDto->pageHeading !== null) {
            return $this->offerDto->pageHeading;
        } else {
            return $this->offerDto->name;
        }
    }

    public function pageDescription(): string
    {
        if ($this->offerDto->pageDescriptionWithCity !== null) {
            return $this->replace($this->offerDto->pageDescriptionWithCity);
        } elseif ($this->offerDto->pageDescription !== null) {
            return $this->offerDto->pageDescription;
        } else {
            return $this->offerDto->name;
        }
    }

    private function replace(string $text): string
    {
        return  strtr($text, ['#CITY_WHERE#' => $this->cityDto->where, '#CITY_NAME#' => $this->cityDto->name]);
    }

    public function url(): string
    {
        return '/internet-ad/' . $this->offerDto->slug . '/' . $this->cityDto->slug;
    }

    public function seoTextCode(): string
    {
        return 'internet-advertising-offer-' . $this->offerDto->slug . '-' . $this->cityDto->slug;
    }
}
