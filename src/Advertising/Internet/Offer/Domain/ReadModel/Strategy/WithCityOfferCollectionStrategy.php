<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy;

use NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferCollectionStrategy;
use NikoM\Location\City\Domain\DTO\CityDto;

class WithCityOfferCollectionStrategy implements IOfferCollectionStrategy
{
    private CityDto $cityDto;

    public function __construct(CityDto $cityDto)
    {
        $this->cityDto = $cityDto;
    }

    public function pageTitle(): string
    {
        return 'Реклама в интернете ' . $this->cityDto->where . ' от агентства интернет рекламы «Нико-М»';
    }

    public function pageHeading(): string
    {
        return 'Реклама в интернете ' . $this->cityDto->where;
    }

    public function pageDescription(): string
    {
        return 'Комплексный интернет-маркетинг ' . $this->cityDto->where . ' от создания контекстной рекламы и'
               . ' продвижения сайтов до ведения Ваших соцсетей. Делаем под ключ все работы по интернет рекламе.'
               . ' Работаем на результат!';
    }

    public function seoTextCode(): string
    {
        return 'internet-advertising-' . $this->cityDto->slug;
    }
}
