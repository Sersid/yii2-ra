<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel\Strategy;

use NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferCollectionStrategy;

class WithoutCityOfferCollectionStrategy implements IOfferCollectionStrategy
{
    public function pageTitle(): string
    {
        return 'Реклама в интернете от агентства интернет рекламы «Нико-М»';
    }

    public function pageHeading(): string
    {
        return 'Реклама в интернете';
    }

    public function pageDescription(): string
    {
        return 'Комплексный интернет-маркетинг от создания контекстной рекламы и продвижения сайтов до ведения'
               . ' Ваших соцсетей. Делаем под ключ все работы по интернет рекламе. Работаем на результат!';
    }

    public function seoTextCode(): string
    {
        return 'internet-advertising';
    }
}
