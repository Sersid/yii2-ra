<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel;

interface IOfferCollectionStrategy
{
    public function pageTitle(): string;
    public function pageHeading(): string;
    public function pageDescription(): string;
    public function seoTextCode(): string;
}
