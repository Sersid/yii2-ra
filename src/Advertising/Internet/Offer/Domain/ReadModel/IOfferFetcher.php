<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel;

use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDto;
use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDtoCollection;

interface IOfferFetcher
{
    public function findOne(OfferFilter $filter): ?OfferDto;
    public function findCollection(OfferFilter $filter): OfferDtoCollection;
}
