<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Domain\ReadModel;

interface IOfferStrategy
{
    public function pageTitle(): string;
    public function pageHeading(): string;
    public function pageDescription(): string;
    public function url(): string;
    public function seoTextCode(): string;
}
