<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Application\ShowOfferCollection;

use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferFilter;

class ShowOfferCollectionQuery
{
    public OfferFilter $filter;
    public ?string $citySlug;

    public function __construct()
    {
        $this->filter = new OfferFilter();
    }
}
