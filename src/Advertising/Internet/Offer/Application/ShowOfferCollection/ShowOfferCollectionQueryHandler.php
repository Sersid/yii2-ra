<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Application\ShowOfferCollection;

use NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferViewCollection;
use NikoM\Location\City\Domain\Entity\ICityFetcher;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

class ShowOfferCollectionQueryHandler
{
    private IOfferFetcher $offerFetcher;
    private ICityFetcher $cityFetcher;

    public function __construct(IOfferFetcher $offerFetcher, ICityFetcher $cityFetcher)
    {
        $this->offerFetcher = $offerFetcher;
        $this->cityFetcher = $cityFetcher;
    }

    public function handle(ShowOfferCollectionQuery $query): OfferViewCollection
    {
        $dtoCollection = $this->offerFetcher->findCollection($query->filter);
        $collection = OfferViewCollection::fromDtoCollection($dtoCollection);
        if ($query->citySlug !== null) {
            $cityDto = $this->cityFetcher->getBySlug(new CitySlug($query->citySlug));
            $collection->setCity($cityDto);
        }

        return $collection;
    }
}
