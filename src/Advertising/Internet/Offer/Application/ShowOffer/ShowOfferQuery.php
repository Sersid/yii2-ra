<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Application\ShowOffer;

class ShowOfferQuery
{
    public string $offerSlug;
    public ?string $citySlug = null;
}
