<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Application\ShowOffer;

use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDto;
use NikoM\Advertising\Internet\Offer\Domain\Entity\OfferSlug;
use NikoM\Advertising\Internet\Offer\Domain\Exception\OfferNotFoundException;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferFilter;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferView;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;
use NikoM\Location\City\Infrastructure\CityFetcher;

class ShowOfferQueryHandler
{
    private IOfferFetcher $offerFetcher;
    private CityFetcher $cityFetcher;

    public function __construct(IOfferFetcher $offerFetcher, CityFetcher $cityFetcher)
    {
        $this->offerFetcher = $offerFetcher;
        $this->cityFetcher = $cityFetcher;
    }

    public function handle(ShowOfferQuery $query): OfferView
    {
        $offerDto = $this->getOfferDto($query->offerSlug);
        $offerView = new OfferView($offerDto);
        if ($query->citySlug !== null) {
            $cityDto = $this->cityFetcher->getBySlug(new CitySlug($query->citySlug));
            $offerView->setCity($cityDto);
        }

        return $offerView;
    }

    private function getOfferDto(string $slug): OfferDto
    {
        $filter = new OfferFilter();
        $filter->activeOnly = true;
        $filter->slug = new OfferSlug($slug);

        $offerDto = $this->offerFetcher->findOne($filter);
        if ($offerDto === null) {
            throw new OfferNotFoundException();
        }

        return $offerDto;
    }
}
