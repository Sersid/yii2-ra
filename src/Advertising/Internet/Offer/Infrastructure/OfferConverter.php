<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Infrastructure;

use NikoM\Advertising\Internet\FAQ\Infrastructure\FaqConverter;
use NikoM\Advertising\Internet\MoreOffer\Infrastructure\MoreOfferConverter;
use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDto;
use NikoM\Advertising\Internet\SubOffer\Infrastructure\SubOfferConverter;
use NikoM\Advertising\Internet\Tariff\Domain\Dto\TariffDto;
use NikoM\Advertising\Internet\Tariff\Domain\Dto\TariffItemDto;
use NikoM\Advertising\Internet\Tariff\Domain\Dto\TariffItemDtoCollection;

class OfferConverter
{
    private SubOfferConverter $subOfferConverter;
    private MoreOfferConverter $moreOfferConverter;
    private FaqConverter $faqConverter;

    public function __construct(
        SubOfferConverter $subOfferConverter,
        MoreOfferConverter $moreOfferConverter,
        FaqConverter $faqConverter
    )
    {
        $this->subOfferConverter = $subOfferConverter;
        $this->moreOfferConverter = $moreOfferConverter;
        $this->faqConverter = $faqConverter;
    }

    public function fromArrayToDTO(array $data): OfferDto
    {
        $dto = new OfferDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('name', $data)) {
            $dto->name = $data['name'];
        }
        if (array_key_exists('slug', $data)) {
            $dto->slug = $data['slug'];
        }
        if (array_key_exists('icon', $data)) {
            $dto->icon = $data['icon'];
        }
        if (array_key_exists('intro_text', $data)) {
            $dto->introText = $data['intro_text'];
        }
        if (array_key_exists('page_title', $data)) {
            $dto->pageTitle = $data['page_title'];
        }
        if (array_key_exists('page_heading', $data)) {
            $dto->pageHeading = $data['page_heading'];
        }
        if (array_key_exists('page_description', $data)) {
            $dto->pageDescription = $data['page_description'];
        }
        if (array_key_exists('page_title_with_city', $data)) {
            $dto->pageTitleWithCity = $data['page_title_with_city'];
        }
        if (array_key_exists('page_heading_with_city', $data)) {
            $dto->pageHeadingWithCity = $data['page_heading_with_city'];
        }
        if (array_key_exists('page_description_with_city', $data)) {
            $dto->pageDescriptionWithCity = $data['page_description_with_city'];
        }
        if (array_key_exists('subOffers', $data)) {
            $dto->subOfferDTOCollection = $this->subOfferConverter->fromArrayToDTOCollection($data['subOffers']);
        }
        if (array_key_exists('moreOffers', $data)) {
            $dto->moreOfferDtoCollection = $this->moreOfferConverter->fromArrayToDTOCollection($data['moreOffers']);
        }
        if (array_key_exists('faqList', $data)) {
            $dto->faqDtoCollection = $this->faqConverter->fromArrayToDTOCollection($data['faqList']);
        }
        $tariffDto = new TariffDto();
        if (array_key_exists('tariff_title', $data)) {
            $tariffDto->title = $data['tariff_title'];
        }
        if (array_key_exists('tariff_description', $data)) {
            $tariffDto->description = $data['tariff_description'];
        }
        $tariffItems = [];
        foreach (['low' => 'low', 'mig' => 'mid', 'high' => 'high'] as $key => $position) {
            $tariffItem = new TariffItemDto();
            if (array_key_exists('tariff_' . $key . '_item_title', $data)) {
                $tariffItem->name = $data['tariff_' . $key . '_item_title'];
            }
            if (array_key_exists('tariff_' . $key . '_item_description', $data)) {
                $tariffItem->description = $data['tariff_' . $key . '_item_description'];
            }
            if (array_key_exists('tariff_' . $key . '_item_price', $data)) {
                $tariffItem->price = (float)$data['tariff_' . $key . '_item_price'];
            }
            if (!empty($tariffItem)) {
                $tariffItem->position = $position;
                $tariffItems[] = $tariffItem;
            }
        }
        if (!empty($tariffItems)) {
            $tariffDto->itemCollection = new TariffItemDtoCollection($tariffItems);
        }
        if (!empty($tariffDto)) {
            $dto->tariff = $tariffDto;
        }

        return $dto;
    }
}
