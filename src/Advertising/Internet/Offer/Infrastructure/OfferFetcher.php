<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Offer\Infrastructure;

use common\models\InternetOffer;
use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDto;
use NikoM\Advertising\Internet\Offer\Domain\Dto\OfferDtoCollection;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\Internet\Offer\Domain\ReadModel\OfferFilter;

class OfferFetcher implements IOfferFetcher
{
    private OfferConverter $offerConverter;

    public function __construct(OfferConverter $offerConverter)
    {
        $this->offerConverter = $offerConverter;
    }

    public function findCollection(OfferFilter $filter): OfferDtoCollection
    {
        $query = InternetOffer::find()
            ->select(['name', 'slug', 'icon', 'intro_text'])
            ->asArray();
        if ($filter->activeOnly) {
            $query->andWhere(['status' => 1]);
        }
        $offers = [];
        foreach ($query->all() as $data) {
            $offers[] = $this->offerConverter->fromArrayToDTO($data);
        }

        return new OfferDtoCollection($offers);
    }

    public function findOne(OfferFilter $filter): ?OfferDto
    {
        $query = InternetOffer::find()
            ->select([
                'id',
                'name',
                'slug',
                'icon',
                'intro_text',
                'page_title',
                'page_heading',
                'page_description',
                'page_title_with_city',
                'page_heading_with_city',
                'page_description_with_city',
                'tariff_title',
                'tariff_description',
                'tariff_low_item_title',
                'tariff_low_item_description',
                'tariff_low_item_price',
                'tariff_mig_item_title',
                'tariff_mig_item_description',
                'tariff_mig_item_price',
                'tariff_high_item_title',
                'tariff_high_item_description',
                'tariff_high_item_price',
            ])
            ->with([
                'subOffers' => function ($query) {
                    $query->select(['id', 'name', 'icon', 'description'])->where(['status' => 1]);
                },
                'moreOffers' => function ($query) {
                    $query->select(['id', 'name', 'icon'])->where(['status' => 1]);
                },
                'faqList' => function ($query) {
                    $query->select(['id', 'question', 'answer'])->where(['is_active' => 1]);
                },
            ])
            ->asArray();
        if (isset($filter->activeOnly)) {
            $query->andWhere(['status' => (int)$filter->activeOnly]);
        }
        if (isset($filter->slug)) {
            $query->andWhere(['slug' => $filter->slug->getValue()]);
        }
        $data = $query->one();

        if (!empty($data)) {
            return $this->offerConverter->fromArrayToDTO($data);
        } else {
            return null;
        }
    }

    public function getOne(OfferFilter $filter): OfferDto
    {
        // TODO: Implement getOne() method.
    }
}
