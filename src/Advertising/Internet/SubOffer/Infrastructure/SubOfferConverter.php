<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\SubOffer\Infrastructure;

use NikoM\Advertising\Internet\SubOffer\Domain\Dto\SubOfferDto;
use NikoM\Advertising\Internet\SubOffer\Domain\Dto\SubOfferDTOCollection;

class SubOfferConverter
{
    public function fromArrayToDTO(array $data): SubOfferDto
    {
        $dto = new SubOfferDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('name', $data)) {
            $dto->name = $data['name'];
        }
        if (array_key_exists('icon', $data)) {
            $dto->icon = $data['icon'];
        }
        if (array_key_exists('description', $data)) {
            $dto->description = $data['description'];
        }

        return $dto;
    }

    public function fromArrayToDTOCollection(array $items): SubOfferDTOCollection
    {
        $result = [];
        foreach ($items as $item) {
            $result[] = $this->fromArrayToDTO($item);
        }

        return new SubOfferDTOCollection($result);
    }
}
