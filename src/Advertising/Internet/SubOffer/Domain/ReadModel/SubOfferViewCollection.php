<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\SubOffer\Domain\ReadModel;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method SubOfferView[] all()
 */
class SubOfferViewCollection extends Collection
{
    protected static function type(): string
    {
        return SubOfferView::class;
    }
}
