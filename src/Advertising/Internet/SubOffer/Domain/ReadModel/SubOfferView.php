<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\SubOffer\Domain\ReadModel;

use NikoM\Advertising\Internet\Kernel\Domain\ReadModel\IconView;
use NikoM\Advertising\Internet\SubOffer\Domain\Dto\SubOfferDto;

class SubOfferView
{
    private SubOfferDto $dto;
    private IconView $icon;

    public function __construct(SubOfferDto $dto)
    {
        $this->dto = $dto;
        if ($dto->hasProperty('icon')) {
            $this->icon = new IconView($dto->icon);
        }
    }

    public function name(): string
    {
        return $this->dto->name;
    }

    public function icon(): IconView
    {
        return $this->icon;
    }

    public function description(): string
    {
        return $this->dto->description;
    }
}
