<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\SubOffer\Domain\ReadModel;

interface ISubOfferFetcher
{
    public function all(): SubOfferViewCollection;
}
