<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\SubOffer\Domain\Dto;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method SubOfferDto[] all()
 */
class SubOfferDTOCollection extends Collection
{
    protected static function type(): string
    {
        return SubOfferDto::class;
    }
}
