<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\SubOffer\Domain\Dto;

use NikoM\Kernel\Domain\Dto;

class SubOfferDto extends Dto
{
    public int $id;
    public string $name;
    public string $description;
    public string $icon;
}
