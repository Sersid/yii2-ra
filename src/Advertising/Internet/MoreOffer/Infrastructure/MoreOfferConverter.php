<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\MoreOffer\Infrastructure;

use NikoM\Advertising\Internet\MoreOffer\Domain\Dto\MoreOfferDto;
use NikoM\Advertising\Internet\MoreOffer\Domain\Dto\MoreOfferDtoCollection;

class MoreOfferConverter
{
    public function fromArrayToDTO(array $data): MoreOfferDto
    {
        $dto = new MoreOfferDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('name', $data)) {
            $dto->name = $data['name'];
        }
        if (array_key_exists('icon', $data)) {
            $dto->icon = $data['icon'];
        }

        return $dto;
    }

    public function fromArrayToDTOCollection(array $items): MoreOfferDtoCollection
    {
        $result = [];
        foreach ($items as $item) {
            $result[] = $this->fromArrayToDTO($item);
        }

        return new MoreOfferDtoCollection($result);
    }
}
