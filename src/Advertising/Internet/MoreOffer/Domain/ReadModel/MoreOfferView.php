<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\MoreOffer\Domain\ReadModel;

use NikoM\Advertising\Internet\Kernel\Domain\ReadModel\IconView;
use NikoM\Advertising\Internet\MoreOffer\Domain\Dto\MoreOfferDto;

class MoreOfferView
{
    private IconView $icon;
    private MoreOfferDto $dto;

    public function __construct(MoreOfferDto $dto)
    {
        $this->dto = $dto;
        if ($dto->hasProperty('icon')) {
            $this->icon = new IconView($dto->icon);
        }
    }

    public function name(): string
    {
        return $this->dto->name;
    }

    public function icon(): IconView
    {
        return $this->icon;
    }
}
