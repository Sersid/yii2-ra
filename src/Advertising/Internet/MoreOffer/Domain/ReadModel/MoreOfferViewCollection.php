<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\MoreOffer\Domain\ReadModel;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method MoreOfferView[] all()
 */
class MoreOfferViewCollection extends Collection
{
    protected static function type(): string
    {
        return MoreOfferView::class;
    }
}
