<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\MoreOffer\Domain\Dto;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

class MoreOfferDtoCollection extends Collection
{
    protected static function type(): string
    {
        return MoreOfferDto::class;
    }
}
