<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\MoreOffer\Domain\Dto;

use NikoM\Kernel\Domain\Dto;

class MoreOfferDto extends Dto
{
    public int $id;
    public string $name;
    public string $icon;
}
