<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\Kernel\Domain\ReadModel;

class IconView
{
    private string $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function src(): string
    {
        return '/img/internet-reklama/' . $this->fileName;
    }
}
