<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Domain\ReadModel;

use NikoM\Advertising\Internet\OurWork\Domain\Dto\OurWorkDto;
use NikoM\Advertising\Kernel\ReadModel\ImageView;

class OurWorkView
{
    private OurWorkDto $dto;

    public function __construct(OurWorkDto $dto)
    {
        $this->dto = $dto;
    }

    public function name(): string
    {
        return $this->dto->name;
    }

    public function url(): string
    {
        return '/cases/' . $this->dto->slug;
    }

    public function image(): ImageView
    {
        return new ImageView($this->dto->image);
    }
}
