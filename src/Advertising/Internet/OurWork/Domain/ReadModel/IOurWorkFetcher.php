<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Domain\ReadModel;

use NikoM\Advertising\Internet\OurWork\Domain\Dto\OurWorkDtoCollection;

interface IOurWorkFetcher
{
    public function findCollectionByCategories(array $categories): OurWorkDtoCollection;
}
