<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Domain\ReadModel;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method OurWorkView[] all()
 */
class OurWorkViewCollection extends Collection
{
    protected static function type(): string
    {
        return OurWorkView::class;
    }
}
