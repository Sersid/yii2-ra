<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Domain\Dto;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

class OurWorkDtoCollection extends Collection
{
    protected static function type(): string
    {
        return OurWorkDto::class;
    }
}
