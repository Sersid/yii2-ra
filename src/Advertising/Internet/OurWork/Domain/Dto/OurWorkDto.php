<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Domain\Dto;

use NikoM\Advertising\Kernel\DTO\ImageDto;
use NikoM\Kernel\Domain\Dto;

class OurWorkDto extends Dto
{
    public string $name;
    public string $slug;
    public string $description;
    public ImageDto $image;
}
