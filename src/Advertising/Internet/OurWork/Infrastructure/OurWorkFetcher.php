<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Infrastructure;

use common\models\AdvCase;
use NikoM\Advertising\Internet\OurWork\Domain\Dto\OurWorkDtoCollection;
use NikoM\Advertising\Internet\OurWork\Domain\ReadModel\IOurWorkFetcher;

class OurWorkFetcher implements IOurWorkFetcher
{
    private OurWorkConverter $ourWorkConverter;

    public function __construct(OurWorkConverter $ourWorkConverter)
    {
        $this->ourWorkConverter = $ourWorkConverter;
    }

    public function findCollectionByCategories(array $categories): OurWorkDtoCollection
    {
        $items = AdvCase::find()
            ->select(['slug', 'name', 'short_desc', 'image_id'])
            ->with('image')
            ->where(['category' => $categories, 'status' => AdvCase::STATUS_ACTIVE])
            ->orderBy(['id' => SORT_DESC])
            ->limit(4)
            ->asArray()
            ->all();

        return $this->ourWorkConverter->fromArrayToDTOCollection($items);
    }
}
