<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Infrastructure;

use NikoM\Advertising\Internet\OurWork\Domain\Dto\OurWorkDto;
use NikoM\Advertising\Internet\OurWork\Domain\Dto\OurWorkDtoCollection;
use NikoM\Kernel\Infrastructure\ImageConverter;

class OurWorkConverter
{
    private ImageConverter $imageConverter;

    public function __construct(ImageConverter $imageConverter)
    {
        $this->imageConverter = $imageConverter;
    }

    public function fromArrayToDto(array $data): OurWorkDto
    {
        $dto = new OurWorkDto();
        if (array_key_exists('name', $data)) {
            $dto->name = $data['name'];
        }
        if (array_key_exists('slug', $data)) {
            $dto->slug = $data['slug'];
        }
        if (array_key_exists('image', $data)) {
            $dto->image = $this->imageConverter->fromArrayToDTO($data['image']);
        }

        return $dto;
    }

    public function fromArrayToDTOCollection(array $items): OurWorkDtoCollection
    {
        $result = [];
        foreach ($items as $item) {
            $result[] = $this->fromArrayToDto($item);
        }

        return new OurWorkDtoCollection($result);
    }
}
