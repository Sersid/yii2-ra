<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Application\Query\FindOutWorkCollection;

use NikoM\Advertising\Internet\OurWork\Domain\ReadModel\IOurWorkFetcher;
use NikoM\Advertising\Internet\OurWork\Domain\ReadModel\OurWorkViewCollection;

class FindOutWorkCollectionQueryHandler
{
    private IOurWorkFetcher $ourWorkFetcher;

    public function __construct(IOurWorkFetcher $ourWorkFetcher)
    {
        $this->ourWorkFetcher = $ourWorkFetcher;
    }

    public function handle(FindOutWorkCollectionQuery $query): OurWorkViewCollection
    {
        $dtoCollection = $this->ourWorkFetcher->findCollectionByCategories($query->categories);

        return OurWorkViewCollection::fromDtoCollection($dtoCollection);
    }
}
