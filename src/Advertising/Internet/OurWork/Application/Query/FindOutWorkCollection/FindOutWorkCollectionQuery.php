<?php
declare(strict_types=1);

namespace NikoM\Advertising\Internet\OurWork\Application\Query\FindOutWorkCollection;

class FindOutWorkCollectionQuery
{
    public array $categories;
}
