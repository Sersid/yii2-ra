<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Order\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderType;

class OrderType implements IOrderType
{
    public function getName(): string
    {
        return 'Реклама на радио';
    }

    public function getOfferName(): string
    {
        return 'Радиостанция';
    }
}
