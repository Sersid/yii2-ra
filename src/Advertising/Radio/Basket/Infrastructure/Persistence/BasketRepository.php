<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Basket\Infrastructure\Persistence;

use common\models\UserRadioAd;
use common\models\UserRadioAdRadio;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketId;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Radio\Basket\Domain\Entity\Basket;
use NikoM\Kernel\Domain\Entity\Hydrator;

class BasketRepository implements IBasketRepository
{
    private static array $basket = [];
    private BasketConverter $basketConverter;
    private Hydrator $hydrator;

    public function __construct(BasketConverter $basketConverter, Hydrator $hydrator)
    {
        $this->basketConverter = $basketConverter;
        $this->hydrator = $hydrator;
    }

    public function getByBuyerId(BuyerId $buyerId): IBasket
    {
        $data = UserRadioAd::find()
            ->select(['id', 'type', 'order_id', 'guest_id', 'user_radio_ad.status'])
            ->where(['guest_id' => $buyerId->getValue(), 'user_radio_ad.status' => UserRadioAd::STATUS_NEW])
            ->with([
                'userRadioAdRadios' => function ($query) {
                    $query->select([
                        'user_radio_ad_id',
                        'radio_ad_id',
                        'duration',
                        'views',
                        'days',
                        'price',
                        'price_without_discount',
                    ]);
                },
                'userRadioAdRadios.radioAd' => function ($query) {
                    $query->select(['id', 'city_id', 'radio_id', 'price_ad_sec', 'discount']);
                },
                'userRadioAdRadios.radioAd.city' => function ($query) {
                    $query->select(['id', 'name']);
                },
                'userRadioAdRadios.radioAd.radio' => function ($query) {
                    $query->select(['id', 'name']);
                },
            ])
            ->orderBy('user_radio_ad.created_at')
            ->asArray()
            ->one();

        if (empty($data)) {
            throw new BasketNotFoundException();
        }

        $dto = $this->basketConverter->fromArrayToDTO($data);
        $basket = Basket::fromDTO($dto);
        self::$basket = $basket->toPrimitives();

        return $basket;
    }

    public function add(IBasket $basket): void
    {
        $model = new UserRadioAd();
        $model->guest_id = $basket->getBuyerId()->getValue();
        $model->type = $basket->getAdvType()->getValue();
        $model->status = $basket->getStatus()->getValue();
        $model->save(false);

        $this->hydrator->setPropertyValue($basket, 'id', new BasketId($model->id));
        self::$basket = $basket->toPrimitives();
    }

    public function save(IBasket $basket): void
    {
        $data = [
            'prev' => self::$basket,
            'current' => $basket->toPrimitives(),
        ];

        if ($data['prev'] === $data['current']) {
            return;
        }

        $id = $basket->getId()->getValue();

        $update = [];
        if ($data['current']['advType'] !== $data['prev']['advType']) {
            $update['type'] = $data['current']['advType'];
        }
        if ($data['current']['status'] !== $data['prev']['status']) {
            $update['status'] = $data['current']['status'];
        }
        if (!empty($update)) {
            UserRadioAd::updateAll($update, ['id' => $id]);
        }

        if ($data['current']['items'] === $data['prev']['items']) {
            return;
        }

        foreach ($data['current']['items'] as $key => $item) {
            if (isset($data['prev']['items'][$key])) {
                $prev = $data['prev']['items'][$key];
                if ($prev !== $item) {
                    $update = [];
                    if ($item['viewsPerDay'] !== $prev['viewsPerDay']) {
                        $update['views'] = $item['viewsPerDay'];
                    }
                    if ($item['duration'] !== $prev['duration']) {
                        $update['duration'] = $item['duration'];
                    }
                    if ($item['days'] !== $prev['days']) {
                        $update['days'] = $item['days'];
                    }
                    if ($item['priceWithoutDiscount'] !== $prev['priceWithoutDiscount']) {
                        $update['price_without_discount'] = $item['priceWithoutDiscount'];
                    }
                    if ($item['price'] !== $prev['price']) {
                        $update['price'] = $item['price'];
                    }
                    if (!empty($update)) {
                        UserRadioAdRadio::updateAll($update, ['user_radio_ad_id' => $id, 'radio_ad_id' => $key]);
                    }
                }
                unset($data['current']['items'][$key], $data['prev']['items'][$key]);
            }
        }

        foreach ($data['current']['items'] as $item) {
            $model = new UserRadioAdRadio();
            $model->user_radio_ad_id = $id;
            $model->radio_ad_id = $item['offerId'];
            $model->days = $item['days'];
            $model->duration = $item['duration'];
            $model->views = $item['viewsPerDay'];
            $model->price = $item['price'];
            $model->price_without_discount = $item['priceWithoutDiscount'];
            $model->save(false);
        }

        foreach ($data['prev']['items'] as $item) {
            UserRadioAdRadio::deleteAll(['user_radio_ad_id' => $id, 'radio_ad_id' => $item['offerId']]);
        }
    }
}
