<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Basket\Infrastructure\Persistence;

use NikoM\Advertising\Radio\Basket\Domain\DTO\BasketItemDTO;
use NikoM\Advertising\Radio\Offer\Infrastructure\Persistence\OfferConverter;

class BasketItemConverter
{
    private OfferConverter $offerConverter;

    public function __construct(OfferConverter $offerConverter)
    {
        $this->offerConverter = $offerConverter;
    }

    public function fromArrayToDTO(array $data): BasketItemDTO
    {
        $dto = new BasketItemDTO();
        $dto->offer = $this->offerConverter->fromArrayToDTO($data['radioAd']);
        $dto->duration = empty($data['duration']) ? null : (int)$data['duration'];
        $dto->viewsPerDay = empty($data['views']) ? null : (int)$data['views'];
        $dto->days = empty($data['days']) ? null : (int)$data['days'];
        $dto->price = empty($data['price']) ? null : (float)$data['price'];
        if (empty($data['price_without_discount'])) {
            $dto->priceWithoutDiscount = null;
        } else {
            $dto->priceWithoutDiscount = (float)$data['price_without_discount'];
        }

        return $dto;
    }
}
