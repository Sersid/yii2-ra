<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Basket\Domain\DTO;

use NikoM\Advertising\Radio\Offer\Domain\DTO\OfferDto;

class BasketItemDTO
{
    public OfferDto $offer;
    public ?int $duration;
    public ?int $viewsPerDay;
    public ?int $days;
    public ?float $price;
    public ?float $priceWithoutDiscount;
}
