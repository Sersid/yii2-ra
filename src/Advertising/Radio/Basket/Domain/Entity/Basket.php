<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Basket\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\AbstractBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketAdvTypeCollection;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketId;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketItemCollection;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketStatus;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketTotal;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\OrderId;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Radio\Basket\Domain\DTO\BasketDTO;
use NikoM\Advertising\Radio\Basket\Domain\Entity\AdvTypes\BasketAdvTypeAds;
use NikoM\Advertising\Radio\Basket\Domain\Entity\AdvTypes\BasketAdvTypeAdvertisement;
use NikoM\Advertising\Radio\Basket\Domain\Entity\AdvTypes\BasketAdvTypeSponsorship;

class Basket extends AbstractBasket
{
    public static function fromDTO(BasketDTO $dto): self
    {
        $self = new static();
        $self->id = new BasketId($dto->id);
        $self->buyerId = new BuyerId($dto->buyerId);
        $self->advType = $self->getAdvTypeCollection()->get($dto->advType);
        $self->status = new BasketStatus($dto->status);
        $self->orderId = new OrderId($dto->orderId);
        $items = [];
        foreach ($dto->items as $item) {
            $items[] = BasketItem::fromDTO($item);
        }
        $self->itemCollection = new BasketItemCollection($items);
        $self->total = new BasketTotal($self->itemCollection);

        return $self;
    }

    public function getAdvTypeCollection(): BasketAdvTypeCollection
    {
        return new BasketAdvTypeCollection([
            new BasketAdvTypeAds(),
            new BasketAdvTypeSponsorship(),
            new BasketAdvTypeAdvertisement(),
        ]);
    }
}
