<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Basket\Domain\Entity\AdvTypes;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketAdvType;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketAdvType;

class BasketAdvTypeSponsorship extends BasketAdvType implements IBasketAdvType
{
    public function getName(): string
    {
        return 'Спонсорство';
    }

    public function getValue(): int
    {
        return 2;
    }

    public function getText(): string
    {
        return 'Этот вид рекламы интересен своей не стандартностью. Это зачастую целый сценарий с '
               . 'упоминанием до определенной программы (прогноз погоды, новости...) прокатом ролика во '
               . 'время и персонифицированным текстовым сообщением от диктора в конце передачи. Заполните '
               . 'нашу форму и предложим вас различные варианты спонсорских пакетов.';
    }

    public function mustCalculate(): bool
    {
        return false;
    }
}
