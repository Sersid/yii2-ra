<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Basket\Domain\Entity\AdvTypes;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketAdvType;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketAdvType;

class BasketAdvTypeAdvertisement extends BasketAdvType implements IBasketAdvType
{
    public function getName(): string
    {
        return 'Рекламное объявление';
    }

    public function getValue(): int
    {
        return 3;
    }

    public function getText(): string
    {
        return 'Это родственник "текстового объявления" на телевидении только на радио он поизносится '
               . 'голосом. Довольно популярный и не дорогой вид рекламы.';
    }

    public function mustCalculate(): bool
    {
        return false;
    }
}
