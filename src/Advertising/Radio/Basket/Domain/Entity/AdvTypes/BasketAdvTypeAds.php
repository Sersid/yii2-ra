<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Basket\Domain\Entity\AdvTypes;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketAdvType;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketAdvType;

class BasketAdvTypeAds extends BasketAdvType implements IBasketAdvType
{
    public function getName(): string
    {
        return 'Рекламный ролик';
    }

    public function getValue(): int
    {
        return 1;
    }

    public function getText(): string
    {
        return 'Размещение рекламных роликов в эфире радиостанции. Реклама на радио специфична и при '
               . 'грамотной постановке крайне эффективна.';
    }

    public function mustCalculate(): bool
    {
        return true;
    }
}
