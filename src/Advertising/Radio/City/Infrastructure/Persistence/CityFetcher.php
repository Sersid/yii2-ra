<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\City\Infrastructure\Persistence;

use common\models\RadioAd;
use NikoM\Advertising\AbstractMedia\City\Domain\ICityFetcher;
use NikoM\Advertising\Kernel\Infrastructure\Persistence\AbstractCityFetcher;
use yii\db\ActiveQuery;

class CityFetcher extends AbstractCityFetcher implements ICityFetcher
{
    protected function getQuery(): ActiveQuery
    {
        return RadioAd::find()
            ->select('city_id')
            ->groupBy('city_id')
            ->where(['status' => RadioAd::STATUS_ACTIVE]);
    }
}
