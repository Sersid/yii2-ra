<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Offer\Infrastructure\Persistence;

use common\models\Radio;
use common\models\RadioAd;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\Filter;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\OfferViewCollection;
use NikoM\Advertising\Radio\Offer\Domain\ReadModel\OfferView;

class OfferFetcher implements IOfferFetcher
{
    private OfferConverter $offerConverter;

    public function __construct(OfferConverter $offerConverter)
    {
        $this->offerConverter = $offerConverter;
    }

    public function all(Filter $filter, int $offset): OfferViewCollection
    {
        $result = [];
        $offers = $this->getOffers($filter, $offset);
        foreach ($offers['offers'] as $offer) {
            $dto = $this->offerConverter->fromArrayToDTO($offer);
            $result[] = new OfferView($dto);
        }
        return new OfferViewCollection($result, $offers['total'], $offers['offset']);
    }

    private function getOffers(Filter $filter, int $offset): array
    {
        $query = RadioAd::find()
            ->select([
                'radio_ad.id',
                'price_ad_sec',
                'price_ad_show',
                'city_id',
                'radio_id',
            ])
            ->joinWith([
                'radio' => function ($query) {
                    $query->select(['id', 'name', 'image_id', 'slug']);
                },
                'radio.image',
            ])
            ->with([
                'city' => function ($query) {
                    $query->select(['id', 'name', 'slug']);
                },
            ])
            ->orderBy(['radio_ad.sort' => SORT_ASC, 'radio.sort' => SORT_ASC])
            ->where(['radio_ad.status' => RadioAd::STATUS_ACTIVE, 'radio.status' => Radio::STATUS_ACTIVE]);

        if (!is_null($filter->cityId)) {
            $query->andWhere(['city_id' => $filter->cityId]);
        }
        if (!is_null($filter->offerSlug)) {
            $query->andWhere(['radio.slug' => $filter->offerSlug]);
        }
        $query->offset($offset);
        $query->limit(self::LIMIT);
        $total = (int)$query->count();
        $offset = $offset + self::LIMIT;
        $offset = $offset > $total ? null : $offset;

        return [
            'total' => $total,
            'offers' => $query->asArray()->all(),
            'offset' => $offset,
        ];
    }
}
