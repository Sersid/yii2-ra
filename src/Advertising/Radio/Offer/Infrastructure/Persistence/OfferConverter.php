<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Offer\Infrastructure\Persistence;

use NikoM\Advertising\Radio\Station\Infrastructure\Persistence\StationConverter;
use NikoM\Advertising\Radio\Offer\Domain\DTO\OfferDto;
use NikoM\Location\City\Infrastructure\CityConverter;

class OfferConverter
{
    private StationConverter $stationConverter;
    private CityConverter $cityConverter;

    public function __construct(StationConverter $stationConverter, CityConverter $cityConverter)
    {
        $this->cityConverter = $cityConverter;
        $this->stationConverter = $stationConverter;
    }

    public function fromArrayToDTO(array $data): OfferDto
    {
        $dto = new OfferDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('price_ad_sec', $data)) {
            $dto->pricePerSec = (float)$data['price_ad_sec'];
            $dto->pricePerSec = empty($dto->pricePerSec) ? null : $dto->pricePerSec;
        }
        if (array_key_exists('discount', $data)) {
            $dto->discount = (float)$data['discount'];
            $dto->discount = empty($dto->discount) ? null : $dto->discount;
        }
        if (array_key_exists('city', $data)) {
            $dto->city = $this->cityConverter->fromArrayToDTO($data['city']);
        }
        if (array_key_exists('radio', $data)) {
            $dto->station = $this->stationConverter->fromArrayToDTO($data['radio']);
        }

        return $dto;
    }
}
