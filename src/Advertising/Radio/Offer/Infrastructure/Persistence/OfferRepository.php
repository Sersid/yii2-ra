<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Offer\Infrastructure\Persistence;

use common\models\RadioAd;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Exception\OfferNotFoundException;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferRepository;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;
use NikoM\Advertising\Radio\Offer\Domain\ReadModel\OfferView;

class OfferRepository implements IOfferRepository
{
    private OfferConverter $offerConverter;

    public function __construct(OfferConverter $offerConverter)
    {
        $this->offerConverter = $offerConverter;
    }

    public function getById(OfferId $offerId): IOfferView
    {
        $data = RadioAd::find()
            ->select([
                'radio_ad.id',
                'price_ad_sec',
                'discount',
                'city_id',
                'radio_id',
            ])
            ->joinWith([
                'radio' => function ($query) {
                    $query->select(['id', 'name']);
                },
            ])
            ->with([
                'city' => function ($query) {
                    $query->select(['id', 'name']);
                },
            ])
            ->where(['radio_ad.id' => $offerId->getValue(), 'radio_ad.status' => RadioAd::STATUS_ACTIVE])
            ->asArray()
            ->one();
        if (empty($data)) {
            throw new OfferNotFoundException();
        }

        $dto = $this->offerConverter->fromArrayToDTO($data);

        return new OfferView($dto);
    }
}
