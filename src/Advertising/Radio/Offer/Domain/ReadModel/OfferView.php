<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Offer\Domain\ReadModel;

use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;
use NikoM\Advertising\Kernel\ReadModel\ImageView;
use NikoM\Advertising\Radio\Offer\Domain\DTO\OfferDto;
use NikoM\Advertising\Radio\Station\Domain\ReadModel\StationView;
use NikoM\Location\City\Domain\ReadModel\CityView;

class OfferView implements IOfferView
{
    private int $id;
    private CityView $city;
    private StationView $station;
    private ?float $pricePerSec;
    private ?float $discount;

    public function __construct(OfferDto $dto)
    {
        if ($dto->hasProperty('id')) {
            $this->id = $dto->id;
        }
        if ($dto->hasProperty('city')) {
            $this->city = new CityView($dto->city);
        }
        if ($dto->hasProperty('station')) {
            $this->station = new StationView($dto->station);
        }
        if ($dto->hasProperty('pricePerSec')) {
            $this->pricePerSec = $dto->pricePerSec;
        }
        if ($dto->hasProperty('discount')) {
            $this->discount = $dto->discount;
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->city->name() . ' - ' . $this->station->name();
    }

    public function pricePerSec(): ?float
    {
        return $this->pricePerSec;
    }

    public function hasPricePerSec(): bool
    {
        return !is_null($this->pricePerSec);
    }

    public function discount(): ?float
    {
        return $this->discount;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name(),
            'hasPricePerSec' => $this->hasPricePerSec(),
        ];
    }

    public function logo(): ImageView
    {
        return $this->station->logo();
    }

    public function url(): string
    {
        return '/radio/' . $this->city->slug() . '/' . $this->station->slug();
    }
}
