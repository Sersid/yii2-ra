<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Offer\Domain\DTO;

use NikoM\Advertising\Radio\Station\Domain\DTO\StationDto;
use NikoM\Kernel\Domain\Dto;
use NikoM\Location\City\Domain\DTO\CityDto;

class OfferDto extends Dto
{
    public int $id;
    public CityDto $city;
    public StationDto $station;
    public ?float $pricePerSec;
    public ?float $discount;
}
