<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Station\Domain\DTO;

use NikoM\Advertising\Kernel\DTO\ImageDto;
use NikoM\Kernel\Domain\Dto;

class StationDto extends Dto
{
    public int $id;
    public string $name;
    public string $slug;
    public ImageDto $logo;
}
