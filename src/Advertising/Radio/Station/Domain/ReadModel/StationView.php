<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Station\Domain\ReadModel;

use NikoM\Advertising\Kernel\ReadModel\ImageView;
use NikoM\Advertising\Radio\Station\Domain\DTO\StationDto;

class StationView
{
    private int $id;
    private string $name;
    private string $slug;
    private ImageView $logo;

    public function __construct(StationDto $dto)
    {
        if ($dto->hasProperty('id')) {
            $this->id = $dto->id;
        }
        if ($dto->hasProperty('name')) {
            $this->name = $dto->name;
        }
        if ($dto->hasProperty('slug')) {
            $this->slug = $dto->slug;
        }
        if ($dto->hasProperty('logo')) {
            $this->logo = new ImageView($dto->logo);
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function slug(): string
    {
        return $this->slug;
    }

    public function logo(): ImageView
    {
        return $this->logo;
    }
}
