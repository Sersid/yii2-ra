<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Station\Domain\Entity;

use NikoM\Advertising\Radio\Station\Domain\DTO\StationDto;

class Station
{
    private StationId $id;
    private StationName $name;

    private function __construct()
    {
    }

    public static function fromDTO(StationDto $dto): self
    {
        $self = new self();
        $self->id = new StationId($dto->id);
        $self->name = new StationName($dto->name);

        return $self;
    }

    public function getId(): StationId
    {
        return $this->id;
    }

    public function getName(): StationName
    {
        return $this->name;
    }
}
