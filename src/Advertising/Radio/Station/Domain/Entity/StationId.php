<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Station\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;

class StationId extends IntValueObject
{
}
