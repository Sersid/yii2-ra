<?php
declare(strict_types=1);

namespace NikoM\Advertising\Radio\Station\Infrastructure\Persistence;

use NikoM\Advertising\Radio\Station\Domain\DTO\StationDto;
use NikoM\Kernel\Infrastructure\ImageConverter;

class StationConverter
{
    private ImageConverter $imageConverter;

    public function __construct(ImageConverter $imageConverter)
    {
        $this->imageConverter = $imageConverter;
    }

    public function fromArrayToDTO(array $data): StationDto
    {
        $dto = new StationDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('name', $data)) {
            $dto->name = $data['name'];
        }
        if (array_key_exists('slug', $data)) {
            $dto->slug = $data['slug'];
        }
        if (array_key_exists('image', $data)) {
            $dto->logo = $this->imageConverter->fromArrayToDTO($data['image']);
        }

        return $dto;
    }
}
