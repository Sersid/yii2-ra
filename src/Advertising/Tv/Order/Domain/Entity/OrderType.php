<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Order\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderType;

class OrderType implements IOrderType
{
    public function getName(): string
    {
        return 'Реклама на тв';
    }

    public function getOfferName(): string
    {
        return 'Телеканал';
    }
}
