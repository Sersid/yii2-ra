<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\ReadModel;

class ChannelColorView
{
    private ?string $heading;
    private ?string $line;
    private ?string $potentialAudience;

    public function __construct(?string $heading, ?string $line, ?string $potentialAudience)
    {
        $this->heading = empty($heading) ? null : $heading;
        $this->line = empty($line) ? null : $line;
        $this->potentialAudience = empty($potentialAudience) ? null : $potentialAudience;
    }

    public function hasHeading(): bool
    {
        return !is_null($this->heading);
    }

    public function heading(): ?string
    {
        return $this->heading;
    }

    public function line(): ?string
    {
        return $this->line;
    }
    public function potentialAudience(): ?string
    {
        return $this->potentialAudience;
    }
}
