<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\ReadModel;

use NikoM\Advertising\Kernel\ReadModel\ImageView;
use NikoM\Advertising\Tv\Channel\Domain\DTO\ChannelDto;

class ChannelView
{
    private int $id;
    private string $name;
    private string $slug;
    private ImageView $logo;
    private ?ImageView $bigLogo;
    private ?ImageView $bigBanner;
    private ChannelColorView $color;

    public function __construct(ChannelDto $dto)
    {
        if ($dto->hasProperty('id')) {
            $this->id = $dto->id;
        }
        if ($dto->hasProperty('name')) {
            $this->name = $dto->name;
        }
        if ($dto->hasProperty('slug')) {
            $this->slug = $dto->slug;
        }
        if ($dto->hasProperty('logo')) {
            $this->logo = new ImageView($dto->logo);
        }
        if ($dto->hasProperty('bigLogo')) {
            $this->bigLogo = is_null($dto->bigLogo) ? null : new ImageView($dto->bigLogo);
        }
        if ($dto->hasProperty('bigBanner')) {
            $this->bigBanner = is_null($dto->bigBanner) ? null : new ImageView($dto->bigBanner);
        }
        if ($dto->hasAnyProperty(...['colorHeader', 'colorLine', 'colorPotentialAudience'])) {
            $this->color = new ChannelColorView($dto->colorHeader, $dto->colorLine, $dto->colorPotentialAudience);
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function slug(): string
    {
        return $this->slug;
    }

    public function logo(): ImageView
    {
        return $this->logo;
    }

    public function hasBigLogo(): bool
    {
        return !is_null($this->bigLogo);
    }

    public function bigLogo(): ?ImageView
    {
        return $this->bigLogo;
    }

    public function hasBigBanner(): bool
    {
        return !is_null($this->bigBanner);
    }

    public function bigBanner(): ?ImageView
    {
        return $this->bigBanner;
    }

    public function color(): ChannelColorView
    {
        return $this->color;
    }
}
