<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\NotFoundException;

class ChannelNotFoundException extends NotFoundException
{
    public function message(): string
    {
        return 'Channel not found';
    }
}
