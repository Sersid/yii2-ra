<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\DTO;

use NikoM\Advertising\Kernel\DTO\ImageDto;
use NikoM\Kernel\Domain\Dto;

class ChannelDto extends Dto
{
    public int $id;
    public string $name;
    public string $slug;
    public ImageDto $logo;
    public ?ImageDto $bigLogo;
    public ?ImageDto $bigBanner;
    public bool $status;
    public int $sort;
    public ?string $colorHeader;
    public ?string $colorLine;
    public ?string $colorPotentialAudience;
}
