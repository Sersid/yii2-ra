<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdValueObject;

class ChannelId extends IdValueObject
{
}
