<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\Entity;

use NikoM\Advertising\Tv\Channel\Domain\Exception\ChannelNotFoundException;

interface IChannelRepository
{
    public function add(Channel $channel): void;

    /**
     * @param ChannelId $channelId
     *
     * @return Channel
     * @throws ChannelNotFoundException
     */
    public function getById(ChannelId $channelId): Channel;

    public function save(Channel $channel): void;

    public function delete(ChannelId $channelId): void;
}
