<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\Entity;

use NikoM\Advertising\Tv\Channel\Domain\DTO\ChannelDto;

class Channel
{
    private ChannelId $id;
    private ChannelName $name;

    private function __construct()
    {
    }

    public static function fromDTO(ChannelDto $dto): self
    {
        $self = new self();
        $self->id = new ChannelId($dto->id);
        $self->name = new ChannelName($dto->name);

        return $self;
    }

    public function getId(): ChannelId
    {
        return $this->id;
    }

    public function getName(): ChannelName
    {
        return $this->name;
    }
}
