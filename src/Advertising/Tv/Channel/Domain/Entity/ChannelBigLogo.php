<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntNullableValueObject;

class ChannelBigLogo extends IntNullableValueObject
{
}
