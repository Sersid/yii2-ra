<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Bool\BoolValueObject;

class ChannelStatus extends BoolValueObject
{
}
