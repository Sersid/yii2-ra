<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Application\Command\AddChannel;

use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelBigBanner;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelBigLogo;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelLogo;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelName;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelSlug;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelSort;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelStatus;
use NikoM\Advertising\Tv\Channel\Domain\Entity\IChannelRepository;
use NikoM\Kernel\Domain\Aggregate\EventDispatcher;

class AddChannelCommandHandler
{
    private IChannelRepository $channelRepository;
    private EventDispatcher $eventDispatcher;

    public function __construct(IChannelRepository $channelRepository, EventDispatcher $eventDispatcher)
    {
        $this->channelRepository = $channelRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function handle(AddChannelCommand $command): void
    {
        $name = new ChannelName($command->name);
        $slug = is_null($command->slug) ? ChannelSlug::fromString($command->name) : new ChannelSlug($command->slug);
        $logo = new ChannelLogo($command->logo);
        $bigLogo = new ChannelBigLogo($command->bigLogo);
        $bigBanner = new ChannelBigBanner($command->bigBanner);
        $status = new ChannelStatus($command->status);
        $sort = new ChannelSort($command->sort);

        // $channel = new Channel($name, $slug, $logo, $bigLogo, $bigBanner, $status, $sort);

        // $this->channelRepository->add($channel);
        // $this->eventDispatcher->dispatch($channel->releaseEvents());
    }
}
