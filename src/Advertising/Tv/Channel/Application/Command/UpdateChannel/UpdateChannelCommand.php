<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Application\Command\UpdateChannel;

class UpdateChannelCommand
{
    public int $id;
    public string $name;
    public ?string $slug;
    public ?int $logo;
    public ?int $bigLogo;
    public ?int $bigBanner;
    public bool $status;
    public ?int $sort;
}
