<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Application\Command\UpdateChannel;

use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelBigBanner;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelBigLogo;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelId;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelLogo;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelName;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelSlug;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelSort;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelStatus;
use NikoM\Advertising\Tv\Channel\Domain\Entity\IChannelRepository;
use NikoM\Kernel\Domain\Aggregate\EventDispatcher;

class UpdateChannelCommandHandler
{
    private IChannelRepository $channelRepository;
    private EventDispatcher $eventDispatcher;

    public function __construct(IChannelRepository $channelRepository, EventDispatcher $eventDispatcher)
    {
        $this->channelRepository = $channelRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function handle(UpdateChannelCommand $command): void
    {
        $channel = $this->channelRepository->getById(new ChannelId($command->id));
        $channel->rename(new ChannelName($command->name));
        $channel->changeSlug(new ChannelSlug($command->slug));
        $channel->changeLogo(new ChannelLogo($command->logo));
        $channel->changeBigLogo(new ChannelBigLogo($command->bigLogo));
        $channel->changeBigBanner(new ChannelBigBanner($command->bigBanner));
        $channel->changeStatus(new ChannelStatus($command->status));
        $channel->changeSort(new ChannelSort($command->sort));

        $this->channelRepository->save($channel);
        $this->eventDispatcher->dispatch($channel->releaseEvents());
    }
}
