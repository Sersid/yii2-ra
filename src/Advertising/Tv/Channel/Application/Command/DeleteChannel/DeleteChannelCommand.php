<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Application\Command\DeleteChannel;

class DeleteChannelCommand
{
    public int $id;
}
