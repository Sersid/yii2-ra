<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Application\Command\DeleteChannel;

use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelId;
use NikoM\Advertising\Tv\Channel\Domain\Entity\IChannelRepository;

class DeleteChannelCommandHandler
{
    private IChannelRepository $channelRepository;

    public function __construct(IChannelRepository $channelRepository)
    {
        $this->channelRepository = $channelRepository;
    }

    public function handle(DeleteChannelCommand $command): void
    {
        $id = new ChannelId($command->id);
        $this->channelRepository->delete($id);
    }
}
