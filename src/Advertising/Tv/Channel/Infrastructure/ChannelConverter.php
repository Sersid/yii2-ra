<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Channel\Infrastructure;

use NikoM\Advertising\Tv\Channel\Domain\DTO\ChannelDto;
use NikoM\Kernel\Infrastructure\ImageConverter;

class ChannelConverter
{
    private ImageConverter $imageConverter;

    public function __construct(ImageConverter $imageConverter)
    {
        $this->imageConverter = $imageConverter;
    }

    public function fromArrayToDTO(array $data): ChannelDto
    {
        $dto = new ChannelDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('name', $data)) {
            $dto->name = $data['name'];
        }
        if (array_key_exists('slug', $data)) {
            $dto->slug = $data['slug'];
        }
        if (array_key_exists('image', $data)) {
            $dto->logo = $this->imageConverter->fromArrayToDTO($data['image']);
        }
        if (array_key_exists('bigLogo', $data)) {
            $dto->bigLogo = is_null($data['bigLogo']) ? null : $this->imageConverter->fromArrayToDTO($data['bigLogo']);
        }
        if (array_key_exists('bigBanner', $data)) {
            $dto->bigBanner = is_null($data['bigBanner']) ? null : $this->imageConverter->fromArrayToDTO($data['bigBanner']);
        }
        if (array_key_exists('color_heading', $data)) {
            $dto->colorHeader = $data['color_heading'];
        }
        if (array_key_exists('color_line', $data)) {
            $dto->colorLine = $data['color_line'];
        }
        if (array_key_exists('color_potential_audience', $data)) {
            $dto->colorPotentialAudience = $data['color_potential_audience'];
        }

        return $dto;
    }
}
