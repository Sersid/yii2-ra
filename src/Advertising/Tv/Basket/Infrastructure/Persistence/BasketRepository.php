<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Infrastructure\Persistence;

use common\models\UserTvAd;
use common\models\UserTvAdTv;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketId;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Tv\Basket\Domain\Entity\Basket;
use NikoM\Kernel\Domain\Entity\Hydrator;

class BasketRepository implements IBasketRepository
{
    private static array $basket = [];
    private BasketConverter $basketConverter;
    private Hydrator $hydrator;

    public function __construct(BasketConverter $basketConverter, Hydrator $hydrator)
    {
        $this->basketConverter = $basketConverter;
        $this->hydrator = $hydrator;
    }

    public function getByBuyerId(BuyerId $buyerId): IBasket
    {
        $data = UserTvAd::find()
            ->select(['id', 'type', 'order_id', 'guest_id', 'user_tv_ad.status'])
            ->where(['guest_id' => $buyerId->getValue(), 'user_tv_ad.status' => UserTvAd::STATUS_NEW])
            ->with([
                'userTvAdTvs' => function ($query) {
                    $query->select([
                        'user_tv_ad_id',
                        'tv_ad_id',
                        'duration',
                        'views',
                        'days',
                        'price',
                        'price_without_discount',
                    ]);
                },
                'userTvAdTvs.tvAd' => function ($query) {
                    $query->select(['id', 'city_id', 'tv_id', 'price_ad_sec', 'discount']);
                },
                'userTvAdTvs.tvAd.city' => function ($query) {
                    $query->select(['id', 'name', 'where']);
                },
                'userTvAdTvs.tvAd.tv' => function ($query) {
                    $query->select(['id', 'name']);
                },
            ])
            ->orderBy('user_tv_ad.created_at')
            ->asArray()
            ->one();

        if (empty($data)) {
            throw new BasketNotFoundException();
        }

        $dto = $this->basketConverter->fromArrayToDTO($data);
        $basket = Basket::fromDTO($dto);
        self::$basket = $basket->toPrimitives();

        return $basket;
    }

    public function add(IBasket $basket): void
    {
        $model = new UserTvAd();
        $model->guest_id = $basket->getBuyerId()->getValue();
        $model->type = $basket->getAdvType()->getValue();
        $model->status = $basket->getStatus()->getValue();
        $model->save(false);

        $this->hydrator->setPropertyValue($basket, 'id', new BasketId($model->id));
        self::$basket = $basket->toPrimitives();
    }

    public function save(IBasket $basket): void
    {
        $data = [
            'prev' => self::$basket,
            'current' => $basket->toPrimitives(),
        ];

        if ($data['prev'] === $data['current']) {
            return;
        }

        $id = $basket->getId()->getValue();

        $update = [];
        if ($data['current']['advType'] !== $data['prev']['advType']) {
            $update['type'] = $data['current']['advType'];
        }
        if ($data['current']['status'] !== $data['prev']['status']) {
            $update['status'] = $data['current']['status'];
        }
        if (!empty($update)) {
            UserTvAd::updateAll($update, ['id' => $id]);
        }

        if ($data['current']['items'] === $data['prev']['items']) {
            return;
        }

        foreach ($data['current']['items'] as $key => $item) {
            if (isset($data['prev']['items'][$key])) {
                $prev = $data['prev']['items'][$key];
                if ($prev !== $item) {
                    $update = [];
                    if ($item['viewsPerDay'] !== $prev['viewsPerDay']) {
                        $update['views'] = $item['viewsPerDay'];
                    }
                    if ($item['duration'] !== $prev['duration']) {
                        $update['duration'] = $item['duration'];
                    }
                    if ($item['days'] !== $prev['days']) {
                        $update['days'] = $item['days'];
                    }
                    if ($item['priceWithoutDiscount'] !== $prev['priceWithoutDiscount']) {
                        $update['price_without_discount'] = $item['priceWithoutDiscount'];
                    }
                    if ($item['price'] !== $prev['price']) {
                        $update['price'] = $item['price'];
                    }
                    if (!empty($update)) {
                        UserTvAdTv::updateAll($update, ['user_tv_ad_id' => $id, 'tv_ad_id' => $key]);
                    }
                }
                unset($data['current']['items'][$key], $data['prev']['items'][$key]);
            }
        }

        foreach ($data['current']['items'] as $item) {
            $model = new UserTvAdTv();
            $model->user_tv_ad_id = $id;
            $model->tv_ad_id = $item['offerId'];
            $model->days = $item['days'];
            $model->duration = $item['duration'];
            $model->views = $item['viewsPerDay'];
            $model->price = $item['price'];
            $model->price_without_discount = $item['priceWithoutDiscount'];
            $model->save(false);
        }

        foreach ($data['prev']['items'] as $item) {
            UserTvAdTv::deleteAll(['user_tv_ad_id' => $id, 'tv_ad_id' => $item['offerId']]);
        }
    }
}
