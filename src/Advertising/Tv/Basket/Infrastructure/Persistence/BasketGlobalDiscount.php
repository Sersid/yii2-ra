<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Infrastructure\Persistence;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketGlobalDiscount;
use NikoM\Advertising\Kernel\Infrastructure\Persistence\GlobalDiscount;

class BasketGlobalDiscount extends GlobalDiscount
    implements IBasketGlobalDiscount
{
    protected function getConfigKey(): string
    {
        return 'tv_discount';
    }
}
