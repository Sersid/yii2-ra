<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Infrastructure\Persistence;

use NikoM\Advertising\Tv\Basket\Domain\DTO\BasketDTO;

class BasketConverter
{
    private BasketItemConverter $basketItemConverter;

    public function __construct(BasketItemConverter $basketItemConverter)
    {
        $this->basketItemConverter = $basketItemConverter;
    }

    public function fromArrayToDTO(array $data): BasketDTO
    {
        $dto = new BasketDTO();
        $dto->id = (int)$data['id'];
        $dto->buyerId = (int)$data['guest_id'];
        $dto->advType = (int)$data['type'];
        $dto->status = (int)$data['status'];
        $dto->orderId = empty($data['order_id']) ? null : (int)$data['order_id'];
        $dto->items = [];
        foreach ($data['userTvAdTvs'] as $item) {
            $dto->items[] = $this->basketItemConverter->fromArrayToDTO($item);
        }

        return $dto;
    }
}
