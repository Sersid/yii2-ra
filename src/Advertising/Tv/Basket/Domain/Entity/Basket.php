<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\AbstractBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketAdvTypeCollection;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketId;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketItemCollection;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketStatus;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketTotal;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\OrderId;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Tv\Basket\Domain\DTO\BasketDTO;
use NikoM\Advertising\Tv\Basket\Domain\Entity\AdvTypes\BasketAdvTypeAds;
use NikoM\Advertising\Tv\Basket\Domain\Entity\AdvTypes\BasketAdvTypeScreen;
use NikoM\Advertising\Tv\Basket\Domain\Entity\AdvTypes\BasketAdvTypeSponsorship;

class Basket extends AbstractBasket implements IBasket
{
    public static function fromDTO(BasketDTO $dto): self
    {
        $self = new static();
        $self->id = new BasketId($dto->id);
        $self->buyerId = new BuyerId($dto->buyerId);
        $self->advType = $self->getAdvTypeCollection()->get($dto->advType);
        $self->status = new BasketStatus($dto->status);
        $self->orderId = new OrderId($dto->orderId);
        $items = [];
        foreach ($dto->items as $item) {
            $items[] = BasketItem::fromDTO($item);
        }
        $self->itemCollection = new BasketItemCollection($items);
        $self->total = new BasketTotal($self->itemCollection);

        return $self;
    }

    public function toDTO(): BasketDTO
    {
        $dto = new BasketDTO();
        if (isset($this->id) && $this->id->isNotNull()) {
            $dto->id = (int)$this->id->getValue();
        }
        if (isset($dto->buyerId)) {
            $dto->buyerId = (int)$this->buyerId->getValue();
        }
        if (isset($dto->orderId)) {
            $dto->orderId = (int)$this->orderId->getValue();
        }
        $dto->advType = $this->advType->getValue();
        $dto->status = $this->status->getValue();
        $dto->items = [];

        return $dto;
    }

    public function getAdvTypeCollection(): BasketAdvTypeCollection
    {
        return new BasketAdvTypeCollection([
            new BasketAdvTypeAds(),
            new BasketAdvTypeSponsorship(),
            new BasketAdvTypeScreen(),
        ]);
    }
}
