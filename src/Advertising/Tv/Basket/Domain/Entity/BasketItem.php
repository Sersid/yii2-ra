<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\AbstractBasketItem;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketItem;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDays;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDuration;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPrice;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPriceWithoutDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemProfit;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemViewsPerDay;
use NikoM\Advertising\Tv\Basket\Domain\DTO\BasketItemDTO;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\OfferView;

class BasketItem extends AbstractBasketItem implements IBasketItem
{
    public static function fromDTO(BasketItemDTO $dto): self
    {
        $self = new self();
        $self->offer = new OfferView($dto->offer);
        $self->duration = new BasketItemDuration($dto->duration);
        $self->viewsPerDay = new BasketItemViewsPerDay($dto->viewsPerDay);
        $self->days = new BasketItemDays($dto->days);
        $self->priceWithoutDiscount = new BasketItemPriceWithoutDiscount($dto->priceWithoutDiscount);
        $self->price = new BasketItemPrice($dto->price);
        $self->profit = new BasketItemProfit($self->priceWithoutDiscount, $self->price);

        return $self;
    }
}
