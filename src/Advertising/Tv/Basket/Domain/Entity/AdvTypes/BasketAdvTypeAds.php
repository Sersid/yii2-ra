<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Domain\Entity\AdvTypes;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketAdvType;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketAdvType;

class BasketAdvTypeAds extends BasketAdvType implements IBasketAdvType
{
    public function getName(): string
    {
        return 'Рекламный ролик';
    }

    public function getValue(): int
    {
        return 1;
    }

    public function getText(): string
    {
        return 'Рекламный ролик работает сразу по трем каналам: аудио, видео и '
               . 'эмоции. Сочетание правильных слов и запоминающихся визуальных образов помогает быстро '
               . 'захватить внимание аудитории, удержать в памяти зрителей нужные слоганы, названия, '
               . 'логотипы.';
    }

    public function mustCalculate(): bool
    {
        return true;
    }
}
