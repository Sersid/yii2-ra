<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Domain\Entity\AdvTypes;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketAdvType;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketAdvType;

class BasketAdvTypeSponsorship extends BasketAdvType implements IBasketAdvType
{
    public function getName(): string
    {
        return 'Спонсорство';
    }

    public function getValue(): int
    {
        return 2;
    }

    public function getText(): string
    {
        return 'Реклама, вызывающая уважение у аудитории. Спонсоров рекламируют во время трансляции '
               . 'самых популярных программ и развлекательных шоу. Тогда, когда зрители точно находятся у '
               . 'экранов.';
    }

    public function mustCalculate(): bool
    {
        return false;
    }
}
