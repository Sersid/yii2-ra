<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Domain\Service;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketItem;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Service\AbstractBasketServiceProvider;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Service\IBasketServiceProvider;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Advertising\Tv\Basket\Domain\Entity\Basket;
use NikoM\Advertising\Tv\Basket\Domain\Entity\BasketItem;

class BasketServiceProvider extends AbstractBasketServiceProvider implements IBasketServiceProvider
{
    public function create(BuyerId $buyerId): IBasket
    {
        return Basket::create($buyerId);
    }

    public function createItem(IOfferView $offer): IBasketItem
    {
        return BasketItem::create($offer);
    }
}
