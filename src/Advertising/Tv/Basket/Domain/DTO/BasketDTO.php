<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Basket\Domain\DTO;

class BasketDTO
{
    public int $id;
    public int $buyerId;
    public int $advType;
    public int $status;
    public ?int $orderId;
    /** @var BasketItemDTO[] */
    public array $items;
}
