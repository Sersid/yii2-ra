<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Infrastructure\Persistence;

use common\models\Tv as TvModel;
use common\models\TvAd;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\Filter;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\OfferViewCollection;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelSlug;
use NikoM\Advertising\Tv\Offer\Domain\DTO\OfferDto;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\OfferView;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

class OfferFetcher implements IOfferFetcher
{
    private OfferConverter $offerConverter;

    public function __construct(OfferConverter $offerConverter)
    {
        $this->offerConverter = $offerConverter;
    }

    public function all(Filter $filter, int $offset): OfferViewCollection
    {
        $result = [];
        $offers = $this->getOffers($filter, $offset);
        foreach ($offers['offers'] as $offer) {
            $dto = $this->offerConverter->fromArrayToDTO($offer);
            $result[] = new OfferView($dto);
        }
        return new OfferViewCollection($result, $offers['total'], $offers['offset']);
    }

    private function getOffers(Filter $filter, int $offset): array
    {
        $query = TvAd::find()
            ->select([
                'tv_ad.id',
                'price_ad_sec',
                'city_id',
                'tv_id',
            ])
            ->joinWith([
                'tv' => function ($query) {
                    $query->select(['id', 'name', 'image_id', 'slug']);
                },
                'tv.image',
            ])
            ->with([
                'city' => function ($query) {
                    $query->select(['id', 'name', 'slug']);
                },
            ])
            ->orderBy(['tv_ad.sort' => SORT_ASC, 'tv.sort' => SORT_ASC])
            ->where(['tv_ad.status' => TvAd::STATUS_ACTIVE, 'tv.status' => TvModel::STATUS_ACTIVE]);

        if (!is_null($filter->cityId)) {
            $query->andWhere(['city_id' => $filter->cityId]);
        }
        if (!is_null($filter->offerSlug)) {
            $query->andWhere(['tv.slug' => $filter->offerSlug]);
        }
        $query->offset($offset);
        $query->limit(self::LIMIT);
        $total = (int)$query->count();
        $offset = $offset + self::LIMIT;
        $offset = $offset > $total ? null : $offset;

        return [
            'total' => $total,
            'offers' => $query->asArray()->all(),
            'offset' => $offset,
        ];
    }

    public function findByCitySlugAndChannelSlug(CitySlug $citySlug, ChannelSlug $channelSlug): ?OfferDto
    {
        $offer = TvAd::find()
            ->select([
                'tv_ad.id',
                'tv_ad.price_ad_sec',
                'tv_ad.discount',
                'tv_ad.city_id',
                'tv_ad.tv_id',
                'tv_ad.city_map',
                'tv_ad.price_table',
            ])
            ->innerJoinWith([
                'tv' => function ($query) {
                    $query->select([
                        'id',
                        'name',
                        'slug',
                        'image_id',
                        'big_logo',
                        'big_banner',
                        'color_heading',
                        'color_line',
                        'color_potential_audience',
                    ]);
                },
                'city' => function ($query) {
                    $query->select(['id', 'name', 'slug', 'where', 'content_tv']);
                },
            ])
            ->with([
                'cityMap',
                'tv.image',
                'tv.bigLogo',
                'tv.bigBanner',
                'potentialAudience' => function ($query) {
                    $query->select(['men', 'women', 'youth', 'total']);
                },
            ])
            ->where([
                'tv.slug' => $channelSlug->getValue(),
                'city.slug' => $citySlug->getValue(),
                'tv_ad.status' => TvAd::STATUS_ACTIVE,
            ])
            ->asArray()
            ->one();

        return empty($offer) ? null : $this->offerConverter->fromArrayToDTO($offer);
    }
}
