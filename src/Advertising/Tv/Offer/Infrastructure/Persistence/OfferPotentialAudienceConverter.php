<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Infrastructure\Persistence;

use NikoM\Advertising\Tv\Offer\Domain\DTO\OfferPotentialAudienceDto;

class OfferPotentialAudienceConverter
{
    public function fromArrayToDTO(array $data): OfferPotentialAudienceDto
    {
        $dto = new OfferPotentialAudienceDto();
        $dto->men = is_null($data['men']) ? null : (float)$data['men'];
        $dto->women = is_null($data['women']) ? null : (float)$data['women'];
        $dto->youth = is_null($data['youth']) ? null : (float)$data['youth'];
        $dto->total = is_null($data['total']) ? null : (float)$data['total'];

        return $dto;
    }
}
