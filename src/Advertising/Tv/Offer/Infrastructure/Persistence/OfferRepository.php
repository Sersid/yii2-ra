<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Infrastructure\Persistence;

use common\models\TvAd;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Exception\OfferNotFoundException;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferRepository;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\OfferView;

class OfferRepository implements IOfferRepository
{
    private OfferConverter $converter;

    public function __construct(OfferConverter $converter)
    {
        $this->converter = $converter;
    }

    public function getById(OfferId $offerId): IOfferView
    {
        $offer = TvAd::find()
            ->select([
                'tv_ad.id',
                'price_ad_sec',
                'discount',
                'city_id',
                'tv_id',
            ])
            ->joinWith([
                'tv' => function ($query) {
                    $query->select(['id', 'name']);
                },
            ])
            ->with([
                'city' => function ($query) {
                    $query->select(['id', 'name', 'where']);
                },
            ])
            ->where(['tv_ad.id' => $offerId->getValue(), 'tv_ad.status' => TvAd::STATUS_ACTIVE])
            ->asArray()
            ->one();
        if (empty($offer)) {
            throw new OfferNotFoundException();
        }
        $dto = $this->converter->fromArrayToDTO($offer);

        return new OfferView($dto);
    }
}
