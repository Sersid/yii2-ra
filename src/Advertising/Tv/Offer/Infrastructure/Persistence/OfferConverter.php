<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Infrastructure\Persistence;

use NikoM\Advertising\Tv\Channel\Infrastructure\ChannelConverter;
use NikoM\Advertising\Tv\Offer\Domain\DTO\OfferDto;
use NikoM\Kernel\Infrastructure\ImageConverter;
use NikoM\Location\City\Infrastructure\CityConverter;

class OfferConverter
{
    private ChannelConverter $channelConverter;
    private CityConverter $cityConverter;
    private OfferPotentialAudienceConverter $potentialAudienceConverter;
    private ImageConverter $imageConverter;

    public function __construct(ChannelConverter $channelConverter,
        CityConverter $cityConverter,
        OfferPotentialAudienceConverter $potentialAudienceConverter,
        ImageConverter $imageConverter)
    {
        $this->cityConverter = $cityConverter;
        $this->channelConverter = $channelConverter;
        $this->potentialAudienceConverter = $potentialAudienceConverter;
        $this->imageConverter = $imageConverter;
    }

    public function fromArrayToDTO(array $data): OfferDto
    {
        $dto = new OfferDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('price_ad_sec', $data)) {
            $dto->pricePerSec = (float)$data['price_ad_sec'];
            $dto->pricePerSec = empty($dto->pricePerSec) ? null : $dto->pricePerSec;
        }
        if (array_key_exists('discount', $data)) {
            $dto->discount = (float)$data['discount'];
            $dto->discount = empty($dto->discount) ? null : $dto->discount;
        }
        if (array_key_exists('cityMap', $data)) {
            $dto->cityMapImage = is_null($data['cityMap']) ? null : $this->imageConverter->fromArrayToDTO($data['cityMap']);
        }
        if (array_key_exists('city', $data)) {
            $dto->city = $this->cityConverter->fromArrayToDTO($data['city']);
        }
        if (array_key_exists('tv', $data)) {
            $dto->channel = $this->channelConverter->fromArrayToDTO($data['tv']);
        }
        if (array_key_exists('potentialAudience', $data)) {
            if (is_null($data['potentialAudience'])) {
                $dto->potentialAudience = null;
            } else {
                $dto->potentialAudience = $this->potentialAudienceConverter->fromArrayToDTO($data['potentialAudience']);
            }
        }
        if (array_key_exists('price_table', $data)) {
            $dto->priceTable = empty($data['price_table']) ? null : $data['price_table'];
        }

        return $dto;
    }
}
