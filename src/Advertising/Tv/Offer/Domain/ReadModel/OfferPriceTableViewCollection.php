<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Domain\ReadModel;

use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method OfferPriceTableView[] all()
 */
class OfferPriceTableViewCollection extends Collection
{
    protected static function type(): string
    {
        return OfferPriceTableView::class;
    }

    public static function fromString(?string $string): self
    {
        $items = [];
        if (!is_null($string)) {
            foreach (explode("\n", $string) as $line) {
                $items[] = OfferPriceTableView::fromString($line);
            }
        }

        return new self($items);
    }
}
