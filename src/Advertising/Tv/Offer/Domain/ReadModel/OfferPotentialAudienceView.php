<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Domain\ReadModel;

use NikoM\Advertising\Tv\Offer\Domain\DTO\OfferPotentialAudienceDto;
use NikoM\Kernel\Domain\FormatPriceTrait;

class OfferPotentialAudienceView
{
    use FormatPriceTrait;

    private ?float $men;
    private ?float $women;
    private ?float $youth;
    private ?float $total;

    public function __construct(OfferPotentialAudienceDto $dto)
    {
        $this->men = $dto->men;
        $this->women = $dto->women;
        $this->youth = $dto->youth;
        $this->total = $dto->total;
    }

    public function men(): ?string
    {
        return $this->formatPrice($this->men);
    }

    public function women(): ?string
    {
        return $this->formatPrice($this->women);
    }

    public function youth(): ?string
    {
        return $this->formatPrice($this->youth);
    }

    public function total(): ?string
    {
        return $this->formatPrice($this->total);
    }
}
