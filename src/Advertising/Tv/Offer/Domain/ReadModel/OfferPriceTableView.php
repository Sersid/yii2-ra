<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Domain\ReadModel;

class OfferPriceTableView
{
    private ?string $duration;
    private ?string $primeTime;
    private ?string $offPrime;

    public static function fromString(string $string): self
    {
        $parts = explode('|', trim($string));
        $self = new self();
        $self->duration = $parts[0] ?? null;
        $self->primeTime = $parts[1] ?? null;
        $self->offPrime = $parts[2] ?? null;

        return $self;
    }

    public function duration(): ?string
    {
        return $this->duration;
    }

    public function primeTime(): ?string
    {
        return $this->primeTime;
    }

    public function offPrime(): ?string
    {
        return $this->offPrime;
    }
}
