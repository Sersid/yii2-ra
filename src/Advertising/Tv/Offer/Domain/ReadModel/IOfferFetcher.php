<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Domain\ReadModel;

use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelSlug;
use NikoM\Advertising\Tv\Offer\Domain\DTO\OfferDto;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

interface IOfferFetcher extends \NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferFetcher
{
    public function findByCitySlugAndChannelSlug(CitySlug $citySlug, ChannelSlug $channelSlug): ?OfferDto;
}
