<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Domain\ReadModel;

use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;
use NikoM\Advertising\Kernel\ReadModel\ImageView;
use NikoM\Advertising\Tv\Channel\Domain\ReadModel\ChannelView;
use NikoM\Advertising\Tv\Offer\Domain\DTO\OfferDto;
use NikoM\Location\City\Domain\ReadModel\CityView;

class OfferView implements IOfferView
{
    private int $id;
    private CityView $city;
    private ChannelView $channel;
    private ?float $pricePerSec;
    private ?float $discount;
    private ?OfferPotentialAudienceView $potentialAudience;
    private ?ImageView $cityMapImage;
    private OfferPriceTableViewCollection $priceTableCollection;

    public function __construct(OfferDto $dto)
    {
        if ($dto->hasProperty('id')) {
            $this->id = $dto->id;
        }
        if ($dto->hasProperty('city')) {
            $this->city = new CityView($dto->city);
        }
        if ($dto->hasProperty('channel')) {
            $this->channel = new ChannelView($dto->channel);
        }
        if ($dto->hasProperty('pricePerSec')) {
            $this->pricePerSec = $dto->pricePerSec;
        }
        if ($dto->hasProperty('discount')) {
            $this->discount = $dto->discount;
        }
        if ($dto->hasProperty('potentialAudience')) {
            if (!is_null($dto->potentialAudience)) {
                $this->potentialAudience = new OfferPotentialAudienceView($dto->potentialAudience);
            } else {
                $this->potentialAudience = null;
            }
        }
        if ($dto->hasProperty('cityMapImage')) {
            $this->cityMapImage = is_null($dto->cityMapImage) ? null : new ImageView($dto->cityMapImage);
        }
        if ($dto->hasProperty('priceTable')) {
            $this->priceTableCollection = OfferPriceTableViewCollection::fromString($dto->priceTable);
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function city(): CityView
    {
        return $this->city;
    }

    public function channel(): ChannelView
    {
        return $this->channel;
    }

    public function name(): string
    {
        return $this->city->name() . ' - ' . $this->channel->name();
    }

    public function url(): string
    {
        return '/tv/' . $this->city->slug() . '/' . $this->channel->slug();
    }

    public function heading(): string
    {
        return 'Реклама на ' . $this->channel->name() . ' ' . $this->city->where();
    }

    public function pageTitle(): string
    {
        return 'Реклама на ' . $this->channel->name() . ' ' . $this->city->where() . ' - РА «Нико-Медиа»';
    }

    public function pageDescription(): string
    {
        return 'Реклама на телеканалах города ' . $this->city->name() . '. '
               . 'Размещаем рекламные ролики и заставки на телеканале ' . $this->channel->name() . '. '
               . 'Подберём для вас оптимальный вариант размещения.';
    }

    public function hasPricePerSec(): bool
    {
        return !is_null($this->pricePerSec);
    }

    public function pricePerSec(): ?float
    {
        return $this->pricePerSec;
    }

    public function discount(): ?float
    {
        return $this->discount;
    }

    public function hasPotentialAudience(): bool
    {
        return !is_null($this->potentialAudience);
    }

    public function potentialAudience(): OfferPotentialAudienceView
    {
        return $this->potentialAudience;
    }

    public function hasCityMapImage(): bool
    {
        return !is_null($this->cityMapImage);
    }

    public function cityMapImage(): ?ImageView
    {
        return $this->cityMapImage;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name(),
            'hasPricePerSec' => $this->hasPricePerSec(),
        ];
    }

    public function isAdvanced(): bool
    {
        return $this->channel->hasBigLogo() && $this->channel->hasBigBanner();
    }

    public function hasPriceTableCollection(): bool
    {
        return $this->priceTableCollection->isNotEmpty();
    }

    public function priceTableCollection(): OfferPriceTableViewCollection
    {
        return $this->priceTableCollection;
    }

    public function logo(): ImageView
    {
        return $this->channel->logo();
    }
}
