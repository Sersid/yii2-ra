<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Domain\DTO;

use NikoM\Kernel\Domain\Dto;

class OfferPotentialAudienceDto extends Dto
{
    public ?float $men;
    public ?float $women;
    public ?float $youth;
    public ?float $total;
}
