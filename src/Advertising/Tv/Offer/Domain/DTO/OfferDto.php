<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Domain\DTO;

use NikoM\Advertising\Kernel\DTO\ImageDto;
use NikoM\Advertising\Tv\Channel\Domain\DTO\ChannelDto;
use NikoM\Kernel\Domain\Dto;
use NikoM\Location\City\Domain\DTO\CityDto;

class OfferDto extends Dto
{
    public int $id;
    public CityDto $city;
    public ChannelDto $channel;
    public ?float $pricePerSec;
    public ?float $discount;
    public ?string $color;
    public ?OfferPotentialAudienceDto $potentialAudience;
    public ?ImageDto $cityMapImage;
    public ?string $priceTable;
}
