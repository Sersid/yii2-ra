<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Application\ShowAdvanced;

class ShowAdvancedQuery
{
    public string $citySlug;
    public string $channelSlug;
}
