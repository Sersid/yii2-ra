<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\Offer\Application\ShowAdvanced;

use NikoM\Advertising\AbstractMedia\Offer\Domain\Exception\OfferNotFoundException;
use NikoM\Advertising\Tv\Channel\Domain\Entity\ChannelSlug;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\Tv\Offer\Domain\ReadModel\OfferView;
use NikoM\Location\City\Domain\Entity\Seo\CitySlug;

class ShowAdvancedQueryHandler
{
    private IOfferFetcher $offerRepository;

    public function __construct(IOfferFetcher $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    public function handle(ShowAdvancedQuery $query): ?OfferView
    {
        $citySlug = new CitySlug($query->citySlug);
        $channelSlug = new ChannelSlug($query->channelSlug);
        $dto = $this->offerRepository->findByCitySlugAndChannelSlug($citySlug, $channelSlug);
        if (is_null($dto)) {
            throw new OfferNotFoundException();
        }

        return new OfferView($dto);
    }
}
