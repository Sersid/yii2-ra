<?php
declare(strict_types=1);

namespace NikoM\Advertising\Tv\City\Infrastructure\Persistence;

use common\models\TvAd;
use NikoM\Advertising\AbstractMedia\City\Domain\ICityFetcher;
use NikoM\Advertising\Kernel\Infrastructure\Persistence\AbstractCityFetcher;
use yii\db\ActiveQuery;

class CityFetcher extends AbstractCityFetcher implements ICityFetcher
{
    protected function getQuery(): ActiveQuery
    {
        return TvAd::find()
            ->select('city_id')
            ->groupBy('city_id')
            ->where(['status' => TvAd::STATUS_ACTIVE]);
    }
}
