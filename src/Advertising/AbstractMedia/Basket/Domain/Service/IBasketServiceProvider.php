<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Service;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketItem;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

interface IBasketServiceProvider
{
    /**
     * @param BuyerId $buyerId
     *
     * @return IBasket
     * @throws BasketNotFoundException
     */
    public function get(BuyerId $buyerId): IBasket;
    public function getOrAdd(BuyerId $buyerId): IBasket;
    public function getOrCreate(BuyerId $buyerId): IBasket;
    public function create(BuyerId $buyerId): IBasket;
    public function createItem(IOfferView $offer): IBasketItem;
}
