<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Service;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketGlobalDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketItem;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

abstract class AbstractBasketServiceProvider implements IBasketServiceProvider
{
    private IBasketRepository $basketRepository;
    private IBasketGlobalDiscount $basketGlobalDiscount;

    public function __construct(IBasketRepository $basketRepository, IBasketGlobalDiscount $basketGlobalDiscount)
    {
        $this->basketRepository = $basketRepository;
        $this->basketGlobalDiscount = $basketGlobalDiscount;
    }

    public function getOrAdd(BuyerId $buyerId): IBasket
    {
        try {
            return $this->get($buyerId);
        } catch (BasketNotFoundException $e) {
            $basket = $this->create($buyerId);
            $this->basketRepository->add($basket);

            return $basket;
        }
    }

    /**
     * @inheritDoc
     */
    public function get(BuyerId $buyerId): IBasket
    {
        $basket = $this->basketRepository->getByBuyerId($buyerId);
        $basket->recalculate($this->basketGlobalDiscount);

        return $basket;
    }

    abstract public function create(BuyerId $buyerId): IBasket;

    abstract public function createItem(IOfferView $offer): IBasketItem;

    public function getOrCreate(BuyerId $buyerId): IBasket
    {
        try {
            return $this->get($buyerId);
        } catch (BasketNotFoundException $e) {
            return $this->create($buyerId);
        }
    }
}
