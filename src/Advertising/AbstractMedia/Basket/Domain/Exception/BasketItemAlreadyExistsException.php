<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class BasketItemAlreadyExistsException extends DomainException
{
    public function message(): string
    {
        return 'Basket item already exists.';
    }

    public function code(): int
    {
        return 400;
    }
}
