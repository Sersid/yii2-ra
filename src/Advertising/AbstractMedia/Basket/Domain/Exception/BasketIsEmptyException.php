<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Exception;

use NikoM\Kernel\Domain\Entity\Exception\DomainException;

class BasketIsEmptyException extends DomainException
{
    public function message(): string
    {
        return 'Basket is empty.';
    }

    public function code(): int
    {
        return 404;
    }
}
