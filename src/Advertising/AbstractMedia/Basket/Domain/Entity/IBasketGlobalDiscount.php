<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

interface IBasketGlobalDiscount
{
    public function getValue(): ?float;

    public function has(): bool;
}
