<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDays;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDuration;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPrice;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPriceWithoutDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemProfit;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemViewsPerDay;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;

interface IBasketItem
{
    public static function create(IOfferView $offer): self;
    public function getOffer(): IOfferView;
    public function getDays(): BasketItemDays;
    public function changeDays(BasketItemDays $days): void;
    public function getDuration(): BasketItemDuration;
    public function changeDuration(BasketItemDuration $duration): void;
    public function getViewsPerDay(): BasketItemViewsPerDay;
    public function changeViewsPerDay(BasketItemViewsPerDay $viewsPerDay): void;
    public function isEqualParams(self $other): bool;
    public function recalculate(IBasketAdvType $advType, IBasketGlobalDiscount $globalDiscount): void;
    public function getPriceWithoutDiscount(): BasketItemPriceWithoutDiscount;
    public function getPrice(): BasketItemPrice;
    public function getProfit(): BasketItemProfit;
}
