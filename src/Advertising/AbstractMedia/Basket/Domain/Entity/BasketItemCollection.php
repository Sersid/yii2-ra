<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use JsonSerializable;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketItemAlreadyExistsException;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketItemNotFoundException;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method IBasketItem[] all()
 */
class BasketItemCollection extends Collection implements JsonSerializable
{
    public function __construct(array $items)
    {
        parent::__construct($items);
        $this->items = [];
        foreach ($items as $item) {
            $this->items[$item->getOffer()->id()] = $item;
        }
    }

    public function jsonSerialize(): array
    {
        return array_values($this->items);
    }

    public function add(IBasketItem $item): void
    {
        $offerId = $item->getOffer()->id();
        if (isset($this->items[$offerId])) {
            throw new BasketItemAlreadyExistsException();
        }
        $this->items[$offerId] = $item;
    }

    public function get(OfferId $offerId): IBasketItem
    {
        if (!isset($this->items[$offerId->getValue()])) {
            throw new BasketItemNotFoundException();
        }

        return $this->items[$offerId->getValue()];
    }

    /**
     * @param IBasketItem $item
     *
     * @return IBasketItem[]
     */
    public function getNextItems(IBasketItem $item): array
    {
        $keys = array_keys($this->items);
        $items = [];
        for ($i = array_search($item->getOffer()->id(), $keys) + 1; $i < $this->count(); $i++) {
            $items[$keys[$i]] = $this->items[$keys[$i]];
        }

        return $items;
    }

    public function remove(OfferId $offerId): void
    {
        if (!isset($this->items[$offerId->getValue()])) {
            throw new BasketItemNotFoundException();
        }
        unset($this->items[$offerId->getValue()]);
    }

    public function recalculate(IBasketAdvType $advType, IBasketGlobalDiscount $globalDiscount): void
    {
        foreach ($this->all() as $item) {
            $item->recalculate($advType, $globalDiscount);
        }
    }

    protected static function type(): string
    {
        return IBasketItem::class;
    }
}
