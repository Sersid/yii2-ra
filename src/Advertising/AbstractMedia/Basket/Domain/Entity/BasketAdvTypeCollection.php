<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use JsonSerializable;
use NikoM\Kernel\Domain\Entity\LaraCollection;
use Webmozart\Assert\Assert;

/**
 * @property IBasketAdvType[] $items
 */
class BasketAdvTypeCollection extends LaraCollection implements JsonSerializable
{
    protected static function type(): string
    {
        return IBasketAdvType::class;
    }

    public function __construct(array $items)
    {
        Assert::notEmpty($items);
        parent::__construct($items);
    }

    public function getDefault(): IBasketAdvType
    {
        return $this->first();
    }

    public function get(int $value): IBasketAdvType
    {
        $item = $this->first(fn(IBasketAdvType $advType) => $advType->getValue() === $value);
        Assert::notEmpty($item, 'Invalid advertising type');
        return $item;
    }

    public function jsonSerialize(): array
    {
        $json = [];
        foreach ($this->all() as $advType) {
            $json[] = [
                'name' => $advType->getName(),
                'value' => $advType->getValue(),
                'text' => $advType->getText(),
            ];
        }
        return $json;
    }
}
