<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

interface IBasket
{
    public static function create(BuyerId $buyerId): self;
    public function getId(): BasketId;
    public function getStatus(): BasketStatus;
    public function changeStatus(BasketStatus $status): void;
    public function getAdvTypeCollection(): BasketAdvTypeCollection;
    public function changeAdvType(IBasketAdvType $advType): void;
    public function getItemCollection(): BasketItemCollection;
    public function addItem(IBasketItem $item): void;
    public function removeItem(OfferId $offerId): void;
    public function recalculate(IBasketGlobalDiscount $globalDiscount): void;
    public function toPrimitives(): array;
    public function getTotal(): BasketTotal;
}
