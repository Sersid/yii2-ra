<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntNullableValueObject;

class BasketTotalViewsPerDay extends IntNullableValueObject
{
}
