<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total;

use NikoM\Kernel\Domain\Entity\ValueObject\Money\MoneyNullableValueObject;

class BasketTotalPriceWithoutDiscount extends MoneyNullableValueObject
{
    public function __construct(?float $value)
    {
        parent::__construct($value);
        $this->floor();
    }
}
