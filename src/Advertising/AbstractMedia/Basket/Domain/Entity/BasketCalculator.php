<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPrice;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPriceWithoutDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemProfit;

class BasketCalculator
{
    private IBasketItem $basketItem;
    private IBasketAdvType $advType;
    private IBasketGlobalDiscount $globalDiscount;
    private BasketItemPriceWithoutDiscount $priceWithoutDiscount;
    private BasketItemDiscount $discount;
    private BasketItemProfit $profit;
    private BasketItemPrice $price;

    public function __construct(IBasketItem $basketItem, IBasketAdvType $advType, IBasketGlobalDiscount $globalDiscount)
    {
        $this->basketItem = $basketItem;
        $this->advType = $advType;
        $this->globalDiscount = $globalDiscount;

        $this->discount = new BasketItemDiscount($this->getCalculatedDiscount());
        $this->priceWithoutDiscount = new BasketItemPriceWithoutDiscount($this->getCalculatedPriceWithoutDiscount());
        $this->price = new BasketItemPrice($this->getCalculatedPrice());
        $this->profit = new BasketItemProfit($this->priceWithoutDiscount, $this->price);
    }

    private function getCalculatedPriceWithoutDiscount(): ?float
    {
        if (!$this->advType->mustCalculate()) {
            return null;
        }
        if (!$this->basketItem->getOffer()->hasPricePerSec()) {
            return null;
        }
        if ($this->basketItem->getDuration()->isNull()) {
            return null;
        }
        if ($this->basketItem->getViewsPerDay()->isNull()) {
            return null;
        }
        if ($this->basketItem->getDays()->isNull()) {
            return null;
        }
        $value = $this->basketItem->getOffer()->pricePerSec();
        $value *= $this->basketItem->getDuration()->getValue();
        $value *= $this->basketItem->getViewsPerDay()->getValue();
        $value *= $this->basketItem->getDays()->getValue();

        return $value;
    }

    public function getPriceWithoutDiscount(): BasketItemPriceWithoutDiscount
    {
        return $this->priceWithoutDiscount;
    }

    private function getCalculatedDiscount(): ?float
    {
        if ($this->globalDiscount->has()) {
            return $this->globalDiscount->getValue();
        } else {
            return $this->basketItem->getOffer()->discount();
        }
    }

    public function getProfit(): BasketItemProfit
    {
        return $this->profit;
    }

    private function getCalculatedPrice(): ?float
    {
        if ($this->discount->isNotNull() && $this->priceWithoutDiscount->isNotNull()) {
            $profit = $this->priceWithoutDiscount->getValue() / 100 * $this->discount->getValue();

            return $this->priceWithoutDiscount->getValue() - $profit;
        } else {
            return $this->priceWithoutDiscount->getValue();
        }
    }

    public function getPrice(): BasketItemPrice
    {
        return $this->price;
    }
}
