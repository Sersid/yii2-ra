<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use JsonSerializable;

abstract class BasketAdvType implements IBasketAdvType, JsonSerializable
{
    abstract public function getName(): string;

    abstract public function getText(): string;

    public function isEqualTo(IBasketAdvType $other): bool
    {
        return $this->getValue() === $other->getValue();
    }

    abstract public function getValue(): int;

    public function jsonSerialize(): array
    {
        return [
            'value' => $this->getValue(),
            'mustCalculate' => $this->mustCalculate(),
        ];
    }

    abstract public function mustCalculate(): bool;
}
