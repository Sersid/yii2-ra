<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

interface IBasketAdvType
{
    public function getName(): string;

    public function getText(): string;

    public function isEqualTo(self $other): bool;

    public function getValue(): int;

    public function mustCalculate(): bool;
}
