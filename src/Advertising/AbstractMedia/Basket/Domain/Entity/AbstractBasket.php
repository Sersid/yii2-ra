<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use JsonSerializable;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\OrderId;
use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketAdvTypeAlreadyException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

abstract class AbstractBasket implements IBasket, JsonSerializable
{
    protected BasketId $id;
    protected OrderId $orderId;
    protected BuyerId $buyerId;
    protected IBasketAdvType $advType;
    protected BasketStatus $status;
    protected BasketItemCollection $itemCollection;
    protected BasketTotal $total;

    protected function __construct()
    {
    }

    public static function create(BuyerId $buyerId): IBasket
    {
        $self = new static();
        $self->buyerId = $buyerId;
        $self->advType = $self->getAdvTypeCollection()->getDefault();
        $self->status = BasketStatus::inBasket();
        $self->itemCollection = new BasketItemCollection([]);
        $self->total = new BasketTotal($self->itemCollection);

        return $self;
    }

    public function jsonSerialize(): array
    {
        return [
            'advType' => $this->advType,
            'items' => $this->itemCollection,
            'count' => $this->itemCollection->count(),
            'total' => $this->total,
        ];
    }

    public function toPrimitives(): array
    {
        $result = [
            'id' => $this->id->getValue(),
            'orderId' => null,
            'advType' => $this->advType->getValue(),
            'status' => $this->status->getValue(),
            'items' => [],
        ];
        if (isset($this->orderId)) {
            $result['orderId'] = $this->orderId->getValue();
        }

        foreach ($this->itemCollection->all() as $item) {
            $result['items'][$item->getOffer()->id()] = [
                'offerId' => $item->getOffer()->id(),
                'days' => $item->getDays()->getValue(),
                'viewsPerDay' => $item->getViewsPerDay()->getValue(),
                'duration' => $item->getDuration()->getValue(),
                'price' => $item->getPrice()->getValue(),
                'priceWithoutDiscount' => $item->getPriceWithoutDiscount()->getValue(),
            ];
        }

        return $result;
    }

    public function getId(): BasketId
    {
        return $this->id;
    }

    public function getBuyerId(): BuyerId
    {
        return $this->buyerId;
    }

    public function getAdvType(): IBasketAdvType
    {
        return $this->advType;
    }

    public function changeAdvType(IBasketAdvType $advType): void
    {
        if ($this->advType->isEqualTo($advType)) {
            throw new BasketAdvTypeAlreadyException();
        }
        $this->advType = $advType;
    }

    public function getStatus(): BasketStatus
    {
        return $this->status;
    }

    public function changeStatus(BasketStatus $status): void
    {
        if (!$this->status->isEqual($status)) {
            $this->status = $status;
        }
    }

    public function getItemCollection(): BasketItemCollection
    {
        return $this->itemCollection;
    }

    public function addItem(IBasketItem $item): void
    {
        $this->itemCollection->add($item);
    }

    public function removeItem(OfferId $offerId): void
    {
        $this->itemCollection->remove($offerId);
    }

    public function recalculate(IBasketGlobalDiscount $globalDiscount): void
    {
        $this->itemCollection->recalculate($this->advType, $globalDiscount);
        $this->total = new BasketTotal($this->itemCollection);
    }

    public function getTotal(): BasketTotal
    {
        return $this->total;
    }
}
