<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item;

use NikoM\Kernel\Domain\Entity\ValueObject\Money\MoneyNullableValueObject;

class BasketItemProfit extends MoneyNullableValueObject
{
    public function __construct(BasketItemPriceWithoutDiscount $priceWithoutDiscount, BasketItemPrice $price)
    {
        if ($priceWithoutDiscount->isNull() || $price->isNull()) {
            $value = null;
        } else {
            $profit = $priceWithoutDiscount->getValue() - $price->getValue();
            $value = $profit > 0 ? $profit : null;
        }
        parent::__construct($value);
        $this->floor();
    }
}
