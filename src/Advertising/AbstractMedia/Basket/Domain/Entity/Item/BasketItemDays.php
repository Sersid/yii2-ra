<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntNullableValueObject;
use Webmozart\Assert\Assert;

class BasketItemDays extends IntNullableValueObject
{
    public function __construct(?int $value)
    {
        parent::__construct($value);
        if ($this->isNotNull()) {
            Assert::greaterThan($value, 0);
        }
    }
}
