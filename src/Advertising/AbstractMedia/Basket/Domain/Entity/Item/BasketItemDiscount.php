<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item;

use NikoM\Kernel\Domain\Entity\ValueObject\Float\FloatNullableValueObject;

class BasketItemDiscount extends FloatNullableValueObject
{
}
