<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use JsonSerializable;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total\BasketTotalDays;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total\BasketTotalDuration;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total\BasketTotalPrice;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total\BasketTotalPriceWithoutDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total\BasketTotalProfit;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Total\BasketTotalViewsPerDay;

class BasketTotal implements JsonSerializable
{
    private BasketTotalViewsPerDay $viewsPerDay;
    private BasketTotalDuration $duration;
    private BasketTotalDays $days;
    private BasketTotalPrice $price;
    private BasketTotalPriceWithoutDiscount $priceWithoutDiscount;
    private BasketTotalProfit $profit;

    public function __construct(BasketItemCollection $itemCollection)
    {
        $duration = null;
        $viewsPerDay = null;
        $days = null;
        $price = null;
        $priceWithoutDiscount = null;
        $profit = null;

        foreach ($itemCollection->all() as $item) {
            if ($item->getDuration()->isNotNull()) {
                $duration += $item->getDuration()->getValue();
            }
            if ($item->getViewsPerDay()->isNotNull()) {
                $viewsPerDay += $item->getViewsPerDay()->getValue();
            }
            if ($item->getDays()->isNotNull()) {
                $days += $item->getDays()->getValue();
            }
            if ($item->getPrice()->isNotNull()) {
                $price += $item->getPrice()->getValue();
            }
            if ($item->getPriceWithoutDiscount()->isNotNull()) {
                $priceWithoutDiscount += $item->getPriceWithoutDiscount()->getValue();
            }
            if ($item->getProfit()->isNotNull()) {
                $profit += $item->getProfit()->getValue();
            }
        }

        $this->viewsPerDay = new BasketTotalViewsPerDay($viewsPerDay);
        $this->duration = new BasketTotalDuration($duration);
        $this->days = new BasketTotalDays($days);
        $this->price = new BasketTotalPrice($price);
        $this->priceWithoutDiscount = new BasketTotalPriceWithoutDiscount($priceWithoutDiscount);
        $this->profit = new BasketTotalProfit($profit);
    }

    public function jsonSerialize(): array
    {
        return [
            'viewsPerDay' => $this->viewsPerDay,
            'duration' => $this->duration,
            'days' => $this->days,
            'oldPrice' => $this->profit->isNotNull() ? $this->priceWithoutDiscount->getFormatted() : null,
            'price' => $this->price->getFormatted(),
            'profit' => $this->profit->getFormatted(),
        ];
    }

    public function getViewsPerDay(): BasketTotalViewsPerDay
    {
        return $this->viewsPerDay;
    }

    public function getDuration(): BasketTotalDuration
    {
        return $this->duration;
    }

    public function getDays(): BasketTotalDays
    {
        return $this->days;
    }

    public function getPriceWithoutDiscount(): BasketTotalPriceWithoutDiscount
    {
        return $this->priceWithoutDiscount;
    }

    public function getPrice(): BasketTotalPrice
    {
        return $this->price;
    }

    public function getProfit(): BasketTotalProfit
    {
        return $this->profit;
    }
}
