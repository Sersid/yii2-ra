<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;
use Webmozart\Assert\Assert;

class BasketStatus extends IntValueObject
{
    private const IN_BASKET = 1;
    private const ORDERED = 2;

    public function __construct(int $value)
    {
        Assert::inArray($value, [self::IN_BASKET, self::ORDERED]);
        parent::__construct($value);
    }

    public static function inBasket(): self
    {
        return new self(self::IN_BASKET);
    }

    public static function checkout(): self
    {
        return new self(self::ORDERED);
    }

    public function isOrdered(): bool
    {
        return $this->value === self::ORDERED;
    }
}
