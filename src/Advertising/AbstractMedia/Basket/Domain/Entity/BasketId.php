<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdNullableValueObject;

class BasketId extends IdNullableValueObject
{
}
