<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use NikoM\Advertising\Kernel\Domain\Basket\Exception\BasketNotFoundException;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

interface IBasketRepository
{
    /**
     * @param BuyerId $buyerId
     *
     * @return IBasket
     * @throws BasketNotFoundException
     */
    public function getByBuyerId(BuyerId $buyerId): IBasket;

    public function add(IBasket $basket): void;

    public function save(IBasket $basket): void;
}
