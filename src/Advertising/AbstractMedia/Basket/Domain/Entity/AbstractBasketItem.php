<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Domain\Entity;

use JsonSerializable;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDays;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDuration;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPrice;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemPriceWithoutDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemProfit;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemViewsPerDay;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferView;

abstract class AbstractBasketItem implements IBasketItem, JsonSerializable
{
    protected IOfferView $offer;
    protected BasketItemViewsPerDay $viewsPerDay;
    protected BasketItemDuration $duration;
    protected BasketItemDays $days;
    protected BasketItemPrice $price;
    protected BasketItemPriceWithoutDiscount $priceWithoutDiscount;
    protected BasketItemProfit $profit;

    public static function create(IOfferView $offer): self
    {
        $item = new static();
        $item->offer = $offer;
        $item->viewsPerDay = new BasketItemViewsPerDay(null);
        $item->duration = new BasketItemDuration(null);
        $item->days = new BasketItemDays(null);
        $item->price = new BasketItemPrice(null);
        $item->priceWithoutDiscount = new BasketItemPriceWithoutDiscount(null);
        $item->profit = new BasketItemProfit($item->priceWithoutDiscount, $item->price);

        return $item;
    }

    public function jsonSerialize(): array
    {
        return [
            'offer' => $this->offer,
            'viewsPerDay' => $this->viewsPerDay,
            'duration' => $this->duration,
            'days' => $this->days,
            'oldPrice' => $this->profit->isNotNull() ? $this->priceWithoutDiscount->getFormatted() : null,
            'price' => $this->price->getFormatted(),
            'profit' => $this->profit->getFormatted(),
        ];
    }

    public function getOffer(): IOfferView
    {
        return $this->offer;
    }

    public function getDays(): BasketItemDays
    {
        return $this->days;
    }

    public function changeDays(BasketItemDays $days): void
    {
        if (!$this->days->isEqual($days)) {
            $this->days = $days;
        }
    }

    public function getDuration(): BasketItemDuration
    {
        return $this->duration;
    }

    public function changeDuration(BasketItemDuration $duration): void
    {
        if (!$this->duration->isEqual($duration)) {
            $this->duration = $duration;
        }
    }

    public function getViewsPerDay(): BasketItemViewsPerDay
    {
        return $this->viewsPerDay;
    }

    public function changeViewsPerDay(BasketItemViewsPerDay $viewsPerDay): void
    {
        if (!$this->viewsPerDay->isEqual($viewsPerDay)) {
            $this->viewsPerDay = $viewsPerDay;
        }
    }

    public function isEqualParams(IBasketItem $other): bool
    {
        $isEqual = $this->days->isEqual($other->getDays());
        $isEqual = $isEqual && $this->duration->isEqual($other->getDuration());
        $isEqual = $isEqual && $this->viewsPerDay->isEqual($other->getViewsPerDay());

        return $isEqual;
    }

    public function recalculate(IBasketAdvType $advType, IBasketGlobalDiscount $globalDiscount): void
    {
        $calculator = new BasketCalculator($this, $advType, $globalDiscount);
        $this->priceWithoutDiscount = $calculator->getPriceWithoutDiscount();
        $this->price = $calculator->getPrice();
        $this->profit = $calculator->getProfit();
    }

    public function getPriceWithoutDiscount(): BasketItemPriceWithoutDiscount
    {
        return $this->priceWithoutDiscount;
    }

    public function getPrice(): BasketItemPrice
    {
        return $this->price;
    }

    public function getProfit(): BasketItemProfit
    {
        return $this->profit;
    }
}
