<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\AddToBasket;

class AddToBasketCommand
{
    public int $buyerId;
    public int $offerId;
}
