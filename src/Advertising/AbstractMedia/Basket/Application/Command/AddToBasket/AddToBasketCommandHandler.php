<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\AddToBasket;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketAlreadyOrderedException;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Service\IBasketServiceProvider;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferRepository;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

class AddToBasketCommandHandler
{
    private IBasketRepository $basketRepository;
    private IOfferRepository $offerRepository;
    private IBasketServiceProvider $basketServiceProvider;

    public function __construct(
        IBasketServiceProvider $basketServiceProvider,
        IBasketRepository $basketRepository,
        IOfferRepository $offerRepository
    ) {
        $this->basketServiceProvider = $basketServiceProvider;
        $this->basketRepository = $basketRepository;
        $this->offerRepository = $offerRepository;
    }

    public function handle(AddToBasketCommand $command): IBasket
    {
        $offerId = new OfferId($command->offerId);
        $basket = $this->basketServiceProvider->getOrAdd(new BuyerId($command->buyerId));
        if ($basket->getStatus()->isOrdered()) {
            throw new BasketAlreadyOrderedException();
        }
        $offer = $this->offerRepository->getById($offerId);
        $item = $this->basketServiceProvider->createItem($offer);
        $basket->addItem($item);
        $this->basketRepository->save($basket);

        return $basket;
    }
}
