<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasketItem;

class UpdateBasketItemCommand
{
    public int $buyerId;
    public int $offerId;
    public ?int $days;
    public ?int $duration;
    public ?int $viewsPerDay;
}
