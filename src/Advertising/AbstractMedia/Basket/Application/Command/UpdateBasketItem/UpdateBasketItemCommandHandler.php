<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasketItem;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketGlobalDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDays;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemDuration;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\Item\BasketItemViewsPerDay;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferRepository;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

class UpdateBasketItemCommandHandler
{
    private IBasketRepository $basketRepository;
    private IOfferRepository $offerRepository;
    private IBasketGlobalDiscount $basketGlobalDiscount;

    public function __construct(
        IBasketRepository $basketRepository,
        IOfferRepository $offerRepository,
        IBasketGlobalDiscount $basketGlobalDiscount
    ) {
        $this->basketRepository = $basketRepository;
        $this->offerRepository = $offerRepository;
        $this->basketGlobalDiscount = $basketGlobalDiscount;
    }

    public function handle(UpdateBasketItemCommand $command): IBasket
    {
        $offerId = new OfferId($command->offerId);
        $days = new BasketItemDays($command->days);
        $duration = new BasketItemDuration($command->duration);
        $viewsPerDay = new BasketItemViewsPerDay($command->viewsPerDay);
        $basket = $this->basketRepository->getByBuyerId(new BuyerId($command->buyerId));
        $items = $basket->getItemCollection();

        $item = $items->get($offerId);
        $clone = clone $item;
        $item->changeDays($days);
        $item->changeDuration($duration);
        $item->changeViewsPerDay($viewsPerDay);

        foreach ($items->getNextItems($item) as $nextItem) {
            if ($nextItem->isEqualParams($clone)) {
                $nextItem->changeDays($days);
                $nextItem->changeDuration($duration);
                $nextItem->changeViewsPerDay($viewsPerDay);
            }
        }

        $basket->recalculate($this->basketGlobalDiscount);
        $this->basketRepository->save($basket);

        return $basket;
    }
}
