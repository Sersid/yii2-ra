<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasket;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketGlobalDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketAlreadyOrderedException;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Service\IBasketServiceProvider;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

class UpdateBasketCommandHandler
{
    private IBasketRepository $basketRepository;
    private IBasketServiceProvider $basketServiceProvider;
    private IBasketGlobalDiscount $basketGlobalDiscount;

    public function __construct(
        IBasketRepository $basketRepository,
        IBasketServiceProvider $basketServiceProvider,
        IBasketGlobalDiscount $basketGlobalDiscount
    ) {
        $this->basketRepository = $basketRepository;
        $this->basketServiceProvider = $basketServiceProvider;
        $this->basketGlobalDiscount = $basketGlobalDiscount;
    }

    public function handle(UpdateBasketCommand $command): IBasket
    {
        $basket = $this->basketServiceProvider->getOrAdd(new BuyerId($command->buyerId));
        if ($basket->getStatus()->isOrdered()) {
            throw new BasketAlreadyOrderedException();
        }
        $advType = $basket->getAdvTypeCollection()->get($command->advType);
        $basket->changeAdvType($advType);
        $basket->recalculate($this->basketGlobalDiscount);
        $this->basketRepository->save($basket);

        return $basket;
    }
}
