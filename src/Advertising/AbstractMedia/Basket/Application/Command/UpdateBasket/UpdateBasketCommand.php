<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\UpdateBasket;

class UpdateBasketCommand
{
    public int $buyerId;
    public int $advType;
}
