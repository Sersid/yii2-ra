<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\DeleteFromBasket;

class DeleteFromBasketCommand
{
    public int $buyerId;
    public int $offerId;
}
