<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Command\DeleteFromBasket;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketGlobalDiscount;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketAlreadyOrderedException;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferRepository;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

class DeleteFromBasketCommandHandler
{
    private IOfferRepository $offerRepository;
    private IBasketRepository $basketRepository;
    private IBasketGlobalDiscount $globalDiscount;

    public function __construct(
        IBasketRepository $basketRepository,
        IOfferRepository $offerRepository,
        IBasketGlobalDiscount $globalDiscount
    ) {
        $this->basketRepository = $basketRepository;
        $this->offerRepository = $offerRepository;
        $this->globalDiscount = $globalDiscount;
    }

    public function handle(DeleteFromBasketCommand $command): IBasket
    {
        $basket = $this->basketRepository->getByBuyerId(new BuyerId($command->buyerId));
        if ($basket->getStatus()->isOrdered()) {
            throw new BasketAlreadyOrderedException();
        }
        $basket->removeItem(new OfferId($command->offerId));
        $basket->recalculate($this->globalDiscount);
        $this->basketRepository->save($basket);

        return $basket;
    }
}
