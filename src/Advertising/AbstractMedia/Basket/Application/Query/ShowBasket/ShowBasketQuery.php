<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Query\ShowBasket;

class ShowBasketQuery
{
    public int $buyerId;
}
