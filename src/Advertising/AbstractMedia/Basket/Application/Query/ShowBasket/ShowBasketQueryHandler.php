<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Basket\Application\Query\ShowBasket;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Service\IBasketServiceProvider;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;

class ShowBasketQueryHandler
{
    private IBasketServiceProvider $basketServiceProvider;

    public function __construct(IBasketServiceProvider $basketServiceProvider) {
        $this->basketServiceProvider = $basketServiceProvider;
    }

    public function handle(ShowBasketQuery $query): IBasket
    {
        $buyerId = new BuyerId($query->buyerId);

        return $this->basketServiceProvider->getOrCreate($buyerId);
    }
}
