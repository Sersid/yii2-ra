<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\City\Application\Query\ShowCities;

use NikoM\Advertising\AbstractMedia\City\Domain\ICityFetcher;
use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;

class ShowCitiesQueryHandler
{
    private ICityFetcher $fetcher;

    public function __construct(ICityFetcher $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    public function handle(): RegionViewCollection
    {
        return $this->fetcher->all();
    }
}
