<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\City\Domain;

use NikoM\Advertising\Kernel\Domain\City\RegionViewCollection;

interface ICityFetcher
{
    public function all(): RegionViewCollection;
}
