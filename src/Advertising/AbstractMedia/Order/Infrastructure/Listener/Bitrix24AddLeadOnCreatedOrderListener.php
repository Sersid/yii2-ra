<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Infrastructure\Listener;

use common\components\Bitrix24;
use NikoM\Advertising\AbstractMedia\Order\Domain\Event\OrderCreatedEvent;
use NikoM\Kernel\Domain\Aggregate\EventListener;
use Yii;

class Bitrix24AddLeadOnCreatedOrderListener implements EventListener
{
    /**
     * @param OrderCreatedEvent $event
     */
    public function handle($event): void
    {
        /** @var  Bitrix24 $bitrix24 */
        $order = $event->getOrder();
        $bitrix24 = Yii::$app->bitrix24;
        $bitrix24->title = 'Заказ на сайте niko-m.ru ('. $order->getType()->getName() .')';
        $bitrix24->name = $order->getName();
        $bitrix24->email = $order->getEmail();
        $bitrix24->phone = $order->getPhone();
        $bitrix24->addLead();
    }
}
