<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Infrastructure\Listener;

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;
use NikoM\Advertising\AbstractMedia\Order\Domain\Event\OrderCreatedEvent;
use NikoM\Kernel\Domain\Aggregate\EventListener;
use Yii;

class SendMailOnCreatedOrderListener implements EventListener
{
    /**
     * @param OrderCreatedEvent $event
     */
    public function handle($event): void
    {
        $this->sendToAdmin($event->getOrder());
        $this->sendToBuyer($event->getOrder());
    }

    private function sendToAdmin(Order $order)
    {
        Yii::$app->mailer->compose(
            ['html' => 'order-media-admin-html', 'text' => 'order-media-admin-text'],
            ['order' => $order]
        )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Новый заказ (' . $order->getType()->getName() . ')')
            ->send();
    }

    private function sendToBuyer(Order $order)
    {
        if (empty($order->getEmail())) {
            return;
        }

        Yii::$app->mailer->compose(
            ['html' => 'order-media-buyer-html', 'text' => 'order-media-buyer-text'],
            ['order' => $order]
        )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])
            ->setTo($order->getEmail())
            ->setSubject('Заказ #' . $order->getNumber() . ' ' . Yii::$app->name)
            ->send();
    }
}
