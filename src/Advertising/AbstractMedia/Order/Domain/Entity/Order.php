<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Domain\Entity;

use JsonSerializable;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasket;
use NikoM\Advertising\AbstractMedia\Order\Domain\Event\OrderCreatedEvent;
use NikoM\Kernel\Domain\Aggregate\AggregateRoot;
use NikoM\Kernel\Domain\Aggregate\EventTrait;
use Webmozart\Assert\Assert;

class Order implements AggregateRoot, JsonSerializable
{
    use EventTrait;

    private OrderId $id;
    private string $name;
    private string $phone;
    private string $email;
    private IBasket $basket;
    private IOrderType $type;

    public function __construct(string $name, string $phone, string $email, IBasket $basket, IOrderType $type)
    {
        $name = trim($name);
        Assert::notEmpty($name, 'Name is required field');
        $this->name = $name;

        $phone = trim($phone);
        Assert::notEmpty($phone, 'Phone is required field');
        $this->phone = $phone;

        $email = trim($email);
        Assert::notEmpty($email, 'Email is required field');
        Assert::email($email);
        $this->email = $email;

        $this->basket = $basket;
        $this->id = OrderId::empty();
        $this->type = $type;

        $this->recordEvent(new OrderCreatedEvent($this));
    }

    public function getId(): OrderId
    {
        return $this->id;
    }

    public function setId(OrderId $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getBasket(): IBasket
    {
        return $this->basket;
    }

    public function getType(): IOrderType
    {
        return $this->type;
    }

    /**
     * Get 6 integer number
     * @return string
     */
    public function getNumber(): string
    {
        return str_repeat('0', 6 - strlen((string)$this->id->getValue())) . $this->id->getValue();
    }

    public function jsonSerialize(): array
    {
        return [
            'number' => $this->getNumber(),
        ];
    }
}
