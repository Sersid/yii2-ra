<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Domain\Entity;

interface IOrderType
{
    public function getName(): string;

    public function getOfferName(): string;
}
