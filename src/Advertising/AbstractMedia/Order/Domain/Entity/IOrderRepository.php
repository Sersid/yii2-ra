<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Domain\Entity;

interface IOrderRepository
{
    public function add(Order $order): void;
}
