<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdNullableValueObject;

class OrderId extends IdNullableValueObject
{
}
