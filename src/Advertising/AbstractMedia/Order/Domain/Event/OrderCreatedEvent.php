<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Domain\Event;

use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;

class OrderCreatedEvent
{
    private Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }
}
