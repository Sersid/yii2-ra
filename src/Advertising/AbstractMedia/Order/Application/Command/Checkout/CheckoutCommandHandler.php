<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Application\Command\Checkout;

use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\IBasketRepository;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Entity\BasketStatus;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketAlreadyOrderedException;
use NikoM\Advertising\AbstractMedia\Basket\Domain\Exception\BasketIsEmptyException;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderRepository;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\IOrderType;
use NikoM\Advertising\AbstractMedia\Order\Domain\Entity\Order;
use NikoM\Advertising\Kernel\Domain\Buyer\BuyerId;
use NikoM\Kernel\Domain\Aggregate\EventDispatcher;

class CheckoutCommandHandler
{
    private IBasketRepository $basketRepository;
    private IOrderRepository $orderRepository;
    private EventDispatcher $dispatcher;
    private IOrderType $type;

    public function __construct(
        IBasketRepository $basketRepository,
        IOrderRepository $orderRepository,
        EventDispatcher $dispatcher,
        IOrderType $type
    )
    {
        $this->basketRepository = $basketRepository;
        $this->orderRepository = $orderRepository;
        $this->dispatcher = $dispatcher;
        $this->type = $type;
    }

    /**
     * @param CheckoutCommand $command
     *
     * @return Order
     * @throws BasketIsEmptyException
     */
    public function handle(CheckoutCommand $command): Order
    {
        $basket = $this->basketRepository->getByBuyerId(new BuyerId($command->buyerId));
        if ($basket->getStatus()->isOrdered()) {
            throw new BasketAlreadyOrderedException();
        }
        if ($basket->getItemCollection()->count() === 0) {
            throw new BasketIsEmptyException();
        }
        $order = new Order($command->name, $command->phone, $command->email, $basket, $this->type);
        $this->orderRepository->add($order);

        $basket->changeStatus(BasketStatus::checkout());
        $this->basketRepository->save($basket);

        $this->dispatcher->dispatch($order->releaseEvents());

        return $order;
    }
}
