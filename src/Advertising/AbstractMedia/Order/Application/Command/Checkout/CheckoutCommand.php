<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Order\Application\Command\Checkout;

class CheckoutCommand
{
    public int $buyerId;
    public string $name;
    public string $phone;
    public string $email;
}
