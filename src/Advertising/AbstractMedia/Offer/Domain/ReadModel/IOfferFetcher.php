<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel;

interface IOfferFetcher
{
    public const LIMIT = 9;

    public function all(Filter $filter, int $offset): OfferViewCollection;
}
