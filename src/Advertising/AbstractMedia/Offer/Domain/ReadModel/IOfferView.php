<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel;

use JsonSerializable;
use NikoM\Advertising\Kernel\ReadModel\ImageView;

interface IOfferView extends JsonSerializable
{
    public function id(): int;

    public function name(): string;

    public function url(): string;

    public function pricePerSec(): ?float;

    public function hasPricePerSec(): bool;

    public function discount(): ?float;

    public function logo(): ImageView;
}
