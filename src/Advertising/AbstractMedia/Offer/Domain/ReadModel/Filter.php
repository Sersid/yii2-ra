<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel;

class Filter
{
    public ?int $cityId;
    public ?string $offerSlug;
}
