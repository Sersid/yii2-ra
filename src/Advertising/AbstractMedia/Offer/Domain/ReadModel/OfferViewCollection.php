<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel;

use JsonSerializable;
use NikoM\Kernel\Domain\Entity\Collection\Collection;

/**
 * @method IOfferView[] all()
 */
class OfferViewCollection extends Collection implements JsonSerializable
{
    private int $total;
    private ?int $offset;

    public function __construct(array $items, int $total, ?int $offset)
    {
        parent::__construct($items);
        $this->total = $total;
        $this->offset = $offset;
    }

    public function jsonSerialize(): array
    {
        $items = [];
        foreach ($this->all() as $offerView) {
            $items[] = [
                'id' => $offerView->id(),
                'name' => $offerView->name(),
                'logo' => ['src' => $offerView->logo()->src(160, 80)],
            ];
        }
        return [
            'items' => $items,
            'count' => $this->count(),
            'total' => $this->total,
            'offset' => $this->offset,
        ];
    }

    protected static function type(): string
    {
        return IOfferView::class;
    }
}
