<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel;

use NikoM\Advertising\AbstractMedia\Offer\Domain\Entity\OfferId;
use NikoM\Advertising\AbstractMedia\Offer\Domain\Exception\OfferNotFoundException;

interface IOfferRepository
{
    /**
     * @param OfferId $offerId
     *
     * @return IOfferView
     * @throws OfferNotFoundException
     */
    public function getById(OfferId $offerId): IOfferView;
}
