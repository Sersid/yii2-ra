<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Domain\Entity;

use NikoM\Kernel\Domain\Entity\ValueObject\Id\IdNullableValueObject;

class OfferId extends IdNullableValueObject
{
}
