<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Domain\Exception;

use DomainException;

class OfferNotFoundException extends DomainException
{
    public function __construct()
    {
        $message = 'Offer not found';
        parent::__construct($message, 404);
    }
}
