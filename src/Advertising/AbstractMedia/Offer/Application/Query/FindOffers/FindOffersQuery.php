<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Application\Query\FindOffers;

class FindOffersQuery
{
    public ?int $cityId;
    public ?string $offerSlug;
    public int $offset = 0;
}
