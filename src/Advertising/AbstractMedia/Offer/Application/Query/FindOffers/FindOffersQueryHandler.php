<?php
declare(strict_types=1);

namespace NikoM\Advertising\AbstractMedia\Offer\Application\Query\FindOffers;

use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\Filter;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\IOfferFetcher;
use NikoM\Advertising\AbstractMedia\Offer\Domain\ReadModel\OfferViewCollection;

class FindOffersQueryHandler
{
    private IOfferFetcher $fetcher;

    public function __construct(IOfferFetcher $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    public function handle(FindOffersQuery $command): OfferViewCollection
    {
        $filter = new Filter();
        $filter->cityId = $command->cityId;
        $filter->offerSlug = $command->offerSlug;

        return $this->fetcher->all($filter, $command->offset);
    }
}
