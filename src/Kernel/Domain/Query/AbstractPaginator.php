<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Query;

abstract class AbstractPaginator implements IPaginator
{
    private iterable $items;

    public function __construct(iterable $items)
    {
        $this->items = $items;
    }

    public function url(int $page): string
    {
        // TODO: Implement url() method.
    }

    public function appends(string $key, ?string $value = null): self
    {
        // TODO: Implement appends() method.
    }

    public function fragment(?string $fragment = null)
    {
        // TODO: Implement fragment() method.
    }

    public function nextPageUrl(): ?string
    {
        // TODO: Implement nextPageUrl() method.
    }

    public function previousPageUrl(): ?string
    {
        // TODO: Implement previousPageUrl() method.
    }

    public function items(): iterable
    {
        return $this->items;
    }

    public function firstItem(): int
    {
        // TODO: Implement firstItem() method.
    }

    public function lastItem(): int
    {
        // TODO: Implement lastItem() method.
    }

    public function perPage(): int
    {
        // TODO: Implement perPage() method.
    }

    public function currentPage(): int
    {
        // TODO: Implement currentPage() method.
    }

    public function hasPages(): bool
    {
        // TODO: Implement hasPages() method.
    }

    public function hasMorePages(): bool
    {
        // TODO: Implement hasMorePages() method.
    }

    public function path(): ?string
    {
        // TODO: Implement path() method.
    }

    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    public function isNotEmpty(): bool
    {
        return !empty($this->items);
    }
}
