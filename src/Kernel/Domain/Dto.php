<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain;

use Webmozart\Assert\Assert;

abstract class Dto
{
    public function hasAllProperties(string ...$names): bool
    {
        Assert::notEmpty($names);
        foreach ($names as $name) {
            if (!$this->hasProperty($name)) {
                return false;
            }
        }

        return true;
    }

    public function hasProperty(string $name): bool
    {
        return array_key_exists($name, get_object_vars($this));
    }

    public function hasAnyProperty(string ...$names): bool
    {
        Assert::notEmpty($names);
        foreach ($names as $name) {
            if ($this->hasProperty($name)) {
                return true;
            }
        }

        return false;
    }
}
