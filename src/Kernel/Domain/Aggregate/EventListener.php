<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Aggregate;

interface EventListener
{
    public function handle($event): void;
}
