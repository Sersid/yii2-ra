<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Aggregate;

trait EventTrait
{
    private array $events = [];

    public function releaseEvents(): array
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }

    protected function recordEvent(object $event): void
    {
        $this->events[] = $event;
    }
}
