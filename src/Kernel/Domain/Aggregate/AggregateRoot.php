<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Aggregate;

interface AggregateRoot
{
    public function releaseEvents(): array;
}
