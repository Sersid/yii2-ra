<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain;

trait FormatPriceTrait
{
    private function formatPrice(?float $price): ?string
    {
        if (is_null($price)) {
            return null;
        } else {
            return number_format($price, 0, ' ', ' ');
        }
    }
}
