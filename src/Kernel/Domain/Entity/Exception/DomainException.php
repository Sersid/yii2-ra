<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\Exception;

abstract class DomainException extends \DomainException
{
    public function __construct()
    {
        parent::__construct($this->message(), $this->code());
    }

    abstract public function message(): string;

    abstract public function code(): int;
}
