<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\Exception;

use InvalidArgumentException;

abstract class AlreadyExistException extends InvalidArgumentException
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
