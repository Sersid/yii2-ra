<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\Exception;

abstract class NotFoundException extends DomainException
{
    public function code(): int
    {
        return 404;
    }
}
