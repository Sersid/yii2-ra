<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\Collection;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Webmozart\Assert\Assert;

abstract class Collection implements Countable, IteratorAggregate, ICollection
{
    protected array $items;

    public function __construct(array $items)
    {
        Assert::allIsInstanceOf($items, static::type());
        $this->items = $items;
    }

    abstract protected static function type(): string;

    /**
     * @param ICollection $collection
     *
     * @return static
     */
    public static function fromDtoCollection(ICollection $collection): self
    {
        $items = [];
        foreach ($collection->all() as $dto) {
            $className = static::type();
            $items[] = new $className($dto);
        }

        return new static($items);
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->all());
    }

    public function all(): array
    {
        return $this->items;
    }

    public function count(): int
    {
        return count($this->all());
    }

    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    public function isNotEmpty(): bool
    {
        return $this->isEmpty() === false;
    }

    public function getFirst()
    {
        reset($this->items);

        return current($this->items);
    }
}
