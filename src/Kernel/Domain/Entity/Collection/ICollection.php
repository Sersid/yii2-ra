<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\Collection;

interface ICollection
{
    public function all(): array;

    public function isEmpty(): bool;

    public function isNotEmpty(): bool;
}
