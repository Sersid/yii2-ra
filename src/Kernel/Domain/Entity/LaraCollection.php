<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;
use Webmozart\Assert\Assert;
use Illuminate\Support\Collection as IlluminateCollection;

abstract class LaraCollection implements Countable, JsonSerializable, ArrayAccess, IteratorAggregate
{
    private IlluminateCollection $items;

    public function __construct(array $items)
    {
        Assert::allIsInstanceOf($items, static::type());
        $this->items = new IlluminateCollection($items);
    }

    abstract protected static function type(): string;

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    public function count(): int
    {
        return $this->items->count();
    }

    public function jsonSerialize(): array
    {
        return [
            'items' => $this->items,
            'count' => $this->count(),
        ];
    }

    public function offsetExists($offset): bool
    {
        return $this->items->offsetExists($offset);
    }

    public function offsetGet($offset)
    {
        return $this->items->offsetGet($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->items->offsetSet($offset, $value);
    }

    public function offsetUnset($offset)
    {
        $this->items->offsetUnset($offset);
    }

    public function all(): array
    {
        return $this->items->all();
    }

    public function first(callable $callback = null, $default = null)
    {
        return $this->items->first($callback, $default);
    }

    public function values(): array
    {
        return $this->items->values()->all();
    }

    public function has($key): bool
    {
        return $this->items->has($key);
    }

    public function add($item): void
    {
        $this->items->add($item);
    }

    public function filter(callable $callback): array
    {
        return $this->items->filter($callback)->all();
    }

    public function countBy($countBy = null): int
    {
        return $this->items->countBy($countBy)->count();
    }

    public function hasBy($countBy = null): bool
    {
        return $this->countBy($countBy) > 0;
    }

    /**
     * @param $value
     *
     * @return false|int
     */
    public function search($value)
    {
        return $this->items->search($value);
    }
}
