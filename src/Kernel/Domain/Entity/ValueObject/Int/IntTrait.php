<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Int;

trait IntTrait
{
    public function getFormatted(): ?string
    {
        if ($this->isNull()) {
            return null;
        } else {
            return number_format($this->getValue(), 0, ' ', ' ');
        }
    }
}
