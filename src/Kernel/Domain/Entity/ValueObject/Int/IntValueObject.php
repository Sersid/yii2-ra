<?php

declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Int;

use NikoM\Kernel\Domain\Entity\ValueObject\ValueObject;

abstract class IntValueObject extends ValueObject
{
    use IntTrait;

    protected int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
