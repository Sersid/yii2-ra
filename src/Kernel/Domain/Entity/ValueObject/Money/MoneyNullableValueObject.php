<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Money;

use NikoM\Kernel\Domain\Entity\ValueObject\Float\FloatNullableValueObject;

abstract class MoneyNullableValueObject extends FloatNullableValueObject
{
    use MoneyTrait;
}
