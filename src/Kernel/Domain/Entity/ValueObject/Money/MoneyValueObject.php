<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Money;

use NikoM\Kernel\Domain\Entity\ValueObject\Float\FloatValueObject;

abstract class MoneyValueObject extends FloatValueObject
{
    use MoneyTrait;
}
