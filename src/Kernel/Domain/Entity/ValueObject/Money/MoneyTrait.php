<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Money;

trait MoneyTrait
{
    public function getFormatted(): ?string
    {
        if ($this->isNull()) {
            return null;
        } else {
            return number_format($this->getValue(), 0, ' ', ' ');
        }
    }
}
