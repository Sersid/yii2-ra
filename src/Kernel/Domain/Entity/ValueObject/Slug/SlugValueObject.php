<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Slug;

use Cocur\Slugify\Slugify;
use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;
use Webmozart\Assert\Assert;

abstract class SlugValueObject extends StringValueObject
{
    public function __construct(string $value)
    {
        $value = trim($value);
        Assert::notEmpty($value);
        parent::__construct($value);
    }

    /**
     * @param string $string
     *
     * @return static
     */
    public static function fromString(string $string): self
    {
        $string = trim($string);
        Assert::notEmpty($string);
        $slugify = new Slugify();
        $slug = $slugify->slugify($string);

        return new static($slug);
    }
}
