<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Sort;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntValueObject;

class SortValueObject extends IntValueObject
{
    public function __construct(?int $value)
    {
        parent::__construct($value ?? 500);
    }
}
