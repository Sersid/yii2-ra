<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\String;

use Illuminate\Support\Str;
use NikoM\Kernel\Domain\Entity\ValueObject\ValueObject;

abstract class BaseStringValueObject extends ValueObject
{
    private function clone(callable $function): self
    {
        $clone = clone $this;
        if ($clone->isNotNull()) {
            $function($clone);
        }

        return $clone;
    }

    public function toUpper(): self
    {
        return $this->clone(function (self $clone) {
            $clone->value = Str::upper($clone->value);
        });
    }

    public function toLower(): self
    {
        return $this->clone(function (self $clone) {
            $clone->value = Str::lower($clone->value);
        });
    }

    public function e(): self
    {
        return $this->clone(function (self $clone) {
            $clone->value = e($this->value);
        });
    }

    public function trim(): self
    {
        return $this->clone(function (self $clone) {
            $clone->value = trim($clone->value);
        });
    }
}
