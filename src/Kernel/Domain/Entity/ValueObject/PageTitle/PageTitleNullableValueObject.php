<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\PageTitle;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringNullableValueObject;

abstract class PageTitleNullableValueObject extends StringNullableValueObject
{
}
