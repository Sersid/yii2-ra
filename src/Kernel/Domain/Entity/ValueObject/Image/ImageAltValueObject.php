<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Image;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringNullableValueObject;

class ImageAltValueObject extends StringNullableValueObject
{
    public function __construct(?string $value)
    {
        parent::__construct($value);
        $this->value = $this->trim()->e()->getValue();
    }
}
