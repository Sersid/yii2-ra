<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Image;

use NikoM\Kernel\Domain\Entity\ValueObject\String\StringNullableValueObject;
use NikoM\Kernel\Domain\Entity\ValueObject\String\StringValueObject;
use Webmozart\Assert\Assert;

class ImageValueObject extends StringValueObject
{
    private StringNullableValueObject $alt;

    public function __construct(string $src, ?string $alt = null)
    {
        parent::__construct($src);
        $this->trim();
        Assert::notEmpty($this->value);
        $this->alt = new ImageAltValueObject($alt);
    }

    public function getAlt()
    {
        return $this->alt;
    }
}
