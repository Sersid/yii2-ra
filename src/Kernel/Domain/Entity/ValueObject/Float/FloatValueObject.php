<?php

declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Float;

use NikoM\Kernel\Domain\Entity\ValueObject\ValueObject;

abstract class FloatValueObject extends ValueObject
{
    use FloatTrait;

    protected float $value;

    public function __construct(float $value)
    {
        $this->value = $value;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
