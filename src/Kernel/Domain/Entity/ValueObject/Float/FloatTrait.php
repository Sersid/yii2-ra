<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Float;

use function floor;

trait FloatTrait
{
    public function floor(): void
    {
        if ($this->isNotNull()) {
            $this->value = floor($this->value);
        }
    }
}
