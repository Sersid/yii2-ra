<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject;

use JsonSerializable;

abstract class ValueObject implements JsonSerializable
{
    abstract public function getValue();

    public function __toString(): string
    {
        return (string)$this->getValue();
    }

    public function jsonSerialize()
    {
        return $this->getValue();
    }

    public function isEqual(self $other): bool
    {
        if ($this->isNull() && $other->isNull()) {
            return true;
        }
        if ($this->isNotNull() && $other->isNotNull() && $this->getValue() === $other->getValue()) {
            return true;
        }

        return false;
    }

    public function isNull(): bool
    {
        return is_null($this->getValue());
    }

    public function isNotNull(): bool
    {
        return !$this->isNull();
    }

    public function isEmpty(): bool
    {
        return $this->isNull() || empty($this->getValue());
    }

    public function isNotEmpty(): bool
    {
        return !$this->isEmpty();
    }
}
