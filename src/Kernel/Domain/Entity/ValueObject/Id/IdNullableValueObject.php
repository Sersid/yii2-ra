<?php
declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Id;

use NikoM\Kernel\Domain\Entity\ValueObject\Int\IntNullableValueObject;
use Webmozart\Assert\Assert;

abstract class IdNullableValueObject extends IntNullableValueObject
{
    public function __construct(?int $value)
    {
        if (!is_null($value)) {
            Assert::greaterThan($value, 0);
        }
        parent::__construct($value);
    }

    /**
     * @return static
     */
    public static function empty()
    {
        return new static(null);
    }
}
