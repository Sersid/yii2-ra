<?php

declare(strict_types=1);

namespace NikoM\Kernel\Domain\Entity\ValueObject\Bool;

use NikoM\Kernel\Domain\Entity\ValueObject\ValueObject;

abstract class BoolValueObject extends ValueObject
{
    protected bool $value;

    public function __construct(bool $value)
    {
        $this->value = $value;
    }

    public function getValue(): bool
    {
        return $this->value;
    }

    public function isTrue(): bool
    {
        return $this->getValue() === true;
    }

    public function isFalse(): bool
    {
        return !$this->isTrue();
    }
}
