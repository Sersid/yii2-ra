<?php
declare(strict_types=1);

namespace NikoM\Kernel\Infrastructure\Aggregate;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;
use Psr\EventDispatcher\StoppableEventInterface;
use Yii;

class EventDispatcher implements EventDispatcherInterface
{
    private ListenerProviderInterface $listenerProvider;

    public function __construct(ListenerProviderInterface $listenerProvider)
    {
        $this->listenerProvider = $listenerProvider;
    }

    public function dispatch(object $event)
    {
        if ($event instanceof StoppableEventInterface && $event->isPropagationStopped()) {
            return $event;
        }
        foreach ($this->listenerProvider->getListenersForEvent($event) as $listener) {
            $handler = Yii::$container->get($listener);
            if ($handler instanceof StoppableEventInterface && $handler->isPropagationStopped()) {
                break;
            }
            $handler->handle($event);
        }

        return $event;
    }
}
