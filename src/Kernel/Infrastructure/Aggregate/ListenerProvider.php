<?php
declare(strict_types=1);

namespace NikoM\Kernel\Infrastructure\Aggregate;

use Psr\EventDispatcher\ListenerProviderInterface;
use Yii;

class ListenerProvider implements ListenerProviderInterface
{
    private array $listeners;

    public function __construct()
    {
        $this->listeners = Yii::$app->params['listeners'] ?? [];
    }

    public function getListenersForEvent(object $event): iterable
    {
        $className = get_class($event);
        if (!empty($className) && isset($this->listeners[$className]) && is_array($this->listeners[$className])) {
            return $this->listeners[$className];
        } else {
            return [];
        }
    }
}
