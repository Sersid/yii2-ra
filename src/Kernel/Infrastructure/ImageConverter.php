<?php
declare(strict_types=1);

namespace NikoM\Kernel\Infrastructure;

use NikoM\Advertising\Kernel\DTO\ImageDto;

class ImageConverter
{

    public function fromArrayToDTO(array $data): ImageDto
    {
        $dto = new ImageDto();
        if (array_key_exists('id', $data)) {
            $dto->id = (int)$data['id'];
        }
        if (array_key_exists('code', $data)) {
            $dto->code = $data['code'];
        }
        if (array_key_exists('ext', $data)) {
            $dto->ext = $data['ext'];
        }
        if (array_key_exists('width', $data)) {
            $dto->width = (int)$data['width'];
        }
        if (array_key_exists('height', $data)) {
            $dto->height = (int)$data['height'];
        }
        if (array_key_exists('alt', $data)) {
            $dto->alt = $data['alt'];
        }
        if (array_key_exists('title', $data)) {
            $dto->title = $data['title'];
        }

        return $dto;
    }
}
