install: perm docker-build composer-install perm migrate
docker-build:
	cd laradock && docker-compose up --build -d nginx mariadb
docker-up:
	cd laradock && docker-compose up -d nginx mariadb
docker-down:
	cd laradock && docker-compose down
docker-restart: docker-down docker-up
bash:
	cd laradock && docker-compose exec workspace bash
npm-watch:
	cd laradock && docker-compose exec workspace npm run development -- --watch
perm:
	sudo chown ${USER}:${USER} ./* -R
composer-install:
	cd laradock && docker-compose exec workspace composer install
migrate:
	cd laradock && docker-compose exec workspace php yii migrate
