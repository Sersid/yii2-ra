Niko-M
===============================
Запуск в Docker
-------------------
1) Клонирование проекта
    ```
    git clone https://Sersid@bitbucket.org/Sersid/yii2-ra.git
    ```
   При необходимости обновить модули
   ```
   git submodule update
   ```
2) Скопировать `laradock/env-example` в `laradock/.env`
3) В `.env` изменить параметры:
    ```
    NGINX_SITES_PATH=../docker/nginx/sites/
    MARIADB_DATABASE=niko-m
    ```
4) Запустить докер
    ```
    make docker-build
    ```
5) Перейти в `workspace` контейнер
    ```
    make bash
    ```
6) Через контейнер `workspace` установить проект
     ```
     composer install
     php yii migrate
     ```

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```
