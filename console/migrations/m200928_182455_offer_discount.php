<?php

use yii\db\Migration;

/**
 * Class m200928_182455_offer_discount
 */
class m200928_182455_offer_discount extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tv_ad}}', 'discount', $this->money()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tv_ad}}', 'discount');
    }
}
