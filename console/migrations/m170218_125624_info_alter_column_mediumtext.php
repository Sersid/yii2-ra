<?php

use yii\db\Migration;

class m170218_125624_info_alter_column_mediumtext extends Migration
{
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql')
        {
            Yii::$app->db->createCommand("ALTER TABLE `info` MODIFY COLUMN `full_desc` mediumtext");
        }
        else
        {
            echo 'БД:' . $this->db->driverName . ', не поддерживает тип MEDIUMTEXT\n';
        }
    }

    public function safeDown()
    {
        $this->alterColumn('{{%info}}', 'full_desc', $this->text());
    }
}
