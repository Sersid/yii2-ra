<?php

use yii\db\Migration;

class m170523_030946_cities extends Migration
{
    public function up()
    {
        $this->addColumn('{{%city}}', 'title_outdoor_ad', $this->string()->null());
        $this->addColumn('{{%city}}', 'title_tv', $this->string()->null());
        $this->addColumn('{{%city}}', 'title_ticker', $this->string()->null());
        $this->addColumn('{{%city}}', 'title_radio', $this->string()->null());
    }

    public function down()
    {
        $this->dropColumn('{{%city}}', 'title_outdoor_ad');
        $this->dropColumn('{{%city}}', 'title_tv');
        $this->dropColumn('{{%city}}', 'title_ticker');
        $this->dropColumn('{{%city}}', 'title_radio');
    }
}
