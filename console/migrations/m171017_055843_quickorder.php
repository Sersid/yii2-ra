<?php

use yii\db\Migration;

class m171017_055843_quickorder extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'model_type', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'model_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171017_055843_quickorder cannot be reverted.\n";

        return false;
    }
    */
}
