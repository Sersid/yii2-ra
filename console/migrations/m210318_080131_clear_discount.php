<?php
declare(strict_types=1);

use yii\db\Migration;

class m210318_080131_clear_discount extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('{{%tv_ad}}', ['discount' => null]);
        $this->update('{{%radio_ad}}', ['discount' => null]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('{{%tv_ad}}', ['discount' => 20]);
        $this->update('{{%radio_ad}}', ['discount' => 20]);
    }
}
