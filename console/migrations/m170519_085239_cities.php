<?php

use yii\db\Migration;

class m170519_085239_cities extends Migration
{
    public function up()
    {
        $this->addColumn('{{%city}}', 'slug', $this->string()->notNull());
        $this->createIndex('city_slug', '{{%city}}', 'slug');
    }

    public function down()
    {
        $this->dropColumn('{{%city}}', 'slug');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
