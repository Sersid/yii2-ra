<?php

use yii\db\Migration;

class m161017_020651_ticker extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ticker}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull()->comment('City'),
            'tv_id' => $this->integer()->notNull()->comment('TV'),
            'price_comm' => $this->money()->notNull()->comment('Commercial'),
            'price_no_comm' => $this->money()->notNull()->comment('Non-profit'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('ticker_sort', '{{%ticker}}', 'sort');
        $this->createIndex('ticker_status', '{{%ticker}}', 'status');
        $this->createIndex('ticker_city_id', '{{%ticker}}', 'city_id');
        $this->createIndex('ticker_tv_id', '{{%ticker}}', 'tv_id');
        $this->createIndex('ticker_tv_id_city_id', '{{%ticker}}', ['city_id', 'tv_id'], true);
        $this->addForeignKey('ticker_city', '{{%ticker}}', 'city_id', '{{%city}}', 'id');
        $this->addForeignKey('ticker_tv', '{{%ticker}}', 'tv_id', '{{%tv}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('ticker_city','{{%ticker}}');
        $this->dropForeignKey('ticker_tv','{{%ticker}}');
        $this->dropTable('{{%ticker}}');
    }
}
