<?php

use yii\db\Migration;

class m170424_043501_update_foreign extends Migration
{
    public function up()
    {
        // Cases
        $this->dropForeignKey('case_image','{{%adv_case}}');
        $this->dropForeignKey('case_city','{{%adv_case}}');
        $this->alterColumn('{{%adv_case}}', 'image_id', $this->integer()->null());
        $this->alterColumn('{{%adv_case}}', 'city_id', $this->integer()->null());
        $this->addForeignKey('case_image', '{{%adv_case}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');
        $this->addForeignKey('case_city', '{{%adv_case}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');

        // Cities
        $this->dropForeignKey('city_image','{{%city}}');
        $this->addForeignKey('city_image', '{{%city}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');

        // Orders
        $this->dropForeignKey('order_guest_id','{{%order}}');
        $this->alterColumn('{{%order}}', 'guest_id', $this->integer()->null());
        $this->addForeignKey('order_guest_id', '{{%order}}', 'guest_id', '{{%guest}}', 'id', 'SET NULL', 'SET NULL');

        // Outdoor ad
        $this->dropForeignKey('outdoor_ad_city','{{%outdoor_ad}}');
        $this->dropForeignKey('outdoor_ad_street','{{%outdoor_ad}}');
        $this->alterColumn('{{%outdoor_ad}}', 'city_id', $this->integer()->null());
        $this->alterColumn('{{%outdoor_ad}}', 'street_id', $this->integer()->null());
        $this->addForeignKey('outdoor_ad_city', '{{%outdoor_ad}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');
        $this->addForeignKey('outdoor_ad_street', '{{%outdoor_ad}}', 'street_id', '{{%street}}', 'id', 'SET NULL', 'SET NULL');

        // Outdoor ad sides
        $this->dropForeignKey('outdoor_ad_side_image','{{%outdoor_ad_side}}');
        $this->alterColumn('{{%outdoor_ad_side}}', 'image_id', $this->integer()->null());
        $this->addForeignKey('outdoor_ad_side_image', '{{%outdoor_ad_side}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');

        // Radios
        $this->dropForeignKey('radio_image','{{%radio}}');
        $this->alterColumn('{{%radio}}', 'image_id', $this->integer()->null());
        $this->addForeignKey('radio_image', '{{%radio}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');

        // Radio ads
        $this->dropForeignKey('radio_ad_city','{{%radio_ad}}');
        $this->dropForeignKey('radio_ad_radio','{{%radio_ad}}');
        $this->alterColumn('{{%radio_ad}}', 'city_id', $this->integer()->null());
        $this->alterColumn('{{%radio_ad}}', 'radio_id', $this->integer()->null());
        $this->addForeignKey('radio_ad_city', '{{%radio_ad}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');
        $this->addForeignKey('radio_ad_radio', '{{%radio_ad}}', 'radio_id', '{{%radio}}', 'id', 'SET NULL', 'SET NULL');

        // Reviews
        $this->dropForeignKey('review_image','{{%review}}');
        $this->alterColumn('{{%review}}', 'image_id', $this->integer()->null());
        $this->addForeignKey('review_image', '{{%review}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');

        // Tickers
        $this->dropForeignKey('ticker_city','{{%ticker}}');
        $this->alterColumn('{{%ticker}}', 'city_id', $this->integer()->null());
        $this->addForeignKey('ticker_city', '{{%ticker}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');

        // Tvs
        $this->dropForeignKey('tv_image','{{%tv}}');
        $this->alterColumn('{{%tv}}', 'image_id', $this->integer()->null());
        $this->addForeignKey('tv_image', '{{%tv}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');

        // Tv ads
        $this->dropForeignKey('tv_ad_city','{{%tv_ad}}');
        $this->alterColumn('{{%tv_ad}}', 'city_id', $this->integer()->null());
        $this->addForeignKey('tv_ad_city', '{{%tv_ad}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');

        // User radio ads
        $this->dropForeignKey('user_radio_ad_city','{{%user_radio_ad}}');
        $this->alterColumn('{{%user_radio_ad}}', 'city_id', $this->integer()->null());
        $this->addForeignKey('user_radio_ad_city', '{{%user_radio_ad}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');

        // User tickers
        $this->dropForeignKey('user_ticker_city','{{%user_ticker}}');
        $this->alterColumn('{{%user_ticker}}', 'city_id', $this->integer()->null());
        $this->addForeignKey('user_ticker_city', '{{%user_ticker}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');

        // User tv ads
        $this->dropForeignKey('user_tv_ad_city','{{%user_tv_ad}}');
        $this->alterColumn('{{%user_tv_ad}}', 'city_id', $this->integer()->null());
        $this->addForeignKey('user_tv_ad_city', '{{%user_tv_ad}}', 'city_id', '{{%city}}', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        echo "m170424_043501_update_foreign cannot be reverted.\n";
        return false;
    }
}
