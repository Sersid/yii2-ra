<?php

use yii\db\Migration;

class m170207_011459_outdoor_ad extends Migration
{
    public function up()
    {
        $this->addColumn('{{%outdoor_ad}}', 'number', $this->string()->null()->after('id'));
        $this->createIndex('outdoor_ad_number','{{%outdoor_ad}}', 'number', true);
        $this->dropIndex('outdoor_ad_side_number', '{{%outdoor_ad_side}}');
        $this->dropColumn('{{%outdoor_ad_side}}', 'number');
    }

    public function down()
    {
        $this->dropIndex('outdoor_ad_number','{{%outdoor_ad}}');
        $this->dropColumn('{{%outdoor_ad}}', 'number');
        $this->addColumn('{{%outdoor_ad_side}}', 'number', $this->string()->null()->after('id'));
        $this->createIndex('outdoor_ad_side_number', '{{%outdoor_ad_side}}', 'number', true);
    }
}
