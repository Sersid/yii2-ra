<?php
declare(strict_types=1);

use yii\db\Migration;

/**
 * Class m210318_102227_tv_channel_color
 */
class m210318_102227_tv_channel_color extends Migration
{
    private string $table = '{{%tv}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'color', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'color');
    }
}
