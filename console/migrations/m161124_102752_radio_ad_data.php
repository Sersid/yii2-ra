<?php

use yii\db\Migration;

class m161124_102752_radio_ad_data extends Migration
{
    public function up()
    {
        $sql = <<<SQL
INSERT INTO {{%radio_ad}} (`id`, `city_id`, `radio_id`, `price_ad_sec`, `price_ad_show`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '100.0000', '100.0000', 1, 500, 1479983152, 1479983152),
(2, 1, 2, '150.0000', '150.0000', 1, 500, 1479983161, 1479983161),
(3, 1, 3, '200.0000', '100.0000', 1, 500, 1479983168, 1479983168),
(4, 1, 4, '100.0000', '100.0000', 1, 500, 1479983173, 1479983173),
(5, 1, 5, '100.0000', '100.0000', 1, 500, 1479983181, 1479983181),
(6, 1, 6, '300.0000', '300.0000', 1, 500, 1479983189, 1479983189),
(7, 1, 7, '250.0000', '100.0000', 1, 500, 1479983198, 1479983198),
(8, 1, 8, '200.0000', '300.0000', 1, 500, 1479983204, 1479983204),
(9, 2, 1, '100.0000', '100.0000', 1, 500, 1479983213, 1479983213);
SQL;
        $this->db->createCommand($sql)->execute();
    }

    public function down()
    {
        $sql = <<<SQL
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE {{%radio_ad}};
SET FOREIGN_KEY_CHECKS = 1;
SQL;
        $this->db->createCommand($sql)->execute();
    }
}
