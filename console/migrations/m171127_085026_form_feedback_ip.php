<?php

use yii\db\Migration;

class m171127_085026_form_feedback_ip extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%form_feedback}}', 'ip', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%form_feedback}}', 'ip');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171127_085026_form_feedback_ip cannot be reverted.\n";

        return false;
    }
    */
}
