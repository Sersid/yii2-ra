<?php

use yii\db\Migration;

class m170630_064654_tv extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tv}}', 'slug', $this->string()->notNull());
        $this->createIndex('tv_slug', '{{%tv}}', 'slug');
    }

    public function down()
    {
        $this->dropColumn('{{%tv}}', 'slug');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170630_064654_tv cannot be reverted.\n";

        return false;
    }
    */
}
