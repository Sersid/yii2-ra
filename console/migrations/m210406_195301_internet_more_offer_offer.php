<?php
declare(strict_types=1);

use yii\db\Migration;

class m210406_195301_internet_more_offer_offer extends Migration
{
    private string $table = '{{%internet_offer_more_offer}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'offer_id' => $this->integer()->notNull(),
                'more_offer_id' => $this->integer()->notNull(),
            ]
        );

        $this->addPrimaryKey(
            'internet_offer_more_offer_primary',
            $this->table,
            ['offer_id', 'more_offer_id']
        );

        $this->insert($this->table, [
            'offer_id' => 1,
            'more_offer_id' => 1,
        ]);
        $this->insert($this->table, [
            'offer_id' => 1,
            'more_offer_id' => 2,
        ]);
        $this->insert($this->table, [
            'offer_id' => 1,
            'more_offer_id' => 3,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
