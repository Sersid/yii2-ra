<?php

use yii\db\Migration;

class m161027_032221_streets_data extends Migration
{
    public function up()
    {
        $sql = <<<SQL
INSERT INTO {{%street}} (`id`, `name`, `city_id`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'ул. Ленина', 1, 1, 500, 1477538367, 1477538367),
(2, 'ул. Университетская', 1, 1, 500, 1477538380, 1477538380),
(3, 'ул. Чехова', 1, 1, 500, 1477538498, 1477538498),
(4, 'ул. Победы', 2, 1, 500, 1477538513, 1477538513);
SQL;
        $this->db->createCommand($sql)->execute();
    }

    public function down()
    {
        $sql = <<<SQL
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE {{%street}};
SET FOREIGN_KEY_CHECKS = 1;
SQL;
        $this->db->createCommand($sql)->execute();
    }
}
