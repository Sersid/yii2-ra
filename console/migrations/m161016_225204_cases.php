<?php

use yii\db\Migration;

class m161016_225204_cases extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%adv_case}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'image_id' => $this->integer()->notNull()->comment('Image'),
            'short_desc' => $this->text()->notNull()->comment('Short description'),
            'full_desc' => $this->text()->comment('Full description'),
            'product' => $this->string(),
            'customer' => $this->string()->comment('Customer Portrait'),
            'city_id' => $this->integer(),
            'purpose' => $this->string(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('case_sort', '{{%adv_case}}', 'sort');
        $this->createIndex('case_status', '{{%adv_case}}', 'status');

        $this->createIndex('case_slug', '{{%adv_case}}', 'slug', true);

        $this->createIndex('case_image_id', '{{%adv_case}}', 'image_id');
        $this->addForeignKey('case_image', '{{%adv_case}}', 'image_id', '{{%file}}', 'id');

        $this->createIndex('case_city_id', '{{%adv_case}}', 'city_id');
        $this->addForeignKey('case_city', '{{%adv_case}}', 'city_id', '{{%city}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('case_image','{{%adv_case}}');
        $this->dropForeignKey('case_city','{{%adv_case}}');
        $this->dropTable('{{%adv_case}}');
    }
}
