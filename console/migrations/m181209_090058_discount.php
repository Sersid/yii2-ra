<?php

use yii\db\Migration;

/**
 * Class m181209_090058_discount
 */
class m181209_090058_discount extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%outdoor_ad_side}}', 'discount', $this->money()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%outdoor_ad_side}}', 'discount');
    }
}
