<?php

use yii\db\Migration;

class m161122_024608_outdoordata extends Migration
{
    public function up()
    {
        $sql = <<<SQL
INSERT INTO {{%outdoor_ad}} (`id`, `city_id`, `street_id`, `address`, `format`, `size`, `sides`, `lat`, `lon`, `rotate`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'д. 12', 10, '3x6\r\n\r\n', 2, '61.26724600', '73.38816600', 30, 1, 500, 1479700400, 1479700400),
(2, 2, 4, NULL, 20, '3x6', 2, '61.27365100', '73.38069900', 29, 1, 500, 1479700400, 1479700400);

INSERT INTO {{%outdoor_ad_side}} (`id`, `parent_id`, `number`, `side`, `image_id`, `price`, `price_installation`, `price_print`, `price_design`, `rating`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '03.204.A', 1, 11, '45000.0000', '4400.0000', '5400.0000', '5000.0000', 3, 1, 1479700400, 1479700400),
(2, 1, '03.204.Б', 2, 12, '45000.0000', '4400.0000', '5400.0000', '5000.0000', 2, 1, 1479700400, 1479700400),
(3, 2, '02.204.А', 1, 13, '10000.0000', '2000.0000', '5000.0000', '1000.0000', 2, 1, 1479700400, 1479700400);

INSERT INTO {{%outdoor_ad_side_busy}} (`side_id`, `month`, `year`) VALUES
(1, 1, 2016),
(1, 2, 2016);
SQL;
        $this->db->createCommand($sql)->execute();
    }

    public function down()
    {
        $sql = <<<SQL
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE {{%outdoor_ad}};
TRUNCATE {{%outdoor_ad_side}};
TRUNCATE {{%outdoor_ad_side_busy}};
SET FOREIGN_KEY_CHECKS = 1;
SQL;
        $this->db->createCommand($sql)->execute();
    }
}
