<?php

use yii\db\Migration;

class m170620_054431_city extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addColumn('{{%city}}', 'region_id', $this->integer()->null());
        $this->createIndex('city_region_id', '{{%city}}', 'region_id');
        $this->addForeignKey('city_region', '{{%city}}', 'region_id', '{{%region}}', 'id', 'SET NULL', 'SET NULL');

        $this->addColumn('{{%city}}', 'phone', $this->text()->null());
        $this->addColumn('{{%city}}', 'contact_text', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropForeignKey('city_region','{{%city}}');
        $this->dropColumn('{{%city}}', 'region_id');
        $this->dropTable('{{%region}}');
        $this->dropColumn('{{%city}}', 'phone');
        $this->dropColumn('{{%city}}', 'contact_text');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170620_054431_city cannot be reverted.\n";

        return false;
    }
    */
}
