<?php

use yii\db\Migration;

class m170113_040621_ticker_change extends Migration
{
    public function up()
    {
        $this->dropIndex('ticker_tv_id_city_id','{{%ticker}}');
        $this->addColumn('{{%ticker}}', 'price_text_block', $this->decimal(19,4)->null()->after('price_no_comm'));
        $this->addColumn('{{%ticker}}', 'type', $this->smallInteger()->notNull()->after('tv_id'));
        $this->addColumn('{{%ticker}}', 'weekend', $this->boolean());
        $this->addColumn('{{%ticker}}', 'x2', $this->boolean());
        $this->addColumn('{{%ticker}}', 'cnt_shows', $this->integer()->notNull());
        $this->alterColumn('{{%ticker}}', 'price_comm', $this->decimal(19,4)->null());
        $this->alterColumn('{{%ticker}}', 'price_no_comm', $this->decimal(19,4)->null());
        $this->delete('{{%user_ticker_phone}}');
        $this->delete('{{%user_ticker_tv}}');
        $this->delete('{{%user_ticker}}');
        $this->delete('{{%ticker}}');
    }

    public function down()
    {
        $this->createIndex('ticker_tv_id_city_id', '{{%ticker}}', ['city_id', 'tv_id'], true);
        $this->dropColumn('{{%ticker}}', 'price_text_block');
        $this->dropColumn('{{%ticker}}', 'type');
        $this->dropColumn('{{%ticker}}', 'weekend');
        $this->dropColumn('{{%ticker}}', 'x2');
        $this->dropColumn('{{%ticker}}', 'cnt_shows');
        $this->alterColumn('{{%ticker}}', 'price_comm', $this->decimal(19,4)->notNull());
        $this->alterColumn('{{%ticker}}', 'price_no_comm', $this->decimal(19,4)->notNull());
    }
}
