<?php
declare(strict_types=1);

use yii\db\Migration;

class m210317_085345_tv_channel_banners extends Migration
{
    private string $table = '{{%tv}}';


    public function safeUp()
    {
        $this->addColumn($this->table, 'big_logo', $this->integer()->null());
        $this->addColumn($this->table, 'big_banner', $this->integer()->null());

        $this->createIndex('tv_big_logo', '{{%tv}}', 'big_logo');
        $this->createIndex('tv_big_banner', '{{%tv}}', 'big_banner');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'big_banner');
        $this->dropColumn($this->table, 'big_logo');
    }
}
