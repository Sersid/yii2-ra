<?php

use yii\db\Migration;

class m170726_012916_busy extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%outdoor_ad_side_busy}}', 'status', $this->integer()->notNull()->defaultValue(10));
        $this->createIndex('outdoor_ad_side_busy_status', '{{%outdoor_ad_side_busy}}', 'status');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%outdoor_ad_side_busy}}', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170726_012916_busy cannot be reverted.\n";

        return false;
    }
    */
}
