<?php
declare(strict_types=1);

use yii\db\Migration;

/**
 * Class m200928_185906_user_tv_ad_prices
 */
class m200928_185906_user_tv_ad_prices extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_tv_ad_tv}}', 'price_without_discount', $this->money()->null());
        $this->addColumn('{{%user_tv_ad_tv}}', 'price', $this->money()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_tv_ad_tv}}', 'price_without_discount');
        $this->dropColumn('{{%user_tv_ad_tv}}', 'price');
    }
}
