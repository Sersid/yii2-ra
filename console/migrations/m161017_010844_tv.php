<?php

use yii\db\Migration;

class m161017_010844_tv extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tv}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_id' => $this->integer()->comment('Logo'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('tv_sort', '{{%tv}}', 'sort');
        $this->createIndex('tv_status', '{{%tv}}', 'status');
        $this->createIndex('tv_image_id', '{{%tv}}', 'image_id');
        $this->addForeignKey('tv_image', '{{%tv}}', 'image_id', '{{%file}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tv_image','{{%tv}}');
        $this->dropTable('{{%tv}}');
    }
}
