<?php

use yii\db\Migration;

class m170227_130638_change_length_name_to_book extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%book}}', 'name', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%book}}', 'name', $this->string(60));
    }
}
