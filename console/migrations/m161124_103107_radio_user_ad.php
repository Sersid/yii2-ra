<?php

use yii\db\Migration;

class m161124_103107_radio_user_ad extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_radio_ad}}', [
            'id' => $this->primaryKey(),
            'guest_id' => $this->integer()->notNull()->comment('Guest'),
            'type' => $this->smallInteger()->notNull(),
            'city_id' => $this->integer()->comment('City'),
            'show_all_cities' => $this->boolean()->defaultValue(0),

            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('user_radio_ad_status', '{{%user_radio_ad}}', 'status');

        $this->createIndex('user_radio_ad_guest_id', '{{%user_radio_ad}}', 'guest_id');
        $this->addForeignKey('user_radio_ad_guest', '{{%user_radio_ad}}', 'guest_id', '{{%guest}}', 'id');

        $this->createIndex('user_radio_ad_city_id', '{{%user_radio_ad}}', 'city_id');
        $this->addForeignKey('user_radio_ad_city', '{{%user_radio_ad}}', 'city_id', '{{%city}}', 'id');

        $this->createTable('{{%user_radio_ad_radio}}', [
            'user_radio_ad_id' => $this->integer()->notNull(),
            'radio_ad_id' => $this->integer()->notNull(),
            'duration' => $this->integer(),
            'views' => $this->integer(),
            'days' => $this->integer(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('user_radio_ad_radio_primary','{{%user_radio_ad_radio}}',['user_radio_ad_id', 'radio_ad_id']);
        $this->addForeignKey('user_radio_ad_radio_user_radio_ad', '{{%user_radio_ad_radio}}', 'user_radio_ad_id', '{{%user_radio_ad}}', 'id');
        $this->addForeignKey('user_tv_ad_radio_radio_ad', '{{%user_radio_ad_radio}}', 'radio_ad_id', '{{%radio_ad}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('user_radio_ad_radio_user_radio_ad', '{{%user_radio_ad_radio}}');
        $this->dropForeignKey('user_radio_ad_radio_radio_ad', '{{%user_radio_ad_radio}}');
        $this->dropTable('{{%user_radio_ad_radio}}');


        $this->dropForeignKey('user_radio_ad_guest','{{%user_radio_ad}}');
        $this->dropForeignKey('user_radio_ad_city','{{%user_radio_ad}}');
        $this->dropTable('{{%user_radio_ad}}');
    }
}
