<?php

use yii\db\Migration;

class m170328_132514_busy_status extends Migration
{
    public function up()
    {
        $this->addColumn('{{%outdoor_ad_side}}', 'busy_status', $this->boolean()->notNull()->comment('Busy status')->defaultValue(10));
        $this->createIndex('outdoor_ad_side_busy_status', '{{%outdoor_ad_side}}', 'busy_status');
    }

    public function down()
    {
        echo "m170328_132514_busy_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
