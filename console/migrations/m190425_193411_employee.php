<?php

use yii\db\Migration;

/**
 * Class m190425_193411_employee
 */
class m190425_193411_employee extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%employee}}',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'position' => $this->string()->notNull(),
                'photo_id' => $this->integer()->notNull(),
                'status' => $this->integer()->notNull(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex('employee_photo_id', '{{%employee}}', 'photo_id');

        $this->addForeignKey('employee_photo_id', '{{%employee}}', 'photo_id', '{{%file}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('employee_photo_id', '{{%employee}}');
        $this->dropTable('{{%employee}}');
    }
}
