<?php
declare(strict_types=1);

use yii\db\Migration;

class m210406_195010_internet_more_offer extends Migration
{
    private string $table = '{{%internet_more_offer}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'icon' => $this->string()->notNull(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert($this->table, [
            'name' => 'Ведение рекламных кампаний',
            'icon' => 'offers_13.svg',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
        ]);

        $this->insert($this->table, [
            'name' => 'Ретаргетинг/ремаркетинг',
            'icon' => 'offers_14.svg',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
        ]);

        $this->insert($this->table, [
            'name' => 'Смарт-баннер Яндекс Директ',
            'icon' => 'offers_15.svg',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
