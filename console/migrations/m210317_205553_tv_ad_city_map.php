<?php
declare(strict_types=1);

use yii\db\Migration;

class m210317_205553_tv_ad_city_map extends Migration
{
    private string $table = '{{%tv_ad}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'city_map', $this->integer()->null());
        $this->createIndex('tv_ad_city_map', $this->table, 'city_map');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'city_map');
    }
}
