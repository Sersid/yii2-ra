<?php

use yii\db\Migration;

class m170425_110229_pages extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'content' => $this->text(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('page_slug', '{{%page}}', 'slug', true);
        $this->createIndex('page_status', '{{%page}}', 'status');
    }

    public function down()
    {
        $this->dropTable('{{%page}}');
    }
}
