<?php
declare(strict_types=1);

use yii\db\Migration;

class m210330_182434_internet_offer extends Migration
{
    private string $table = '{{%internet_offer}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'icon' => $this->string()->notNull(),
            'intro_text' => $this->text()->null(),
            'page_title' => $this->string()->null(),
            'page_heading' => $this->string()->null(),
            'page_description' => $this->text()->null(),
            'page_title_with_city' => $this->string()->null(),
            'page_heading_with_city' => $this->string()->null(),
            'page_description_with_city' => $this->text()->null(),
            'tariff_title' => $this->string()->null(),
            'tariff_description' => $this->text()->null(),
            'tariff_low_item_title' => $this->string()->null(),
            'tariff_low_item_description' => $this->text()->null(),
            'tariff_low_item_price' => $this->float()->null(),
            'tariff_mig_item_title' => $this->string()->null(),
            'tariff_mig_item_description' => $this->text()->null(),
            'tariff_mig_item_price' => $this->float()->null(),
            'tariff_high_item_title' => $this->string()->null(),
            'tariff_high_item_description' => $this->text()->null(),
            'tariff_high_item_price' => $this->float()->null(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('internet_offer_sort', $this->table, 'sort');

        $this->insert($this->table, [
            'name' => 'Контекстная реклама',
            'slug' => 'kontekstnaja',
            'icon' => 'offers_1.svg',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
            'page_title' => 'Разработка контекстной рекламы - цены на рекламу в Яндекс и Google',
            'page_description' => 'Контекстная реклама - самый быстрый и один из самых эффективных способов привлечения клиентов в интернете. Проще говоря, «уже завтра» Вы сможете получить свои первые звонки и продажи. Работаем на результат!',
            'page_title_with_city' => 'Контекстная реклама #CITY_WHERE# - цены на рекламу в Яндекс и Google',
            'page_heading_with_city' => 'Контекстная реклама #CITY_WHERE#',
            'page_description_with_city' => 'Контекстная реклама #CITY_WHERE# - самый быстрый и один из самых эффективных способов привлечения клиентов в интернете. Проще говоря, «уже завтра» ',
            'tariff_title' => 'Тарифы на разработку рекламных кампаний',
            'tariff_description' => 'При разработке рекламной кампании мы всегда советуем своим клиентам отслеживать её эффективность. Для этого Вам необходимо учитывать, какое количество заявок оставили пользователи на сайте, например с форм обратной связи, и какое количество звонков они совершили, перейдя с рекламных объявлений.

Для учёта такой статистики необходимо настроить счётчики аналитики на Вашем сайте, и систему отслеживания звонков (calltracking). В стоимость рекламных пакетов эти услуги не включены, но при условии, что вы хотите их подключить, — — обратитесь к менеджеру, чтобы мы смогли сделать рассчёт стоимости их подключения.',
            'tariff_low_item_title' => 'Тариф «Старт»',
            'tariff_low_item_description' => 'Подходит для небольших рекламных кампаний, небольших городов, узких ниш или тестирования новой услуги',
            'tariff_low_item_price' => 8000,
            'tariff_mig_item_title' => 'Тариф «Стандарт»',
            'tariff_mig_item_description' => 'Подходит почти Всем! Полноценная разработка рекламной кампании: на поиске, на площадках, по конкурентам',
            'tariff_mig_item_price' => 15000,
            'tariff_high_item_title' => 'Тариф «Турбо»',
            'tariff_high_item_description' => 'Для очень больших рекламных кампаний, интернет-магазинов, для сложных высококонкурентных ниш. Включает в себя использование большинство возможных инструментов',
            'tariff_high_item_price' => 30000,
        ]);
    }

    public function down()
    {
        $this->dropTable($this->table);
    }
}
