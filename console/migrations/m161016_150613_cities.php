<?php

use yii\db\Migration;

class m161016_150613_cities extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_id' => $this->integer()->comment('Coat of arms'), // Герб
            'salary' => $this->money()->notNull()->comment('Average salary'), // Среднаяя зп
            'population' => $this->double()->notNull()->comment('Population'), // Численность населения
            'density' => $this->double()->comment('Population density'), // Плотность населения
            'activities' => $this->text()->comment('Main activities'), // Основные виды деятельности
            'is_large_city' => $this->boolean()->notNull()->comment('Is large city'), // Крупный город

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('city_sort', '{{%city}}', 'sort');
        $this->createIndex('city_status', '{{%city}}', 'status');
        $this->createIndex('city_is_large_city', '{{%city}}', 'is_large_city');
        $this->createIndex('city_image_id', '{{%city}}', 'image_id');
        $this->addForeignKey('city_image', '{{%city}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('city_image','{{%city}}');
        $this->dropTable('{{%city}}');
    }
}
