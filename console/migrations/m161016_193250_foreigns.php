<?php

use yii\db\Migration;

class m161016_193250_foreigns extends Migration
{
    public function up()
    {
        $this->dropForeignKey('info_image','{{%info}}');
        $this->addForeignKey('info_image', '{{%info}}', 'image_id', '{{%file}}', 'id');

        $this->dropForeignKey('review_image','{{%review}}');
        $this->addForeignKey('review_image', '{{%review}}', 'image_id', '{{%file}}', 'id');

        $this->dropForeignKey('customer_image','{{%customer}}');
        $this->addForeignKey('customer_image', '{{%customer}}', 'image_id', '{{%file}}', 'id');

        $this->dropForeignKey('city_image','{{%city}}');
        $this->addForeignKey('city_image', '{{%city}}', 'image_id', '{{%file}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('info_image','{{%info}}');
        $this->addForeignKey('info_image', '{{%info}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');

        $this->dropForeignKey('review_image','{{%review}}');
        $this->addForeignKey('review_image', '{{%review}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');

        $this->dropForeignKey('customer_image','{{%customer}}');
        $this->addForeignKey('customer_image', '{{%customer}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');

        $this->dropForeignKey('city_image','{{%city}}');
        $this->addForeignKey('city_image', '{{%city}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');
    }
}
