<?php

use yii\db\Migration;

/**
 * Class m190227_185739_discount_monthly
 */
class m190227_185739_discount_monthly extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%outdoor_ad_side}}', 'discount_monthly', $this->money()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%outdoor_ad_side}}', 'discount_monthly');
    }
}
