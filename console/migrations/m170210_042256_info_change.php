<?php

use yii\db\Migration;

class m170210_042256_info_change extends Migration
{
    public function up()
    {
        $this->dropForeignKey('info_image', '{{%info}}');
        $this->addForeignKey('info_image', '{{%info}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        echo "m170210_042256_info_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
