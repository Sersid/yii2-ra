<?php

use yii\db\Migration;

class m161124_081326_radio_ad extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%radio_ad}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull()->comment('City'),
            'radio_id' => $this->integer()->notNull()->comment('TV'),
            'price_ad_sec' => $this->money()->comment('Ads - Sec'),
            'price_ad_show' => $this->money()->comment('Ads - Show'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('radio_ad_sort', '{{%radio_ad}}', 'sort');
        $this->createIndex('radio_ad_status', '{{%radio_ad}}', 'status');
        $this->createIndex('radio_ad_city_id', '{{%radio_ad}}', 'city_id');
        $this->createIndex('radio_ad_radio_id', '{{%radio_ad}}', 'radio_id');
        $this->createIndex('radio_ad_radio_id_city_id', '{{%radio_ad}}', ['city_id', 'radio_id'], true);
        $this->addForeignKey('radio_ad_city', '{{%radio_ad}}', 'city_id', '{{%city}}', 'id');
        $this->addForeignKey('radio_ad_radio', '{{%radio_ad}}', 'radio_id', '{{%radio}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('radio_ad_city','{{%radio_ad}}');
        $this->dropForeignKey('radio_ad_tv','{{%radio_ad}}');
        $this->dropTable('{{%radio_ad}}');
    }
}
