<?php
declare(strict_types=1);

use yii\db\Migration;

class m210318_080634_tv_ad_price_table extends Migration
{
    private string $table = '{{%tv_ad}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'price_table', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'price_table');
    }
}
