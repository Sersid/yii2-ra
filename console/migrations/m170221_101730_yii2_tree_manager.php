<?php

/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2015 - 2017
 * @package yii2-tree-manager
 * @version 1.0.8
 * @see http://demos.krajee.com/tree-manager
 */

use yii\db\Migration;

/**
 * Migration for creating the database structure for the kartik-v/yii2-tree-manager module.
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 1.0
 */
class m170221_101730_yii2_tree_manager extends Migration
{
    const TABLE_NAME = '{{%book}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->bigPrimaryKey(),
            'root' => $this->integer(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'lvl' => $this->smallInteger(5)->notNull(),
            'name' => $this->string(255)->notNull(),
            'icon' => $this->string(255),
            'icon_type' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'selected' => $this->boolean()->notNull()->defaultValue(false),
            'disabled' => $this->boolean()->notNull()->defaultValue(false),
            'readonly' => $this->boolean()->notNull()->defaultValue(false),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
            'collapsed' => $this->boolean()->notNull()->defaultValue(false),
            'movable_u' => $this->boolean()->notNull()->defaultValue(true),
            'movable_d' => $this->boolean()->notNull()->defaultValue(true),
            'movable_l' => $this->boolean()->notNull()->defaultValue(true),
            'movable_r' => $this->boolean()->notNull()->defaultValue(true),
            'removable' => $this->boolean()->notNull()->defaultValue(true),
            'removable_all' => $this->boolean()->notNull()->defaultValue(false),
        ], $tableOptions);
        $this->createIndex('tree_NK1', self::TABLE_NAME, 'root');
        $this->createIndex('tree_NK2', self::TABLE_NAME, 'lft');
        $this->createIndex('tree_NK3', self::TABLE_NAME, 'rgt');
        $this->createIndex('tree_NK4', self::TABLE_NAME, 'lvl');
        $this->createIndex('tree_NK5', self::TABLE_NAME, 'active');

        // Дополнительные поля
        $this->addColumn(self::TABLE_NAME, 'image_id', $this->integer());
        $this->addColumn(self::TABLE_NAME, 'parent_id', $this->integer()->defaultValue(0));
        $this->addColumn(self::TABLE_NAME, 'slug', $this->text());
        $this->addColumn(self::TABLE_NAME, 'short_desc', $this->text());
        $this->addColumn(self::TABLE_NAME, 'full_desc', $this->text());

        $this->insert('{{%book}}', [
            'root' => 1,
            'lft' => 1,
            'rgt' => 2,
            'lvl' => 0,
            'name' => 'Книжная полка',
            'icon_type' => 1,
            'active' => true,
            'selected' => false,
            'disabled' => false,
            'readonly' => false,
            'visible' => true,
            'collapsed' => false,
            'movable_u' => false,
            'movable_d' => false,
            'movable_l' => false,
            'movable_r' => false,
            'removable' => false,
            'removable_all' => false,
            'parent_id' => 0,
        ]);
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}