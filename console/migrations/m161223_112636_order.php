<?php

use yii\db\Migration;

class m161223_112636_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'guest_id' => $this->integer()->notNull()->comment('Guest'),
            'name' => $this->string(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('order_guest_id', '{{%order}}', 'guest_id');
        $this->addForeignKey('order_guest_id', '{{%order}}', 'guest_id', '{{%guest}}', 'id');

        $this->addColumn('{{%user_outdoor_ad}}', 'order_id', $this->integer()->null());
        $this->addColumn('{{%user_radio_ad}}', 'order_id', $this->integer()->null());
        $this->addColumn('{{%user_ticker}}', 'order_id', $this->integer()->null());
        $this->addColumn('{{%user_tv_ad}}', 'order_id', $this->integer()->null());
    }

    public function down()
    {
        echo "m161223_112636_order cannot be reverted.\n";
        return false;
    }
}
