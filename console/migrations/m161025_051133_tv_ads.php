<?php

use yii\db\Migration;

class m161025_051133_tv_ads extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tv_ad}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull()->comment('City'),
            'tv_id' => $this->integer()->notNull()->comment('TV'),
            'price_ad_sec' => $this->money()->comment('Ads - Sec'),
            'price_sponsorship_sec' => $this->money()->comment('Sponsorship - Sec'),
            'price_screen_sec' => $this->money()->comment('Screen - Sec'),
            'price_ad_show' => $this->money()->comment('Ads - Show'),
            'price_sponsorship_show' => $this->money()->comment('Sponsorship - Show'),
            'price_screen_show' => $this->money()->comment('Screen - Show'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('tv_ad_sort', '{{%tv_ad}}', 'sort');
        $this->createIndex('tv_ad_status', '{{%tv_ad}}', 'status');
        $this->createIndex('tv_ad_city_id', '{{%tv_ad}}', 'city_id');
        $this->createIndex('tv_ad_tv_id', '{{%tv_ad}}', 'tv_id');
        $this->createIndex('tv_ad_tv_id_city_id', '{{%tv_ad}}', ['city_id', 'tv_id'], true);
        $this->addForeignKey('tv_ad_city', '{{%tv_ad}}', 'city_id', '{{%city}}', 'id');
        $this->addForeignKey('tv_ad_tv', '{{%tv_ad}}', 'tv_id', '{{%tv}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tv_ad_city','{{%tv_ad}}');
        $this->dropForeignKey('tv_ad_tv','{{%tv_ad}}');
        $this->dropTable('{{%tv_ad}}');
    }
}
