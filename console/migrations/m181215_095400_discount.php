<?php

use yii\db\Migration;

/**
 * Class m181215_095400_discount
 */
class m181215_095400_discount extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%outdoor_ad_side_busy}}', 'discount', $this->money()->unsigned()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%outdoor_ad_side_busy}}', 'discount');
    }
}
