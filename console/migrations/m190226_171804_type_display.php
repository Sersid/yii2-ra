<?php

use yii\db\Migration;

/**
 * Class m190226_171804_type_display
 */
class m190226_171804_type_display extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%outdoor_ad}}', 'type_display', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%outdoor_ad}}', 'type_display');
    }
}
