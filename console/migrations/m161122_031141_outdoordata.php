<?php

use yii\db\Migration;

class m161122_031141_outdoordata extends Migration
{
    public function up()
    {
        $sql = <<<SQL
INSERT INTO {{%outdoor_ad_side_busy}} (`side_id`, `month`, `year`) VALUES
(1, 1, 2017),
(1, 2, 2017);
SQL;
        $this->db->createCommand($sql)->execute();
    }

    public function down()
    {
        $sql = <<<SQL
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE {{%outdoor_ad_side_busy}};
SET FOREIGN_KEY_CHECKS = 1;
SQL;
        $this->db->createCommand($sql)->execute();
    }
}
