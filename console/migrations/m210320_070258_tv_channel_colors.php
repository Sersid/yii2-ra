<?php
declare(strict_types=1);

use yii\db\Migration;

class m210320_070258_tv_channel_colors extends Migration
{
    private string $table = '{{%tv}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn($this->table, 'color');
        $this->addColumn($this->table, 'color_heading', $this->string()->null());
        $this->addColumn($this->table, 'color_line', $this->string()->null());
        $this->addColumn($this->table, 'color_potential_audience', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn($this->table, 'color', $this->string()->null());
        $this->dropColumn($this->table, 'color_heading');
        $this->dropColumn($this->table, 'color_line');
        $this->dropColumn($this->table, 'color_potential_audience');
    }
}
