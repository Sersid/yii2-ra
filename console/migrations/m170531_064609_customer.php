<?php

use yii\db\Migration;

class m170531_064609_customer extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer}}', 'slug', $this->string()->notNull());
        $this->createIndex('customer_slug', '{{%customer}}', 'slug');
        $this->addColumn('{{%customer}}', 'content', $this->text()->null());
    }

    public function down()
    {
        $this->dropColumn('{{%customer}}', 'slug');
        $this->dropColumn('{{%customer}}', 'content');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
