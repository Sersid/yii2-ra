<?php

use yii\db\Migration;

class m170203_104540_order extends Migration
{
    public function up()
    {
        $this->createIndex('user_outdoor_ad_order', '{{%user_outdoor_ad}}', 'order_id');
        $this->addForeignKey('user_outdoor_ad_order', '{{%user_outdoor_ad}}', 'order_id', '{{%order}}', 'id');

        $this->createIndex('user_radio_ad_order', '{{%user_radio_ad}}', 'order_id');
        $this->addForeignKey('user_radio_ad_order', '{{%user_radio_ad}}', 'order_id', '{{%order}}', 'id');

        $this->createIndex('user_ticker_order', '{{%user_ticker}}', 'order_id');
        $this->addForeignKey('user_ticker_order', '{{%user_ticker}}', 'order_id', '{{%order}}', 'id');

        $this->createIndex('user_tv_ad_order', '{{%user_tv_ad}}', 'order_id');
        $this->addForeignKey('user_tv_ad_order', '{{%user_tv_ad}}', 'order_id', '{{%order}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('user_outdoor_ad_order', '{{%user_outdoor_ad}}');
        $this->dropIndex('user_outdoor_ad_order', '{{%user_outdoor_ad}}');

        $this->dropForeignKey('user_radio_ad_order', '{{%user_radio_ad}}');
        $this->dropIndex('user_radio_ad_order', '{{%user_radio_ad}}');

        $this->dropForeignKey('user_ticker_order', '{{%user_ticker}}');
        $this->dropIndex('user_ticker_order', '{{%user_ticker}}');

        $this->dropForeignKey('user_tv_ad_order', '{{%user_tv_ad}}');
        $this->dropIndex('user_tv_ad_order', '{{%user_tv_ad}}');
    }
}
