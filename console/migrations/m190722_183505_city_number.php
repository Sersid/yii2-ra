<?php

use yii\db\Migration;

/**
 * Class m190722_183505_city_number
 */
class m190722_183505_city_number extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%city}}', 'phone_digit', $this->boolean()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%city}}', 'phone_digit');
    }
}
