<?php

use yii\db\Migration;

class m170718_091059_region extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%region}}', 'sort', $this->integer()->notNull()->defaultValue(500));
        $this->createIndex('region_sort', '{{%region}}', 'sort');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%region}}', 'sort');
    }
}
