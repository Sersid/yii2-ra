<?php

use yii\db\Migration;

class m161016_120759_customers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%customer}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_id' => $this->integer()->notNull()->comment('Image'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('customer_sort', '{{%customer}}', 'sort');
        $this->createIndex('customer_image_id', '{{%customer}}', 'image_id');
        $this->addForeignKey('customer_image', '{{%customer}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('customer_image','{{%customer}}');
        $this->dropTable('{{%customer}}');
    }
}
