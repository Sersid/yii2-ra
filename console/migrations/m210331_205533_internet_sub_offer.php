<?php
declare(strict_types=1);

use yii\db\Migration;

class m210331_205533_internet_sub_offer extends Migration
{
    private string $table = '{{%internet_sub_offer}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'icon' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert($this->table, [
            'name' => 'Сертифицированные специалисты',
            'icon' => 'offers_11.svg',
            'description' => 'Все наши специалисты по рекламе в обязательном порядке, ежегодно сдают экзамены по работе с Яндекс.Директ, Google Ads.',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
        ]);

        $this->insert($this->table, [
            'name' => 'Фиксированная цена',
            'icon' => 'offers_10.svg',
            'description' => 'Стоимость не зависит от количества ключевых слов, по которым показывается реклама. Заранее оговариваем стоимость разработки и ведения рекламных кампаний.',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
        ]);

        $this->insert($this->table, [
            'name' => 'Прозрачность оказания услуг',
            'icon' => 'offers_9.svg',
            'description' => 'Максимальная прозрачность всех работ: ежемесячный расширенный отчет. При необходимости настроим сквозную аналитику.',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
        ]);

        $this->insert($this->table, [
            'name' => 'Значимый опыт',
            'icon' => 'offers_12.svg',
            'description' => 'На рынке с 2012 года, за плечами команды опыт достижения результата в самых конкурентных тематиках.',
            'status' => 1,
            'sort' => 500,
            'created_at' => '01-04-2021 00:25:00',
            'updated_at' => '01-04-2021 00:25:00',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
