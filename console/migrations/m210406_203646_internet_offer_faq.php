<?php
declare(strict_types=1);

use yii\db\Migration;

class m210406_203646_internet_offer_faq extends Migration
{
    private string $table = '{{%internet_offer_faq}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'offer_id' => $this->integer()->notNull(),
                'faq_id' => $this->integer()->notNull(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
