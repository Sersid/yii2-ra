<?php

use yii\db\Migration;

class m161027_012720_outdoor_ads extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%outdoor_ad}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull()->comment('City'),
            'street_id' => $this->integer()->notNull()->comment('Street'),
            'address' => $this->string(),
            'format' => $this->smallInteger(),
            'size' => $this->string(),
            'sides' => $this->smallInteger()->notNull(),
            'lat' => $this->decimal(10, 8),
            'lon' => $this->decimal(11, 8),
            'rotate' => $this->smallInteger(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('outdoor_ad_sort', '{{%outdoor_ad}}', 'sort');
        $this->createIndex('outdoor_ad_status', '{{%outdoor_ad}}', 'status');
        $this->createIndex('outdoor_ad_city_id', '{{%outdoor_ad}}', 'city_id');
        $this->createIndex('outdoor_ad_street_id', '{{%outdoor_ad}}', 'street_id');

        $this->createTable('{{%street}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'city_id' => $this->integer()->comment('City'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('street_sort', '{{%street}}', 'sort');
        $this->createIndex('street_status', '{{%street}}', 'status');
        $this->createIndex('street_city_id', '{{%street}}', 'city_id');


        $this->createTable('{{%outdoor_ad_side}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->notNull()->comment('Outdoor ad'),
            'number' => $this->string()->notNull(),
            'side' => $this->smallInteger()->notNull(),
            'image_id' => $this->integer()->comment('Image'),
            'price' => $this->money()->notNull(),
            'price_installation' => $this->money()->notNull(),
            'price_print' => $this->money()->notNull(),
            'price_design' => $this->money()->notNull(),
            'rating' => $this->smallInteger(),


            'status' => $this->boolean()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex('outdoor_ad_side_status', '{{%outdoor_ad_side}}', 'status');
        $this->createIndex('outdoor_ad_side_parent_id', '{{%outdoor_ad_side}}', 'parent_id');
        $this->createIndex('outdoor_ad_side_image_id', '{{%outdoor_ad_side}}', 'image_id');
        $this->createIndex('outdoor_ad_side_number', '{{%outdoor_ad_side}}', 'number', true);

        $this->createTable('{{%outdoor_ad_side_busy}}', [
            'side_id' => $this->integer()->notNull()->comment('Outdoor ad side'),
            'month' => $this->smallInteger(2)->notNull(),
            'year' => $this->smallInteger(4)->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('outdoor_ad_side_busy', '{{%outdoor_ad_side_busy}}', ['side_id', 'year', 'month']);

        $this->addForeignKey('outdoor_ad_city', '{{%outdoor_ad}}', 'city_id', '{{%city}}', 'id');
        $this->addForeignKey('outdoor_ad_street', '{{%outdoor_ad}}', 'street_id', '{{%street}}', 'id');
        $this->addForeignKey('street_city', '{{%street}}', 'city_id', '{{%city}}', 'id');
        $this->addForeignKey('outdoor_ad_side_parent', '{{%outdoor_ad_side}}', 'parent_id', '{{%outdoor_ad}}', 'id');
        $this->addForeignKey('outdoor_ad_side_image', '{{%outdoor_ad_side}}', 'image_id', '{{%file}}', 'id');
        $this->addForeignKey('outdoor_ad_side_busy_side', '{{%outdoor_ad_side_busy}}', 'side_id', '{{%outdoor_ad_side}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('outdoor_ad_side_busy_side','{{%outdoor_ad_side_busy}}');
        $this->dropForeignKey('outdoor_ad_side_parent','{{%outdoor_ad_side}}');
        $this->dropForeignKey('outdoor_ad_side_image','{{%outdoor_ad_side}}');
        $this->dropForeignKey('outdoor_ad_city','{{%outdoor_ad}}');
        $this->dropForeignKey('outdoor_ad_street','{{%outdoor_ad}}');
        $this->dropForeignKey('street_city','{{%street}}');
        $this->dropTable('{{%outdoor_ad_side_busy}}');
        $this->dropTable('{{%outdoor_ad_side}}');
        $this->dropTable('{{%street}}');
        $this->dropTable('{{%outdoor_ad}}');
    }
}
