<?php

use yii\db\Migration;

class m170405_230557_outdoor_ad_number extends Migration
{
    public function up()
    {
        $this->addColumn('{{%outdoor_ad}}', 'full_number', $this->text()->null());
    }

    public function down()
    {
        $this->dropColumn('{{%outdoor_ad}}', 'full_number');
    }
}
