<?php

use yii\db\Migration;

class m170518_030652_cities extends Migration
{
    public function up()
    {
        $this->addColumn('{{%city}}', 'content_outdoor_ad', $this->text()->null());
        $this->addColumn('{{%city}}', 'content_tv', $this->text()->null());
        $this->addColumn('{{%city}}', 'content_ticker', $this->text()->null());
        $this->addColumn('{{%city}}', 'content_radio', $this->text()->null());
    }

    public function down()
    {
        $this->dropColumn('{{%city}}', 'content_outdoor_ad');
        $this->dropColumn('{{%city}}', 'content_tv');
        $this->dropColumn('{{%city}}', 'content_ticker');
        $this->dropColumn('{{%city}}', 'content_radio');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
