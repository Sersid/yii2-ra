<?php

use yii\db\Migration;

class m170214_092121_add_long_text_to_info extends Migration
{
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql')
        {
            Yii::$app->db->createCommand("ALTER TABLE info MODIFY full_desc LONGTEXT");
        }
    }

    public function safeDown()
    {
        if ($this->db->driverName === 'mysql')
        {
            $this->alterColumn('{{%info}}', 'full_desc', $this->text());
        }
    }
}
