<?php

use yii\db\Migration;

class m161116_145548_feedback extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%form_feedback}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->null()->comment('Ваше имя'),
            'phone' => $this->string()->notNull()->comment('Ваш телефон'),
            'message' => $this->text()->null()->comment('Комментарии или вопросы'),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%form_feedback}}');
    }
}
