<?php

use yii\db\Migration;

/**
 * Class m210114_001101_offer_discount
 */
class m210114_001101_offer_discount extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%radio_ad}}', 'discount', $this->money()->null());
        $this->update('{{%radio_ad}}', ['discount' => 20]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%radio_ad}}', 'discount');
    }
}
