<?php

use yii\db\Migration;

class m161124_071648_radio extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%radio}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_id' => $this->integer()->comment('Logo'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('radio_sort', '{{%radio}}', 'sort');
        $this->createIndex('radio_status', '{{%radio}}', 'status');
        $this->createIndex('radio_image_id', '{{%radio}}', 'image_id');
        $this->addForeignKey('radio_image', '{{%radio}}', 'image_id', '{{%file}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('radio_image','{{%radio}}');
        $this->dropTable('{{%radio}}');
    }
}
