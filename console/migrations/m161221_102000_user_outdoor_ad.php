<?php

use yii\db\Migration;

class m161221_102000_user_outdoor_ad extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_outdoor_ad}}', [
            'id' => $this->primaryKey(),
            'guest_id' => $this->integer()->notNull()->comment('Guest'),
            'outdoor_ad_id' => $this->integer()->notNull()->comment('Guest'),
            'install' => $this->boolean(),
            'print' => $this->boolean(),
            'design' => $this->boolean(),
            'dates' => $this->string()->notNull(),

            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('user_outdoor_ad_status', '{{%user_outdoor_ad}}', 'status');
        $this->createIndex('user_outdoor_ad_primary', '{{%user_outdoor_ad}}',['guest_id', 'outdoor_ad_id', 'status']);

        $this->addForeignKey('user_outdoor_ad_guest_id', '{{%user_outdoor_ad}}', 'guest_id', '{{%guest}}', 'id');
        $this->addForeignKey('user_outdoor_ad_outdoor_ad_id', '{{%user_outdoor_ad}}', 'outdoor_ad_id', '{{%outdoor_ad_side}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('user_outdoor_ad_guest_id', '{{%user_outdoor_ad}}');
        $this->dropForeignKey('user_outdoor_ad_outdoor_ad_id', '{{%user_outdoor_ad}}');
        $this->dropTable('{{%user_outdoor_ad}}');
    }
}
