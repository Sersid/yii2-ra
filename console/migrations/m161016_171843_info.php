<?php

use yii\db\Migration;

class m161016_171843_info extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%info_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createTable('{{%info}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'image_id' => $this->integer()->notNull()->comment('Image'),
            'category_id' => $this->integer()->notNull()->comment('Category'),
            'short_desc' => $this->text()->notNull()->comment('Short description'),
            'full_desc' => $this->text()->comment('Full description'),

            'status' => $this->boolean()->notNull()->comment('Active'),
            'sort' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('info_sort', '{{%info}}', 'sort');
        $this->createIndex('info_status', '{{%info}}', 'status');

        $this->createIndex('info_slug', '{{%info}}', 'slug', true);
        $this->createIndex('info_image_id', '{{%info}}', 'image_id');
        $this->addForeignKey('info_image', '{{%info}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('info_slug', '{{%info_category}}', 'slug', true);
        $this->createIndex('info_category_id', '{{%info}}', 'category_id');
        $this->addForeignKey('info_category', '{{%info}}', 'category_id', '{{%info_category}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('info_category','{{%info}}');
        $this->dropForeignKey('info_image','{{%info}}');
        $this->dropTable('{{%info}}');
        $this->dropTable('{{%info_category}}');
    }
}
