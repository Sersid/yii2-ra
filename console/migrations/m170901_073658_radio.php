<?php

use yii\db\Migration;

class m170901_073658_radio extends Migration
{
    public function up()
    {
        $this->addColumn('{{%radio}}', 'slug', $this->string()->notNull());
        $this->createIndex('radio_slug', '{{%radio}}', 'slug');
    }

    public function down()
    {
        $this->dropColumn('{{%radio}}', 'slug');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170901_073658_radio cannot be reverted.\n";

        return false;
    }
    */
}
