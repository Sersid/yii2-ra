<?php

use yii\db\Migration;

class m170310_105110_customer extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%customer}}', 'image_id', $this->integer()->null());
        $this->dropForeignKey('customer_image','{{%customer}}');
        $this->addForeignKey('customer_image', '{{%customer}}', 'image_id', '{{%file}}', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%customer}}', 'image_id', $this->integer()->notNull());
        $this->dropForeignKey('customer_image','{{%customer}}');
        $this->addForeignKey('customer_image', '{{%customer}}', 'image_id', '{{%file}}', 'id', 'CASCADE', 'CASCADE');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
