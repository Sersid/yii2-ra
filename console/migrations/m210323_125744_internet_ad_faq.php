<?php
declare(strict_types=1);

use yii\db\Migration;

class m210323_125744_internet_ad_faq extends Migration
{
    private string $table = '{{%internet_faq}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'question' => $this->string()->notNull(),
            'answer' => $this->text()->notNull(),

            'is_active' => $this->boolean()->notNull(),
            'is_global' => $this->boolean()->notNull(),
            'sort' => $this->integer()->notNull(),
            'date_created' => $this->dateTime(),
            'date_updated' => $this->dateTime()->null(),
        ], $tableOptions);

        $this->createIndex('internet_faq_sort', $this->table, 'sort');
    }

    public function down()
    {
        $this->dropTable($this->table);
    }
}
