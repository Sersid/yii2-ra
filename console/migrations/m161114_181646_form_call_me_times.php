<?php

use yii\db\Migration;

class m161114_181646_form_call_me_times extends Migration
{
    public function up()
    {
        $this->addColumn('{{%form_call_me}}', 'created_at', $this->integer()->notNull());
        $this->addColumn('{{%form_call_me}}', 'updated_at', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%form_call_me}}', 'created_at');
        $this->dropColumn('{{%form_call_me}}', 'updated_at');
    }
}
