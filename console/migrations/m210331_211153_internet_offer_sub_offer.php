<?php
declare(strict_types=1);

use yii\db\Migration;

class m210331_211153_internet_offer_sub_offer extends Migration
{
    private string $table = '{{%internet_offer_sub_offer}}';

    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'offer_id' => $this->integer()->notNull(),
                'sub_offer_id' => $this->integer()->notNull(),
            ]
        );

        $this->addPrimaryKey('internet_offer_sub_offer_primary', $this->table, ['offer_id', 'sub_offer_id']);

        $this->insert($this->table, [
            'offer_id' => 1,
            'sub_offer_id' => 1,
        ]);
        $this->insert($this->table, [
            'offer_id' => 1,
            'sub_offer_id' => 2,
        ]);
        $this->insert($this->table, [
            'offer_id' => 1,
            'sub_offer_id' => 3,
        ]);
        $this->insert($this->table, [
            'offer_id' => 1,
            'sub_offer_id' => 4,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
