<?php
declare(strict_types=1);

use yii\db\Migration;

class m210317_175138_potential_audience extends Migration
{
    private string $table = '{{%tv_ad_potential_audience}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'tv_ad_id' => $this->integer()->unsigned()->notNull(),
            'men' => $this->float()->unsigned()->null(),
            'women' => $this->float()->unsigned()->null(),
            'youth' => $this->float()->unsigned()->null(),
            'total' => $this->float()->unsigned()->null(),
        ]);
        $this->createIndex('tv_ad_potential_audience_tv_ad_id', $this->table, 'tv_ad_id', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
