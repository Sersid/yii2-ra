<?php

use yii\db\Migration;

class m170118_014624_user_ticker extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%user_ticker}}', 'not_phone');
        $this->addColumn('{{%user_ticker_phone}}', 'type', $this->smallInteger()->notNull());
    }

    public function down()
    {
        echo "m170118_014624_user_ticker cannot be reverted.\n";

        return false;
    }
}
