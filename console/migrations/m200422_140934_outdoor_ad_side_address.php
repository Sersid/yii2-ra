<?php

use yii\db\Migration;

/**
 * Class m200422_140934_outdoor_ad_side_address
 */
class m200422_140934_outdoor_ad_side_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%outdoor_ad}}', 'address');
        $this->addColumn('{{%outdoor_ad_side}}', 'address', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%outdoor_ad}}', 'address', $this->string());
        $this->dropColumn('{{%outdoor_ad_side}}', 'address');
    }
}
