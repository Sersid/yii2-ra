<?php

use yii\db\Migration;

/**
 * Class m190402_163838_yandex_disk
 */
class m190402_163838_yandex_disk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%yandex_disk_folder}}',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'href' => $this->text()->notNull(),
                'season' => $this->smallInteger()->notNull(),
                'status' => $this->integer()->notNull(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex('yandex_disk_folders_name', '{{%yandex_disk_folder}}', 'name', true);
        $this->createIndex('yandex_disk_folders_status', '{{%yandex_disk_folder}}', 'status');

        $this->createTable(
            '{{%outdoor_ad_side_image}}',
            [
                'id' => $this->primaryKey(),
                'folder_id' => $this->integer()->notNull(),
                'file_name' => $this->string()->notNull(),
                'side_id' => $this->integer(),
                'href' => $this->text()->notNull(),
                'content_type' => $this->string(),
                'last_modified' => $this->integer(),
                'last_upload' => $this->integer(),
                'status' => $this->integer()->notNull(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex(
            'outdoor_ad_side_images_folder_id_file_name',
            '{{%outdoor_ad_side_image}}',
            ['folder_id', 'file_name'],
            true
        );
        $this->createIndex('outdoor_ad_side_images_side_id', '{{%outdoor_ad_side_image}}', 'side_id');
        $this->createIndex('outdoor_ad_side_images_status', '{{%outdoor_ad_side_image}}', 'status');

        $this->addForeignKey(
            'outdoor_ad_side_images_folder_id',
            '{{%outdoor_ad_side_image}}',
            'folder_id',
            '{{%yandex_disk_folder}}',
            'id'
        );
        $this->addForeignKey(
            'outdoor_ad_side_images_side_id',
            '{{%outdoor_ad_side_image}}',
            'side_id',
            '{{%outdoor_ad_side}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('outdoor_ad_side_images_side_id', '{{%outdoor_ad_side_image}}');
        $this->dropForeignKey('outdoor_ad_side_images_folder_id', '{{%outdoor_ad_side_image}}');
        $this->dropTable('{{%outdoor_ad_side_image}}');
        $this->dropTable('{{%yandex_disk_folder}}');
    }
}
