<?php

use yii\db\Migration;

class m170215_045952_meta_tag_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%meta_tag}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->string(255)->notNull(),
            'url' => $this->string(255)->unique()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => $this->boolean()->defaultValue(true),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%meta_tag}}');
    }
}
