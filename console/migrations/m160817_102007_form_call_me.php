<?php

use yii\db\Migration;

class m160817_102007_form_call_me extends Migration
{
    public function up()
    {
        $table = $this->createTable('form_call_me', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->null()->comment('Ваше имя'),
            'phone' => $this->string()->notNull()->comment('Ваш телефон'),
            'email' => $this->string()->null()->comment('Ваш E-mail'),
        ]);
    }

    public function down()
    {
        $this->dropTable('form_call_me');
    }
}
