<?php

use yii\db\Migration;

class m170328_145604_outdoor_ad_descr extends Migration
{
    public function up()
    {
        $this->addColumn('{{%outdoor_ad_side}}', 'description', $this->text()->null());
    }

    public function down()
    {
        echo "m170328_145604_outdoor_ad_descr cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
