<?php

use common\models\AdvCase;
use yii\db\Migration;

/**
 * Class m200707_191144_adv_case
 */
class m200707_191144_adv_case extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%adv_case}}',
            'html',
            $this->text()
                ->null());
        $this->addColumn('{{%adv_case}}',
            'type',
            $this->tinyInteger(1)
                ->defaultValue(AdvCase::TYPE_DEFAULT));
        $this->addColumn('{{%adv_case}}',
            'category',
            $this->integer());
        $this->createIndex('adv_case_category', '{{%adv_case}}', 'category');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%adv_case}}', 'type');
        $this->dropColumn('{{%adv_case}}', 'html');
        $this->dropColumn('{{%adv_case}}', 'category');
    }
}
