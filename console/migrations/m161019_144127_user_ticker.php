<?php

use yii\db\Migration;

class m161019_144127_user_ticker extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_ticker}}', [
            'id' => $this->primaryKey(),
            'guest_id' => $this->integer()->notNull()->comment('Guest'),
            'type' => $this->smallInteger()->notNull(),
            'city_id' => $this->integer()->comment('City'),
            'show_all_cities' => $this->boolean()->defaultValue(0),
            'not_phone' => $this->boolean()->defaultValue(0),
            'message' => $this->text(),

            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('user_ticker_status', '{{%user_ticker}}', 'status');

        $this->createIndex('user_ticker_guest_id', '{{%user_ticker}}', 'guest_id');
        $this->addForeignKey('user_ticker_guest', '{{%user_ticker}}', 'guest_id', '{{%guest}}', 'id');

        $this->createIndex('user_ticker_city_id', '{{%user_ticker}}', 'city_id');
        $this->addForeignKey('user_ticker_city', '{{%user_ticker}}', 'city_id', '{{%city}}', 'id');


        $this->createTable('{{%user_ticker_tv}}', [
            'user_ticker_id' => $this->integer()->notNull(),
            'ticker_id' => $this->integer()->notNull(),
            'date_from' => $this->date()->notNull(),
            'date_to' => $this->date()->notNull(),
            'view_type' => $this->smallInteger()->notNull(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('user_ticker_tv_primary','{{%user_ticker_tv}}',['user_ticker_id', 'ticker_id']);
        $this->addForeignKey('user_ticker_tv_user_ticker', '{{%user_ticker_tv}}', 'user_ticker_id', '{{%user_ticker}}', 'id');
        $this->addForeignKey('user_ticker_tv_ticker', '{{%user_ticker_tv}}', 'ticker_id', '{{%ticker}}', 'id');


        $this->createTable('{{%user_ticker_phone}}', [
            'id' => $this->primaryKey(),
            'user_ticker_id' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('user_ticker_phone_user_ticker', '{{%user_ticker_phone}}', 'user_ticker_id', '{{%user_ticker}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('user_ticker_phone_user_ticker', '{{%user_ticker_phone}}');
        $this->dropTable('{{%user_ticker_phone}}');

        $this->dropForeignKey('user_ticker_tv_user_ticker', '{{%user_ticker_tv}}');
        $this->dropForeignKey('user_ticker_tv_ticker', '{{%user_ticker_tv}}');
        $this->dropTable('{{%user_ticker_tv}}');


        $this->dropForeignKey('user_ticker_guest','{{%user_ticker}}');
        $this->dropForeignKey('user_ticker_city','{{%user_ticker}}');
        $this->dropTable('{{%user_ticker}}');
    }
}
