<?php

use yii\db\Migration;

class m161025_060559_user_tv_ads extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_tv_ad}}', [
            'id' => $this->primaryKey(),
            'guest_id' => $this->integer()->notNull()->comment('Guest'),
            'type' => $this->smallInteger()->notNull(),
            'city_id' => $this->integer()->comment('City'),
            'show_all_cities' => $this->boolean()->defaultValue(0),

            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('user_tv_ad_status', '{{%user_tv_ad}}', 'status');

        $this->createIndex('user_tv_ad_guest_id', '{{%user_tv_ad}}', 'guest_id');
        $this->addForeignKey('user_tv_ad_guest', '{{%user_tv_ad}}', 'guest_id', '{{%guest}}', 'id');

        $this->createIndex('user_tv_ad_city_id', '{{%user_tv_ad}}', 'city_id');
        $this->addForeignKey('user_tv_ad_city', '{{%user_tv_ad}}', 'city_id', '{{%city}}', 'id');

        $this->createTable('{{%user_tv_ad_tv}}', [
            'user_tv_ad_id' => $this->integer()->notNull(),
            'tv_ad_id' => $this->integer()->notNull(),
            'duration' => $this->integer(),
            'views' => $this->integer(),
            'days' => $this->integer(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('user_tv_ad_tv_primary','{{%user_tv_ad_tv}}',['user_tv_ad_id', 'tv_ad_id']);
        $this->addForeignKey('user_tv_ad_tv_user_tv_ad', '{{%user_tv_ad_tv}}', 'user_tv_ad_id', '{{%user_tv_ad}}', 'id');
        $this->addForeignKey('user_tv_ad_tv_tv_ad', '{{%user_tv_ad_tv}}', 'tv_ad_id', '{{%tv_ad}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('user_tv_ad_tv_user_tv_ad', '{{%user_tv_ad_tv}}');
        $this->dropForeignKey('user_tv_ad_tv_tv_ad', '{{%user_tv_ad_tv}}');
        $this->dropTable('{{%user_tv_ad_tv}}');


        $this->dropForeignKey('user_tv_ad_guest','{{%user_tv_ad}}');
        $this->dropForeignKey('user_tv_ad_city','{{%user_tv_ad}}');
        $this->dropTable('{{%user_tv_ad}}');
    }
}
