<?php

use yii\db\Migration;

class m170418_062402_full_number extends Migration
{
    public function up()
    {
        $this->dropIndex('outdoor_ad_number', '{{%outdoor_ad}}');
        $this->createIndex('outdoor_ad_number', '{{%outdoor_ad}}', 'number');
        $this->alterColumn('{{%outdoor_ad}}', 'full_number', $this->string());
        $this->createIndex('outdoor_ad_full_number', '{{%outdoor_ad}}', 'full_number');
    }

    public function down()
    {
        echo "m170418_062402_full_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
