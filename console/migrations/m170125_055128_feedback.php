<?php

use yii\db\Migration;

class m170125_055128_feedback extends Migration
{
    public function up()
    {
        $this->addColumn('{{%form_feedback}}', 'email', $this->string()->null()->comment('Ваш E-mail'));
    }

    public function down()
    {
        echo "m170125_055128_feedback cannot be reverted.\n";

        return false;
    }
}
