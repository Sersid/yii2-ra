<?php

use yii\db\Migration;

class m170622_134401_city extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%city}}', 'where', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%city}}', 'where');
    }
}
