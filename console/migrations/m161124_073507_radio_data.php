<?php

use yii\db\Migration;

class m161124_073507_radio_data extends Migration
{
    public function up()
    {
        $sql = <<<SQL
        INSERT INTO {{%file}} (`id`, `code`, `is_image`, `width`, `height`, `size`, `file_name`, `content_type`, `ext`, `status`, `created_at`, `updated_at`) VALUES
(49, '7f4f41dd04e798669e11b27310ded872', 1, 550, 342, 24589, '7e5d8dbbab08.jpg', 'image/jpeg', 'jpg', 10, 1479972556, 1479972556),
(50, '57d4a656e0e48edb73779f74a3b655da', 1, 492, 495, 52767, '95b.jpg', 'image/jpeg', 'jpg', 10, 1479972601, 1479972601),
(51, '383b9e5db236c0db94e3c3fe845b6255', 1, 233, 268, 16973, '169b.jpg', 'image/jpeg', 'jpg', 10, 1479972635, 1479972635),
(52, 'c3e8a00c3718612ab6a24e5504403c20', 1, 500, 250, 19166, '1363296144_shanson-radio-online-besplatno-spushat.jpg', 'image/jpeg', 'jpg', 10, 1479972824, 1479972824),
(53, 'e6310c60d152c742b5ecaea30ab180d2', 1, 1600, 1351, 122313, '1430261887_dorognoe-radio-online.jpg', 'image/jpeg', 'jpg', 10, 1479972836, 1479972836),
(54, '0154effe9e0bd80a4863df112c62862b', 1, 355, 249, 33747, 'comedy_radio_online_logo_2012.jpg', 'image/jpeg', 'jpg', 10, 1479972847, 1479972847),
(55, '0bb04c7adf5e9f9d88d876a59e143634', 1, 689, 326, 43653, 'love_radio_logo_2011.gif', 'image/gif', 'gif', 10, 1479972858, 1479972858),
(56, '9cab8e33bbb207f4837da0aee1033565', 1, 450, 307, 40873, 'russkoe_radio_2010.jpg', 'image/jpeg', 'jpg', 10, 1479972879, 1479972879);
SQL;
        $this->db->createCommand($sql)->execute();
        $sql = <<<SQL
INSERT INTO {{%radio}} (`id`, `name`, `image_id`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Europa Plus', 49, 1, 500, 1479972585, 1479972585),
(2, 'Юмор FM', 50, 1, 500, 1479972631, 1479972631),
(3, 'Love', 51, 1, 500, 1479972640, 1479972640),
(4, 'Шансон', 52, 1, 500, 1479972832, 1479972832),
(5, 'Дорожное', 53, 1, 500, 1479972841, 1479972841),
(6, 'Comedy Radio', 54, 1, 500, 1479972853, 1479972853),
(7, 'Love радио', 55, 1, 500, 1479972874, 1479972874),
(8, 'Русское радио', 56, 1, 500, 1479972889, 1479972889);
SQL;
        $this->db->createCommand($sql)->execute();
    }

    public function down()
    {
        $sql = <<<SQL
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE {{%radio}};
TRUNCATE {{%file}};
SET FOREIGN_KEY_CHECKS = 1;
SQL;
        $this->db->createCommand($sql)->execute();
    }
}
