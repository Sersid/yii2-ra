<?php

use yii\db\Migration;

class m170210_040046_info_change extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%info}}', 'short_desc', $this->string()->null());
        $this->alterColumn('{{%info}}', 'image_id', $this->integer()->null());
    }

    public function down()
    {
        echo "m170210_040046_info_change cannot be reverted.\n";

        return false;
    }
}
