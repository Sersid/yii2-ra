<?php

use yii\db\Migration;

class m170515_072116_cities extends Migration
{
    public function up()
    {
        $this->addColumn('{{%city}}', 'title', $this->string()->null());
        $this->addColumn('{{%city}}', 'content', $this->text()->null());
    }

    public function down()
    {
        $this->dropColumn('{{%city}}', 'title');
        $this->dropColumn('{{%city}}', 'content');
    }
}
