<?php

use yii\db\Migration;

/**
 * Class m210113_234641_user_radio_ad_prices
 */
class m210113_234641_user_radio_ad_prices extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_radio_ad_radio}}', 'price_without_discount', $this->money()->null());
        $this->addColumn('{{%user_radio_ad_radio}}', 'price', $this->money()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_radio_ad_radio}}', 'price_without_discount');
        $this->dropColumn('{{%user_radio_ad_radio}}', 'price');
    }
}
