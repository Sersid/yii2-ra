<?php

namespace console\controllers;

use common\models\Guest;
use yii\console\Controller;
use Yii;
use yii\helpers\Console;

class ClearTrashController extends Controller
{
    public function actionIndex()
    {
        $query = Guest::find()->with(['outdoors', 'radios', 'tickers', 'tv'])->joinWith('orders')->where(
            ['<', 'guest.updated_at', time() - 60 * 60 * 24 * 30]
        )->andWhere(['order.id' => null])->limit(500);
        $cnt = 0;
        // Удаление по 500 записей
        while (true) {
            $arItems = $query->all();
            if (empty($arItems)) {
                break;
            }
            $cnt += count($arItems);
            foreach ($arItems as $item) {
                $item->delete();
                $this->stdout("Guest #" . $item->id . " is deleted\n", Console::FG_RED, Console::UNDERLINE);
            }
            unset($arItems);
        }
        $this->stdout('Done. Deleted ' . $cnt . ' quests');
    }
}
